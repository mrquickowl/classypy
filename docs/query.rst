query
===================

This module provides methods to query data and send it to redshift.

query
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: classypy.query
   :members:


redshift
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: classypy.query.redshift
   :members:

ml
=============================

The **ml** module provides various resources for machine learning.

Under `ml/text`, you will find several methods for natural language processing:

Classes in this repo wrap the ``scikit-learn`` API, and provide the familiar ``fit`` and ``transform`` methods.

  - ``transformers`` provides the ``TextProcessor`` class. This class is used for pre-processing and tokenization of raw text data.
  - ``vectorizers`` provides ``scikit-learn`` wrappers for common vectorization methods in `gensim`.

    - ``GensimBoW``: Bag of Words implementation that uses ``gensim`` under the hood.
    - ``GensimTfidf``: Term Document-Inverse Document Frequency implementation that uses ``gensim`` under the hood.

  - ``distributed_representations`` provides scikit-learn wrappers for gensims `word2vec` and `doc2vec` models.

    - ``GensimWord2Vec``: word2vec implementation that uses ``gensim`` under the hood.
    - ``GensimDoc2Vec``: doc2vec implementation that uses ``gensim`` under the hood.

  - ``topics`` provides scikit-learn wrappers for topic-modeling algorithms, such as Latent Semantic Indexing.
    - ``GensimLsi``: Latent Semantic Analysis model that uses ``gensim`` under the hood.



ml Documentation
----------------


transformers
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
.. automodule:: classypy.ml.transformers
   :members:


Distributed Representations
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: classypy.ml.distributed_representations
    :members:


Bag of Words
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: classypy.ml.vectorizers
   :members:


distances
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: classypy.ml.distances
   :members:


Pipelines
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: classypy.ml.text_pipeline
  :members:

dimensionality_reduction
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: classypy.ml.dimensionality_reduction
   :members:


text_analysis
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:(:) classypy.ml.text_analysis
..   :members:


text_functions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: classypy.ml.text_functions
   :members:

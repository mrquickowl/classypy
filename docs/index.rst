.. classypy documentation master file, created by
   sphinx-quickstart on Tue Jul  4 01:19:52 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

====================================
Welcome to classypy's documentation!
====================================
------------------------------------
Classypy
------------------------------------

``Classypy`` is an open-source release of Classy.org Data Insight's internal repository of shared Python functions.

The intent is to encapsulate functionality that spans repos.

Subdirectories:
===============

* ``io`` - A set of ``Datasets`` and ``Fetchers`` for documenting external datasets, retrieving the data and files, and returning them in a clean, consistent, and massaged format.
* ``ml`` - Machine learning functions
* ``query`` - Using ssh tunnels with queries / running devops-type queries on redshift.
* ``util`` - Utility functions (e.g. defining & accessing common repo structures).
* ``viz`` - Visualization functions.


.. toctree::
   :maxdepth: 1
   :caption: Documentation Contents:

   io
   ml
   viz
   query
   devops
   charity
   util


Installation
=============

For use in prod:

``pip install git+ssh://git@bitbucket.org/stayclassy/classypy.git``

For local development:

``git clone git@bitbucket.org/stayclassy/classypy.git``, then add the directory the code was cloned to (e.g. ``~/code/classypy``) to your ``PYTHONPATH`` environment variable.

If using any text processing procedures from ``classypy.ml``, you will need to run ``python -c "import nltk; nltk.download('stopwords')"``


Testing
========

Tests in ``classypy`` are written with ``pytest``.

The test layout uses inlining test directories; tests are distributed alongside each
submodule.

To create a new test, simply as a ``test_<your_test>.py`` file into the submodule's corresponding ``tests/`` directory.

To run tests, simply call ``pytest`` from the command-line, at the head of your projects directory.
To test tests for a specific dir, call ``pytest <path/to/dir``.

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

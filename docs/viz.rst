viz
=================

This provides various functionalities for data visualization.


mapping_functions
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: classypy.viz.mapping_functions
   :members:


plotting
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

.. automodule:: classypy.viz.plotting
   :members:

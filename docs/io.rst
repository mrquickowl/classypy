=============================
io
=============================
---------------------------------------------------------
classypy.io - social impact datasets at your fingertips
---------------------------------------------------------

Classypy.io provides documentation and easy access functions for datasets on social impact. Features include:

* **Resumable, cached downloads** - Data are downloaded once and cached. If your Internet connection fails while downloading a dataset, it is in many times resumable.
* **Transformation to Pandas** - All data are returned as Pandas dataframes. You can also access raw downloads if desired.
* **Few global dependencies** - Dependencies are installed dynamically as needed, so that you're not installing hundreds of libraries up front.
* **Extensibility** - Don't have access to your favorite data provider? See instructions below to add your own!


Dependencies
================

``classypy.io`` is tested in Python 2.7, and 3.5. The only package-level dependencies are ``pip``, ``numpy``, and ``six``.

Individual datasets may have package dependencies for downloads or examples. If so, this package attempts to install them via pip.


Usage
======

To run an example,
``python classypy/io/datanyze/example1.py``

To download data::

  python
  from classypy.io import DatanyzeDataset
  DatanyzeDataset(api_email=#####, api_secret=######).fetch(reports=[1234])


Adding new datasets
===================

To add a dataset, you should:

* Duplicate ``_example`` to a new subdirectory.
* Add any needed fetcher, and the datset, to ``__init__.py``
* Add an example as ``example1.py``
* Document the dataset in ``README.rst``.
* Add tests (when possible).

#### Using the HTTP Fetcher

The HTTP Fetcher class can be confusing. Here's some tips & tricks to getting what you want.

* If you are downloading a compressed file, and you want the compressed directory:
   * Pass tuple: ``(dest_dir, url, {'uncompress': True, 'move': True})``` to unpack the compressed file, and to move it as a directory into `dest_dir`.
* If you are downloading a compressed file, and you just want the file/file contents
   * Pass tuple: ``(dest_dir, url, {'uncompress': True,})``` to unpack the compressed file, and to move the file contents to your base dataset directory.
* If you are downloading a file, and you don't care about the name:
   * Pass string: ``url``; it will be downloaded and moved to a filename within your base dataset directory.


Common questions
=====================

* What's a ``Dataset`` vs. ``Fetcher``?

   ``Fetcher`` objects know how to get and cache raw data. ``Dataset`` objectss should
   massage the raw data returned into a more usable format, and provide
   kind filters on top of the data, to determine what data should be fetched.

   In other words, ``Fetcher`` objects provide data access (deal with oauth, http, APIs, etc),
   while ```Dataset`` objectss massage data once it's downloaded.

* How do I associate a ``Fetcher`` with a dataset?

   Set the ``fetcher_class`` class-level property on the dataset to the
   ``Fetcher`` of your choice. When ``__init__`` is called (either by your ``Dataset``
   through ``super(MyDatasetClass, self).__init__``), all unknown parameters are
   passed to the ``Fetcher`` class' constructor. So, send any needed params
   to the fetcher for construction there!

   If you'd like to create your own ```Fetcher`` subclass, just import
   ``classypy.io.core.fetchers.Fetcher``. The base class implements
   a constructor and a `fetch` method - that's it! The ``fetch`` method
   should simply download data and return it, or load cached downloads
   and return that same data from the cache. It should do minimal processing
   on the actual data, just focus on downloading the bare minimum of data
   necessary to service the request, and caching of that data.


* Which ``Dataset`` should I subclass?

Great question! It depends on your needs:

   * Most datasets will use the ``HttpDataset`` - data are available as unauthenticated file downloads.
   * A frequent use-case is also to access an API. Use the ``HttpApiDataset`` in that case; it also contains some handy functions for translating JSON responses to dataframes.
   * If you're going to use a new SDK or data source, then just extend the base `Dataset`. Put the logic for downloading files into a new ``Fetcher``, and instantiate that ``Fetcher`` from within your ``Dataset``'s constructor. You'll have to implement nice things, like caching, some of the core library functions found in ``core/fetchers/http_fetchers.py``.

* What functions do I implement?

The main user interfaces into a dataset is the class constructor, and the ``fetch`` function. Keep your ``__init__`` and ``fetch`` functions short by adding new

   * The constructor should take any args that are needed to access the data (e.g. authentication, secrets, etc).
   * ``fetch`` should take of the standard arguments (see the base class; these in clude: ``force`` and ```verbose``), as well as arguments for filtering your data. By default, your data download should be complete, if reasonably possible; allow simple filters (e.g. a list of ids, or a number of results) to allow the user to avoid downloading the entire dataset.


io Documentation
=====================



core
^^^^^^^^^^^^^^^^^^^^


.. automodule:: classypy.io.core.datasets
   :members:

.. automodule:: classypy.io.core.fetchers
   :members:



datanyze
^^^^^^^^^^^^^^^^^^^^

.. automodule:: classypy.io.datanyze
   :members:


desk
^^^^^^^^^^^^^^^^^^^^

.. automodule:: classypy.io.desk
   :members:


Facebook
^^^^^^^^^^^^^^^^^^^^

.. automodule:: classypy.io.facebook
   :members:


google
^^^^^^^^^^^^^^^^^^^^

.. automodule:: classypy.io.google
   :members:


irs
^^^^^^^^^^^^^^^^^^^^

.. automodule:: classypy.io.irs
   :members:


metro
^^^^^^^^^^^^^^^^^^^^

.. automodule:: classypy.io.metro
   :members:


propublica
^^^^^^^^^^^^^^^^^^^^

.. automodule:: classypy.io.propublica
   :members:


rjmetrics
^^^^^^^^^^^^^^^^^^^^

.. automodule:: classypy.io.rjmetrics
   :members:


social_media
^^^^^^^^^^^^^^^^^^^^

.. automodule:: classypy.io.social_media
   :members:



twitter
^^^^^^^^^^^^^^^^^^^^

.. automodule:: classypy.io.twitter
   :members:

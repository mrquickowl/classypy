# ClassyPy Docs

Classypy Docs are written using [Sphinx](http://www.sphinx-doc.org/en/stable/)


## Installation

To install the required packages, inside `classypy/` call:

`pip install -r requirements_docs.txt`

## Viewing Docs

Viewing the documentation in its development stage requires several easy steps:

1. Navigate to `classypy/docs/`.
2. Call `make html` in the terminal.
3. Navigate to `classypy/docs/_build/html`.
4. Launch a web-server in this dir. This can be done easily with python by calling `python -m SimpleHTTPServer 8000`.
5. Open a web-browser and switch to the localhost port running the server. In the above example, this would be `localhost:8000`.
6. View documentation.


## Adding/Editing Documentation

Unlike this `README.md` file, all documentation is written with `reStructuredText Markup`.

To edit a particular file, edit the associated `.rst` file. For example, to add documentation to
`classypy/io`, edit `docs/io.rst`.

If you want new documentation added to an existing module to be uploaded to the docs, after updating the code:

1. In terminal, inthe `classypy/docs` directory, call `make clean`.
2. Call `make html`.

If you want to add documentation for new modules, create an associated `<new_module>.rst` file,
and follow the structure of the other `.rst` files.

# Add new packages to classypy/version.py ; new dependencies there and here.
--no-binary psycopg2
--index-url https://pypi.python.org/simple/

-e git+git://github.com/bcipolli/isort.git#egg=isort
-e git+https://github.com/bcipolli/nbstripout.git#egg=7c842d72480698466606cc4355130c3b4aac309f
-e .

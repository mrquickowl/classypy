set -e  # Stop on first failure.

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
REPO_DIR="`realpath $DIR/../classypy`"
EXCLUDES=$REPO_DIR/ml/tests/docs.py,$REPO_DIR/version.py,$REPO_DIR/external/*
SQL_LINTING_PATH="${*:-$REPO_DIR}"

echo "Testing Data Insights annotations..."
"$REPO_DIR/linters/annotations.sh" "$REPO_DIR" "$EXCLUDES"

echo "Testing sql style..."
python "$REPO_DIR/linters/sql.py" $SQL_LINTING_PATH

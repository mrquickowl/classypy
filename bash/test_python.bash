set -e  # Stop on first failure.

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
REPO_DIR="`realpath $DIR/../classypy`"
EXCLUDES=$REPO_DIR/ml/tests/docs.py,$REPO_DIR/version.py,$REPO_DIR/external/*

echo "Testing pep8 code style..."
flake8 "$REPO_DIR" --exclude "$EXCLUDES"

echo "Testing Data Insights python styles..."
"$REPO_DIR/linters/python.sh" "$REPO_DIR" "$EXCLUDES" "package"

echo "Testing code security..."
# Skip B608 - SQL injection test
bandit -s B301,B303,B608 -ll -r -x "$EXCLUDES" "$REPO_DIR"

echo "Testing code complexity..."
xenon "$REPO_DIR" --exclude "$EXCLUDES,$REPO_DIR/io/core/fetchers/http_fetcher.py,$REPO_DIR/ml/text_functions.py,$REPO_DIR/io/irs/tax_extract.py" --ignore "$EXCLUDES" --max-average C --max-modules C --max-absolute C

echo "Testing code cruft..."
vulture "$REPO_DIR" "--exclude=$EXCLUDES" | grep -v attribute | grep -v function | grep -v class | echo "$(</dev/stdin)"

pytest "$REPO_DIR"

set -e  # Stop on first failure.

MD_FILES=`find . -name \*.md | grep -v venv`

echo "Testing Markdown style..."
mdl $MD_FILES -i --ignore-front-matter -s classy_data_insights

import os
import os.path as op

CLASSYPY_DATA_PATH = os.environ.get(
    'CLASSYIO_PATH',
    op.join(op.expanduser("~"), 'classypy_data'))

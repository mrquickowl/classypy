import os

import psycopg2

from classypy.devops import logger
from classypy.query import redshift


class MockContext(object):
    """Mocks the context object used by Lambda.

    See: https://docs.aws.amazon.com/lambda/latest/dg/python-context-object.html"""
    def __init__(self, function_name):
        self.function_name = function_name
        self.aws_request_id = 0

    def get_remaining_time_in_millis(self):
        return 300000  # Always return that 5 minutes are remaining


def get_redshift_connection(async_=False):
    """Gets a Redshift connection using credentials stored in environment variables.

    Parameters
    ----------
    async_ : bool, optional
        If True, return an asynchronous connection (default: False)
        http://initd.org/psycopg/docs/advanced.html#asynchronous-support

    Returns
    -------
    psycopg2.extensions.connection
        A Psycopg2 connection for Redshift.
    """
    if os.environ.get('REDSHIFT_PASSWORD') is None:
        logger.debug('Fetching temporary credentials.')
        temporary_creds = redshift.fetch_temporary_credentials(
            user=os.environ['REDSHIFT_USERNAME'],
            database=os.environ['REDSHIFT_DATABASE'],
            cluster_identifier=os.environ['REDSHIFT_CLUSTER_IDENTIFIER'],
        )

        redshift_username = temporary_creds['redshift_username']
        redshift_password = temporary_creds['redshift_password']
    else:
        redshift_username = os.environ['REDSHIFT_USERNAME']
        redshift_password = os.environ['REDSHIFT_PASSWORD']

    logger.debug('Connecting to Redshift.')
    connection = psycopg2.connect(
        database=os.environ['REDSHIFT_DATABASE'],
        user=redshift_username,
        password=redshift_password,
        host=os.environ['REDSHIFT_HOST'],
        port=os.environ['REDSHIFT_PORT'],
        async_=async_,
    )
    logger.debug('Connected to Redshift.')
    return connection


def load_kwargs_to_environment(required_kwargs, optional_kwarg_sets=[], **kwargs):
    """Loads values from kwargs into os.environ, to simulate Lambda environment variables.

    Parameters
    ----------
    required_kwargs : iterable of strings
        A collection of named arguments that must be in kwargs, and will be loaded.
    optional_kwarg_sets : iterable of iterables of strings
        Each element of this collection is a collection of strings.
        For each inner collection, at least one of the strings must have a corresponding named argument to be loaded.
    kwargs : dict
        Named arguments.

    Side-effects
    ------------
    Modifies os.environ, possibly overriding existing state.
    """
    for named_arg in required_kwargs:
        if named_arg not in kwargs:
            raise ValueError("Named argument {named_arg} must be defined.".format(named_arg=named_arg))
        os.environ[named_arg] = str(kwargs[named_arg])

    for optional_kwargs in optional_kwarg_sets:
        if all([named_arg not in kwargs for named_arg in optional_kwargs]):
            raise ValueError("One of {optional_kwargs} must be defined.".format(optional_kwargs=optional_kwargs))
        for named_arg in optional_kwargs:
            if named_arg in kwargs:
                os.environ[named_arg] = str(kwargs[named_arg])


def load_redshift_kwargs_to_environment(**kwargs):
    """Loads necessary Redshift named arguments from kwargs to os.environ, to simulate Lambda environment variables.

    Parameters
    ----------
    kwargs : dict
        Named arguments.
    """
    load_kwargs_to_environment(
        required_kwargs=('REDSHIFT_DATABASE', 'REDSHIFT_HOST', 'REDSHIFT_PORT', 'REDSHIFT_USERNAME'),
        optional_kwarg_sets=[('REDSHIFT_CLUSTER_IDENTIFIER', 'REDSHIFT_PASSWORD')],
        **kwargs
    )

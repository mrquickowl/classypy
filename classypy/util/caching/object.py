"""
Methods for caching objects as pickle files.
"""
import functools
import os
import os.path as op

import dill

from classypy.compat import func_has_param
from classypy.devops import logger


def save_object(*outer_args, **outer_kwargs):
    """
    Decorator for saving dataframes to a pkl file.

    Parameters
    ----------
    pkl_file : string (Default: None)
        File name to save dataframe to (None will prevent saving)
    keep_pkl_file :  bool (Default: False)
        If True, don't remove the pkl_file parameter from the calling function.
    verbose : int (Default: 1)
        Level of verbosity

    Returns
    -------
    Dataframe from wrapped function

    Side Effects
    ------------
    Saves the dataframe to disk, if pkl_file is specified.
    """
    keep_pkl_file = outer_kwargs.pop('keep_pkl_file', False)

    def save_object_decorator(func):
        @functools.wraps(func)
        def save_object_inner_func(*args, **kwargs):
            # we don't want pkl_file as the first param.
            pkl_file = kwargs.pop('pkl_file', None)
            if keep_pkl_file or func_has_param(func, 'pkl_file'):
                # Share 'pkl_file' with downstream function
                kwargs['pkl_file'] = pkl_file

            # Call Dataframe generating function
            obj = func(*args, **kwargs)

            # Skip saving, if no csv file specified.
            if pkl_file is None:
                logger.debug("Not saving pkl file for {module}.{func}; no pkl_file parameter specified.".format(
                    module=func.__module__, func=func.__name__))
                return obj

            try:
                if op.dirname(pkl_file) and not op.exists(op.dirname(pkl_file)):
                    # Make the directory, if it is specified (i.e. pkl_file != 'file.csv')
                    os.makedirs(op.dirname(pkl_file))
                with open(pkl_file, 'wb') as fp:
                    dill.dump(obj, fp, **outer_kwargs)

                logger.info("Saved dataframe to {pkl_file}".format(pkl_file=pkl_file))
            except:  # noqa: E722
                # TODO: specify errors that could cause an issue.
                if op.exists(pkl_file):
                    # Don't allow storage of broken file.
                    os.remove(pkl_file)
                raise

            return obj
        return save_object_inner_func
    return save_object_decorator if not outer_args else save_object_decorator(*outer_args)


def cache_object(*args, **outer_kwargs):
    """
    A decorator for functions that returns pandas.DataFrame objects.
    Adds caching functionality (saving / loading) dataframes to/from a pkl file.

    The decorated function will check for an existing file in the path
    specified by `pkl_file`, and if available, load and return the dataset
    without running func(). If no existing file is found,
    the returned dataframe will be written to the path.

    Parameters
    ----------
    pkl_file : string (Default: None)
        File name to save dataframe to (None will prevent saving)
    force : bool (Default: False)
        If False, any existing dataset in the path specified by `pkl_file`
        will be loaded and returned without running func().
        If True, func() will be always run and the results saved,
        overriding any existing file in the path.
    verbose : int (Default: 1)
        Level of verbosity

    Returns
    -------
    Dataframe from wrapped function

    Side Effects
    ------------
    Saves the dataframe to disk, if pkl_file is specified.
    """
    def cache_object_decorator(func):
        @functools.wraps(func)
        def cache_object_inner_func(*args, **kwargs):
            # we don't want pkl_file as the first param.
            pkl_file = kwargs.pop('pkl_file', None)

            # Force is optional, and we don't want it as the first param
            force = kwargs.pop('force', False)

            if not force and pkl_file and op.exists(pkl_file):
                logger.debug("Loading cached data from {pkl_file}".format(pkl_file=pkl_file))
                with open(pkl_file, 'rb') as fp:
                    return dill.load(fp, **outer_kwargs)
            elif force:
                logger.debug("Force re-run on {pkl_file}".format(pkl_file=pkl_file))
            elif pkl_file and not op.exists(pkl_file):
                logger.debug("Could not find {pkl_file} ".format(pkl_file=pkl_file))

            if func_has_param(func, 'force'):
                # Share 'force' with downstream function
                kwargs['force'] = force

            wrapped_func = save_object(**outer_kwargs)(func)
            return wrapped_func(pkl_file=pkl_file, *args, **kwargs)
        return cache_object_inner_func
    return cache_object_decorator if not args else cache_object_decorator(*args)

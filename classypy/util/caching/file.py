"""
Methods for caching text files.
"""
import codecs
import functools
import os
import os.path as op

from classypy.compat import func_has_param
from classypy.devops import logger


def save_file(*outer_args, **outer_kwargs):
    """
    Decorator for saving files.

    Parameters
    ----------
    file_path : string (Default: None)
        File name to save text document to (None will prevent saving)
    encoding : string (Default: utf-8)
        Text encoding.
    keep_file : bool (Default: False)
        If True, don't remove the file_path parameter from the calling function.
    verbose : int (Default: 1)
        Level of verbosity

    Returns
    -------
    Dataframe from wrapped function

    Side Effects
    ------------
    Saves the content to disk, if file_path is not None.
    """
    encoding = outer_kwargs.pop('encoding', 'utf-8')
    keep_file = outer_kwargs.pop('keep_file', False)

    def save_file_decorator(func):
        @functools.wraps(func)
        def save_file_inner_func(*args, **kwargs):
            file_path = kwargs.pop('file_path', None)
            if keep_file or func_has_param(func, 'file_path'):
                # Share file_path with downstream function
                kwargs['file_path'] = file_path

            # Run the function
            content = func(*args, **kwargs)

            # Skip saving, if no csv file specified.
            if file_path is None:
                logger.debug("Not saving txt file; no file_path parameter specified.")
                return content

            # Make the path
            file_dir = op.dirname(file_path)
            if file_dir and not op.exists(file_dir):
                os.makedirs(file_dir)

            try:
                if op.dirname(file_path) and not op.exists(op.dirname(file_path)):
                    os.makedirs(op.dirname(file_path))
                with codecs.open(file_path, 'w', encoding=encoding) as fp:
                    # Cache results
                    fp.write(content)
                logger.info("Saved text file to {file_path}".format(file_path=file_path))
            except:  # noqa: E722
                # TODO: specify errors that could cause an issue.
                if op.exists(file_path):
                    # Don't allow storage of broken file.
                    os.remove(file_path)
                raise

            return content
        return save_file_inner_func
    return save_file_decorator if not outer_args else save_file_decorator(*outer_args)


def cache_file(*outer_args, **outer_kwargs):
    """
    A decorator for functions that returns text data.
    Adds caching functionality (saving / loading) of text to/from .txt files.

    The decorated function will check for an existing file in the path
    specified by `file_path`, and if available, load and return the content
    without running func(). If no existing file is found,
    the returned dataframe will be written to the path.

    Parameters
    ----------
    file_path : string (Default: None)
        File name to save text document to (None will prevent saving)
    encoding : string (Default: utf-8)
        Text encoding.
    force : bool (Default: False)
        If False, any existing dataset in the path specified by `pkl_file`
        will be loaded and returned without running func().
        If True, func() will be always run and the results saved,
        overriding any existing file in the path.
    verbose : int (Default: 1)
        Level of verbosity

    Returns
    -------
    Dataframe from wrapped function

    Side Effects
    ------------
    Saves the text document to disk, if file_path is specified.
    """
    encoding = outer_kwargs.pop('encoding', 'utf-8')

    def cache_file_decorator(func):
        @functools.wraps(func)
        @save_file(keep_file=True, encoding=encoding, **outer_kwargs)
        def cache_file_inner_func(*args, **kwargs):
            # file_path is obligatory, but we don't want it as the first param.
            file_path = kwargs.pop('file_path', None)

            # Force is optional, and we don't want it as the first param
            force = kwargs.get('force', False)
            if 'force' in kwargs:
                del kwargs['force']

            if not force and file_path and op.exists(file_path):
                # Use the cache file
                with codecs.open(file_path, 'r', encoding=encoding) as fp:
                    # Cache results
                    return fp.read()

            if func_has_param(func, 'force'):
                # Share 'force' with downstream function
                kwargs['force'] = force
            if func_has_param(func, 'file_path'):
                # Share file_path with downstream function
                kwargs['file_path'] = file_path

            content = func(*args, **kwargs)
            return content
        return cache_file_inner_func
    return cache_file_decorator if not outer_args else cache_file_decorator(*outer_args)

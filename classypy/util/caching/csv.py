"""
Methods for caching dataframes and csv files.
"""
import functools
import os
import os.path as op

import pandas as pd

from classypy.compat import func_has_param
from classypy.devops import logger
from classypy.util import update_dict


def _to_csv_kwargs(**kwargs):
    """Take overlapping kwargs for read/write csv, translate to to_csv kwargs."""
    out_kwargs = {}
    for key, val in kwargs.items():
        if key in ('dtype', 'converters', 'parse_dates'):
            # read-only key
            continue
        else:
            out_kwargs[key] = val
    return out_kwargs


def _read_csv_kwargs(**kwargs):
    """Take overlapping kwargs for read/write csv, translate to read_csv kwargs."""
    out_kwargs = {}
    for key, val in kwargs.items():
        if key == 'index':
            # if True, assume the index is column 0. False: None
            out_kwargs['index_col'] = 0 if val else None
        else:
            out_kwargs[key] = val
    return out_kwargs


def save_dataframe(*outer_args, **outer_kwargs):
    """
    Decorator for saving dataframes.

    Parameters
    ----------
    csv_file : string (Default: None)
        File name to save dataframe to (None will prevent saving)
    encoding : string (Default: utf-8)
        Text encoding passed to Pandas, for saving dataframes.
    index : bool (Default: False)
        If True, the dataframe index is saved as the first column
    keep_csv_file :  bool (Default: False)
        If True, don't remove the csv_file parameter from the calling function.

    Returns
    -------
    Dataframe from wrapped function

    Side Effects
    ------------
    Saves the dataframe to disk, if csv_file is specified.
    """
    if 'index_col' in outer_kwargs:
        raise ValueError('index_col is not supported by save_dataframe.')

    encoding = outer_kwargs.pop('encoding', 'utf-8')
    index = outer_kwargs.pop('index', False)
    keep_csv_file = outer_kwargs.pop('keep_csv_file', False)
    date_format = outer_kwargs.pop('date_format', '%Y-%m-%d %H:%M:%S.%f')
    default_csv_file = outer_kwargs.pop("default_csv_file", None)

    def save_dataframe_decorator(func):
        @functools.wraps(func)
        def save_dataframe_inner_func(*args, **kwargs):
            # we don't want csv_file as the first param.
            csv_file = kwargs.pop('csv_file', None) or default_csv_file
            if keep_csv_file or func_has_param(func, 'csv_file'):
                # Share 'csv_file' with downstream function
                kwargs['csv_file'] = csv_file

            # Run the function
            df = func(*args, **kwargs)

            # Skip saving, if no csv file specified.
            if csv_file is None:
                logger.debug("Not saving csv file for {module}.{func}; no csv_file parameter specified.".format(
                    module=func.__module__, func=func.__name__))
                return df

            elif df is None:
                logger.info("Deleting file as dataframe is None")
                if op.exists(csv_file):
                    os.remove(csv_file)
                return

            try:
                if op.dirname(csv_file) and not op.exists(op.dirname(csv_file)):
                    # Make the directory, if it is specified (i.e. csv_file != 'file.csv')
                    os.makedirs(op.dirname(csv_file))
                df.to_csv(csv_file, **_to_csv_kwargs(index=index, encoding=encoding,
                                                     date_format=date_format, **outer_kwargs))
                logger.info("Saved dataframe to {csv_file}".format(csv_file=csv_file))
            except:  # noqa: E722
                # TODO: specify errors that could cause an issue.
                if op.exists(csv_file):
                    # Don't allow storage of broken file.
                    os.remove(csv_file)
                raise

            return df
        return save_dataframe_inner_func
    return save_dataframe_decorator if not outer_args else save_dataframe_decorator(*outer_args)


def cache_dataframe(*args, **outer_kwargs):
    """
    A decorator for functions that return pandas.DataFrame objects.
    Adds caching functionality (saving / loading) dataframes to/from csv.

    The decorated function will check for an existing file in the path
    specified by `csv_file`, and if available, load and return the dataset
    without running func(). If no existing file is found,
    the returned dataframe will be written to the path.

    Used throughout classypy/io with the Dataset class,
    particularly in conjunction with the `force` argument.

    Parameters
    ----------
    csv_file : string (Default: None)
        File name to save dataframe to (None will prevent saving)
    encoding : string (Default: utf-8)
        Text encoding passed to Pandas, for saving dataframes.
    index : bool (Default: False)
        If True, the dataframe index is saved as the first column
    force : bool (Default: False)
        If False, any existing dataset in the path specified by `csv_file`
        will be loaded and returned without running func().
        If True, func() will be always run and the results saved,
        overriding any existing file in the path.

    Returns
    -------
    Dataframe from wrapped function

    Side Effects
    ------------
    Saves the dataframe to disk, if csv_file is specified.
    """
    if 'index_col' in outer_kwargs:
        raise ValueError('index_col is not supported by cache_dataframe.')

    # Set some defaults
    outer_kwargs = update_dict({"encoding": "utf-8"}, outer_kwargs)
    # Remove some settings
    default_csv_file = outer_kwargs.pop("default_csv_file", None)

    def cache_dataframe_decorator(func):
        @functools.wraps(func)
        def cache_dataframe_inner_func(*args, **kwargs):
            # we don't want csv_file as the first param.
            csv_file = kwargs.pop('csv_file', None) or default_csv_file

            # Force is optional, and we don't want it as the first param
            force = kwargs.pop('force', False)

            if not force and csv_file and op.exists(csv_file):
                logger.debug("Loading cached data from {csv_file}".format(csv_file=csv_file))
                return pd.read_csv(csv_file, **_read_csv_kwargs(**outer_kwargs))
            elif force:
                logger.debug("Force re-run on {csv_file}".format(csv_file=csv_file))
            elif csv_file and not op.exists(csv_file):
                logger.debug("Could not find {csv_file} ".format(csv_file=csv_file))

            if func_has_param(func, 'force'):
                # Share 'force' with downstream function
                kwargs['force'] = force

            wrapped_func = save_dataframe(default_csv_file=default_csv_file, **outer_kwargs)(func)
            return wrapped_func(csv_file=csv_file, *args, **kwargs)
        return cache_dataframe_inner_func
    return cache_dataframe_decorator if not args else cache_dataframe_decorator(*args)

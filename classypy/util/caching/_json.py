import functools
import json
import os.path as op
import warnings

from classypy.devops import logger


def cache_as_json(func):
    """Decorator to cache an object as JSON.

    see cache_dataframe for overall usage.

    Parameters
    ----------
    json_path : string
        Path of cache file.
    force : boolean (optional; Default: False)
        When True, will run wrapped function even if cache file exists.
    cache_only : boolean (optional; Default: False)
        When True, will check if cache file exists and run the function if not;
        will not load cached results.

    Returns
    -------
    When not cache_only, returns a JSON-serializable object (e.g. dict, array)
    When cache_only, returns None (if object cached) or an object (if not).

    Side-effects
    ------------
    Updates timestamp and contents of json_path.
    """
    @functools.wraps(func)
    def cache_as_json_inner(*args, **kwargs):
        json_path = kwargs.pop('json_path', None)
        force = kwargs.pop('force', False)
        cache_only = kwargs.pop('cache_only', False)

        if not force and json_path and op.exists(json_path):
            if cache_only:
                logger.debug("Found cached data at {json_path}".format(json_path=json_path))
                return None  # Just detect
            logger.debug("Loading cached data from {json_path}".format(json_path=json_path))
            try:
                with open(json_path, 'r') as fp:
                    return json.load(fp)
            except Exception as ex:  # noqa
                # Could not load file; report and run the function.
                warnings.warn(str(ex))

        obj = func(*args, **kwargs)

        if json_path:
            with open(json_path, 'w') as fp:
                json.dump(obj, fp)

        return obj
    return cache_as_json_inner

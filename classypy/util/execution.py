from multiprocessing import cpu_count

import numpy as np
from dask import compute, delayed

from classypy.devops import logger
from classypy.util.etl import dict_of_lists_to_list_of_dicts


def wrap_with_error_handling(func, return_errors):
    """Wrap function to catch and return exceptions, if requested."""
    if not return_errors:
        return func

    def func_result_or_exception(*args, **kwargs):
        """Thin wrapper that returns a function result, or the error caught while executing the function."""
        try:
            return func(*args, **kwargs)
        except Exception as ex:  # noqa
            return ex
    return func_result_or_exception


def run_in_chunks(func, params, chunk_size, return_errors=False):
    """
    Execute in sequential chunks. Good for breaking long jobs
    into more easily observable pieces

    Parameters
    ----------
    func: function
    params: dict of lists
        Containing parameters. Each list should have an equivalent length
    chunk_size: int
        How many func calls to batch together before logging a result.
    return_errors: bool
        If true, captures errors and returns in output list

    Returns
    -------
    List of values, one per batch. If return_errors = True and any function call raises,
    the value for that function call within the batch will be the exception raised.

    Raises
    ------
    If return_array is False and a function call raises, that single exception
    will be raised to the caller of this function.
    """
    func_to_chunk = wrap_with_error_handling(func, return_errors=return_errors)
    param_lengths = [len(val) for val in params.values()]
    assert len(np.unique(param_lengths)) == 1, (
        "All parameter lists have the same length")

    # Generate parameter batches
    num_params = param_lengths[0]  # They're all the same, so just choose one.
    num_batches = int(np.ceil(1. * num_params / chunk_size))
    # Split as lists, rather than np arrays.
    # Arrays act different when testing for equality.
    param_batches = {key: [split.tolist() for split in np.array_split(val, num_batches)]
                     for key, val in params.items()}
    param_batches = dict_of_lists_to_list_of_dicts(param_batches)

    # Run each batch sequentially
    results = []
    for pi, param_batch in enumerate(param_batches):
        logger.info("Running batch {batch_num} of {num_batches} batches..".format(
            batch_num=pi + 1, num_batches=num_batches))
        results.append(func_to_chunk(**param_batch))
        logger.info("Done.")
    return results


def run_functions_in_parallel(funcs, n_workers=cpu_count(), return_errors=False):
    """
    Executes different functions in parallel. funcs is a list of functions that can be called
    without any arguments. If the functions require arguments, then functools.partial() should
    be used to apply arguments to their corresponding functions.

    Parameters
    ----------
    funcs: list-like
        A list of functions to be parallelized
    n_workers: int, required
        A integer specifying the number of workers to spin up to process
        the functions in funcs in parallel. By default, this is set to
        the number of cores
    return_errors: bool
        If true, captures errors and returns in output list

    Returns
    -------
    List of values. If return_errors = True and any function call raises,
    the value for that function call within the batch will be the exception raised.

    Raises
    ------
    If return_errors is False and a function call raises, that single exception
    will be raised to the caller of this function.
    """
    from concurrent.futures import ProcessPoolExecutor, as_completed

    executor = ProcessPoolExecutor(max_workers=n_workers)
    executor_list = []
    for func in funcs:
        executor_list.append(executor.submit(func))

    return_list = [x.result() for x in as_completed(executor_list)]

    return return_list


def run_in_parallel(func, params, return_errors=False):
    """
    Execute in parallel.

    Parameters
    ----------
    func: function
    params: list of dicts
    return_errors: bool
        If true, captures errors and returns in output list

    Returns
    -------
    List of values. If return_errors = True and any function call raises,
    the value for that function call within the batch will be the exception raised.

    Raises
    ------
    If return_array is False and a function call raises, that single exception
    will be raised to the caller of this function.
    """
    func_to_parallelize = wrap_with_error_handling(func, return_errors=return_errors)

    funcs = [delayed(func_to_parallelize)(**kwargs) for kwargs in params]
    values = compute(*funcs, scheduler="threads")

    return list(values)

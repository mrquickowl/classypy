import collections
import json
import re

import pandas as pd
import six

from classypy.compat import u as unicode


def numeric_val(val, numeric_type):
    """Convert float/object column into empty string and the given numeric type."""
    if pd.isnull(val) or val == '':
        return ''
    try:
        return numeric_type(val)
    except:  # noqa: E722
        return ''


def int_val(val):
    """Convert float/object column into empty string and ints."""
    return numeric_val(val, int)


def float_val(val):
    """Convert float/object column into empty string and floats."""
    return numeric_val(val, float)


def normalize_whitespace(msg, normalize_escaped=True):
    """Normalize each whitespace character to a space (including newlines)."""
    if pd.isnull(msg):
        return msg
    if isinstance(msg, six.string_types):
        # Deal with escaped data.
        if normalize_escaped:
            msg = re.sub(r'[\\]+[rnt]', ' ', msg, flags=re.M | re.I | re.S)
        msg = re.sub(r'\s+', ' ', msg, flags=re.M | re.S)
    return msg


def to_ascii(s):
    """Returns string-native type for version (i.e. bytes for PY2, unicode for PY3),
    with ascii/valid characters only.
    """
    if pd.isnull(s):
        return s
    try:
        ascii_bytes = unicode(s).encode('utf-8', 'ignore')
    except:  # noqa
        # This happens when bytes has a `\uxxx` invalid sequence.
        # Since we know it must be bytes, let's just assign.
        ascii_bytes = s

    ascii_unicode = ascii_bytes.decode('ascii', 'ignore')
    return str(ascii_unicode)  # str will return bytes in PY2, unicode in PY3


def remove_html_artifacts(text):
    """Removes common artifacts left over from html parsing."""
    for bad_string in ['rnt', '#39', 'nbsp', '&', ';', 'rnrn']:
        text = text.replace(bad_string, '')
    return text


def remove_html_tags(text, replace=''):
    """Removes html tags from text using regex."""
    regex = '<.*?>'
    clean_text = re.sub(regex, replace, text)
    cleaner_text = remove_html_artifacts(clean_text)
    return cleaner_text


def remove_hyperlinks(text, replace=''):
    """Removes hyperlinks identified by http:// or https:// from text."""
    return re.sub(r'https?://[^\s]+', replace, text)


def flatten_list_of_lists(li):
    """Flattens a list of lists."""
    return [item for sublist in li for item in sublist]


def flatten_dict(d, sep='.', parse_json=False, keep_dict_props=True):
    """
    Flattens sub-dictionaries to a single-level dict.

    Parameters
    ----------
    d : dict
        Source object
    sep : char (optional; Default: '.')
        Separator between parent and child keys.
        By default, uses dot notation.
        If None, will promote child properties to the top directly.
    keep_dict_props : bool (optional; Default: True)
        If False, keys with dictionary values will be eliminated;
        only the dictionary's subkeys will be promoted to the final dict.
    parse_json : bool (optional; Default: False)
        If True, will attempt to flatten JSON objects that are stored as strings.

    Returns
    -------
    Dict, with child dict's keys promoted (with prefix) to the top level.
    """
    out_data = {}

    for key in d:
        set_key = keep_dict_props  # Flag whether to set current key in output.
        if isinstance(d[key], dict):
            # Found a sub-dictionary.
            fd = flatten_dict(d[key], sep=sep, parse_json=parse_json, keep_dict_props=keep_dict_props)
            fd = {'{key}{sep}{subkey}'.format(sep=sep, key=key, subkey=key2): val for key2, val in fd.items()}
            out_data.update(fd)

        elif parse_json and isinstance(d[key], six.string_types):
            # Found a string; try to process it as JSON.
            try:
                nd = json.loads(d[key])
            except:  # noqa: E722
                # TODO: be explicit about what exceptions are real issues.
                set_key = True  # Override, since the prop was not a dict.
            else:
                if isinstance(nd, dict):
                    fd = flatten_dict(nd, sep=sep, parse_json=parse_json, keep_dict_props=keep_dict_props)
                    fd = {'{key}{sep}{subkey}'.format(sep=sep, key=key, subkey=key2): val for key2, val in fd.items()}
                    out_data.update(fd)

        else:
            set_key = True  # Override, since the prop was not a dict.

        if set_key:
            out_data[key] = d[key]

    return out_data


def unflatten_dict(d, sep='.'):
    """Take dict with dot donation, and restructure into dict of dicts."""
    out_data = {}

    for key, val in d.items():
        # First pass: copy over values,
        # do first-level destructuring
        if not isinstance(key, six.string_types) or sep not in key:
            # Copy over unembedded keys
            out_data[key] = val
            continue
        # First-level destructure for embedded keys
        key_base, key_stem = key.split(sep, 1)
        out_data[key_base] = out_data.get(key_base, {})  # initialize dict
        out_data[key_base][key_stem] = val

    for key, val in out_data.items():
        # Second pass: recursive destructuring of dicts
        if isinstance(val, dict):
            out_data[key] = unflatten_dict(val, sep=sep)

    return out_data


def dict_of_lists_to_list_of_dicts(d):
    """Returns a list of dicts

    Parameters
    ----------
    d : dict of lists (assumed to be the same length)
    """
    keys = list(d.keys())
    assert isinstance(d[keys[0]], collections.Iterable)

    num_items = len(d[keys[0]])
    out_list = [{} for li in range(num_items)]
    for li in range(num_items):
        for key in keys:
            out_list[li][key] = d[key][li]
    return out_list


def strip_quotes(val):
    """Strip quotes from a string value."""
    for start_quote, end_quote in (('"', '"'), ("'", "'")):
        if val.startswith(start_quote) and val.endswith(end_quote):
            return strip_quotes(val[1:-1])
    return val

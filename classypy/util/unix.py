"""Helps support operating system usage."""
import os.path as op
import re
import subprocess

from classypy.devops import logger


class UnixError(ValueError):
    """Error generated within the Unix OS."""

    def __init__(self, msg, cmd=None, log_file=None, *args, **kwargs):
        """Store key info.

        Parameters
        ----------
        msg: string
            Error message
        cmd: string, optional (default: None)
            Command run that generated the unix error
        log_file: string, optional (default: None)
            Log file tracking command output
        """
        super(UnixError, self).__init__(msg, *args, **kwargs)
        self.log_file = log_file
        self.cmd = cmd

    def __str__(self):
        """Serialize error to string."""
        msg = super(UnixError, self).__str__()
        if self.log_file is not None and op.exists(self.log_file):
            # Prepend log file
            try:
                with open(self.log_file, 'r') as fp:
                    msg = fp.read() + "\n" + msg
            except:  # noqa: E722
                # TODO: specify errors that could cause an issue.
                pass

        if self.cmd:
            # Prepend command
            msg = self.cmd + "\n" + msg

        return msg

    def __unicode__(self):
        """Serialize error to string."""
        return self.__str__()


def run_unix_command(cmd, log_file=None, raise_errors=True):
    """Run a command, checks the result and logs or raises errors.

    Parameters
    ----------
    cmd: string, optional (default: None)
        Command run that generated the unix error
    log_file: string, optional (default: None)
        Log file tracking command output
    raise_errors boolean, optional (default: True)
        When True, raises unix exit values != 0 as errors

    Returns
    -------
    Unix return code from command (zero => success)

    Side-effects
    ------------
    Logs output to the console

    Raises
    ------
    UnixError: When raise_errors, raises this error when command completes with non-zero return code.
    """
    cmd_list = ['/bin/bash', '-c', cmd]
    if log_file:
        # Add command-line args to capture a log and still show output
        # Allows us to get the cmd exit code.
        cmd_list[-1] = "set -o pipefail && " + cmd_list[-1]
        # Risky trick to get incremental output from python
        cmd_list[-1] = re.sub(r'\s(python)\s', ' \\1 -u ', cmd_list[-1])
        # Tee caputres output to file, and sends to screen as well.
        cmd_list[-1] += " 2>&1 | tee {log_file}".format(log_file=log_file)

    logger.info(cmd_list[-1])

    result = subprocess.call(cmd_list)
    if result != 0 and raise_errors:
        raise UnixError(
            "Command returned non-zero value: {result}".format(result=result),
            cmd=cmd, log_file=log_file)
    return result

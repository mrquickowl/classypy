import argparse

from dateutil.parser import parse

from .dates import datetime_to_utc


def valid_datetime_as_utc(date_str):
    """
    Check whether `date_str` can be coerced to a valid datetime object.
    Datetime objects returned are timezone naive but in UTC.
    If not, raise an error.
    From http://stackoverflow.com/questions/25470844/specify-format-for-input-arguments-argparse-python
    """
    try:
        dt_obj = parse(date_str)
    except (TypeError, ValueError):
        msg = "Not a valid date: '{date_str}'.".format(date_str=date_str)
        raise argparse.ArgumentTypeError(msg)

    return datetime_to_utc(dt_obj)

import pytest

from classypy.util.unix import UnixError, run_unix_command


def test_run_unix_command(temp_file):
    """Validate that we can run unix commands within Python."""
    cmd = "python -c 'raise NotImplementedError'"

    # Validate that an exception is raised
    with pytest.raises(UnixError):
        run_unix_command(cmd)
    with pytest.raises(UnixError):
        run_unix_command(cmd, log_file=temp_file)

    # Validate that no exception is raised
    rc = run_unix_command(cmd, raise_errors=False)
    assert rc != 0


def test_unix_error_serialization(temp_file):
    """Validate that the exception is serialized correctly."""
    cmd = "python -c 'raise NotImplementedError'"

    try:
        run_unix_command(cmd, log_file=temp_file)
    except UnixError as ue:
        assert cmd in str(ue)
        assert 'NotImplementedError' in str(ue)

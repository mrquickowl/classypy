import pytest
import six

from classypy.util.etl import flatten_dict


@pytest.fixture
def some_dict():
    return {
        'shallow_int_key': 1,
        'shallow_str_key': 'a1',
        'shallow_json_array_key': '[1, 2, 3]',
        'shallow_json_dict_key': '{"a": "b"}',
        'deep_key': {
            'key': 'val',
        }
    }


def test_flatten_with_dots(some_dict):
    new_dict = flatten_dict(some_dict)
    for key in some_dict:
        assert key in new_dict

    assert 'deep_key.key' in new_dict
    assert new_dict['deep_key.key'] == some_dict['deep_key']['key']

    assert isinstance(new_dict['shallow_json_array_key'], six.string_types)  # still a string
    assert isinstance(new_dict['shallow_json_dict_key'], six.string_types)  # still a string


def test_flatten_drop_keys(some_dict):
    new_dict = flatten_dict(some_dict, keep_dict_props=False)
    for key in some_dict:
        if 'deep_key' == key:
            assert key not in new_dict
        else:
            assert key in new_dict

    assert 'deep_key.key' in new_dict
    assert new_dict['deep_key.key'] == some_dict['deep_key']['key']

    assert isinstance(new_dict['shallow_json_array_key'], six.string_types)  # still a string
    assert isinstance(new_dict['shallow_json_dict_key'], six.string_types)  # still a string


def test_flatten_parse_json(some_dict):
    new_dict = flatten_dict(some_dict, parse_json=True)
    for key in some_dict:
        assert key in new_dict

    assert isinstance(new_dict['shallow_str_key'], six.string_types)  # didn't convert all strings

    assert 'deep_key.key' in new_dict
    assert new_dict['deep_key.key'] == some_dict['deep_key']['key']

    assert isinstance(new_dict['shallow_json_array_key'], six.string_types)  # still a string
    assert isinstance(new_dict['shallow_json_dict_key'], six.string_types)  # still a string
    assert 'shallow_json_dict_key.a' in new_dict  # but sub-propertis are now in!


def test_flatten_sep(some_dict):
    new_dict = flatten_dict(some_dict, sep='__')
    for key in some_dict:
        assert key in new_dict

    assert 'deep_key__key' in new_dict
    assert new_dict['deep_key__key'] == some_dict['deep_key']['key']

    assert isinstance(new_dict['shallow_json_array_key'], six.string_types)  # still a string
    assert isinstance(new_dict['shallow_json_dict_key'], six.string_types)  # still a string

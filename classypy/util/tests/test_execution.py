from classypy.util.execution import run_in_chunks, run_in_parallel


def array_of_arrays_equal(arr1, arr2):
    assert len(arr1) == len(arr2)
    matches = [v1 == v2 for v1, v2 in zip(arr1, arr2)]
    return all(matches)


def raise_exception(val):
    raise Exception(str(val))


def test_run_in_chunks():
    # Chunk size equal to range size
    assert array_of_arrays_equal([[0, 1, 2]], run_in_chunks(
        lambda x: x, params={'x': list(range(3))}, chunk_size=3))

    # Chunk size is smaller / not perfect divisions
    assert array_of_arrays_equal([[0, 1], [2]], run_in_chunks(
        lambda x: x, params={'x': list(range(3))}, chunk_size=2))

    # Chunk size is bigger
    assert array_of_arrays_equal([[0, 1, 2]], run_in_chunks(
        lambda x: x, params={'x': list(range(3))}, chunk_size=4))


def test_run_in_parallel():
    assert [0, 1, 2] == run_in_parallel(
        lambda x: x, params=[{'x': ii} for ii in list(range(3))])


def test_run_in_chunks_with_errors():
    vals = run_in_chunks(
        raise_exception,
        params={'val': list(range(3))},
        chunk_size=3,
        return_errors=True)
    assert all([isinstance(val, Exception) for val in vals])


def test_run_in_parallel_with_errors():
    vals = run_in_parallel(
        raise_exception,
        params=[{'val': ii} for ii in list(range(3))],
        return_errors=True)
    assert all([isinstance(val, Exception) for val in vals])

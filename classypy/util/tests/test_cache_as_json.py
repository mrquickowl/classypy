import os.path as op

from classypy.util.caching import cache_as_json


def some_array():
    return [1, 2, 3]


def some_dict():
    return {'some': 'dict'}


def test_cache_dict_as_json(temp_file):
    @cache_as_json
    def foo():
        return some_dict()

    output_dict = foo(json_path=temp_file)
    assert output_dict == some_dict()
    assert op.exists(temp_file)

    output_dict = foo(json_path=temp_file)
    assert output_dict == some_dict()
    assert op.exists(temp_file)


def test_cache_array_as_json(temp_file):
    @cache_as_json
    def foo():
        return some_array()

    output_array = foo(json_path=temp_file)
    assert output_array == some_array()
    assert op.exists(temp_file)

    output_array = foo(json_path=temp_file)
    assert output_array == some_array()
    assert op.exists(temp_file)

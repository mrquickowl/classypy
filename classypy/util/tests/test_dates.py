import warnings
from datetime import datetime, timedelta

import pandas as pd
import pytest
import pytz
from tzlocal import get_localzone

from classypy.util.dates import convert_datetime_columns, datetime_to_local, datetime_to_utc


@pytest.mark.skipif(get_localzone() != pytz.timezone('America/Los_Angeles'),
                    reason="This test assumes your local timezone is America/Los_Angeles.")
def test_datetime_to_local_tz_unaware_utc():
    """
    Timezone unaware datetime objects should convert according to get_localzone.
    This test runs if your local timezone is America/Los_Angeles.
    """
    dt_obj = datetime.utcnow()
    assert datetime_to_local(dt_obj) in (dt_obj - timedelta(hours=7), dt_obj - timedelta(hours=8)), (
        "Test default to local timezone")

    dt_obj = datetime.utcnow()
    assert datetime_to_local(dt_obj, convert_to_tz=get_localzone()) in (
        dt_obj - timedelta(hours=7), dt_obj - timedelta(hours=8)), (
        "Test default to local timezone")


@pytest.mark.skipif(get_localzone() != pytz.utc,
                    reason="This test assumes your local timezone is UTC.")
def test_datetime_to_utc_tz_unaware_utc():
    """
    Timezone unaware datetime objects should convert according to get_localzone.
    This test runs if your local timezone is UTC.
    """
    dt_obj = datetime.utcnow()
    assert datetime_to_utc(dt_obj) == dt_obj, "Test default to local timezone"

    dt_obj = datetime.now()
    assert datetime_to_utc(dt_obj) == dt_obj, "Test default to local timezone"


@pytest.mark.skipif(get_localzone() != pytz.timezone('America/Los_Angeles'),
                    reason="This test assumes your local timezone is America/Los_Angeles.")
def test_datetime_to_utc_tz_unaware_pacific():
    """
    Timezone unaware datetime objects should convert according to get_localzone.
    This test runs if your local timezone is America/Los_Angeles.
    """
    dt_obj = datetime.utcnow()
    assert datetime_to_utc(dt_obj) in (dt_obj + timedelta(hours=7), dt_obj + timedelta(hours=8)), (
        "Test default to local timezone")

    dt_obj = datetime.now()
    assert datetime_to_utc(dt_obj) in (dt_obj + timedelta(hours=7), dt_obj + timedelta(hours=8)), (
        "Test default to local timezone")


def test_datetime_to_utc_tz_unaware_timezone_specified():
    """Timezone unaware datetime objects should convert according to local_tz."""
    pdt = pytz.timezone('America/Los_Angeles')

    dt_obj = datetime.utcnow()
    assert datetime_to_utc(dt_obj, local_tz=pytz.utc) == dt_obj
    assert datetime_to_utc(dt_obj, local_tz=pdt) in (dt_obj + timedelta(hours=7), dt_obj + timedelta(hours=8))

    dt_obj = datetime.now()
    assert datetime_to_utc(dt_obj, local_tz=pytz.utc) == dt_obj
    assert datetime_to_utc(dt_obj, local_tz=pdt) in (dt_obj + timedelta(hours=7), dt_obj + timedelta(hours=8))


def test_datetime_to_utc_tz_aware():
    """
    Timezone aware datetime objects should be returned timezone naive and in UTC.
    This test should also ignore the local_tz argument.
    """
    pdt = pytz.timezone('America/Los_Angeles')
    utc = pytz.utc

    non_utc_dt_obj = pdt.localize(datetime(2017, 10, 1, 3, 0, 0, 0))
    dt_obj = datetime(2017, 10, 1, 10, 0, 0, 0)
    assert datetime_to_utc(non_utc_dt_obj) == dt_obj
    assert datetime_to_utc(non_utc_dt_obj, local_tz=utc) == dt_obj
    assert datetime_to_utc(non_utc_dt_obj, local_tz=pdt) == dt_obj

    utc_dt_obj = utc.localize(datetime(2017, 10, 1, 3, 0, 0, 0))
    dt_obj = datetime(2017, 10, 1, 3, 0, 0, 0)
    assert datetime_to_utc(utc_dt_obj) == dt_obj
    assert datetime_to_utc(utc_dt_obj, local_tz=utc) == dt_obj
    assert datetime_to_utc(utc_dt_obj, local_tz=pdt) == dt_obj


def test_convert_datetime_columns_vanilla():

    @convert_datetime_columns(column_names=['dat'])
    def the_func(dat):
        return pd.DataFrame(data=[{'dat': dat}])
    # Success
    assert the_func('2017-01-01')['dat'][0] == pd.Timestamp('2017-01-01', tz='UTC')

    # Fail, with coersion
    assert pd.isnull(the_func('0017-01-01')['dat'][0])


def test_convert_datetime_columns_no_utc():

    @convert_datetime_columns(column_names=['dat'], utc=False)
    def the_func(dat):
        return pd.DataFrame(data=[{'dat': dat}])
    # Success
    assert the_func('2017-01-01')['dat'][0] == pd.Timestamp('2017-01-01')

    # Fail, with coersion
    assert pd.isnull(the_func('0017-01-01')['dat'][0])


def test_convert_datetime_columns_ignore_errors():
    # Fail, with no conversion
    @convert_datetime_columns(column_names=['dat'], errors='ignore')
    def the_func(dat):
        return pd.DataFrame(data=[{'dat': dat}])
    assert the_func('0017-01-01')['dat'][0] == '0017-01-01'


def test_convert_datetime_columns_raise_errors():
    # Fail, with exception
    @convert_datetime_columns(column_names=['dat'], errors='raise')
    def the_func(dat):
        return pd.DataFrame(data=[{'dat': dat}])
    with pytest.raises(pd.errors.OutOfBoundsDatetime):
        the_func('0017-01-01')


def test_convert_datetime_columns_warn_missing_columns():
    # Warns on missing columns
    @convert_datetime_columns(column_names=['dat', 'goo'])
    def the_func_with_warning(dat):
        return pd.DataFrame(data=[{'dat': dat}])
    with pytest.warns(UserWarning):
        assert the_func_with_warning('2017-01-01')['dat'][0] == pd.Timestamp('2017-01-01', tz='UTC')


def test_convert_datetime_columns_nowarn_missing_columns():
    # Does not warn on missing columns
    @convert_datetime_columns(column_names=['dat', 'goo'], warn_on_missing_columns=False)
    def the_func_no_warning(dat):
        return pd.DataFrame(data=[{'dat': dat}])
    with warnings.catch_warnings(record=True) as w:
        assert the_func_no_warning('2017-01-01')['dat'][0] == pd.Timestamp('2017-01-01', tz='UTC')
        assert len(w) == 0

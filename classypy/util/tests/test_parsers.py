import argparse
from datetime import datetime

import pytest
import pytz
from tzlocal import get_localzone

from classypy.util.parsers import valid_datetime_as_utc


@pytest.mark.skipif(get_localzone() != pytz.timezone('America/Los_Angeles'),
                    reason="This test assumes your local timezone is America/Los_Angeles.")
def test_valid_datetime_strings_no_timezone_pacific():
    """
    Strings with no timezone should be parsed correctly as datetimes.
    This test is duplicated as test_valid_datetime_strings_no_timezone_utc
    for machines running in UTC time.
    """
    datetime_str = '2017-10-01'
    dt_obj = datetime(2017, 10, 1, 7, 0, 0, 0)
    assert valid_datetime_as_utc(datetime_str) == dt_obj

    datetime_str = '2017-10-01 08:20:13.945'
    dt_obj = datetime(2017, 10, 1, 15, 20, 13, 945000)
    assert valid_datetime_as_utc(datetime_str) == dt_obj


@pytest.mark.skipif(get_localzone() != pytz.utc,
                    reason="This test assumes your local timezone is UTC.")
def test_valid_datetime_strings_no_timezone_utc():
    """
    Strings with no timezone should be parsed correctly as datetimes.
    This test is duplicated as test_valid_datetime_strings_no_timezone_pacific
    for machines running in America/Los_Angeles time.
    """
    datetime_str = '2017-10-01'
    dt_obj = datetime(2017, 10, 1, 0, 0, 0, 0)
    assert valid_datetime_as_utc(datetime_str) == dt_obj

    datetime_str = '2017-10-01 08:20:13.945'
    dt_obj = datetime(2017, 10, 1, 8, 20, 13, 945000)
    assert valid_datetime_as_utc(datetime_str) == dt_obj


def test_valid_datetime_strings_with_timezone():
    """Strings with timezones should be parsed correctly as datetimes."""
    datetime_str = '2017-10-03T18:37:29Z'
    dt_obj = datetime(2017, 10, 3, 18, 37, 29, 0)
    assert valid_datetime_as_utc(datetime_str) == dt_obj

    datetime_str = '2017-10-03T18:37:29+00:00'
    dt_obj = datetime(2017, 10, 3, 18, 37, 29, 0)
    assert valid_datetime_as_utc(datetime_str) == dt_obj

    datetime_str = '2017-10-03T18:37:29-02:00'
    dt_obj = datetime(2017, 10, 3, 20, 37, 29, 0)
    assert valid_datetime_as_utc(datetime_str) == dt_obj

    datetime_str = '2017-10-03T18:37:29+02:00'
    dt_obj = datetime(2017, 10, 3, 16, 37, 29, 0)
    assert valid_datetime_as_utc(datetime_str) == dt_obj


def test_valid_datetime_invalid_input():
    # Test some invalid inputs
    with pytest.raises(argparse.ArgumentTypeError):
        valid_datetime_as_utc('abc')
    with pytest.raises(argparse.ArgumentTypeError):
        valid_datetime_as_utc(dict())

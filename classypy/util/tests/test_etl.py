import numpy as np
import pandas as pd

from classypy.util.etl import (dict_of_lists_to_list_of_dicts, flatten_dict, int_val, normalize_whitespace,
                               remove_html_tags, remove_hyperlinks, strip_quotes, to_ascii, unflatten_dict)


def test_dict_of_lists_to_list_of_dicts():
    # Try a single list
    abc123 = dict_of_lists_to_list_of_dicts({'abc': [1, 2, 3]})
    assert len(abc123) == 3
    assert all(['abc' in d for d in abc123])
    assert not any(set([1, 2, 3]) - set([d['abc'] for d in abc123]))

    # Try two list of equal length
    abcdef = dict_of_lists_to_list_of_dicts({'abc': [1, 2, 3], 'def': [4, 5, 6]})
    assert len(abcdef) == 3
    assert all(['abc' in d for d in abcdef])
    assert all(['def' in d for d in abcdef])
    assert not any(set([1, 2, 3]) - set([d['abc'] for d in abcdef]))
    assert not any(set([4, 5, 6]) - set([d['def'] for d in abcdef]))


def test_int_val():
    # Non-numeric string => ''
    assert int_val('') == ''
    assert int_val('test') == ''
    assert int_val('/20143') == '', 'Regression for DPLAT-1144'

    # Numeric => numeric
    assert int_val(-1) == -1
    assert int_val(0.6) == 0

    # Numeric strings
    assert int_val("6") == 6
    assert int_val("0.6") == ''

    df = pd.DataFrame(data=[[1.0, np.nan], [2, '1']], columns=['one', 'two'])

    assert 1 == df['one'].map(int_val).iloc[0]
    assert '' == df['two'].map(int_val).iloc[0]
    assert 2 == df['one'].map(int_val).iloc[1]
    assert 1 == df['two'].map(int_val).iloc[1]


def test_flatten_dict():
    dict1 = flatten_dict({'a': {'b': [1]}}, keep_dict_props=False)
    assert len(list(dict1.keys())) == 1
    assert 'a.b' in dict1
    assert dict1['a.b'] == [1]


def test_unflatten_dict():
    dict1 = unflatten_dict({'a.b': [1]})
    assert len(list(dict1.keys())) == 1
    assert 'a' in dict1
    assert len(list(dict1['a'].keys())) == 1
    assert 'b' in dict1['a']
    assert dict1['a']['b'] == [1]

    dict2 = unflatten_dict({'a.b.c': [1]})
    assert dict2['a']['b']['c'] == [1]

    # Test non-string keys
    dict3 = unflatten_dict({1: 2})
    assert dict3[1] == 2


def test_normalize_whitespace_vanilla():
    assert " " == normalize_whitespace("\n"), "Newline to space conversion"
    assert " " == normalize_whitespace("\t"), "Tab to space conversion"
    assert " t u" == normalize_whitespace(" t  u"), "Mess with spaces in text"


def test_normalize_whitespace_double_escaped():
    assert " " == normalize_whitespace("\\n\\t\\r"), "Normalize escaped"
    assert " " == normalize_whitespace("\\\\n\\\\t\\\\r"), "Normalize double-escaping"
    assert "\\n\\t\\r" == normalize_whitespace("\\n\\t\\r", normalize_escaped=False), (
        "normalize_escaped=False works")


def test_remove_html_tags():
    assert '' == remove_html_tags('<br>')
    assert 'header' == remove_html_tags('<h1>header</h1>')
    assert 'body' == remove_html_tags('<br><p>body</p>')
    assert 'some text' == remove_html_tags('some text'), "Don't mess with non-html text"
    assert 'some text' == remove_html_tags('<head><title>some text</title></head>'), 'Remove nested tags'
    assert '' == remove_html_tags('<img src="img_girl.jpg" style="max-width:100%;height:auto;">')


def test_remove_hyperlinks():
    assert '' == remove_hyperlinks('http://classy.org')
    assert '' == remove_hyperlinks('https://classy.org')
    assert 'link: ' == remove_hyperlinks('link: http://classy.org')
    assert 'some text' == remove_hyperlinks('some text')
    assert 'this site  is great' == remove_hyperlinks('this site https://classy.org is great')


def test_to_ascii():
    assert 'ab\n' == to_ascii(u"ab\n"), "Ascii unicode string all remain"
    assert 'ab\n' == to_ascii(b"ab\n"), "Ascii bytes string all remain"
    assert 'ab\n' == to_ascii(u"a\x9fb\n"), "Unicode stripped out"
    assert r'\umax' == to_ascii(r"\umax"), "Deal with 'bad' unicode"


def test_strip_quotes():
    assert strip_quotes("a") == "a"
    assert strip_quotes("'a'") == "a"
    assert strip_quotes("\"a\"") == "a"
    assert strip_quotes("\"'a'\"") == "a"

    # Keep quotes
    assert strip_quotes("\"a") == "\"a"
    assert strip_quotes("a\"") == "a\""

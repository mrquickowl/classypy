import os
import os.path as op
import sys

import pandas as pd
import pytest

from classypy.util.caching import cache_dataframe


def create_data(one_val=1):
    df = pd.DataFrame(data=[{'one': one_val, 'three': 4}, {'one': 3, 'five': 7}])
    return df[['one', 'three', 'five']]


def create_data_ascii():
    return pd.DataFrame(data=[{"one": "\xe0"}])


def test_caching(temp_file):
    @cache_dataframe
    def create_data_no_args():
        return create_data()

    @cache_dataframe
    def create_data_one_arg(one_val=1):
        return create_data(one_val=one_val)

    # Create & validate dataframe
    create_data_no_args(csv_file=temp_file)
    assert op.exists(temp_file)
    df = pd.read_csv(temp_file)
    assert len(df) == 2
    assert df.iloc[0]['one'] == 1

    # Validate that dataframe is cached.
    create_data_one_arg(csv_file=temp_file, one_val=2)
    assert op.exists(temp_file)
    df = pd.read_csv(temp_file)
    assert len(df) == 2
    assert df.iloc[0]['one'] == 1  # didnt' change

    # Validate that dataframe is overwritten when 'force' passed.
    create_data_one_arg(csv_file=temp_file, one_val=2, force=True)
    assert op.exists(temp_file)
    df = pd.read_csv(temp_file)
    assert len(df) == 2
    assert df.iloc[0]['one'] == 2  # changed :D

    # Validate that passing None still executes.
    os.remove(temp_file)
    df = create_data_no_args(csv_file=None)
    assert not op.exists(temp_file)
    assert len(df) == 2
    assert df.iloc[0]['one'] == 1

    # Validate that passing no argument still executes.
    df = create_data_no_args()
    assert not op.exists(temp_file)
    assert len(df) == 2
    assert df.iloc[0]['one'] == 1


def test_args_kwargs(temp_file):
    @cache_dataframe
    def create_data_one_arg(one_val=1, *args, **kwargs):
        assert len(args) == 0
        assert len(kwargs) == 0
        return create_data(one_val=one_val)

    create_data_one_arg(csv_file=temp_file)
    assert op.exists(temp_file)
    df = pd.read_csv(temp_file)
    assert len(df) == 2
    assert df.iloc[0]['one'] == 1

    create_data_one_arg(csv_file=temp_file, force=True, one_val=2)
    assert op.exists(temp_file)
    df = pd.read_csv(temp_file)
    assert len(df) == 2
    assert df.iloc[0]['one'] == 2


def test_args_csv_file(temp_file):
    outer_csv_file = temp_file  # needed due to function aliasing below

    @cache_dataframe
    def create_data_csv_file(csv_file, *args, **kwargs):
        # Will error if csv_file not passed.
        assert len(args) == 0
        assert len(kwargs) == 0
        assert csv_file == outer_csv_file
        return create_data()

    create_data_csv_file(csv_file=temp_file)
    assert op.exists(temp_file)
    df = pd.read_csv(temp_file)
    assert len(df) == 2
    assert df.iloc[0]['one'] == 1


def test_args_force(temp_file):
    @cache_dataframe
    def create_data_force(force, force_correct_val, one_val, *args, **kwargs):
        assert len(args) == 0
        assert len(kwargs) == 0
        assert force == force_correct_val
        return create_data(one_val=one_val)

    create_data_force(csv_file=temp_file, force=True, force_correct_val=True, one_val=2)
    assert op.exists(temp_file)
    df = pd.read_csv(temp_file)
    assert len(df) == 2
    assert df.iloc[0]['one'] == 2

    create_data_force(csv_file=temp_file, force=False, force_correct_val=False, one_val=1)
    assert op.exists(temp_file)
    df = pd.read_csv(temp_file)
    assert len(df) == 2
    assert df.iloc[0]['one'] == 2  # default dataframe value or passed val would go to 1; 2 is cached


def test_decorator_kwargs_noindex(temp_file):
    @cache_dataframe(index=False)
    def create_data_force(*args, **kwargs):
        return create_data(*args, **kwargs)

    create_data_force(csv_file=temp_file, force=True, one_val=2)
    assert op.exists(temp_file)

    df = pd.read_csv(temp_file)
    assert isinstance(df.index, pd.core.indexes.range.RangeIndex)
    assert df.index.min() == 0
    assert df.index.max() == 1


def test_decorator_kwargs_withindex(temp_file):
    @cache_dataframe(index=True)
    def create_data_force(*args, **kwargs):
        return create_data(*args, **kwargs)

    create_data_force(csv_file=temp_file, force=True, one_val=2)
    assert op.exists(temp_file)

    df = pd.read_csv(temp_file, index_col=0)
    isinstance(df.index, pd.core.indexes.numeric.Int64Index)
    assert df.index.min() == 0
    assert df.index.max() == 1

    df = pd.read_csv(temp_file, index_col='one')
    isinstance(df.index, pd.core.indexes.numeric.Int64Index)
    assert df.index.min() == 2
    assert df.index.max() == 3


@pytest.mark.skipif(sys.version_info >= (3, 0), reason="Requires Python2")
def test_decorator_encoding(temp_file):

    # Fail to save file with utf-8 encoding and non-ansii characters.
    @cache_dataframe
    def create_data_default_encoding():
        return create_data_ascii()

    with pytest.raises(UnicodeDecodeError):
        create_data_default_encoding(csv_file=temp_file)
    assert not op.exists(temp_file)

    # Save file with no encoding and non-ansii characters.
    @cache_dataframe(encoding=None)
    def create_data_no_encoding():
        return create_data_ascii()
    create_data_no_encoding(csv_file=temp_file)

    assert op.exists(temp_file)
    df = pd.read_csv(temp_file)
    assert df['one'].iloc[0] == "\xe0"
    assert len(df) == 1

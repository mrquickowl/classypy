import os
import os.path as op

import numpy as np
import pandas as pd
import pytest

from classypy.util.caching import pickle_dataframe


def create_with_arrays(var=1):
    data = [{'one': var, 'two': np.ones(5)} for _ in range(2)]
    return data


def create_with_nans(var=1):
    data = [{'one': var, 'two': None}, {'one': var, 'two': np.nan}]
    return data


def create_mixed_data(var=1):
    data = [{'one': var, 'two': {'x': [1, 2, 3], 'y': str}}, {'one': 7, 'two': set()}]
    return data


@pytest.fixture(params=[create_with_arrays, create_mixed_data, create_with_nans])
def dataframes(request):
    df_default = pd.DataFrame(data=request.param())
    df_var = pd.DataFrame(data=request.param(2))
    return (df_default, df_var)


def test_caching(dataframes, temp_file):
    @pickle_dataframe
    def return_df(default=True):
        if default:
            return dataframes[0]
        else:
            return dataframes[1]

    run_caching_tests(get_df=return_df, temp_file=temp_file)


def test_compression(dataframes, temp_file):
    @pickle_dataframe(compression='gzip')
    def return_df():
        return dataframes[0]

    run_compression_tests(get_df=return_df, temp_file=temp_file)


def run_caching_tests(temp_file, get_df):
    # Create and validate DataFrame
    df_return = get_df(pkl_file=temp_file)
    df_load = pd.read_pickle(temp_file)

    assert op.exists(temp_file)
    assert len(df_return) == len(df_load)
    assert df_return['one'].equals(df_load['one'])
    assert df_load.iloc[0]['one'] == 1

    for obj_return, obj_load in zip(df_return['two'].tolist(), df_load['two'].tolist()):
        assert isinstance(obj_return, type(obj_load))

    # Validate that dataframe is cached.
    get_df(pkl_file=temp_file, default=False)
    assert op.exists(temp_file)
    df_load = pd.read_pickle(temp_file)
    assert df_load.iloc[0]['one'] == 1

    # Validate that dataframe is overwritten when 'force' passed.
    get_df(pkl_file=temp_file, force=True, default=False)
    assert op.exists(temp_file)
    df_load = pd.read_pickle(temp_file)
    assert df_load.iloc[0]['one'] == 2  # changed :D

    # Validate that passing None still executes.
    os.remove(temp_file)
    df = get_df(pkl_file=None)
    assert not op.exists(temp_file)
    assert df.iloc[0]['one'] == 1

    # Validate that passing no argument still executes.
    df = get_df()
    assert not op.exists(temp_file)
    assert df.iloc[0]['one'] == 1


def run_compression_tests(get_df, temp_file):
    get_df(pkl_file=temp_file)
    with pytest.raises(IOError):
        pd.read_pickle(temp_file, compression='bz2')

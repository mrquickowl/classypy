import os
import os.path as op

from classypy.text import text_from_disk
from classypy.util.caching import cache_file


def test_cache_file(temp_file):
    FILE_CONTENT = "file content"

    @cache_file
    def create_data_no_args():
        return FILE_CONTENT

    @cache_file
    def create_data_one_arg(one_val=1):
        return str(one_val)

    # Create & validate text file
    create_data_no_args(file_path=temp_file)
    assert op.exists(temp_file)
    content = text_from_disk(temp_file)
    assert content == FILE_CONTENT

    # Validate that text data is cached.
    create_data_one_arg(file_path=temp_file, one_val="abc")
    assert op.exists(temp_file)
    content = text_from_disk(temp_file)
    assert content == FILE_CONTENT

    # Validate that text data is overwritten when 'force' passed.
    create_data_one_arg(file_path=temp_file, one_val="abc", force=True)
    assert op.exists(temp_file)
    content = text_from_disk(temp_file)
    assert content == "abc"

    # Validate that passing None still executes.
    os.remove(temp_file)
    content = create_data_no_args(file_path=None)
    assert not op.exists(temp_file)
    assert content == FILE_CONTENT

    # Validate that passing no argument still executes.
    content = create_data_no_args()
    assert not op.exists(temp_file)
    assert content == FILE_CONTENT

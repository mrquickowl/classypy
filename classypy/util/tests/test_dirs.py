import os.path as op

from classypy.util.dirs import caller_dir, this_files_dir


def test_this_files_dir():
    assert this_files_dir() == op.dirname(op.abspath(__file__))

    def inner_fn():
        assert this_files_dir() == op.dirname(op.abspath(__file__))
    inner_fn()


def test_caller_dir():
    assert caller_dir(frames_above=0) == op.dirname(op.abspath(__file__))

    def inner_fn():
        assert caller_dir(frames_above=0) == op.dirname(op.abspath(__file__))
    inner_fn()

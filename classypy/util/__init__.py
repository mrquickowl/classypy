from collections import OrderedDict


def sort_dict(d, recursive=False):
    """Sort dictionary alphabetically, by keys

    Parameters
    ----------
    d: dict
    recursive: (optional)
        if True, sort dictionaries at all levels

    Returns
    -------
    OrderedDict, in alphabetical order
    """
    sorted_dict = OrderedDict()

    for key in sorted(d.keys()):
        values = d[key]
        if recursive and isinstance(values, dict):
            values = sort_dict(values)
        sorted_dict[key] = values
    return sorted_dict


def update_dict(value, dic):
    "Returns updated dict"
    dic.update(value)
    return dic

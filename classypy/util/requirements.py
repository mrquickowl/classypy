def has_prop(obj, requirement):
    req = requirement.split('.', 1)
    if obj.get(req[0]) is None:
        return False
    if len(req) == 1:
        return True
    return has_prop(obj[req[0]], req[1])


def has_props(obj, requirements):
    for req in requirements:
        if not has_prop(obj, requirement=req):
            return False
    return True


def meets_requirements(funcs, **kwargs):
    for func in funcs:
        if not func(**kwargs):
            return False
    return True

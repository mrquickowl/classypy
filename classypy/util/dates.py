import warnings

import pandas as pd
import pytz
from tzlocal import get_localzone


def _is_timezone_aware(dt_obj):
    return dt_obj.tzinfo is not None and dt_obj.tzinfo.utcoffset(dt_obj) is not None


def _as_timezone_naive(dt_obj, convert_to_tz=pytz.utc):
    if _is_timezone_aware(dt_obj):
        # Timezone aware datetimes should be converted to the given timezone and then turned naive
        dt_obj = dt_obj.astimezone(convert_to_tz)
        dt_obj = dt_obj.replace(tzinfo=None)

    return dt_obj


def datetime_to_utc(dt_obj, local_tz=None):
    if not _is_timezone_aware(dt_obj):
        # Timezone naive datetime, assume that user meant the time in their current timezone
        local_tz = local_tz or get_localzone()
        dt_obj = local_tz.localize(dt_obj)

    return _as_timezone_naive(dt_obj)


def datetime_to_local(dt_obj, convert_to_tz=None):
    """Convert the given datetime object to some custom ("local") timezone."""
    if not _is_timezone_aware(dt_obj):
        # Timezone naive datetime, assume that user meant utc
        dt_obj = pytz.utc.localize(dt_obj)

    convert_to_tz = convert_to_tz or get_localzone()
    return _as_timezone_naive(dt_obj, convert_to_tz=convert_to_tz)


def convert_datetime_columns(*args, **outer_kwargs):
    """
    Convert (inplace) df columns to pandas datetime64 dtype.

    Parameters
    ----------
    column_names : iterable of strings
        Columns to convert datetimes
    warn_on_missing_columns : bool (optional)
        Whether to warn on named columns missing from returned dataframe (default: True)
    errors : string (optional)
        How to handle conversion errors (Default: coerce to NaT).
        See pandas.to_datetime docs for details.
    kwargs : dict
        Pass-through arguments to pandas.to_datetime

    Returns
    -------
    Function decorator that wraps a function returning a single dataframe.
    """
    column_names = outer_kwargs.pop('column_names')
    warn_on_missing_columns = outer_kwargs.pop('warn_on_missing_columns', True)
    # Handle the 'errors' pandas parameter explicitly (i.e. not via kwargs),
    # so that we can set a different default from Pandas.
    errors = outer_kwargs.pop('errors', "coerce")
    # Default to converting all datetime strings to UTC (this was the default in Pandas before 0.24.0)
    # https://pandas.pydata.org/pandas-docs/stable/whatsnew/v0.24.0.html#parsing-datetime-strings-with-timezone-offsets
    utc = outer_kwargs.pop('utc', True)

    def convert_datetime_columns_decorator(func):
        def convert_datetime_columns_inner_func(*args, **kwargs):
            df = func(*args, **kwargs)

            for col in column_names:
                if col in df.columns:
                    df[col] = pd.to_datetime(df[col], errors=errors, utc=utc, **outer_kwargs)
                elif warn_on_missing_columns:
                    warnings.warn("Missing datetime column: {col}".format(col=col))

            return df
        return convert_datetime_columns_inner_func

    return convert_datetime_columns_decorator

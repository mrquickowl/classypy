import os.path as op

import plotly as py


def save_plot(fig, output_path, save_remote=False, sharing="public", auto_open=True):
    """
    Saves a plot either locally, or to a plotly account. Depends on `py.tools.set_credentials_file`
    being called first.

    Parameters
    ----------
    fig : The plotly object to be saved
    output_path : Where the file should be saved if saved locally
    save_remote : Whether the figure should be saved to plotly's website
    sharing : If the setting on plotly's website for the plot should be a public or private
    auto_open : If saving locally, whether the HTML file should open in a browser automatically

    Returns
    ----------
    If saved locally, an HTML file with the plot
    If saved remotely, a saved plot on plotly's website
    """
    if save_remote:
        py.plotly.iplot(fig, filename=op.basename(output_path.replace(".html", "")), sharing=sharing)
    else:
        py.offline.plot(fig, filename=output_path, auto_open=auto_open)

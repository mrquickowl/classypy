"""
Functions to map location data

TODO: "to respect the privacy of the organizations that we work with, locations displayed are approximated, not exact."
    - size
    - color_sets
    - have to jiggle wheel scroll on figures to make them work

DOC TODO:
    - Finish documentation for parameters with 'TODO' in docstring.
"""
import collections

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
from bokeh.io import output_file, save
from bokeh.models import Circle, DataRange1d, GMapOptions, GMapPlot, HoverTool, Line, PanTool, ResetTool, WheelZoomTool
from bokeh.plotting import ColumnDataSource

from classypy.devpos import logger
from classypy.util.etl import to_ascii


def lookup_country(lat, lng):
    import reverse_geocode  # inline b/c import is slow
    metadata = reverse_geocode.search(((lat, lng), ))
    return metadata[0]['country']


def map_all_programs(df, api_key, width=1000, height=500, title=None,
                     custom_color_field=None, out_file=None,
                     loc_types=None):
    """
    Generates a map of program locations around the world. this function requires either csv_file or account df.
    Using a prebuilt account_df (using build_account_df) is faster for multiple maps.

    Parameters
    ----------
    df : dataframe
        Locations generated using get_locations.
    color_set : list
        Features to assign colors to. Should be same length as account_df[1].
    size_set : list
        Features to assign sizes of circles to. Should be same length as account_df[1].
    width : int
        Width of plot.
    height : int
        Height of plot.
    title : str
        Title of plot.
    out_file : str
        Filename for saving the HTML document.
    loc_types : str
        Location type.

    Returns
    --------
        bokeh.plotting.figure: A cool bokeh figure.
    """
    # Flesh out all maps.
    loc_types = loc_types or df['location_type'].unique()
    if len(loc_types) > 1:
        # Do individual maps, AND this map.
        for loc_type in loc_types:
            map_all_programs(
                df=df[df['location_type'] == loc_type],
                title=loc_type,
                api_key=api_key, width=width, height=height)
    title = title or '_'.join(loc_types)
    out_file = "%s_map.html" % title

    # Organizing Data
    df = df.sort_values(by=['score'])

    sizes = [5 + (0 if np.isnan(s) else np.exp(s) / 200)
             for s in df['score']]

    if custom_color_field:
        uniques = list(df[custom_color_field].unique())
        raw_colors = list(plt.cm.Accent(np.linspace(0, 1, len(uniques))))
        fixed_colors = ['#' + ''.join('%02x' % (i * 255) for i in c[:3]) for c in raw_colors]
        colors = [fixed_colors[uniques.index(i)] for i in df[custom_color_field]]
        color_type = df[custom_color_field]
        logger.info(color_type[0])
    else:
        colors = ['#0000ff' if lt == 'program' else '#00ff00'
                  for lt in df['location_type']]
        color_type = df['location_type']

    source_dict = dict(
        lng=df['lng'],
        lat=df['lat'],
        x=df['lng'],  # (-df['lng'] - 54),
        y=df['lat'],
        acc_name=df['account name'],
        prog_name=df['name'],
        address=[to_ascii(address) for address in df['address'].tolist()],
        country=df['country'],
        category=df['category'],
        color=colors,
        size=sizes,
        color_type=color_type,
        score=df['score'])

    # Centered at San Diego
    map_options = GMapOptions(lat=32.7, lng=-57.2, map_type="roadmap", zoom=2)
    source = ColumnDataSource(data=source_dict)
    plot = GMapPlot(
        x_range=DataRange1d(), y_range=DataRange1d(),
        map_options=map_options,
        plot_width=width, plot_height=height,
        api_key=api_key)

    circle = Circle(x="x", y="y", size="size", fill_color="color",
                    fill_alpha=0.8)
    plot.add_glyph(source, circle)
    if title:
        plot.title.text = title
    else:
        plot.title.text = "World Map of %s" % ', '.join(loc_types)
    hover = HoverTool()
    hover.tooltips = collections.OrderedDict([
        ("Org", "@acc_name"),
        ("Program", "@prog_name"),
        ("Address", "@address"),
        ("Cause Category", "@category"),
        ("Color", "@color_type")
    ])
    plot.add_tools(hover, PanTool(), WheelZoomTool(), ResetTool())

    output_file(out_file)
    save(plot)


def map_program_links(df, api_key, width=1000, height=500, title=None, pl=None):
    """
    Generates a map of links between program HQs and locations around the world.

    Parameters
    ----------
    df : dataframe
        Locations generated using get_locations.
    color_set : list
        Features to assign colors to. Should be same length as account_df[1].
    size_set : list
        Features to assign sizes of circles to. Should be same length as account_df[1].
    width : int
        Width of plot.
    height : int
        Height of plot.
    title : str
        Title of plot.
    pl :
        TODO

    Returns
    --------
        bokeh.plotting.figure: A cool bokeh figure.
    """
    # Flesh out all maps.
    program_ids = df['program_id'].unique()
    program_ids = [pid for pid in program_ids if not np.isnan(pid)]
    if len(program_ids) > 1:
        for program_id in program_ids:
            program_rows = df[df['program_id'] == program_id]
            if len(program_rows) == 0:
                continue
            account_id = program_rows.iloc[0]['account_id']
            hq_rows = df[np.logical_and(
                df['account_id'] == account_id,
                df['location_type'] == 'hq')]
            prog_rows = df[df['program_id'] == program_id]

            pl = map_program_links(df=pd.concat([hq_rows, prog_rows], sort=True), api_key=api_key, pl=pl)
        return

    # 1 program
    hq_rows = df[df['location_type'] == 'hq']
    prog_rows = df[df['location_type'] == 'program']

    # Organizing Data
    def get_source_dict(df):
        df = df.sort_values(by=['score'])
        colors = ['#0000ff' if lt == 'program' else '#00ff00'
                  for lt in df['location_type']]
        sizes = [5 + (0 if np.isnan(s) else np.exp(s) / 200)
                 for s in df['score']]
        return dict(
            lng=df['lng'],
            lat=df['lat'],
            x=df['lng'],  # (-df['lng'] - 54),
            y=df['lat'],
            acc_name=df['account name'],
            prog_name=df['name'],
            address=[to_ascii(address) for address in df['address'].tolist()],
            country=df['country'],
            category=df['category'],
            color=colors,
            size=sizes,
            location_type=df['location_type'],
            score=df['score'])

    # Show all locations

    # Centered at San Diego
    map_options = GMapOptions(lat=32.7, lng=-57.2, map_type="roadmap", zoom=2)
    source = ColumnDataSource(data=get_source_dict(df))
    if pl is None:
        pl = GMapPlot(
            x_range=DataRange1d(), y_range=DataRange1d(),
            map_options=map_options,
            plot_width=width, plot_height=height,
            api_key=api_key)
        hover = HoverTool()
        hover.tooltips = collections.OrderedDict([
            ("Org", "@acc_name"),
            ("Program", "@prog_name"),
            ("Address", "@address"),
            ("Cause Category", "@category"),
        ])
        pl.add_tools(hover, PanTool(), WheelZoomTool(), ResetTool())

    # draw markers
    circle = Circle(x="x", y="y", size="size", fill_color="color",
                    fill_alpha=0.5)
    pl.add_glyph(source, circle)

    # draw lines between markers
    if len(prog_rows) > 0 and len(hq_rows) > 0:
        for pi in range(len(prog_rows)):
            src = dict(
                x=[hq_rows.iloc[0]['lng'], prog_rows.iloc[pi]['lng']],
                y=[hq_rows.iloc[0]['lat'], prog_rows.iloc[pi]['lat']], )
            line = Line(x='x', y='y')
            pl.add_glyph(ColumnDataSource(src), line)

    output_file("%s_map.html" % (title or 'all_links'))
    save(pl)

    return pl

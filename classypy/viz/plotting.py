"""
DOC TODO:
- Add doc for parameters with 'TODO'
"""
import os
import os.path as op
import re

import matplotlib
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import plotly
import plotly.graph_objs as go
import plotly.plotly as py
from bokeh.models import ColumnDataSource, HoverTool, LabelSet, Legend
from bokeh.plotting import figure, output_file, show
from mpl_toolkits.mplot3d import Axes3D
from slugify import slugify

from classypy.devops import logger
from classypy.viz import classy_colors


def save_plot(func):
    """Decorator to save a returned plot from a function.

    Input must have the method .save() or .savefig().

    Parameters
    ----------
    plot_path : str
        Filename to save plot to. None will prevent saving.
    verbose : int (Default: 1)
        Level of verbosity

    Returns
    -------
    Plot from the wrapped function.

    Side Effects
    ------------
    Saves the plot to disk, if plot_path is specified.
    """
    def save_plot_decorator(*args, **kwargs):
        plot_path = kwargs.pop('plot_path', None)

        plot = func(*args, **kwargs)
        if isinstance(plot, matplotlib.axes.Axes):
            plot, ax = plt.subplots(1, 1)
            kwargs['ax'] = ax
            func(*args, **kwargs)

        if not (hasattr(plot, 'save') or hasattr(plot, 'savefig')):
            raise ValueError('Function should return an object with the save() or savefig() method.')

        if not plot_path:
            logger.info('Not saving plot: {p}'.format(p=str(plot)))
        else:
            dir_ = op.dirname(plot_path)

            if not op.exists(dir_):
                os.makedirs(dir_)

            try:
                plot.save(plot_path)
            except AttributeError:
                plot.savefig(plot_path)

            logger.info('Saved plot to: {path}'.format(path=plot_path))

        return plot

    return save_plot_decorator


def to_slug(name):
    """
    Slugify the name of the field on excel. Bokeh only wants alpha numeric
    variable names for the hover tool

    Parameters
    ----------
    name : str
        Name of the field.

    Returns
    --------
        str: slug
    """
    return slugify(name, separator='_')


def build_data_object(df):
    """
    Converts the header names to an object for the ColumnDataSource and
    hover tool

    Parameters
    ----------
    df : dataframe
        dataframe of the CSV

    Returns
    --------
        dict: Object that maps the slugified field to the columns
    """
    headers = list(df.columns.values)
    data = dict()
    for key in headers:
        data[to_slug(key)] = df[key]

    return data


def _build_hover_tooltip(df):
    """
    Converts the header names to an array that has the mapping for the hover
    tool

    Parameters
    ----------
    df : dataframe
        Dataframe of the csv.

    Returns
    --------
        array: Array for the tooltip.
    """
    headers = list(df.columns.values)
    tooltips = []
    for key in headers:
        tooltips.append((key, '@' + to_slug(key)))

    return tooltips


def build_bokeh_figure(df, title='Graph'):
    """
    Returns the figure object with the default attributes

    Parameters
    ----------
    df : dataframe
        Dataframe of the csv.
    title : str (optional)
        Title of the graph.

    Returns
    --------
        figure: Bokeh figure
    """
    hover = HoverTool(
        names=['scatter'],
        tooltips=_build_hover_tooltip(df)
    )

    return figure(title=title, width=1100, height=650,
                  tools=[hover, 'save', 'pan', 'wheel_zoom', 'box_zoom',
                         'reset', 'box_select'])


def plot_sne_matplotlib(Y, Y_cats, cat_labels, programs_of_interest=tuple(),
                        names=None,
                        output_path="2Dtsne_matplotlib.svg", **kwargs):
    """
    Plots the results of stochastic neighbors embedding. Programs of interest
    and their names can be entered as well.

    Parameters
    ----------
    Y : list
        Output list from t-sne
    Y_cats :
        TODO
    cat_labels :
        TODO
    programs_of_interest : tuple
        TODO
    names : list (optional)
        List of string names to be used for axis annotations.
    output_path: str
        Filename to save figure to. Defaults to "2Dtsne_matplotlib.svg".

    Returns
    --------
        figure: Bokeh figure
    """
    fig = plt.figure(figsize=(30, 20))
    colors = plt.cm.jet(np.linspace(0, 1, len(cat_labels)))
    ax1 = fig.add_subplot(1, 1, 1)
    totals = np.zeros((len(cat_labels),))

    for i in range(len(Y[:, 0])):
        ax1.scatter(
            Y[i, 0],
            Y[i, 1],
            s=40,
            color=colors[int(Y_cats[i])],
            label=cat_labels[int(Y_cats[i])] if totals[int(Y_cats[i])] == 0 else ""
        )
        totals[int(Y_cats[i])] = 1

        if i in programs_of_interest:
            if names is not None:
                ax1.annotate(names[i].decode('utf-8'), (Y[i, 0], Y[i, 1]),
                             fontsize=14)

    ax1.legend(loc=4, prop={'size': 20})
    ax1.patch.set_visible(False)
    ax1.axis('off')

    if output_path is not None:
        fig.tight_layout()
        fig.savefig(output_path)


def plot_3D_sne_matplotlib(Y, Y_cats, cat_labels,
                           output_path='3Dtsne_matplotlib.svg', **kwargs):
    """
    Plots the results of stochastic neighbors embedding. Programs of interest
    and their names can be entered as well.

    Parameters
    ----------
    Y : list
        Output list from t-sne
    Y_cats :
        TODO
    cat_labels :
        TODO
    output_path: str
        Filename to save figure to. Defaults to "2Dtsne_matplotlib.svg".

    Returns
    --------
        figure: Bokeh figure
    """
    fig = plt.figure()
    ax = Axes3D(fig)
    colors = plt.cm.jet(np.linspace(0, 1, len(cat_labels)))
    for i in range(len(cat_labels)):
        label = cat_labels[i]
        subset = Y[np.where(Y_cats == i)]
        ax.scatter(subset[:, 0], subset[:, 1], subset[:, 2], c=colors[i], label=label)

    ax.legend(loc='lower right', prop={'size': 6})

    if output_path is not None:
        fig.savefig(op.join(output_path, output_path))

    plt.show()


def _plotly_hover_text(hover_subset, cat_label):
    """
    Defines hover_text properties for plotly plot.

    Parameters
    ----------
    hover_subset : dict
        TODO
    cat_label :
        TODO

    Returns
    -------
        hover_text : html enclosed text.
    """
    hover_text = [' '.join(row) for row in hover_subset.values]
    # Word wrap after 32 chars
    hover_text = [re.sub(r"(.{32}[^\s]*)", "\\1<br />", txt, 0, flags=re.DOTALL)
                  for txt in hover_text]
    # Add title
    hover_text = ["<b>%s</b><br />%s" % (cat_label, txt) for txt in hover_text]
    return hover_text


def plot_3D_sne_plotly(Y, Y_cats, cat_labels, username, api_key,
                       title='', hover_data=None,
                       output_path="3Dtsne_plotly.html", **kwargs):
    """
    Plots the results of stochastic neighbors embedding. Programs of interest
    and their names can be entered as well.

    Parameters
    ----------
    Y : list
        Output list from t-sne
    Y_cats :
        TODO
    cat_labels :
        TODO
    username : str
        Username associated with credstash.
    api_key : str
        plotly api key.
    title : str
        Title for plot. Defaults to ''.
    hover_data : dict
        Dictionary of arrays (length same as data above) of hover annotations.
    output_path: str
        Filename to save figure to. Defaults to "2Dtsne_matplotlib.svg".

    Returns
    --------
        figure: Bokeh figure
    """
    plotly.tools.set_credentials_file(username, api_key=api_key)

    if hover_data is None:
        hover_df = pd.DataFrame()
    else:
        hover_df = pd.DataFrame(hover_data)

    plot_data = []
    colors = plt.cm.jet(np.linspace(0, 1, len(cat_labels)))
    for i in range(len(np.unique(Y_cats))):
        subset_indices = np.where(Y_cats == i)
        data_subset = Y[subset_indices]
        hover_subset = hover_df.iloc[subset_indices]
        plot_data.append(go.Scatter3d(
            x=data_subset[:, 0],
            y=data_subset[:, 1],
            z=data_subset[:, 2],
            mode='markers',
            marker={
                "size": 3,
                "line": {"width": 0},
                "color": str(colors[i]),
                "opacity": 0.8},
            name=cat_labels[i],
            text=_plotly_hover_text(hover_subset, cat_label=cat_labels[i]),
            hoverinfo='text'))

    layout = go.Layout(title=title, margin={'l': 0, 'r': 0, 'b': 0, 't': 0})
    fig = go.Figure(data=plot_data, layout=layout)
    if output_path is not None:
        plotly.offline.plot(fig, filename=output_path)
    else:
        py.iplot(fig, filename=op.basename(output_path).split('.')[0])


def filterDataByCategory(category, data_dict):
    """
    Filters data from data_dict for a given category.

    Parameters
    ----------
    category : str
        Category to filter on.
    data_dict : dict
        Dictionary with data 'categories' as key.

    Returns
    -------
        dict : Dictionary filtered by the desired category.
    """
    if category is None:
        indices = np.ones((len(data_dict['categories']),))
    else:
        indices = np.asarray(data_dict['categories']) == category

    # Filter & create new data dict
    source_dict = dict()
    for key in data_dict:
        source_dict[key] = np.asarray(data_dict[key])[indices]
    return source_dict


def plot_sne_bokeh(Y, Y_cats, cat_labels, names=None,
                   title="2D plot of how similar programs are, \
                    based on their text data.",
                   hover_data=None, sizes=None, colors=None,
                   indices_of_interest=tuple(), width=1100, height=650,
                   output_path="2Dtsne_bokeh.html", **kwargs):
    """
    Plots the results of stochastic neighbors embedding.

    Parameters
    ----------
    Y : list
        Output list from t-sne.
    Y_cats :
        TODO
    cat_labels : str
        Text cat_labels of lda categories
    cats :
        The argmax for lda topics of each program (ouput of categories)
    names : list
        List of string program names.
    title : str
        Plot title.
    hover_data : dict
        Dictionary of arrays (length same as data above) of hover annotations.
    sizes : list
        List (length of same as above) of marker sizes (default: 15).
    colors : list
        List (length of same as above) of html colors.
    output_path : str
        Path to output file to create.

    Returns
    -------
        dict : Dictionary filtered by the desired category.
    """
    # Clean the hover data keys
    hover_data = hover_data or dict()
    hover_data = dict(zip(
        [k.replace(' ', '_') for k in hover_data.keys()],
        hover_data.values()))

    # Set up the colors
    raw_colors = plt.cm.jet(np.linspace(0, 1, len(cat_labels)))
    colors = colors or ['#' + ''.join('%02x' % (i * 255) for i in c[:3])
                        for c in raw_colors]
    pl = figure(title=title, width=width, height=height,
                tools="hover,save,pan,wheel_zoom,box_zoom,reset,box_select")

    # Construct the data source
    root_source = dict(
        x=Y[:, 0],
        y=Y[:, 1],
        names=names or [''] * Y.shape[0],
        categories=[cat_labels[int(ci)] for ci in Y_cats],
        colors=[colors[int(ci)] for ci in Y_cats],
        sizes=sizes or [15] * Y.shape[0],  # default size: 1
    )
    root_source['cat_labels'] = [name if pi in indices_of_interest else ''
                                 for pi, name in enumerate(root_source['names'])]
    root_source.update(hover_data or dict())

    # Loop over categories, to build a good legend.
    markers = []
    label_sets = []
    for category in cat_labels:
        source_dict = filterDataByCategory(category=category, data_dict=root_source)
        source = ColumnDataSource(data=source_dict)
        markers.append(pl.circle('x', 'y',
                                 source=source,
                                 color='colors',
                                 size='sizes'))

        if np.any([lbl != '' for lbl in source_dict['cat_labels']]):
            label_sets.append(LabelSet(
                x='x', y='y', text='cat_labels', level='glyph', x_offset=5,
                y_offset=5, source=source, render_mode='canvas'))

        glyph = markers[-1].glyph
        glyph.fill_alpha = 0.75
        glyph.line_width = 1

    # Add the cat_labels & legend separately, to be on top / outside the plot.
    legend = Legend(
        legends=[(lbl, [markers[ri]]) for ri, lbl in enumerate(cat_labels)],
        location=(30, -30))
    pl.add_layout(legend, 'right')
    for cat_labels in label_sets:
        pl.add_layout(cat_labels)

    # Set up the hover interaction
    hover_tuples = list(zip(hover_data.keys(), ['@' + k for k in hover_data.keys()]))
    pl.select_one(HoverTool).tooltips = [
        ('name', '@names'),
        ('category', '@categories'), ] + hover_tuples

    if output_path is not None:
        # Save the result.
        output_file(output_path, title=title)

    show(pl)


def classy_ax(ax, font_size=12):
    """Applies Classy colors and plot formats to ax object.
    Works with Matplotlib and Seaborn ("Axes-level" functions, including regplot, boxplot, and kdeplot).

    Parameters
    ----------
    ax : Axes object
        Single matplotlib.axes.Axes object to be modified
    font_size : int
        Font size for figure ticks and labels

    Returns
    -------
    ax : Axes object
        modified Ax object

    Side Effects
    ------------
    Alters original axis object
    """
    LIGHT_GREY = classy_colors()['light_grey']
    # Color of spines
    ax.spines['bottom'].set_color(LIGHT_GREY)
    ax.spines['left'].set_color(LIGHT_GREY)
    # Hide the right and top spines
    ax.spines['right'].set_visible(False)
    ax.spines['top'].set_visible(False)
    # Color of ticks
    ax.tick_params(axis='all', colors=LIGHT_GREY)
    # Set font size and color for labels
    for item in ([ax.title, ax.xaxis.label, ax.yaxis.label] +
                 ax.get_xticklabels() + ax.get_yticklabels()):
        item.set_fontsize(font_size)
        item.set_color(LIGHT_GREY)
    return ax

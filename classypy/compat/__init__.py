"""
Compatibility layer for Python 3/Python 2 single codebase
"""
import hashlib
import inspect
import sys

import six

numeric_types = tuple(list(six.integer_types) + [float])


def is_python_version(major_version, minor_version=None):
    if sys.version_info[0] != major_version:
        return False
    return minor_version is None or sys.version_info[1] == minor_version


def at_least_python_version(major_version, minor_version=None):
    if sys.version_info[0] < major_version:
        return False
    return minor_version is None or sys.version_info[1] >= minor_version


if is_python_version(2):
    import types
    import urllib

    import cPickle
    import StringIO
    import urllib2
    import urlparse

    cPickle = cPickle
    StringIO = BytesIO = StringIO.StringIO

    class _module_lookup(object):  # noqa
        modules = [urlparse, urllib2, urllib]

        def __getattr__(self, name):
            for module in self.modules:
                if hasattr(module, name):
                    attr = getattr(module, name)
                    if not isinstance(attr, types.ModuleType):
                        return attr
            raise NotImplementedError('This function has not be imported properly')

    module_lookup = _module_lookup()

    class _urllib():  # noqa
        request = module_lookup
        error = module_lookup
        parse = module_lookup

    def md5_hash(string):
        m = hashlib.md5()
        m.update(u(string).encode('utf-8'))
        return m.hexdigest()

    def u(string):
        return string if isinstance(string, unicode) else six.u(string)  # noqa

    def b(string):
        return str(string)

    def func_has_param(func, param_name):
        func_param_names, _, _, _ = inspect.getargspec(func)
        return param_name in func_param_names

else:
    import io
    import pickle
    import urllib

    cPickle = pickle
    StringIO = io.StringIO
    BytesIO = io.BytesIO
    _urllib = urllib

    def md5_hash(string):
        m = hashlib.md5()
        m.update(string.encode('utf-8'))
        return m.hexdigest()

    def u(string):  # unicode
        return string.decode(encoding='utf-8') if isinstance(string, bytes) else str(string)

    def b(string):  # bytes
        return six.b(string)

    def func_has_param(func, param_name):
        return param_name in inspect.signature(func).parameters

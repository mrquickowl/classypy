
"""
Functions for performing spike detection on time series data.
"""
import warnings

import numpy as np
import pandas as pd

from classypy.ml.linalg import rolling_window


def median_absolute_deviation(array, axis=0):
    """
    Calculates the median absolute deviation (MAD) for an array.

    (Adapted from statsmodels.robust.scale.mad)

    Parameters
    ----------
    array : ndarray-like
    axis : int
        The axis along which median absolute deviation is calculated.

    Returns
    -------
    mad : float
        The median absolute deviation.
    """
    array = np.asarray(array)
    medians = np.apply_over_axes(np.median, array, axis)
    mad = np.median(np.fabs(array - medians), axis=axis)
    return mad


def rolling_median(array, window_size):
    """
    Given a DataFrame and a window, returns rolling median.

    Parameters
    ----------
    array : ndarray-like
    window_size : int
        The size of the moving window.

    Returns
    -------
    median_roll : ndarray-like
        The rolling median.
    """
    array = np.asarray(array)
    windowed_arr = rolling_window(array, window_size)
    median_roll = np.median(windowed_arr, axis=1)
    nans = np.array(np.nan).repeat(window_size - 1)
    return np.concatenate((nans, median_roll), axis=0)


def rolling_mad(array, window_size):
    """
    Given a DataFrame and a window, returns the rolling median absolute deviation.

    Parameters
    ----------
    array : ndarray-like
    window_size : int
        The size of the moving window.

    Returns
    -------
    mad_roll : ndarray-like
        The rolling MAD.
    """
    array = np.asarray(array)
    windowed_arr = rolling_window(array, window_size)
    mad_roll = median_absolute_deviation(windowed_arr, axis=1)
    nans = np.array(np.nan).repeat(window_size - 1)
    return np.concatenate((nans, mad_roll), axis=0)


def detect_spikes_df(time_series, window_size, gain):
    """
    Performs positive spike detection in time series data using the median absolute deviation method.

    Parameters
    ----------
    time_series : ndarray-like
    window_size : int
        The size of the moving window.
    gain : float
        A multiplicative constant.

    Returns
    -------
    spikes_df : pandas.DataFrame
        A dataframe with five columns:
            ts - the original time series
            median - the rolling median
            mad - the rolling MAD
            median_plus_mad - the rolling median with the gain-adjusted MAD added
            is_spike - boolean indicating if the observation is a spike
    """
    if not (isinstance(time_series, pd.Series) or isinstance(time_series, np.ndarray)):
        raise ValueError('time_series must be ndarray-like')

    time_series = np.asarray(time_series)
    if window_size > time_series.shape[0]:
        raise ValueError('Window size must be less than or equal to the time series length.')

    median = rolling_median(time_series, window_size)
    mad = rolling_mad(time_series, window_size)
    median_plus_mad = median + gain * mad

    with warnings.catch_warnings():
        # First few values in the timeseries are nan; catch the warning.
        warnings.simplefilter("ignore", RuntimeWarning)
        is_spike = time_series > median_plus_mad

    col_order = ['ts', 'median', 'mad', 'median_plus_mad', 'is_spike']

    spikes_df = pd.DataFrame({
        'ts': time_series,
        'median': median,
        'mad': mad,
        'median_plus_mad': median_plus_mad,
        'is_spike': is_spike
    }, columns=col_order)

    return spikes_df


def detect_spikes(time_series, window_size, gain):
    """
    Performs positive spike detection in time series data using the median absolute deviation method.

    Parameters
    ----------
    time_series : ndarray-like
    window_size : int
        The size of the moving window.
    gain : int
        A multiplicative constant.

    Returns
    -------
    is_spike : ndarray-like of bool
        A boolean vector indicating if an observation is a spike.
    """
    spikes_df = detect_spikes_df(time_series, window_size, gain)
    is_spike = spikes_df['is_spike'].values
    return is_spike

"""
Topic learning and dimensionality reduction techniques.

All t-sne code taken from https://lvdmaaten.github.io/tsne/
"""
import lda
import numpy as np
from sklearn.feature_extraction.text import CountVectorizer

from classypy.devops import logger


def build_word_occurrence_matrix(docs, tfidf=False, norm_by_word_count=False,
                                 lemmatize=False, min_df=1, min_word_length=3,
                                 vectorizer=None):
    """
    Simplifies LDA code and makes recommendations quicker
    CAUTION: vocab order in _build mat is different than vocab order in lda_categories.
    """
    if lemmatize:
        raise NotImplementedError("Must reintroduce the stemmer before lemmatization.")
        from nltk.stem import WordNetLemmatizer
        wordnet_lemmatizer = WordNetLemmatizer()
        stemmed_docs = []
        for doc in docs:
            words = [w.strip() for w in doc.split(' ') if w.strip()]
            new_words = [wordnet_lemmatizer.lemmatize(w) for w in words]
            stemmed_docs.append(' '.join(new_words))
        docs = stemmed_docs
        del words, new_words, stemmed_docs

    # Vectorize before stemming
    if vectorizer:
        X = vectorizer.transform(docs)
    else:
        vectorizer = CountVectorizer(
            min_df=min_df, token_pattern=u'(?u)\\b[a-zA-Z]{%d,}\\b' % min_word_length,
            max_df=np.inf, analyzer='word', stop_words="english",
            encoding='utf-8', decode_error='ignore')
        vectorizer.fit(docs)
        X = vectorizer.transform(docs)
    # Convert dict_keys to list before turning into numpy array
    vocab = np.asarray(list(vectorizer.vocabulary_.keys()))
    vocab = vocab[np.argsort(list(vectorizer.vocabulary_.values()))]  # ordered by counts (rev)

    lda_mat = X.toarray()
    del X

    if tfidf:
        # ntimes word shows up in program / total occurances.
        float_sums = [float(i) for i in np.sum(lda_mat, axis=0)]  # floats needed for / in next step
        # TODO: divide by document lengths
        # *100 beacuse LDA doesn't like floats
        lda_mat = (lda_mat / float_sums) * 100

    # normalizing by number of words per program.
    # Programs with more words get unfair preference
    # TODO: think about whether I should do this before or after tfidf.
    # Different consequences
    if norm_by_word_count:
        word_counts = [len(doc.split()) for doc in docs]  # length includes stop_words
        lda_mat = np.asarray([i / float(j) if j != 0 else i for i, j in zip(lda_mat, word_counts)])

    return vectorizer, lda_mat, vocab


def lda_categories(docs, n_topics=10, n_top_words=10, n_iter=1500,
                   lemmatize=False, min_df=1, min_word_length=3,
                   tfidf=False, norm_by_word_count=False,
                   model=None, random_state=1, return_model=False):
    """
    Uses LDA to algorithmically find topics in a bag of words.

    Parameters
    ----------
    docs : An array of documents.
    This is generated by text_functions.read_from_CSV
    n_topics: the number of topics for LDA to find
    n_top_words: when creating topic labels, how many words to keep?

    Returns
    ----------

    lda_labels : the word that most represents each category

    lda_output_mat : program x topic weight matrix

    lda_cats : the argmax for lda topics of each program
    """
    # need to find all words that were used to build a program x word
    #  count matrix for LDA
    if len(docs) < n_topics and model is None:
        raise ValueError("Must have more docs than topics! ({n_docs} < {n_topics})".format(
            n_docs=len(docs), n_topics=n_topics))

    vectorizer, lda_mat, vocab = build_word_occurrence_matrix(
        docs=docs, tfidf=tfidf, norm_by_word_count=norm_by_word_count,
        lemmatize=lemmatize, min_df=min_df, min_word_length=min_word_length,
        vectorizer=model.vectorizer if model is not None else None)
    if model is None:
        logger.info("Running LDA for {n_topics} topics for {n_iter} iterations.".format(
            n_topics=n_topics, n_iter=n_iter))
        model = lda.LDA(n_topics=n_topics, n_iter=n_iter, random_state=random_state)
        model.vectorizer = vectorizer
        model.vocab = vocab
        model.fit(lda_mat)  # .astype(np.int64))

    # top word for each topic
    lda_labels = []
    t_word = model.topic_word_
    topic_order_idx = np.argsort(np.linalg.norm(t_word, axis=1))[::-1]
    topic_word = t_word[topic_order_idx]  # order by max length
    for ti, topic_dist in enumerate(topic_word):
        topic_words = vocab[np.argsort(topic_dist)][::-1]
        topic_words = topic_words[:n_top_words]
        lda_labels.append(' '.join(topic_words))
        logger.info('Topic {}: {}'.format(ti + 1, ' '.join(topic_words)))

    logger.info("building program by topic weight output matrix")
    n_docs = lda_mat.shape[0]
    lda_cats = np.zeros(n_docs, dtype=int)
    lda_output_mat = np.zeros((n_docs, n_topics))
    for x in range(n_docs):
        lda_output_mat[x, :] = model.doc_topic_[x][topic_order_idx]
        lda_cats[x] = np.argmax(lda_output_mat[x, :])

    if return_model:
        return lda_labels, lda_output_mat, lda_cats, lda_mat, model
    else:
        return lda_labels, lda_output_mat, lda_cats, lda_mat


def Hbeta(D=np.array([]), beta=1.0):
    """
    helper function for t-sne
    Compute the perplexity and the P-row for a specific value of the precision
    of a Gaussian distribution.
    """
    # Compute P-row and corresponding perplexity
    P = np.exp(-D.copy() * beta)
    sumP = sum(P)
    H = np.log(sumP) + beta * np.sum(D * P) / sumP
    P = P / sumP
    return H, P


def x2p(X=np.array([]), tol=1e-5, perplexity=30.0):
    """
    helper function for t-sne
    Performs a binary search to get P-values in such a way that each
    conditional Gaussian has the same perplexity.
    """
    # Initialize some variables
    logger.info("Computing pairwise distances...")
    (n, d) = X.shape
    sum_X = np.sum(np.square(X), 1)
    D = np.add(np.add(-2 * np.dot(X, X.T), sum_X).T, sum_X)
    P = np.zeros((n, n))
    beta = np.ones((n, 1))
    logU = np.log(perplexity)

    # Loop over all datapoints
    for i in range(n):

        # Share progress
        if i % 500 == 0:
            logger.info("Computing P-values for point %d of %d..." % (i, n))

        # Compute the Gaussian kernel and entropy for the current precision
        betamin = -np.inf
        betamax = np.inf
        Di = D[i, np.concatenate((np.r_[0:i], np.r_[i + 1:n]))]
        (H, thisP) = Hbeta(Di, beta[i])

        # Evaluate whether the perplexity is within tolerance
        Hdiff = H - logU
        tries = 0
        while np.abs(Hdiff) > tol and tries < 50:

            # If not, increase or decrease precision
            if Hdiff > 0:
                betamin = beta[i].copy()
                if betamax == np.inf or betamax == -np.inf:
                    beta[i] = beta[i] * 2
                else:
                    beta[i] = (beta[i] + betamax) / 2
            else:
                betamax = beta[i].copy()
                if betamin == np.inf or betamin == -np.inf:
                    beta[i] = beta[i] / 2
                else:
                    beta[i] = (beta[i] + betamin) / 2

            # Recompute the values
            (H, thisP) = Hbeta(Di, beta[i])
            Hdiff = H - logU
            tries = tries + 1

        # Set the final row of P
        P[i, np.concatenate((np.r_[0:i], np.r_[i + 1:n]))] = thisP

    # Return final P-matrix
    logger.info("Mean value of sigma: %.4f" % np.mean(np.sqrt(1 / beta)))
    return P


def pca(X=np.array([]), n_dims=50):
    """
    helper function for t-sne
    Runs PCA on the NxD array X in order to reduce its dimensionality to
    n_dims dimensions.
    """
    logger.info("Preprocessing the data using PCA...")
    (n, d) = X.shape
    X = X - np.tile(np.mean(X, 0), (n, 1))
    (l, M) = np.linalg.eig(np.dot(X.T, X))
    Y = np.dot(X, M[:, 0:n_dims])
    return Y


def tsne(X, n_dims=2, initial_dims=50, perplexity=30.0, max_iters=500):
    """
    Runs t-SNE on the dataset in the NxD array X to reduce its
    dimensionality to n_dims dimensions.
    The syntax of the function is Y = tsne.tsne(X, n_dims, perplexity),
    where X is an NxD NumPy array.
    """
    # Check inputs
    if isinstance(n_dims, float):
        logger.error("Array X should have type float.")
        return -1

    if round(n_dims) != n_dims:
        logger.error("Number of dimensions should be an integer.")
        return -1

    # Initialize variables
    X = pca(X, initial_dims).real
    (n, d) = X.shape
    initial_momentum = 0.5
    final_momentum = 0.8
    eta = 500
    min_gain = 0.01
    Y = np.random.randn(n, n_dims)
    dY = np.zeros((n, n_dims))
    iY = np.zeros((n, n_dims))
    gains = np.ones((n, n_dims))

    # Compute P-values
    P = x2p(X, 1e-5, perplexity)
    P = P + np.transpose(np.nan_to_num(P))  # tn
    P = np.nan_to_num(P) / np.sum(np.nan_to_num(P))  # tn
    P = P * 4  # early exaggeration
    P = np.maximum(P, 1e-12)

    # Run iterations
    for iter in range(max_iters):

        # Compute pairwise affinities
        sum_Y = np.sum(np.square(Y), 1)
        num = 1 / (1 + np.add(np.add(-2 * np.dot(Y, Y.T), sum_Y).T, sum_Y))
        num[list(range(n)), list(range(n))] = 0
        Q = num / np.sum(num)
        Q = np.maximum(Q, 1e-12)

        # Compute gradient
        PQ = P - Q
        for i in range(n):
            dY[i, :] = np.sum(np.tile(PQ[:, i] * num[:, i],
                                      (n_dims, 1)).T * (Y[i, :] - Y), 0)

        # Perform the update
        if iter < 20:
            momentum = initial_momentum
        else:
            momentum = final_momentum

        gains = (gains + 0.2) * ((dY > 0) != (iY > 0)) + \
                (gains * 0.8) * ((dY > 0) == (iY > 0))
        gains[gains < min_gain] = min_gain
        iY = momentum * iY - eta * (gains * dY)
        Y = Y + iY
        Y = Y - np.tile(np.mean(Y, 0), (n, 1))

        # Compute current value of cost function
        if (iter + 1) % 10 == 0:
            C = np.sum(P * np.log(P / Q))
            logger.info("Iteration %d: error is %s" % (iter + 1, C))

        # Stop lying about P-values
        if iter == 100:
            P = P / 4

    # Return solution
    return Y

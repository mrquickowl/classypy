import numpy as np
import pytest

import classypy.ml.model_metrics as mm


# We generate a zero division error to tests the case when baseline prediction is exactly correct
@pytest.mark.filterwarnings("ignore")
def test_error_over_baseline():
    """
    Test that get_error_over_baseline returns the correct values for two sample vectors. We test to ensure
    that we get the correct % difference over baseline for both MSE and MAE.
    """
    y_pred = [-1, 0, 1, 2, 3, 4, 5]
    y_test = [-1, 1, 2, 3, 4, 5, 6]

    mse_diff = mm.get_error_over_baseline(y_pred=y_pred, y_test=y_test, method="mse")
    mae_diff = mm.get_error_over_baseline(y_pred=y_pred, y_test=y_test, method="mae")

    # Test baseline improvement in MSE was computed correctly
    assert round(mse_diff, 2) == 0.83, (
        "The percentage improvement over baseline using MSE is incorrect")

    # Test baseline improvement in MAE was computed correctly
    assert round(mae_diff, 2) == 0.54, (
        "The percentage improvement over baseline using MAE is incorrect")

    # Test infs were computed when baseline prediction is exactly correct
    y_pred = [0, 1, 2, 3, 4, 5]
    y_test = [2, 2, 2, 2, 2, 2]

    mse_diff = mm.get_error_over_baseline(y_pred=y_pred, y_test=y_test, method="mse")
    mae_diff = mm.get_error_over_baseline(y_pred=y_pred, y_test=y_test, method="mae")

    # Test baseline improvement in MSE was computed correctly when Avg(y_test) is correct
    assert mse_diff == -np.inf, (
        "The percentage improvement over baseline using MSE is incorrect when baseline is correct")

    # Test baseline improvement in MAE was computed correctly when Avg(y_test) is correct
    assert mae_diff == -np.inf, (
        "The percentage improvement over baseline using MAE is incorrect when baseline is correct")

    # Test max improvement is 100% (predictions are exactly correct)
    y_pred = [0, 1, 2, 3, 4, 5]
    y_test = [0, 1, 2, 3, 4, 5]

    mse_diff = mm.get_error_over_baseline(y_pred=y_pred, y_test=y_test, method="mse")
    mae_diff = mm.get_error_over_baseline(y_pred=y_pred, y_test=y_test, method="mae")

    # Test baseline improvement in MSE was computed correctly when y_pred == y_test
    assert round(mse_diff, 2) == 1, (
        "The percentage improvement over baseline using MSE is incorrect when prediction is correct")

    # Test baseline improvement in MAE was computed correctly when y_pred == y_test
    assert round(mae_diff, 2) == 1, (
        "The percentage improvement over baseline using MAE is incorrect when prediction is correct")


def test_kendall_distance():
    """
    Test that kendall_distance returns the correct values for two sample vectors. Kendalls distance is a measurement of
    discordance amongst two ranked lists. Our ranked lists below have 6 elements, we can choose 15 unique pairs from
    each list when the order between the pairs does not matter. We are interested in how many pairs the lists would
    order differently than each other - that is how many pairs return different rank orderings between the two ordered
    lists. We also test to ensure that a TypeError is raised when we pass an array composed of non-numeric values.
    """
    y_pred = [2, 1, 3, 4, 6, 5]
    y_test = [1, 2, 3, 4, 5, 6]

    kd = mm.kendall_distance(y_pred=y_pred, y_test=y_test)

    # Test Kendall Distance was computed correctly
    assert kd == 2, (
        "Kendall Distance was computed incorrectly")

    # Test error is thrown when list contains non-numeric objects
    y_pred = [None, "Rob", 1, 2, 3, np.inf]
    y_test = [0, 1, 2, 3, 4, 5]

    # Test that TypeError is raised when an array not composed of numeric objects is passed
    with pytest.raises(TypeError):
        mm.kendall_distance(y_pred=y_pred, y_test=y_test)


def test_correct_rank_probability():
    """
    Test that correct_rank_probability is returning correct values. Kendall's tau can be defined as:

    Probability(Same ordering of pairs) - Probability(Different ordering of pairs)

    between any two ordered lists (of the same length) for every pair that we can choose from each list.
    There are 15 possible pairs in the first test below, only one of which have different orderings. Therefore,
    the probability of same is 1 - (1/15).

    We also test that a TypeError is raised when we pass an array composed of non-numeric values
    """
    def _factorial(n):
        if n == 0:
            return 1
        else:
            return n * _factorial(n - 1)

    y_pred = [1, 2, 3, 4, 6, 5]
    y_test = [1, 2, 3, 4, 5, 6]

    possible_pairs = _factorial(len(y_pred)) / (2 * _factorial(len(y_pred) - 2))
    crp_value = (1. - (1. / possible_pairs))
    crp = mm.correct_rank_probability(y_pred=y_pred, y_test=y_test)

    # Test Correct Rank Probability was computed correctly
    assert crp == crp_value, (
        "Correct Rank Probability was computed incorrectly")

    # Test error is thrown when list contains non-numeric objects
    y_pred = [None, "Rob", 1, 2, 3, np.inf]
    y_test = [0, 1, 2, 3, 4, 5]

    # Test that TypeError is raised when an array not composed of numeric objects is passed
    with pytest.raises(TypeError):
        mm.correct_rank_probability(y_pred=y_pred, y_test=y_test)


def test_precision_recall_fscore_support():
    """
    Test that we are returning the correct precision, recall, f-1 score, and support for all average
    type selections, and for binary and multi-label classification results.
    """
    # Test dictionary contains y_test, y_pred, and confusion matrix result for both
    # binary and multi-label tests
    test_dict = {
        'binary': {
            'y_test': [0, 0, 1, 1, 1, 1, 1, 0, 0, 0],
            'y_pred': [0, 1, 1, 0, 1, 0, 1, 0, 1, 0],
            'average_tests': {
                'None': {
                    'average': None,
                    'precision_recall_fscore': [
                        np.array([0.60, 0.60]), np.array([0.60, 0.60]), np.array([0.60, 0.60]), np.array([5, 5])],
                },
                'micro': {
                    'average': 'micro',
                    'precision_recall_fscore': np.array([0.60, 0.60, 0.60, None]),
                },
                'macro': {
                    'average': 'macro',
                    'precision_recall_fscore': np.array([0.60, 0.60, 0.60, None]),
                },
                'weighted': {
                    'average': 'weighted',
                    'precision_recall_fscore': np.array([0.60, 0.60, 0.60, None]),
                },
            },
        },
        'multi-label': {
            'y_test': [0, 1, 1, 2, 2, 0, 1, 2, 0, 0, 2, 1, 0, 2, 0, 1, 3],
            'y_pred': [1, 2, 0, 1, 1, 2, 1, 2, 0, 1, 0, 1, 2, 3, 0, 2, 0],
            'average_tests': {
                'None': {
                    'average': None,
                    'precision_recall_fscore': [
                        np.array([0.40, 0.33, 0.20, 0.00]), np.array([0.33, 0.40, 0.20, 0.00]),
                        np.array([0.36, 0.36, 0.20, 0.00]), np.array([6, 5, 5, 1])],
                },
                'micro': {
                    'average': 'micro',
                    'precision_recall_fscore': [0.29, 0.29, 0.29, None],
                },
                'macro': {
                    'average': 'macro',
                    'precision_recall_fscore': [0.23, 0.23, 0.23, None],
                },
                'weighted': {
                    'average': 'weighted',
                    'precision_recall_fscore': [0.30, 0.29, 0.29, None],
                },
            },
        },
    }
    # Test for both binary and multi-label classification
    for class_type, class_dict in test_dict.items():
        for average, average_dict in class_dict['average_tests'].items():
            # For each type of classification, test if function returns the correct confusion matrix
            precision, recall, fscore, support = mm.get_precision_recall_fscore_support(
                class_dict['y_pred'], class_dict['y_test'], average=average_dict['average'])
            assert np.array_equal(
                [precision, recall, fscore], average_dict['precision_recall_fscore'][0:3]) \
                and np.array_equal(support, average_dict['precision_recall_fscore'][3]), (
                "".join(
                    ["Precision, Recall, F-score, and Support are incorrect for {} classification".format(class_type),
                        " problem using average={}.".format(average)]))


def test_classification_report():
    """
    Test the classification report results for both binary and multi-label classification problems.
    """

    # Test dictionary contains y_test, y_pred, and confusion matrix result for both
    # binary and multi-label tests
    test_dict = {
        'binary': {
            'y_test': [0, 0, 1, 1, 0, 1, 0, 1],
            'y_pred': [0, 0, 1, 0, 1, 1, 0, 0],
            'cr_test': {
                '0': {'precision': 0.60, 'recall': 0.75, 'f1-score': 0.67, 'support': 4},
                '1': {'precision': 0.67, 'recall': 0.50, 'f1-score': 0.57, 'support': 4},
                'micro avg': {'precision': 0.62, 'recall': 0.62, 'f1-score': 0.62, 'support': 8},
                'macro avg': {'precision': 0.63, 'recall': 0.62, 'f1-score': 0.62, 'support': 8},
                'weighted avg': {'precision': 0.63, 'recall': 0.62, 'f1-score': 0.62, 'support': 8}},
        },
        'multi-label': {
            'y_test': [0, 1, 1, 2, 2, 0, 1, 2, 0, 0, 2, 1, 0, 2, 0, 1, 3],
            'y_pred': [1, 2, 0, 1, 1, 2, 1, 2, 0, 1, 0, 1, 2, 3, 0, 2, 0],
            'cr_test': {
                '0': {'precision': 0.40, 'recall': 0.33, 'f1-score': 0.36, 'support': 6},
                '1': {'precision': 0.33, 'recall': 0.40, 'f1-score': 0.36, 'support': 5},
                '2': {'precision': 0.20, 'recall': 0.20, 'f1-score': 0.20, 'support': 5},
                '3': {'precision': 0.00, 'recall': 0.00, 'f1-score': 0.00, 'support': 1},
                'micro avg': {'precision': 0.29, 'recall': 0.29, 'f1-score': 0.29, 'support': 17},
                'macro avg': {'precision': 0.23, 'recall': 0.23, 'f1-score': 0.23, 'support': 17},
                'weighted avg': {'precision': 0.30, 'recall': 0.29, 'f1-score': 0.29, 'support': 17}},
        },
    }
    # Test for both binary and multi-label classification
    for class_type, values in test_dict.items():
        cr = mm.get_classification_report(y_pred=values['y_pred'], y_test=values['y_test'])
        # Test classification report dictionary
        assert cr == values['cr_test'], (
            "Classification Report is incorrect for a {} classification problem.".format(class_type))


def test_confusion_matrix():
    """
    Test that get_confusion_matrix returns the correct matrices for both binary and
    multi-label classification problems.
    """
    # Test dictionary contains y_test, y_pred, and confusion matrix result for both
    # binary and multi-label tests
    test_dict = {
        'binary': {
            'y_test': [0, 0, 1, 1, 0, 1, 0, 1, 1, 0],
            'y_pred': [0, 0, 1, 0, 1, 1, 0, 1, 0, 0],
            'cm_test': np.array([[4, 1], [2, 3]]),
        },
        'multi-label': {
            'y_test': [0, 1, 1, 2, 2, 0, 1, 2, 0, 0, 2, 1, 0, 2, 0, 1, 0],
            'y_pred': [1, 2, 0, 1, 1, 2, 1, 2, 0, 1, 0, 1, 2, 3, 0, 2, 0],
            'cm_test': np.array([[3, 2, 2, 0], [1, 2, 2, 0], [1, 2, 1, 1], [0, 0, 0, 0]]),
        },
    }
    # Test for both binary and multi-label classification
    for class_type, values in test_dict.items():
        # For each type of classification, test if function returns the correct confusion matrix
        cm = mm.get_confusion_matrix(y_pred=values['y_pred'], y_test=values['y_test'])
        assert np.array_equal(cm, values['cm_test']), (
            "Confusion matrix is incorrect for a {} classification problem.".format(class_type))

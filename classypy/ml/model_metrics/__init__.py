import itertools

import numpy as np
from scipy.stats import kendalltau, rankdata
from sklearn.metrics import (classification_report, confusion_matrix, f1_score, mean_absolute_error, mean_squared_error,
                             precision_recall_fscore_support)


def get_mse(y_pred, y_test):
    """
    Returns the MSE - mean squared error, over the predictions on the test set.

    Parameters
    ----------
    y_pred : arr
        Array containing ranked predicted results
    y_test: arr
        Array containing ranked actual results

    Return:
    -------
    MSE
    """
    model_result = mean_squared_error(y_pred=y_pred, y_true=y_test)

    return model_result


def get_error_over_baseline(y_pred, y_test, method="mse"):
    """
    Get the % difference over the baseline mse or mae using the simple average from the test set

    Parameters
    ----------
    y_pred : arr
        Array containing ranked predicted results
    y_test: arr
        Array containing ranked actual results
    mse : bool
        If true, find % difference over baseline <SE else find % difference over baseline MAE

    Return:
    -------
    Improvement over baseline prediction (naively predicting the mean)

    """
    y_pred_avg = [np.mean(y_test)] * len(y_test)

    if method == "mse":
        baseline_result = mean_squared_error(y_pred=y_pred_avg, y_true=y_test)
        model_result = mean_squared_error(y_pred=y_pred, y_true=y_test)
    elif method == "mae":
        baseline_result = mean_absolute_error(y_pred=y_pred_avg, y_true=y_test)
        model_result = mean_absolute_error(y_pred=y_pred, y_true=y_test)
    else:
        raise TypeError("{method} is not one of mae or mse".format(method=method))

    # Get percentage difference
    percent_diff = (baseline_result - model_result) / baseline_result

    return percent_diff


def kendall_distance(y_pred, y_test):
    """
    Kendall's distance is a measure of concordance, how often are the two ranked lists (y_pred and y_test) in
    agreement with each other for any two-paired combination of observations?

    Reference: https://stats.stackexchange.com/questions/168602/whats-the-kendall-taus-distance-between-these-2-rankings

    Parameters
    ----------
    y_pred : arr
        Array containing ranked predicted results
    y_test: arr
        Array containing ranked actual results

    Return:
    -------
    Returns kendall's distance between the ranks in y_pred and y_test

    """
    pairs = itertools.combinations(range(0, len(y_pred)), 2)
    distance = 0

    for x, y in pairs:
        a = y_pred[x] - y_pred[y]
        b = y_test[x] - y_test[y]

        # if discordant (different signs)
        if (a * b < 0):
            distance += 1

    return distance


def correct_rank_probability(y_pred, y_test):
    """
    Compute the probability that two accounts selected at random will be in the correct order in both the true ranked
    list and predicted ranked list. Assumes that the index of y_pred and y_test are aligned.

    The Kendall Tau Correlation Coefficient maps onto the equation: P(same) - P(different).

    - P(different): The probability that two examples picked at random will NOT be in the same order in both
    the original ranked list and predicted ranked list.
    P(different) = (2 * kendall's symmetric difference distance) / (N * (N-1))
    - P(same): The probability that two examples picked at random will be in the same order in both
    the original ranked list and predicted ranked list.
    P(same) = Tau - P(different)

    Reference: https://www.utdallas.edu/~herve/Abdi-KendallCorrelation2007-pretty.pdf, equation 5

    Parameters
    ----------
    y_pred : arr
        Array containing predicted results
    y_test: arr
        Array containing actual results

    Return:
    -------
    P(same), calculated from Kendall's Tau coefficient

    """
    y_pred = [-1 * i for i in y_pred]
    y_test = [-1 * i for i in y_test]
    y_pred_ranked = rankdata(y_pred, method="ordinal")
    y_test_ranked = rankdata(y_test, method="ordinal")

    kendalls_tau, p_value = kendalltau(y_pred_ranked, y_test_ranked)
    distance = kendall_distance(y_pred=y_pred_ranked, y_test=y_test_ranked)
    n = len(y_pred)
    p_same = 1. - ((2. * distance) / (n * (n - 1.)))

    return p_same


def get_precision_recall_fscore_support(y_pred, y_test, average=None, n_digits=2):
    """
    Returns the Precision, Recall, F-1 scores, and Support over the predictions on the test set,
    for each class.

    Average lets the user determine the type of averaging performed on the data:
        - None: returns the scores for each class (Default)
        - 'micro': returns metrics calculated globally by counting the total true positives,
            false negatives and false positives.
        - 'macro': returns metrics calculated for each label, and find their unweighted mean.
            This does not take label imbalance into account.
        - 'weighted': returns metrics calculated for each label, average weighted by the support.
            This does take into account label imbalance.

    Parameters
    ----------
    y_pred : arr
        Array containing ranked predicted results
    y_test: arr
        Array containing ranked actual results
    average : string, one of [None, 'micro', 'macro', 'weighted']
        Type of averaging to be performed. Default is None.
    n_digits: int
        Number of digits to round the output. Default is 2.

    Return:
    -------
    precision : float
        Precision is the ratio Tp / (Tp + Fp) where Tp is the number of true positives and Fp the number
        of false positives, and it tells us, out of all the samples classified as positive, how many were
        actually positive.
    recall : float
        Recall is the ratio Tp / (Tp + Fn) where Fn is the number of false negatives. Recall tells us out
        of all the positive samples, how many were actually labeled as positive.
    fscore : float
        F-1 score can be interpreted as a weighted harmonic mean of the precision and recall (weighted equally),
        and it reaches its best value at 1 and worst value at 0.
    support : int
        Support is the number of occurrences of each class in y_true.
    """
    assert average in [None, 'micro', 'macro', 'weighted'], (
        "Average is not one of [None, 'micro', 'macro', 'weighted']")
    precision, recall, fscore, support = precision_recall_fscore_support(y_pred=y_pred, y_true=y_test, average=average)
    precision = np.round(precision, n_digits)
    recall = np.round(recall, n_digits)
    fscore = np.round(fscore, n_digits)

    return precision, recall, fscore, support


def get_f1score(y_pred, y_test, average='weighted', n_digits=2):
    """
    Returns the F-1 score over the predictions on the test set, for each class.

    Average lets the user determine the type of averaging performed on the data:
        - None: returns the scores for each class (Default)
        - 'micro': returns metrics calculated globally by counting the total true positives,
            false negatives and false positives.
        - 'macro': returns metrics calculated for each label, and find their unweighted mean.
            This does not take label imbalance into account.
        - 'weighted': returns metrics calculated for each label, average weighted by the support.
            This does take into account label imbalance.

    Parameters
    ----------
    y_pred : arr
        Array containing ranked predicted results
    y_test: arr
        Array containing ranked actual results
    average : string, one of [None, 'micro', 'macro', 'weighted']
        Type of averaging to be performed. Default is 'weighted'.
    n_digits: int
        Number of digits to round the output. Default is 2.

    Return:
    -------
    f1score : float
        F-1 score can be interpreted as a weighted harmonic mean of the precision and recall (weighted equally),
        and it reaches its best value at 1 and worst value at 0.
    """
    assert average in [None, 'micro', 'macro', 'weighted'], (
        "Average is not one of [None, 'micro', 'macro', 'weighted']")

    f1score = f1_score(y_pred=y_pred, y_true=y_test, average=average)
    f1score = np.round(f1score, n_digits)

    return f1score


def get_classification_report(y_pred, y_test, n_digits=2):
    """
    Returns a dictionary containing the main classification metrics (see sklearn.metrics.classification_report)

    Parameters
    ----------
    y_pred : arr
        Array containing ranked predicted results
    y_test: arr
        Array containing ranked actual results
    n_digits: int
        Number of digits to round the output. Default is 2.

    Return
    ------
    Classification report as a dict.
    """
    report_dict = classification_report(y_true=y_test, y_pred=y_pred, output_dict=True)
    # Round values first
    for label, key_dict in report_dict.items():
        for metric, value in report_dict[label].items():
            report_dict[label][metric] = np.round(value, n_digits)

    return report_dict


def get_confusion_matrix(y_pred, y_test):
    """
    Creates a confusion matrix to evaluate a classifier.

    From sklearn:
        "By definition a confusion matrix C is such that Cij is equal to the number of
        observations known to be in group i but predicted to be in group j."


    Parameters
    ----------
    y_pred : array
        Array containing ranked predicted results
    y_test: array
        Array containing ranked actual results

    Returns
    -------
    cm : array
        Confusion matrix
    """
    cm = confusion_matrix(y_true=y_test, y_pred=y_pred)

    return cm

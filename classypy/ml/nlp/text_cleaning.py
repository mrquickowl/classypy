"""Object for cleaning text blocks"""

import re

import nltk
from bs4 import BeautifulSoup

from classypy.devops import logger
from classypy.util.etl import normalize_whitespace, remove_html_artifacts, to_ascii


class TextCleaner(object):
    """Class for cleaning text blocks."""

    def __init__(self, language='english'):
        nltk.download('stopwords', quiet=not logger.logsLevel(logger.DEBUG))
        self.stopwords = nltk.corpus.stopwords.words(language)

    @classmethod
    def normalize_spacing(cls, text):
        """If punctuation is followed by a character, insert space.

        Parameters
        ----------
        text: str
          Block of text

        Returns
        -------
        str
          Same block of text, but with normalized punctuation spacing,
          e.g. "word.word" becomes "word. word"

        """
        # Handle edge case: preserve question semantics in favor of exclamation
        text = text.replace("?!", "?")
        text = re.sub(r'\.(?=[^ \W\d])', '. ', text)
        text = re.sub(r'\!(?=[^ \W\d])', '! ', text)
        text = re.sub(r'\?(?=[^ \W\d])', '? ', text)
        text = re.sub(r'\,(?=[^ \W\d])', ', ', text)
        return normalize_whitespace(text).strip()

    @classmethod
    def strip_html(cls, html_text, parser='lxml'):
        """Uses BeautifulSoup to strip HTML from text."""
        cleantext = BeautifulSoup(html_text, parser).text
        return remove_html_artifacts(cleantext)

    def tokenize(self, text, min_length=1, remove_stops=True):
        """Word-tokenizes text, then removes stopwords.

        Parameters
        ----------
        text: str
          cleaned text string with punctuation removed
        min_length: int
          minimum word length to keep in returned list

        Returns
        -------
        list
          list of words in text, without stopwords.
        """
        if remove_stops:
            return [w for w in text.split() if w not in self.stopwords and len(w) >= min_length]
        else:
            return [w for w in text.split() if len(w) >= min_length]

    @classmethod
    def remove_punctuation(cls, text):
        """Removes all punctuation from text (and special characters, if not already removed).

        Spacing should already be normalized to prevent accidental word concatenation, use
        TextCleaner.normalize_spacing(text) first.

        Note that this does not protect non-English characters like Cyrillic.

        Parameters
        ----------
        text: str
          text string

        Returns
        -------
        str
          string with punctuation removed (and all special characters)
        """
        return re.sub(r'[^A-Za-z0-9\s]+', '', text)

    @classmethod
    def preserve_only_text(cls, text):
        """Preserve only text, e.g. a-z and A-z.

        Spacing should already be normalized to prevent accidental word concatenation, use
        TextCleaner.normalize_spacing(text) first.

        Note that this does not protect non-English characters like Cyrillic.

        Parameters
        ----------
        text: str
          text string

        Returns
        -------
        str
          string with special characters, punctuation, and numbers removed.
        """
        return re.sub(r'[^A-Za-z\s]+', '', text)

    @classmethod
    def tokenize_sentences(cls, text):
        """Normalize spacing, then tokenize text into list of sentences using NLTK.

        Parameters
        ----------
        text: str
          text string

        Returns
        -------
        list
          list of each sentence in text
        """
        return nltk.sent_tokenize(cls.normalize_spacing(text))

    @classmethod
    def remove_special_characters(cls, text):
        """Remove special characters, but preserve standard punctuation.
        Parameters
        ----------
        text: str
          text string

        Returns
        -------
        str
          string with special characters removed
        """
        return re.sub(r'[^A-Za-z0-9.?!$,\s]+', '', text)

    @classmethod
    def remove_hyperlinks(cls, text):
        """Remove hyperlinks beginning with 'http' from text.

        Other class methods will simply remove special characters, but will preserve characters
        between them. This replaces all character strings that start with http, www, or ftp.

        Parameters
        ----------
        text: str
          text string

        Returns
        --------
        str
          string with hyperlinks removed
        """
        # Actually the .com has a bug, it'll remove anything with "com"
        text = re.sub(r'http\S+|ftp\S+|www\.\S+|\S+\.com|\S+\.org|\S+\.net|\S+\.edu', '', text)
        return text

    @classmethod
    def clean_text(cls, text, lowercase=False, remove_punctuation=False,
                   normalize=True):
        """Bundle other text-cleaning methods together in a suite.

        Currently used in predictive-models and knowledge-repo (8/9/2018).

        Note that in Python2, Unicode characters embedded in strings (e.g. they are
        <str>, not <unicode> objects), are not currently removed, whereas
        in Python3 they are.

        Parameters
        ----------
        text: str
          Block of HTML-formatted text
        lowercase: bool, optional
          Controls whether text is forced to lowercase. (Default: False)
        remove_punctuation: bool, optional (Default: False)
          Controls whether punctuation is stripped from text
        normalize: bool, optional (Default: True)
          Controls whether spaces are added after punctuation

        Returns
        -------
        str
          cleaned text string

        Raises
        ------
        TypeError
          If text is not a string, the call to strip_html will fail, in
          which case an empty string is returned.

        """
        text = to_ascii(text)
        try:
            cleantext = cls.strip_html(text)
        except TypeError:
            return ""
        cleantext = cls.remove_hyperlinks(cleantext)
        cleantext = cls.remove_special_characters(cleantext)
        if normalize:
            cleantext = cls.normalize_spacing(cleantext)
        if lowercase:
            cleantext = cleantext.lower()
        if remove_punctuation:
            cleantext = cls.remove_punctuation(cleantext)
        return cleantext

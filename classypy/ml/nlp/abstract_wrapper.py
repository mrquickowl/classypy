"""Abstract interface for sentiment analysis wrapper."""

from abc import abstractmethod

import numpy as np
import scipy.stats


class AbstractSentimentAnalyzer(object):
    """Abstract interface for a sentiment analysis wrapper."""

    def __init__(self):
        pass

    @abstractmethod
    def score_sentiment_of_sentences(self, text):
        pass

    @classmethod
    def calculate_zero_crossings(cls, vector):
        """Return number of zero-crossings.

        Parameters
        ----------
        vector: list
           List of floats.

        Returns
        -------
        numpy.int64
          Number of times that the sign (+ or -) changes in vector.
        """
        return (np.diff(np.sign(vector)) != 0).sum()

    @classmethod
    def calculate_weighted_average(cls, probability_weights, distribution):
        """Calculate weighted average using probability_weights over distribution.

        Parameters
        ----------
        probability_weights: np.array
           List of length N, with each element corresponding to a probability.
           Should sum to 1. For example: [.5, .25, .25, 0, 0]
        distribution: np.array
            List of length N, with each element corresponding to a value.
            For example: [-1, -.5, 0, .5, 1]

        Returns
        -------
        numpy.float64
          Mean of distribution, weighted by probability_weights.
        """
        weighted = probability_weights * distribution
        return weighted.mean()

    @classmethod
    def calculate_prop_pos(cls, scores):
        """Calculate proportion of positive scores.

        Parameters
        ----------
        scores: list
           List of sentiment scores.

        Returns
        -------
        float
            Proportion of positive scores.
        """
        if len(scores) == 0:
            return 0
        return len([s for s in scores if s > 0]) / float(len(scores))

    @classmethod
    def calculate_prop_neg(cls, scores):
        """Calculate proportion of negative scores.

        Parameters
        ----------
        scores: list
           List of sentiment scores.

        Returns
        -------
        float
            Proportion of negative scores.
        """
        if len(scores) == 0:
            return 0
        return len([s for s in scores if s < 0]) / float(len(scores))

    def sentiment_analysis_suite(self, text_blocks):
        """Run sentiment analysis suite over array of text blocks.
        For each text block, we calculate the sentiment of each sentence,
        then use the by-sentence scores to calculate standard deviation, IQR,
        mean, etc. Note that we assume the by-sentence scores contain no
        null values.

        Parameters
        ----------
        text_blocks: array
          array of cleaned text blocks
        analyzer: AbstractSentimentAnalyzer
          should be a subclass of abstract sentiment analyzer

        Returns
        -------
        dict
          dictionary containing mean sentiment scores, # of zero-crossings,
          # of sentences for each text block in text_block, and proportion
          of zero crossings (# of zero-crossings normalized by # of sentences)

        """
        # Initialize feature arrays
        means, zero_crossings, lengths = [], [], []
        prop_zero_crossings, sd, prop_pos, prop_neg, iqr = [], [], [], [], []

        # For each, grab mean sentiment, # zero-crossings, and # sentences
        for block in text_blocks:
            scores = self.score_sentiment_of_sentences(block)
            mean_score = scores.mean() if len(scores) > 0 else 0.0
            means.append(mean_score)
            zeroes = self.calculate_zero_crossings(scores)
            zero_crossings.append(zeroes)
            lengths.append(len(scores))
            prop_zero_crossings.append(zeroes / len(scores))
            sd.append(np.std(scores))
            prop_pos.append(self.calculate_prop_pos(scores))
            prop_neg.append(self.calculate_prop_neg(scores))
            iqr.append(scipy.stats.iqr(scores))

        return {
            'means': means,
            'zero_crossings': zero_crossings,
            'lengths': lengths,
            'prop_zeroes': prop_zero_crossings,
            'sd': sd,
            'prop_pos_sentences': prop_pos,
            'prop_neg_sentences': prop_neg,
            'iqr': iqr
        }

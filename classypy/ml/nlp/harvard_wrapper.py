"""Custom sentiment wrapper using custom wordlist."""

import os.path as op
from collections import Counter
from functools import partial

import pandas as pd

from classypy.ml.nlp.abstract_wrapper import AbstractSentimentAnalyzer
from classypy.ml.transformers import TextProcessor
from classypy.util.dirs import this_files_dir


class WordlistAnalyzer(object):
    """General interface for finding proportion of words in text from a word list.

    This list could be a semantic category, e.g. battle: ['fight', ...], sentiment
    categories, e.g. positive: ['yes'], etc.
    """

    def __init__(self, wordlist, lemmatize=True, remove_stopwords=True):
        """Initialize wordlist with mapping between labels and words

        Parameters
        ----------
        wordlist: list
            wordlist should be a list of words
        lemmatize: bool
            whether to lemmatize words in text_block before searching for match
        remove_stopwords: bool
            whether to remove stopwords before getting proportion of overlap
         """
        self.wordlist = wordlist
        self.preprocessor = TextProcessor(do_lemmatize=lemmatize, remove_stopwords=remove_stopwords)

    def _count_words_from_list(self, text_block):
        """Count number of occurrences in text_block for each word in wordlist.

        Parameters
        ----------
        text_block: str
          cleaned text block

        Returns
        -------
        dict
          dictionary with counts of each relevant word, and the total number of words (for normalizing)
        """
        processed_text = self.preprocessor.preprocess(text_block)
        if len(processed_text) == 0:
            return {'counts': 0,
                    'total_words': 0}
        appearances = [w for w in processed_text if w in self.wordlist]
        return {'counts': Counter(appearances),
                'total_words': len(processed_text)}

    def _get_prop_from_text(self, text_block):
        """
        Parameters
        ----------
        text_block: str
          cleaned text block

        Returns
        -------
        float
          proportion of words appearing in text_block from wordlist
        """
        counts = self._count_words_from_list(text_block)
        return 0.0 if counts['total_words'] == 0 else sum(counts['counts'].values()) / counts['total_words']

    def get_wc_prop(self, text_blocks):
        """Get proportion of overlap between wordlist and each text block.

        Parameters
        ----------
        text_blocks: list
          list of cleaned text blocks

        Returns
        -------
        list
          proportion of words from wordlist in each text_block
        """
        proportions = [self._get_prop_from_text(block) for block in text_blocks]
        return proportions


class HarvardCsvWordlistAnalyzer(AbstractSentimentAnalyzer):
    """Implements the abstract sentiment analyzer interface. By default, loads words sourced from
    SentimentAnalysis package in R.
    """
    def __init__(self, path_to_wordlist=op.join(this_files_dir(), 'annotated_words.csv'),
                 lemmatize=True, remove_stopwords=True):
        """Initialize wordlist with mapping between labels and words. By default, it uses
        annotated_words.csv, words sourced from the SentimentAnalysis package in R.

        Parameters
        ----------
        path_to_wordlist: str
            path to .csv file with word dictionary.
        lemmatize: bool
            whether to lemmatize words in text_block before searching for match
        remove_stopwords: bool
            whether to remove stopwords before getting proportion of overlap
        """
        self.lemmatize = lemmatize
        wordlists = self._load_wordlists_from_csv(path_to_wordlist=path_to_wordlist)
        analyzer_cls = partial(WordlistAnalyzer, remove_stopwords=remove_stopwords, lemmatize=lemmatize)
        self.analyzers = {key: analyzer_cls(wordlist=wordlist) for key, wordlist in wordlists.items()}

    def _load_wordlists_from_csv(self, path_to_wordlist):
        """Load dictionary of positive or negative words.

        Assumes the format in 'annotated_words.csv'.

        Parameters
        ----------
        path_to_wordlist: str
          path to .csv file with word dictionary. Column names of .csv file
          should be feature types (e.g. 'positive')

        Returns
        -------
        dict
          dictionary with mappings from word categories to word entries
        """
        df = pd.read_csv(path_to_wordlist, index_col=0)
        wordlists = {}
        # Automatically populate wordlist using column names (e.g. feature names)
        for column in df.columns:
            if column != "word":
                # Grab all words where relevant column is True
                wordlists[column] = list(df.loc[df[column]]['word'])
        return wordlists

    def sentiment_analysis_suite(self, text_blocks):
        """Overwrites the inherited function.

        Parameters
        ----------
        text_blocks: array
          array of cleaned text blocks

        Returns
        -------
        dict
          dictionary containing proportion of positive and negative words for each text block.
        """
        text_info = {}
        for label, analyzer in self.analyzers.items():
            text_info["prop_{label}".format(label=label)] = analyzer.get_wc_prop(text_blocks)
        return text_info

"""Wrapper function for NLTK sentiment."""

import nltk
import numpy as np
from nltk.sentiment.vader import SentimentIntensityAnalyzer

from classypy.devops import logger
from classypy.ml.nlp.abstract_wrapper import AbstractSentimentAnalyzer


class NLTKSentimentWrapper(AbstractSentimentAnalyzer):
    """Implements the abstract sentiment analyzer interface."""
    def __init__(self):
        """Initialize NLTK sentiment analyzer."""
        nltk.download('punkt', quiet=not logger.logsLevel(logger.DEBUG))
        nltk.download('vader_lexicon', quiet=not logger.logsLevel(logger.DEBUG))
        self.analyzer = SentimentIntensityAnalyzer()

    def score_sentiment_of_sentence(self, sentence):
        """Returns sentiment score for sentence."""
        return self.analyzer.polarity_scores(sentence)['compound']

    def score_sentiment_of_sentences(self, text):
        """Label sentiment using NLTK, outputs sentiment per sentence.

        If #sentences is 0, returns 0.0 sentiment.

        Parameters
        ----------
        text: str
           Cleaned text block, with punctuation preserved.

        Returns
        -------
        array
          Array of sentiment score for each sentence in text.
        """
        sentences = nltk.sent_tokenize(text)
        if len(sentences) == 0:
            return np.array([0.0])
        scores = np.array([self.score_sentiment_of_sentence(s) for s in sentences])
        return scores

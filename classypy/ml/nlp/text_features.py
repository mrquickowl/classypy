"""Class for extracting features from text blocks."""

import numpy as np
import pandas as pd
from sklearn.decomposition import LatentDirichletAllocation, TruncatedSVD
from sklearn.feature_extraction.text import CountVectorizer, TfidfVectorizer

from classypy.ml.nlp.harvard_wrapper import HarvardCsvWordlistAnalyzer
from classypy.ml.nlp.nltk_wrapper import NLTKSentimentWrapper
from classypy.ml.nlp.stanford_wrapper import CoreNLPWrapper
from classypy.ml.transformers import TextProcessor


class SentimentTransformer(object):
    """Extract sentiment features from text blocks."""

    def __init__(self, analyzer='nltk', name='sentiment', **params):
        """Initialize sentiment transformer object.

        Parameters
        ----------
        analyzer: str (default: 'nltk')
          one of {'nltk', 'corenlp', 'harvard'}
        **params: dict
          parameters to pass into sentiment wrapper constructor
        """
        self.sentiment_analyzer = self._load_analyzer(analyzer, **params)
        self.labels = None
        self.name = name

    def _load_analyzer(self, analyzer='nltk', **params):
        """Load specified analyzer.

        Parameters
        ----------
        analyzer: str (default: 'nltk')
          one of {'nltk', 'corenlp', 'harvard'}
        **params: dict
          parameters to pass into sentiment wrapper constructor
        """
        analyzers = {'nltk': NLTKSentimentWrapper,
                     'corenlp': CoreNLPWrapper,
                     'harvard': HarvardCsvWordlistAnalyzer}
        return analyzers[analyzer](**params)

    def fit(self, text_blocks):
        """No fitting required."""
        return self

    def transform(self, text_blocks):
        """Return a dataframe with sentiment features for each block in text_blocks.

        Parameters
        ----------
        text_blocks: list
          list of cleaned text blocks

        Returns
        -------
        pandas.DataFrame
          dataframe with sentiment features for each text_block
        """
        sent_features = self.sentiment_analyzer.sentiment_analysis_suite(text_blocks)
        self.labels = list(sent_features.keys())
        return pd.DataFrame.from_dict(sent_features)


class TopicModeler(object):
    """Class for extracting topic models from text blocks."""

    def __init__(self, language='english'):
        self.language = language
        self.tfidf_vectorizer = None
        self.count_vectorizer = None

    def _get_topic_labels(self, feature_names, dimensions, top_words=3):
        """Get labels for each dimension of a trained topic model, using original list of feature names.

        Parameters
        ----------
        feature_names: list
          list of original feature names from vectorizer
        dimensions: list
          matrix of factor loadings for each dimension
        top_words: int
          number of features to select from each model component

        Returns
        -------
        list
          list of len(dimensions), with aggregated feature labels for each dimension.
        """
        # Aggregate top-weighted features for each dimension of the model
        return [' '.join(feature_names[np.argsort(dimension)[-top_words:]]) for dimension in dimensions]

    def vectorize_text(self, text_blocks, vectorizer='tfidf', max_df=.5, min_df=2, lemmatize=False):
        """Fit specified vectorizer to text, then transform it.

        Parameters
        ----------
        text_blocks: list
          list of cleaned text blocks
        vectorizer: str
          specifies which vectorizer to use, one of {'tfidf', 'count'}
        max_df: float
          ignore terms that appear in more than this percentage of documents
        min_df: int
          ignore terms that appear in less than this number of documents

        Returns
        -------
        numpy.array
          matrix of vectorized text blocks
        """
        if vectorizer == 'tfidf':
            if self.tfidf_vectorizer is None:
                self.tfidf_vectorizer = TfidfVectorizer(max_df=max_df, min_df=min_df, stop_words='english',
                                                        use_idf=True, tokenizer=TextProcessor(do_lemmatize=lemmatize))
                self.tfidf_vectorizer.fit(text_blocks)
            return self.tfidf_vectorizer.transform(text_blocks)
        elif vectorizer == 'count':
            if self.count_vectorizer is None:
                self.count_vectorizer = CountVectorizer(max_df=max_df, min_df=min_df, stop_words='english',
                                                        tokenizer=TextProcessor(do_lemmatize=lemmatize))
                self.count_vectorizer.fit(text_blocks)
            return self.count_vectorizer.transform(text_blocks)
        raise Exception("Invalid vectorizer specified.")


class LDATransformer(TopicModeler):
    """Class for extracting LDA topic models from text blocks."""

    def __init__(self, language='english', name='LDA', num_topics=10, vectorizer='count', top_words=3):
        """Initialize LDA Transformer.

        Parameters
        ----------
        num_topics: int
          number of dimensions to reduce to
        vectorizer: string (default: count)
          vectorizer to use, one of {'tfidf', 'count'}
        top_words: int
          number of features to grab for each topic label
        """
        TopicModeler.__init__(self)
        self.lda_model = None
        self.labels = None
        self.name = name
        self.num_topics = num_topics
        self.vectorizer_name = vectorizer
        self.top_words = top_words

    def fit(self, text_blocks):
        """Fit LDA model to text_blocks.

        Parameters
        ----------
        text_blocks: list
          list of cleaned text blocks
        """
        vectorized_blocks = self.vectorize_text(text_blocks, vectorizer=self.vectorizer_name)
        self.lda_model = LatentDirichletAllocation(n_components=self.num_topics, max_iter=5,
                                                   learning_method='online',
                                                   random_state=0)
        self.lda_model.fit(vectorized_blocks)

    def transform(self, text_blocks):
        """Return a dataframe with distributions over LDA topics for each text_block.

        Parameters
        ----------
        text_blocks: list
          list of cleaned text blocks

        Returns
        -------
        pandas.DataFrame
          dataframe with LDA topic distributions
        """
        if self.lda_model is None:
            raise Exception("LDA Model is not yet trained")

        vectorized_blocks = self.vectorize_text(text_blocks, vectorizer=self.vectorizer_name)
        lda_matrix = self.lda_model.transform(vectorized_blocks)

        lda_labels = self._get_topic_labels(feature_names=np.array(self.count_vectorizer.get_feature_names()),
                                            dimensions=self.lda_model.components_,
                                            top_words=self.top_words)

        lda_df = pd.DataFrame(lda_matrix, columns=lda_labels)
        self.labels = lda_labels

        return lda_df


class LSATransformer(TopicModeler):
    """Class for extracting LSA topic models from text blocks."""

    def __init__(self, language='english', name='LSA', num_topics=10, vectorizer='tfidf', top_words=3):
        """Initialize LSA Transformer.

        Parameters
        ----------
        num_topics: int
          number of dimensions to reduce to
        vectorizer: string (default: tfidf)
          vectorizer to use, one of {'tfidf', 'count'}
        top_words: int
          number of features to grab for each topic label
        """
        TopicModeler.__init__(self)
        self.lsa_model = None
        self.name = name
        self.labels = None
        self.num_topics = num_topics
        self.vectorizer_name = vectorizer
        self.top_words = top_words

    def fit(self, text_blocks):
        """Fit LSA model to text_blocks.

        Parameters
        ----------
        text_blocks: list
          list of cleaned text blocks
        """
        vectorized_blocks = self.vectorize_text(text_blocks, vectorizer=self.vectorizer_name)
        self.lsa_model = TruncatedSVD(n_components=self.num_topics)
        self.lsa_model.fit(vectorized_blocks)

    def transform(self, text_blocks):
        """Return a dataframe with distributions over LSA topics for each text_block.

        Parameters
        ----------
        text_blocks: list
          list of cleaned text blocks

        Returns
        -------
        pandas.DataFrame
          dataframe with LSA topic distributions
        """
        if self.lsa_model is None:
            raise Exception("LSA Model is not yet trained")

        vectorized_blocks = self.vectorize_text(text_blocks, vectorizer=self.vectorizer_name)
        lsa_matrix = self.lsa_model.transform(vectorized_blocks)

        lsa_labels = self._get_topic_labels(feature_names=np.array(self.tfidf_vectorizer.get_feature_names()),
                                            dimensions=self.lsa_model.components_,
                                            top_words=self.top_words)

        lsa_df = pd.DataFrame(lsa_matrix, columns=lsa_labels)
        self.labels = lsa_labels

        return lsa_df


class TextTransformer(object):
    """Extract features (LDA, LSA, sentiment) from text blocks."""

    TRANSFORMATIONS = {'LDA': LDATransformer,
                       'LSA': LSATransformer,
                       'sentiment': SentimentTransformer}

    def __init__(self, config):
        """Initializes based on values in config list.

        Parameters
        ----------
        config: list
          should be list of tuples pairing transformer name with __init__ params, e.g.
          [('sentiment': {'analyzer': 'nltk'}),
           ('lda': {'num_topics': 10,
                    'vectorizer': 'count'})]
        """
        self.config = config
        self.transformers = [(self.TRANSFORMATIONS[name](**params)) for name, params in config]

    def fit(self, text_blocks):
        """Fit series of transformers on text blocks.

        Parameters
        ----------
        text_blocks: list
          list of cleaned text blocks to transform
        """
        for transformer in self.transformers:
            transformer.fit(text_blocks)

    def transform(self, text_blocks):
        """Transform text_blocks with configured transformers.

        Parameters
        ----------
        text_blocks: list
          list of cleaned text blocks to transform

        Returns
        -------
        pandas.DataFrame
          dataframe with features corresponding to each transformer
        """
        features_dfs = []
        for transformer in self.transformers:
            features_dfs.append(transformer.transform(text_blocks))
        return pd.concat(features_dfs, axis=1)

    def fit_transform(self, text_blocks):
        """Fit, then transform, text blocks using transformers.

        Parameters
        ----------
        text_blocks: list
          list of cleaned text blocks to transform

        Returns
        -------
        pandas.DataFrame
          dataframe with features corresponding to each transformer
        """
        self.fit(text_blocks)
        return self.transform(text_blocks)

"""Wrapper for CoreNLP Package.

In order to instantiate the Stanford CoreNLP analyzer in Python,
the Java code must be running on a server. If installed locally, run:

cd stanford-corenlp-full-2018-01-31
java -mx5g -cp "*" edu.stanford.nlp.pipeline.StanfordCoreNLPServer -timeout 10000
"""

import numpy as np
from pycorenlp import StanfordCoreNLP

from classypy.ml.nlp.abstract_wrapper import AbstractSentimentAnalyzer


class CoreNLPWrapper(AbstractSentimentAnalyzer):
    """Implements the abstract sentiment analyzer interface."""
    def __init__(self, server_url='http://localhost:9000'):
        """Initialize CoreNLP sentiment analyzer. Requires Java code to be running
        on local server.
        """
        self.analyzer = StanfordCoreNLP(server_url)

    def score_sentiment_of_sentences(self, cleantext):
        """Extract sentiment for each sentence with CoreNLP.

        Parameters
        ----------
        text: str
           Cleaned text block, with punctuation preserved.

        Returns
        -------
        array
          Array of sentiment score for each sentence in text.
        """
        # Initializes Python wrapper by connecting to Java CoreNLP object on local server.
        res = self.analyzer.annotate(cleantext,
                                     properties={
                                         'annotators': 'sentiment',
                                         'outputFormat': 'json',
                                         'timeout': 5000,
                                     })
        weighted = []
        if type(res) == str:  # In case annotation times out
            return None
        for anno in res['sentences']:
            prob_weights = np.array(anno['sentimentDistribution'])
            score = self.calculate_weighted_average(probability_weights=prob_weights,
                                                    distribution=np.array([-1, -5, 0, .5, 1]))
            weighted.append(score)
        return weighted

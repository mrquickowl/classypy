"""
Class for named entity recognition.
"""
import pandas as pd
from sklearn.base import BaseEstimator, TransformerMixin

from classypy.devops import logger


class NER(BaseEstimator, TransformerMixin):
    """
    Class for Named Entity Recognition.
    """

    NUMERIC = ['DATE', 'PERCENT', 'CARDINAL', 'TIME', 'MONEY', 'ORDINAL', 'QUANTITY']
    PLACE = ['GPE', 'LOC', 'FACILITY', 'ORG', 'NORP', 'EVENT']
    THING = ['LANGUAGE', 'PRODUCT', 'WORK_OF_ART', 'LAW']

    def __init__(self, language='en'):
        """
        Initialize NER.

        Parameters
        ----------
        language: str
            Human-language for model. Options are 'en'-English,
            'de'-German, and 'fr'-French.
            Languages must be downloaded to environment once before being loaded, e.g.;
            >>> python -m spacy download en
        """
        import spacy

        self.df = None
        self.ent_list = []
        self.embedding = []
        self.nlp = spacy.load('en')

    def _clean_dataframe(self, df):
        """
        Format returned dataframe.
        Parameters
        ----------
        df: pandas.DataFrame
            2-column, ('category', 'entity') dataframe where 'entity' corresponds to
            spacy entity outputs.

        Returns
        --------
            keep_df: pd.DataFrame
                Dtaframe with entities classified as PERSON, PLACE, or THING. Entities
                are stripped of whitespace and null values are removed.
        """
        keep_df = df[~df['category'].isin(self.NUMERIC)]
        keep_df.loc[keep_df['category'].isin(self.PLACE), 'category'] = 'PLACE'
        keep_df.loc[keep_df['category'].isin(self.THING), 'category'] = 'THING'
        keep_df['entity'] = keep_df['entity'].str.strip()
        keep_df = keep_df[~keep_df['category'].isnull()]
        keep_df = keep_df.reset_index(drop=True)
        return keep_df

    def _transform_sentence(self, sent):
        """
        Extract entities from sentence.

        Parameters
        ----------
        sent: str
            Sentence stored as a string.

        Returns
        -------
        entity_list: list
            List of tuples: ('word', 'label'), where label corresponds
            to a spacy entity type.
            See https://spacy.io/docs/usage/entity-recognition for entity types.
        """
        doc = self.nlp(sent.decode('utf-8'))  # noqa
        entity_list = [(ent.text, ent.label_)
                       for ent in doc.ents]
        return entity_list

    def fit(self, X, y=None):
        """
        `fit` method required for inclusion in sklearn pipeline object.
        Returns data as is.
        """
        return self

    def transform(self, documents):
        """
        Parameters
        ----------
        documents: iterable
            List of documents. Each document raw, untokenized text.

        Returns
        -------
        all_ents: List of lists.
            Each list stores the entities found in the corresponding document.
        """
        from nltk.tokenize import sent_tokenize
        if not isinstance(documents, list):
            raise ValueError("documents must be of type list.")
        all_ents = []
        for raw_doc in documents:
            entity_list = []
            docs = sent_tokenize(raw_doc)
            for doc in docs:
                try:
                    entities = self._transform_sentence(doc)
                    entity_list.extend(entities)
                except Exception as ex:  # noqa
                    logger.warning(ex)
            ent_df = pd.DataFrame(entity_list, columns=['entity', 'category'])
            new_df = self._clean_dataframe(ent_df)
            all_ents.append(new_df['entity'].tolist())
        return all_ents

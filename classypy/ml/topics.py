"""
Classes for topic modeling with gensim.
"""
from sklearn.base import BaseEstimator, TransformerMixin

from classypy.compat import u as unicode
from classypy.devops import logger


class GensimLsi(BaseEstimator, TransformerMixin):
    """
    Custom sklearn transformer to convert tokenized,
    preprocessed data to tf-idf representation.
    """
    def __init__(self, id2word_path, lsi_path=None, index_path=None):
        """
        Initialize GensimLsi class.

        Parameters
        ----------
        id2word_path: string
            File-path to saved gensim id2word Dictionary.
        lsi_path: string
            Optional path to saved gensim lsi model.
        index_path: string
            Optional path to saved Gensim MatrixSimilarity object.
        """
        self.num_topics = 10
        self.corpus = None
        self.index = None
        self.model = None
        self.id2word = self.load(id2word_path=id2word_path)
        if lsi_path is not None:
            self.model = self.load(lsi_path=lsi_path)
        if index_path is not None:
            self.index = self.load(index_path=index_path)

    @staticmethod
    def load(lsi_path=None, id2word_path=None, index_path=None):
        """
        If specified, attempts to load gensim LsiModel from `lsi_path`
        and gensim Dictionary from `id2word_path`.

        Parameters
        ----------
        lsi_path: str
            File-path designating where self.model should be saved.
        id2word_path: str
            File-path designating where self.dictionary should be saved.
        index_path: str
            File-path designating where self.index should be saved.
        """
        if lsi_path is not None:
            from gensim.models import LsiModel
            return LsiModel.load(lsi_path)
        if id2word_path is not None:
            from gensim.corpora.dictionary import Dictionary
            return Dictionary.load(id2word_path)
        if index_path is not None:
            from gensim.similarities import MatrixSimilarity
            return MatrixSimilarity.load(index_path)

    def save(self, lsi_path=None, id2word_path=None, index_path=None):
        """
        Saves objects from fit process: gensim.LsiModel to `lsi_path`
        and gensim.Dictionary to `id2word_path`.
        If either self.model or self.dictionary does not exist, an
        AttributeError is raised.

        Parameters
        ----------
        lsi_path: str
            File-path designating where self.model should be saved.
        id2word_path: str
            File-path designating where self.dictionary should be saved.
        index_path: str
            File-path designating where self.index should be saved.
        """
        if not (self.model and self.id2word):
            raise AttributeError('Nothing to save yet, please run .fit first.')
        if lsi_path is not None:
            self.model.save(lsi_path)
        if id2word_path is not None:
            self.id2word.save(id2word_path)
        if index_path is not None:
            self.index.save(index_path)

    def fit(self, documents, num_topics=25, labels=None):
        """
        Fits a gensim LsiModel to documents.

        Parameters
        ----------
        documents: iterable
            List of documents. Each document must be a list of preprocessed tokens.
        num_topics: int
            Number of topics with which to fit model. default=25.
        labels: iterable
            Optional list of same size as documents, specifying label for each document.

        """
        from gensim.models import LsiModel
        self.index = None
        self.num_topics = num_topics
        self.model = LsiModel(documents, id2word=self.id2word, num_topics=self.num_topics)
        self.corpus = self.model[documents]
        return self

    def transform(self, documents):
        """
        Returns a vectorized embedding of each document in documents.

        Parameters
        -----------
        documents: iterable
            List of documents. Each document must be a list of tokens.

        Returns
        -------
            iterable: list of vectorized documents.
        """
        if self.model is None:
            raise AttributeError('Must have a fit model in order'
                                 ' to call transform.')
        return self.model[documents]

    def build_index(self):
        from gensim.similarities import MatrixSimilarity
        if self.index is None:
            self.index = MatrixSimilarity(self.corpus)

    def similarity(self, doc, n=10, data_names=None):
        """
        Returns the `n` most similar items in `self.corpus`
        to `doc`.

        Parameters
        ----------
        doc: list
            A document. embedded in same tfidf space as model.
        n: int (default=10)
            Number of most similar items to return.
        data_names: list
            Optional list of data names. If provided, the a verbose output
            will be printed, specifying the data names in the result list.

        Returns
        -------
            sims: dictionary of (item, distance) key, value pairs sorted by
                similarity in descending order.
        """
        if self.model is None:
            raise AttributeError('Must have a fit model in order'
                                 ' to call similarity.')
        self.build_index()

        doc_lsi = self.model[doc]
        sims = self.index[doc_lsi]
        sims = sorted(enumerate(sims), key=lambda _, score: -score)[0:n]
        if data_names is not None:
            results = ["{name} -- {score}".format(name=unicode(data_names[item_num]).strip(), score=score)
                       for item_num, score in sims]
            logger.info(results)
        return sims

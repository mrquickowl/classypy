import numpy as np
import pandas as pd
import pytest
from numpy.testing import assert_almost_equal, assert_array_almost_equal

from classypy.ml.distances import _gower_vector, gower_similarity_matrix, levenshtein


def test_two_different_strings_should_be_size_of_longer_string():
    small_string = 'sss'
    large_string = 't' * 30
    assert levenshtein(small_string, large_string) == len(large_string)


def test_capital_and_non_capital_should_be_different():
    classy = 'classy'
    assert levenshtein(classy, classy.upper()) == len(classy)


def test_integer_args_should_raise_error():
    with pytest.raises(TypeError):
        levenshtein(5, 6)


def test_only_one_arg_should_raise_error():
    with pytest.raises(TypeError):
        assert levenshtein('',) == 0


def test_case_sensitivity():
    assert levenshtein('ben', 'Ben') == 1
    assert levenshtein('ben', 'Ben', case_sensitive=False) == 0

    # Length differences makes for a recursive call; make sure the param is passed
    assert levenshtein('benjie', 'Ben') == 4
    assert levenshtein('benjie', 'Ben', case_sensitive=False) == 3

    assert levenshtein('Ben', 'benjie') == 4
    assert levenshtein('Ben', 'benjie', case_sensitive=False) == 3


@pytest.mark.parametrize("word1, word2, distance", [
    ('classy', '', 6),
    ('house', 'haus', 2),
    ('d1n0s@ur.', 'dinosaur', 4),
    ('cat', 'cat', 0),
    ('', '', 0),
    ('cat', 'dog', 3), ])
def test_lev_distances(word1, word2, distance):
    assert levenshtein(word1, word2) == distance


def test_gower_vector_numeric():
    X = [1, 2, 3]
    Y = [1, 0, 0]
    R = [5, 10, 6]
    mask = [False, False, False]
    X_null = [False, False, False]
    Y_null = [False, False, False]

    W = [1, 1, 1]
    s = _gower_vector(X=X, Y=Y, R=R, W=W, categorical_mask=mask, X_null=X_null, Y_null=Y_null)
    assert_almost_equal(s, (1. + 0.8 + 0.5) / sum(W))

    W = [1, 10, 1]
    s = _gower_vector(X=X, Y=Y, R=R, W=W, categorical_mask=mask, X_null=X_null, Y_null=Y_null)
    assert_almost_equal(s, (1 + 0.8 * 10 + 0.5) / sum(W))


def test_gower_vector_categorical():
    X = [1, 2, 3, 'red', 'green']
    Y = [1, 0, 0, 'blue', 'green']
    R = [5, 10, 6, 1, 1]
    mask = [False, False, False, True, True]
    X_null = [False, False, False, False, False]
    Y_null = [False, False, False, False, False]

    # without variable weights
    W = [1, 1, 1, 1, 1]
    s = _gower_vector(X=X, Y=Y, R=R, W=W, categorical_mask=mask, X_null=X_null, Y_null=Y_null)
    assert_almost_equal(s, (1 + 0.8 + 0.5 + 0 + 1) / sum(W))

    # with variable weights
    W = [1, 10, 1, 1, 5]
    s = _gower_vector(X=X, Y=Y, R=R, W=W, categorical_mask=mask, X_null=X_null, Y_null=Y_null)
    assert_almost_equal(s, (1 + (10 * 0.8) + 0.5 + 0 + (5 * 1)) / sum(W))


def test_gower_vector_missing():
    X = [1, 2, 3, 'red', 'green', np.nan]
    Y = [1, 0, 0, 'blue', 'green', 99]
    R = [5, 10, 6, 1, 1, 100]
    mask = [False, False, False, True, True, False]
    X_null = [False, False, False, False, False, True]
    Y_null = [False, False, False, False, False, False]

    W = [1, 1, 1, 1, 1, 1]
    s = _gower_vector(X=X, Y=Y, R=R, W=W, categorical_mask=mask, X_null=X_null, Y_null=Y_null)
    assert_almost_equal(s, (1 + 0.8 + 0.5 + 0 + 1) / (sum(W) - 1))

    # with variable weights
    W = [1, 10, 1, 1, 5, 1]
    s = _gower_vector(X=X, Y=Y, R=R, W=W, categorical_mask=mask, X_null=X_null, Y_null=Y_null)
    assert_almost_equal(s, (1 + (10 * 0.8) + 0.5 + 0 + (5 * 1)) / (sum(W) - 1))


def test_gower_vector_arraytypes():
    # test pd.Series, np.array, and lists as inputs
    X = [1, 2, 3, 'red', 'green', np.nan]
    Y = [1, 0, 0, 'blue', 'green', 99]
    R = [5, 10, 6, 1, 1, 100]
    mask = [False, False, False, True, True, False]
    X_null = [False, False, False, False, False, True]
    Y_null = [False, False, False, False, False, False]

    for arraytype in [pd.Series, np.array, list]:
        [X, Y, R, mask] = [arraytype(x) for x in [X, Y, R, mask]]

        W = arraytype([1, 1, 1, 1, 1, 1])
        s = _gower_vector(X=X, Y=Y, R=R, W=W, categorical_mask=mask, X_null=X_null, Y_null=Y_null)
        assert_almost_equal(s, (1 + 0.8 + 0.5 + 0 + 1) / (sum(W) - 1))

        # with variable weights
        W = arraytype([1, 10, 1, 1, 5, 1])
        s = _gower_vector(X=X, Y=Y, R=R, W=W, categorical_mask=mask, X_null=X_null, Y_null=Y_null)
        assert_almost_equal(s, (1 + (10 * 0.8) + 0.5 + 0 + (5 * 1)) / (sum(W) - 1))


def test_gower_vector_zero_denom():
    X = [1, 2, 3]
    Y = [1, 2, 3]
    R = [0, 0, 1]  # if R is 0, should not try to divide by 0
    mask = [False, False, False]
    X_null = [False, False, False]
    Y_null = [False, False, False]

    W = [1, 1, 1]
    s = _gower_vector(X=X, Y=Y, R=R, W=W, categorical_mask=mask, X_null=X_null, Y_null=Y_null)
    assert_almost_equal(s, 1)


def test_gower_matrix_precomputed():
    # this test compares the results from the incomplete scikit-learn implementation:
    # https://github.com/scikit-learn/scikit-learn/pull/9555/
    # and our implementation
    X = pd.DataFrame({'age': [21, 21, 19, 30, 21, 21, 19, 30, None],
                      'gender': ['M', 'M', 'N', 'M', 'F', 'F', 'F', 'F', None],
                      'civil_status': ['MARRIED', 'SINGLE', 'SINGLE', 'SINGLE', 'MARRIED',
                                       'SINGLE', 'WIDOW', 'DIVORCED', None],
                      'salary': [3000.0, 1200.0, 32000.0, 1800.0, 2900.0, 1100.0, 10000.0, 1500.0, None],
                      'has_children': [True, False, True, True, True, False, False, True, None],
                      'available_credit': [2200, 100, 22000, 1100, 2000, 100, 6000, 2200, None]})

    # dtype str should not throw an error with null checking (it does with np.nan)
    X['gender'] = X['gender'].astype(str)

    expected = [(1.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000),
                (0.640976, 1.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000),
                (0.329260, 0.303570, 1.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000),
                (0.682126, 0.686123, 0.344719, 1.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000),
                (0.831272, 0.476371, 0.327199, 0.517521, 1.000000, 0.000000, 0.000000, 0.000000, 0.000000),
                (0.473770, 0.832794, 0.303030, 0.518917, 0.642498, 1.000000, 0.000000, 0.000000, 0.000000),
                (0.403021, 0.543998, 0.259572, 0.251814, 0.567627, 0.710125, 1.000000, 0.000000, 0.000000),
                (0.522212, 0.346037, 0.184806, 0.656677, 0.687896, 0.512164, 0.425234, 1.000000, 0.000000),
                (0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 0.000000, 1.000000)]

    expected = pd.DataFrame(expected)

    categorical_features = ['gender', 'civil_status', 'has_children']
    actual = gower_similarity_matrix(df=X, categorical_features=categorical_features, weights=None)
    assert_array_almost_equal(actual, expected)


def test_gower_matrix_comparison_vector():
    X = pd.DataFrame({'age': [21, 21, 19, 30, 21, 21, 19, 30, None],
                      'gender': ['M', 'M', 'N', 'M', 'F', 'F', 'F', 'F', None],
                      'civil_status': ['MARRIED', 'SINGLE', 'SINGLE', 'SINGLE', 'MARRIED',
                                       'SINGLE', 'WIDOW', 'DIVORCED', None],
                      'salary': [3000.0, 1200.0, 32000.0, 1800.0, 2900.0, 1100.0, 10000.0, 1500.0, None],
                      'has_children': [True, False, True, True, True, False, False, True, None],
                      'available_credit': [2200, 100, 22000, 1100, 2000, 100, 6000, 2200, None]})

    Y = pd.DataFrame({'age': [21, 21],
                      'gender': ['M', 'M'],
                      'civil_status': ['MARRIED', 'SINGLE'],
                      'salary': [3000.0, 1200.0],
                      'has_children': [True, False],
                      'available_credit': [2200, 100]})

    # dtype str should not throw an error with null checking (it does with np.nan)
    X['gender'] = X['gender'].astype(str)

    expected = [(1.000000, 0.640976),
                (0.640976, 1.000000),
                (0.329260, 0.303570),
                (0.682126, 0.686123),
                (0.831272, 0.476371),
                (0.473770, 0.832794),
                (0.403021, 0.543998),
                (0.522212, 0.346037),
                (0.000000, 0.000000)]

    expected = pd.DataFrame(expected)

    categorical_features = ['gender', 'civil_status', 'has_children']
    actual = gower_similarity_matrix(df=X, Y=Y, categorical_features=categorical_features, weights=None)
    assert_array_almost_equal(actual, expected)

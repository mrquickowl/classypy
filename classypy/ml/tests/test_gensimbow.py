import numpy as np
import pytest

from classypy.ml.vectorizers import GensimBOW


# init GensimBOW for re-use in tests
@pytest.fixture
def gbow():
    """
    Returns a GensimBOW instance with base settings.
    """
    return GensimBOW()


def test_noniterable_throws_error(gbow):
    list_of_noniterables = [1]
    with pytest.raises(TypeError):
        assert gbow.fit(list_of_noniterables) == 1


def test_input_must_be_list_of_documents(gbow):
    """
    Ensures that a collection of documents is passed
    as input. Each document itself should be a list of tokens.
    """
    iterable = ['Michael', 'Ben']
    with pytest.raises(TypeError):
        assert gbow.fit(iterable) == 0


def test_return_is_iterable(gbow):
    gbow.fit([['']])
    assert iter(gbow.transform([['']]))


@pytest.mark.parametrize("word_list, vec", [
    (['two'] * 2 + ['three'] * 3, [2., 3.]),
    (['one', 'two', 'three'], [1., 1., 1.]),
    (['classy'] * 100, [100.])])
def test_bag_of_words_works(gbow, word_list, vec):
    gbow.fit([word_list])
    result = gbow.transform([word_list])
    result_dict = dict(zip(gbow.id2word.values(), result[0]))

    for word in np.unique(word_list):
        assert result_dict[word] == np.sum(np.asarray(word_list) == word)

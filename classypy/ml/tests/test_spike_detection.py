import numpy as np
import pytest
from numpy.testing import assert_almost_equal, assert_array_almost_equal

from classypy.ml.spike_detection import (detect_spikes, detect_spikes_df, median_absolute_deviation, rolling_mad,
                                         rolling_median)


@pytest.fixture(scope='module')
def arrays():
    arr = np.array([7, 5, 7, 8, 3, 6, 6])
    arr_spikes = np.array([0, 0, 0, 0, 100, 1, 2])
    return {'no_spikes': arr, 'spikes': arr_spikes}


def test_median_absolute_deviation(arrays):
    assert_almost_equal(1.0, median_absolute_deviation(arrays['no_spikes']))
    assert_almost_equal(0.0, median_absolute_deviation(arrays['spikes']))


def test_rolling_median(arrays):
    arr_med = np.array([np.nan, np.nan, 7.0, 7.0, 7.0, 6.0, 6.0])
    assert_array_almost_equal(arr_med, rolling_median(arrays['no_spikes'], 3))

    arr_spikes_med = np.array([np.nan, np.nan, 0.0, 0.0, 0.0, 1.0, 2.0])
    assert_array_almost_equal(arr_spikes_med, rolling_median(arrays['spikes'], 3))


def test_rolling_mad(arrays):
    arr_mad = np.array([np.nan, np.nan, 0.0, 1.0, 1.0, 2.0, 0.0])
    assert_array_almost_equal(arr_mad, rolling_mad(arrays['no_spikes'], 3))

    arr_spikes_mad = np.array([np.nan, np.nan, 0.0, 0.0, 0.0, 1.0, 1.0])
    assert_array_almost_equal(arr_spikes_mad, rolling_mad(arrays['spikes'], 3))


def test_detect_spikes(arrays):
    arr_spikes = np.array([False, False, False, False, False, False, False])
    assert_array_almost_equal(arr_spikes, detect_spikes(arrays['no_spikes'], window_size=3, gain=3))

    arr_spikes_spikes = np.array([False, False, False, False, True, False, False])
    assert_array_almost_equal(arr_spikes_spikes, detect_spikes(arrays['spikes'], window_size=3, gain=3))

    with pytest.raises(ValueError):
        detect_spikes(0, window_size=3, gain=3)


def test_detect_spikes_df(arrays):
    spikes_df = detect_spikes_df(arrays['spikes'], window_size=3, gain=3)
    cols = ('ts', 'median', 'mad', 'median_plus_mad', 'is_spike')
    all_columns_present = [col in spikes_df.columns for col in cols]
    assert all_columns_present, 'Output should have 5 columns: {cols}'.format(cols=cols)

    with pytest.raises(ValueError):
        detect_spikes_df(0, window_size=3, gain=3)
    with pytest.raises(ValueError):
        detect_spikes_df(arrays['spikes'], window_size=1000, gain=3)

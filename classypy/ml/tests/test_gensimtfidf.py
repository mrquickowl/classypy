import pytest
from pytest import approx

from classypy.ml.vectorizers import GensimTfidf


# init GensimTfidf for re-use in tests
@pytest.fixture
def gtfidf():
    """
    Returns a GensimTfidf instance with base settings.
    """
    return GensimTfidf()


@pytest.fixture
def gtfidf_sparse():
    """
    Returns a GensimTfidf instance with use_sparse_representation True.
    """
    return GensimTfidf(use_sparse_representation=True)


def test_noniterable_throws_error(gtfidf):
    list_of_noniterables = [1]
    with pytest.raises(TypeError):
        assert gtfidf.fit(list_of_noniterables) == 1


def test_input_must_be_list_of_documents(gtfidf):
    """
    Ensures that a collection of documents is passed
    as input. Each document itself should be a list of tokens.
    """
    iterable = ['Michael', 'Ben']
    with pytest.raises(TypeError):
        assert gtfidf.fit(iterable) == 0


def test_return_is_iterable(gtfidf):
    gtfidf.fit([['']])
    assert iter(gtfidf.transform([['']]))


def test_tfidf_sparse_works(gtfidf_sparse):
    docs = [['doc', 'this', 'that', 'classy'],
            ['love', 'this', 'classy']
            ]
    gtfidf_sparse.fit(docs)
    result = gtfidf_sparse.transform(docs)
    expected_val = [[(1, 0.7071067811865475), (2, 0.7071067811865475)], [(4, 1.0)]]
    for result_doc, expected_doc in zip(result, expected_val):
        for result_doc_term, expected_doc_term in zip(result_doc, expected_doc):
            assert result_doc_term == approx(expected_doc_term)


def test_tfidf_works(gtfidf):
    docs = [['doc', 'this', 'that', 'classy'],
            ['love', 'this', 'classy']
            ]
    gtfidf.fit(docs)
    result = gtfidf.transform(docs)
    assert result[0].tolist() == approx([0.0, 0.7071067690849304, 0.7071067690849304, 0.0, 0.0])

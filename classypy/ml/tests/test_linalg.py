import numpy as np
import pytest
from numpy.testing import assert_array_almost_equal

from classypy.ml.linalg import rolling_window


@pytest.fixture(scope='module')
def array():
    array = np.array([1, 2, 3, 4, 5, 6])
    return array


def test_rolling_window(array):
    arr_1 = np.array([[i] for i in array])
    assert_array_almost_equal(arr_1, rolling_window(array, 1))

    arr_2 = np.array([[1, 2], [2, 3], [3, 4], [4, 5], [5, 6]])
    assert_array_almost_equal(arr_2, rolling_window(array, 2))

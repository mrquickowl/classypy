import nltk
import numpy as np
import pytest
from numpy.testing import assert_almost_equal, assert_array_almost_equal, assert_equal
from scipy import stats
from sklearn.exceptions import NotFittedError

from classypy.devops import logger
from classypy.ml.transformers import BoxCoxTransformer, TextProcessor


# init TextProcessor for re-use in tests
@pytest.fixture
def processor():
    """
    Returns a TextProcessor instance with base settings.
    """
    # This can be done here, as these files are cached (don't re-download)
    # Localizing here also gets this for anybody running tests.
    # This would get weird, however, for parallel tests where this library doesn't exist.
    nltk.download('stopwords', quiet=not logger.logsLevel(logger.DEBUG))  # 11K
    nltk.download('averaged_perceptron_tagger', quiet=not logger.logsLevel(logger.DEBUG))  # 2MB
    nltk.download('wordnet', quiet=not logger.logsLevel(logger.DEBUG))  # 10MB
    return TextProcessor()


@pytest.fixture
def X_2d():
    X_2d = np.random.random((1000, 30))
    X_2d = np.abs(X_2d)
    return X_2d


@pytest.fixture
def X_1d():
    return np.abs(np.random.random((1000, 1)))


@pytest.fixture
def bct():
    return BoxCoxTransformer()


def test_string_of_stopwords_should_return_empty_string(processor):
    stopwords = ['you is all just being its', 'i']
    assert processor.transform(stopwords) == [[], []]


def test_punctuation_should_be_removed_from_string(processor):
    punct_str = ['th!s. i$. @n. 3x@mpL3?']
    assert processor.transform(punct_str) == [['ths', 'i', 'n', 'xmpl']]


def test_transform_return_should_always_be_list(processor):
    assert processor.transform([]) == []


def test_lemmatizer_works():
    processor_stem = TextProcessor(do_lemmatize=True)
    result = processor_stem.transform(["gardeners"])
    assert result[0][0] == 'gardener'


def test_stemmer_works():
    processor_stem = TextProcessor(do_stem=True)
    result = processor_stem.transform(["cats"])
    assert result[0][0] == 'cat'


def test_lemmatizer_and_stemmer_works():
    processor_stem = TextProcessor(do_lemmatize=True, do_stem=True)
    result = processor_stem.transform(["gardeners"])
    assert result[0][0] == 'garden'


def test_boxcox_transformer_notfitted(bct, X_1d):
    with pytest.raises(NotFittedError):
        bct.transform(X_1d)

    with pytest.raises(NotFittedError):
        bct.inverse_transform(X_1d)


def test_boxcox_transformer_1d(bct, X_1d):
    X_trans = bct.fit_transform(X_1d)
    X_expected, lambda_expected = stats.boxcox(X_1d.flatten())

    assert_almost_equal(X_expected.reshape(-1, 1), X_trans)
    assert_almost_equal(X_1d, bct.inverse_transform(X_trans))
    assert_almost_equal(lambda_expected, bct.lambdas_[0])
    assert_equal(len(bct.lambdas_), X_1d.shape[1])
    assert isinstance(bct.lambdas_, np.ndarray)


def test_boxcox_transformer_2d(bct, X_2d):
    X_trans = bct.fit_transform(X_2d)
    for j in range(X_trans.shape[1]):
        X_expected, lmbda = stats.boxcox(X_2d[:, j].flatten())
        assert_almost_equal(X_trans[:, j], X_expected)
        assert_almost_equal(lmbda, bct.lambdas_[j])
    assert_equal(len(bct.lambdas_), X_2d.shape[1])
    assert isinstance(bct.lambdas_, np.ndarray)


def test_boxcox_negative_exception(bct, X_2d):
    X_with_negatives = X_2d - X_2d.mean()

    with pytest.raises(ValueError):
        bct.transform(X_with_negatives)

    with pytest.raises(ValueError):
        bct.fit(X_with_negatives)

    with pytest.raises(ValueError):
        bct.fit(np.zeros(X_2d.shape))


def test_boxcox_different_shape_exception(bct, X_2d):
    bct.fit(X_2d)

    with pytest.raises(ValueError):
        bct.transform(X_2d[:, 0:1])


def test_boxcox_inverse_and_zero_case(bct, X_2d):
    X_trans = bct.fit_transform(X_2d)

    X_inv = bct.inverse_transform(X_trans)
    assert_array_almost_equal(X_inv, X_2d)

    X_2d = np.abs(X_2d)[:, 0:1]
    bct.lambdas_ = np.array([0])
    X_trans = bct.transform(X_2d)
    assert_array_almost_equal(bct.inverse_transform(X_trans), X_2d)

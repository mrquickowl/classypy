import os.path as op
import tempfile

import nltk
import pytest
import six

from classypy.ml.text_pipeline import process_and_vectorize_pipeline
from classypy.ml.topics import GensimLsi
from classypy.ml.transformers import TextProcessor
from classypy.ml.vectorizers import GensimTfidf

from .docs import anti_abortion, gorilla, kony

pytestmark = pytest.mark.skipif(six.PY3, reason="gensim is Python2 only.")

ID2W_PATH = op.join(tempfile.mkdtemp(), 'id2word_path.pkl')
TFIDF_PATH = op.join(op.dirname(ID2W_PATH), 'tfidf_path.pkl')


@pytest.fixture
def data_tfidf(id2word_path=ID2W_PATH, tfidf_path=TFIDF_PATH):
    """
    Sets up data to be fed to GensimLsi.
    """
    nltk.download('stopwords')  # 11K
    pipe = process_and_vectorize_pipeline(TextProcessor(),
                                          GensimTfidf(use_sparse_representation=True))
    data = [anti_abortion, gorilla, kony]
    pipe.fit(data)
    data_tfidf = pipe.transform(data)
    pipe.named_steps['vectorize'].save(tfidf_path, id2word_path)
    return data_tfidf


@pytest.fixture
def glsi(data_tfidf):
    """
    Returns an initialized GensimLsi instance.
    """
    return GensimLsi(id2word_path=ID2W_PATH)


def test_init_fails_without_id2word():
    with pytest.raises(TypeError):
        assert GensimLsi()


def test_class_mutates_internal_state_num_topics(glsi, data_tfidf):
    n_topics = 3
    glsi.fit(data_tfidf, num_topics=n_topics)
    assert glsi.model.num_topics == n_topics


def test_expected_words_in_topics(glsi, data_tfidf):
    glsi.fit(data_tfidf, num_topics=2)
    topics = glsi.model.print_topic(topicno=0)
    assert 'gorilla' in topics


def test_embed_len_equals_num_topics(glsi, data_tfidf):
    n_topics = 2
    glsi.fit(data_tfidf, num_topics=n_topics)
    assert len(glsi.model.show_topics()) == n_topics


def test_init(glsi):
    glsi.fit

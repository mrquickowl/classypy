#!/usr/bin/env python
# *- encoding: utf-8 -*-

anti_abortion = u"""
Just as racism and sexism are 'beliefs,' abortion is a 'belief' that diminishes and destroys the humanity of another
class of human beings. We covered that in part one yesterday. But before we can see how that's really true, we have to
get into the science.
What Science Tells Us
You can pretend preborn babies aren't human beings, but science proves otherwise, repeatedly showing that a preborn
humanis a unique individual from his mother. He has his own circulatory system, his own organs, his own blood type, and
 his own fingerprints. From the moment of conception — which has been described as a flash of light, a burst of
 fluorescence, or the “zinc spark” by scientists –he was his own person, separate from his mother, and very much alive.
How do we know he is alive? For one, he has a heartbeat (new science says as early as 16 days after conception), but in
addition to that, he is growing. Non-living things don't grow. Living things do. It's as simple as that.
If abortion was all about a woman's body, then she would be aborting herself. There's a reason aborted babies are
sometimes born alive – because they were alive to begin with. You can't kill something; you can't poison something or
 someone to death, starve it, or stop its heartbeat if it was never alive to start.
Back in 1973, when abortion became legal in the United States, ultrasounds weren't a regular part of prenatal care. We
 couldn't see inside the womb as we can today. So the argument that a preborn person is a “cluster of cells” or a “ball
 of tissue” was easily digested by the American public. Coming off the 'free love' '60s, many wanted the freedom of sex
 without the responsibility of children (which is, as a belief, impossible because the two cannot be separated).
The idea that preborn humans are just “tissue” was proven wrong as soon as the window to the womb was opened for us.
Yes, at the first stages, a preborn person is a group of cells, but he will continue to be a group of cells for the rest
of his life. Just as you are. Every single human being is made up of a group of cells and his own unique DNA. It takes
only a matter of days for that group of cells to come together to look like a human being and have a beating heart.
Human life, however, begins well before we 'look' fully like the human beings we are today.
How History and Science Prove Abortion Is Wrong
*Warning: Graphic photo of human rights abuse below.
Go back to part one in this series to review the history. We know that science proves that a preborn child is a living
human being (after all, if he isn't human, what is he?), and past human rights abuses show us that believing that a
particular group of people is inferior is completely unacceptable and always on the wrong side of history. Holding
people as slaves is wrong. Killing people for their beliefs is wrong. Beating women is wrong.
Why do people see abortion as right?
All a 'pro-choicer' would have to do is pay attention, watch the videos of what abortion does, and educate himself.
Blindly following the abortion party line will simply lead to more oppression and discrimination against an entire
group of people by way of abortion.
Abortion is an act of violence against another human being simply because that human being is deemed “unwanted” or an
“inconvenience” or “disabled” or “not conscious of his own life.” The reason or justification doesn't matter. Abortion
is still wrong. Otherwise, any of us who felt unwanted or like an inconvenience, or who had a disability, or were simply
sleeping or blacked out from drinking alcohol, wouldn't have the right to life.
In the video below, former abortionist Dr. Anthony Levatino shows us how the abortion pill works. What some people
currently see as not a big deal is actually quite awful, as the preborn child is starved of nutrients and oxygen until
he dies. (Information on reversing these abortions is here.)
In this next video, Dr. Levatino describes how a second-trimester D&E abortion is performed. It's cruel. It's gruesome.
But D&Es are the most common form of second-trimester abortion in the U.S. They happen every day, and not only does our
government approve them, but some politicians want us to pay for it.
Just because preborn human beings can't stand up for themselves and can't speak to say that their lives matter, too,
doesn't mean it is right or acceptable for us to see them as inferior and kill them for it.
Today's Hitler is the abortion industry, and today's slaves are the preborn children, who are seen as unworthy of rights
because of their age and location. What abortion does to a preborn human would never be allowed or accepted if it were
done to an animal, never mind a born person. We can't continue to say abortion is just a 'belief' and look the other
way. After all, hate crimes are fueled by a person's beliefs, and we know how the majority of Americans – and you and
me, personally – feel about hate crimes.
# *- encoding: utf-8 -*-
""".encode('utf-8')

gorilla = u"""
Harambe, the 17-year-old Western lowland gorilla shot dead at the Cincinnati Zoo late last month after a 3-year-old boy
fell into his enclosure, may be physically gone, his tissues harvested for research and his sperm extracted to help
diversify the captive breeding gene pool.
Yet the 440-pound silverback leaves another metaphorical gorilla in the room, raising questions that extend far beyond
the particulars of the case, including whether the zoo or the boy's mother were more to blame for Harambe's death.
For primatologists and conservationists who devote their lives to studying the great apes and to doing what they can to
help protect the rapidly vanishing populations of the primates in the wild, a linked set of ethical and practical
dilemmas looms almost unbearably large.
As research continues to reveal the breadth of our genetic, emotional and cognitive kinship with the world's four great
apes — gorillas, chimpanzees, bonobos and orangutans — many primatologists admit to feeling frankly uncomfortable at
the sight of a captive ape on display, no matter how luxe or “natural” the zoo exhibit may be.
“When I visit zoos, I have to turn off my feelings and just tell myself that I am at a museum admiring nature's
masterpieces,” said the primatologist Sarah Blaffer Hrdy, professor emerita at the University of California, Davis.
“Otherwise, I can't really justify keeping great apes in cages.”
At the same time, researchers acknowledge that apes in today's zoos, at least in the industrialized world, were all born
and raised in captivity, and could no more survive being “set free” into the forests of Africa or Indonesia than could
the average tourist on safari.
Catherine Hobaiter of St. Andrews University in Scotland, who studies chimpanzees in Uganda, described the reaction of
zoo gorillas that had been raised in indoor enclosures when the zoo finally added an outdoor annex to the exhibit.
“It was heartbreaking to see,” she said. “The gentlest specks of rain, and the gorillas were drumming on the door to get
back inside. They were afraid of getting wet.”
Yet while primatologists concur that people have a moral obligation to care for the thousands of apes who are now in
captivity and may live 60 years or longer, they differ on what that care should look like.
Barbara Smuts, a renowned primatologist at the University of Michigan, recently distributed a petition asking that the
other gorillas at the Cincinnati Zoo be relocated to a sanctuary far from the ogling, screeching crowds of their clothed
relations.
Researchers also disagree on whether we should continue breeding apes in captivity, and if so, to what end. Some experts
believe that well-designed zoos play an essential educational role, and that exposure to a flesh-and-blood ape can be a
transformative experience, especially for children.
“I remember going to the Milwaukee zoo when I was a kid and seeing the gorilla,” said Peter D. Walsh, a biological
anthropologist at Cambridge University who works on gorilla conservation in Africa. “I was rapt. It's like a drug.
You don't get that emotional bond from an IMAX movie.”
Others deride most zoos as little more than amusement parks with educational placards that few people bother to read.
“There's no good evidence that captive apes are having any positive effect on their wild relatives,” said Marc Bekoff,
a behavioral ecologist and professor emeritus at the University of Colorado. As for education, he added, “one of the
most wonderful and educational lessons in biodiversity I've ever seen was a snail exhibit at the Detroit Zoo.”
Peter Singer, a bioethicist at Princeton University, said, “Our primary concern ought to be the well-being of gorillas,
but zoos are constructed the other way around: The primary concern is that humans can see the gorillas.”
No matter their feelings about zoos, primatologists despair at the shocking statistics on wild apes.
According to the IUCN Red List of Threatened Species, all species and subspecies of wild apes rank as endangered or
critically endangered, and in all cases, the trends point implacably downward. Apes are being lost to poaching, the
bushmeat trade, habitat destruction and disease.
In Sumatra and Borneo, forests have been pulped to make way for palm oil plantations, with devastating consequences for
orangutans. Since the 1990s, 80 percent of Eastern lowland gorillas in Central Africa have died of Ebola.
Even among Jane Goodall's celebrated chimpanzees of the Gombe forest in Tanzania, human activities have lately slashed
the population by almost 40 percent.
By the Red List reckoning, the planetwide total for all wild apes amounts to 350,000 individuals, down from premodern
figures estimated in the millions.
Nor does it help, said Dr. Walsh of Cambridge University, that the public's concern for the environment is now focused
almost exclusively on climate change: “I feel like shouting, 'Hey, guys, you could end climate change tomorrow and we'd
still be facing the greatest extinction crisis we've ever seen.'”
A Meeting of Relatives
There's a reason humans and the great apes are bunched together taxonomically in the family Hominidae. We split off from
chimpanzees and gorillas only about six million to 10 million years ago. The DNA of a chimpanzee is about 98 percent
analogous to ours.
Apes are avid tool users and tool makers. Chimpanzees fashion sticks to fish termites from a mound and to hunt monkeys
hidden in tree holes. Orangutans can learn to row a boat and flip pancakes on a griddle.
In observations at the Prague Zoo, Khalil Baalbaki watched gorillas turn empty crates into a series of household
objects: tables, chairs, stepping stools to extend their reach, trays to carry their food and as weapons to be thrown in
a fight. One young female gorilla extracted wood stuffing from a crate to fashion a pair of slippers, padding her feet
with the fibers before venturing onto the snow.
According to meta-analyses of intelligence studies, the average ape has the cognitive, quantitative and spatial skills
of a 2½- to 4-year-old human child. Yet Tetsuro Matsuzakawa's laboratory in Japan showed that an exceptionally
sharp-witted chimpanzee named Ayuma was twice as good as any university student at recalling numbers flashed on a
screen.
The great apes also exhibit basic temperamental differences. David Watts, a primatologist at Yale University who has
studied chimpanzees and gorillas in the wild, found that while chimpanzees generally didn't like people or show much
nterest in their affairs, gorillas were deeply curious.
“I quickly realized that the gorillas not only wanted to touch me, but to climb all over me,” he said. In one famous
incident, a female gorilla stuck her hand down the shirt of a female primatologist and started feeling around.
That innate curiosity, researchers suggest, may explain some of Harambe's behavior seen on the video of his fatal
encounter with the boy who fell into his enclosure — fiddling with the boy's clothing, taking a quick peek as he
pulled the boy's pants upward. He tried pulling the boy into a grotto, perhaps to protect him or to claim the
fascinating ew playmate for himself.
But with the mounting commotion and screams from the onlookers above, researchers said, Harambe grew agitated and soon
assumed the stance of a male silverback in dominance display mode.
“It's what we used to call strutting, and male gorillas do it all the time,” Dr. Watts said. “A silverback will stand
or walk around with arms and legs stiffly extended, his hair piloerect, to make himself look bigger and more
impressive. Harambe was definitely doing that when he was standing over the boy.”
The behavior is mostly bluster: If Harambe had been intent on killing the boy, Dr. Hrdy said, as an interloping male
gorilla might kill the babies sired by the silverback he just deposed — the quicker to claim the resident females
for himself — “he would have done it in seconds,” probably with a bite to the skull.
Nevertheless, the strut introduced risks of its own, particularly when Harambe began dragging the boy around the
enclosure, as a displaying gorilla will sometimes drag around a large branch.
Dr. Watts, who said he had been “punched, knocked over and dragged” by male gorillas but never seriously injured,
wishes he had been at the Cincinnati Zoo as the crisis unfolded. He would have volunteered to enter the enclosure
and assume a submissive fetal position on the floor to try drawing the gorilla's attention from the boy. (He admits
he is engaging in a kind of Monday morning strut-display of his own.)
A Welcoming, Limiting Captivity
The look and logic of zoos have changed drastically over time. When the first apes were exhibited in the West, in the
late 18th century, they were seen as trophies, evidence of imperial victory over savagery. The unfortunate souvenirs
usually died within months of their arrival from disease or malnutrition.
As zoos sought to improve the health of their resident apes, the enclosures often assumed a blandly sterile
configuration, devoid of risky foliage or toys. That approach led to problems of its own, like boredom, repetitive
behaviors and depression.
More recently, most zoos have worked hard to give apes the mental and emotional stimulation they need, with tires for
swinging, rocks for climbing, social groups for mutual grooming or bouts of contagious laughing or yawning.
Frans de Waal of Emory University and the Yerkes National Primate Research Center said he was a “big fan” of quality
zoos, although perhaps not for large, gregarious animals like killer whales and elephants. “But for great apes, the
record now is excellent,” he said.
Their health is good, they reproduce readily in captivity and they live 10 years or more longer than their wild peers.
Indeed, the first gorilla born in captivity, a female named Colo, is still alive at the zoo in Columbus, Ohio, for
which she was named. She will turn 60 in December, a birthday the great-great grandmother will not celebrate, zoo
officials said, by wearing the adorable pinafore and straw bonnet her caretakers dressed her in as a youth.
Dr. de Waal said that it was easier than ever to keep apes enriched and entertained.
“They like to work on computers,” he said. “When you bring in a touch screen, they get excited about that, and it's a
great way to teach the public about how smart they are.”
But what the public must accept, he said, is that the pleasant notion of zoos as nurseries for restocking wild
populations of endangered animals has proved a fantasy in all but a handful of cases, most notably the successful
reintroduction of zoo-bred golden lion tamarins into the Atlantic Forests of Brazil.
By contrast, when the British aristocrat Damian Aspinall released 11 of his captive-bred lowland gorillas into the
wilds of Gabon in 2014, five were soon violently dispatched, probably by a resident gorilla, while others disappeared.
Yet critics say that zoo life has its serious downsides, too, as Harambe's story made plain.
The captive ape is the designated “ambassador” for its kind, an object lesson in evolutionary fraternity and shared fate
for those of us who remain on the other side of the glass, proclaiming the primacy of human needs, desires and lives.
# *- encoding: utf-8 -*-
""".encode('utf-8')

kony = u"""
A member of the United States Army Special Forces speaking with troops from the Central African Republic and Uganda who were searching for Joseph Kony, leader of the Lord's Resistance Army, in Obo, Central African Republic, in 2012. Credit Ben Curtis/Associated Press
LILONGWE, Malawi — A contingent of Ugandan and American military officials — and a handful of journalists — will board a plane on Tuesday in the Ugandan city of Entebbe for an approximately two-and-a-half-hour flight.
Destination: the remote town of Obo, in the southeastern part of Central African Republic, where they will take part in a ceremony organized by Uganda to mark the end of the mission to capture or kill Joseph Kony, the notorious leader of the Lord's Resistance Army, or L.R.A.
Mr. Kony was, of course, never captured or killed.
The United States spent almost $800 million on the effort since 2011, when President Barack Obama deployed Special Operations forces to the region to provide advisory support, intelligence and logistical assistance to African Union soldiers fighting the Lord's Resistance Army. Officials from the countries involved say they have significantly degraded the L.R.A., diminishing it to around 100 people today from a fighting force of 3,000. Now, they say, it's time to go home.
Still, when Ugandan officials informed their American counterparts that they wanted to have a ceremony to celebrate the end of the mission, some Defense Department officials at the Pentagon, mindful of “mission accomplished” ceremonies that can come back to bite celebrants, were wary. But the Ugandans were insistent, defense officials said, and the Americans offered up Brig. Gen. Kenneth H. Moore, the deputy commanding general of United States Army Africa, along with the American ambassador to the Central African Republic, Jeffrey Hawkins, to carry the flag.
The United States and the Ugandan military decided to end their search for Mr. Kony in late April, abandoning the international effort to bring him to justice. In that effort, Ugandan soldiers are accused of leaving behind their own trail of abuse, according to the United Nations peacekeeping mission, including rape, sexual slavery and the exploitation of young girls.
Continue reading the main story
Advertisement
Continue reading the main story
In Obo, the American military has already begun the so-called retrograde of its mission, which is Pentagon-speak for packing up shop and heading home, taking everything they brought with them. That means sending in planes and vehicles to haul away light infantry tents, cots, communications equipment and all of the weaponry used by the American Special Operations and Ugandan forces trying to hunt down the elusive Mr. Kony and his guerrillas.
Publicly, American officials insist the ceremony is not ill-advised, even if Mr. Kony reappears at some point, as he has done in the past. But they were not rushing to publicize it either; one American defense official attending the African Land Forces Summit meeting last week in Lilongwe, Malawi's capital, looked pained when a reporter asked about the planned ceremony. “Who told you about this?” he asked.

# *- encoding: utf-8 -*-
""".encode('utf-8')

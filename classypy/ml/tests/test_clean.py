from classypy.ml.nlp import TextCleaner


def test_remove_html():
    assert TextCleaner.clean_text("<b>Here is some HTML.</b>") == "Here is some HTML."


def test_remove_punctuation():
    test_string = "<b>Here is some HTML. It also contains punctuation!</b>"
    assert TextCleaner.clean_text(
        test_string, remove_punctuation=True) == "Here is some HTML It also contains punctuation"

    punctuation_string = "!$?,^punc"
    assert TextCleaner.clean_text(punctuation_string, remove_punctuation=True) == " punc"

    punctuation_string = "!$?,^punc"
    assert TextCleaner.clean_text(punctuation_string, remove_punctuation=False) == "!$?, punc"


def test_force_lower():
    test_string = "This String Should Become Lowercase"
    assert TextCleaner.clean_text(test_string, lowercase=True) == "this string should become lowercase"


def test_remove_hyperlinks():
    for test_string in ["http://someurl.com/home See here",
                        "https://someurl.com/home/embedded_link See here",
                        "http://www.someurl.org See here",
                        "www.someurl See here",
                        "test.com See here",
                        "test.org See here",
                        "test.net See here",
                        "test.edu See here"]:
        assert TextCleaner.remove_hyperlinks(test_string) == " See here"


def test_normalize_spacing():
    test_string = "Sentence1.Sentence2."
    assert TextCleaner.normalize_spacing(test_string) == "Sentence1. Sentence2."

    test_string = "Sentence1?Sentence2! Sentence3."
    assert TextCleaner.normalize_spacing(test_string) == "Sentence1? Sentence2! Sentence3."

    test_string = "word1.   word2"
    assert TextCleaner.normalize_spacing(test_string) == "word1. word2"

    test_string = "Sentence1?Sentence2! Sentence3."
    assert TextCleaner.clean_text(
        test_string, normalize=True, remove_punctuation=True) == "Sentence1 Sentence2 Sentence3"

    test_string = "Sentence1?<br>Sentence2!Sentence3."
    assert TextCleaner.clean_text(test_string, normalize=True) == "Sentence1? Sentence2! Sentence3."

    test_string = "What?!Disbelief."
    assert TextCleaner.clean_text(test_string, normalize=True) == "What? Disbelief."


def test_remove_media_refs():
    test_string = "#Hashtag @User"
    assert TextCleaner.clean_text(test_string) == "Hashtag User"


def test_remove_special_characters():
    test_string = "(Parenthetical aside.)Back to the main point."
    assert TextCleaner.clean_text(test_string) == "Parenthetical aside. Back to the main point."

    test_string = "That is /*ridiculous*/. No, [this] is."
    assert TextCleaner.clean_text(test_string) == "That is ridiculous. No, this is."

    test_string = "rnrnOther&&& characters"
    assert TextCleaner.clean_text(test_string) == "Other characters"


def test_to_ascii():
    test_strings = ['\x15test', '\x00test', "\xa0test", u'\ua000test']

    for ts in test_strings:
        assert TextCleaner.clean_text(ts) == "test"

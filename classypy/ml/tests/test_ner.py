"""
Tests for Named Entity Recognizer class.
"""
import nltk
import pytest
from sklearn.pipeline import Pipeline

from classypy.devops import logger
from classypy.ml.nlp.named_entity_rec import NER
from classypy.ml.vectorizers import GensimTfidf

pytestmark = pytest.mark.skip("# 2017-09-19: skipping gensim tests due to bitbucket memory issues.")


def check_for_en():
    """
    Checks if en has been downloaded.
    If not, it downloads it.
    """
    if not NER().nlp.__dict__['matcher']:
        import spacy
        spacy.cli.download('en')


@pytest.fixture
def ner():
    """
    Returns an initialized NER instance.
    """
    # if data not downloaded, download it
    check_for_en()
    nltk.download('punkt', quiet=not logger.logsLevel(logger.DEBUG))
    return NER()


def test_pipeline(ner):
    """
    Sets up data to be fed to GensimTfidf.
    """
    pipe = Pipeline([
        ('ner', NER()),
        ('tfidf', GensimTfidf())])

    data = ["I work at Classy. Classy is in San Diego. San Diego is in the USA."]
    pipe.fit(data)
    data_tfidf = pipe.transform(data)
    assert data_tfidf


def test_transform_fails_if_documents_not_in_list(ner):
    with pytest.raises(ValueError):
        assert ner.transform("Document one")


def test__transform_sentence(ner):
    sent = "My name is Ben. I work at Classy."
    result = ner._transform_sentence(sent)
    assert result == [(u'Ben', u'PERSON'), (u'Classy', u'ORG')]


def test_transform(ner):
    assert ner.transform(["America."]) == [["America"]]


def test_transform_returns_list_of_lists(ner):
    result = ner.transform(["America"])
    assert isinstance(result[0], list)


def test__transform_sentence_returns_list_of_tuples(ner):
    result = ner._transform_sentence(["America"])
    assert isinstance(result[0], tuple)

"""
Class wrappers for gensim's doc2vec and word2vec, all inheriting from sklearn BaseEstimators
and TransformerMixin.
"""
import os.path as op

from sklearn.base import BaseEstimator, TransformerMixin


class GensimDoc2Vec(BaseEstimator, TransformerMixin):
    """
    Custom sklearn transformer that converts tokenized,
    preprocessed data into doc2vec format, and trains
    a doc2vec model on said data.
    """
    def __init__(self, doc2vec_path=None, word2vec_path=None):
        """
        Instantiate GensimDoc2Vec object.

        Parameters
        ----------
        doc2vec_path : str
            Path to location of saved gensim doc2vec Model.
            If specified, the model will load and use this object as its doc2vec model.
        word2vec_path : str
            Path to location of pretrained word2vec embeddings.
            This is required if you plan to call `pretrain=True` in the fit method.
            Visit https://code.google.com/archive/p/word2vec/ and go to P're-trained
            word and phrase vectors' to obtain pretrained word2vec embeddings.
        """
        self.model = None
        # if path specified, load doc2vec model
        if doc2vec_path:
            self.model = GensimDoc2Vec.load(doc2vec_path=doc2vec_path)
        self.word2vec_path = word2vec_path

    @staticmethod
    def load(doc2vec_path):
        """
        Loads gensim.doc2vec model located at doc2vec_path

        Parameters
        ----------
        doc2vec_path: str
            File path to trained gensim doc2vec model.
        """
        from gensim.models import Doc2Vec
        if not op.exists(doc2vec_path):
            raise IOError('The provided file path to the doc2vec model was not found.'
                          ' Please ensure that the argument is the correct path.')
        return Doc2Vec.load(doc2vec_path)

    def save(self, doc2vec_path):
        """
        Saves self.model to doc2vec_path.
        If no model exists, an AttributeError is raised.

        Parameters
        ----------
        doc2vec_path: str
            File path to save trained gensim doc2vec model.
        """
        if not self.model:
            raise AttributeError('Nothing to save yet, please run .fit first.')
        self.model.save(doc2vec_path)

    def fit(self, documents, labels=None, epochs=10, **kwargs):
        """
        Instantiates a gensim doc2vec model, and fits it to `documents`.

        Parameters
        ----------
        documents: iterable
            List of documents. Each document must be a list of preprocessed tokens.
        labels: iterable
            List of document names. If None, uses integer as index value of documents.
        epochs: int (default=10)
            Number of epochs to train doc2vec model. Typical values range 5-20.
        kwargs:
            Various parameters for Doc2Vec model.
            Visit https://radimrehurek.com/gensim/models/doc2vec.html for details.

        Returns
        -------
            Trained gensim.doc2vec model, stored in self.model.
        """
        from gensim.models import Doc2Vec
        from gensim.models.doc2vec import TaggedDocument
        if labels is None:
            labels = list(range(len(documents)))
        # create TaggedDoument, to be fed into doc2vec model
        taggedDocuments = [TaggedDocument(words=documents[i], tags=[labels[i]]) for i in range(len(documents))]
        # init Doc2Vec model with arguments specified in kwargs
        self.model = Doc2Vec(iter=epochs, **kwargs)
        # build model vocab
        self.model.build_vocab(taggedDocuments)
        # fit pretrained word2vec embeddings if desired
        if self.word2vec_path:
            self.model.intersect_word2vec_format(self.word2vec_path, binary=True)
        # train model
        self.model.train(taggedDocuments, total_examples=self.model.corpus_count, epochs=self.model.iter)

        return self

    def transform(self, documents):
        """
        Embeds each document in documents into the doc2vec space.

        Parameters
        ----------
        documents: iterable
            List of documents. Each document must be a list of preprocessed tokens.

        Returns
        -------
            vecs: List of doc2vec embeddings, corresponding to each document in
                `documents`.
        """
        if self.model is None:
            raise AttributeError('Must have a fit model in order'
                                 ' to call transform. Run .fit'
                                 ' or load pre-trained model '
                                 'before calling .transform.')
        return [self.model.infer_vector(doc) for doc in documents]


class GensimWord2Vec(BaseEstimator, TransformerMixin):
    """
    Custom sklearn transformer that converts tokenized, preprocessed data into
    word2vec format, and trains a word2vec model on said data.
    """
    def __init__(self, word2vec_path=None):
        """
        Instantiate GensimWord2Vec object.

        Parameters
        ----------
        word2vec_path : str
            Path to location of saved gensim word2vec Model. If specified, the model
            will load and use this object as its word2vec model.
        """
        self.model = None
        # if word2vec_path specified, load model
        if word2vec_path:
            self.model = GensimWord2Vec.load(word2vec_path=word2vec_path)

    @staticmethod
    def load(word2vec_path):
        """
        Loads gensim.word2vec model located at word2vec_path

        Parameters
        ----------
        word2vecpath: str
            File path to trained gensim word2vec model.
        """
        from gensim.models import KeyedVectors
        if not op.exists(word2vec_path):
            raise IOError('The provided file path to the word2vec model was not found.'
                          'Please ensure that the argument is the correct path.')
        return KeyedVectors.load(word2vec_path)

    def save(self, word2vec_path):
        """
        Saves self.model to word2vec_path.
        If no model exists, an AttributeError is raised.

        Parameters
        ----------
        word2vecpath: str
            File path to save gensim word2vec model.
        """
        if not self.model:
            raise AttributeError('Nothing to save yet, please'
                                 ' run .fit first, or specify'
                                 ' a pre-trained model to load.')
        self.model.save(word2vec_path)

    def fit(self, documents, pretrain_path=None, **kwargs):
        """
        Instantiates a gensim word2vec model, and fits it to `documents`.

        Parameters
        ----------
        documents: iterable
            List of documents. Each document must be a list of preprocessed tokens.
        pretrain_path : str
            Path to location of pretrained word2vec embeddings.
            This is required if you plan to call `pretrain=True` in the fit method.
            Visit https://code.google.com/archive/p/word2vec/ and go to P're-trained
            word and phrase vectors' to obtain pretrained word2vec embeddings.
        kwargs:
            Various parameters for Word2Vec model.
            Visit https://radimrehurek.com/gensim/models/word2vec.html for details.

        Returns
        -------
            Trained gensim.word2vec model, stored in self.model.
        """
        from gensim.models import KeyedVectors, Word2Vec

        # Create model from pretrained embeddings
        if pretrain_path is not None:
            self.model = KeyedVectors.load_word2vec_format(pretrain_path, binary=True)
        # else train model on internal data
        else:
            self.model = Word2Vec(sentences=documents, **kwargs)

        # if done training, make model more memory-efficient
        self.model.init_sims(replace=True)
        return self

    def transform(self, documents):
        """
        Embeds each document in documents into the trained word2vec space.

        Parameters
        ----------
        documents: iterable
            List of documents. Each document must be a list of preprocessed tokens.

        Returns
        -------
            vecs: List of lists of word2vec embeddings, corresponding to
                each document in `documents`.
        """
        if self.model is None:
            raise AttributeError('Must have a fit model in order'
                                 ' to call transform. Run .fit'
                                 ' or specify pre-trained model'
                                 ' before calling .transform.')
        vecs = []
        for doc in documents:
            vec = [self.model[word] for word in doc if word in self.model]
            vecs.append(vec)
        return vecs

def sum_list(arr):
    """This function sums all the values in arr and returns one value

    Parameters
    ----------
    arr : array-like
        array containing numeric values

    Return:
    -------
    sum_arr : float
        The sum of values in arr
    """
    try:
        arr = [float(x) for x in arr]  # ensure arr is filled with numeric variables
        sum_arr = sum(arr)
        return sum_arr
    except TypeError:
        return arr


def sum_list_dict(arr, sum_key):
    """This function sums values in each dictionary within arr

    Parameters
    ----------
    arr : array-like
        An array composed of dictionary objects
    sum_key : str
        A key in each dictionary in arr

    Return:
    -------
    list_sum : float
        The sum of values in each dictionary in arr
    """
    sum_list = []
    try:
        for dictionary in arr:
            sum_list.append(dictionary[sum_key])
        list_sum = sum_list(arr=sum_list)
        return list_sum
    except TypeError:
        return 0


def add_prefix(text, prefix):
    """This function prepends text with prefix.

    Parameters
    ----------
    text : str
        A generic dataframe object
    prefix : str
        A string containing the prefix all columns in df will be appended by

    Return:
    -------
    new_text : str
        The text value prepended with prefix
    """
    if prefix != "" and prefix:
        new_text = prefix + "_" + text

        return new_text
    else:

        return text

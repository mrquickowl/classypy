from datetime import datetime
from random import choice

import numpy as np
import pandas as pd

import classypy.ml.feature_transformers as ft


def get_random_junk_sentence():
    """Returns a randomly generated sentence based on manually entered part of speech lists"""
    nouns = ["puppy", "car", "rabbit", "girl", "monkey", "jerry"]
    verbs = ["runs", "hits", "jumps", "drives", "barfs", "skips"]
    adv = ["crazily.", "dutifully'", "foolishly;", "merrily(", "occasionally)", "sadly!", "happily!"]
    adj = ["adorable", "clueless", "dirty", "odd", "stupid", "happy", "joyful"]
    punc = ["!", "?", ".", "-", ":"]
    random_sentence = choice(nouns) + ' ' + choice(verbs) + '   ' + choice(adv) + ' ' + choice(adj) + '  ' + \
        choice(punc)

    return random_sentence


def test_make_categorical_pca():
    """Test that columns from PCA decomposition of categorical feature are computed correctly
    test_col_one and test_col_two form a 4x4 matrix when their values are one hot encoded:

    1 0 1 0
    0 1 0 1
    1 0 1 0
    0 1 0 1

    With a dimensionality reduction algorithm, we can represent the above matrix as a vector:

    x
    not x
    x
    not x

    Hence, this test ensures that the resulting vector contains exactly 2 unique values.

    """
    col_one = ["cat_one", "cat_two", "cat_one", "cat_two"]
    col_two = ["dog_one", "dog_two", "dog_one", "dog_two"]

    data = {
        "col_one": col_one,
        "col_two": col_two,
    }

    test_df = pd.DataFrame(data)
    _, test_df = ft.make_categorical_pca_cols(df=test_df, cols=['col_one', 'col_two'], n_components=1)

    # Test columns were created correctly
    assert set(test_df.columns) == {'col_one', 'col_two', 'col_one_col_two_linear_pca_0'}, (
        "Processing categorical PCA feature resulted in unexpected columns")

    # Test factor loadings were correctly computed

    assert len(set([round(x, 2) for x in test_df['col_one_col_two_linear_pca_0'].unique()])) == 2, (
        "Processing categorical PCA feature resulted in incorrect number of factor loadings")


def test_make_categorical_embeddings():
    """ Test that columns from spectral embedding decomposition of categorical feature are computed correctly.
    The logic for testing exactly 2 values is the same as the logic for test_make_categorical_pca """
    col_one = ["cat_one", "cat_two", "cat_one", "cat_two"]
    col_two = ["dog_one", "dog_two", "dog_one", "dog_two"]

    data = {
        "col_one": col_one,
        "col_two": col_two,
    }

    test_df = pd.DataFrame(data)

    test_df = ft.make_categorical_embeddings_cols(df=test_df, cols=['col_one', 'col_two'],
                                                  n_components=1, n_neighbors=2)

    # Test columns were created correctly
    assert set(test_df.columns) == {'col_one', 'col_two', 'col_one_col_two_local_linear_0'}, (
        "Processing categorical embedding feature resulted in unexpected columns")

    # Test embedding loadings were correctly computed
    assert len(set([round(x, 2) for x in test_df['col_one_col_two_local_linear_0'].unique()])) == 2, (
        "Processing categorical embedding feature resulted in incorrect number of factor loadings")


def test_make_comparision_ind():
    """Test that comparison indicator column is computed correctly."""
    greater_col = [1, 2, 3, 4, 5, 6]
    lesser_col = [50, 1, 30, 3, 4, 6]
    test_result = [False, True, False, True, True, False]

    test_df = pd.DataFrame({"greater": greater_col, "lesser": lesser_col})
    test_df = ft.make_comparison_ind_col(df=test_df, greater_col="greater", lesser_col="lesser")

    assert list(test_df["greater_lesser_ind"]) == test_result, (
        "Making comparison feature resulted in incorrect values")


def test_make_log():
    """Test that logs are computed correctly as well as nans and infs are being filled correctly."""
    test_col = [0, -1, 1, 9.025, 27.11, 1, 0]
    test_results = [0, 0, 0, 2.2, 3.3, 0, 0]

    test_df = pd.DataFrame({"col": test_col})
    test_df = ft.make_log_col(df=test_df, col="col", fill_na=False, fill_inf=False)

    # Test log columns were created
    assert set(test_df.columns) == {'log_col', 'col'}, "Incorrect columns were created"

    # Test nan values were filled
    test_df = ft.make_log_col(df=test_df, col="col", fill_na=True, fill_inf=False)
    assert np.nan not in set(test_df["log_col"]), (
        "There are nan values in the dataframe after logging features")

    # Test inf values were filled
    test_df = ft.make_log_col(df=test_df, col="col", fill_na=False, fill_inf=True)
    assert not any({np.inf, -np.inf}.intersection(set(test_df["log_col"]))), (
        "There are inf values in the dataframe after logging features")

    # Test log values were correctly computed
    test_df = ft.make_log_col(df=test_df, col="col", fill_na=True, fill_inf=True)
    assert [round(x, 2) for x in list(test_df["log_col"])] == test_results, (
        "Making logged feature resulted in incorrect values")


def test_make_non_nan_ind_col():
    """Test that nan indicator column is being computed correctly"""
    test_col = [np.nan, 0, 2, np.nan, 0.4, None]
    test_result = [True, False, False, True, False, True]

    test_df = pd.DataFrame({"test_col": test_col})
    test_df = ft.make_non_nan_ind_col(df=test_df, col="test_col")

    # Test new indicator column for correct values
    assert list(test_df["test_col_is_non_nan"]) == test_result, "Non-Nan indicator feature contains incorrect values"


def test_make_identity_ind():
    """Test that identity indicator column is being computed correctly"""
    num_col = [np.nan, 0, 2, None, 0.4, np.inf, -np.inf, 0]
    num_result = [False, True, False, False, False, False, False, True]
    num_identity = 0

    string_col = ["michael", "rob", "page", "marianne", "ben", "michael", "jackie", "tim"]
    string_result = [True, False, False, False, False, True, False, False]
    string_identity = "michael"

    test_df = pd.DataFrame({"num_col": num_col, "string_col": string_col})
    test_df = ft.make_identity_ind_col(df=test_df, col="num_col", identity=num_identity)
    test_df = ft.make_identity_ind_col(df=test_df, col="string_col", identity=string_identity)

    # Test new numerical indicator column for correct values
    assert list(test_df["num_col_is_not_0"]) == num_result, (
        "Numerical identity indicator feature contains incorrect values")

    # Test new string indicator column for correct values
    assert list(test_df["string_col_is_not_michael"]) == string_result, (
        "String identity indicator feature contains incorrect values")


def test_make_scale_col():
    """Test that scaling column is being computed correctly and infs/nans are handled properly"""
    test_col = [1, 2, 0, -30, 50, 1, 0, None, 1]
    scale_col = [2, 4, 1, 0, 10, 0, 1, 1, None]
    test_result = [0.5, 0.5, 0, -np.inf, 5, np.inf, 0]

    test_df = pd.DataFrame({"col": test_col, "scale_col": scale_col})
    test_df = ft.make_scale_col(df=test_df, col="col", scale_col="scale_col")

    # Test scale columns were made correctly
    assert all([
        (np.isnan(val) and np.isnan(exp)) or val == exp
        for (val, exp)
        in zip(test_df['col_scale_col'], test_result)
    ]), "Incorrect scaling columns were created"

    # Test nan values were filled
    test_df = ft.make_scale_col(df=test_df, col="col", scale_col="scale_col", fill_na=True, fill_na_value=0)
    assert np.nan not in set(test_df["col_scale_col"]), (
        "There are nan values in the dataframe after scaling features")

    # Test inf values were filled
    test_df = ft.make_scale_col(df=test_df, col="col", scale_col="scale_col", fill_inf=True, fill_inf_value=0)
    assert not any({np.inf, -np.inf}.intersection(set(test_df["col_scale_col"]))), (
        "There are inf values in the dataframe after scaling features")


def test_make_sentiment_cols():
    """Test that sentiment columns are being computed. Note, we can't test for specific values because they will change
    every time the VADER package update its sentiment measures."""

    test_col = [get_random_junk_sentence() for i in range(5)]
    test_df = pd.DataFrame({"test_col": test_col})
    test_df = ft.make_sentiment_cols(df=test_df, col="test_col")

    # Test inf values were filled
    assert set(test_df.columns) == {'test_col', 'test_col_iqr', 'test_col_lengths', 'test_col_means',
                                    'test_col_prop_neg_sentences', 'test_col_prop_pos_sentences',
                                    'test_col_prop_zeroes', 'test_col_sd', 'test_col_zero_crossings'}, \
        "Sentiment features were not created"


def test_make_sum_list_col():
    """Test that columns containing lists stored as strings are summed correctly"""
    test_col = ["[1, 2, 3]", "[2.3, 4.3]", "[1.2, 3.2, 4.3]", "[0, -1, -2]", "[-2, 2]"]
    test_result = [6, 6.6, 8.7, -3, 0]
    test_df = pd.DataFrame({"test_col": test_col})
    test_df = ft.make_sum_list_col(df=test_df, col="test_col")

    assert list(test_df['sum_test_col']) == test_result, "Summing lists stored as strings resulted in incorrect values"


def test_make_time_col():
    """Test that time features, computed from a column containing datetime objects, are computed correctly"""
    test_col = [datetime(2018, 1, 2, 3), datetime(2017, 3, 10, 23), datetime(2014, 5, 22, 12), datetime(1920, 4, 25, 3)]
    test_df = pd.DataFrame({"test_col": test_col})
    hours = [3, 23, 12, 3]
    days = [2, 10, 22, 25]
    months = [1, 3, 5, 4]
    years = [2018, 2017, 2014, 1920]
    daysofweeks = [1, 4, 3, 6]

    test_df = pd.DataFrame({"col": test_col})

    # Test different time values to see if they were properly returned
    test_df = ft.make_time_cols(df=test_df, date_col="col", hour=True)
    assert list(test_df['col_hour']) == hours, "Making hour time features resulted in incorrect hour values"

    test_df = ft.make_time_cols(df=test_df, date_col="col", day=True)
    assert list(test_df['col_day']) == days, "Making day time features resulted in incorrect day values"

    test_df = ft.make_time_cols(df=test_df, date_col="col", month=True)
    assert list(test_df['col_month']) == months, "Making month time features resulted in incorrect month values"

    test_df = ft.make_time_cols(df=test_df, date_col="col", year=True)
    assert list(test_df['col_year']) == years, "Making year time features resulted in incorrect year values"

    test_df = ft.make_time_cols(df=test_df, date_col="col", hour=True, day=True, month=True, year=True)
    assert list(test_df['col_hour']) == hours and list(test_df['col_day']) == days and \
        list(test_df['col_month']) == months and list(test_df['col_year']) == years, (
        "Making all features resulted in incorrect values")

    test_df = ft.make_time_cols(df=test_df, date_col="col", dayofweek=True)
    assert list(test_df['col_day_of_week']) == daysofweeks, (
        "Making time features resulted in incorrect day of week values")

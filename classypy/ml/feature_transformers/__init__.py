""" Feature transformer functions take a pandas dataframe object as the base object upon which to transform """
import numpy as np
import pandas as pd
from sklearn.decomposition import PCA, KernelPCA, SparsePCA
from sklearn.manifold import LocallyLinearEmbedding, SpectralEmbedding

from classypy.ml.feature_transformers import utils


def eval_str_cols(df, col):
    """ This function evaluates objects stored as strings in a col within df.
    This function wraps the ast package and returns a dataframe with all the
    values in the column col evaluated as python objects.

    Parameters
    ----------
    df : dataframe
        Dataframe containing the column specified by col
    col : str
        A string indicating a column in dataframe. The column should
        contain python objects that are stored as strings

    Return:
    -------
    df
        A dataframe where the values in column are evaluated as python objects

    """
    from ast import literal_eval

    df[col] = df[col].apply(literal_eval)

    return df


def make_bool_col(df, col):
    """ This function ensures that a column is boolean

    Parameters
    ----------
    df : dataframe
        A dataframe object, must contain col
    col : str
        A column in dataframe, this is the column
        that we want to ensure is boolean (contains only 0 or 1 values)

    Return:
    -------
    df: dataframe

    """
    if df[col].dtype == bool:
        df[col] = [int(x == True) for x in df[col]]   # noqa
        return df
    else:
        df[col] = df[col].astype(str).str.lower()
        boolean_cases = [{'true': 1}, {'yes': 1}, {'y': 1}, {'t': 1}, {'on': 1},
                         {'false': 0}, {'no': 0}, {'n': 0}, {'f': 0}, {'off': 0}]
        for bool_case in boolean_cases:
            df[col] = df[col].replace(bool_case)

    assert(len(df[col].unique()) > 0 & len(df[col].unique()) < 3)

    return df


def make_categorical_pca_cols(df, cols, fitted_transformer=None, n_components=None, prefix="", linear=True,
                              sparse=False, primary_key=None):
    """ This function creates a PCA representation of the one-hot encoded columns based on cols (columns in df).

    Parameters
    ----------
    df : Dataframe
        Dataframe must contain col and scale_col
    cols : list
        A list containing columns to be passed through CATPCA
    n_components : int
        Approximating Dimensions. Default: None.
    prefix : str
        A string containing a feature set name. Default: "".
    linear : str
        if false, fit kernel PCA else PCA. Default: True.
    sparse : str
        if true, fit sparse PCA. Default: False.

    Returns
    -------
    A dataframe containing one-hot encoded columns based on cat_col in df
    """
    prefix_cat_cols = []
    for col in cols:
        prefix_cat_col = utils.add_prefix(text=col, prefix=prefix)
        prefix_cat_cols.append(prefix_cat_col)

    cat_df = df[prefix_cat_cols]

    if primary_key:
        prefix_primary_key = utils.add_prefix(text=primary_key, prefix=prefix)
        df = df[[prefix_primary_key]]

    cat_df = pd.get_dummies(data=cat_df, columns=prefix_cat_cols)
    decomposer, cat_df = make_pca_cols(df=cat_df, cols=cols, fitted_transformer=fitted_transformer,
                                       n_components=n_components, prefix=prefix, linear=linear, sparse=sparse)
    df = pd.concat([df, cat_df.reset_index(drop=True)], axis=1)

    return decomposer, df


def make_categorical_embeddings_cols(df, cols, spectral=False, n_components=None, n_neighbors=5, prefix=""):
    """ This function creates embeddings for the one-hot encoded columns based on the cols (columns in df).

    Parameters
    ----------
    df : Dataframe
        Dataframe must contain col and scale_col
    cols : list
        A list containing columns to be passed through CATEMBEDDING
    spectral : bool
        If true, enables spectral embedding for non-linear dimensionality reduction. Default: False.
    n_components : int
        Approximating Dimensions. Number of coordinates for the manifold. Default: None.
    n_neighbors : int
        Number of neighbors to consider for each point. Default: 5.
    prefix : str
        A string containing a feature set name. Default: "".

    Returns
    -------
    A dataframe containing one-hot encoded columns based on cat_col in df
    """
    prefix_cat_cols = []
    for col in cols:
        prefix_cat_col = utils.add_prefix(text=col, prefix=prefix)
        prefix_cat_cols.append(prefix_cat_col)

    cat_df = pd.get_dummies(data=df, columns=prefix_cat_cols)
    cat_df = make_embeddings_cols(df=cat_df, cols=cols, spectral=spectral, n_components=n_components,
                                  n_neighbors=n_neighbors, prefix=prefix)
    df = pd.concat([df, cat_df.reset_index(drop=True)], axis=1)

    return df


def make_comparison_ind_col(df, greater_col, lesser_col, prefix="", strict=True):
    """ This function creates a indicator column in df that compares the value in greater_col
    col against the values in lesser_col in df. By default, it returns a column set to true if
    col > compare_col. When greater_than is false, it returns an indicator column set to
    true if col <= compare_col.

    Parameters
    ----------
    df : Dataframe
        Dataframe must contain col and compare_col
    greater_col : str
        String object referencing a column in df, to be used as x in x >/>= y.
    lesser_col : str
        String object referencing a column in df, to be used as y in x >/>= y.
    prefix : str, optional
        String object, prepended to the newly created indicator column
    strict: bool, required
        Boolean object indicating the type of comparison between col and compare_col
        By default this is set to True, creates a column equal to True when
        df[col] > df[compare_col]

    Returns
    -------
    A dataframe containing a new indicator column, prefixed by prefix, whether or not
    col is greater than compare_col
    """

    bool_col = utils.add_prefix(text=greater_col + "_" + lesser_col + "_ind", prefix=prefix)

    if strict:
        df[bool_col] = df[greater_col] > df[lesser_col]
    else:
        df[bool_col] = df[greater_col] >= df[lesser_col]

    df = make_bool_col(df=df, col=bool_col)

    return df


def make_embeddings_cols(df, cols, spectral=False, n_components=None, n_neighbors=5, prefix=""):
    """ cols is assumed to be a collection of columns that are related to each other by some abstract concept (a latent
    variable). These columns and corresponding rows will be represented be a matrix and this function uses either
    local linear embedding or spectral embedding to find the underlying latent variables that best approximate
    the original column values. To read more about embeddings, see below:
    https://en.wikipedia.org/wiki/Spectral_clustering
    https://www.cs.cmu.edu/~aarti/Class/10701/readings/Luxburg06_TR.pdf

    df is assumed to be a dataframe containing only the columns that will be approximated. n_components is the lower
    dimension that will approximate df. linear and sparse control the flavor of the PCA reduction algorithm.

    Parameters
    ----------
    df : dataframe
        Dataframe must contain col
    col : list
        A list containing columns from which we will impute embeddings
    spectral : bool
        If true, enables spectral embedding for non-linear dimensionality reduction. Default: False.
    n_components : int
        Approximating Dimensions. Number of coordinates for the manifold. Default: None.
    n_neighbors : int
        Number of neighbors to consider for each point. Default: 5.
    prefix : str
        A string containing a feature set name. Default: "".

    Returns
    -------
    A dataframe containing sentiment features derived from col
    """
    # If n_components is none, set to int(sqrt(n_features))
    if n_components is None:
        n_components = int(np.sqrt(len(df.columns)))
    if spectral:
        decomposer = SpectralEmbedding(n_components=n_components, random_state=3)
        embedding_type = "spectral_"
    else:
        decomposer = LocallyLinearEmbedding(n_components=n_components, n_neighbors=n_neighbors,
                                            random_state=3)
        embedding_type = "local_linear_"

    df_array = np.array(df)
    df_array = decomposer.fit_transform(df_array)
    df = pd.DataFrame(df_array)

    group_col = '_'.join(cols)
    new_cols = [utils.add_prefix(text=group_col + "_" + embedding_type + str(x), prefix=prefix)
                for x in range(0, n_components)]

    col_dict = dict(zip(df.columns, new_cols))
    df = df.rename(index=str, columns=col_dict)

    return df


def make_log_col(df, col, prefix="", fill_na=True, fill_inf=True):
    """ This function creates a new column in df
    Which contains the logged version of col. We floor each
    value in col to 1 so that we do not generate nans or infs

    Parameters
    ----------
    df : df
        Dataframe must contain col
    col : str
        A string indicating a column in df
    prefix : str
        A string containing a feature set name
    fill_inf : bool
        Indicates whether or not to replace inf values with 0. Default: False.
    fill_na : bool
        Indicates whether or not to replace nan values with 0. Default: False.
    Returns
    -------
    A dataframe with a new column containing the logged values in col
    """
    col_prefix = utils.add_prefix(text=col, prefix=prefix)
    log_col_prefix = utils.add_prefix(text="log_" + col, prefix=prefix)
    df[log_col_prefix] = df[col_prefix].apply(lambda x: np.log(x))

    if fill_na:
        df[log_col_prefix].fillna(0, inplace=True)
    if fill_inf:
        df = df.replace([np.inf, -np.inf], 0)

    return df


def make_non_nan_ind_col(df, col):
    """ Create non-nan boolean indicator column for values in col.

    Parameters
    ----------
    df : pd.DataFrame
        Dataset
    col : str
        Column in df

    Return
    ------
    pd.DataFrame with non_nan indicator columns for values in col
    """
    df['{col}_is_non_nan'.format(col=col)] = df[col].map(lambda x: np.isnan(x))

    return df


def make_identity_ind_col(df, col, identity=0):
    """ Create boolean indicator column for values in col by testing whether or not
    each value in col is equal to identity.

    Parameters
    ----------
    df : pd.DataFrame
        dataset
    col : str
        column in df
    identity : numeric
        numeric variable that every value in col will be compared against

    Return
    ------
    pd.DataFrame with nonzero indicator columns for values in col
    """
    df['{col}_is_not_{identity}'.format(col=col, identity=str(identity))] = df[col].map(lambda x: x == identity)

    return df


def make_pca_cols(df, cols, fitted_transformer=None, n_components=None, prefix="", linear=True, sparse=False):
    """ cols is assumed to be a collection of columns that are related to each other by some abstract concept (a latent
    variable). df is assumed to be a dataframe containing only the columns that will be approximated.
    n_components is the lower dimension that will approximate df. linear and sparse control the flavor of the PCA
    reduction algorithm.

    Parameters
    ----------
    df : dataframe
        Dataframe must contain col
    cols : list
        A list composed of columns from which we will impute PCA factor loadings
    n_components : int
        Approximating Dimensions. Number of coordinates for the manifold. Default: None.
    prefix : str
        A string containing a feature set name. Default: "".
    linear : str
        if false, fit kernel PCA else PCA. Default: True.
    sparse : str
        if true, fit sparse PCA. Default: False.

    Returns
    -------
    A dataframe containing sentiment features derived from col
    """
    # If n_components is none, set to int(sqrt(n_features))
    if n_components is None:
        n_components = int(np.sqrt(len(df.columns)))

    if linear:
        decomposer = PCA(n_components=n_components, random_state=3)
        pca_type = "linear_pca_"
    else:
        decomposer = KernelPCA(n_components=n_components, kernel='rbf', random_state=3)
        pca_type = "kernel_pca_"

    if sparse:
        decomposer = SparsePCA(n_components=n_components, random_state=3)
        pca_type = "sparse_pca_"

    df_array = np.array(df)

    if not fitted_transformer:
        df_array = decomposer.fit_transform(df_array)
    else:
        df_array = fitted_transformer.fit_transform(df_array)

    df = pd.DataFrame(df_array)
    group_col = '_'.join(cols)
    new_cols = [utils.add_prefix(text=group_col + "_" + pca_type + str(x), prefix=prefix)
                for x in range(0, n_components)]
    col_dict = dict(zip(df.columns, new_cols))
    df = df.rename(index=str, columns=col_dict)

    return decomposer, df


def make_scale_col(df, col, scale_col, fill_inf=False, fill_inf_value=None, fill_na=False, fill_na_value=None):
    """ This function creates a new column in df which contains the values in col scaled by the values in  scale_col.
    fill_inf is a boolean variable that indicates whether nor to fill inf values (generated by divison by 0) with
    fill_inf_values. fill_na and fill_na_values serve the same purpose as their inf cousins.

    Parameters
    ----------
    df : dataframe
        Dataframe must contain col and scale_col
    col : str
        A string indicating a column in df
    scale_col : str
        A string indicating a column in df
    fill_inf : bool
        Indicates whether or not to replace inf values with 0. Default: False.
    fill_inf_value : int or float
        If fill_inf is true, the value to replace inf values with. Default: None.
    fill_na : bool
        Indicates whether or not to replace nan values with 0. Default: False.
    fill_na_value : int or float
        If fill_na is true, the value to replace nan values with. Default: None.

    Returns
    -------
    A dataframe with a new column containing the value of col scaled by scale_col
    """

    df[col + '_' + scale_col] = df[col] / df[scale_col]

    if fill_inf:
        df = df.replace([np.inf, -np.inf], value=fill_inf_value)

    if fill_na:
        df = df.replace(np.nan, value=fill_na_value)

    return df


def make_sentiment_cols(df, col, prefix=""):
    """ col is assumed to be a column in df composed of textual data. This function generates
    sentiment features from the textual column. It returns sentiment from the VADER package
    as well as various word list measures.

    Parameters
    ----------
    df : dataframe
        Dataframe must contain col
    col : str
        A column in df containing text values
    prefix : str, optional
        A string containing a feature set name, blank by default. Default: "".

    Returns
    -------
    A dataframe containing sentiment features derived from col
    """
    from classypy.ml.nlp.text_cleaning import TextCleaner
    from classypy.ml.nlp.text_features import SentimentTransformer
    from classypy.ml.pipeline.DataCleaner import DataCleaner

    text_cleaner = TextCleaner()
    transformer = SentimentTransformer()

    text_col_prefix = utils.add_prefix(text=col, prefix=prefix)
    df[text_col_prefix] = [text_cleaner.clean_text(x, lowercase=True) for x in df[text_col_prefix]]
    # Make sentiment features from textual column then drop the column
    sentiment_df = transformer.transform(text_blocks=df[text_col_prefix])
    sentiment_df = DataCleaner.add_column_name_prefix(df=sentiment_df, prefix=text_col_prefix)
    df = pd.concat([df.reset_index(drop=True), sentiment_df.reset_index(drop=True)], axis=1, sort=False)

    return df


def make_sum_list_col(df, col, prefix=""):
    """ col is assumed to be a column in df composed of strings. Each string
    is assumed to evaluate to a list. This function creates a new column in df
    containing the sum of each evaluated list.

    Parameters
    ----------
    df : df
        Dataframe must contain col
    col : str
        A column composed of lists
    prefix : str
        A string containing a feature set name, blank by default. Default: "".

    Returns
    -------
    A dataframe containing the sum of lists
    """
    # Get sums for list columns (columns containing lists of numeric values)
    col_prefix = utils.add_prefix(text=col, prefix=prefix)
    df = eval_str_cols(df=df, col=col_prefix)
    sum_col_prefix = utils.add_prefix(text="sum_" + col, prefix=prefix)
    df[sum_col_prefix] = df[col_prefix].apply(lambda row: utils.sum_list(arr=row))

    return df


def make_time_cols(df, date_col, hour=False, day=False, month=False, year=False, dayofweek=False, prefix=""):
    """ This function creates a new column in df containing time features generated from
    date_col. Date_col must either by of type datetime or it must contain a string
    formatted as a valid datetime format

    Parameters
    ----------
    df : Dataframe
        Dataframe must contain col and scale_col
    date_col : String
        A string indicating a date column in df
    hour : Boolean
        Indicates whether or not to get hour from date_col
    day : Boolean
        Indicates whether or not to get day from date_col
    month : Boolean
        Indicates whether or not to get month from date_col
    year : Boolean
        Indicates whether or not to get year from date_col
    dayofweek : Boolean
        Indicates whether or not to get the day of week from date_col
    prefix : String
        Prefix for new time feature columns

    Returns
    -------
    A dataframe containing time features generated from date_col
    """
    prefix_col = utils.add_prefix(text=date_col, prefix=prefix)
    if df[date_col].dtype != np.dtype('datetime64[ns]'):
        df[date_col] = pd.to_datetime(df[date_col])

    if hour:
        df[prefix_col + "_hour"] = df[date_col].apply(lambda x: x.hour)
    if day:
        df[prefix_col + "_day"] = df[date_col].apply(lambda x: x.day)
    if month:
        df[prefix_col + "_month"] = df[date_col].apply(lambda x: x.month)
    if year:
        df[prefix_col + "_year"] = df[date_col].apply(lambda x: x.year)
    if dayofweek:
        df[prefix_col + "_day_of_week"] = df[date_col].apply(lambda x: x.dayofweek)

    return(df)

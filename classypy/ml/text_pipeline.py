r"""
Implementations of pipeline functions, to be used in conjunction with sklearn
transformers and estimators.

These allow us to pass input data through all estimators/transformers in a unified session

Example Code
-------------
>> cluster = text_classifier_pipeline( \
    processor=TextNormalizer(return_list=False), \
    vectorizer=TfidfVectorizer(), \
    estimator=KMeans(), \
    reduction=TruncatedSVD())
>> cluster.fit(data)
>> clusters = cluster.transform(data)
"""
from sklearn.pipeline import Pipeline


def text_classifier_pipeline(processor, vectorizer, estimator, reducer=None):
    """
    This is a pipeline used to construct text classifiers.

    Parameters
    ----------
    processor: class that inherits from sklearn's BaseEstimator and TransformerMixin.
        Preprocesses text.
    vectorizer: sklearn transformer.
        Vectorizes text.
    estimator: sklearn estimator.
        Estimator with fit, transform, and predict methods.
    reducer: sklearn estimator.
        If not None, an estimator that will reduce the dimensions of the data
        before feeding it to  `estimator`.

    Returns
    -------
    sklearn estimator: Single estimator.
    """
    steps = [
        ('processor', processor),
        ('vectorize', vectorizer)
    ]

    if reducer:
        steps.append((
            'reduce', reducer)
        )

    # Add the estimator after reduction
    steps.append(('classifier', estimator))
    return Pipeline(steps)


def process_and_vectorize_pipeline(processor, vectorizer):
    """
    Pipeline to process and vectorize a document.

    Parameters
    ----------
    processor: class that inherits from sklearn's BaseEstimator and TransformerMixin.
        Preprocesses text.
    vectorizer: sklearn transformer.
        Vectorizes text.

    Returns
    -------
    sklearn estimator: Single estimator.
    """
    steps = [
        ('processor', processor),
        ('vectorize', vectorizer)
    ]
    return Pipeline(steps)

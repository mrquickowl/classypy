"""
"""
import collections
import os.path as op
import pickle

import numpy as np
import six

from classypy.devops import logger
from classypy.ml import dimensionality_reduction
from classypy.text.cleaning import strip_tags


def process_text_via_LDA(df, text_sections, n_topics=20, n_iter=1500,
                         n_words_per_label=3, min_word_length=3, min_df=1,
                         preprocess=False, model_path=None,
                         stem_words=False, force=False):

    assert not isinstance(text_sections, six.string_types) and \
        isinstance(text_sections, collections.Iterable)

    # Massage data
    def create_docs(df):
        logger.info("Creating docs from dataframe.")
        if preprocess:
            for section in text_sections:
                df[section] = df[section].map(lambda v: strip_tags(v or ''))

        docs = np.asarray([' '.join(row[text_sections]).strip()
                           for ri, row in df.iterrows()])
        idx = np.asarray([bool(doc) for doc in docs])
        if not np.all(idx):
            # TODO Should warn
            pass

        docs = docs[idx]
        names = df['name'][idx] if 'name' in df else [None] * len(idx)
        categories = df['category'][idx] if 'category' in df else [None] * len(idx)

        return docs, names, categories, idx

    docs, names, categories, idx = create_docs(df)

    # Create topics & score programs
    model = None
    load_model = not force and model_path is not None and op.exists(model_path)
    if load_model:
        with open(model_path, 'rb') as fp:
            model = pickle.load(fp)

    topic_labels, lda_matrix, Y_cats, word_mat, model = dimensionality_reduction.lda_categories(
        docs=docs, n_topics=n_topics, n_iter=n_iter, lemmatize=stem_words,
        min_df=min_df, min_word_length=min_word_length, model=model,
        n_top_words=n_words_per_label, return_model=True)

    if not load_model and model_path:
        with open(model_path, 'wb') as fp:
            pickle.dump(model, fp)

    return names, topic_labels, Y_cats, lda_matrix, idx

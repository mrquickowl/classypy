"""Object for training models on a full set of labeled data"""
import glob
import os.path as op

import pandas as pd
from scipy.stats import rankdata

from classypy.devops import logger
from classypy.ml.pipeline.ModelLoader import ModelLoader
from classypy.util.caching import cache_dataframe
from classypy.util.execution import run_in_parallel


class ModelTrainer(ModelLoader):
    """Class containing generic methods and attributes necessary for model training"""

    def __init__(self, config_file, force=False, output_dir=None, model_dir=None, data_dir=None, dataset_date=None,
                 yaml_file=None, targets_dir=None):
        super(ModelTrainer, self).__init__(config_file=config_file, force=force, output_dir=output_dir,
                                           data_dir=data_dir, dataset_date=dataset_date, targets_dir=targets_dir,
                                           yaml_file=yaml_file)
        # Dict containing methods that transform the final prediction to some other form (for example, rank)
        self.prediction_transforms = {"rank": self._rank_prediction}
        self.model_dir = model_dir

    def fit(self):
        """ Fit a model using the entire labels dataset, which has been split into features and labels """
        self.fitted_model = self.model.fit(X=self.X, y=self.y)

    def _rank_prediction(self, prediction_df):
        """ Generate ranks for predicted continuous label

        Parameters
        ----------
        prediction_df : df
            Dataframe object containing primary keys and predictions generated from a ML model

        Return:
        -------
        This function expects a continuous variable in prediction_df (the predicted label) and generates
        a rank for each row in predcition_df based on its place in the sorted dataframe (sorted in ascending
        order by the predicted label).

        """
        rank_list = [-1 * i for i in prediction_df[self.label]]
        y_pred_rank = rankdata(rank_list, method='ordinal')
        prediction_df['predicted_rank'] = y_pred_rank

        return prediction_df

    @cache_dataframe
    def make_predictions(self):
        """ Generate prediction using pickled models. This function works by loading a model as specified
        by this object's model_dir attribute and YAML filename. Then, it generates predictions using the fitted model
        and a fully merged dataset containing features for the target dataset. Finally, it saves predictions to a
        csv file at the file path specified in csv_file.

        Return:
        -------
        Adataframe predictions generated from a fitted model

        """
        model_name = self.yaml_obj.filename
        fitted_model = self.load_model(model_dir=self.model_dir,
                                       model_name=model_name,
                                       dataset_date=self.dataset_date)
        if model_name not in self.predict:
            # Current model is not one specified in config for predictions, so, refuse
            return
        if not fitted_model:
            logger.info("Did not find fitted model. Please fit before predicting")
            return

        logger.info("Predicting using {model_name} trained on data as of {dataset_date}"
                    .format(model_name=self.yaml_obj.filename.replace("_", " "),
                            dataset_date=self.dataset_date))

        y_predict = fitted_model.predict(self.target_data)
        # Check if target ids exist for prediction data, if so then create primary key column
        if any(self.target_ids):
            primary_keys = self.target_ids
        else:
            primary_keys = [None] * len(y_predict)

        prediction_df = pd.DataFrame({self.primary_key: primary_keys,
                                      self.label: y_predict})

        if self.config['transform_predictions']:
            for transform_prediction in self.transform_predictions:
                prediction_df = self.prediction_transforms[transform_prediction](prediction_df=prediction_df)
        return prediction_df

    @staticmethod
    def train_models(yaml_dir, trainer_kwargs, parallel):
        """This function trains models. The config.py file in yaml_dir specifies which models will be trained.
        Each model will be trained with hyper-parameters as specified in each individual model yaml file.
        If the parallel flag is set to true, then models will be trained in parallel.

        Parameters
        ----------
        yaml_dir : str
            The directory containing YAML files specifying models and the parameters they will be loaded with
        trainer_kwargs : dict
            A dictionary object containing arguments for which to load ModelTrainer
        parallel : bool
            A boolean flag indicating whether or not to train models in parallel

        Return:
        -------
        Nothing, this function generates a series of pickled objects containing trained models

        """
        def _train_model(**kwargs):
            trainer = ModelTrainer(**kwargs)
            if trainer.yaml_obj.filename in trainer.train:
                logger.info("Fitting {model_name}".format(model_name=trainer.yaml_obj.filename.replace("_", " ")))
                trainer.fit()
                trainer.save_model(fitted_model=trainer.fitted_model,
                                   model_name=trainer.yaml_obj.filename,
                                   dataset_date=trainer.dataset_date,
                                   output_dir=trainer.output_dir)
                logger.info("Finished fitting and saving {model_name}"
                            .format(model_name=trainer.yaml_obj.filename.replace("_", " ")))

        yaml_files = glob.glob(op.join(yaml_dir, "model*"))
        args_list = []

        # Create list of model evaluation arguments out of yaml files
        for yaml_file in yaml_files:
            # Dictionaries are mutable, so we have to make a copy in each iteration
            kwargs = trainer_kwargs.copy()
            kwargs['yaml_file'] = yaml_file
            args_list.append(kwargs)

        if parallel:
            run_in_parallel(func=_train_model, params=args_list)
        else:
            for args in args_list:
                _train_model(**args)

    @staticmethod
    def predict_targets(yaml_dir, prediction_kwargs, parallel=False, force=False):
        """This function generates predictions from fitted models. The config.yml file in yaml_dir specifies which models
        will be used to generate predictions. If the parallel flag is set to true, then predictions will be generated
        in parallel.

        Parameters
        ----------
        yaml_dir : str
            The directory containing the main config file, which specifies the models to generate predictions
        prediction_kwargs : dict
            A dictionary object containing arguments for which to load ModelTrainer
        parallel : bool
            A boolean flag indicating whether or not to generate predictions in parallel

        Return:
        -------
        Nothing, this function generates a series of csv files containing predictions generated from trained models

        """
        def _predict_targets(**kwargs):
            trainer = ModelTrainer(**kwargs)
            if trainer.yaml_obj.filename not in trainer.predict:
                return
            logger.info("Generating predictions using {model_name}".format(
                model_name=trainer.yaml_obj.filename.replace("_", " ")))
            csv_file = trainer.prediction_data_path
            return trainer.make_predictions(csv_file=csv_file, force=force)

        yaml_files = glob.glob(op.join(yaml_dir, "model*"))
        args_list = []

        # Create list of model evaluation arguments out of yaml files
        for yaml_file in yaml_files:
            # Dictionaries are mutable, so we have to make a copy in each iteration
            kwargs = prediction_kwargs.copy()
            kwargs['yaml_file'] = yaml_file
            args_list.append(kwargs)

        if parallel:
            return run_in_parallel(func=_predict_targets, params=args_list)
        else:
            return [_predict_targets(**args) for args in args_list]

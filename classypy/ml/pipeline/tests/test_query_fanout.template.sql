/* Test to ensure that primary keys are unique in our queries. */

WITH annotation_query AS (
  -- The annotation query will be inserted here
  {query}
)

SELECT {primary_key} AS duplicate_primary_keys
FROM annotation_query
GROUP BY {primary_key}
HAVING COUNT(*) > 1

import os.path as op

from classypy.testing import TestWithSecrets
from classypy.text import text_from_disk
from classypy.util.dirs import this_files_dir


class TestQueries(TestWithSecrets):
    """This class is to test queries by running them."""

    def test_query_fanout(self, query, primary_key, secrets):
        """Test that queries do not have duplicate primary keys.
        Parameters
        ----------
        query : str, required
            String containing the SQL query that we want to test for fanout
        primary_key : str, required
            Specifies the primary key on which to test the queries
        """
        TestWithSecrets.setup_secrets(**secrets)
        test_query_template = text_from_disk(op.join(this_files_dir(), 'test_query_fanout.template.sql'))
        test_query = test_query_template.format(query=query, primary_key=primary_key)
        df = self.redshift_qf(test_query)
        assert len(df) == 0, (
            "{query} returns duplicate values of {primary_key} {duplicate_keys}".format(
                primary_key=primary_key,
                duplicate_keys=set(df['duplicate_primary_keys']),
                query=test_query))

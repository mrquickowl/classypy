import os.path as op
from datetime import datetime
from random import choice, randint, uniform

import numpy as np
import pandas as pd

from classypy.ml.pipeline.DataEngineer import DataEngineer
from classypy.ml.pipeline.YAMLFile import YAMLFile
from classypy.util.dirs import this_files_dir

test_cases = 50

config_obj = YAMLFile(yaml_file=op.join(this_files_dir(), 'features_test.yml'))


def get_random_sentence():
    """Returns a randomly generated sentence based on manually entered part of speech lists"""
    nouns = ["puppy", "car", "rabbit", "girl", "monkey"]
    verbs = ["runs", "hits", "jumps", "drives", "barfs"]
    adv = ["crazily.", "dutifully.", "foolishly.", "merrily.", "occasionally."]
    adj = ["adorable", "clueless", "dirty", "odd", "stupid"]
    punc = ["!", "?", ".", "-", ":"]
    random_sentence = choice(nouns) + ' ' + choice(verbs) + '   ' + choice(adv) + ' ' + choice(adj) + '  ' + \
        choice(punc)

    return random_sentence


def get_test_df_from_list(col_name, cases):
    """Returns a dataframe containing one column, col_name, populated with values randomly chosen from cases"""
    list_col = []
    for i in range(test_cases):
        list_col.append(choice(cases))

    test_df = pd.DataFrame({col_name: list_col})

    return test_df


def get_engineer():
    """Returns a DataEngineer object and a simulated dataset to run tests on"""
    engineer = DataEngineer(config_obj=config_obj)

    return engineer


def test_boolean_features_are_boolean():
    """Tests that boolean features are actually boolean"""
    engineer = get_engineer()
    boolean_cases = ['true', 'yes', 'y', 't', 'on', 'false', 'no', 'n', 'f', 'off']

    test_df = get_test_df_from_list(col_name='test_boolean', cases=boolean_cases)
    test_df = engineer._process_boolean_features(df=test_df)

    assert set(test_df['test_boolean']) == {0, 1}, "Boolean column contains values different than 0, 1"


def test_categorical_features_are_categorical():
    """Tests that categorical features are transformed into their own categorical columns"""
    engineer = get_engineer()
    cat_cases = ["cat_one", "cat_two", "cat_three", "cat_four", "cat_five"]

    test_df = get_test_df_from_list(col_name='test_categorical', cases=cat_cases)

    test_df = engineer._process_categorical_features(df=test_df)

    assert set(test_df.columns) == {'test_cat_one', 'test_cat_two', 'test_cat_three', 'test_cat_four', 'test_cat_five',
                                    'test_nan'}, "Processing categorical feature resulted in unexpected columns"


def test_numeric_features_are_numeric():
    """Tests that numeric features are transformed into the correct dtype"""
    engineer = get_engineer()
    integers, floating_points = [], []
    int_min_seed = min(datetime.today().minute, datetime.today().second)
    int_max_seed = max(datetime.today().minute, datetime.today().second)
    float_min_seed = int_min_seed + (datetime.today().day / 100)
    float_max_seed = int_max_seed + (datetime.today().day / 100)

    for i in range(test_cases):
        integers.append(randint(int_min_seed, int_max_seed))
        floating_points.append(uniform(float_min_seed, float_max_seed))

    test_df = pd.DataFrame({'test_floating_points': floating_points, 'test_integers': integers})

    test_df = engineer._process_numeric_features(df=test_df)

    assert set(test_df.dtypes) == {np.dtype('float64')}, "Numeric features have dtypes that are not float64"


def test_textual_features_are_textual():
    """Tests that textual features are correctly cleaned (all lowercase, normalized spacing, no punctuation)"""
    # Punctuation test cases
    punc = {"!", "?", ".", "-", ":"}
    textual = []
    engineer = get_engineer()

    for i in range(test_cases):
        textual.append(get_random_sentence())

    test_df = pd.DataFrame({'test_textual': textual})

    test_df = engineer._process_textual_features(df=test_df)
    textual_list = list(test_df['test_textual'])

    # Test all text is lower case
    assert all(x for x in textual_list if x.islower()), "Uppercase characters found in processed textual data"

    # Test all text has been normalized
    assert '' not in [x.split(' ') for x in textual_list], "Unnormalized spaces found in processed textual data"

    # Test punctuation is removed
    assert not any(punc.intersection(set(list(x))) for x in textual_list), "Punctuation found in processed textual data"

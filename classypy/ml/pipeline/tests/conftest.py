import ast
import glob
import os.path as op
from datetime import datetime

from classypy.text import text_from_disk


def get_all_queries(dataset_date, primary_keys, n_targets, primary_key, query_dir, query_kwargs):
    """Returns all queries in the queries directory as a list of (query, primary_key). Updates query_kwargs
    for universal keys, every model utilizing the pipeline is assumed to need a dataset_date, primary_keys,
    and n_targets (which may be 0).

    Parameters
    ----------
    primary_keys : str
        Specifies the primary keys for which we get features
    n_targets : int
        Controls how many target accounts to get
    dataset_date : str
        Specifies the date for which we are pulling data
    primary_key : str
        Specifies the primary key on which to test the queries
    query_dir : str
        Specifies where the SQL queries to test are stored
    query_kwargs: str
        Dictionary stored as string, specifies variables needed to run model SQL queries
    """
    if query_kwargs:
        query_kwargs = ast.literal_eval(query_kwargs)
    else:
        query_kwargs = {}

    query_kwargs["dataset_date"] = dataset_date
    query_kwargs["primary_keys"] = primary_keys
    query_kwargs["n_targets"] = n_targets

    queries = glob.glob(op.join(query_dir, "*.sql"))

    queries = [text_from_disk(query).format(**query_kwargs) for query in queries]
    queries_and_primary_key = zip(queries, [primary_key] * len(queries))

    return queries_and_primary_key


def pytest_addoption(parser):
    """Adds get_all_queries arguments as command line arguments"""
    parser.addoption("--dataset-date", default=datetime.today().strftime('%Y-%m-%d'))
    parser.addoption("--primary-keys", default=[])
    parser.addoption("--n-targets", default=10)
    parser.addoption("--primary-key", default='')
    parser.addoption("--query-dir", default='')
    parser.addoption("--query-kwargs", default=None)


def pytest_generate_tests(metafunc):
    """Adds get_all_queries arguments as command line arguments"""
    if 'query' in metafunc.fixturenames:
        kwargs = {}
        kwargs['dataset_date'] = metafunc.config.getoption('dataset_date')
        kwargs['primary_keys'] = metafunc.config.getoption('primary_keys')
        kwargs['n_targets'] = metafunc.config.getoption('n_targets')
        kwargs['primary_key'] = metafunc.config.getoption('primary_key')
        kwargs['query_dir'] = metafunc.config.getoption('query_dir')
        kwargs['query_kwargs'] = metafunc.config.getoption('query_kwargs')
        metafunc.parametrize('query, primary_key', get_all_queries(**kwargs))

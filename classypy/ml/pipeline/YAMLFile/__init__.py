"""Object for handling raw data"""
import os.path as op
import re

from ruamel import yaml


class YAMLFile(object):
    """Class containing generic methods for reading in configurations from a YAML file"""

    def __init__(self, yaml_file, yaml_dir=None):
        """Loads yaml_file from yaml_dir and creates a dictionary from the keys as well as instance attributes for
        commonly used values. Note, we assume that the SQL query shares the same name as the YAML file and we will
        output csv file names using the yaml name as the base filename.

        Attributes:
        -------
        self.yaml_file : str
            Location of yaml_file containing configuration data
        self.yaml_dir : str
            Dictionary containing yaml_file
        """
        # Make Python dictionary object out of YAML file
        self.yaml_dict = self.get_yaml_dict(yaml_file=yaml_file, yaml_dir=yaml_dir)

        # Get just the file name, stripped of directory location
        yaml_file = yaml_file.split("/")[-1]
        self.filename = yaml_file.replace(".yml", "")
        self.query_name = yaml_file.replace(".yml", "")

        if re.match("^feat.*", yaml_file):
            self.prefix = yaml_file.replace("features_", "").replace(".yml", "")
        else:
            self.prefix = yaml_file.replace(".yml", "")

        self.feature_types = self.yaml_dict.get('feature_types', {}).keys()
        self.feature_engineer = self.yaml_dict.get('feature_engineer', {}).keys()

    def get_yaml_dict(self, yaml_file, yaml_dir=None):
        """This function reads in a YAML file and returns a dictionary object containing the structure and contents
        specified in yaml_file

        Parameters
        ----------
        yaml_file : str
            String containing filename to yaml file
        yaml_dir : str
            String containing directory housing the yaml file

        Return:
        -------
        yaml_dict
            A dictionary containing the structure/content in the yaml file
        """
        if yaml_dir is not None:
            yaml_file = op.join(yaml_dir, yaml_file)

        with open(yaml_file, 'r') as stream:
            yaml_dict = yaml.safe_load(stream)
            if yaml_dict is None:
                yaml_dict = {}

        return yaml_dict

    def set_target_csv_file(self):
        """This function prepends a targets_ tag to the base csv_file string in the yaml file. This method assumes a
        valid csv_file key in self.yaml_dict

        TODO: Deprecate this across the pipeline, should be using the get_csv_file method in DataTransformer

        Attributes:
        -------
        self.yaml_dict['csv_file']
            prepends "targets_" to self.yaml_dict['csv_file']
        """
        if not self.filename.startswith('targets_'):
            self.filename = "targets_" + self.filename

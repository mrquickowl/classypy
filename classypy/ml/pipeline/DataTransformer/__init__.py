"""Object for handling raw data"""
import abc
import glob
import os.path as op

from classypy.ml.pipeline.YAMLFile import YAMLFile


class DataTransformer(object):
    __metaclass__ = abc.ABCMeta
    """Class containing generic methods for dealing with loading, transform and saving data"""
    def __init__(self, config_dir=None, is_targets=False):
        if config_dir:
            self.configs = self.read_configs_from_dir(config_dir=config_dir, is_targets=is_targets)
        else:
            self.configs = {}

    @staticmethod
    def add_column_name_prefix(df, prefix):
        """This function appends every column in df with a prefix. For example, if prefix is equal to data_insights and
        df only contains one column named example_col then this function would return a dataframe with one column named
        data_insights_example_col

        Parameters
        ----------
        df : dataframe
            A generic dataframe object
        prefix : str
            A string containing the prefix all columns in df will be appended by

        Return:
        -------
        df : dataframe
            A dataframe with updated column names

        """
        orig_cols = df.columns
        new_cols = [prefix + "_" + x for x in orig_cols]
        prefixed_cols = dict(zip(orig_cols, new_cols))
        df = df.rename(index=str, columns=prefixed_cols)

        return df

    @staticmethod
    def add_prefix(text, prefix):
        """Utility function - prepends text with prefix.

        Parameters
        ----------
        text : str
            A generic dataframe object
        prefix : str
            A string containing the prefix all columns in df will be appended by

        Return:
        -------
        new_text : str
            The text value prepended with prefix
        """
        new_text = prefix + "_" + text

        return new_text

    @abc.abstractmethod
    def fit():
        """Abstract method to maintain consistency across pipeline"""
        pass

    def fit_transform(self, df):
        """Abstract method to maintain consistency across pipeline"""
        self.fit(df=df)
        df = self.transform(df=df)

        return df

    @classmethod
    def get_csv_file(cls, filename, csv_file_dir, is_targets, dataset_date, suffix=None):
        """This method creates a formatted csv file name based on filename, csv_file_dir, is_targets, and dataset date

        Parameters
        ----------
        filename : str
            String containing filename to which we want to append dataset_date
        csv_file_dir : str
            String containing directory to append to the filename
        is_targets : boolean
            Indicates whether or not to process targets version of the filename
        dataset_date : str
            Datetime string formatted in YYYY-MM-DD
        suffix : str
            A string to be appended to the filename

        Return:
        -------
        csv_file: str
            A string containing the formatted filename
        """
        if is_targets and filename not in ['labels', 'targets']:
            filename = "targets_" + filename

        csv_file = op.join(csv_file_dir, cls.get_dated_file_name(filename=filename,
                                                                 suffix=suffix,
                                                                 dataset_date=dataset_date))

        return csv_file

    @staticmethod
    def get_dated_file_name(filename, dataset_date, suffix=None, file_type='.csv'):
        """This method strips dashes and slashes in dataset date and returns a formatted, filename. This function
        is intended to be used to append a date stamp to a filename.

        Parameters
        ----------
        filename : str
            String containing filename to which we want to append dataset_date
        dataset_date : str
            Datetime string formatted in YYYY-MM-DD
        append : str
            A string to be appended to the filename
        file_type : str
            File type of the filename, by default this is set to csv

        Return:
        -------
        dated_file_name: str
            A string containing filename appended with a well formatted dataset_date
        """
        date = dataset_date.replace("-", "")
        date = date.replace("/", "")
        if suffix:
            dated_file_name = filename + '_{suffix}{date}{file_type}'. \
                format(date=date, suffix=suffix, file_type=file_type)
        else:
            dated_file_name = filename + '{date}{file_type}'. \
                format(date=date, file_type=file_type)

        return dated_file_name

    @staticmethod
    def read_configs_from_dir(config_dir, is_targets):
        """ This method accepts config_dir as an argument and returns all configuration files in config dir within a
        dictionary object. The dictionary has the config filenames as keys and the values are YAMLFile objects.
        This method also has logic for creating separate entries for target versions of feature set config files

        Parameters
        ----------
        config_dir : str
            String containing filename to which we want to append dataset_date
        is_targets : str
            Datetime string formatted in YYYY-MM-DD
        """
        configs = {}
        config_files = glob.glob(op.join(config_dir, "*.yml"))
        config_files = [x.replace(config_dir, "").replace("/", "") for x in config_files]

        # Remove target config if not processing target files
        if not is_targets:
            try:
                config_files.remove('targets.yml')
            except ValueError:
                try:
                    config_files.remove('targets.yaml')
                except ValueError:
                    pass
            for config_file in config_files:
                configs[config_file] = YAMLFile(yaml_file=config_file, yaml_dir=config_dir)
        else:
            # Create a targets key for each feature set config file
            for config_file in config_files:
                configs[config_file] = YAMLFile(yaml_file=config_file, yaml_dir=config_dir)
                # Do not create a targets version for the targets and labels config files
                if 'targets' in config_file or 'labels' in config_file:
                    pass
                else:
                    configs['targets_' + config_file] = YAMLFile(yaml_file=config_file, yaml_dir=config_dir)
        return configs

    @abc.abstractmethod
    def transform():
        """Abstract method to maintain consistency across pipeline"""
        pass

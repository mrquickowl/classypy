import os.path as op

import numpy as np
import pandas as pd

from classypy.devops import logger
from classypy.ml.pipeline.DataTransformer import DataTransformer
from classypy.util.caching import cache_dataframe


class DataMerger(DataTransformer):
    ## Class does not need any variables to be instantiated but fit needs to be called before transform ##
    def __init__(self, configs):
        self.configs = configs

    @classmethod
    def _handle_nans_and_infs(cls, config, df):
        """Check merged dataframe to see if it has any nans or infs, model won't fit if they exist

        Parameters
        ----------
        config: dict
            Dictionary containing rules for how nans and infs will be handled
        df : dataframe object

        Return:
        -------
        A dataframe object where the NaNs are handled. If there are infs in df, raise an assertion error.

        """
        if df.isnull().values.any():
            logger.info("Merging {prefix} feature resulted in {n} NaNs, implementing NaN policy".format(
                        prefix=config.prefix, n=max(df.isna().sum())))
            df = cls._handle_nans(df=df, feature_name=config.filename, nan_policy=config.yaml_dict['impute'])

        assert not df.isin([np.inf, -np.inf]).values.any(), ("{prefix} features contains Infs, please replace or drop \
            Infs in the SQL or feature engineering layer before merging".format(prefix=config.prefix))

        return df

    @classmethod
    def _handle_nans(cls, df, feature_name, nan_policy):
        """ Deal with NaNs in df according to nan_policy. The pipeline process each of the features independently,
        while ignoring NaNs that may occur across feature set. This method deals with NaNs after they've been merged
        with other feature sets using a common set of the primary keys. The four options are:

        {indicator}
        Replace nan columns with an indicator column that is 1 if value is nan
        {drop_col}
        Drop nan columns
        {drop}
        Drop rows with nans
        {impute_median}
        Replace nans with column median
        {impute_mean}
        Replace nans with column median
        {zero}
        Replace nans with zeroes

        Parameters
        ----------
        df : dataframe object
        feature_name: str
            Name of the feature set being handled
        nan_policy : str
            Indicataes how to handle nans for a given feature set

        Return:
        -------
        A dataframe object where the NaNs are handled according to nan_policy. If invalid nan_policy, raise error.

        """
        null_cols = list(df.columns[df.isnull().any()])

        if nan_policy == "indicator":
            for null_col in null_cols:
                df['{col}_is_nonnan'.format(col=null_col)] = df[null_col].map(lambda x: np.isnan(x))
                df = df.drop(labels=null_col, axis=1)
        elif nan_policy == "drop_col":
            df = df.drop(labels=null_cols, axis=1)
        elif nan_policy == "drop":
            df = df.dropna(axis=0)
        elif nan_policy == "impute_median":
            for null_col in null_cols:
                median = np.nanmedian(df[null_col])
                if pd.isnull(median):
                    median = 0
                df[null_col].replace(to_replace={None: median}, inplace=True)
        elif nan_policy == "impute_mean":
            for null_col in null_cols:
                mean = np.nanmean(df[null_col])
                if pd.isnull(mean):
                    mean = 0
                df[null_col].replace(to_replace={None: mean}, inplace=True)
        elif nan_policy == "zero":
            for null_col in null_cols:
                df[null_col].fillna(0, inplace=True)
        else:
            raise ValueError("No nan policy was provided for {feature_set}"
                             .format(feature_set=feature_name.replace("feature_", "")))

        return df

    @staticmethod
    def _handle_missing_cols(labels_df, targets_df, targets_csv_file, label):
        """This method imputes columns that are in the labels dataset and not in the target dataset by creating the
        corresponding columns in the target dataset and filling them with zeros. It also drops columns that are in the
        target dataset but not in the labels dataset.

        Parameters
        ----------
        labels_df : dataframe
            Dataframe containing the labels data
        targets_df : dataframe
            Dataframe containing the targets data
        targets_csv_file: str
            File path to where the cleaned up merged target data will be stored

        """
        def _add_and_drop_cols(csv_file, df, drop_cols, impute_cols, label):
            df = df.drop(columns=drop_cols)
            for impute_col in impute_cols:
                if impute_col != label:
                    df[impute_col] = 0

            # Overwrite existing csv file
            df.to_csv(csv_file, index=False)

        cols_in_labels_not_targets = set(labels_df.columns).difference(targets_df.columns)
        cols_in_targets_not_labels = set(targets_df.columns).difference(labels_df.columns)

        _add_and_drop_cols(csv_file=targets_csv_file, df=targets_df, drop_cols=cols_in_targets_not_labels,
                           impute_cols=cols_in_labels_not_targets, label=label)

        return labels_df, targets_df

    def fit(self, primary_key_df):
        """This method loads the primary key dataframe and sets the dictionary of feature set configs as
        class attributes

        Parameters
        ----------
        primary_df : dataframe
            Dataframe containing primary keys on which data will be merged

        """
        self.primary_key_df = primary_key_df

    @classmethod
    def run(cls, configs, data, dataset_date, force, is_targets, output_dir, primary_key_dir):
        """This function manages the orchestration for merging multiple datasets (labels and targets)
        as well as outputting the fully merged data into a specified output path.

        Parameters
        ----------
        configs : dict
            Dictionary where the keys are file paths and the values are a yaml_obj
        data : dict
            Dictionary where the keys are the same as the keys in config and the values are dataframes
        dataset_date : str
            Date on which the raw data was sourced
        force : bool, optional
            Forces cached data to be overwritten (Default: False)
        is_targets : bool
            Indicates whether or not to clean features for target dataset
        output_dir : str
            Directory where cleaned up csv files will be stored
        primary_key_dir : str
            Directory containing a dataframe populated with primary keys on which to merge

        Return:
        -------
        This function generates merged dataframes based on labels and/or targets primary keys

        """
        @cache_dataframe
        def _run(configs, data, dataset_name, dataset_date):
            primary_key_df_path = op.join(primary_key_dir,
                                          cls.get_dated_file_name(filename=dataset_name,
                                                                  dataset_date=dataset_date,
                                                                  suffix="clean"))
            merger = DataMerger(configs)
            primary_key_df = pd.read_csv(primary_key_df_path)

            merger.fit(primary_key_df=primary_key_df)
            merged_df = merger.transform(data=data)

            return merged_df

        config_data_dict = {}

        if is_targets:
            # Get all configs and data specific to targets
            target_configs = dict((k, configs[k]) for k in configs.keys() if 'targets' in k)
            target_data = dict((k, data[k]) for k in data.keys() if 'targets' in k)

            # Get all configs and data specific to labels
            label_configs = dict((k, configs[k]) for k in configs.keys() if 'targets' not in k)
            label_data = dict((k, data[k]) for k in data.keys() if 'targets' not in k)

            # Put all configs and data in a dictionary
            config_data_dict['labels'] = {'configs': label_configs, 'data': label_data}
            config_data_dict['targets'] = {'configs': target_configs, 'data': target_data}

        else:
            config_data_dict['labels'] = {'configs': configs, 'data': data}

        merged_dfs = {}
        for dataset_name, configs_and_data in config_data_dict.items():
            merged_dfs[dataset_name] = {}
            csv_file = op.join(output_dir, cls.get_dated_file_name(filename=dataset_name,
                                                                   dataset_date=dataset_date))
            merged_dfs[dataset_name]['csv_file'] = csv_file
            merged_dfs[dataset_name]['df'] = _run(configs=configs_and_data['configs'], csv_file=csv_file, force=force,
                                                  data=configs_and_data['data'], dataset_name=dataset_name,
                                                  dataset_date=dataset_date)
        if len(merged_dfs.keys()) > 1:
            label_config_key = [k for k in configs if 'labels' in k][0]
            # When dropping columns in targets, ignore the presence of the label column in the labels dataset
            label = configs[label_config_key].yaml_dict['label']
            labels_df, targets_df = cls._handle_missing_cols(labels_df=merged_dfs['labels']['df'],
                                                             targets_df=merged_dfs['targets']['df'],
                                                             targets_csv_file=merged_dfs['targets']['csv_file'],
                                                             label=label)

    def transform(self, data):
        """This method merges all dataframes found in data against the primary key dataframe. Note that fit must be called first
        before transform so that the instance has a primary key dataframe and a config dictionary as class attributes.

        Parameters
        ----------
        data : dictionary
            A dictionary where the keys are the names of the feature sets and the values are dataframes

        """
        assert all(v is not None for v in [self.primary_key_df, self.configs]), "Must first call fit before transform"
        merged_df = self.primary_key_df.copy()

        for feature_set_name, feature_set_config in self.configs.items():
            # Merge each feature set together, when nulls or infs are encountered then implement
            # the null policy specified by the config
            if 'feat' in feature_set_name:
                logger.info("Merging {prefix}".format(prefix=feature_set_config.prefix))
                primary_key = feature_set_config.yaml_dict['primary_key']
                prefixed_primary_key = self.add_prefix(text=primary_key, prefix=feature_set_config.prefix)

                feature_df = data[feature_set_name]
                merged_df = pd.merge(merged_df, feature_df, how='left',
                                     left_on=primary_key,
                                     right_on=prefixed_primary_key)
                del merged_df[prefixed_primary_key]

                assert(len(merged_df) <= len(self.primary_key_df))
                merged_df = self._handle_nans_and_infs(df=merged_df, config=feature_set_config)
                logger.info("Merged resulted in {n_rows} rows".format(n_rows=len(merged_df)))

        return merged_df

    @classmethod
    def load_data(cls, configs, data_dir, dataset_date):
        """This function loads all the raw data files indicated by the keys in the confings dictionary.

        Parameters
        ----------
        configs : dict
            Dictionary where the keys are file paths and the values are a yaml_obj
        data_dir : str
            File path to where data is being stored
        dataset_date : str
            Date for which the raw data is filtered on

        Return:
        -------
        data_dict : dict
            Dictionary where the keys are the names of the config files and the values are dataframes
        """
        data_dict = {}
        is_targets = False

        # Nested for loop necessary to get both targets and features data
        for feature_set_name, feature_set_config in configs.items():
            dataset_name = feature_set_name.split('.')[0].replace("_", " ")
            logger.info("Loading engineered {dataset}".format(dataset=dataset_name))

            is_targets = ('targets' in dataset_name)
            if feature_set_name.replace('.yml', '') in ['labels', 'targets']:
                continue

            csv_file = cls.get_csv_file(filename=feature_set_config.filename,
                                        csv_file_dir=data_dir,
                                        is_targets=is_targets,
                                        dataset_date=dataset_date,
                                        suffix="engineered")

            data_dict[feature_set_name] = pd.read_csv(csv_file)

        return data_dict

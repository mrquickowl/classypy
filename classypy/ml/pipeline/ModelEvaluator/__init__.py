"""Object for handling raw data"""
import glob
import os.path as op

import numpy as np
from sklearn.model_selection import KFold, train_test_split

from classypy.devops import logger
from classypy.ml.class_imbalance import get_combination_sampled_classes, get_oversampled_classes
from classypy.ml.model_metrics import (correct_rank_probability, get_classification_report, get_confusion_matrix,
                                       get_error_over_baseline, get_f1score, get_mse,
                                       get_precision_recall_fscore_support)
from classypy.ml.pipeline.DataImporter import DataImporter
from classypy.ml.pipeline.ModelLoader import ModelLoader
from classypy.util.execution import run_in_parallel


class ModelEvaluator(ModelLoader):
    """Class containing generic methods for dealing with evaluating ML models"""
    # This dictionary contains a mapping between the metrics label in each model yaml file
    # and the corresponding function found in model_metrics
    safe_metrics = {
        "baseline error": get_error_over_baseline,
        "correct rank probability": correct_rank_probability,
        "mse": get_mse,
        "classification report": get_classification_report,
        "confusion matrix": get_confusion_matrix,
        "precision recall fscore support": get_precision_recall_fscore_support,
        "f1 score": get_f1score,
    }
    safe_balance_dataset = {
        "combination": get_combination_sampled_classes,
        "oversampling": get_oversampled_classes,
    }

    def __init__(self, config_file, force=False, data_dir=None, dataset_date=None, output_dir=None, yaml_file=None,
                 is_cv=None):
        super(ModelEvaluator, self).__init__(config_file=config_file, force=force, data_dir=data_dir,
                                             dataset_date=dataset_date, output_dir=output_dir, yaml_file=yaml_file)
        self.is_cv = is_cv
        self.get_train_test_split()

    def get_train_test_split(self):
        """This function splits the labels data into test and training sets. It looks inside of the configuration
        file to find the size of the test set and the random seed used for determining test and training indices.

        Return:
        -------
        This function sets the training and test splits of the merged label data as object attributes

        """
        self.X_train, self.X_test, self.y_train, self. y_test = \
            train_test_split(self.X, self.y, shuffle=True,
                             test_size=self.test_size, random_state=self.random_state)
        if self.config['balance_dataset']:
            for balance_method in self.balance_dataset:
                self.X_train, self.y_train = self.safe_balance_dataset[balance_method](
                    X_train=self.X_train, y_train=self.y_train, random_state=self.random_state)

    def get_cv_results(self, X_train, y_train):
        """This function fits a series of models using subsets of X_train and y_train in order to get
        cross validation results. Note, the number of folds is set in the config file within yaml_dir.

        Parameters
        ----------
        model : model object
            Model for which we want test set results
        X_train : np.array
            Numpy array containing features for the training set
        y_train : np.array
            Numpy array containing actual label values for the training set

        Return:
        -------
        This function returns a dictionary object where the values are different model metrics and the
        values are the cross validation results for those metrics

        """
        kf = KFold(n_splits=self.n_splits)

        metric_results = {}
        for metric in self.metrics:
            cv_results = []
            for train_index, test_index in kf.split(X_train):
                cv_X_train, cv_X_test = X_train[train_index], X_train[test_index]
                cv_y_train, cv_y_test = y_train[train_index], y_train[test_index]
                fitted_model = self.model.fit(X=cv_X_train, y=cv_y_train)
                cv_y_predict = fitted_model.predict(cv_X_test)
                cv_result = self.safe_metrics[metric](y_pred=cv_y_predict, y_test=cv_y_test)
                cv_results.append(cv_result)
            metric_results[metric] = np.mean(cv_result)

        return metric_results

    def get_test_results(self, X_train, X_test, y_train, y_test):
        """This function fits a model using X_train and y_train and then generates predictions
        using the fitted model and X_test. Finally, it generates model metrics using y_test and
        the predicted results.

        Parameters
        ----------
        model : model object
            Model for which we want test set results
        X_train : np.array
            Numpy array containing features for the training set
        X_test : np.array
            Numpy array containing features for the test set
        y_train : np.array
            Numpy array containing actual label values for the training set
        y_test : np.array
            Numpy array containing actual label values for the test set

        Return:
        -------
        This function returns a dictionary object where the values are different model metrics and the
        values are the test set results for those metrics

        """
        fitted_model = self.model.fit(X=X_train, y=y_train)
        y_predict = fitted_model.predict(X_test)

        test_results = {}
        for metric in self.metrics:
            metric_result = self.safe_metrics[metric](y_pred=y_predict, y_test=y_test)
            test_results[metric] = metric_result

        return test_results

    def save_results(self, result, append=True):
        """This function produces a log file in self.output_dir containing model evaluation results

        Parameters
        ----------
        result : array-like
            Directory containing the YAML files for each model
        append : bool
            Boolean flag indicating whether or not to append model results to log

        Return:
        -------
        Nothing, this function generates a log file containing model metrics at output_file_path

        """
        output_file = DataImporter.get_dated_file_name(filename="model_results",
                                                       dataset_date=self.dataset_date,
                                                       file_type=".txt")
        output_file_path = op.join(self.output_dir, output_file)

        if append:
            append_write = 'a'  # append if already exists
        else:
            append_write = 'w+'  # make a new file if not

        # Check if output directory exists, make if not
        if not op.exists(self.output_dir):
            import os
            os.makedirs(self.output_dir)

        with open(output_file_path, append_write) as out_file:
            out_file.write("#" * 40)
            out_file.write("\n")
            out_file.write(result)
            out_file.write("\n")

    def calculate_model_metrics(self, yaml_dir, evaluator_kwargs, parallel=False):
        """This function produces model metrics, if parallel is set to true then it will evaluate each of the models in
        parallel. Note, parallel processing for model metrics is memory-intensive and should be avoided on local
        machines. This is because each process has to get a copy of the entire dataset and each process is only
        allocated a small amount of memory to get both cross-validation and test metrics.

        Parameters
        ----------
        yaml_dir : array-like
            Directory containing the YAML files for each model
        evaluator_kwargs : dict
            Dictionary containing arguments for model evaluation
        parallel : bool, optional
            Boolean flag indicating whether or not evaluate models in parallel

        Return:
        -------
        Nothing, this function generates a log file containing model metrics

        """
        def _evaluate_model(**kwargs):
            """ Private method for evaluating one model per process"""
            evaluator = ModelEvaluator(**kwargs)

            if evaluator.is_cv:
                cv_results = evaluator.get_cv_results(X_train=evaluator.X_train,
                                                      y_train=evaluator.y_train)
                for metric in cv_results.keys():
                    output_log = "Cross-Validation result for {model} using {metric}: {result}".\
                                 format(metric=metric,
                                        model=evaluator.yaml_obj.filename.replace("_", " "),
                                        result=cv_results[metric])
                    logger.info(output_log)
                    evaluator.save_results(result=output_log)

            test_results = evaluator.get_test_results(X_train=evaluator.X_train, X_test=evaluator.X_test,
                                                      y_train=evaluator.y_train, y_test=evaluator.y_test)
            for metric in test_results.keys():
                output_log = "Test set result for {model} using {metric}: {result}".\
                             format(metric=metric,
                                    model=evaluator.yaml_obj.filename.replace("_", " "),
                                    result=test_results[metric])
                logger.info(output_log)
                evaluator.save_results(result=output_log)

        yaml_files = glob.glob(op.join(yaml_dir, "model*"))
        args_list = []

        # Create list of model evaluation arguments for each yaml file
        for yaml_file in yaml_files:
            # Dictionaries are mutable, so we have to make a copy in each iteration
            kwargs = evaluator_kwargs.copy()
            kwargs['yaml_file'] = yaml_file
            args_list.append(kwargs)

        # Create output log for CV and test results
        self.save_results(result="Model metrics as of {dataset_date}".format(dataset_date=self.dataset_date),
                          append=False)
        if parallel:
            run_in_parallel(func=_evaluate_model, params=args_list)
        else:
            for args in args_list:
                _evaluate_model(**args)

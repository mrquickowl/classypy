# classypy.ml.pipeline - Fast and Reliable ML Models, the Classy way

The machine learning pipeline is intended to be an end to end pipeline for deploying **supervised** machine learning models. The pipeline has methods and procedures for sourcing/cleaning raw data from the data warehouse (`Redshift`), perform feature engineering on raw data, training machine learning models using the engineered features, evaluating model performance, and generating predictions. The pipeline is designed in such a way to facilitate model deployment and implementation. It is not intended to be used as a tool for data exploration and analysis - modelers should come into model development with analysis, a solid target to predict, well defined model metrics, and a understanding of the statistical structures in the datasets they are working with.

The model pipeline also utilizes standardized set of tools, `classypy.ml.feature_transformers` and `classypy.ml.model_metrics`, to handle feature engineering and model evaluation.

The full project doc can be found [here](https://docs.google.com/document/d/1mWRhQGhUfUqRo211jGPHuYEloua-FMcTPNBbeeE1idE/edit#heading=h.kdg0h6g2qnis).

## Dependencies

The dependencies specified in `requirements.txt` for the classypy package ought to sufficient.

However, individual models may have package dependencies for importing data or feature engineering. All dependencies on a model level are handled independently of the modeling pipeline. Available models are delineated in the imports in ModelLoader, these are:

* BaggingRegressor
* ExtraTreesRegressor
* RandomForestRegressor

Models that utilize ML packages that aren't already being imported in ModelLoader need to be added to those imports.

## Usage

Individual models live in different repositories. However, each should be implementing the following classes in each portion of the pipeline, delineated in each pipe's section below. Throughout the pipeline, each pipe uses the YAMLFile class extensively. YAMLFile has methods for loading YAML files as Python dictionaries, finding key fields in the YAML files, and for formatting file names. In the sections related to models themselves, ModelLoader is used extensively for saving and loading models, getting merged datasets, and loading configuration files.

The pipes of the pipeline are:

* *Import and Clean Data*
* *Engineer Features and Merge Data*
* *Generate Model Metrics*
* *Train Models*
* *Generate Predictions*

### Import and Clean Data:

Classes: DataImporter and DataCleaner

This portion of the pipeline is used to import data, most likely from the `Redshift` data warehouse, and to perform **simple** data cleaning operations. DataImporter looks for YAML files in the yaml_dir argument and it looks for SQL queries in the query_dir argument. The following SQL queries are expected to exist in query_dir:

* `targets.template.sql`
* `labels.template.sql`
* `features_.template.sql`

There should be a one-to-one mapping between YAML files and query files, with the exception of the `query_kwargs.yml` file. The `query_kwargs.yml` file specifies the necessary arguments for each SQL query.

Each SQL file should take a `dataset_date` argument to limit data to a particular date. Targets should **only** return primary keys and labels should **only** return primary keys and a column containing labeled data (the prediction objective). Targets are the primary keys that we want to produce predictions for, labels are the primary keys for which we have observations for what we're trying to predict. The `features_.template.sql` queries are queries related to a specific feature set. See the common questions section for how to define a feature set. We have an automated test in the pipeline to ensure that primary keys returned across SQL queries are unique. DataImporter looks for a --n-targets argument, which is used to determine the number of primary keys that we will generate predictions for and whether or not we pull features for the target primary keys. Further, the --targets argument in data cleaning determines whether or not we will produced cleaned data for the target dataset.

The available cleaning operations are:

    * Replace

That's it. More complicated transformations should be done in the feature engineering pipe of the pipeline. Data cleaning should be used for things like replacing "No" and "N" with False.

#### Example Data YAML

```yaml
    replace:
      all:
        'N': 0
        'Y': 1
```

This YAML file says to replace every field with the value 'N' with 0 and every field with the value 'Y' with 1. So it doesn't say a lot, but the pipeline looks for the existence of these YAML files in order to pull the corresponding SQL queries.

### Engineer Features and Merge Data:

Classes: DataEngineer and DataMerger

DataEngineer takes a yaml_dir argument, each feature set is expected to have a corresponding YAML file in the yaml_dir directory. These YAML files dictate how a feature set will be engineered. First, each feature will be assigned a type. This is controlled by the feature_types key. Next, the class will look for user defined functions to perform feature engineering. All user defined feature engineering functions should be stored in a `feature_engineering_functions.py` file stored in the same directory as the script which calls DataEngineer.

Which features will undergo which user defined feature engineering operations is to be defined by the feature_engineer key in the YAML file. Each procedure *must* have a corresponding method within the FeatureEngineer class inside feature_engineering_functions.

DataEngineer looks for a `--targets` argument to determine whether or not to engineer features for the target dataset. Similarly, DataMerger looks for a `--targets` argument to determine whether or not to produced merged data for the targets dataset.

Feature sets are merged by a left-join and will merge along the primary keys column (specified in the YAML file for each feature set). This left-join can produce nulls, therefore the pipeline tests for the presence of nulls and infinites (which can result from ZeroDivision errors). The pipeline looks in the YAML file of each feature set for a nan_policy key, which is specified under an impute flag. This nan_policy specifies what is to be done about nulls that result from a merging. Infs should be dealt with in either the SQL or feature engineering portions of the pipeline. Note, modelers must be careful about which imputation policy to implement - if the probability of nans in the labels dataset do not reflect the probability of nans in the targets dataset then certain imputation policies (such as drop_col) may result in label and target datasets that do not match each other. This will result in models that cannot produce predictions for the fully merged targets dataset. Columns in that targets data must be dropped to match the columns in the labels dataset in that scenario.

#### Example Feature YAML

```yaml
    primary_key:
      truncated_account_id
    feature_types:
      boolean:
        - transactions_exist
      categorical:
        - account_segment
      numeric:
        - transactions_num
        - transactions_total
        - transactions_days_premature
    feature_engineer:
      scale:
        -
          numerator: transactions_total
          denominator: transactions_num
        -
          numerator: transactions_total
          denominator: transactions_days_premature
    impute:
      impute_median
```

This YAML file says that we want to merge on truncated_account_id field, that we want to make transactions_exist a boolean type, account_segment a categorical type (generate dummy variables for each level), and to set transactions_num, transaction_total, and transactions_days_premature as numeric variables. It also says to generate new columns one that scales transactions_total by transactions_num and another that scales transactions_total by transactiosn_days_premature. Finally, it says that if we encounter nulls/infs in our merge to replace them with the median in each column.

### Generate Model Metrics:

Classes: ModelEvaluator

ModelEvaluator looks for the following YAML files:

* `config.yml` - Specifies how to split the training set, random states, primary keys to split on, and which model metrics to generate
* `model_.yml` - These YAML files are specific to **each** model and specifies the hyperparameters for each model.

Further, ModelEvaluator looks for the `--is_cv` argument. If this flag is passed, then it produces cross validation model metrics in addition to test set metrics. It also looks for a `--parallel` flag, if this is passed then it produces model metrics in parallel for a set of a models. Running models in parallel is precarious, especially on local machines. Each process is allocated only a subset of memory and models that require searching across a large hyper-parameter space, Monte Carlo simulation, or bootstrapping methods will take much longer to run in parallel versus sequentially.

Note, the pipeline expects the existence of the below keys in the config file - any missing keys will result in an error and the pipeline will output which keys need to be filled.

#### Example Model YAMLs

`config.yml`:

```yaml
    primary_key:
      truncated_account_id
    label:
      log_first_year_revenue
    test_size: 0.15
    random_state: 3
    n_splits: 3
    metrics:
      - baseline error
      - correct rank probability
```

`model_rf_regressor.yml`:

```yaml
    model:
      RandomForestRegressor
    params:
      n_estimators: 1000
      max_depth: 20

```

`config.yml` says that the primary key to split the training set on is truncated_account_id and that size of the test set should be 15% of the overall labeled dataset and that it will use a random_state of 3 to determine which primary keys to include in which set. If the modeler wants cross validation, then the n_splits key specifies that it will be 3-fold cross validation. The model metrics that will be outputted are baseline error rates and correct rank probability. The label that we're trying to predict is log_first_year_revenue.

`model_rf_regressor.yml` states that we want to instantiate a RandomForestRegressor model with 1000 decision trees (n_estimators) each with a max depth of 20 nodes.

### Train Models:

Classes: ModelTrainer

Just like ModelEvaluator, ModelTrainer looks for a `config.yml` and a YAML file specific to each model. `config.yml` should have a train key which specifies which models will be trained. Each of the entries under train should correspond to a model yaml file. In the below example, the train key specifies that a random forest regressor will be trained on the full labeled dataset and that ModelTrainer should look in the `model_rf_regressor.yml` for model hyper-parameter settings.

Models will be trained on the entire merged labeled dataset and will be saved as .pkl files to the model_dir directory. ModelTrainer also takes a `--parallel` argument, when this flag is passed then it will train models in parallel. Note, training models in parallel that incorporate monte carlo or bootstrapping methods will be very slow on most machines.

#### Example Model Config YAML w/ Training

`config.yml`:

```yaml
    primary_key:
      truncated_account_id
    label:
      log_first_year_revenue
    test_size: 0.15
    random_state: 3
    n_splits: 3
    metrics:
      - baseline error
      - correct rank probability
    train:
      - model_rf_regressor
```

### Generate Predictions

Classes: ModelTrainer

In order to generate predictions, `config.yml` should have a train key which specifies which models will be trained. Each of the entries under train should correspond to a model YAML file. In the below example, the train key specifies that a random forest regressor will be trained on the full labeled dataset and that ModelTrainer should look in the `model_rf_regressor.yml` for model hyper-parameter settings.

Where `--parallel` is only specified if we want to get predictions in parallel for a set of a models. The main configuration file (config.yml) in the yaml directory (src/models/yaml) specifies which models will be used for prediction. These models are specified under the predict key. Note, config.yml expects to receive model names that are the same as the individual YAML files corresponding to each model. All predictions will be stored in models/predictions directory. Only primary keys that are in the merged targets dataset will have predictions - all other primary keys do not have the necessary features (the ones produced from the various nan policies) to get predictions.

#### Example Model Config YAML w/ Prediction

`config.yml`:

```yaml
    primary_key:
      truncated_account_id
    label:
      log_first_year_revenue
    test_size: 0.15
    random_state: 3
    n_splits: 3
    metrics:
      - baseline error
      - correct rank probability
    train:
      - model_rf_regressor
    predict:
      - model_rf_regressor
```

This YAML is saying to generate predictions for a Random Forest regression model and that it expects `model_rf_regressor.yml` to exist in yaml_dir.

### Class Imbalance

There are two methods available to fix class imbalance:

* `oversampling`: utilizes an implementation of SMOTE - Synthetic Minority Over-sampling Technique from [imbalance-learn](https://imbalanced-learn.readthedocs.io/en/stable/generated/imblearn.over_sampling.SMOTE.html).
* `combination`: utilizes an implementation implements for combining [over- and undersampling methods (SMOTEEN)](https://imbalanced-learn.readthedocs.io/en/stable/combine.html).

To select a method, modify `config.yml` as follows:

```yaml
    primary_key:
      truncated_account_id
    label:
      log_first_year_revenue
    test_size: 0.15
    random_state: 3
    n_splits: 3
    metrics:
      - baseline error
      - correct rank probability
    train:
      - model_rf_regressor
    predict:
      - model_rf_regressor
    balance_dataset:
      - oversampling
```

### Model (Hyper-parameter) Optimization

Class: ModelTuner

Hyper-parameter tuning might not be critical for most use cases but can significantly affect the efficiency of your processes and the quality of the results you are trying to obtain (e.g. you can run in boots but you might not run as fast as with trainers). Hyperparameter optimization should be performed whenever a new model is implemented, or when the dataset changes significantly.

**NOTE**: This Class assumes the model has a `set_params` method.

To tune models, we leverage the python package Hyperopt, a hyper-parameter search package that implements various search algorithms for Sequential model-based optimization (SMBO, aka Bayesian optimization) to find the best set of hyper-parameters within a search space. Bayesian optimization finds the value that minimizes an objective function by building a surrogate function (probability model) based on past evaluation results of the objective.

The model's name should be listed under `tune_models` in the `config.yml`. In addition, the parameters to tune and how the parameter space is defined should be included in the model's YAML file. In the below example, the `tune_models` key specifies that a random forest regressor will be optimized on the full labeled dataset and that ModelTuner should look in the `model_rf_regressor.yml` for model hyper-parameter settings.

Note that `--parallel` is only specified if we want to get predictions in parallel for a set of a models. All results (trials file and best parameters results) related to the hyper-parameter optimization will be saved under the tuned model folder (`models/tuned_models`), using the model's name and dataset date (for example, `hyperparameter_results_model_rf_regressor20190124.pkl` contains to the optimized best parameters for `model_rf_regressor` with dataset date of `2019-01-24`).

#### Example Model Config YAML w/ Hyperparameter Optimization

`config.yml`:

```yaml
    primary_key:
      truncated_account_id
    label:
      log_first_year_revenue
    test_size: 0.15
    random_state: 3
    n_splits: 3
    metrics:
      - baseline error
      - correct rank probability
    train:
      - model_rf_regressor
    predict:
      - model_rf_regressor
    tune_models:
      - model_rf_regressor

```

This YAML is saying to perform hyperparameter optimization for a Random Forest regression model and that it expects `model_rf_regressor.yml` to exist in yaml_dir.

The ModelTuner expects the following structure when specifying a hyperparameter space:

`model_rf_regressor.yml`:

```yaml
    model:
      RandomForestRegressor
    params:
      n_estimators: 1000
      max_depth: 20
    tune_params:
      max_eval: 1000
      _name_of_parameter_:
        function: _function_name_
        _function_keys_: value

```

For each of the expressions, or functions, that can be used to construct the parameter space for a specific parameter, the yaml file will expect different `_function_keys_`. For more information, please refer to [hyperopt documentation](https://github.com/hyperopt/hyperopt/wiki/FMin).

* choice
  Expects `_function_keys_`: `options`
Gives the user the option to define a range. Expects `_function_keys_` = `options`, which can be a list (i.e. `[1, 2, 3]` or a function `np.arange(1, 5, 1, dtype=int)`).

Usage:

```yaml
      max_depth:
        function: choice
        options: np.arange(1, 5, 1, dtype=int)
```

* randint
  Expects `_function_keys_`: `upper`
Returns random integer in the range [0, upper), where upper is the `_function_key_`. As sunch, expects `_function_keys_` = `upper`, which is an integer.

Usage:

```yaml
      max_depth:
        function: randint
        upper: 10
```

* uniform, loguniform
  Expects `_function_keys_`: `low` and `high`
`uniform` returns a value uniformly between the `_function_keys_`  `low` and `high`. `loguniform` returns a value drawn according to `exp(uniform(low, high))` so that the logarithm of the return value is uniformly distributed.

Usage:

```yaml
      max_depth:
        function: uniform
        low: 10
        high: 100
```

* normal, lognormal
  Expects `_function_keys_`: `mu` and `sigma`
`normal` returns a real value that's normally-distributed with mean (`_function_key_` `mu`) and standard deviation (`_function_key_` `sigma`). When optimizing, this is an unconstrained variable.
`lognormal` returns a value drawn according to `exp(normal(mu, sigma))` so that the logarithm of the return value is normally distributed. When optimizing, this variable is constrained to be positive.

Usage:

```yaml
      max_depth:
        function: normal
        mu: 10
        sigma: 1
```

* quniform, qloguniform
  Expects `_function_keys_`: `low`, `high`, and `q`
`quniform` returns a value like `round(uniform(low, high) / q) * q`. Suitable for a discrete value with respect to which the objective is still somewhat "smooth", but which should be bounded both above and below.
`qloguniform` returns a value like `round(exp(uniform(low, high)) / q) * q`. Suitable for a discrete variable with respect to which the objective is "smooth" and gets smoother with the size of the value, but which should be bounded both above and below.

Usage:

```yaml
      max_depth:
        function: qloguniform
        low: 1
        high: 10
        q: 1
```

#### Once the optimization is complete

Update the model's YAML file with the best set of parameters, under the `params` key. Model optimization results (including best parameter set) are saved under the tuned model folder (`models/tuned_models`), using the model's name and dataset date (for example, `hyperparameter_results_model_rf_regressor20190124.pkl`).

For example, here we set 3 optimized parameters for `model_rf_regressor.yml`.

`model_rf_regressor.yml`:

```yaml
    model:
      RandomForestRegressor
    params:
      n_estimators: 1000
      max_depth: 20
      min_samples_leaf: 10

```

### Setting up a new model

To set up a new model, you should:

* Create a new directory in `predictive-models` following the `_example` subdirectory
* Create SQL queries and YAML files in src/data/yaml (query_kwargs.yml must contain the set intersection of query arguments across all your SQL queries)
* Add `get_raw_data.py` and `get_clean_data.py` to src/data, both scripts should leverage methods in DataCleaner and DataImporter
* Add YAML files in src/features/yaml specifying feature engineering operations for each feature set
* Add `get_engineered_data.py`, `get_merged_data.py`, and `feature_engineering_functions.py` to src/features if you're using user defined feature engineering functions. These scripts should leverage methods in DataEngineer and DataMerger
* Add YAML files in src/models/yaml specifying training configurations and model hyperparameters
* Add `get_model_metrics.py`, `get_trained_models.py`, and `get_predictions.py` to src/models. These scripts should leverage methods in ModelEvaluator and ModelTrainer
* Document the model with a `README.md`
* Add tests (when possible)

For examples of each of these scripts, please see the account_scoring model in `predictive-models`

Key components include:

* Determining which feature sets are important to the model
* Determining which values in your data need to be replaced (if any)
* Adding new model metrics or feature engineering functions to `classypy.ml.feature_transformers` and `classypy.ml.model_metrics` respectively


### Common questions

#### How do I define a feature set?

For this pipeline, we assume that feature sets are independent. Feature sets ought to be defined outside of the pipeline, how they are defined should come from prior analysis/reports/model testing/or some other form of prior knowledge. Feature sets will be defined by the modeler but they should be defined in such a way that feature engineering and raw data sourcing can be done independent of other feature sets. The assumption is that the modeler has already found statistical dependencies in the data for which a machine learning model can utilize and that the modeler understands the data well enough that they can form discrete feature sets ex-ante. All analysis and testing (including notebook creation) should be done outside of the pipeline.

#### How do I add a new model package?

All model packages must be pre-loaded in ModelLoader. The individual model YAML files specify which models will be instantiated but ultimately the string objects that the YAML files specify are evaluated into model objects in the ModelLoader class.

#### How do I add a new feature engineering method?

Feature engineering methods that are expected to be used multiple times should be added to classypy.ml.feature_transformers.

#### How do I add a new model metrics method?

Functions for evaluating model performance that are expected to be used multiple times should be added to classypy.ml.model_metrics.

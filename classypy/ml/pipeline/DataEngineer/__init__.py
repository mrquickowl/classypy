import os.path as op

import nltk
import pandas as pd
from sklearn.externals import joblib

import classypy.ml.feature_transformers as ft
from classypy.devops import logger
from classypy.ml.pipeline.DataTransformer import DataTransformer
from classypy.util.caching import cache_dataframe
from classypy.util.execution import run_in_parallel


class DataEngineer(DataTransformer):
    ## Class for feature engineering, first type-cast columns then run user-defined functions ##
    def __init__(self, config_obj=None):
        self.fitted_transformer = None
        self.config_obj = config_obj

    @classmethod
    def load_data(cls, configs, data_dir, dataset_date):
        """This function loads all the raw data files indicated by the keys in the configs dictionary.

        Parameters
        ----------
        configs : dict
            Dictionary where the keys are file paths and the values are a yaml_obj
        data_dir : str
            File path to where data is being stored
        dataset_date : str
            Date for which the raw data is filtered on

        Return:
        -------
        data_dict : dict
            Dictionary where the keys are the names of the config files and the values are dataframes
        """
        data_dict = {}
        is_targets = False

        # Nested for loop necessary to get both targets and features data
        for feature_set_name, feature_set_config in configs.items():
            dataset_name = feature_set_name.split('.')[0].replace("_", " ")
            logger.info("Loading clean {dataset}".format(dataset=dataset_name))

            is_targets = ('targets' in dataset_name)
            if feature_set_name.replace('.yml', '') in ['labels', 'targets']:
                continue

            csv_file = cls.get_csv_file(filename=feature_set_config.filename,
                                        csv_file_dir=data_dir,
                                        is_targets=is_targets,
                                        dataset_date=dataset_date,
                                        suffix="clean")

            data_dict[feature_set_name] = pd.read_csv(csv_file)

        return data_dict

    def _process_boolean_features(self, df):
        """This function casts all columns that are specified to be of type boolean as boolean types

        Parameters
        ----------
        df : dataframe
            Dataframe containing columns that need to be processed

        Return:
        -------
        A dataframe object where the boolean columns under the feature_types key are cast as boolean

        """
        if "boolean" in self.config_obj.feature_types:
            for bool_col in self.config_obj.yaml_dict['feature_types']['boolean']:
                bool_col = self.add_prefix(text=bool_col, prefix=self.config_obj.prefix)
                df = ft.make_bool_col(df=df, col=bool_col)

        return df

    def _process_categorical_features(self, df):
        """This function casts all columns that are specified to be of type categorical as categorical types

        Parameters
        ----------
        df : dataframe
            Dataframe containing columns that need to be processed

        Return:
        -------
        A dataframe object where the categorical columns under the feature_types key are cast as categorical

        """
        if "categorical" in self.config_obj.feature_types:
            for cat_col in self.config_obj.yaml_dict['feature_types']['categorical']:
                cat_col = self.add_prefix(text=cat_col, prefix=self.config_obj.prefix)
                df = pd.get_dummies(data=df, columns=[cat_col], prefix=self.config_obj.prefix, dummy_na=True)

        return df

    def _process_numeric_features(self, df):
        """This function casts all columns that are specified to be of type numeric as float types

        Parameters
        ----------
        df : dataframe
            Dataframe containing columns that need to be processed

        Return:
        -------
        A dataframe object where the numeric columns under the feature_types key are cast as float

        """
        if "numeric" in self.config_obj.feature_types:
            for num_col in self.config_obj.yaml_dict['feature_types']['numeric']:
                num_col = self.add_prefix(text=num_col, prefix=self.config_obj.prefix)
                df[num_col] = df[num_col].astype(float)
        return df

    def _process_textual_features(self, df):
        """This function casts all columns that are specified to be of type textual as float types

        Parameters
        ----------
        df : dataframe
            Dataframe containing columns that need to be processed

        Return:
        -------
        A dataframe object where the textual columns under the feature_types key are cleaned up.
        Specifically, the text is lowercased, punctuation is removed, and spaces are normalized.

        """
        from classypy.ml.nlp.text_cleaning import TextCleaner
        cleaner = TextCleaner()

        if "textual" in self.config_obj.feature_types:
            for text_col in self.config_obj.yaml_dict['feature_types']['textual']:
                text_col = self.add_prefix(text=text_col, prefix=self.config_obj.prefix)
                df[text_col] = [cleaner.clean_text(text=x,
                                                   lowercase=True, remove_punctuation=True,
                                                   normalize=True) for x in df[text_col]]
        return df

    @cache_dataframe
    def transform(self, df):
        """This function transforms dataframe by first typing all the columns then performing user defined feature
        engineering procedures. config_obj is a dictionary with the following structure
        {feature_types: {'textual': [cols], 'boolean': [cols], 'numeric': [cols], 'categorical': [cols]},
         feature_engineer: {'udf_keys': [cols]}}

        Note, each of the keys are optional - if they are not presented then features won't be typed/engineered.

        Parameters
        ----------
        df : dataframe
            Dataframe containing columns that need to be transformed

        Return:
        -------
        A dataframe object where the features are correctly typed and engineered

        """
        # Cast data columns as their specified types
        if self.config_obj.feature_types:
            df = self._process_numeric_features(df=df)
            df = self._process_boolean_features(df=df)
            df = self._process_textual_features(df=df)
            df = self._process_categorical_features(df=df)

        # Process dataset using user-defined methods
        if self.config_obj.feature_engineer:
            transform_dict = {'scale': self.transform_scaled_features,
                              'log': self.transform_log_features,
                              'sentiment': self.transform_sentiment_features,
                              'comparison': self.transform_boolean_comparison_features,
                              'pca': self.transform_pca_features}
            for feature_engineer_operation in self.config_obj.feature_engineer:
                transform_function = transform_dict[feature_engineer_operation]
                df = transform_function(config_obj=self.config_obj, df=df)

        return df

    @staticmethod
    def run(configs, data, dataset_date, force, model_dir, output_dir, is_targets, parallel):
        """This function processes datasets in parallel

        Parameters
        ----------
        configs : dict
            Dictionary where the keys are file paths and the values are a yaml_obj
        data : dict
            Dictionary where the keys are the same as the keys in config and the values are dataframes
        dataset_date : str
            Date on which the raw data was sourced
        force : bool, optional
            Forces cached data to be overwritten (Default: False)
        model_dir : str
            Directory where fitted transformers will be stored
        output_dir : str
            Directory where cleaned up csv files will be stored
        is_targets : bool
            Indicates whether or not to clean features for target dataset
        parallel : bool
            Indicates whether or not to process datasets in parallel

        Return:
        -------
        Nothing, this function generates a series of csv files containing feature engineered data

        """
        def _run(df, config_obj, dataset_date, force, model_dir, output_dir, is_targets):
            engineer = DataEngineer(config_obj=config_obj)
            # Get base csv file name
            feature_set_name = config_obj.filename

            # Set pickled transformer name
            model_path = op.join(model_dir, engineer.get_dated_file_name(filename=feature_set_name,
                                                                         dataset_date=dataset_date,
                                                                         file_type=".pkl"))

            # Prepend csv file name with target and load fitted transformer if one exists
            if is_targets:
                feature_set_name = "targets" + "_" + feature_set_name
                logger.info("Working on target {prefix} feature engineering".format(prefix=config_obj.prefix))
                engineer.load(model_input_path=model_path)

            else:
                logger.info("Working on {prefix} feature engineering".format(prefix=config_obj.prefix))
                engineer.fit(config_obj=config_obj, df=df)
                engineer.dump(model_output_path=model_path)

            # Set output data path for engineered features
            processed_dataset_csv_file = op.join(output_dir,
                                                 engineer.get_dated_file_name(filename=feature_set_name + "_engineered",
                                                                              dataset_date=dataset_date))
            df = engineer.transform(df=df, csv_file=processed_dataset_csv_file, force=force)

            return df

        # The below logic is necessary to avoid nltk breaking multiprocessing
        try:
            nltk.data.find('corpora/stopwords.zip')
        except LookupError:
            pass  # totally valid; user will know if they need stopwords.
        else:
            # Once NLTK loads stopwords in one process,
            # every other process thinks that it's loaded when they do not have access to the data in stopwords.
            # Reference: https://github.com/nltk/nltk/issues/947
            nltk.corpus.stopwords.fileids()

        param_list = []

        # Create list of feature engineering arguments out of yaml files
        for feature_set_name, feature_set_config in configs.items():
            if feature_set_name.replace('.yml', '') in ['labels', 'targets']:
                continue
            params = {'df': data[feature_set_name],
                      'config_obj': configs[feature_set_name],
                      'dataset_date': dataset_date,
                      'model_dir': model_dir,
                      'output_dir': output_dir,
                      'force': force,
                      'is_targets': ("targets" in feature_set_name)}
            param_list.append(params)

        if parallel:
            return run_in_parallel(func=_run, params=param_list)
        else:
            return [_run(**args) for args in param_list]

    def fit(self, config_obj, df):
        """This method fits according to some user defined function

        Parameters
        ----------
        config_obj : dict
            Dictionary containing settings for how feature engineering should be done
        df : dataframe
            Dataframe on which to fit a feature transformer
        """
        fit_dict = {'pca': self.fit_pca_features}
        for feature_engineer_operation in config_obj.feature_engineer:
            try:
                fit_function = fit_dict[feature_engineer_operation]
                fitted_transformer = fit_function(config_obj=config_obj, df=df)
                self.fitted_transformer = fitted_transformer
            except KeyError:
                logger.debug("{feature_engineer_operation} for {prefix} does not have a fit method".format(
                    feature_engineer_operation=feature_engineer_operation, prefix=config_obj.prefix))

    def dump(self, model_output_path):
        """This method dumps a fitted transformer

        Parameters
        ----------
        model_output_path : str
            Path to where fitted transformer will be dumped
        """
        try:
            if self.fitted_transformer:
                with open(model_output_path, 'wb') as f:
                    joblib.dump(self.fitted_transformer, f)
        except IOError:
            logger.debug("Fitted transformer not found")
            pass

    def load(self, model_input_path):
        """This method loads a fitted transformer

        Parameters
        ----------
        model_input_path : str
            Path to where fitted transformer will be loaded
        """
        try:
            with open(model_input_path, 'rb') as f:
                self.fitted_transformer = joblib.load(f)
        except IOError:
            logger.debug("Fitted transformer not found")
            pass

    def fit_transform(self, df, config_obj):
        """This method calls self.fit and self.transform, it returns the a dictionary of transformed data.
        Each dataframe is transformed according to the rules specified in configs. Note that data
        and configs must share the same keys.

        Parameters
        ----------
        config_obj : dict
            Dictionary specifying how data needs to be fit and transformed
        df : dict
            Dataframe on which to fit and transform data

        Returns
        -------
        data : dict
            Contains transformed data
        """

        self.fit(df=df)
        data = self.transform(df=df, config_obj=config_obj)

        return data

    @classmethod
    def transform_scaled_features(cls, config_obj, df):
        """
        Look for a scale keyword under the feature_engineer key, if found then scale each numerator value
        by each denominator value

        Parameters:
        -----------
        config_obj : dict
            Dictionary containing specifications about what should be scaled
        df : dataframe object
            A dataframe object that contains cleaned up feature data to be engineered

        """
        for feature_engineering_dict in config_obj.yaml_dict['feature_engineer']['scale']:
            numerator = cls.add_prefix(text=feature_engineering_dict['numerator'], prefix=config_obj.prefix)
            denominator = cls.add_prefix(text=feature_engineering_dict['denominator'], prefix=config_obj.prefix)

            if "*" in numerator:
                base = numerator.replace("*", "")
                numerators = [x for x in config_obj.yaml_dict['feature_types']['numeric'] if base in x]
            else:
                numerators = [numerator]

            for numerator in numerators:
                df = ft.make_scale_col(df=df, col=numerator, scale_col=denominator, fill_inf=True, fill_inf_value=0,
                                       fill_na=False)

        return df

    @classmethod
    def transform_log_features(cls, config_obj, df):
        """
        Look for the log keyword under the feature_engineer key, if found then log each value,
        values less than 0 are floored at 1 (resulting in a value of 0)

        Parameters:
        -----------
        config_obj : dict
            Dictionary containing specifications about what should be logged
        df : dataframe object
            A dataframe object that contains cleaned up feature data to be engineered

        """
        for log_col in config_obj.yaml_dict['feature_engineer']['log']:
            df = ft.make_log_col(df=df, col=log_col, prefix=config_obj.prefix)

        return df

    @classmethod
    def transform_sentiment_features(cls, config_obj, df):
        """
        Look for the sentiment keyword under the feature_engineer key, if found then create sentiment
        features based on a textual data column

        Parameters:
        -----------
        config_obj : dict
            Dictionary containing specifications about how to generate sentiment features
        df : dataframe object
            A dataframe object that contains cleaned up feature data to be engineered

        """
        for text_col in config_obj.yaml_dict['feature_engineer']['sentiment']:
            df = ft.make_sentiment_cols(df=df, col=text_col, prefix=config_obj.prefix)

        return df

    @classmethod
    def transform_boolean_comparison_features(cls, config_obj, df):
        """
        Look for the comparison keyword under the feature_engineer key, if found then create a boolean
        column based on the result of the comparison between two given columns in df

        Parameters:
        -----------
        config_obj : dict
            Dictionary containing specifications about how to generate comparison features
        df : dataframe object
            A dataframe object that contains cleaned up feature data to be engineered

        """
        for feature_engineering_dict in config_obj.yaml_dict['feature_engineer']['comparison']:
            greater_col = cls.add_prefix(text=feature_engineering_dict['greater'], prefix=config_obj.prefix)
            lesser_col = cls.add_prefix(feature_engineering_dict['lesser'], prefix=config_obj.prefix)
            df = ft.make_comparison_ind_col(df=df, greater_col=greater_col, prefix=config_obj.prefix,
                                            lesser_col=lesser_col)

        return df

    @classmethod
    def transform_pca_features(cls, config_obj, df, fitted_transformer=None):
        """
        Look for the pca keyword under the feature_engineer key, if found then compute a PCA representation of df. Note,
        df will be subsetted for the columns indicated in the feature engineering yaml file for a given feature set.

        Parameters:
        -----------
        config_obj : dict
            Dictionary containing specifications about how to generate PCA features
        df : dataframe object
            A dataframe object that contains feature data to be engineered
        fitted_transformer : fitted pca object
            A sklearn PCA model that has been fit on the data in df
        """
        n_components = config_obj.yaml_dict['feature_engineer']['pca']['params']['n_components']
        linear = config_obj.yaml_dict['feature_engineer']['pca']['params']['linear']
        cols = config_obj.yaml_dict['feature_engineer']['pca']['cols']
        cols = [config_obj.prefix + "_" + c for c in cols]
        pca_df = df[cols]
        _, pca_df = ft.make_pca_cols(df=pca_df, n_components=n_components, linear=linear, cols=cols,
                                     prefix=config_obj.prefix, fitted_transformer=fitted_transformer)
        df = pd.concat([df, pca_df.reset_index(drop=True)], axis=1)

        return df

    @classmethod
    def fit_pca_features(cls, config_obj, df):
        """
        Look for the pca keyword under the feature_engineer key, if found then compute a PCA representation of df. Note,
        df will be subsetted for the columns indicated in the feature engineering yaml file for a given feature set.

        Parameters:
        -----------
        config_obj : dict
            Dictionary containing specifications about how to fit a PCA model
        df : dataframe object
            A dataframe object that contains feature data to be engineered
        """
        n_components = config_obj.yaml_dict['feature_engineer']['pca']['params']['n_components']
        linear = config_obj.yaml_dict['feature_engineer']['pca']['params']['linear']
        cols = config_obj.yaml_dict['feature_engineer']['pca']['cols']
        cols = [config_obj.prefix + "_" + c for c in cols]
        pca_df = df[cols]
        fitted_transformer, _ = ft.make_pca_cols(df=pca_df, n_components=n_components, linear=linear, cols=cols,
                                                 prefix=config_obj.prefix)

        return fitted_transformer

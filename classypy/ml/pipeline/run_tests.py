import os

import pytest

from classypy.ml.pipeline.DataImporter import DataImporter
from classypy.util.dirs import this_files_dir


class FanoutTestException(Exception):
    def __init__(self, num_test_failures):
        super(FanoutTestException, self).__init__("Failed {num_test_failures} tests.".format(
            num_test_failures=num_test_failures))


def run(qf, output_dir, query_dir, yaml_dir, n_targets, dataset_date, primary_key_value, query_kwargs, force,
        env=None, env_file=None):
    """This function calls pytest and tests all the SQL queries to ensure that
    the primary key is not duplicated.

    Parameters
    ----------
    qf : func (query.generate_query_func)
        Closure that can query a MySQL database through an SSH-tunnel or locally.
    output_dir : str
        Directory where cleaned up csv files will be stored
    query_dir : path
        Path of queries' directory
    yaml_dir : str, optional
        Path to yaml files directory
    primary_keys : list-like
        List of primary keys, will be used for the feature SQL queries
    n_targets : int
        Controls how many target accounts to get
    dataset_date : str
        Specifies the date for which we are pulling raw data
    primary_key_value : str
        Specifies the primary key on which we will test the queries
    query_kwargs : dict
        Dictionary containing any additional variables to be passed to the downstream SQL query
    force : bool, optional
        Forces cached data to be overwritten (Default: False)
    """
    assert env or env_file, "Must specify env or env_file parameter"

    # Get primary keys
    importer = DataImporter(qf=qf, output_dir=output_dir, query_dir=query_dir, dataset_date=dataset_date, force=force,
                            config_dir=yaml_dir, is_targets=bool(n_targets))

    # Labels primary keys
    primary_keys = importer.get_primary_keys(primary_key_col=primary_key_value, is_targets=False,
                                             query_kwargs=query_kwargs)

    if n_targets is not None:
        target_query_kwargs = {'n_targets': n_targets}
        target_query_kwargs.update(query_kwargs)
        targets_primary_keys = importer.get_primary_keys(primary_key_col=primary_key_value, is_targets=True,
                                                         query_kwargs=target_query_kwargs)
        primary_keys = list(primary_keys) + list(targets_primary_keys)
        n_targets = str(n_targets)
    else:
        n_targets = 0

    primary_keys = importer.format_primary_keys_for_sql(primary_keys=primary_keys)
    args = ['-x', "--dataset-date", dataset_date, '--primary-keys', primary_keys,
            '--n-targets', n_targets, '--primary-key', primary_key_value, '--query-dir', query_dir,
            '--query-kwargs', str(query_kwargs)]

    if env:
        args += ['--env', env]
    if env_file:
        args += ['--env-file', env_file]

    try:
        wd = os.getcwd()
        os.chdir(this_files_dir())
        num_test_failures = pytest.main(args=args)
    finally:
        os.chdir(wd)

    if num_test_failures > 0:
        raise FanoutTestException(num_test_failures=num_test_failures)

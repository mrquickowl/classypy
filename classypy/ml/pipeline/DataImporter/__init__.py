"""Object for handling raw data"""
from collections import OrderedDict

import six

import classypy.query as qt
from classypy.devops import logger
from classypy.ml.pipeline.DataTransformer import DataTransformer
from classypy.util.caching import cache_dataframe


class DataImporter(DataTransformer):
    """Class containing generic methods for dealing with importing raw data"""

    def __init__(self, output_dir, force=False, query_dir=None, qf=None, dataset_date=None, config_dir=None,
                 yaml_file=None, is_targets=False):
        super(DataImporter, self).__init__(config_dir=config_dir, is_targets=is_targets)

        self.data = {}
        self.dataset_date = dataset_date
        self.force = force
        self.output_dir = output_dir
        self.query_dir = query_dir
        self.qf = qf

    def save_data(self):
        """Saves data from redshift. This method works by generating a call to redshift using the query_func object.
        DataImporter's YAMLFile object should contain the name of the SQL query as well as the arguments necessary to
        run the SQL query. This function calls import_data with the csv_file argument (joined with the output directory)
        in order to handle saving and caching the resulting data from the SQL query.

        Return:
        -------
        df : dataframe

        """
        @cache_dataframe
        def _cache_data(df):
            return df

        if not self.data:
            logger.warning("Data dictionary is empty. Please make sure to call load_data first")

        for csv_file in self.data.keys():
            _cache_data(csv_file=csv_file, force=self.force, df=self.data[csv_file])

    def load_data(self, query_kwargs, primary_key_col, n_targets):
        """Loads feature set data (labels and targets if N-targets is not none)

        Parameters
        ----------
        query_kwargs : dict, optional
            Dictionary containing any additional variables to be passed to the downstream SQL query
        primary_key_col : str
            Specifies a column in both labels_df and target_df (if targets specified) that
            contains the unique identifier for which we will pull raw features
        n_targets : int, optional
            Controls how many target accounts to get

        Return:
        -------
        primary_keys: list
            A list containing unique primary keys from the labels and/or targets dataset
        """
        feature_config_files = [x for x in self.configs if "feat" in x]

        # Labels primary keys
        primary_keys = self.get_primary_keys(primary_key_col=primary_key_col, is_targets=False,
                                             query_kwargs=query_kwargs)

        dataset_dict = OrderedDict()
        dataset_dict['label'] = {'is_targets': False, 'primary_keys': list(primary_keys)}

        if n_targets is not None:
            target_query_kwargs = {'n_targets': n_targets}
            target_query_kwargs.update(query_kwargs)
            targets_primary_keys = self.get_primary_keys(primary_key_col=primary_key_col, is_targets=True,
                                                         query_kwargs=target_query_kwargs)
            dataset_dict['target'] = {'is_targets': True, 'primary_keys': list(targets_primary_keys)}

        for config_file in feature_config_files:
            self.yaml_obj = self.configs[config_file]
            logger.info("Extracting {dataset} from redshift".format(dataset=self.yaml_obj.filename))

            dataset = 'label'
            if 'targets' in config_file:
                dataset = 'target'

            primary_keys = dataset_dict[dataset]['primary_keys']
            query_kwargs["primary_keys"] = self.format_primary_keys_for_sql(primary_keys=primary_keys)
            csv_file = self.get_csv_file(filename=self.yaml_obj.filename,
                                         csv_file_dir=self.output_dir,
                                         is_targets=dataset_dict[dataset]['is_targets'],
                                         dataset_date=self.dataset_date)

            # Caching, if force attribute is set to true then always import data
            df = self._import_data(query_kwargs=query_kwargs)

            self.data[csv_file] = df

        return self.data

    @staticmethod
    def format_primary_keys_for_sql(primary_keys):
        """ Format primary keys to be used in a SQL query. String primary keys need to be wrapped
        in single quotations while numeric keys do not. Wrapping numeric keys in quotations leads
        to the wrong type being parsed by SQL alchemy.

        Parameters
        ----------
        primary_keys : array
            An array-like object containing primary keys to be formatted for SQL

        Returns
        -------
        A string object where the primary keys are properly formatted to be used in a SQL query
        """
        if any([isinstance(x, six.string_types) for x in primary_keys]):
            primary_keys = ["'" + str(x) + "'" for x in primary_keys]
        else:
            primary_keys = [str(x) for x in primary_keys]

        primary_keys = "{primary_keys}".format(primary_keys=','.join(primary_keys))

        return primary_keys

    @cache_dataframe
    def _import_data(self, query_kwargs):
        """This function imports data using a SQL query by wrapping the import_data method in classypy.query. It asserts
        that the resulting dataframe does not contain duplicates and outputs a log indicating query status.

        Parameters
        ----------
        query_kwargs : dict
            Additional SQL query arguments to be passed to qt.import_data

        Returns
        -------
        df : pd.DataFrame
            Dataframe consisting of data specified in SQL query.
        """
        df = qt.import_data(qf=self.qf, query_name=self.yaml_obj.query_name, query_dir=self.query_dir,
                            kwargs=query_kwargs)

        logger.info("Downloaded {n_rows} rows for {query_name}.".format(
            query_name=self.yaml_obj.query_name, n_rows=len(df)))

        if len(df) == 0:
            logger.warning("No data returned.")

        # Make sure there are no duplicate rows in raw data
        assert len(df.drop_duplicates()) == len(df)

        return df

    def get_primary_keys(self, primary_key_col, is_targets, query_kwargs):
        """This function sources a list of primary keys, either from a target dataset or from a
        labels dataset. This function assumes that there exists either a targets.yml or labels.yml
        file in config_dir.

        Parameters
        ----------
        primary_key_col : str, required
            Specifies a column in both labels_df and target_df (if targets specified) that
            contains the unique identifier for which we will pull raw features
        targets : Boolean, required
            Indicates whether or not to process features for target dataset
            By default, this is set to False
        query_kwargs : dict, required
            Dictionary object containing key word arguments, to be passed to self.load_data()
        Return:
        -------
        primary_keys: list
            A list containing unique primary keys from either the labels or targets dataset
        """
        if is_targets:
            logger.info("Extracting target account primary keys...")
            self.yaml_obj = self.configs["targets.yml"]
        else:
            logger.info("Extracting labels dataset with primary keys...")
            self.yaml_obj = self.configs["labels.yml"]

        primary_keys_df = self._import_data(query_kwargs=query_kwargs)
        primary_keys = primary_keys_df[primary_key_col].values

        # Assert that primary keys are unique
        assert(len(primary_keys) == len(set(primary_keys)))

        # Save primary keys - not targets must be set to false
        # otherwise we get the filename target_targets
        csv_file = self.get_csv_file(filename=self.yaml_obj.filename,
                                     csv_file_dir=self.output_dir,
                                     is_targets=False,
                                     dataset_date=self.dataset_date)
        self.data[csv_file] = primary_keys_df
        self.save_data()
        del self.data[csv_file]
        return primary_keys

    @staticmethod
    def transform(df):
        """This method returns the df that is passed to it, necessary to implement the DataTransformer interface"""
        return df

    @staticmethod
    def fit(df):
        """This method returns the df that is passed to it, necessary to implement the DataTransformer interface"""
        return df

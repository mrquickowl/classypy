"""Object for performing hyper-parameter tuning for models"""
import glob
import os
import os.path as op
from datetime import datetime
from functools import partial

import hyperopt
from hyperopt import STATUS_OK, Trials, fmin, hp
from sklearn.externals import joblib

from classypy.devops import logger
from classypy.ml.pipeline.DataImporter import DataImporter
from classypy.ml.pipeline.ModelEvaluator import ModelEvaluator
from classypy.ml.pipeline.ModelLoader import ModelLoader
from classypy.ml.pipeline.YAMLFile import YAMLFile
from classypy.util.execution import run_in_parallel


class ModelTuner(ModelLoader):
    """Class containing generic methods and attributes necessary for model hyper-parameter tuning"""

    def __init__(self, config_file, force=False, output_dir=None, model_dir=None, data_dir=None, dataset_date=None,
                 yaml_file=None, param_space=None, max_eval=None, tune_params=None):
        super(ModelTuner, self).__init__(config_file=config_file, force=force, output_dir=output_dir,
                                         data_dir=data_dir, dataset_date=dataset_date, yaml_file=yaml_file)
        # Initialize class attributes
        self.param_space = param_space
        self.max_eval = max_eval
        self.tune_params = tune_params
        self.force = force

        if model_dir:
            self.model_dir = model_dir
        else:
            self.model_dir = None

    @staticmethod
    def _hyperopt_params(tune_params):
        """This function transforms the dictionary of parameters into an hyperopt-acceptable dictionary

        Parameters
        ----------
        tune_params : dict
            Dictionary containing tuning attributes for each parameter to be optimized

        Returns
        -------
        param_space : dict
            Dictionary to be used in hyperopt's fmin function
        """
        # Define Parameter Space to be used in optimization
        param_space = {}
        params = list(tune_params.keys())

        for param in params:
            if param != 'max_eval':
                param_space.update({param: ModelTuner._get_hyperopt_function_params(param,
                                                                                    param_dict=tune_params[param])})
        return param_space

    @staticmethod
    def _load_hyperopt_space(function, function_dict):
        """ Evaluates function  specified in each parameter dictionary in tune_params in the model's yaml_file

        Parameters
        ----------
        function : str
            Name of hyperopt function to be evaluated
        function_dict : dict
            Dictionary containing hyperopt expression attributes

        Returns
        -------
        Hyperopt expression object
        """
        function = eval('hp.' + function)  # nosec
        return function(**function_dict)

    @staticmethod
    def _get_hyperopt_function_params(param, param_dict):
        """This function checks each model's YAML file for the necessary hyperopt function parameters and,
        if they exist, it then builds the equivalent hyperopt expressions

        Parameters
        ----------
        param : str
            Name of parameter to be optimized
        param_dict : dict
            Dictionary containing attributes to build the parameter space for param.

        Returns
        -------
        Hyperopt expression object for each param
        """
        function = param_dict['function']

        # QUniform, QLogUniform
        if function in ['quniform', 'qloguniform']:
            if all(attribute in param_dict.keys() for attribute in ['low', 'high', 'q']):
                function_dict = {
                    'label': param,
                    'low': param_dict['low'],
                    'high': param_dict['high'],
                    'q': param_dict['q']
                }
                return ModelTuner._load_hyperopt_space(function, function_dict)
            else:
                logger.warning(
                    "Parameters for parameter space function {} missing ['low', 'high', 'q'].".format(
                        function))

        if function in ['uniform', 'loguniform']:
            if all(attribute in param_dict.keys() for attribute in ['low', 'high']):
                function_dict = {
                    'label': param,
                    'low': param_dict['low'],
                    'high': param_dict['high']
                }
                return ModelTuner._load_hyperopt_space(function, function_dict)
            else:
                logger.warning(
                    "Parameters for parameter space function {} missing ['low', 'high'].".format(function))

        # Choice and Randint
        if function in ['choice', 'randint']:
            if all(attribute in param_dict.keys() for attribute in ['options']):
                return hp.choice(param, param_dict['options'])
            elif all(attribute in param_dict.keys() for attribute in ['upper']):
                return hp.randint(param, param_dict['upper'])
            else:
                logger.warning("Parameters for parameter space function {} missing.".format(function))

        # Normal, LogNormal
        if function in ['normal', 'lognormal']:
            if all(attribute in param_dict.keys() for attribute in ['mu', 'sigma']):
                function_dict = {
                    'label': param,
                    'mu': param_dict['mu'],
                    'sigma': param_dict['sigma']
                }
                return ModelTuner._load_hyperopt_space(function, function_dict)
            else:
                logger.warning(
                    "Parameters for parameter space function {} missing ['mu', 'sigma'].".format(function))

    def _fix_best_params(self, best_params):
        """ One of the annoying things about hyperopt is that it returns an index, and not the actual
        parameter, when the expression/function is choice. Here, we set a so that we can identify
        the best parameter after optimization is complete.

        Parameters
        ----------
        best_params : dict
            Dictionary containing the optimized set of parameters

        Returns
        -------
        best_params, with indexing replaced by actual best value when the expression function is 'choice'
        """
        for param in self.tune_params.keys():
            if (param != 'max_eval') and (self.tune_params[param]['function'] == 'choice'):
                best_params[param] = self.tune_params[param]['options'][best_params[param]]
        return best_params

    @staticmethod
    def _check_config_file_for_tuning_params(yaml_file, kwargs):
        """ Check if tuning_parameters are specified properly """
        if 'tune_params' in YAMLFile(yaml_file=yaml_file).yaml_dict.keys():
            if 'max_eval' in YAMLFile(yaml_file=yaml_file).yaml_dict['tune_params']:
                tune_params = YAMLFile(yaml_file=yaml_file).yaml_dict['tune_params']
                kwargs['tune_params'] = tune_params
                kwargs['max_eval'] = tune_params['max_eval']
                kwargs['param_space'] = ModelTuner._hyperopt_params(tune_params)
                return kwargs
            else:
                logger.warning(
                    "No max_eval parameter provided for {}. Halting tuning for this model.".format(
                        YAMLFile(yaml_file=yaml_file).filename.replace("_", " ")))
                return None
        else:
            logger.warning(
                "No tuning parameters provided for {}. Halting tuning for this model.".format(
                    YAMLFile(yaml_file=yaml_file).filename.replace("_", " ")))
            return None

    @staticmethod
    def optimize_params(yaml_dir, tuning_kwargs, parallel=False, force=False):
        """This function runs parameter optimization for the models in the config.yml.
        If the parallel flag is set to true, then model optimizations will be run in parallel

        Parameters
        ----------
        yaml_dir : str
            The directory containing the main config file, which specifies the models to generate predictions
        tuning_kwargs : dict
            A dictionary object containing arguments for which to load ModelTuner
        parallel : bool
            A boolean flag indicating whether or not to generate predictions in parallel

        Return:
        -------
        Nothing, this function generates a series of csv files containing predictions generated from trained models
        """
        def _optimize_params(**args):
            """ Run optimization trials. """
            tuner = ModelTuner(**args)

            if tuner.yaml_obj.filename in tuner.tune_models:
                logger.info(
                    "Preparing to tune {}.".format(tuner.yaml_obj.filename.replace("_", " ")))
                # Load Trials
                trials = tuner._load_trials(force=tuner.force)

                # Define Parameter space
                args.pop('max_eval')
                args.pop('param_space')
                args.pop('tune_params')

                # Define max evals
                # if trials have been run before, defines max_evals to be the remaining number of runs
                max_trials = tuner.max_eval - len(trials.trials)
                max_trials = 1 if max_trials <= 0 else max_trials

                # Call out objective function to optimize
                optimization_objective = partial(ModelTuner._hyperopt_objective, **args)

                logger.info("Optimizing {}...".format(tuner.yaml_obj.filename.replace("_", " ")))
                # Run hyper-parameter optimization
                best_params = fmin(
                    fn=optimization_objective,
                    space=tuner.param_space,
                    algo=hyperopt.tpe.suggest,
                    trials=trials,
                    max_evals=max_trials,
                )

                # If using choice, need to fix best_params
                best_params = tuner._fix_best_params(best_params=best_params)
                min_loss = ModelTuner._hyperopt_objective(params=best_params, **args)
                logger.info("Optimization for {} concluded. Best parameters: {}, Score: {}".
                            format(tuner.yaml_obj.filename.replace("_", " "), best_params, min_loss['loss']))
                # Save trials
                tuner._save_trials(trials=trials, force=True)
                tuner._save_best_params(best_params=best_params, loss=min_loss, force=True)

        yaml_files = glob.glob(op.join(yaml_dir, "model*"))
        args_list = []

        # Create list of model evaluation arguments out of yaml files
        for yaml_file in yaml_files:
            kwargs = tuning_kwargs.copy()
            kwargs['yaml_file'] = yaml_file
            kwargs = ModelTuner._check_config_file_for_tuning_params(yaml_file=yaml_file, kwargs=kwargs)
            if kwargs:
                args_list.append(kwargs)
        if parallel:
            run_in_parallel(func=_optimize_params, params=args_list)
        else:
            for args in args_list:
                _optimize_params(**args)

    def _load_trials(self, force=False):
        """This function loads trials file if they're existing, overwrites or loads existing ones
        depending on the force flag passed.

        Parameters
        ----------
        force : boolean
            When True, creates and saves new trials file
            When False (default), check if file exists: if it does, loads it; if it doesn't, creates new one.

        Returns
        -------
        Trials object
        """
        # Get model name to index trials file by
        model_name = self.yaml_obj.filename
        filename = 'hyperparameter_trials_' + model_name

        # Define trial path
        trial_filename = DataImporter.get_dated_file_name(
            filename=filename, dataset_date=self.dataset_date, file_type=".pkl")
        trials_filepath = op.join(self.model_dir, trial_filename)

        if not op.exists(self.model_dir):
            os.makedirs(self.model_dir)

        # Check if trials exist
        if not op.exists(trials_filepath):
            # If not, create new Trials file
            logger.info("No existing Trials found for {}... Creating new trials".format(model_name))
            trials = Trials()
            self._save_trials(trials=trials, force=True)
        else:
            # Else, load trials
            with open(trials_filepath, 'rb') as f:
                trials = joblib.load(f)
            logger.info("Loaded trials for {}, dated {}.".format(model_name, self.dataset_date))

        # If force flag, always rewrite trials
        if force:
            # If force is true, rewrite trials file
            logger.info("Creating new trials for {}.".format(model_name))
            trials = Trials()
            self._save_trials(trials=trials, force=True)

        return trials

    def _save_trials(self, trials, force=False):
        """Saves trials file.

        Parameters
        ----------
        trials : obj
            Hyperopt trials object
        force : boolean
            When True, saves trials file. If False (Default), raises a warning.

        This function does not return anything but saves the trials file.
        """
        # Get model name to index trials file by
        filename = 'hyperparameter_trials_' + self.yaml_obj.filename

        # Define trial path
        trial_filename = DataImporter.get_dated_file_name(
            filename=filename,
            dataset_date=self.dataset_date,
            file_type=".pkl")
        trials_filepath = op.join(self.model_dir, trial_filename)

        if not op.exists(self.model_dir):
            os.makedirs(self.model_dir)

        if force:
            logger.info("Saved trials for {}".format(self.yaml_obj.filename))
            with open(trials_filepath, 'wb') as f:
                joblib.dump(trials, f)
        else:
            raise logger.warning("Did not save trials. Please try again using force=True.")

    def _hyperopt_objective(params, **args):
        """Objective function for hyper-parameter optimization: creates model, set parameters,
        and performs training.

        Parameters
        ----------
        params : dict
            Parameter dictionary, passed by the fmin hyperopt function

        Returns
        -------
        Dictionary containing metric, parameter set, and a hyperopt flag for the model's run result
        """
        args.pop('model_dir')  # Model Evaluator does not take this arg
        evaluator = ModelEvaluator(**args)
        # Set params from param_space
        evaluator.model.set_params(**params)
        # Get all metrics and results
        cv_results = evaluator.get_cv_results(X_train=evaluator.X_train, y_train=evaluator.y_train)

        # Scores that are better as they increase need to be converted for minimization
        if 'f1 score' in list(cv_results.keys()):
            loss = 1 - cv_results['f1 score']
        elif 'mse' in list(cv_results.keys()):
            loss = cv_results['mse']
        else:
            loss = list(cv_results.values())[0]

        logger.info("Using score: {} - Loss: {}, Params: {}".format(list(cv_results.keys()), loss, params))
        return {'loss': loss, 'status': STATUS_OK}

    def _save_best_params(self, best_params, loss, force=False):
        """Saves best parameters after optimization is complete.

        Parameters
        ----------
        best_params : dict
            Parameter dictionary, containing the best parameter set resulting from the fmin hyperopt optimization.
        loss : float
            MSE associated with best parameter set
        force : boolean
            When True, saves best parameter file. If False (Default), raises a warning.

        Returns
        -------
        Dictionary containing metric, parameter set, and a hyperopt flag for the model's run result
        """
        filename = 'hyperparameter_results_' + self.yaml_obj.filename

        # Define trial path
        best_params_filename = DataImporter.get_dated_file_name(
            filename=filename,
            dataset_date=self.dataset_date,
            file_type=".txt")
        best_params_output_path = op.join(self.model_dir, best_params_filename)

        if best_params_output_path:
            with open(best_params_output_path, 'w+') as f:
                f.write("#" * 40)
                f.write("\nHyper-parameter Tuning Results")
                f.write("\nModel: {model} \nDataset date: {date} \nMSE: {loss:.6f}".format(
                    model=self.yaml_obj.filename.replace("_", " "),
                    date=self.dataset_date,
                    loss=loss['loss']))
                f.write("\nParameters:")
                for param in best_params.keys():
                    f.write("\n  - {param}: {result}\n".format(
                        param=param,
                        result=best_params[param]))
                now = datetime.now()
                f.write("#" * 40)
                f.write("\nTuning concluded on {date}\n".format(date=now.strftime("%Y-%m-%d %H:%M")))
            logger.info("Saved best params for {}".format(self.yaml_obj.filename))
        else:
            raise logger.warning("Did not save results for model. Please use force flag.")

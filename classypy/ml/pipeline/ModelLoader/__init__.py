"""Object for handling raw model data and model objects"""
import os
import os.path as op

import numpy as np
import pandas as pd
# We need to import each model so that we can specify the models in model YAMLs and instantiate them via a eval() call
from sklearn.ensemble import BaggingRegressor  # noqa
from sklearn.ensemble import ExtraTreesRegressor  # noqa
from sklearn.ensemble import RandomForestClassifier  # noqa
from sklearn.ensemble import RandomForestRegressor  # noqa
from sklearn.externals import joblib
from xgboost import XGBRegressor  # noqa

from classypy.devops import logger
from classypy.ml.pipeline.DataImporter import DataImporter
from classypy.ml.pipeline.YAMLFile import YAMLFile


class ModelLoader(object):
    """Class containing generic methods for dealing with importing raw data for models and pickled models"""

    def __init__(self, config_file, force=False, data_dir=None, dataset_date=None, targets_dir=None,
                 yaml_file=None, output_dir=None):
        self.config = YAMLFile(yaml_file=config_file).yaml_dict
        self.data_path = op.join(data_dir, DataImporter.get_dated_file_name(filename="labels",
                                 dataset_date=dataset_date))
        self.dataset_date = dataset_date
        self.force = force
        self.output_dir = output_dir
        self.fetch_config()
        self.fetch_labels_data()

        if yaml_file:
            self.yaml_obj = YAMLFile(yaml_file=yaml_file)
            self.fetch_model()
        else:
            self.yaml_obj = None

        if targets_dir:
            self.targets_dir = targets_dir
            self.fetch_target_data_and_prediction_path()
        else:
            pass

    def fetch_config(self):
        """Set configuration settings as object attributes"""
        # Necessary (and sufficient) keys
        try:
            self.label = self.config['label']
            self.primary_key = self.config['primary_key']
            self.metrics = self.config['metrics']
            self.random_state = self.config['random_state']
            self.test_size = self.config['test_size']
            self.n_splits = self.config['n_splits']
            self.tune_models = self.config['tune_models']
        except KeyError as e:
            raise KeyError("Please pass settings for {key}".format(key=e))

        # Keys necessary for training and prediction
        try:
            self.balance_dataset = self.config['balance_dataset']
            self.predict = self.config['predict']
            self.train = self.config['train']
        except KeyError as e:
            logger.debug("Please pass settings for {key} if you want to train models or to generate predictions".
                         format(key=e))

    def fetch_labels_data(self):
        """ Read in labels data. Remove primary key and label. Set numpy array objects for modeling"""
        labels_data = pd.read_csv(self.data_path)

        # Remove primary key
        if self.primary_key in labels_data.columns:
            del labels_data[self.primary_key]

        # Re-arrange columns to be alphabetical
        self.y = labels_data[[self.label]]
        self.y = np.ravel(self.y)

        # Subset sorted features for model training
        features = [col for col in labels_data.columns if col != self.label]
        features = sorted(features)

        self.X = np.array(labels_data[features])

    def fetch_model(self):
        """ Evaluate model specified in each model yaml_file object"""
        # Note, only models that are imported will be properly evaluated
        model = eval(self.yaml_obj.yaml_dict['model'])  # nosec
        params = self.yaml_obj.yaml_dict['params']
        self.model = model(**params)

    def fetch_target_data_and_prediction_path(self):
        """Read in target data. Remove primary key if it exists. Set numpy array objects for prediction"""
        target_data = pd.read_csv(op.join(self.targets_dir,
                                          DataImporter.get_dated_file_name(filename="targets",
                                                                           dataset_date=self.dataset_date)))

        # Remove primary key if it exists in target data
        if self.primary_key in target_data.columns:
            self.target_ids = target_data[self.primary_key]
            del target_data[self.primary_key]
        else:
            logger.info("Target primary keys not provided, predictions will not have associated primary keys")

        # Subset sorted features for model prediction
        features = [col for col in target_data.columns if col != self.label]
        features = sorted(features)

        self.target_data = np.array(target_data[features])
        self.prediction_data_path = op.join(self.output_dir,
                                            DataImporter.get_dated_file_name("{model_name}_predictions"
                                                                             .format(model_name=self.yaml_obj.filename),
                                                                             dataset_date=self.dataset_date))

    @staticmethod
    def save_model(fitted_model, model_name, dataset_date, output_dir):
        """This function saves a fitted model object to output directory using the a name generated from model_name and
        dataset_date

        Parameters
        ----------
        fitted_model : model object
            Fitted model that we want to save
        model_name : str
            Name indicating what kind of model is being saved
        dataset_date : str
            Date for the dataset on which the model was trained
        output_dir : str
            Directory to which the model will be saved

        Return:
        -------
        This function saves a fitted model to output_dir

        """
        if fitted_model:
            model_output = DataImporter.get_dated_file_name(filename=model_name,
                                                            dataset_date=dataset_date,
                                                            file_type=".pkl")
            if not op.exists(output_dir):
                os.makedirs(output_dir)

            model_output_path = op.join(output_dir, model_output)

            with open(model_output_path, 'wb') as f:
                joblib.dump(fitted_model, f)
        else:
            raise TypeError("Did not find fitted model. Please fit before saving")

    @staticmethod
    def load_model(model_dir, model_name, dataset_date):
        """This function loads a fitted model object from the directory specified by model_dir using a combination of
        model_name and dataset_date (date on which the dataset was sourced on which the model was fitted)

        Parameters
        ----------
        model_name : str
            Name indicating what kind of model is being saved
        dataset_date : str
            Date for the dataset on which the model was trained
        output_dir : str
            Directory to which the model will be saved

        Return:
        -------
        This function returns a fitted model loaded from model_dir

        """
        model_path = DataImporter.get_dated_file_name(filename=model_name,
                                                      dataset_date=dataset_date,
                                                      file_type=".pkl")
        model_input_path = op.join(model_dir, model_path)
        with open(model_input_path, 'rb') as f:
            fitted_model = joblib.load(f)

        return fitted_model

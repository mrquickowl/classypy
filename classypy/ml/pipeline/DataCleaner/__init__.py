"""Object for cleaning raw data"""
import pandas as pd

from classypy.devops import logger
from classypy.ml.pipeline.DataTransformer import DataTransformer
from classypy.util.caching import cache_dataframe
from classypy.util.execution import run_in_parallel


class DataCleaner(DataTransformer):
    """Class containing generic data cleaning methods"""

    def __init__(self, filter_params=None, is_targets=False, replace_params=None):
        super(DataCleaner, self).__init__(is_targets=is_targets)
        self.filter_params = filter_params
        self.replace_params = replace_params

    def _filter(self, df, filter_dict):
        """This function wraps the loc method in pandas. It works by locating all the rows in the dataframe
        that do not satisfy the constraints specified in the values of filter_dict.

        Parameters
        ----------
        filter_dict : dict
            Dictionary where the keys are columns and the values are lists. The lists ought to contain
            values in the column that are to be filtered out
        """
        for filter_col in filter_dict.keys():
            df = df.loc[~df[filter_col].isin(filter_dict[filter_col])]

        return df

    def _replace(self, df, replace_dict):
        """This function wraps the replace method in pandas and expects a dataframe containing column names matching all
        the keys in replace_dict. It works by replacing all elements in df with the new elements specified in
        replace_dict. Original elements in df are expected to be keys in replace_dict and new values are expected
        to be values in replace_dict.

        When the key in replace_dict is all, all the elements in the dataframe are replaced according to the
        dictionary in replace_dict['all']

        Parameters
        ----------
        replace_dict : dict
            Dictionary where the keys are columns and the values are dictionaries,
            in each underlying dictionary the key represents original elements in df
            and the values represent the elements that will replace the old elements in df

        """
        for replace_col in replace_dict.keys():
            if replace_col == 'all':
                df = df.replace(replace_dict[replace_col])
            else:
                df[replace_col] = df[replace_col].replace(replace_dict[replace_col])

        return df

    def fit(self, df):
        """This method must exist here because it's an abstract method on the base class"""
        pass

    def transform(self, df):
        """This method looks for a replace and filter key within the config_obj attribute. If they are found,
        then values are replaced according to replace_dict and values are filtered based on the filter_dict

        Parameters
        ----------
        df : dataframe
            A dataframe containing values that need to be cleaned

        """
        if self.filter_params:
            df = self._filter(df=df, filter_dict=self.filter_params)
        if self.replace_params:
            df = self._replace(df=df, replace_dict=self.replace_params)

        return df

    @classmethod
    def load_data(cls, configs, data_dir, dataset_date):
        """This function loads all the raw data files indicated by the keys in the confings dictionary.

        Parameters
        ----------
        configs : dict
            Dictionary where the keys are file paths and the values are a yaml_obj
        data_dir : str
            File path to where data is being stored
        dataset_date : str
            Date for which the raw data is filtered on

        Return:
        -------
        data_dict : dict
            Dictionary where the keys are the names of the config files and the values are dataframes
        """
        data_dict = {}
        is_targets = False

        # Nested for loop necessary to get both targets and features data
        for feature_set_name, feature_set_config in configs.items():
            dataset_name = feature_set_name.split('.')[0].replace("_", " ")
            logger.info("Loading {dataset}".format(dataset=dataset_name))

            is_targets = ('targets' in dataset_name)

            csv_file = cls.get_csv_file(filename=feature_set_config.filename,
                                        csv_file_dir=data_dir,
                                        is_targets=is_targets,
                                        dataset_date=dataset_date)

            data_dict[feature_set_name] = pd.read_csv(csv_file)

        return data_dict

    @staticmethod
    @cache_dataframe
    def save_data(df):
        """Cache cleaned data"""
        return df

    @classmethod
    def run(cls, configs, data, dataset_date, force, is_targets, output_dir, parallel):
        """This function wraps clean_data and cleans the raw_files in input_dir in parallel. It outputs each
        of the cleaned files into csv_dir and cleans each file according to the procedures specified
        in their corresponding yaml_file in yaml_list. Each yaml_file must exist in yaml_dir.

        Parameters
        ----------
        configs : dict
            Dictionary where the keys are file paths and the values are a yaml_obj
        data : dict
            Dictionary where the keys are the same as the keys in config and the values are dataframes
        dataset_date : str
            Date on which the raw data was sourced
        force : bool, optional
            Forces cached data to be overwritten (Default: False)
        is_targets : Boolean
            Indicates whether or not to clean features for target dataset
        output_dir : str
            Directory where cleaned up csv files will be stored
        parallel : Boolean
            Indicates whether or not to clean features in parallel

        Return:
        -------
        This function generates a series of csv files containing cleaned up raw data

        """
        def _run(df, config_obj, output_dir, dataset_date, is_targets, force):
            """This is a helper function for the parallel wrapper, necessary because we can't share a global instantiation
            of DataCleaner across processes"""
            from classypy.ml.pipeline.DataCleaner import DataCleaner

            cleaner = DataCleaner(filter_params=config_obj.yaml_dict.get('filter'),
                                  replace_params=config_obj.yaml_dict.get('replace'), is_targets=is_targets)

            # Log the status of the current feature set being processed
            if is_targets:
                targets = " targets"
            else:
                targets = ""
            logger.info("Working on{targets} {prefix}".format(targets=targets,
                                                              prefix=config_obj.prefix))
            df = cleaner.transform(df=df)
            # Set the appropriate appended name for each feature set
            if 'feat' not in config_obj.filename:
                is_targets = False

            output_csv_file = cleaner.get_csv_file(filename=config_obj.filename,
                                                   csv_file_dir=output_dir,
                                                   is_targets=is_targets,
                                                   dataset_date=dataset_date,
                                                   suffix="clean")

            # Append column names with feature set prefixes
            if config_obj.filename not in ['labels', 'targets']:
                # Do not add prefix to labels or targets data
                df = cleaner.add_column_name_prefix(df=df, prefix=config_obj.prefix)

            # Save data
            cleaner.save_data(df=df, force=force, csv_file=output_csv_file)

        # Create a list of parameters, to be run either in parallel or sequentially
        param_list = []

        for feature_set_name, feature_set_config in configs.items():
            params = {'df': data[feature_set_name],
                      'config_obj': configs[feature_set_name],
                      'dataset_date': dataset_date,
                      'force': force,
                      'is_targets': ("targets" in feature_set_name),
                      'output_dir': output_dir}
            param_list.append(params)
        if parallel:
            run_in_parallel(func=_run, params=param_list)
        else:
            for params in param_list:
                _run(**params)

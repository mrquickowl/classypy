"""
Implementations of text-processing and scaling algorithms, all inheriting from sklearn BaseEstimators
and TransformerMixin.

This allows for use in pipeline objects, as well as provides a familiar API for preprocessing.

Example Code
-------------
>> preprocessor = TextProcessor()
>> preprocessor.fit(data)
>> cleaned_data = preprocessor.transform(data)

>> boxcox = BoxCoxTransformer()
>> boxcox.fit(data)
>> data_transformed = boxcox.transform(data)
"""

import numpy as np
from scipy import stats
from sklearn.base import BaseEstimator, TransformerMixin
from sklearn.utils import check_array
from sklearn.utils.validation import check_is_fitted

from classypy.devops import logger


class TextProcessor(BaseEstimator, TransformerMixin):
    """
    Custom sklearn transformer to preprocess data.
    Data fit to should be raw document (not split by tokens).
    """

    def __init__(self, remove_stopwords=True, do_lemmatize=False, do_stem=False, stop_words=None, return_list=True):
        """
        Parameters
        ----------
        remove_stopwords: Boolean
            Whether or not to remove stopwords.
        do_lemmatize: Boolean
            Whether or not to lemmatize tokens.
        do_stem: Boolean
            Whether or not to stem tokens.
        stop_words: iterable
            List of stop words, each word of type str.
        return_list: Boolean
            Whether or not to return list or raw document.
            Some sklearn vectorizers prefer a raw document.

        Note - if neither `do_lemmatize` nor `do_stem` are specified, the transform
        method will preprocess using `fast_process`, which is much faster.
        """
        from nltk.corpus import stopwords
        from nltk.stem import PorterStemmer, WordNetLemmatizer

        self._fetch_data(remove_stopwords=remove_stopwords)
        if remove_stopwords and stop_words:
            self.stopwords = set(stop_words)
        elif remove_stopwords:
            self.stopwords = set(stopwords.words('english'))
        else:
            self.stopwords = None
        self.lemmatizer = WordNetLemmatizer()
        self.stemmer = PorterStemmer()
        self.do_lemmatize = do_lemmatize
        self.do_stem = do_stem
        # if not stemming or lemmatizing, use faster preprocessing
        if do_lemmatize or do_stem:
            self.preprocess = self.process
        else:
            self.preprocess = self.fast_process
        self.return_list = return_list

    def _fetch_data(self, remove_stopwords=True):
        """Download all data necessary to get this instantiation of the class to work"""
        import nltk

        if remove_stopwords:
            nltk.download('stopwords', quiet=not logger.logsLevel(logger.DEBUG))
        nltk.download('averaged_perceptron_tagger', quiet=not logger.logsLevel(logger.DEBUG))
        nltk.download('wordnet', quiet=not logger.logsLevel(logger.DEBUG))

    def stem(self, token):
        """
        Stems word `token` via nltk's PorterStemmer.
        """
        return self.stemmer.stem(token)

    def lemmatize(self, token, pos_tag):
        """
        Lemmatizes token via nltk's WordNetLemmatizer.
        This method bust be called on a tuple of (token, post_tag).

        Parameters
        ----------
        token : str
            Token to be processed.
        pos_tag : str
            Part-of-Speech tag for `token`.

        Returns
        -------
            str: Lemmatized token.

        """
        from nltk.corpus import wordnet as wn

        tag = {
            'N': wn.NOUN,
            'V': wn.VERB,
            'R': wn.ADV,
            'J': wn.ADJ,
            'S': wn.ADJ_SAT
        }.get(pos_tag[0], wn.NOUN)
        return self.lemmatizer.lemmatize(token, tag)

    def clean_token(self, token):
        """
        Cleans `token` by performing the following steps:
            1. Converts to lower-case and strips whitespace.
            2. If `token` is a stopword and `remove_stopwords` is True,
               return an empty string.
            3. Removes non-alpha characters from word.

        Parameters
        ----------
        token : str
            String to be cleaned.

        Returns
        -------
            c_token (str): Cleaned string.
        """
        c_token = token.lower().strip()
        if self.stopwords and c_token in self.stopwords:
            return ''
        # remove non-alpha characters
        c_token = [x for x in c_token if x.isalpha()]
        return ''.join(c_token)

    def process(self, document):
        """
        Preprocessing method that stems and/or lemmatizes token
        during a single pass through data.
        """
        cleaned_tokens = []
        if self.do_lemmatize:
            from nltk import pos_tag as nltk_pos_tag

            # lemmatizing requires that each token has an associated POS tag
            postag_doc = nltk_pos_tag(document.split())
            for token, tag in postag_doc:
                token = self.clean_token(token)
                token = self.lemmatize(token, tag)
                if self.do_stem:
                    token = self.stem(token)
                cleaned_tokens.append(token)
        else:
            for token in document.split():
                token = self.clean_token(token)
                if self.do_stem:
                    token = self.stem(token)
                cleaned_tokens.append(token)

        keep_tokens = [ctoken for ctoken in cleaned_tokens if ctoken != '']
        # determine whether we should return list of tokens or single str
        if not self.return_list:
            return ' '.join(keep_tokens)

        return keep_tokens

    def fast_process(self, document):
        """
        Fast preprocessing of document without stemming or lemmatizing tokens.
        Suitable preprocessing for word2vec.

        Parameters
        ----------
        document: str
            Raw text of document.

        Returns
        -------
            iterable: List of reprocessed tokens from document.
        """
        doc = [
            self.clean_token(token)
            for token in document.split()
        ]
        clean_doc = [token for token in doc if token != '']
        if not self.return_list:
            return ' '.join(clean_doc)
        return clean_doc

    def fit(self, X, y=None):
        """
        `fit` method required for inclusion in sklearn pipeline object.
        Returns data as is.
        """
        return self

    def transform(self, documents):
        """
        Preprocesses each document in `documents`.

        Parameters
        ----------
        documents: iterable
            List of documents. Each document raw, untokenized text.

        Returns
        -------
            iterable: list of lists. Each list contains the preprocessed
            tokens for a document.
        """
        document_list = []
        for document in documents:
            document_list.append(self.preprocess(document))
        return document_list

    def __call__(self, doc):
        """Allows class to be called as a function, as in
        TfidfVectorizer(tokenizer=TextProcesser()...).

        Parameters
        ----------
        doc: str
          document to be preprocessed

        Returns
        --------
        str
          preprocessed document.
        """
        return self.preprocess(doc)


class BoxCoxTransformer(BaseEstimator, TransformerMixin):
    """Apply the Box-Cox transform featurewise to make data more Gaussian-like.

    The Box-Cox transformation is a monotonic transformation that is
    applied to make data more Gaussian-like. This is useful for solving
    modeling issues related to heteroscedasticity (non-constant variance),
    or other situations where normality is desired.

    Box-Cox requires input data to be strictly positive. The optimal value
    for the parameter, lambda, is estimated through maximum likelihood.

    The Box-Cox transformation is given by::
        if lambda == 0:
            X_trans = log(X)
        else:
            X_trans = (X ** lambda - 1) / lambda
    Parameters
    ----------
    copy : boolean, optional, default=True
        Set to False to perform inplace computation. If copy=False, a copy might
        still be triggered by a conversion.

    Attributes
    ----------
    lambdas_ : array of float, shape (n_features,)
        The parameters of the Box-Cox transformation for the selected features.

    Examples
    --------
    >>> import numpy as np
    >>> from classypy.ml.transformers import BoxCoxTransformer
    >>> boxcox = BoxCoxTransformer()
    >>> data = [[1, 2], [3, 2], [4, 5]]
    >>> print(boxcox.fit(data))
    BoxCoxTransformer(copy=True)
    >>> print(np.round(boxcox.lambdas_, 3))
    [ 1.052 -2.345]
    >>> print(np.round(boxcox.transform(data), 3))
    [[ 0.     0.342]
     [ 2.068  0.342]
     [ 3.135  0.417]]

    References
    ----------
    G.E.P. Box and D.R. Cox, "An Analysis of Transformations", Journal of the
    Royal Statistical Society B, 26, 211-252 (1964).
    """
    def __init__(self, copy=True):
        self.copy = copy

    def fit(self, X, y=None):
        """Estimate the optimal lambda for each feature using MLE.

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            The data used to estimate the Box-Cox transformation parameters.
        y : Ignored

        Returns
        -------
        self : object
            Returns self.
        """
        X = check_array(X, dtype='float', ensure_2d=True)

        if np.any(X <= 0):
            raise ValueError("The Box-Cox transformation can only be applied "
                             "to strictly positive data")

        self.lambdas_ = []
        for col in X.T:
            _, lmbda = stats.boxcox(col, lmbda=None)
            self.lambdas_.append(lmbda)
        self.lambdas_ = np.array(self.lambdas_)

        return self

    def transform(self, X):
        """Apply Box-Cox to each feature using the fitted lambdas.

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            The data to be transformed using the Box-Cox transformation.
        """
        check_is_fitted(self, 'lambdas_')
        X = check_array(X, dtype='float', ensure_2d=True, copy=self.copy)

        if np.any(X <= 0):
            raise ValueError("The Box-Cox transformation can only be applied "
                             "to strictly positive data")

        if not len(self.lambdas_) == X.shape[1]:
            raise ValueError("Input data has a different number of features "
                             "than fitting data. Should have {n}, data has {m}"
                             .format(n=len(self.lambdas_), m=X.shape[1]))

        for i, lmbda in enumerate(self.lambdas_):
            X[:, i] = stats.boxcox(X[:, i].flatten(), lmbda=lmbda)

        return X

    def inverse_transform(self, X):
        """Undo the Box-Cox transformation using the fitted lambdas.

        The inverse of the Box-Cox transformation is given by::
            if lambda == 0:
                X = exp(X_trans)
            else:
                X = (X_trans * lambda + 1) ** (1 / lambda)

        Parameters
        ----------
        X : array-like, shape (n_samples, n_features)
            The Box-Cox transformed data.
        """
        check_is_fitted(self, 'lambdas_')
        X = check_array(X, dtype='float', ensure_2d=True, copy=self.copy)

        if not X.shape[1] == len(self.lambdas_):
            raise ValueError("Input data has a different number of features "
                             "than fitting data. Should have {n}, data has {m}"
                             .format(n=len(self.lambdas_), m=X.shape[1]))

        for i, lmbda in enumerate(self.lambdas_):
            x = X[:, i]
            if lmbda == 0:
                x_inv = np.exp(x)
            else:
                x_inv = (x * lmbda + 1) ** (1 / lmbda)
            X[:, i] = x_inv

        return X

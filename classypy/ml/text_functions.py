"""
Various library functions for munging input data.
"""
import ast
import collections
import csv
import os.path as op
import re

import pandas as pd

from classypy.devops import logger
from classypy.util.dirs import this_files_dir
from classypy.util.etl import to_ascii

# consts
STOP_WORDS = None  # lazy fill


def _cleanCategoryCell(cell):
    """
    Helper function to clean up category sections that have lots
    of unparsable chars that need to get cleaned up
    """
    cleaned_cell = []
    mem = ''
    s = True
    for ele in cell:
        if ele == '"':
            s = not(s)
            if mem != '':
                cleaned_cell.append(mem[:-1])  # removes //
            mem = ''
        if s and ele != '"':
            mem = mem + ele
    return cleaned_cell


def contains_digit(word):
    """Helper function to detect a digit in a word."""
    return bool(any([char.isdigit() for char in word]))


def get_stop_words(stopwords_dir=None):
    global STOP_WORDS
    if STOP_WORDS is None:
        STOP_WORDS = []
        stopwords_dir = stopwords_dir or this_files_dir()
        with open(op.join(stopwords_dir, 'stopwords.csv'), 'rb') as csvfile:
            reader = csv.reader(csvfile)
            for row in reader:
                STOP_WORDS.extend(row)
        logger.info("Loaded %d stop words" % (len(STOP_WORDS)))
    return STOP_WORDS


def row2bow(program, sections, split_pipes=False, exclude_stop_words=False,
            exclude_nums=True, min_word_length=3):
    """
    """
    if exclude_stop_words:
        stopwords = get_stop_words()

    this_bag = []

    for section in sections:
        assert section in program, "%s is not a key in this data frame." % section

        if split_pipes:
            # Sets (e.g. problem measurements)
            words = program[section].split('|')
            words = [ele.replace('"', "") for ele in words]
            words = [ele.lower() for ele in words]
            this_bag.extend(words)

        elif program[section] is None:
            this_bag = this_bag if exclude_nums or exclude_stop_words else None

        else:
            # Words or numbers

            words = re.sub(r'[\W_]+', ' ', program[section]).split(' ')

            if exclude_nums:
                words = [w for w in words if not contains_digit(w)]
            if exclude_stop_words:
                words = [w for w in words if w.lower() not in stopwords]

            if len(words) > 1:
                # Words: exclude empty and simple words
                words = filter(lambda w: len(w) > min_word_length, words)
                words = [w.lower() for w in words]  # map
                this_bag.extend(words)

            elif not exclude_nums:
                # Numbers; append single values.
                # NOTE: can only deal with one section
                word = words[0]
                assert len(sections) == 1, (section, word)
                if bool(all(ele.isdigit() for ele in word)):
                    if len(word) == 1 and word[0] == "0":  # weird input
                        this_bag = None
                    else:
                        this_bag = int(word)
                else:
                    this_bag = None

            else:
                # Discardable words
                logger.debug("df2bows: Skipping word '%s'" % program[section])
                continue

    # append after section loop
    if isinstance(this_bag, collections.Iterable):
        if len(this_bag) == 0:
            this_bag = None
        elif len(this_bag) == 1 and this_bag[0].lower() == 'null':
            this_bag = None

    if 'name' in program:
        name = program['name']
        if isinstance(name, str):
            name = name.decode('unicode_escape')
        name = to_ascii(name)
        name = name.replace('"', '')  # eval to remove quotes
    else:
        name = None

    # categories have lots of ",', and / chars that need to get cleaned up.
    if 'categories' in program:
        cleanedCategories = _cleanCategoryCell(program['categories'])
        categories = set(cleanedCategories)
    else:
        categories = None

    return this_bag, name, categories


def df2bows(df, sections='all text', exclude_empty_bows=False, **kwargs):

    if sections == 'all text':
        sections = ['description', 'problem statement', 'strategy description',
                    'differentiator', 'output_description', 'outcome_description', 'progress']

    # find the sections you want to analyze in the data
    bad_sections = [s for s in sections if s not in df.columns]
    sections = [s for s in sections if s in df.columns]
    for bs in bad_sections:
        raise Exception(bs + " could not be found.")

    # clean data
    logger.info("Cleaning {n_documents} documents.".format(n_documents=len(df)))
    df = df.where((pd.notnull(df)), None)

    bag_of_words = []
    names = []
    categories = []

    for ri, (_, row) in enumerate(df.iterrows()):
        if ri % 1000 == 999:
            logger.info(ri + 1)

        # Parse row
        this_bag, name, category = row2bow(row, sections=sections, **kwargs)

        # Determine if we want to save
        if exclude_empty_bows:
            if this_bag is None:
                continue
            if not this_bag and isinstance(this_bag, collections.Iterable):
                continue

        # Save
        bag_of_words.append(this_bag)
        names.append(name)
        categories.append(category)

    # Report on data
    def counter_fn(bow):
        return len(bow) if isinstance(bow, collections.Iterable) else 1
    total_words = sum([counter_fn(bow) for bow in bag_of_words])
    logger.info("Total words: %d (%s sections, %d bags)" % (total_words, sections, len(bag_of_words)))
    return names, categories, bag_of_words


def readFromCSV(in_dir='.', fname='', sections='all text',
                row_key='torbenrow', line_key='torbenline', split_pipes=False,
                exclude_stop_words=False, exclude_nums=True,
                return_dataframe=False):
    """
    Reads a CSV SQL query of the progress database and
    returns the names, categories, and text content for every program

    Parameters
    ----------

    in_dir : path to csv file generated from SQL request

    fname : name of csv file to load

    sections : array of names of sections to draw data from. ['mission','description']
    would put all text data in both of those sections into one bag of words per program

    row_key : custom string used to delineate rows and columns in messy progress data

    line_key : see row_key

    exclude_stop_words : Boolean. True is best for word counts False is best for character counts.
    exclude_nums : Boolean. True if analyzing non-numeric data

    Returns
    ----------
    names : names of all programs
    categories : sets of categories that each program belongs to
    bag_of_words : a list of every word used in each section selected for a program

    """
    try:
        df = pd.read_csv(op.join(in_dir, fname))
    except Exception as ex:  # noqa
        if "Error tokenizing data" not in str(ex):
            raise ex

        csv_data = pd.read_csv(op.join(in_dir, fname))

        col_names = [ast.literal_eval(i) for i in csv_data[0]]
        logger.info(col_names)
        data = csv_data[1:]  # pull off headers
        df = pd.DataFrame(data, columns=col_names)

    if return_dataframe:
        return df

    return df2bows(
        df=df, split_pipes=split_pipes, exclude_stop_words=exclude_stop_words,
        exclude_nums=exclude_nums, sections=sections)


def writeToCSV(csv_file, names, field, field_name='field'):
    """
    writes names and fields to a csv file.

    Parameters
    ========

    csv_file : path and name of csv to be produced
    names : names of programs
    field : values gathered from program i.e. problem statement or number of technologies
    field_name : label for field column. Defaults to field, can be changed to help with bookkeeping
    """
    with open(csv_file, 'wb') as fp:
        writer = csv.writer(fp, delimiter=',')
        writer.writerow(['program', field_name])
        for i in range(len(names)):
            writer.writerow([names[i], field[i]])


def arrays_to_df(names, field, categories=None, field_name=None):

    df = pd.DataFrame()
    df['names'] = names
    if categories is not None:
        df['categories'] = categories
    if field_name is not None:
        df[field_name] = field
    else:
        df['field'] = field

    return df


def digest_dataframe(df, text_split=True):
    """
    Takes a dataframe of a progress field and turns it into arrays
    """
    names = list(df['names'].values)

    field = list(df[[len(df.columns) - 1]].values)

    if text_split:
        field = [field[i][0] for i in range(len(field))]

    if 'categories' in df.columns:
        categories = list(df['categories'].values)
    else:
        categories = []

    return names, categories, field


def buildSplitBOW(names, categories, bag_of_words):
    """
    builds a bag of words where each program is duplicated for every category it belongs to. i.e. #split categories
    i.e. [health,animals][text] --> [health][text]+ [animals][text]

    Parameters
    ----------

    names : names of programs (output of readFromCSV)

    categories : set of categories each program belongs to (output of readFromCSV)

    bag_of_words : list of all words used by each program (output of readFromCSV)


    Returns
    ----------

    split_names : names of each program

    split_categories : one of the potentially multiple categories that each program belongs to

    split_bag_of_words : list of all words used by each program. potentially multiple per program.

    """
    split_categories = []
    split_bag_of_words = []
    split_names = []

    for i, category_set in enumerate(categories):

        for category in category_set:
            split_categories.append(category)
            split_bag_of_words.append(bag_of_words[i])
            split_names.append(names[i])
    return [split_names, split_categories, split_bag_of_words]

"""
"""
from __future__ import division

import copy

import numpy as np
import pandas as pd

from classypy.devops import logger


def levenshtein(s1, s2, case_sensitive=True, thresh=None):
    """
    Computes levenshtein distance between two strings.
    See: https://en.wikipedia.org/wiki/Levenshtein_distance

    Adapted from: https://en.wikibooks.org/wiki/Algorithm_Implementation/Strings/Levenshtein_distance#Python

    Parameters
    ----------
    s1 : str
        First string to be compared
    s2 : str
        Second string to be compared.
    case_insensitive : Boolean (default False)
        Whether or not the distance should take case into account.

    Returns
    -------
        int: Levenshtein distance between s1 and s2.

    """
    # make case-insensitive
    if not case_sensitive:
        s1 = s1.lower()
        s2 = s2.lower()

    if len(s1) < len(s2):
        return levenshtein(s2, s1, case_sensitive=case_sensitive, thresh=thresh)

    # len(s1) >= len(s2)
    if len(s2) == 0:
        return len(s1)

    previous_row = list(range(len(s2) + 1))
    for i, c1 in enumerate(s1):
        current_row = copy.copy(previous_row)
        current_row[0] = i + 1
        for j, c2 in enumerate(s2):
            insertions = previous_row[j + 1] + 1
            deletions = current_row[j] + 1
            substitutions = previous_row[j] + (c1 != c2)
            dist = min(insertions, deletions, substitutions)
            current_row[j + 1] = dist
        previous_row = current_row  # re-allocated every loop, so ok

    return previous_row[-1]


def _R(col, is_categorical):
    """Calculate R, the numeric range of the column.

    If the column is categorical, as indicated by the second element in the tuple,
    a value of 1 is assigned to R, since the similarity will be divided by R
    in the end.

    Parameters
    ----------
    tup : tuple of (pd.Series, bool)
        Tuple where the first element is a column in the pandas DataFrame
        on which Gower similarity is being calculated, and the second element
        is a boolean indicating if the feature is categorical.
    """
    if is_categorical:
        R = 1
    else:
        R = col.max() - col.min()
    return R


def _gower_vector(X, Y, R, W, categorical_mask, X_null, Y_null):
    """Calculates the Gower similarity between two vectors.

    Parameters
    ----------
    X : ndarray-like
    Y : ndarray-like
    R : ndarray-like
        The absolute range of numeric vectors (i.e. max - min).
    W : ndarray-like of float
        The weight vector.
    categorical_mask; : ndarray-like of bool
        A boolean vector that is True when the feature is categorical.
    X_null : ndarray-like
        Null mask for X.
    Y_null : ndarray-like
        Null mask for Y.

    Returns
    -------
    S_ij : float
        The similarity between two vectors.
    """
    assert len(X) == len(Y) == len(categorical_mask) == len(W), 'All vectors must be same length'
    [X, Y] = [np.asarray(arr, dtype='object') for arr in [X, Y]]

    # TODO: see if there's a good way to vectorize this
    S = 0  # the total of numeric similarities (\sigma{s_ijk})
    denom = 0  # delta_k * w_k

    for i in range(len(X)):
        if X_null[i] or Y_null[i] or R[i] == 0:
            continue
        elif categorical_mask[i]:
            s_ijk = int(X[i] == Y[i])
        else:
            s_ijk = 1 - abs(X[i] - Y[i]) / R[i]

        S += s_ijk * W[i]  # scale by weight
        denom += W[i]

    if denom == 0:
        S_ij = np.nan
    else:
        S_ij = S / denom

    return S_ij


def gower_similarity_matrix(df, Y=None, categorical_features=None, weights=None):
    """Computes the Gower similarity matrix for a dataframe.

    Gower similarity takes a value in the range (0, 1), where 1 indicates identical
    observations. To get Gower distance, take 1 - s, where s is the similarity.

    Parameters
    ----------
    df : pandas.DataFrame
    Y : ndarray-like
        The comparison vector for df. If None, df will be used as the comparison, and pairwise
        similarities will be calculated.
    categorical_features : list of str
        A list of the categorical feature column names.
    weights : list of float
        A list of the feature weights. If left None, all features will be weighted equally.

    Returns
    -------
    gower_matrix : pandas.DataFrame
        A pairwise matrix of Gower similarities.
    """
    n_features = df.shape[1]

    if Y is not None:
        comparison_vector_provided = True
        if not df.shape[1] == Y.shape[1]:
            raise ValueError('Inputs should have the same number of columns.')
        if not all([df.columns[i] == Y.columns[i] for i in range(n_features)]):
            raise ValueError('Inputs should have identical column names.')
    else:
        comparison_vector_provided = False
        Y = df.copy()

    # create categorical feature vector
    if categorical_features:
        categorical_mask = np.array([col in categorical_features for col in df.columns])
    else:
        categorical_mask = [False] * n_features

    # create weight vector
    weights = weights or np.ones(shape=n_features)
    assert len(weights) == n_features, 'Weight vector should have same n_dims as columns in input.'

    # calculate ranges for numeric columns
    column_list = [df[col] for col in df.columns]
    R = [_R(col, cat) for col, cat in zip(column_list, categorical_mask)]

    # calculate pairwise gower similarity for df and Y
    X = df.values
    Y = Y.values

    X_null = pd.isnull(X)
    Y_null = pd.isnull(Y)

    nrow_X = X.shape[0]
    nrow_Y = Y.shape[0]

    S = np.zeros(shape=(nrow_X, nrow_Y))

    for i in range(nrow_X):
        if comparison_vector_provided:
            j_range = list(range(0, nrow_Y))
        else:
            j_range = list(range(0, i + 1))

        for j in j_range:
            S[i, j] = _gower_vector(X=X[i, :],
                                    Y=Y[j, :],
                                    R=R,
                                    W=weights,
                                    categorical_mask=categorical_mask,
                                    X_null=X_null[i, :],
                                    Y_null=Y_null[j, :])
        logger.debug('{i} / {n}'.format(i=i, n=nrow_X))

    gower_matrix = pd.DataFrame(S)
    gower_matrix = gower_matrix.replace([-np.inf, np.inf], np.nan)

    return gower_matrix

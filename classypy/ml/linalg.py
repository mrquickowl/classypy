"""
General matrix and linear algebra functions.
"""
import numpy as np


def rolling_window(array, window_size):
    """
    Given an array, returns a rolling window of the specified size.

    Parameters
    ----------
    array : ndarray-like
    window_size : int
        The size of the moving window.

    Returns
    -------
    windowed_array : ndarray-like
        The original array with the window applied.
        Shape will be (array length - window, window).
    """
    shape = array.shape[:-1] + (array.shape[-1] - window_size + 1, window_size)
    strides = array.strides + (array.strides[-1], )
    windowed_array = np.lib.stride_tricks.as_strided(array, shape=shape, strides=strides)
    return windowed_array

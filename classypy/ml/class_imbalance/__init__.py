from imblearn.combine import SMOTEENN
from imblearn.over_sampling import SMOTE

from classypy.devops import logger


def get_combination_sampled_classes(X_train, y_train, random_state):
    """This function utilizes a combination od undersampling and oversampling via SMOTEEN to generate a
    balanced training set.
    (https://imbalanced-learn.readthedocs.io/en/stable/combine.html)

    Parameters:
    -----------
    X_train : np.array
        Numpy array containing features for the training set
    y_train : np.array
        Numpy array containing actual label values for the training set
    random_state : int
        Random seed

    Return:
    -------
    This function modified X_train, y_train set so that it has class balance.
    """
    os = SMOTEENN(random_state=random_state)
    X_train, y_train = os.fit_resample(X_train, y_train)
    logger.info("Training set is imbalanced... Performing SMOTEENN.")

    return X_train, y_train


def get_oversampled_classes(X_train, y_train, random_state):
    """This function utilizes oversampling via SMOTE to generate a balanced training set.
    (https://imbalanced-learn.readthedocs.io/en/stable/generated/imblearn.over_sampling.SMOTE.html)

    Parameters:
    -----------
    X_train : np.array
        Numpy array containing features for the training set
    y_train : np.array
        Numpy array containing actual label values for the training set
    random_state : int
        Random seed

    Return:
    -------
    This function modified X_train, y_train set so that it has class balance.
    """
    os = SMOTE(random_state=random_state)
    X_train, y_train = os.fit_resample(X_train, y_train)
    logger.info("Training set is imbalanced... Performing SMOTE.")

    return X_train, y_train

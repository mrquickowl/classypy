import os
import os.path as op
import warnings

from classypy.io.irs.form990 import IRS990Dataset, IRS990ParsingWarning, _clean_element_content


def test_parse_ez():
    # Regression for DI-502
    dataset = IRS990Dataset()

    def df_func():
        return dataset.fetch(eins=['02-0541300'], years=(2013,), filing_years=(2014,))

    # Fetch once, to get the location and remove.
    with warnings.catch_warnings(record=True) as wns:
        file_path = df_func().iloc[0]['src_file'].replace('.xml', '.features.csv')
        if op.exists(file_path):
            os.remove(file_path)

        # Fetch again, to confirm/deny parse.
        assert df_func()['form'].iloc[0] == '990EZ', "Should find and parse form"

        # Validate parse error
        assert len(wns) >= 1
        assert all([w.category == IRS990ParsingWarning for w in wns])
        assert all(["Row does not contain form information" in str(w.message) for w in wns])


def test_clean_element_content():
    # Need an object with property 'class'
    class DummyClass(object):
        pass
    x = DummyClass()
    x.text = 'wwwclassyorg'

    assert _clean_element_content('website', x) == 'www.classy.org'

"""
Exempt Organizations Business Master File Extract (EO BMF)
"""
import os.path as op

import pandas as pd

from ...charity.ein import ein_cleaner
from ..core.datasets import HttpDataset


def _clean_df(csv_path):
    """
    Loads and returns a pd.DataFrame of IRS compilied EINs.
    EINs are formatted prior to the DataFrame return.
    """
    ein_ref_df = pd.read_csv(csv_path)
    ein_ref_df['EIN'] = ein_ref_df['EIN'].map(ein_cleaner)
    return ein_ref_df


class IrsEOBMFDataset(HttpDataset):
    FEATURES = (
        'ein', 'name', 'ico', 'street', 'city', 'state', 'zip',
        'group', 'subsection', 'affiliation', 'classification',
        'ruling', 'deductibility', 'foundation', 'activity',
        'organization', 'status', 'tax_period', 'asset_cd', 'income_cd',
        'filing_req_cd', 'pf_filing_req_cd', 'acct_pd', 'asset_amt',
        'income_amt', 'revenue_amt', 'ntee_cd', 'sort_name',
    )

    def fetch(self, force=False, verbose=1):
        """
        Download master files, concatenate, and clean.
        """
        url_template = "https://www.irs.gov/pub/irs-soi/eo{page}.csv"
        downloads = [(op.basename(url_template).format(page=page),
                      url_template.format(page=page))
                     for page in range(1, 5)]
        local_paths = super(IrsEOBMFDataset, self).fetch(files=downloads, force=force, verbose=verbose)

        df = pd.concat([_clean_df(local_path) for local_path in local_paths], sort=True)
        # lowercase column names
        return df.rename(columns={col: col.lower() for col in df.columns})

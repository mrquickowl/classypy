IRS 990 Dataset

Notes
-----
Form 990 is the form used by the United States Internal Revenue Service to gather financial information about nonprofit organizations.
Index JSONs are fetched first to determine the Form 990 URLs.

Contents
--------

    :ein: List of known EINs for 501c3s.
    :eobmf: Exempt Organizations Business Master File Extract (EO BMF); contains EIN => NTEE code mappings.
    :form990: Individual filings are fetched and stored as XMLs, then relevant features are extracted from them, such as total revenue, number of employees, etc.

Examples
--------

    :example1.py: Fetches the most recent IRS 990 filings for three organizations and prints the number of successful responses.
    :example2.py: Fetches the current list of known EINs.
    :example3.py: Fetches the current list of known EINs, and their NTEE code mapping.


References
----------

https://aws.amazon.com/public-datasets/irs-990/
https://www.irs.gov/charities-non-profits/exempt-organizations-select-check

import json
import os.path as op
import re
import warnings
from collections import OrderedDict
from functools import partial

import numpy as np
import pandas as pd
import six

from classypy.charity.ein import clean_eins, ein_cleaner
from classypy.io.core._utils.parsing import BeautifulSoupParser
from classypy.io.core.datasets import HttpDataset
from classypy.text import text_from_disk
from classypy.util.caching import cache_dataframe
from classypy.util.execution import run_in_parallel


def select_form_by_filing_year(df, eins, filing_years):
    """
    Return a single form of tax data per unique EIN.

    The form will be chosen such that it matches the tax filing year specified for each ein.
    If that form is not available, the next closest form will be chosen,
    given that it was filed prior to the filing year specified.

    Parameters:
    -----------
    df : pd.DataFrame
        DataFrame containing IRS 990 tax data. This DataFrame will have multiple forms per ein.
    eins : list-like
        List of unique EINs to fetch 990 data for.
    filing_years : like-like
        List specifying filing years that correspond to EINs passed.

    Returns:
    --------
    df : pd.DataFrame
        DataFrame containing IRS 990 tax data. This DataFrame will have a single form per ein.

    """
    assert 'tax year' in df
    assert 'ein' in df
    assert 'filing_year' in df

    # Create a merged dataframe, as we need to filter on the merge below
    df_merged = (
        df
        .reset_index(drop=True)  # ein is ambiguous between index & column
        .merge(
            pd.DataFrame({'ein': eins, 'filing_year': filing_years}),
            how='left', on='ein', suffixes=('', '_specified'))
    )
    return (
        df_merged
        # May lead to dropped eins
        .loc[df_merged.apply(lambda row: row['filing_year_specified'] <= row['filing_year'], axis=1)]
        .sort_values('filing_year_specified', ascending=False)
        .groupby('ein', as_index=False)
        .nth(0)
        .drop('filing_year_specified', axis=1)
    )


def select_latest_form(df):
    """Return a single row per unique EIN, with latest available tax year information."""
    assert 'tax year' in df
    assert 'ein' in df
    assert 'filing_year' in df

    def selector(grp):
        return grp.sort_values(['tax year', 'filing_year'], ascending=False).iloc[0]

    return df.groupby('ein').apply(selector)


def _clean_element_content(field, val):
    """Various type conversions and field-specific cleaning."""
    if val is None:
        return np.nan
    val = val.text

    if field in ('website', 'mission description'):
        if val.lower() in ('n/a', 'none'):
            return np.nan

    if field == 'website':
        if '@' in val:
            return np.nan
        val = val.replace(' ', '')
        if re.match(r'^www[^.]+(com|org)$', val):
            # No periods, and we can actually fix it!
            val = re.sub(r'^(www)([^.]+)(com|org)', r'\1.\2.\3', val)
    if field in ('has local chapters', 'use professional fundraising'):
        if val in (1, '1', 'true', True):
            return True
        if val in (0, '0', 'false', False):
            return False
        return np.nan
    if field == 'tax year':
        return int(val)

    return val


class IRS990ParsingWarning(UserWarning):
    pass


class IRS990Dataset(HttpDataset, BeautifulSoupParser):
    dependencies = HttpDataset.add_dependencies(*BeautifulSoupParser.dependencies)

    EARLIEST_YEAR = 2011
    LATEST_YEAR = 2017

    # Features to be extract from the 990 XML.
    # Field names from 2011-13 differ from those from
    # 2014-2016.
    FEATURES = (
        'tax year',
        'name',
        'ein',
        'website',
        'mission description',
        '# employees',
        'total employee comp',
        'total fundraising expenses',
        'total fundraising expenses, previous year',
        '# volunteers',
        'total expenses',
        'total revenue',
        'total expenses, previous year',
        'total revenue, previous year',
        'total receipts',
        'total liabilities',
        'grants',
        'use professional fundraising',
        'has local chapters',
        'form',
        'address',
        'city',
        'state',
        'zip',
    )

    _FEATURE_PATHNAMES_11_12 = (
        'ReturnHeader/TaxYear',
        'Filer/Name/BusinessNameLine1',
        'Filer/EIN',
        'IRS990/WebSite',
        ('IRS990/MissionDescription', 'IRS990/ActivityOrMissionDescription'),
        'IRS990/TotalNbrEmployees',
        'IRS990/SalariesEtcCurrentYear',
        'IRS990/TotalFundrsngExpCurrentYear',
        'IRS990/TotalFundrsngExpPriorYear',
        'IRS990/TotalNbrVolunteers',
        'IRS990/TotalExpensesCurrentYear',
        'IRS990/TotalRevenueCurrentYear',
        'IRS990/TotalExpensesPriorYear',
        'IRS990/TotalRevenuePriorYear',
        'IRS990/GrossReceipts',
        'IRS990/TotalLiabilitiesEOY',
        'IRS990/ContributionsGrantsCurrentYear',
        'IRS990/ProfessionalFundraising',
        'IRS990/LocalChapters',
        'ReturnHeader/ReturnType',
        'Filer/USAddress/AddressLine1',
        'Filer/USAddress/City',
        'Filer/USAddress/State',
        'Filer/USAddress/ZIPCode',
    )
    assert len(FEATURES) == len(_FEATURE_PATHNAMES_11_12)

    _FEATURE_PATHNAMES_13_17 = (
        'ReturnHeader/TaxYr',
        'Filer/BusinessName/BusinessNameLine1Txt',
        'Filer/EIN',
        'IRS990/WebsiteAddressTxt',
        ('IRS990/MissionDesc', 'IRS990/ActivityOrMissionDesc'),
        'IRS990/TotalEmployeeCnt',
        'IRS990/CYSalariesCompEmpBnftPaidAmt',
        'IRS990/CYTotalProfFndrsngExpnsAmt',
        'IRS990/PYTotalProfFndrsngExpnsAmt',
        'IRS990/TotalVolunteersCnt',
        'IRS990/CYTotalExpensesAmt',
        'IRS990/CYTotalRevenueAmt',  # Line 12
        'IRS990/PYTotalExpensesAmt',
        'IRS990/PYTotalRevenueAmt',
        'IRS990/GrossReceiptsAmt',
        'IRS990/TotalLiabilitiesEOYAmt',
        'IRS990/CYContributionsGrantsAmt',  # Line 8
        'IRS990/ProfessionalFundraisingInd',
        'IRS990/LocalChaptersInd',
        'ReturnHeader/ReturnTypeCd',
        'Filer/USAddress/AddressLine1Txt',
        'Filer/USAddress/CityNm',
        'Filer/USAddress/StateAbbreviationCd',
        'Filer/USAddress/ZIPCd',
    )
    assert len(FEATURES) == len(_FEATURE_PATHNAMES_13_17)

    FEATURE_MAPPINGS = (
        OrderedDict(zip(_FEATURE_PATHNAMES_11_12, FEATURES)),
        OrderedDict(zip(_FEATURE_PATHNAMES_13_17, FEATURES)),
    )

    def get_filings_path(self, year):
        return op.join(self.data_dir, 'index_{year}.csv'.format(year=year))

    @cache_dataframe
    def _fetch_filings_df(self, year, verbose=1, force=False):
        """Fetches the 'index' filing that relates EINs to filing URLs, convert to dataframe."""
        index_url = "https://s3.amazonaws.com/irs-form-990/index_{year}.json".format(
            year=year)
        metadata_file = super(IRS990Dataset, self).fetch(
            files=[(op.basename(index_url), index_url)],
            force=force, verbose=verbose)[0]

        # Load and filter filings
        with open(metadata_file, 'r') as fp:
            metadata = json.load(fp)
        filings = metadata['Filings' + str(year)]
        filings_df = pd.DataFrame(data=filings)
        filings_df['EIN'] = filings_df['EIN'].map(ein_cleaner)
        filings_df['filing_year'] = year  # best to save this off.

        return filings_df

    def fetch_filings_index(self, year, force=False):
        return self._fetch_filings_df(
            year=year, force=force, csv_file=self.get_filings_path(year=year))

    @staticmethod
    def _is_field_path_matched(tag, field_path):
        """Returns True if the tag and parents matches the field_path

        Uses an XPath-ish language (not really)
        """
        def _tag_path(tag):
            parent_names = [getattr(p, "name", "") for p in tag.parents]  # get name, when available
            parent_names = parent_names[::-1]  # reverse order, with root at start
            parent_names = parent_names[1:]  # eliminate root "document"
            return "/".join([""] + parent_names + [tag.name])  # join into a typical path (/foo/bar)
        return _tag_path(tag).endswith(field_path)

    @staticmethod
    def extract_path(parser, field_name, field_paths):
        """Returns all (cleaned) values at a given xpath.

        Parameters
        ----------
        parser: BeautifulSoup parser
             Parser of xml file

        field_name: string
            Name of the field desired (helps with data cleaning)

        field_paths: string or list
            XPath selector for the field.

        Returns
        -------
        np.nan for 0 matches, value for 1 match, list of values for multiple matches
        """
        if isinstance(field_paths, six.string_types):
            field_paths = [field_paths]

        values = []
        for field_path in field_paths:
            # Find all matches
            matches = parser.findAll(partial(IRS990Dataset._is_field_path_matched, field_path=field_path))
            # Clean the elements
            values += [_clean_element_content(field=field_name, val=val)
                       for val in matches]

        # Filter out nans
        values = [v for v in values if not pd.isnull(v)]
        # Unpack values appropriately
        if len(values) == 0:
            return np.nan
        elif len(values) == 1:
            return values[0]
        return values

    def _fetch_and_extract(self, eins, filing_years, tax_year=None, verbose=1, force=False):
        """
        Fetches individual 990 returns (xml) and extracts relevant
        features.

        Parameters
        ----------
        eins : list-like
            List of charity EINs to fetch 990 returns for.
        tax_year : int, optional
            Tax year to get 990 returns for; e.g: 2012
            Returns exist for 2011 - 2016.
            Default: no tax year filter.
        filing_years : int
            Filing years to search for 990 returns for. e.g: (2012,)
        force : bool, optional
            Forcing flag

        Returns
        -------
        rows : list
            List of rows (lists) of features used in the construction of a
            pd.Dataframe. Each row corresponds to a single filing.
        """
        eins_df = pd.DataFrame(data=dict(ein=eins)).set_index('ein')
        output_columns = ['filing_year'] + list(self.FEATURES) + ['src_file']
        output_df = pd.DataFrame(columns=output_columns)

        # a filing for a past year can appear in a future index, so we need to check all future years
        all_ein_filings = []
        # Tax year can go before 2010, but filings only exist from 2011. So, start
        # from 2011, or the earliest date a tax year could be filed, whichever is later.
        for yr in filing_years:
            year_df = self.fetch_filings_index(yr, force=force)
            year_ein_filings_df = year_df.join(eins_df, on='EIN', how='inner')
            del year_df  # heavy-weight; make sure it's cleaned up.

            year_ein_filings_df['filing_year'] = yr
            all_ein_filings.append(year_ein_filings_df)

        ein_filings_df = pd.concat(all_ein_filings, sort=True)
        if len(ein_filings_df) == 0:
            # No matches will foul things up later
            return output_df

        # Fetch the files.
        src_urls = ein_filings_df['URL'].values.tolist()
        ein_filings_df['src_file'] = super(IRS990Dataset, self).fetch(
            files=[(op.basename(url), url) for url in src_urls],
            retries=3,  # hella unreliable...
            verbose=verbose, force=force)

        def _best_parsing(parser, src_file=None):
            """Selects the feature mapping that best fits a given BeautifulSoup parse of a filing (src_file)"""
            best_row = pd.DataFrame(data=[[np.nan for feature in self.FEATURES]], columns=self.FEATURES)
            for feature_mapping in self.FEATURE_MAPPINGS:
                row = [self.extract_path(parser=parser, field_name=field_name, field_paths=field_paths)
                       for field_paths, field_name in feature_mapping.items()]
                row = pd.DataFrame(data=[row], columns=self.FEATURES)

                row_has_form = row['form'].notnull()[0]
                row_has_more_data = row.notnull().sum(axis=1)[0] > best_row.notnull().sum(axis=1)[0]

                if row_has_form and row_has_more_data:
                    best_row = row
                elif row_has_more_data:  # row does not have form info
                    warnings.warn(
                        "Row does not contain form information; likely a parse error in {src_file}. Skipping...".format(
                            src_file=src_file),
                        category=IRS990ParsingWarning)
                else:
                    # Skip all other cases; we will never use data with a null form.
                    pass
            return best_row

        @cache_dataframe
        def parse_form990(src_file, verbose=1):
            """Parses a filing, returns as a one-row dataframe."""
            # paths we know are optional, should not trigger warnings
            KNOWN_MISSING_ENTRIES = (
                'total fundraising expenses, previous year',
                '# volunteers',
            )

            if verbose > 0:
                print('Parsing {src_file}...'.format(src_file=src_file))

            r_xml = text_from_disk(src_file, disk_encoding='utf-8-sig', errors='ignore')
            parser = self.get_bs_xml_obj(r_xml, throw_errors=True)
            parsed_990 = _best_parsing(parser, src_file=src_file)
            null_cols = [col for col in parsed_990.columns if pd.isnull(parsed_990[col]).sum() > 0]
            null_cols = (set(null_cols) - set(KNOWN_MISSING_ENTRIES))
            if verbose > 0 and null_cols:
                print("Warning: row for {src_file} had {n_null_cols} non parseable entries ({null_cols}).".format(
                    src_file=src_file, n_null_cols=len(null_cols), null_cols=null_cols))

            return parsed_990

        def _parse_form_fill_tax_year(row):
            # parse and save file, fill tax year with dummy data if necessary
            csv_file = row['src_file'].replace('.xml', '.features.csv')
            df = parse_form990(src_file=row['src_file'], csv_file=csv_file, verbose=verbose, force=force)
            if tax_year is None or df['tax year'].iloc[0] == tax_year:
                # Accept the row; add metadata & store.
                df['filing_year'] = row['filing_year']
                df['src_file'] = row['src_file']
            return df

        dfs = run_in_parallel(
            _parse_form_fill_tax_year,
            params=[{'row': row} for _, row in ein_filings_df.iterrows()])

        if len(dfs) == 0:
            # No matches found; below code will not return the correct schema,
            # so just exit.
            return output_df

        # Make sure we only select one row per ein & tax year
        output_df = pd.concat(dfs, sort=True)
        output_df['ein'] = output_df['ein'].map(ein_cleaner)
        output_df = output_df.groupby(by=['ein', 'tax year']).apply(
            lambda rows: rows.sort_values(by='filing_year').iloc[-1])

        # Make sure all columns we expect are present
        missing_columns = set(output_columns) - set(output_df.columns)
        for col in missing_columns:
            output_df[col] = np.nan

        return output_df[output_columns]  # ensure column order

    def fetch(self, eins=None, years=(2016,), filing_years=None, verbose=1, force=False):
        """
        Parameters
        ----------
        eins : list-like
            List of charity EINs to fetch 990 returns for.
        years : list-like
            Tax years to get 990 returns for; e.g: 2012
        filing_years : list-like (optional)
            Filing years to search for 990 returns for; e.g: 2012
            (Default: all supported years)
        force : bool
            Forcing re-download & re-parse of data

        Returns
        -------
        out_df : pd.DataFrame
            DataFrame containing 990 returns; each row corresponds
            to a single filing.
        """
        if filing_years is None:
            start_year = max(min(years), self.EARLIEST_YEAR)
            filing_years = list(range(start_year, self.LATEST_YEAR + 1))
        elif np.min(filing_years) < self.EARLIEST_YEAR:
            # Before the first year
            raise NotImplementedError('Filing year {filing_year} is not supported.'.format(
                filing_year=np.min(filing_years)))
        elif np.max(filing_years) > self.LATEST_YEAR:
            # After the last year
            raise NotImplementedError('Filing year {filing_year} is not supported.'.format(
                filing_year=np.max(filing_years)))

        # Fetch all tax years together.
        eins = clean_eins(eins) if eins is not None else None
        downloaded_df = self._fetch_and_extract(
            eins=eins, filing_years=filing_years, verbose=verbose, force=force)

        # Filter to only desired tax years.
        return downloaded_df[downloaded_df['tax year'].isin(years)]

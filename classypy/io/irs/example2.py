"""
Download IRS EINs in 2016.
in 2015 and 2016.
"""
from classypy.io.irs import IrsEINDataset

dataset = IrsEINDataset()
df = dataset.fetch()
print("%d EINs returned." % len(df))
print("columns: %s" % ", ".join(sorted(df.columns)))

"""
Download parts of the IRS Form 990 extract.
"""

from classypy.io.irs import IrsTaxExtractDataset

dataset = IrsTaxExtractDataset()
latest_year = IrsTaxExtractDataset.get_last_available_tax_year()
df = dataset.fetch(years=(latest_year - 1, latest_year), verbose=2)
print("{n_rows} rows across {n_eins} unique EINs returned.".format(
    n_rows=len(df), n_eins=len(df['EIN'].unique())))
print("Columns: {columns}".format(columns=", ".join(sorted(df.columns))))

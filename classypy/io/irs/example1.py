"""
Download IRS 990 returns for three organizations
in 2015 and 2016.
"""
from classypy.io.irs import IRS990Dataset

eins = ['510311790', '340360318', '043356562']

dataset = IRS990Dataset()
df = dataset.fetch(eins=eins, years=(2014, 2015, 2016))
df = df[df['form'].notnull()]  # eliminate not found
print("Downloaded %d results" % len(df))
print("columns: %s" % ", ".join(sorted(df.columns)))
print("Example data: unique values of column 'EIN': %s" % str(df['ein'].unique()))

"""
Fetcher/Dataset for IRS tax extract.

https://www.irs.gov/statistics/soi-tax-stats-annual-extract-of-tax-exempt-organization-financial-data

Contains minimal subset of IRS Form 990 data.
"""
import os.path as op
import re
from datetime import datetime

import pandas as pd

from classypy.charity.ein import ein_cleaner
from classypy.devops import logger
from classypy.io.core._utils.parsing import BeautifulSoupParser
from classypy.io.core.datasets import HttpDataset
from classypy.io.core.fetchers import HttpFetcher


class IrsTaxExtractFetcher(HttpFetcher, BeautifulSoupParser):
    """
    Fetcher for IRS Tax Extract.

    Downloads dat and zip files; xls metadata files.
    see: https://www.irs.gov/statistics/soi-tax-stats-annual-extract-of-tax-exempt-organization-financial-data

    Exceptions:
    * 2014 has no metadata file.
    * some (but not all) ez zip files dump a file with EZ.dat instead of 990ez.dat
    * 2015 has a double-extension (.dat.dat)
    """

    dependencies = HttpFetcher.add_dependencies('xlsx2csv', *BeautifulSoupParser.dependencies)

    BASE_URL = "https://www.irs.gov/statistics/soi-tax-stats-annual-extract-of-tax-exempt-organization-financial-data"

    FILING_YEAR_REGEX = r'^[^\d]*(py|)([0-9]+)(_|eoextract).*\..*$'  # parses filing year from filename.
    TAX_FORM_REGEX = r'^[^\d]*(py|)[0-9]+(_|eoextract)([^.]+)\..*$'  # Parses tax form from filename; deal with .dat.dat
    FILE_EXCEPTIONS = {
        # IRS website packages these files to non-standard locations. Special=case them.
        'py13_990ez.zip': 'py13_EZ.dat',
        'py14_990ez.zip': 'py14_EZ.dat',
        'py15_990ez.zip': 'py15_EZ.dat',
    }

    @classmethod
    def normalize_output_file(cls, filename):
        """Translate dat and zip urls to a single output dat filename."""
        # 14eofinextract990 => py14_990
        filename = re.sub('^([0-9]{2})eofinextract(.*)(\\..*)$', 'py\\1_\\2\\3', op.basename(filename))
        if filename in cls.FILE_EXCEPTIONS:
            filename = cls.FILE_EXCEPTIONS[filename]
        return filename

    @classmethod
    def extract_year(cls, dat_file):
        """Extract 2-digit year part of pyXX_FF.dat filename; convert to 4 digit year.

        Returns
        -------
        4-digit year, in integer form.
        """
        year = '20{match}'.format(match=re.match(cls.FILING_YEAR_REGEX, dat_file).groups()[1])
        return int(year)

    @classmethod
    def extract_form(cls, dat_file):
        """Extract form part of pyXX_FF.dat filename, in lowercase form."""
        form = re.match(cls.TAX_FORM_REGEX, dat_file).groups()[2]
        return form.lower()

    def _fetch_and_parse_links(self, verbose=1, force=False):
        """Fetch html file, parse links."""
        # Fetch HTML contnet
        html_file = super(IrsTaxExtractFetcher, self).fetch([self.BASE_URL], verbose=verbose, force=force)[0]
        with open(html_file, 'r') as fp:
            html_content = fp.read()

        # Parse links.
        parser = self.get_bs_html_obj(content=html_content, tags=('a',), throw_errors=False, encoding='utf-8')
        links = [link['href'] for link in parser if link.has_attr('href')]

        return links

    def fetch_metadata(self, years, verbose=1, force=False):
        """Fetch XLS metadata files that define column mappings to human-readable forms."""
        links = self._fetch_and_parse_links(verbose=verbose, force=force)
        xls_links = [
            link
            for link in links
            if 'extractdoc' in link and (link.endswith('.xls') or link.endswith('.xlsx'))
        ]

        xls_files = [(self.normalize_output_file(link), link, {'move': True})
                     for link in xls_links]

        # Add file for missing 2014: use 2015 schema.
        xls_files += [(xf[0].replace('15', '14'), xf[1], xf[2]) for xf in xls_files if '15' in xf[0]]

        # Filter by year (if specified)
        xls_files = [xf for xf in xls_files if self.extract_year(xf[0]) in years]

        xls_files = super(IrsTaxExtractFetcher, self).fetch(files=xls_files, verbose=verbose, force=force)
        return xls_files

    def xls2dat(self, fil, csv_file=None, force=False):
        from xlsx2csv import Xlsx2csv
        csv_file = csv_file or fil.replace(".xlsx", ".dat")
        if force or not op.exists(csv_file):
            logger.info("Converting {fil} to dat...".format(fil=fil))
            Xlsx2csv(fil, delimiter=" ", outputencoding="utf-8").convert(csv_file)
        return csv_file

    def fetch(self, years, forms, verbose=1, force=False):
        """Fetch data files (some in .dat form, some in .zip form)."""
        # Parse & filter links via HTML content
        links = self._fetch_and_parse_links(verbose=verbose, force=force)

        dat_links = [link for link in links if link.endswith('.dat')]
        zip_links = [link for link in links if link.endswith('.zip')]
        xls_links = [link for link in links if 'extract' in link and link.endswith('.xlsx')]

        # Construct local file / remote url / options tuple
        dat_files = [(self.normalize_output_file(link), link)
                     for link in dat_links]
        zip_files = [(self.normalize_output_file(link).replace('.zip', '.dat'), link, {'uncompress': True})
                     for link in zip_links]
        xls_files = [(self.normalize_output_file(link), link)
                     for link in xls_links]

        # Filter by year
        dat_files = [df for df in dat_files if self.extract_year(df[0]) in years]
        zip_files = [zf for zf in zip_files if self.extract_year(zf[0]) in years]
        xls_files = [xf for xf in xls_files if self.extract_year(xf[0]) in years]

        # Filter by form
        if '990ez' in forms:
            forms = list(forms) + ['ez']
        forms = [form.lower() for form in forms]  # standardize
        dat_files = [df for df in dat_files if self.extract_form(df[0]) in forms]
        zip_files = [zf for zf in zip_files if self.extract_form(zf[0]) in forms]
        xls_files = [xf for xf in xls_files if self.extract_form(xf[0]) in forms]

        # Fetch the dat & zip files (unzip zip to dat)
        dat_files = super(IrsTaxExtractFetcher, self).fetch(dat_files, verbose=verbose, force=force)
        dat_files += super(IrsTaxExtractFetcher, self).fetch(files=zip_files, verbose=verbose, force=force)
        dat_files += [
            (
                fil.replace('.xlsx', '.dat'),
                self.xls2dat(fil, csv_file=fil.replace('.xlsx', '.dat'), force=force),
            )[0]
            for fil in super(IrsTaxExtractFetcher, self).fetch(
                files=xls_files,
                verbose=verbose,
                force=force)
        ]

        # Make sure we got everything
        missing_years = set(years) - set([self.extract_year(df) for df in dat_files])
        if missing_years:
            logger.warn("Missing data for tax years {missing_years}".format(
                missing_years=", ".join([str(y) for y in missing_years])))
        return dat_files


class IrsTaxExtractDataset(HttpDataset):
    """Dataset for IRS Tax extract."""

    fetcher_class = IrsTaxExtractFetcher

    FORM_TO_SHEET_MAP = {
        # Mapping from a form name to a XLS sheet name
        '990': '990',
        '990ez': '990-EZ',
        'ez': '990-EZ',
        '990pf': '990-PF',
    }
    TAX_PERIOD_COLUMNS = (
        "taxpd", "tax_pd", "taxprd", "tax_prd", "a_tax_prd"
    )
    KNOWN_NON_NUMERIC_COLUMNS = (
        "actnotpr", "EIN", "elf", "form", "infleg", "schdbind", "tax period", "nonpfrea"
    )

    @staticmethod
    def get_last_available_tax_year():
        """We expect tax extract data to be released in July."""
        now = datetime.now()
        return now.year - 1 if now.month >= 7 else now.year - 2

    def _read_xls_to_column_map(self, xls_file, form):
        """
        Read the columns from an XLS sheet that produce a column => pretty mapping.

        Returns
        -------
        A dict usable from df.rename().
        """
        # Skip the first two rows of the metadata, as they are garbage.
        # Row 3 is the column headers, Rows 4+ are data.
        metadata = pd.read_excel(xls_file, skiprows=2, sheet_name=self.FORM_TO_SHEET_MAP[form.lower()])
        column_map = dict(zip(metadata['Element Name'], metadata['Description']))
        # Hack to avoid commas in column names, for search/replace trickery.
        column_map = {key: val.replace(',', ';') for key, val in column_map.items()}
        return column_map

    @classmethod
    def _get_tax_period_column(cls, df):
        for col in cls.TAX_PERIOD_COLUMNS:
            if col in df:
                return col
        raise Exception("Could not find any tax period columns in [{cols}]".format(
            cols=", ".join(df.columns)))

    def _apply_friendly_names(self, df, year, form):
        xls_files = self.fetcher.fetch_metadata(years=(int(year),))
        column_map = self._read_xls_to_column_map(xls_files[0], form=form)
        return df.rename(columns=column_map)

    def _read_dat(self, dat_file):
        """Read dat file format, add filing year / tax form metadata."""
        # Extract metadata from filename
        year = self.fetcher.extract_year(dat_file)
        form = self.fetcher.extract_form(dat_file)

        df = pd.read_csv(dat_file, sep=' ')

        # lowercase column names
        df = df.rename(columns={col: col.lower() for col in df.columns if col != col.lower()})

        # parse out tax period to datetime; it's in the format YYYYMM
        tax_pd = df[self._get_tax_period_column(df)]
        df['tax period'] = tax_pd.map(lambda tp: datetime.strptime(str(tp), "%Y%m").date())

        # Append filing year/form metadata.
        df['filing year'] = year
        df['form'] = form

        return df

    @staticmethod
    def _get_tax_years(years, use_friendly_names):
        if years is None and not use_friendly_names:
            # don't know when the new dataset will be released, so hard-code
            return list(range(2012, IrsTaxExtractDataset.get_last_available_tax_year()))
        elif years is None and use_friendly_names:
            # Can't parse metadata for 2012, so must exclude.
            return list(range(2013, IrsTaxExtractDataset.get_last_available_tax_year()))
        elif use_friendly_names and 2012 in years:
            # Can't parse metadata for 2012, so must exclude.
            raise ValueError("Cannot specify use_friendly_names and year 2012; metadata sucks.")
        return years

    @classmethod
    def is_numeric_col(cls, col):
        """Determine if a column is numeric."""
        return (
            not col.endswith("cd") and
            not col.endswith("ind") and  # ind are indicator (Y/N) columns
            col not in cls.KNOWN_NON_NUMERIC_COLUMNS
        )

    def fetch(self, years=None, forms=None, force=False, verbose=1, use_friendly_names=False):
        """Download tax extract for all years into a single dataframe."""
        # Fill in defaults
        forms = forms or ('990', '990ez', '990pf')
        years = self._get_tax_years(years=years, use_friendly_names=use_friendly_names)

        if use_friendly_names:
            # Pre-fetch & cache metadata.
            self.fetcher.fetch_metadata(years=years, force=force, verbose=verbose)

        # Fetch data and convert columns (if requested)
        dfs = [self._read_dat(dat_file)
               for dat_file in self.fetcher.fetch(
                   years=years, forms=forms,
                   force=force, verbose=verbose)]

        # do basic remapping of primary keys
        dfs = [df.rename(columns={"ein": "EIN"}) for df in dfs]
        dfs = [df.rename(columns={self._get_tax_period_column(df): "tax_pd"}) for df in dfs]
        for df in dfs:
            for col in set(df.columns).intersection(set(self.TAX_PERIOD_COLUMNS) - set(["tax_pd"])):
                del df[col]

        df = pd.concat(dfs, sort=True)

        # Standardize format of EINs (otherwise pandas may convert to int)
        df['EIN'] = df['EIN'].map(ein_cleaner)
        assert all(df['EIN'].notnull())

        # Convert to numeric
        numeric_cols = sorted([col for col in df.columns if self.is_numeric_col(col)])
        for col in numeric_cols:
            df[col] = df[col].astype(float)

        # Now rename columns
        if use_friendly_names:
            for form in forms:
                df = self._apply_friendly_names(df, year=max(years), form=forms)

        return df

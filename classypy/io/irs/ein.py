"""
ein.py
"""
import pandas as pd

from classypy.charity.ein import ein_cleaner
from classypy.io.core.datasets import HttpDataset


def _get_eins_dataframe_from_file(txt_path):
    """
    Loads and returns a pd.DataFrame of IRS compilied EINs.
    EINs are formatted prior to the DataFrame return.
    """
    cols = ['ein', 'name', 'city', 'state', 'country', 'dec_status']
    ein_ref_df = pd.read_csv(
        txt_path, sep='|', names=cols, dtype={'ein': str},
        error_bad_lines=False)
    ein_ref_df['ein'] = ein_ref_df['ein'].map(ein_cleaner)
    return ein_ref_df


class IrsEINDataset(HttpDataset):

    def fetch(self, force=False, verbose=1):
        """
        Download ein file, load it into a dataframe, then clean it.
        """
        file_stem = 'data-download-pub78'
        url = "https://apps.irs.gov/pub/epostcard/%s.zip" % file_stem
        local_file = "%s.txt" % file_stem
        local_path = self.fetcher.fetch(
            ((local_file, url, {'uncompress': True}),),
            force=force, verbose=verbose)[0]

        df = _get_eins_dataframe_from_file(local_path)
        return df

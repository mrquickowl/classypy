"""
Download the latest IRS EOBMF dataset.
"""

from classypy.io.irs import IrsEOBMFDataset

dataset = IrsEOBMFDataset()
df = dataset.fetch()
print("%d EINs returned." % len(df))
print("columns: %s" % ", ".join(sorted(df.columns)))

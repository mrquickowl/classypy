"""
Get desk.com case replies for 10 cases.
"""
from __future__ import print_function

from classypy.devops import find_secrets
from classypy.io.desk import DeskCaseRepliesDataset

secrets = find_secrets()
replies_dataset = DeskCaseRepliesDataset(
    username=secrets['DESK_API_USERNAME'],
    passwd=secrets['DESK_API_PASSWORD'])

replies_df = replies_dataset.fetch(case_ids=list(range(1, 11)))
print("Fetched %d replies" % len(replies_df))
print("columns: %s" % ", ".join(replies_df.columns))
print(replies_df)

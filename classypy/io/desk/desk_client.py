"""
Generic client for accessing desk.com cases
"""

from classypy.devops import logger
from classypy.io.core.fetchers import HttpFetcher
from classypy.text import json_from_disk


class DeskAPIClient(HttpFetcher):
    """
    Use requests to fetch desk data.
    """
    API_URL = 'https://support.classy.org/api/v2'

    def _get(self, url, params=None, force=False):
        logger.info("Fetching {url}...".format(url=url))
        files = self.fetch([self.construct_url(url, params=params)], force=force, verbose=0)
        return json_from_disk(files[0])

    def _get_entity_by_id(self, eid, url_ext=None, params=None, force=False):
        return self._get('%s/%s/%s' % (self.API_URL, url_ext, str(eid)), params=params, force=force)

    def _get_first_class_entity_by_id(self, eid, entity_type=None, params=None, force=False):
        return self._get_entity_by_id(eid, url_ext=entity_type, params=params, force=force)

    def _get_child_entity_by_id(self, eid, child_type=None,
                                parent_id=None, parent_type=None, params=None, force=False):
        ext = '%s/%s/%s' % (parent_type, str(parent_id), child_type)
        return self._get_entity_by_id(eid, url_ext=ext, params=params, force=force)

    def _get_entity_page(self, page, url_ext=None, params=None, force=False):
        params = params if params is not None else {}
        params.update({'page': page,
                       'sort_field': 'created_at',
                       # Reverse order for 'since'.
                       'sort_direction': 'asc'})

        page_obj = self._get('%s/%s' % (self.API_URL, url_ext), params=params, force=force)

        return {'entities': page_obj['_embedded']['entries'],
                'next_page': page_obj['_links']['next'] is not None}

    def _get_first_class_entity_page(self, page, entity_type=None, params=None, force=False):
        return self._get_entity_page(page, url_ext=entity_type, params=params, force=force)

    def _get_child_entity_page(self, page, child_type=None, parent_id=None,
                               parent_type=None, params=None, force=False):
        ext = '%s/%s/%s' % (parent_type, str(parent_id), child_type)
        return self._get_entity_page(page, url_ext=ext, params=params, force=force)

    def get_article_by_id(self, article_id, force=False):
        return self._get_first_class_entity_by_id(article_id, entity_type="articles", force=force)

    def get_case_by_id(self, case_id, force=False):
        return self._get_first_class_entity_by_id(
            case_id, entity_type="cases", params={'embed': 'message'}, force=force)

    def get_reply_by_id(self, reply_id, case_id, force=False):
        return self._get_child_entity_by_id(
            reply_id, child_type="replies", parent_id=case_id, parent_type="cases", force=force)

    def get_articles_by_page(self, page, force=False):
        return self._get_first_class_entity_page(page, entity_type="articles", force=force)

    def get_cases_by_page(self, page, since_created=None, force=False):
        params = {'embed': 'message'}
        if since_created is not None:
            params['since_created_at'] = since_created
        return self._get_first_class_entity_page(page, entity_type="cases", params=params, force=force)

    def get_case_replies_by_page(self, case_id, page, force=False):
        return self._get_child_entity_page(
            page, child_type="replies", parent_id=case_id, parent_type="cases", force=force)

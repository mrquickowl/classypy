from __future__ import print_function

import os
import os.path as op
import re
import warnings
from datetime import datetime

import dateutil
import pandas as pd

from classypy.compat import u as unicode
from classypy.io.core.datasets import Dataset
from classypy.io.core.fetchers import HttpApiFetcher
from classypy.io.desk.desk_client import DeskAPIClient
from classypy.text import cleaning, text_from_disk
from classypy.util.dates import convert_datetime_columns
from classypy.util.dirs import this_files_dir
from classypy.util.etl import to_ascii
from classypy.util.execution import run_in_parallel


def _is_flat(thing):
    return type(thing) not in [list, dict]


def _flatten(dikt, keep_fields=None):
    return {k: v for k, v in dikt.items() if _is_flat(v)}


def vprint(content, verbose=1):
    if verbose > 0:
        print(content)


def normalize_article_url(url):
    rgx = re.compile(r'http[s]?:\/\/support\.classy\.org\/customer\/(?:en\/)?portal\/articles\/[0-9]+')
    match = re.search(rgx, url)
    if match is None:
        print("WARNING: found support url {url} not matching help article"
              "format.".format(url=unicode(url)))
    else:
        match = str(match.group(0).replace("en/", "").replace("http://", "https://"))
    return match


def get_unix_timestamp(dt_string):
    """
    returns total seconds from the datetime string given to the unix epoch,
    1970-01-01T00:00:00
    """
    dt = dateutil.parser.parse(dt_string)
    return int((dt.replace(tzinfo=None) - datetime(1970, 1, 1)).total_seconds())


class DeskAPIFetcher(HttpApiFetcher):
    dependencies = HttpApiFetcher.add_dependencies(*DeskAPIClient.dependencies)

    ENTRIES_PER_PAGE = 50  # number of entries provided in a page from the desk api
    PAGE_ASK_LIMIT = 500  # highest number of pages desk allows you to ask for in an api call

    def __init__(self, data_dir=None, username=None, passwd=None):
        super(DeskAPIFetcher, self).__init__(data_dir=data_dir)

        username = username or os.environ.get("DESK_API_USERNAME")
        passwd = passwd or os.environ.get("DESK_API_PASSWORD")
        if username is None or passwd is None:
            raise ValueError("Must define DESK_API_USERNAME and "
                             "DESK_API_PASSWORD environment variables, "
                             "or pass in username and passwd arguments.")

        self.client = DeskAPIClient(data_dir=data_dir, username=username, passwd=passwd)

        self.cache_dirs = {
            "main": self.data_dir,
            "processed": op.join(self.data_dir, "processed"),
            "raw": op.join(self.data_dir, "raw"),
            "articles": op.join(self.data_dir, "raw", "articles"),
            "cases": op.join(self.data_dir, "raw", "cases"),
            "replies": op.join(self.data_dir, "raw", "replies")
        }

    def _download_articles(self, article_ids=None, since_created=None,
                           max_entries=None, force=False, verbose=1):
        assert since_created is None
        if article_ids is not None:
            return [self.client.get_article_by_id(aid, force=force)
                    for aid in article_ids]

        page, next_page, dataset = 1, True, []

        while next_page:
            articles_data = self.client.get_articles_by_page(page, force=force)
            for entry in articles_data['entities']:
                entry["public_url"] = normalize_article_url(entry["public_url"])

            dataset.extend(articles_data['entities'])
            if (max_entries is not None) and (len(dataset) > max_entries):
                break

            next_page = articles_data['next_page']
            page += 1

        return dataset

    def fetch_articles(self, article_ids=None, start_date=None,
                       max_entries=None, force=False, verbose=1):
        if article_ids is not None:
            return self._download_articles(
                article_ids=article_ids, force=force, verbose=verbose)

        articles = self._download_articles(
            since_created=None if start_date is None else get_unix_timestamp(start_date),
            max_entries=max_entries, force=force, verbose=verbose)
        if max_entries is not None:
            articles = articles[:max_entries]

        return articles

    def _download_cases(self, case_ids=None, start_page=1,
                        since_created=None, max_cases=None, force=False, verbose=1):
        if case_ids is not None:
            return run_in_parallel(
                func=self.client.get_case_by_id,
                params=[dict(case_id=cid, force=force) for cid in case_ids])

        page, since = start_page, since_created
        next_page, case_ids, dataset = True, set(), []

        while next_page:
            # need to ask desk for pages 1 through 500, not 0 through 499,
            # so if page % page_limit (page_limit is 500) is 0, ask for
            # page limit (500); else ask for page % page limit (1 - 499).
            cases_data = self.client.get_cases_by_page(
                page % self.PAGE_ASK_LIMIT
                if page % self.PAGE_ASK_LIMIT != 0
                else self.PAGE_ASK_LIMIT,
                since_created=since, force=force)

            for case in cases_data['entities']:
                if case['id'] in case_ids:
                    vprint("Found duplicate of case {case_id} in page {page_num}. "
                           "Omitting.".format(case_id=case['id'], page_num=page),
                           verbose=verbose)
                    cases_data['entities'].remove(case)

            case_ids.update([case['id'] for case in cases_data['entities']])
            dataset.extend(cases_data['entities'])
            if (max_cases is not None) and (len(dataset) > max_cases):
                break

            # update the since_created_date every 500 pages so we can
            # start over asking for page 1. Use -1 index since pages
            # are sorted in ascending order by created_at. Add 1 second to
            # the time so Desk API doesn't regrab the case it came from.
            if page % self.PAGE_ASK_LIMIT == 0:
                since = get_unix_timestamp(dataset[-1]['created_at']) + 1

            next_page = cases_data['next_page']
            page += 1

        return dataset

    def fetch_cases(self, case_ids=None, max_cases=None, start_date=None,
                    force=False, verbose=1):
        if case_ids is not None:
            # Download specific cases
            return self._download_cases(
                case_ids=case_ids, force=force, verbose=verbose)

        cases = self._download_cases(
            since_created=None if start_date is None else get_unix_timestamp(start_date),
            max_cases=max_cases, force=force, verbose=verbose)

        if max_cases is not None:
            cases = cases[:max_cases]

        return cases

    def _download_case_replies(self, case_id, max_entries=None, start_date=None,
                               force=False, verbose=1):
        assert start_date is None
        page, next_page, dataset = 1, True, []

        while next_page:
            replies_data = self.client.get_case_replies_by_page(
                case_id, page=page, force=force)

            dataset.extend(replies_data['entities'])
            next_page = replies_data['next_page']

            if (max_entries is not None) and (len(dataset) > max_entries):
                break

        return dataset

    def fetch_case_replies(self, case_id, max_entries=None, start_date=None,
                           force=False, verbose=1):
        replies = self._download_case_replies(
            case_id, max_entries=max_entries, start_date=start_date,
            force=force, verbose=verbose)

        # Only save the whole set
        if max_entries is not None:
            replies = replies[:max_entries]

        return replies


class DeskDataset(Dataset):
    fetcher_class = DeskAPIFetcher

    @classmethod
    def clean_text(cls, df, cols, filter_words, keep_raw=False, verbose=1):
        # Remove unicode characters.
        for col in cols:
            if keep_raw:
                df['raw_' + col] = df[col]
            df[col] = df[col].map(lambda b: '' if pd.isnull(b) else b)
            df[col] = df[col].map(to_ascii)
            df[col] = df[col].str.lower()

        # remove urls and email addresses
        url_rgx = (r'(https?:\/\/)?(?:www\.)?([\w-]+(?:\.[\w-]+)+)'
                   r'((?:\/[\w-]+)*)\/?((?:\??[\w-]+=[^\s]+)?(?:&[\w-]+=[^\s]+)*)?')
        email_rgx = r'([a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+)'
        phone_rgx = r'\(?[\d]{3}\)?[\- .]?[\d]{3}[\- .]?[\d]{4}'

        meta_text_rgxs = {
            r'(>* *from: [^\n]+)': ' ',
            r'(>* *from url: [^\n]+)': ' ',
            r'(>* *coming from: [^\n]+)': ' ',
            r'(>* *location: [^\n]+)': ' ',
            r'(>* *more details: [^\n]+)': ' ',
            r'(>* *date: [^\n]+)': ' ',
            r'(>* *to: [^\n]+)': ' ',
            r'(>* *subject: [^\n]+)': ' ',
            r'(>* *reply-to: [^\n]+)': ' ',
            r'(>* *widget embed page: [^\n]+)': ' ',
            r'(>* *browser: [^\n]+)': ' ',
            r'(>* *user agent: [^\n]+)': ' ',
            r'(>* *chat transcript:[^\n]*)': ' ',
            r'(>* *livechat conversation transcript:[^\n]*)': ' ',
            r'(>* *donation amount: [^\n]+)': ' ',
            r'(>* *donor name: [^\n]+)': ' ',
            r'(>* *federal tax id: [^\n]+)': ' ',
            r'(>* *donation date: [^\n]+)': ' ',
            r'(>* *confirmation number: [^\n]+)': ' ',
            r'\[visitor page reloaded. new url: {url_rgx} \]'.format(url_rgx=url_rgx): ' ',
            r'( ?visitor:)': ' ',
            r'this message is powered by stayclassy': ' ',
            r'sent from my iphone': ' ',
            r'to whom it may concern': ' ',
        }

        space_punctuation_rgx = r'[<>{}\[\].,"?!@#$%^&*()_\-+=|:;~`\/\\]'
        nospace_punctuation_rgx = r'\''
        if verbose > 0:
            print("Removing formatted text...")
        df = df.replace({col: meta_text_rgxs for col in cols}, regex=True)
        if verbose > 0:
            print("Removing urls/emails...")
        df = df.replace({col: {url_rgx: '', email_rgx: '', phone_rgx: ''} for col in cols}, regex=True)

        # remove punctuation & non-space whitespace
        if verbose > 0:
            print("Removing punctuation/newlines...")
        for col in cols:
            # Html tags contain punctuation, so strip them before punctuation.
            df[col] = df[col].map(cleaning.strip_tags)
        df = df.replace({col: {space_punctuation_rgx: ' '} for col in cols}, regex=True)
        df = df.replace({col: {nospace_punctuation_rgx: ''} for col in cols}, regex=True)
        df = df.replace({col: {r'(\n|\r|\t)': ' '} for col in cols}, regex=True)
        df = df.replace({col: {r'\s+': ' '} for col in cols}, regex=True)

        # remove filter words from list, make sure to remove larger words first.
        # words are removed via a large OR statement regex.
        if verbose > 0:
            print("Sorting in reverse...")
        filter_words.sort(reverse=True)

        # join with ' |' so that if a filterword is part of another, it won't be removed.
        if verbose > 0:
            print("Removing specific words...")
        replacements = {r'({words})'.format(words=' | '.join(filter_words)): ' '}
        df = df.replace({col: replacements for col in cols}, regex=True)

        if verbose > 0:
            print("Done.")
        return df

    def clean(self, df, cols, verbose=1):
        # Load filter words
        filter_words_path = op.join(this_files_dir(), 'filter_words.csv')
        filter_names_path = op.join(this_files_dir(), 'filter_names.csv')

        filter_words = text_from_disk(filter_words_path).splitlines()
        filter_words.extend(text_from_disk(filter_names_path).splitlines())

        df = self.clean_text(df, cols=cols, filter_words=filter_words, verbose=verbose)
        return df


class DeskArticlesDataset(DeskDataset):
    DATETIME_COLUMNS = ('created_at', 'publish_at', 'updated_at')

    def _flatten(self, article):
        match = re.search(r'\/(\d+)', article['_links']['topic']['href'])
        if match:
            article['topic_id'] = match.groups(0)[0]
        return _flatten(article)

    def clean(self, df, verbose=1):
        return super(DeskArticlesDataset, self).clean(
            df=df, cols=['message_body'], verbose=verbose)

    @convert_datetime_columns(column_names=DATETIME_COLUMNS)
    def fetch(self, article_ids=None, max_entries=None, start_date=None,
              clean=False, force=False, verbose=1):
        articles = self.fetcher.fetch_articles(
            article_ids=article_ids, max_entries=max_entries, start_date=start_date,
            force=force, verbose=verbose)

        df = pd.DataFrame(data=[self._flatten(a) for a in articles])
        if clean:
            df = self.clean(df)
        return df


class DeskCasesDataset(DeskDataset):
    COLUMNS = (
        'active_at', 'active_attachments_count', 'active_notes_count', 'api_issue_type',
        'blurb', 'campaign_type', 'case_csat', 'case_is_spam', 'case_link', 'case_status',
        'changed_at', 'component', 'created_at', 'customer', 'description', 'desk_case_number',
        'desk_com_agent', 'email_form_issue_type', 'external_id', 'first_opened_at',
        'first_resolved_at', 'has_failed_interactions', 'has_pending_interactions', 'id',
        'is_duplicate_ticket', 'issue_type', 'jira', 'language', 'locked_until',
        'message_agent_answer_count', 'message_answers_disallowed_at',
        'message_are_answers_disallowed', 'message_bcc', 'message_body', 'message_cc',
        'message_chat_state', 'message_client_type', 'message_created_at',
        'message_customer_answer_count', 'message_customer_last_polled_at', 'message_direction',
        'message_ended_at', 'message_entered_at', 'message_erased_at', 'message_from',
        'message_hidden', 'message_hidden_at', 'message_hidden_by', 'message_id',
        'message_last_customer_message_at', 'message_public_url', 'message_sent_at',
        'message_status', 'message_subject', 'message_to', 'message_twitter_status_id',
        'message_type', 'message_updated_at', 'mute', 'opened_at', 'priority', 'proplus_ent',
        'received_at', 'resolved_at', 'route_status', 'salesforce_issue_type', 'salesforce_version',
        'sf_support', 'status', 'subject', 'to_sf', 'type', 'updated_at', 'urgency',
    )

    DATETIME_COLUMNS = (
        'active_at', 'changed_at', 'created_at', 'first_opened_at', 'first_resolved_at', 'locked_until',
        'message_answers_disallowed_at', 'message_created_at', 'message_customer_last_polled_at', 'message_ended_at',
        'message_entered_at', 'message_erased_at', 'message_hidden_at', 'message_last_customer_message_at',
        'message_sent_at', 'message_updated_at', 'opened_at', 'received_at', 'resolved_at', 'updated_at',
    )

    def _flatten(self, case):
        # bring the nested message object fields into the top level of the
        # case object; prefix 'message_' to each field name.
        if '_embedded' in list(case.keys()):
            case.update({'message_%s' % k: v
                         for k, v in case['_embedded']['message'].items()
                         if _is_flat(v)})

        case.update({k: v for k, v in case['custom_fields'].items()})
        return _flatten(case)

    def clean(self, df, verbose=1):
        return super(DeskCasesDataset, self).clean(
            df=df, cols=['message_body'], verbose=verbose)

    @convert_datetime_columns(column_names=DATETIME_COLUMNS, warn_on_missing_columns=False)  # no warn: dynamic schema
    def fetch(self, case_ids=None, max_cases=None, start_date=None,
              clean=False, force=False, verbose=1):
        cases = self.fetcher.fetch_cases(
            case_ids=case_ids, max_cases=max_cases, start_date=start_date,
            force=force, verbose=verbose)
        return pd.DataFrame(data=[self._flatten(c) for c in cases])

        df = pd.DataFrame(data=[self._flatten(c) for c in cases])
        if clean:
            df = self.clean(df)
        return df


class DeskCaseRepliesDataset(DeskDataset):
    COLUMNS = (
        'bcc', 'body', 'case_id', 'cc', 'client_type', 'created_at',
        'direction', 'entered_at', 'erased_at', 'event_type', 'from', 'hidden', 'hidden_at',
        'hidden_by', 'id', 'is_best_answer', 'rating', 'rating_count', 'rating_score',
        'sent_at', 'status', 'subject', 'to', 'twitter_status_id', 'type', 'updated_at',
    )

    DATETIME_COLUMNS = ('created_at', 'entered_at', 'erased_at', 'hidden_at', 'sent_at', 'updated_at')

    def _flatten(self, reply):
        return _flatten(reply)

    def clean(self, df, verbose=1):
        return super(DeskCaseRepliesDataset, self).clean(
            df=df, cols=['body'], verbose=verbose)

    @convert_datetime_columns(column_names=DATETIME_COLUMNS, warn_on_missing_columns=False)  # no warn = dynamic schema
    def fetch(self, case_ids=None, start_date=None, clean=False, force=False, verbose=1):
        if case_ids is None:
            cases = DeskCasesDataset().fetch(start_date=start_date, verbose=verbose)
            case_ids = list(cases['id'])

        def _fetch_case_replies(case_id):
            try:
                case_replies = self.fetcher.fetch_case_replies(
                    case_id, start_date=start_date,
                    force=force, verbose=verbose)
                for reply in case_replies:
                    reply['case_id'] = case_id

                return case_replies
            except Exception as ex:  # noqa
                warnings.warn("Error fetching case replies for {case_id}: {ex}".format(case_id=case_id, ex=ex))
                return []

        # Download data
        list_of_case_replies = run_in_parallel(
            func=_fetch_case_replies,
            params=[dict(case_id=case_id) for case_id in case_ids])

        # Reformat to dataframe
        all_replies = [self._flatten(reply) for case_replies in list_of_case_replies for reply in case_replies]
        df = pd.DataFrame(data=all_replies) if all_replies else pd.DataFrame(columns=self.COLUMNS)
        if clean:
            df = self.clean(df)

        return df

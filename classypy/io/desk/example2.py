"""
Get 100 desk.com support cases.
"""
from __future__ import print_function

from classypy.devops import find_secrets
from classypy.io.desk import DeskCasesDataset

secrets = find_secrets()
cases_dataset = DeskCasesDataset(
    username=secrets['DESK_API_USERNAME'],
    passwd=secrets['DESK_API_PASSWORD'])

cases_df = cases_dataset.fetch(max_cases=100)
print("Fetched %d cases" % len(cases_df))
print("columns: %s" % ", ".join(cases_df.columns))

"""
Get 100 desk.com support articles
"""
from __future__ import print_function

from classypy.devops import find_secrets
from classypy.io.desk import DeskArticlesDataset

secrets = find_secrets()
dataset = DeskArticlesDataset(
    username=secrets['DESK_API_USERNAME'],
    passwd=secrets['DESK_API_PASSWORD'])

df = dataset.fetch(max_entries=100)
print("Fetched %d articles" % len(df))
print("columns: %s" % ", ".join(sorted(df.columns)))

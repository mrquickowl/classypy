import os.path as op
import pickle
import warnings
from datetime import datetime, timedelta
from functools import partial

import dateutil.parser as date_parser
import pandas as pd

from ...compat import _urllib
from ...util.dates import convert_datetime_columns, datetime_to_utc
from .base import GoogleDataset, GoogleFetcher


def chunker(arr, chunk_sz):
    for ci in range(0, len(arr), chunk_sz):
        yield arr[ci:ci + chunk_sz]


class SampledDataWarning(UserWarning):
    pass


class GoogleAnalyticsFetcher(GoogleFetcher):
    # These are the Google Analytics metrics that can be converted to datetimes
    # https://developers.google.com/analytics/devguides/reporting/core/dimsmets#view=detail&group=time
    service_name = 'analytics'
    service_version = 'v3'
    service_scopes = 'https://www.googleapis.com/auth/analytics.readonly'

    def _fetch(self, metrics, force=False, verbose=1, max_pages=100, **kwargs):
        """Returns an array of responses from GA, one response per page."""
        # Recursion check (can only have so many metrics...)
        metrics_arr = sorted(metrics.split(","))
        if len(metrics_arr) <= 10:
            # Sort metrics for better caching
            metrics = ",".join(metrics_arr)
        else:
            # Recursive base-case.
            # Google Analytics limits to 10 metrics at a time.
            # So, let's chunk to 10 at a time
            fetched_metrics = []
            for mi, metrics_chunk in enumerate(chunker(metrics_arr, chunk_sz=10)):
                if verbose > 0:
                    print("Fetching chunk {chunk_number}...".format(chunk_number=mi + 1))
                fetched_metrics += self.fetch(
                    metrics=",".join(metrics_chunk), force=force,
                    verbose=verbose, max_pages=max_pages, **kwargs)
            return fetched_metrics

        # Massage params, cache check
        cache_file = self._cache_file_from_kwargs(**kwargs)
        if not force and op.exists(cache_file):
            with open(cache_file, 'rb') as fp:
                return pickle.load(fp)

        values_arr = []
        for pi in range(max_pages):  # Do a max # of pages
            result = self.get_service().data().ga().get(metrics=metrics, **kwargs)
            values = result.execute()
            values_arr.append(values)

            # Exit the loop, or get the next start index
            if not values.get('nextLink'):
                break
            if verbose > 0:
                print("Continue to page %d (%s)" % (pi + 2, values['nextLink']))
            try:
                urls = _urllib.parse.urlparse(values['nextLink'])
                qs = _urllib.parse.parse_qs(urls.query)
                kwargs['start_index'] = qs['start-index'][0]
            except Exception as ex:  # noqa
                # TODO: narrow down possible parse exceptions
                if verbose > 0:
                    print("Error parsing urls: %s" % ex)
                break

        with open(cache_file, 'wb') as fp:
            pickle.dump(values_arr, fp)
        return values_arr

    def fetch(self, metrics, force=False, verbose=1, max_pages=100, days_per_window=None, **kwargs):
        if days_per_window is None:
            return self._fetch(metrics=metrics, force=force, verbose=verbose, max_pages=max_pages, **kwargs)

        assert 'start_date' in kwargs
        assert 'end_date' in kwargs
        start_date = date_parser.parse(kwargs['start_date'])
        end_date = date_parser.parse(kwargs['end_date']) + timedelta(days=1)

        values_arr = []
        while start_date < end_date:
            kwargs['start_date'] = str(start_date.date())
            kwargs['end_date'] = str(min(end_date, start_date + timedelta(days=days_per_window)).date())
            values_arr += self._fetch(metrics=metrics, force=force, verbose=verbose, max_pages=max_pages, **kwargs)
            start_date = date_parser.parse(kwargs['end_date']) + timedelta(days=1)
        return values_arr


class GoogleAnalyticsDataset(GoogleDataset):
    fetcher_class = GoogleAnalyticsFetcher
    DATETIME_METADATA = {
        # Mappings for massage_dates:
        # format - strptime format for parsing dates from Google's funky format.
        # convert_tz - if True, can map datetime to UTC.
        'ga:date': {'format': '%Y%m%d', 'convert_tz': False},
        'ga:dateHour': {'format': '%Y%m%d%H', 'convert_tz': True},
        'ga:dateHourMinute': {'format': '%Y%m%d%H%M', 'convert_tz': True},
    }
    DATETIME_COLUMNS = tuple(DATETIME_METADATA.keys())

    def _convert_rows(self, rows, column_headers):
        column_types = [col['dataType'] for col in column_headers]

        converted_rows = []
        for row in rows:
            converted_row = []
            for val, dtype in zip(row, column_types):
                try:
                    if dtype == 'INTEGER':
                        val = int(val)
                except ValueError as ve:
                    print("Failed conversion: %s" % ve)
                converted_row.append(val)
            converted_rows.append(converted_row)
        return converted_rows

    @classmethod
    def massage_dates(cls, df, source_tz=None):
        """Map Google-specific date formatting to something usable by Pandas, then map times to UTC."""
        for col, metadata in cls.DATETIME_METADATA.items():
            if col not in df:
                continue

            # First reformat strings.
            df[col] = df[col].map(partial(
                lambda ts, format: datetime.strptime(ts, format),
                format=metadata['format']))

            # Next, tweak timezone.
            if source_tz and metadata['convert_tz']:
                df[col] = df[col].map(partial(datetime_to_utc, local_tz=source_tz))

        return df

    @convert_datetime_columns(column_names=DATETIME_COLUMNS, warn_on_missing_columns=False)  # cols are optional
    def fetch(self, days_per_window=None, source_tz=None, force=True, verbose=1, **kwargs):
        """
        Fetch data via the Google Analytics API

        Parameters
        ----------
        days_per_window : int (Default: None)
            # of days to fetch per request (to avoid rate limiting).
        source_tz : timezone (Default: None)
            Timezone of account; used to convert datetimes to UTC
        force : bool (Default: True)
            Force reload of cached data.
        verbose : bool (Default: 1)
            Level to log progress to screen.
        kwargs : list of arguments to pass to the Google Analytics API.

        Returns
        -------
        dataframe : Flattened list of Analytics results.

        """
        # Gather data, append to current results
        cur_dfs = []
        values_arr = self.fetcher.fetch(
            days_per_window=days_per_window, force=force,
            verbose=verbose, **kwargs)
        for values in values_arr:
            if values.get('containsSampledData', False):
                warnings.warn("Google data contains sampled data.", SampledDataWarning)

            cols = [r['name'] for r in values['columnHeaders']]
            if values.get('rows') is not None:
                rows = self._convert_rows(values['rows'], values['columnHeaders'])
            else:  # Sometimes Google doesn't return any results.
                rows = []
            cur_df = pd.DataFrame(data=rows, columns=cols)
            cur_df = self.massage_dates(cur_df, source_tz=source_tz)
            cur_dfs.append(cur_df)

        return pd.concat(cur_dfs, sort=True)

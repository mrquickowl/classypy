"""
Extract Google Analytics goals, by date and browser.
"""
from classypy.devops.secrets import find_secrets
from classypy.io.google import GoogleAnalyticsDataset

# Read env file into environ, if available
secrets = find_secrets()

dataset = GoogleAnalyticsDataset(
    client_id=secrets['GOOGLE_API_CLIENT_ID'],
    client_secret=secrets['GOOGLE_API_CLIENT_SECRET'],)

metrics = ["ga:goal{gi}Starts,ga:goal{gi}Completions,ga:goal{gi}ConversionRate".format(gi=gi)
           for gi in range(1, 21)]
df = dataset.fetch(
    ids='ga:7627919',
    start_date='2017-01-01',
    end_date='2017-01-31',
    dimensions='ga:date,ga:devicecategory',
    metrics=",".join(metrics))

for gi in range(1, 21):
    agg_df = df.groupby('ga:devicecategory').agg({
        'ga:devicecategory': 'first',
        'ga:goal{gi}Completions'.format(gi=gi): sum,
        'ga:goal{gi}Starts'.format(gi=gi): sum
    })
    print(agg_df)
    print("")

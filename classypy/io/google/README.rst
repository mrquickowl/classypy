Google

Notes
-----
This is a thin wrapper around the Google Python SDK. The SDK isn't super-intuitive, and knowing how to retrieve and massage data is nice to abstract out.

Content
-------
    :'analytics': API to read Google Sheets spreadsheets.
    :'sheets': API to read Google Sheets spreadsheets.

Examples
----------

    :example1.py: Fetches a snippet of data from a public Google Sheet.
    :example2.py: Fetches data from a Google Analytics set of goals.


References
----------

https://console.developers.google.com/
https://developers.google.com/api-client-library/python/
https://developers.google.com/api-client-library/python/apis/
https://developers.google.com/apis-explorer/#p/analytics/v3/
https://www.google.com/webhp#q=curbal+goal+xx&*
https://developers.google.com/analytics/devguides/reporting/core/dimsmets
https://developers.google.com/analytics/devguides/reporting/core/v3/common-queries

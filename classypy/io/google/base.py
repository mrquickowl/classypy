import json
import os
import os.path as op
import sys
import warnings

from ...compat import md5_hash
from ..core.datasets import Dataset
from ..core.fetchers import Fetcher


def hash_from_kwargs(**kwargs):
    stem = '-'.join(['%s=%s' % (key, val) for key, val in kwargs.items()])
    return md5_hash(stem)


def get_googledocs_credentials(application_name, credential_path, scopes,
                               client_secret_file=None, client_id=None, client_secret=None,
                               credentials_json=None, fail_on_invalid=False, force=False):
    """Gets valid user credentials from storage.

    If nothing has been stored, or if the stored credentials are invalid,
    the OAuth2 flow is completed to obtain the new credentials.

    Returns:
        Credentials, the obtained credential.
    """
    from oauth2client import client, tools
    from oauth2client.file import Storage

    # If modifying these scopes, delete your previously saved credentials
    # at ~/.credentials/classy.org-marketing-classy100.json
    store = Storage(credential_path)
    with warnings.catch_warnings():
        warnings.simplefilter("ignore")
        credentials = store.get()  # Causes warning if store doesn't yet exist.

    if (not credentials or credentials.invalid) and fail_on_invalid:
        raise ValueError("Invalid credentials")
    if not credentials or credentials.invalid or force:
        cp_dir = op.dirname(credential_path)
        if cp_dir and not op.exists(cp_dir):
            os.makedirs(cp_dir)

        if credentials_json:
            # Recursion base case: dump the file, reload
            credentials_obj = json.loads(credentials_json)
            with open(credential_path, 'w') as fp:
                json.dump(credentials_obj, fp)
            return get_googledocs_credentials(
                application_name=application_name, credential_path=credential_path,
                scopes=scopes, fail_on_invalid=True)

        elif client_secret_file:
            flow = client.flow_from_clientsecrets(scopes)
        elif client_id and client_secret:
            flow = client.OAuth2WebServerFlow(client_id, client_secret, scopes)
        else:
            raise NotImplementedError(
                ("No valid credentials at {credential_path}; "
                 "Must specify credentials_json or client_id, client_secret").format(
                    credential_path=credential_path))
        flow.user_agent = application_name
        print('Storing credentials to ' + credential_path)
        # HACK: to work around seeming oauth2 flow bug,
        # where it reads sys.argv... weird.
        old_args, sys.argv = sys.argv, sys.argv[:1]
        credentials = tools.run_flow(flow, store)
        sys.argv = old_args
    return credentials


class GoogleFetcher(Fetcher):
    """"""
    dependencies = Dataset.add_dependencies(oauth2client='google-api-python-client')
    scopes = None
    service_name = None
    service_version = None

    def __init__(self, data_dir=None, client_id=None, client_secret=None, credentials_json=None,
                 credential_path=None):
        """"""
        super(GoogleFetcher, self).__init__(data_dir=data_dir)

        application_name = '%s-%s' % (self.service_name, self.service_version)

        if not credential_path:
            home_dir = op.expanduser('~')
            credential_dir = op.join(home_dir, '.credentials')
            if not op.exists(credential_dir):
                os.makedirs(credential_dir)
            credential_path = credential_path or op.join(
                credential_dir, '%s-creds.json' % application_name)

        self.credentials = get_googledocs_credentials(
            application_name=application_name,
            client_id=client_id,
            client_secret=client_secret,
            credentials_json=credentials_json,
            credential_path=credential_path,
            scopes=self.service_scopes,
            force=credentials_json is not None)

        self.service = None

    def get_service(self):
        import httplib2
        if not self.service:
            from apiclient import discovery
            http = self.credentials.authorize(httplib2.Http())
            self.service = discovery.build(
                self.service_name,
                self.service_version,
                http=http)
        return self.service

    def _cache_file_from_kwargs(self, **kwargs):
        plk_file = '%s.pkl' % hash_from_kwargs(**kwargs)
        pkl_path = op.join(self.data_dir, plk_file)
        return pkl_path


class GoogleDataset(Dataset):
    fetcher_class = GoogleFetcher

    def _csv_file_from_kwargs(self, **kwargs):
        csv_file = '%s.csv' % hash_from_kwargs(**kwargs)
        csv_path = op.join(self.data_dir, csv_file)
        return csv_path

    def __init__(self, data_dir=None, client_id=None, client_secret=None, credentials_json=None,
                 credential_path=None, **kwargs):
        super(GoogleDataset, self).__init__(
            data_dir=data_dir,
            client_id=client_id,
            client_secret=client_secret,
            credentials_json=credentials_json,
            credential_path=credential_path)

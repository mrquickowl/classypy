"""
Download IRS 990 returns for three organizations
in 2015 and 2016.
"""
from classypy.devops.secrets import find_secrets
from classypy.io.google import GoogleSheetsDataset

# Read env file into environ, if available
secrets = find_secrets()

dataset = GoogleSheetsDataset(
    client_id=secrets['GOOGLE_API_CLIENT_ID'],
    client_secret=secrets['GOOGLE_API_CLIENT_SECRET'],)
df = dataset.fetch(
    spreadsheet_id='1vh4-Vv-B5KFmHCozHtwAMrk59OZCvy85K7-krpF__co',
    sheet_name='For Design (FINAL)',
    sheet_range='B5:AB105',
    force=True)

print("Downloaded %d results" % len(df))
print("columns: %s" % ", ".join(sorted(df.columns)))
print("Example data: unique values of column 'charity_id': %s" % str(df['charity_id'].unique()))

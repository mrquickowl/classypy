import os.path as op
import pickle

import pandas as pd

from .base import GoogleDataset, GoogleFetcher


class GoogleSheetsFetcher(GoogleFetcher):

    # discovery_url = ('https://sheets.googleapis.com/$discovery/rest?'
    #                  'version=v4')
    service_name = 'sheets'
    service_version = 'v4'
    service_scopes = 'https://www.googleapis.com/auth/spreadsheets.readonly'

    def _get_sheet_values(self, spreadsheet_id, sheet_name, sheet_range):
        range_name = '%s!%s' % (sheet_name, sheet_range)
        result = self.get_service().spreadsheets().values().get(
            spreadsheetId=spreadsheet_id, range=range_name).execute()
        values = result.get('values', [])
        return values

    def fetch(self, spreadsheet_id, sheet_name, sheet_range,
              force=False, verbose=1):
        """
        Parameters
        ----------
        spreadsheet_id : string
            Google Docs spreadsheet IDs. e.g: 261630239
        sheet_name : string
            Sheet names, one per spreadsheet above. e.g: My Sheet
        sheet_range : String
           Cell ranges in the given sheet. e.g: A1:B3
        verbose : bool
            Level to log progress to screen; see HttpApiDataset.
        force : bool
            Forcing flag

        Returns
        -------
        values : dictionary of raw API responses.
        """
        cache_file = self._cache_file_from_kwargs(
            spreadsheet_id=spreadsheet_id, sheet_name=sheet_name,
            sheet_values=sheet_range)

        if not force and op.exists(cache_file):
            # Use cached version.
            with open(cache_file, 'rb') as fp:
                values = pickle.load(fp)
        else:
            values = self._get_sheet_values(
                spreadsheet_id=spreadsheet_id, sheet_name=sheet_name, sheet_range=sheet_range)

        return values


class GoogleSheetsDataset(GoogleDataset):
    fetcher_class = GoogleSheetsFetcher

    def _values_to_df(self, values):
        columns = values[0]
        rows = []
        for row in values[1:]:
            # Normalize row lengths
            rows.append(row + ([''] * (len(columns) - len(row))))
            assert len(rows[-1]) == len(columns)
        df = pd.DataFrame.from_records(rows, columns=columns)
        return df

    def fetch(self, spreadsheet_id, sheet_name, sheet_range,
              force=False, verbose=1):
        """
        Parameters
        ----------
        spreadsheet_id : iterable
            List of Google Docs spreadsheet IDs. e.g: 261630239
        sheet_name : iterable
            List of sheet names, one per spreadsheet above. e.g: My Sheet
        sheet_range : iterable
            List of cell ranges in the given sheet. e.g: A1:B3
        force : bool
            Forcing flag
        verbose : bool
            Level to log progress to screen; see HttpApiDataset.

        Returns
        -------
        dataframe : Pandas dataframe representation of the requested sheet / range.
        """
        csv_file = self._csv_file_from_kwargs(
            spreadsheet_id=spreadsheet_id, sheet_name=sheet_name,
            sheet_values=sheet_range)

        if not force and op.exists(csv_file):
            # Use cached version.
            df = pd.read_csv(csv_file)

        else:
            values = self.fetcher.fetch(
                spreadsheet_id=spreadsheet_id, sheet_name=sheet_name, sheet_range=sheet_range)
            df = self._values_to_df(values)
            df.to_csv(csv_file, encoding='utf-8')

        return df

"""
"""
import os.path as op

import pandas as pd

from ..core.datasets import HttpDataset
from ..core.fetchers import HttpFetcher


class HuduserMapFetcher(HttpFetcher):
    """
    Get metro area information from huduser.gov; convert to CSV
    """
    XLS_2016_URL = 'https://www.huduser.gov/portal/datasets/usps/ZIP_CBSA_122016.xlsx'

    # Pandas dependency for reading XLS
    dependencies = HttpFetcher.add_dependencies('xlrd')

    def fetch(self, verbose=1, force=False):
        """Trigger the CSV generation, scrape the CSV link, then download the csv."""
        xls_file = super(HuduserMapFetcher, self).fetch(
            (self.XLS_2016_URL,),
            verbose=verbose, force=force)[0]

        # Convert to dataframe, save as csv
        df = pd.read_excel(xls_file)
        csv_file = op.join(self.data_dir, op.basename(xls_file.replace('.xls', '.csv')))
        df.to_csv(csv_file)

        return csv_file


class HuduserMapDataset(HttpDataset):
    fetcher_class = HuduserMapFetcher

    def fetch(self, force=False, verbose=1):
        """
        Download zip => cbsa mapping file.
        """
        return pd.read_csv(self.fetcher.fetch(force=force, verbose=verbose))

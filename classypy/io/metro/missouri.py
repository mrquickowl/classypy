"""
"""
import re

import pandas as pd

from ..core._utils.parsing import BeautifulSoupParser
from ..core.datasets import HttpDataset
from ..core.fetchers import HttpFetcher


class MissouriMapFetcher(HttpFetcher, BeautifulSoupParser):
    """
    Get metro area information from mcdc.missouri.edu. This is a thin wrapper
    around their web interface.

    The web interface allows a GET command to generate the data.
    Then we have to scrape the resulting webpage for the CSV download.
    """
    dependencies = HttpFetcher.add_dependencies(*BeautifulSoupParser.dependencies)

    STATE_TO_PARAM_MAP = {
        'AL': 1, 'AK': 2, 'AZ': 4,
        'AR': 5, 'CA': 6, 'CO': 8, 'CT': 9,
        'DE': 10, 'DC': 11, 'FL': 12, 'GA': 13,
        'HI': 15, 'ID': 16, 'IL': 17, 'IN': 18, 'IA': 19,
        'KS': 20, 'KY': 21, 'LA': 22, 'ME': 23, 'MD': 24,
        'MA': 25, 'MI': 26, 'MN': 27, 'MS': 28, 'MO': 29,
        'MT': 30, 'NE': 31, 'NV': 32, 'NH': 33, 'NJ': 34,
        'NM': 35, 'NY': 36, 'NC': 37, 'ND': 38, 'OH': 39,
        'OK': 40, 'OR': 41, 'PA': 42, 'RI': 44,
        'SC': 45, 'SD': 46, 'TN': 47, 'TX': 48, 'UT': 49,
        'VT': 50, 'VA': 51, 'WA': 53, 'WV': 54,
        'WI': 55, 'WY': 56, }
    TEMPLATE_URL = ("http://mcdc.missouri.edu/cgi-bin/broker?_PROGRAM=websas.geocorr14.sas"
                    "&_SERVICE=bigtime&site=OSEDA%2FMCDC%2FUniv.+of+Missouri&g1_=zcta5&g2_=cbsa"
                    "&wtvar=pop10&csvout=1&listout=1&lstfmt=html&namoptf=b&namoptr=b&title=+&counties="
                    "&metros=&places=&distance=&y0lat=&x0long=&locname=&nrings="
                    "&r1=&r2=&r3=&r4=&r5=&r6=&r7=&r8=&r9=&r10=&lathi=&latlo=&longhi=&longlo=&_DEBUG=0"
                    "&state={state}{state_code}")
    CSV_REGEX = r'^.*/tmpscratch/.*geocorr14.csv$'

    def fetch(self, states=None, verbose=1, force=False):
        """Trigger the CSV generation, scrape the CSV link, then download the csv."""
        states = states or list(self.STATE_TO_PARAM_MAP.keys())
        csv_files = []
        for state in states:
            state = state.upper()

            # Fetch the raw files.
            files = [(
                '%s.html' % state,
                self.TEMPLATE_URL.format(state=state, state_code=self.STATE_TO_PARAM_MAP[state]),
                {'uncompress': True})]
            html_file = super(MissouriMapFetcher, self).fetch(files, verbose=verbose, force=force)[0]

            # Parse with beautifulsoup
            csv_file = None
            with open(html_file, 'r') as fp:
                html_content = fp.read().decode('ascii', 'ignore')
            for link in self.get_bs_html_obj(html_content, tags=['a']):
                if not link.has_attr('href'):
                    continue
                if not re.match(self.CSV_REGEX, link['href']):
                    continue
                files = [('%s.csv' % state, 'http://mcdc.missouri.edu/' + link['href'], {'uncompress': True})]
                csv_file = super(MissouriMapFetcher, self).fetch(files, verbose=verbose, force=force)[0]
                break
            csv_files.append(csv_file)
        return csv_files


class MissouriMapDataset(HttpDataset):
    fetcher_class = MissouriMapFetcher

    def _read_csv(self, csv_file, use_friendly_names=True):
        """Ignore bad lines, strip extra header row."""
        df = pd.read_csv(csv_file, error_bad_lines=False)
        if use_friendly_names:
            df.columns = df.iloc[0]
        return df.iloc[1:]

    def fetch(self, states=None, force=False, verbose=1, use_friendly_names=True):
        """
        Download zcta => cbsa mapping file for each requested state.
        """
        return [self._read_csv(csv_file, use_friendly_names=use_friendly_names) if csv_file else None
                for csv_file in self.fetcher.fetch(
                    states=states, force=force, verbose=verbose)]

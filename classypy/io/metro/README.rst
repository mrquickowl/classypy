Metro areas dataset

Notes
-----


Contents
--------

    :HuduserMapDataset: Mappings from zip code to core-based statistical areas.
    :MissouriMapDataset: Mappings from zip code tabulated areas (ZCTAs) to core-based statistical areas.

Examples
--------

    :example1.py: Fetches the zcta => cbsa mapping files.
    :example1.py: Fetches the zip code => cbsa mapping file.


References
----------

How to relate across states:
http://mcdc.missouri.edu/websas/geocorr14.html

Or use ZCTA:
https://en.wikipedia.org/wiki/ZIP_Code_Tabulation_Area


Zip to CBSA:
https://www.huduser.gov/portal/datasets/usps/ZIP_CBSA_122016.xlsx

ZCTA to CBSA:
https://www.huduser.gov/portal/datasets/usps_crosswalk.html
https://www.census.gov/geo/maps-data/data/zcta_rel_download.html
https://www2.census.gov/geo/docs/maps-data/data/rel/zcta_cbsa_rel_10.txt
http://www2.census.gov/geo/pdfs/maps-data/data/rel/explanation_zcta_cbsa_rel_10.pdf

Zip Code to ZCTA:
https://www.udsmapper.org/zcta-crosswalk.cfm
https://www.udsmapper.org/docs/zip_to_zcta_2016.xlsx
http://pandas.pydata.org/pandas-docs/stable/generated/pandas.read_excel.html
Reference:
http://mcdc2.missouri.edu/webrepts/geography/ZIP.resources.html

ZCTA to shape (could be used with address location):
https://catalog.data.gov/dataset/census-5-digit-zip-code-tabulation-area-zcta5-national
CBSA to shape (could be used with address location):
https://www.census.gov/geo/maps-data/data/cbf/cbf_msa.html
https://www.gislounge.com/how-to-geocode-addresses-using-qgis/

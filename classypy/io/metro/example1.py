"""
Download ZCTA => CBSA mapping files.
"""
from classypy.io.metro import MissouriMapDataset

dataset = MissouriMapDataset()
dfs = dataset.fetch(states=['CA', 'WA'], use_friendly_names=True)
print("{n_states} state returned.".format(n_states=len(dfs)))
print("Columns: {columns}".format(columns=", ".join(dfs[0].columns)))
print("Data length: {n_columns}".format(n_columns=len(dfs[0])))

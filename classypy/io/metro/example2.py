"""
Download zip code => CBSA for 2016
"""
from classypy.io.metro import HuduserMapDataset

dataset = HuduserMapDataset()
df = dataset.fetch()
print("columns: %s" % ", ".join(sorted(df.columns)))
print("Data length: %d" % len(df))
print("Sample row: %s" % str(df.iloc[0]))

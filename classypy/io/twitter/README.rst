Datanyze

Notes
-----
This is a thin wrapper around the Twitter `tweepy` SDK.

Content
-------
    :'users': Basic data about twitter user accounts.


Examples
----------

    :example1.py: Downloads data for the `@classy` twitter account, and shows the available keys.


References
----------

https://dev.twitter.com/rest/reference/get/followers/ids
http://docs.tweepy.org/en/v3.5.0/

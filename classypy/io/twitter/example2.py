"""
TwitterUserPostsDataset Example.
"""
from classypy.devops import find_secrets
from classypy.io.twitter import TwitterUserPostsDataset

secrets = find_secrets()
dataset = TwitterUserPostsDataset(
    consumer_key=secrets['TWITTER_CONSUMER_KEY'],
    consumer_secret=secrets['TWITTER_CONSUMER_SECRET'],
    access_token=secrets['TWITTER_ACCESS_TOKEN'],
    access_token_secret=secrets['TWITTER_ACCESS_TOKEN_SECRET'])

df = dataset.fetch(screen_names=['classy', 'mrquickowl'], wait_on_rate_limit=True,
                   max_posts_per_user=100)
print("Shape of resulting dataframe:")
print(df.shape)
print("Returned dataframe columns:")
print(sorted(df.columns.values))
print("Sample row:")
print(df.iloc[0])

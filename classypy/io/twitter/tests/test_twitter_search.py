from classypy.io.twitter import TwitterSearchDataset


def test_twitter_search_dataset():
    dataset = TwitterSearchDataset(
        consumer_key='a',
        consumer_secret='b',
        access_token='c',
        access_token_secret='d')
    df = dataset.fetch(queries=[])
    assert 'query' in df.columns

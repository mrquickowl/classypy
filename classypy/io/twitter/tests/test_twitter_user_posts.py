from classypy.io.twitter import TwitterUserPostsDataset


def test_twitter_user_posts_dataset():
    dataset = TwitterUserPostsDataset(
        consumer_key='a',
        consumer_secret='b',
        access_token='c',
        access_token_secret='d')
    df = dataset.fetch(screen_names=[])
    assert 'screen_name' in df.columns
    assert 'original_screen_name' in df.columns

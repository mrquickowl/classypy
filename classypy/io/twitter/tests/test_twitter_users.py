from classypy.io.twitter import TwitterUsersDataset


def test_twitter_users_dataset():
    dataset = TwitterUsersDataset(
        consumer_key='a',
        consumer_secret='b',
        access_token='c',
        access_token_secret='d')
    df = dataset.fetch(screen_names=[])
    assert 'original_screen_name' in df.columns

# Author: Ben Cipollini
# License: simplified BSD

import logging
import os
from collections import OrderedDict, defaultdict
from functools import partial

import numpy as np
import pandas as pd

from classypy.devops import logger
from classypy.io.core.datasets import Dataset
from classypy.io.core.fetchers import HttpApiFetcher
from classypy.util.dates import convert_datetime_columns
from classypy.util.etl import to_ascii


class TweepyWarningFilter(logging.Filter):
    def filter(self, record):
        """Prepend warning log messages with `Warning:`"""
        if record.levelno == logging.WARNING:
            record.msg = "Warning: {message}".format(message=record.getMessage())
        return True


class TwitterApiFetcher(HttpApiFetcher):
    """Child class of HttpApiFetcher for Twitter API."""
    dependencies = HttpApiFetcher.add_dependencies(tweepy='git+git://github.com/tweepy/tweepy')

    # Specifics for twitter API
    _ERROR_REQUEST_LIMIT_EXCEEDED = 88
    _ERROR_TOO_MANY_REQUESTS = 429
    _NUM_RETRIES = 2

    TWEEPY_LOGGERS = (logging.getLogger('tweepy.binder'), logging.getLogger('tweepy.cache'))
    TWEEPY_WARNING_FILTER = TweepyWarningFilter()
    DEFAULT_HANDLER = logging.StreamHandler()

    @classmethod
    def _setup_tweepy_loggers(cls, handler, use_tweepy_warning_filter):
        """Setup the loggers used in the Tweepy module.

        First register handlers, so that the loggers work.
        If use_tweepy_warning_filter is True, `Warning:` is prepended
        to log warnings so that they stand out.
        """
        for tw_logger in cls.TWEEPY_LOGGERS:
            tw_logger.addHandler(handler)
            if use_tweepy_warning_filter:
                logger.addFilter(cls.TWEEPY_WARNING_FILTER)

    def __init__(self, data_dir=None, consumer_key=None, consumer_secret=None,
                 access_token=None, access_token_secret=None, tweepy_handler=DEFAULT_HANDLER,
                 use_tweepy_warning_filter=True):
        super(TwitterApiFetcher, self).__init__(data_dir=data_dir)

        self.consumer_key = consumer_key or os.environ.get("TWITTER_CONSUMER_KEY")
        self.consumer_secret = consumer_secret or os.environ.get("TWITTER_CONSUMER_SECRET")
        self.access_token = access_token or os.environ.get("TWITTER_ACCESS_TOKEN")
        self.access_token_secret = access_token_secret or os.environ.get("TWITTER_ACCESS_TOKEN_SECRET")
        if (self.consumer_key is None or self.consumer_secret is None
                or self.access_token is None or self.access_token_secret is None):
            raise ValueError("Must define TWITTER_CONSUMER_KEY, TWITTER_CONSUMER_SECRET,"
                             "TWITTER_ACCESS_TOKEN, TWITTER_ACCESS_TOKEN_SECRET environment variables, "
                             "or pass consumer_key, consumer_secret, access_token "
                             "and access_token_secret arguments.")
        self.api = None
        self._setup_tweepy_loggers(handler=tweepy_handler, use_tweepy_warning_filter=use_tweepy_warning_filter)

    def get_api(self, wait_on_rate_limit=False):
        """
        Method to establish connection with Twitter API, through `tweepy` library.

        Parameters
        -----------
        wait_on_rate_limit : Boolean (default: False)
            Whether or not to tweepy will automatically pause upon hitting rate-limits.

        Return
        -------
            tweepy.api object : Object with established connection to Twitter API.
        """
        import tweepy

        # specify error codes to catch
        if not self.api:
            auth = tweepy.OAuthHandler(self.consumer_key, self.consumer_secret)
            auth.set_access_token(self.access_token, self.access_token_secret)
            self.api = tweepy.API(auth, wait_on_rate_limit=wait_on_rate_limit,
                                  wait_on_rate_limit_notify=logger.verbosity() > 0,
                                  retry_errors=set([self._ERROR_REQUEST_LIMIT_EXCEEDED, self._ERROR_TOO_MANY_REQUESTS]),
                                  retry_count=self._NUM_RETRIES
                                  )
        return self.api

    def fetch_users(self, screen_names, force=False):
        """
        Method to query Twitter API for user-information related to the given Twitter handles, `screen_names`.

        Parameters
        ----------
        screen_name : iterable
            Each element should be a twitter handle.
        force : boolean
            When False (default), previously downloaded data are reused (not implemented).
            When True, reports are downloaded at runtime.

        Returns
        --------
            list : List of `tweepy.models.User` objects, each of which has user-data from Twitter API.
        """
        def fetch_fn(screen_names):
            """Closure to fetch data."""
            if screen_names is None or len(screen_names) == 0:
                # API is not well-behaved on empty
                return screen_names
            screen_names = [to_ascii(sn) for sn in screen_names]

            user_data = OrderedDict()
            user_data = self.get_api().lookup_users(screen_names=screen_names)

            # Sort according to original order
            sorted_user_data = []
            user_screen_names = [u and u.screen_name.lower() for u in user_data]
            for screen_name in screen_names:
                try:
                    idx = user_screen_names.index(screen_name.lower())
                    sorted_user_data.append(user_data[idx])
                except ValueError:  # not found in list
                    sorted_user_data.append(None)
            if np.any([ud is None for ud in sorted_user_data]):
                logger.warning("Failed to fetch twitter user ids: %s" % ', '.join(
                    [sn for sn, ud in zip(screen_names, sorted_user_data)
                     if ud is None]))

            return sorted_user_data
        return self.fetch_in_parts(
            ids=screen_names, force=force, fetch_fn=fetch_fn, max_ids_per_request=100).values()

    def fetch_user_posts(self, screen_name, max_posts=500, earliest_date=None,
                         wait_on_rate_limit=True, force=False):
        """
        Method to query Twitter API for posts (tweets).
        Fetching starts from the the most recent tweet and works backwards
        until `max_posts` tweets are fetched or a tweet from before `earliest_date`
        is found.

        Parameters
        ----------
        screen_name : string
            The twitter screen_name to extract post information from.
        max_posts : integer
            Maximum number of posts to return (the Twitter API requires this to be <= 3200).
        earliest_date : string
            Date in format "yyyy-mm-dd" specifying the earliest date to fetch tweets for.
            When None (default), will return up to max_posts tweets.
        wait_on_rate_limit : Boolean
            Whether or not to automatically wait for rate limits to replenish.
        force : boolean
            When False (default), previously downloaded data are reused (not implemented).
            When True, reports are downloaded at runtime.

        Returns
        --------
            tweepy.models.Status object : Contains queried Twitter post information.
        """
        # initialize api connection
        import tweepy

        if max_posts > 3200:
            raise ValueError("The Twitter API can only return up to 3,200 of a user's most recent Tweets.")
        earliest_date = earliest_date and pd.to_datetime(earliest_date) or pd.to_datetime('2006')
        api = self.get_api(wait_on_rate_limit=wait_on_rate_limit)
        # collect tweet data for `screen_name`
        tweets = []
        for tweet in tweepy.Cursor(api.user_timeline,
                                   screen_name=screen_name,
                                   ).items(max_posts):
            if tweet.created_at < earliest_date:
                break
            tweets.append(tweet)
        return tweets

    def fetch_search_posts(self, query, max_posts=500,
                           wait_on_rate_limit=True):
        """
        Method to query Twitter Search API.
        The Search API only returns tweets from the last 6-9 days.

        Parameters
        ----------
        query : string
            The query to search with.
            Information on building queries can be found at https://dev.twitter.com/rest/public/search
        max_posts : integer
            Maximum number of posts to return.
        wait_on_rate_limit : Boolean
            Whether or not to automatically wait for rate limits to replenish.

        Returns
        --------
            tweepy.models.Status object : Contains queried Twitter post information.
        """
        import tweepy

        api = self.get_api(wait_on_rate_limit=wait_on_rate_limit)
        tweets = []
        for tweet in tweepy.Cursor(api.search, q=query).items(max_posts):
            tweets.append(tweet)

        return tweets


class TwitterUsersDataset(Dataset):
    """Child class of `Dataset` that provides access to Twitter user information."""
    fetcher_class = TwitterApiFetcher

    USER_KEYS = (
        'contributors_enabled', 'created_at', 'default_profile',
        'default_profile_image', 'description', 'favourites_count',
        'followers_count', 'friends_count', 'geo_enabled', 'has_extended_profile',
        'id', 'is_translation_enabled', 'is_translator', 'lang', 'listed_count',
        'location', 'name', 'notifications', 'profile_background_color',
        'profile_background_image_url', 'profile_background_image_url_https',
        'profile_background_tile', 'profile_banner_url', 'profile_image_url',
        'profile_image_url_https', 'profile_link_color', 'profile_location',
        'profile_sidebar_border_color', 'profile_sidebar_fill_color', 'profile_text_color',
        'profile_use_background_image', 'protected', 'screen_name',
        'status', 'statuses_count', 'time_zone', 'translator_type', 'url', 'verified'
    )

    def fetch(self, screen_names, force=False):
        """
        Fetches user-information for given Twitter handes, `screen_names`.

        Parameters
        ----------
        screen_names : iterable
            Each element should be a twitter handle.
        force : boolean
            When False (default), previously downloaded data are reused (not implemented).
            When True, reports are downloaded at runtime.

        Returns
        -------
        pd.DataFrame : DataFrame of user-information.
        """
        import tweepy

        user_objects = self.fetcher.fetch_users(screen_names=screen_names, force=force)

        def user2dict(user, ui):
            if user.__class__ == tweepy.models.User:
                user_data = vars(user)
            else:
                user_data = defaultdict(lambda: None, screen_name=screen_names[ui])
            user_data['original_screen_name'] = screen_names[ui]
            return user_data

        user_dicts = [user2dict(user=udata, ui=ui) for ui, udata in enumerate(user_objects)]
        users_df = pd.DataFrame(data=user_dicts)

        # Fill missing keys
        # NOTE: parenthesis are VERY necessary here.
        missing_keys = (set(self.USER_KEYS) | set(['original_screen_name'])) - set(users_df.columns.tolist())
        for key in missing_keys:
            users_df[key] = np.nan

        return users_df


class _TwitterPostsDataset(Dataset):
    fetcher_class = TwitterApiFetcher

    DEFAULT_POST_KEYS = (
        'created_at', 'entities', 'favorite_count', 'id_str',
        'in_reply_to_screen_name', 'in_reply_to_status_id_str', 'in_reply_to_user_id_str',
        'lang', 'quoted_status_id_str', 'retweet_count', 'source', 'text',
    )

    # These keys are not always returned by the Twitter API
    # See https://dev.twitter.com/overview/api/tweets for explanation
    POSSIBLY_MISSING_POST_KEYS = ('possibly_sensitive', 'quoted_status_id', 'quoted_status_id_str', 'quoted_status')
    DATETIME_COLUMNS = ('created_at',)

    @convert_datetime_columns(column_names=DATETIME_COLUMNS)
    def fetch(self, fetch_fn, fetch_items, post_keys):
        """Fetch Twitter objects via the tweepy API."""
        import tweepy

        post_dfs = []
        for fetch_item in fetch_items:
            logger.info('Currently collecting tweets for: {fetch_item}'.format(fetch_item=fetch_item))

            try:
                twitter_posts = fetch_fn(fetch_item)
            except tweepy.TweepError as e:
                twitter_posts = None
                logger.warning("Received the following error for {fetch_item}: {e}".format(
                    fetch_item=fetch_item, e=e))

            if twitter_posts:
                # extract dict of values from each tweepy.models.Status object
                posts_df = pd.DataFrame(vars(twitter_posts[i]) for i in range(len(twitter_posts)))
                # extract screen_name from tweepy.models.User object
                posts_df['screen_name'] = posts_df['author'].map(lambda author: author.screen_name)
                # coerce to pandas DataFrame with specified keys
                good_keys = list(post_keys) + ['screen_name']
                # Let's insert possibly missing keys if they're missing to avoid a KeyError
                for key in self.POSSIBLY_MISSING_POST_KEYS:
                    if key in good_keys and key not in posts_df.columns:
                        posts_df[key] = np.nan
                posts_df = posts_df[good_keys]

                # Add a column that shows what this was fetched for
                posts_df['fetch_item'] = fetch_item
                post_dfs.append(posts_df)

        return pd.concat(post_dfs, sort=True) if post_dfs else pd.DataFrame(
            columns=list(post_keys) + ['screen_name', 'fetch_item'])


class TwitterUserPostsDataset(_TwitterPostsDataset):
    """Access to Twitter posts for specific users."""
    def fetch(self, screen_names, post_keys=_TwitterPostsDataset.DEFAULT_POST_KEYS,
              max_posts_per_user=500, earliest_date=None,
              wait_on_rate_limit=True):
        """
        Fetches Twitter post information associated with the given Twitter handles.

        Parameters
        ----------
        screen_names : iterable
            A list of different twitter handles.
        post_keys : iterable
            A list of tweepy.models.Status keys to extract.
        max_posts_per_user : integer
            Max number of posts to return (the Twitter API requires this to be <= 3200).
        earliest_date : string
            Date in format "yyyy-mm-dd" specifying the earliest date to fetch tweets for.
            When None (default), will return up to max_posts_per_user tweets.
        wait_on_rate_limit : Boolean
            Whether or not to automatically wait for rate limits to replenish.

        Returns
        --------
            pd.DataFrame : DataFrame associated with post information for the given Twitter handles,`screen_name`,
                with columns `post_keys`, within the specified date range.
        """
        fetch_fn = partial(self.fetcher.fetch_user_posts, max_posts=max_posts_per_user, earliest_date=earliest_date,
                           wait_on_rate_limit=wait_on_rate_limit)
        df = super(TwitterUserPostsDataset, self).fetch(
            fetch_fn=fetch_fn, fetch_items=screen_names,
            post_keys=post_keys)

        # Also has a screen name
        return df.rename(columns={'fetch_item': 'original_screen_name'})


class TwitterSearchDataset(_TwitterPostsDataset):
    """Access to Twitter posts related to given search queries"""
    def fetch(self, queries, post_keys=_TwitterPostsDataset.DEFAULT_POST_KEYS,
              max_posts_per_query=500, wait_on_rate_limit=True):
        """
        Fetches Twitter post information associated with the given search queries.

        Parameters
        ----------
        queries : iterable
            The queries to search with.
            Information on building queries can be found at https://dev.twitter.com/rest/public/search
        post_keys : iterable
            A list of tweepy.models.Status keys to extract.
        max_posts_per_query : integer
            Max number of posts to return per query.
        wait_on_rate_limit : Boolean
            Whether or not to automatically wait for rate limits to replenish.

        Returns
        --------
            pd.DataFrame : DataFrame associated with post information for the given search queries, `query`,
                with columns `post_keys`.
        """
        fetch_fn = partial(self.fetcher.fetch_search_posts, max_posts=max_posts_per_query,
                           wait_on_rate_limit=wait_on_rate_limit)
        df = super(TwitterSearchDataset, self).fetch(
            fetch_fn=fetch_fn, fetch_items=queries,
            post_keys=post_keys)
        return df.rename(columns={'fetch_item': 'query'})

"""
Simple report download & info.
"""
from classypy.devops import find_secrets
from classypy.io.twitter import TwitterUsersDataset

secrets = find_secrets()
dataset = TwitterUsersDataset(
    consumer_key=secrets['TWITTER_CONSUMER_KEY'],
    consumer_secret=secrets['TWITTER_CONSUMER_SECRET'],
    access_token=secrets['TWITTER_ACCESS_TOKEN'],
    access_token_secret=secrets['TWITTER_ACCESS_TOKEN_SECRET'])


df = dataset.fetch(screen_names=['classy'])
print("columns:")
print(df.columns)
print("friends count for user:")
print(df['followers_count'])

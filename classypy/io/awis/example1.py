"""
Fetch web traffic data for a few websites.
"""
from classypy.devops import find_secrets
from classypy.io.awis import AlexaWebInformationServiceUrlInfoDataset

secrets = find_secrets()
dataset = AlexaWebInformationServiceUrlInfoDataset(
    # If these secrets are not defined, will use AWS credentials
    aws_access_key=secrets.get('AWS_ACCESS_KEY'),
    aws_secret_access_key=secrets.get('AWS_SECRET_ACCESS_KEY'))

urls = (
    'campkesem.org',
    'classy.org',
    'invisiblechildren.com',
    'teamrubiconusa.org',
)

df = dataset.fetch(urls=urls)

print("All columns: {columns}".format(columns=list(df.columns)))
print(df)

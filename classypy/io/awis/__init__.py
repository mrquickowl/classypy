from xml.parsers.expat import ExpatError

import pandas as pd

from classypy.compat import _urllib
from classypy.devops import logger
from classypy.io.core.datasets import Dataset
from classypy.io.core.fetchers import RequestsFetcher
from classypy.util.etl import flatten_dict


class AlexaWebInformationServiceFetcher(RequestsFetcher):
    """Fetcher class that leverages Boto to authenticate requests with AWIS."""

    # Can't find any documentation around supported regions and while testing we found that other regions do not work
    # All the AWIS documentation uses us-west-1, so we'll just follow their lead
    AWS_REGION = 'us-west-1'

    def __init__(self, data_dir=None, aws_access_key=None, aws_secret_access_key=None):
        """Initialize AlexaWebInformationServiceFetcher.

        Parameters
        ----------
        data_dir: string
            Path of the data directory. Used to force data storage in a specified location.
        """
        from aws_requests_auth.boto_utils import AWSRequestsAuth, BotoAWSRequestsAuth

        kwargs = dict(
            aws_host="awis.{region}.amazonaws.com".format(region=self.AWS_REGION),
            aws_region=self.AWS_REGION,
            aws_service='awis')

        if aws_access_key and aws_secret_access_key:
            logger.debug("Using AWSRequestsAuth with explicit auth...")
            auth = AWSRequestsAuth(
                aws_access_key=aws_access_key,
                aws_secret_access_key=aws_secret_access_key,
                **kwargs)
        else:
            logger.debug("Using BotoAWSRequestsAuth to gather auth...")
            auth = BotoAWSRequestsAuth(**kwargs)

        super(AlexaWebInformationServiceFetcher, self).__init__(data_dir=data_dir, auth=auth)


class AlexaWebInformationServiceUrlInfoDataset(Dataset):
    """Returns data from the UrlInfo action of AWIS.
    https://docs.aws.amazon.com/AlexaWebInfoService/latest/ApiReference_UrlInfoAction.html
    """
    fetcher_class = AlexaWebInformationServiceFetcher
    dependencies = Dataset.add_dependencies(aws_requests_auth='aws-requests-auth', boto3='boto3', xmltodict='xmltodict')

    # All response groups for the UrlInfo action
    # https://docs.aws.amazon.com/AlexaWebInfoService/latest/ApiReference_UrlInfoAction.html
    URL_INFO_RESPONSE_GROUPS = (
        'AdultContent',
        'Categories',
        'Language',
        'LinksInCount',
        'OwnedDomains',
        'Rank',
        'RankByCountry',
        'SiteData',
        'Speed',
        'UsageStats',
    )

    @staticmethod
    def _clean_rank_by_country_data(data):
        """Clean the TrafficData.RankByCountry.Country data to make it more usable.

        Parameters
        ----------
        data : dict
            Dictionary containing the key 'TrafficData.RankByCountry.Country'

        Side-effects
        ------------
        Removes TrafficData.RankByCountry.Country key from data, and adds keys for individual countries.
        """
        country_data_list = data.get('TrafficData.RankByCountry.Country')
        if country_data_list is None:
            return

        for country in country_data_list:
            country_key_base = "TrafficData.RankByCountry.{code}".format(code=country.pop('@Code'))
            country_data = flatten_dict(country, keep_dict_props=False)
            for key, val in country_data.items():
                data["{base}.{key}".format(base=country_key_base, key=key)] = val

        del data['TrafficData.RankByCountry.Country']

    @staticmethod
    def _clean_usage_statistics_data(data):
        """Clean the TrafficData.UsageStatistics.UsageStatistic data to make it more usable.

        Parameters
        ----------
        data : dict
            Dictionary containing the key 'TrafficData.UsageStatistics.UsageStatistic'

        Side-effects
        ------------
        Removes TrafficData.UsageStatistics.UsageStatistic key from data, and adds keys for individual stats.
        """
        stat_data_list = data.get('TrafficData.UsageStatistics.UsageStatistic')
        if stat_data_list is None:
            return

        for stat in stat_data_list:
            time_range = stat.pop('TimeRange')
            assert len(time_range) == 1, 'Invalid UsageStatistic found with multiple time ranges!'
            time_unit = list(time_range.keys())[0]  # Days, Months, etc.
            time_unit_quantity = time_range[time_unit]  # A number
            stat_key_base = "TrafficData.UsageStatistics.{quantity}{unit}".format(
                quantity=time_unit_quantity, unit=time_unit)
            stat_data = flatten_dict(stat, keep_dict_props=False)
            for key, val in stat_data.items():
                data["{base}.{key}".format(base=stat_key_base, key=key)] = val

        del data['TrafficData.UsageStatistics.UsageStatistic']

    @staticmethod
    def _clean_contributing_subdomains_data(data):
        """Clean the TrafficData.ContributingSubdomains.ContributingSubdomain data to make it more usable.

        Parameters
        ----------
        data : dict
            Dictionary containing the key 'TrafficData.ContributingSubdomains.ContributingSubdomain'

        Side-effects
        ------------
        Removes TrafficData.ContributingSubdomains.ContributingSubdomain key from data,
        and adds keys for individual stats.
        """
        if 'TrafficData.ContributingSubdomains.ContributingSubdomain.DataUrl' in data:
            # There was a single ContributingSubdomain that got flattened, now we need to fit it to the new system

            # Can't change dictionary size as we iterate, so save and apply changes after
            keys_to_delete = []
            keys_to_add = []
            for key in data:
                if key.startswith('TrafficData.ContributingSubdomains.ContributingSubdomain.'):
                    new_key = "TrafficData.ContributingSubdomains.1.{key_end}".format(key_end=key[57:])
                    keys_to_delete.append(key)
                    keys_to_add.append(new_key)

            for delete_me, add_me in zip(keys_to_delete, keys_to_add):
                val = data[delete_me]
                data[add_me] = val
                del data[delete_me]

        if 'TrafficData.ContributingSubdomains.ContributingSubdomain' not in data:
            # No ContributingSubdomains in this data
            return

        contributing_subdomain_data_list = data['TrafficData.ContributingSubdomains.ContributingSubdomain']

        for i, subdomain in enumerate(contributing_subdomain_data_list):
            subdomain_key_base = "TrafficData.ContributingSubdomains.{num}".format(num=i + 1)
            subdomain_data = flatten_dict(subdomain, keep_dict_props=False)
            for key, val in subdomain_data.items():
                data["{base}.{key}".format(base=subdomain_key_base, key=key)] = val

        del data['TrafficData.ContributingSubdomains.ContributingSubdomain']

    @staticmethod
    def _clean_owned_domains_data(data):
        """Clean the ContentData.OwnedDomains.OwnedDomain data to make it more usable.

        Parameters
        ----------
        data : dict
            Dictionary containing the key 'ContentData.OwnedDomains.OwnedDomain'

        Side-effects
        ------------
        Removes ContentData.OwnedDomains.OwnedDomain key from data, and adds keys for individual stats.
        """
        if 'ContentData.OwnedDomains.OwnedDomain.Title' in data:
            # There was a single OwnedDomain that got flattened, now we need to fit it to the new system
            domain = data['ContentData.OwnedDomains.OwnedDomain.Domain']
            title = data['ContentData.OwnedDomains.OwnedDomain.Title']
            assert domain == title, (
                "Found owned domain where Domain ({domain}) and Title ({title}) are different".format(
                    domain=domain, title=title)
            )
            data['ContentData.OwnedDomains'] = domain
            del data['ContentData.OwnedDomains.OwnedDomain.Domain']
            del data['ContentData.OwnedDomains.OwnedDomain.Title']

        if 'ContentData.OwnedDomains.OwnedDomain' not in data:
            # This data is not always present!
            return

        owned_domains_data_list = data['ContentData.OwnedDomains.OwnedDomain']
        domains = []

        for owned_domain in owned_domains_data_list:
            # An OwnedDomain has a Title and Domain, and they appear to always be the same
            unique_values = set(owned_domain.values())
            assert len(unique_values) == 1, (
                "Found owned domain where Title and Domain are different {domain}".format(domain=owned_domain)
            )
            domains.append(unique_values.pop())

        data['ContentData.OwnedDomains'] = ','.join(domains)
        del data['ContentData.OwnedDomains.OwnedDomain']

    @staticmethod
    def _clean_categories_data(data):
        if 'Related.Categories.CategoryData.Title' in data:
            data['Related.Categories.Titles'] = data['Related.Categories.CategoryData.Title']
            data['Related.Categories.AbsolutePaths'] = data['Related.Categories.CategoryData.AbsolutePath']
            del data['Related.Categories.CategoryData.Title']
            del data['Related.Categories.CategoryData.AbsolutePath']

        categories_data_list = data.get('Related.Categories.CategoryData')
        if categories_data_list is None:
            return

        data['Related.Categories.Titles'] = ','.join([d['Title'] for d in categories_data_list])
        data['Related.Categories.AbsolutePaths'] = ','.join([d['AbsolutePath'] for d in categories_data_list])
        del data['Related.Categories.CategoryData']

    def fetch(self, urls, verbose=1):
        """
        Parameters
        ----------
        urls : iterable
            URLs to get web traffic data for.
        verbose: int
            Level of output.
        """
        import xmltodict

        response_group = _urllib.parse.quote(','.join(self.URL_INFO_RESPONSE_GROUPS))
        awis_urls = ["https://awis.amazonaws.com/api?Action=UrlInfo&ResponseGroup={response_group}&Url={url}".format(
            response_group=response_group, url=url) for url in urls]
        responses, exceptions = self.fetcher.fetch(
            urls=awis_urls,
            error_codes_to_retry=[502],  # We see random gateway errors from this service, retry in that case
            retries=3,
            verbose=verbose
        )

        url_data = []
        for response, exception, url in zip(responses, exceptions, urls):
            # Map all namespaces to empty strings, we don't care about them
            namespace_renaming = {
                'http://awis.amazonaws.com/doc/2005-07-11': '',
                'http://awis.amazonaws.com/doc/2005-10-05': '',
            }

            if exception is not None or response is None:
                # Looks like something bad happened while trying to fetch, don't try to parse in this situation
                if verbose > 0:
                    print("Exception or no response while trying to fetch {url}, skipping parsing.".format(url=url))
                url_data.append({'url': url})
                continue

            try:
                response_dict = xmltodict.parse(
                    response.content,
                    process_namespaces=True,
                    namespaces=namespace_renaming
                )
            except ExpatError as ex:
                if verbose > 0:
                    print("Exception while parsing response from {url}: {ex}".format(url=url, ex=ex))
                url_data.append({'url': url})
                continue

            # We only want actual data, need to throw out some metadata that isn't valuable
            if response_dict['UrlInfoResponse'] is None:
                # known special case where we're getting an empty response.
                url_data.append({'url': url})
                continue
            relevant_elements = response_dict['UrlInfoResponse']['Response']['UrlInfoResult']['Alexa']

            # We also don't need details on the request that we made
            del relevant_elements['Request']
            data = flatten_dict(relevant_elements, keep_dict_props=False)
            # Some URLs return lots of Nones :(
            data = {key: val for key, val in data.items() if val is not None}

            # Clean up some attributes that are currently lists
            self._clean_rank_by_country_data(data)
            self._clean_usage_statistics_data(data)
            self._clean_contributing_subdomains_data(data)
            self._clean_owned_domains_data(data)
            self._clean_categories_data(data)

            for key in data:
                assert type(data[key]) != list, "{key} in data was not properly flattened!".format(key=key)

            data['url'] = url
            url_data.append(data)

        return pd.DataFrame(data=url_data)

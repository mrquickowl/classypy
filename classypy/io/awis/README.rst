Notes
-----
The Alexa Web Information Service (AWIS) API makes Alexa's vast repository of information about the web traffic and structure of the web available to developers.

Credentials
---------------------
This dataset retrieves data from AWS. To access the data, you must have an IAM user with Programmatic access to the Alexa Web Information Service. Then you must configure your credentials so that Boto can access them. Make sure to configure the AWS Access Key ID, AWS Secret Access Key, and Default region name.

Full instructions:
1. [Making Requests to the Alexa Web Information Service](https://docs.aws.amazon.com/AlexaWebInfoService/latest/MakingRequestsChapter.html)
2. [Boto Credentials](https://boto3.readthedocs.io/en/latest/guide/configuration.html)

Content
-------
TODO: what goes here??

Examples
--------
    :example1.py: Fetch web traffic data for a few websites.

References
----------
https://aws.amazon.com/awis/

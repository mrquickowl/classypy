Notes
-----
This is a thin wrapper around the Charity Navigator API.

Content
-------
EIN : Employer Identification Number, also known as the Federal Employer Identification Number.

Examples
--------
example1.py : Download a small amount of Charity Navigator data by supplying various valid and invalid EINs.

Obtaining API Application Key and ID
------------------------------------
Find instructions by following the "Charity Navigator API" link under the "References" section.

References
----------
Charity Navigator API : https://charity.3scale.net/docs/data-api/reference
Charity Navigator scoring methodology : https://www.charitynavigator.org/index.cfm?bay=content.view&cpid=1287
Charity Navigator : https://www.charitynavigator.org/

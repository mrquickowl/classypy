"""
Charity Navigator dataset.
"""
import os.path as op

import pandas as pd

from classypy.io.core.datasets import HttpApiDataset
from classypy.text import json_from_disk
from classypy.util.etl import flatten_dict


class CharityNavigatorDataset(HttpApiDataset):
    """
    Dataset of Charity Navigator information.
    """
    API_BASE_URL = "https://api.data.charitynavigator.org/v2/Organizations/"

    def __init__(self, app_key, app_id):
        """
        Initialize CharityNavigatorDataset.
        """
        if app_key is None or app_id is None:
            raise ValueError("Both app_key and app_id must be passed.")

        self.app_key = app_key
        self.app_id = app_id
        super(CharityNavigatorDataset, self).__init__()

    def _generate_api_requests(self, eins):
        """
        Generate HTTP requests for the Charity Navigator API.
        """
        # Ensure eins are in correct format for API
        eins = [ein.replace('-', '') for ein in eins]

        return ["{base}{ein}?app_key={key}&app_id={id}".format(
            base=self.API_BASE_URL, ein=ein, key=self.app_key, id=self.app_id) for ein in eins]

    def fetch(self, eins, force=False, verbose=0):
        """
        Fetch Charity Navigator data.
        """
        response_file_paths = super(CharityNavigatorDataset, self).fetch(
            files=self._generate_api_requests(eins=eins), convert=False, skip_errors=True, clean_on_error=True,
            force=force, verbose=verbose
        )

        dicts = []
        for i, resp_file_path in enumerate(response_file_paths):
            resp_dict = {'original_ein': eins[i]}
            if not op.isfile(resp_file_path):
                # No response returned for ein
                dicts.append(resp_dict)
                continue
            resp_dict.update(json_from_disk(resp_file_path))
            dicts.append(flatten_dict(resp_dict, sep='.', keep_dict_props=False))

        return pd.DataFrame(dicts)

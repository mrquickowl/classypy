"""
Download a small amount of Charity Navigator data.
"""
from classypy.devops import find_secrets
from classypy.io.charity_navigator import CharityNavigatorDataset

print("Checking for Charity Navigator API credentials...")
secrets = find_secrets()

print("Creating CharityNavigatorDataset...")
dataset = CharityNavigatorDataset(
    app_key=secrets['CHARITY_NAVIGATOR_API_KEY'], app_id=secrets['CHARITY_NAVIGATOR_API_ID'])

eins = [
    '41-1601449',  # Org: Feed My Starving Children. Data exists with rating
    '726030391',  # Org: Baton Rouge Area Foundation. Data exists with rating
    '320077563',  # Org: Innocence Project. Data exists *without* rating
    '000000000',  # Bad ein
]
print("Downloading Charity Navigator data...")
df = dataset.fetch(eins=eins, force=False, verbose=0)

# Displaying statistics for downloaded data
print("Data for {num} charities downloaded.".format(num=len(df)))
print("Ratings and scores available for {num} ({perc:.0f}%) charities.".format(
    num=df['currentRating.score'].notnull().sum(),
    perc=100 * df['currentRating.score'].notnull().sum() / len(df)
))
print("Highest score: {high}, lowest score: {low}.".format(
    high=df['currentRating.score'].max(),
    low=df['currentRating.score'].min()
))

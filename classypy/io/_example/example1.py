"""
Simple report download & info.
"""

from classypy.devops import find_secrets
from classypy.io.datanyze import DatanyzeDataset

# Read env file into environ, if available
secrets = find_secrets()
dataset = DatanyzeDataset(
    api_email=secrets['DATANYZE_API_EMAIL'],
    api_secret=secrets["DATANYZE_API_SECRET"])

files = dataset.fetch(report_ids=[754192])
for fil, df in files.items():
    print("Data downloaded to %s" % fil)
    print("Downloaded %d results" % len(df))
    print("columns: %s" % ", ".join(sorted(df.columns)))
    print("Example data: unique values of column 'Public': %s" % str(df['Public'].unique()))

import os
from unittest import TestCase, skipIf

from classypy.io.core._utils.testing import DownloadTestMixin, InstallTestMixin
from classypy.io.datanyze import DatanyzeDataset


@skipIf(os.environ.get('DATANYZE_API_SECRET') is None, "Authentication required.")
class DatanyzeDownloadTest(DownloadTestMixin, TestCase):
    dataset_class = DatanyzeDataset


class DatanyzeHttpDatasetWithDummyCredentials(DatanyzeDataset):
    # For testing http dependency installation
    def __init__(self):
        super(DatanyzeHttpDatasetWithDummyCredentials, self).__init__(
            api_email='dummy', api_secret='dummy')


class DatanyzeInstallHttpTest(InstallTestMixin, TestCase):
    dataset_class = DatanyzeHttpDatasetWithDummyCredentials

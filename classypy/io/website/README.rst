website

Notes
-----
This is a scraper for web data. Specifically, it was designed to work on the urls provided by the irs 990 form (though it will work on almost any url).
It scrapes all of the home-page text content for a given url, and returns a dataframe of the url-name and all of its text content.

Content
-------
    :'__init__.py': Implements the HttpApiFetcher and Dataset classes. This is all you need.

Examples
----------

    :example1.py: Fetches the home-page content for three urls.


References
----------
https://www.crummy.com/software/BeautifulSoup/bs4/doc/

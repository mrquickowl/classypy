import warnings

import pandas as pd
import six

from classypy.io.core._utils.parsing import BeautifulSoupParser
from classypy.io.core.datasets import Dataset
from classypy.io.core.fetchers import WebpageFetcher
from classypy.text import TextParsingWarning, text_from_disk
from classypy.util.caching import cache_file
from classypy.util.etl import normalize_whitespace, to_ascii
from classypy.util.execution import run_in_parallel


class WebpageDataset(Dataset):
    fetcher_class = WebpageFetcher
    COLUMNS = ('url', 'url_clean', 'url_text')

    def fetch(self, urls, cache_on_fail=False, force=False, verbose=1):
        """
        Fetchs frontpage content for a given url.

        Parameters
        ----------
        urls : iterable
            List of website urls to scrape.
        cache_on_fail : boolean, optional
            Cache an empty string when url could not be fetched (default: False)
        force : boolean (default=False)
            When False, previously downloaded data are reused.
            When True, reports are downloaded at runtime.
        verbose: int (default=1)
            Level of output.

        Returns
        --------
        url_to_text_df : pd.DataFrame
            Dataframe with columns url & url_text
        """
        files, clean_urls = self.fetcher.fetch(
            urls=urls, cache_on_fail=cache_on_fail,
            force=force, verbose=verbose)

        with warnings.catch_warnings():
            warnings.simplefilter('ignore', TextParsingWarning)  # load files with best available encoding.
            file_content = [text_from_disk(f, default='') for f in files]

        return pd.DataFrame(
            data=list(zip(urls, clean_urls, file_content)),
            columns=self.COLUMNS)


class HomepageScrapeDataset(WebpageDataset):
    """
    Child class of Datasets that provides access to website text data.
    """
    dependencies = WebpageFetcher.add_dependencies(**BeautifulSoupParser.dependencies)
    TAGS = ('h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'h7', 'p', 'li', 'title', 'div', 'tr', 'td', 'span', 'body', 'ul')
    COLUMNS = ('url', 'url_clean', 'url_text', 'exceptions')

    @cache_file
    def parse_page_text(self, content, strip_unicode=False):
        # Parse content
        soup = BeautifulSoupParser.get_bs_html_obj(content=content)

        site_text = []
        for script in soup(["script", "style"]):
            script.decompose()
        for tag in soup.find_all(self.TAGS):
            site_text.append(tag.get_text().strip())

        all_text = " ".join(site_text)
        cleaned_text = normalize_whitespace(all_text)
        if strip_unicode:
            cleaned_text = to_ascii(cleaned_text)

        return cleaned_text

    def parse(self, files, strip_unicode=True, force=False, verbose=1):
        """
        Scrapes and fetches front-page text content for a given url.
        **Note** - The scraping methods in this are particular to the websites
        provided by the irs 990 forms. In particular, the conditional statements
        dealing with the "www." and "http" stuff may not work across the board for
        different websites. The 990 websites being scraped are for non-profits, and
        often don't have the most secure sites. Thus this seemingly ad-hoc code works
        well.

        Parameters
        ----------
        url : str
            URL of website to be scraped.
        verify : Boolean (default=True)
            Whether or not to ignore verification of SSL certificate.
        strip_unicode : Boolean (default=True)
            Whether or not to strip unicode values from result.

        Returns
        -------
            iterable : List of unicode-stripped tokens from front-page of `url`.
        """
        def _parse_html_file(html_file):
            """Returned parsed text if success, parsing exception if failed."""
            try:
                clean_file = html_file + '_clean'
                with warnings.catch_warnings():
                    warnings.simplefilter('ignore', TextParsingWarning)  # load files with best available encoding.
                    return self.parse_page_text(
                        # WebpageFetcher saves as utf-8, so we can read as utf-8
                        content=text_from_disk(html_file, disk_encoding='utf-8'),
                        strip_unicode=strip_unicode, file_path=clean_file, force=force)
            except Exception as ex:  # noqa
                # TODO: be specific about possible errors

                # DPLAT-1277 - don't output html file name in warning message,
                # otherwise bugsnag won't resolve tickets to be the same.
                if verbose > 0:
                    print("Parsed {html_file}...".format(html_file=html_file))
                warnings.warn("Exception during parsing: {ex}".format(ex=ex))
                return ex

        output = run_in_parallel(
            func=_parse_html_file,
            params=[{'html_file': html_file} for html_file in files])

        # Bubble up exceptions to logs, but don't prevent progress.
        clean_text = [txt if isinstance(txt, six.string_types) else None for txt in output]
        exceptions = [{'parsing': ex} if isinstance(ex, Exception) else {} for ex in output]
        return clean_text, exceptions

    def fetch(self, urls, strip_unicode=False, cache_on_fail=False, skip_spam=True, force=False, verbose=1):
        """
        Fetchs frontpage content for a given url.

        Parameters
        ----------
        urls : iterable
            List of website urls to scrape.
        strip_unicode : boolean (default=False)
            Whether or not to strip unicode values from result.
        cache_on_fail: boolean
            Cache an empty string when url could not be fetched (default: False)
        skip_spam : boolean
            If a requested URL is suspected spam (via Safe Browsing Lookup API),
            returns None
        force : boolean (default=False)
            When False, previously downloaded data are reused.
            When True, reports are downloaded at runtime.
        verbose: int (default=1)
            Level of output.

        Returns
        --------
        url_to_text_df : pd.DataFrame
            Dataframe with columns url & url_text
        """
        files, clean_urls = self.fetcher.fetch(
            urls=urls, cache_on_fail=cache_on_fail, skip_spam=skip_spam,
            force=force, verbose=verbose)
        file_content, exceptions = self.parse(
            files=files, strip_unicode=strip_unicode,
            force=force, verbose=verbose)

        return pd.DataFrame(
            data=list(zip(urls, clean_urls, file_content, exceptions)),
            columns=self.COLUMNS)

"""
Simple report download & info.
"""
from classypy.io.website import HomepageScrapeDataset

# urls to scrape home-page content for
urls = [
    "https://www.classy.org",
    "https://www.cityofhope.org",
    "https://www.nationalgeographic.com",
    "www.americanhuey369.org",  # needs url cleaning, has unusual encoding.
]

# search
dataset = HomepageScrapeDataset()
text_df = dataset.fetch(urls=urls, strip_unicode=False)

# view results
print("Shape of resulting dataframe: {shape}".format(shape=text_df.shape))
print("DataFrame:")
print(text_df)

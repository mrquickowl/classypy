American Community Survey (ACS) API

Notes
-----
The American Community Survey is an ongoing survey that provides demographic data and public funds use data on a yearly basis. Full details can be found at https://www.census.gov/programs-surveys/acs/about.html.

Currently, we only support pulling data from the ACS 5-year estimates. Every year, 3.4 million households are sampled across the United States and Puerto Rico. Annual estimates at the zip-code level are generated through statistical models and all estimates in the ACS 5-year data come with a margin of error (reported in the data).

The ACS breaks up local geographies into neighborhood blocks. Unfortunately, these blocks do not map onto US Postal Service zip codes. The ACS assigns each block a zip-code by ranking all the zip codes in the block by count and picking the one with the highest count. See this link for a full reference: https://www.census.gov/geo/reference/zctas.html

Each year's data point is a 5-year moving average, where the earliest year in the previous estimate is dropped and replaced by the latest years estimates. By default, the ACS wrapper fetches data for 2016. Please see the above links for supported years.

Content
-------
    :'data': ACS data for user supplied zip codes (csv).


Examples
----------

    :example.py: Fetches ACS data for a small set of zip codes, parses it, and outputs a dataframe of the results


References
----------

API reference: https://www.census.gov/content/dam/Census/data/developers/api-user-guide/api-guide.pdf
ACS Variable Names: https://api.census.gov/data/2016/acs/acs1/variables.html

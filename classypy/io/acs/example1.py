"""
Simple ACS API query & download
"""
from classypy.devops import find_secrets
from classypy.io.acs import ACSDataset

secrets = find_secrets()
dataset = ACSDataset(api_key=secrets['CENSUS_API_KEY'])
print(dataset.fetch(year=2016).head(5).T)

# Get individually
zctas = dataset.get_zctas_from_zip_codes(('91977', '01267'))
print(dataset.fetch(year=2016, zctas=zctas).head(5).T)

# Hybrid approach
zctas = dataset.get_zctas_from_zip_codes(('91977', '01267', '92101', '92137', '94587'))
print(dataset.fetch(year=2016, zctas=zctas).head(5).T)

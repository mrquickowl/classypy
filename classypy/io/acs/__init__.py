# Author: Bobertson Wang
# License: simplified BSD
import os.path as op
import tempfile

import numpy as np
import pandas as pd
import requests

from classypy.compat import md5_hash
from classypy.devops import logger
from classypy.io.core.datasets import Dataset
from classypy.io.core.fetchers import Fetcher
from classypy.util.caching import cache_dataframe, cache_object
from classypy.util.etl import to_ascii
from classypy.util.execution import run_in_parallel


class ACSFetcher(Fetcher):
    """
    Fetcher class for calling the census API.
    As of 2017, the Census API imposes 50 variable limit per API call.
    Full API documentation can be found in the reference in the README
    """
    dependencies = Fetcher.add_dependencies('census')
    MAX_VARS = 50

    def __init__(self, api_key=None, data_dir=None):
        """
        Initialize acs Fetcher class.

        Parameters
        ----------
        api_key: string, required
            API key that grants access to the ACS API
        data_dir: string, optional
            Path of the data directory.
        """
        super(ACSFetcher, self).__init__(data_dir=data_dir)
        self.api_key = api_key

    def get_connection(self, year):
        """Returns a Census API connection object. If year
        is not supported, Census will throw an exception (UnsupportedYearException).
        If api key is not valid it will throw an APIKeyError
        """
        from census import Census

        c = Census(self.api_key, year=year)
        return c

    @staticmethod
    def validate_zcta(zcta):
        """This function check to see whether or not a zcta is valid. The checks are:
        1. ZCTA can be encoded as a string, this will catch any Unicode errors
        2. ZCTA must contain only digits
        3. ZCTA cannot contain spaces
        4. ZCTA must be 5 digits long

        Parameters
        ----------
        zcta: string or int
            A ZCTA that we want to test

        Raises
        -------
            1. UnicodeError() if ZCTA cannot be encoded as string
            2. ValueError for other cases

        Returns
        -------
        Either a True, if ZCTA is valid, or raises the appropriate error
        """
        # No alphabetical characters
        zcta = to_ascii(zcta)

        if not zcta.isdigit():
            raise ValueError("Zipcode Incorrect Format, non-numeric characters found ({zcta})".format(zcta=zcta))
        # No Spaces
        elif ' ' in zcta:
            raise ValueError("Zipcode Incorrect Format, space found ({zcta})".format(zcta=zcta))
        # Must have exactly 5 elements (required by API)
        elif len(zcta) != 5:
            raise ValueError("Zipcode Incorrect Format, not five digits ({zcta})".format(zcta=zcta))

        return True

    @staticmethod
    def _generate_query_vars(census_dict=None):
        """Generate a ACS API query for the variables in census_dict. Specifically,
        this function expects the keys in census_dict to  be valid keys in the ACS data.
        It returns a tuple object where the elements are the keys in census_dict

        Parameters
        ----------
        census_dict: dict
            A dictionary containing valid API census variables as keys

        Returns
        -------
        census_vars: tuple
            A tuple object containing the ACS variables (census_dict keys)
        """
        census_vars = tuple(census_dict.keys())
        return census_vars

    @staticmethod
    @cache_dataframe(dtype={"ZIP_CODE": str, "ZCTA": str})
    def fetch_zip_code_to_zcta_mapping():
        """Fetch mappings between zip code and zcta.

        This mapping was the first that showed up on Google search:
        https://www.google.com/search?q=zip+code+to+zcta

        and comes from an organization trying to facilitate analysis
        of health care programs.
        https://www.udsmapper.org/about.cfm
        """
        HEADERS = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': 'en-GB,en;q=0.8,en-US;q=0.6',
            'User-Agent': ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko)'
                           ' Chrome/60.0.3112.113 Safari/537.36')
        }
        URL = "https://www.udsmapper.org/docs/zip_to_zcta_2018.xlsx"
        resp = requests.get(URL, headers=HEADERS)

        resp.raise_for_status()

        file_path = tempfile.mkstemp()[1]
        with open(file_path, 'wb') as fp:
            fp.write(resp.content)

        return pd.read_excel(file_path, dtype={"ZIP_CODE": str, "ZCTA": str})

    @cache_object
    def fetch(self, census_dict=None, zcta=None, year=2016, force=False):
        """ Fetch census data for variables (census_dict keys) for a given geography (zcta).
        This function first checks to see if the variables are less than the API limit
        and if the ZCTA is a valid ZCTA. We are using the 5-year estimates because this dataset
        contains ZCTA level data. See reference:

        https://www.census.gov/programs-surveys/acs/guidance/estimates.html

        Parameters
        ----------
        census_dict: dict
            A dictionary containing valid API census variables as keys
        zcta: str or int
            A (hopefully) valid ZCTA for which we want ACS data
        year: int
            A valid year for which we want ACS data
        force: boolean
            Whether to force re-download data

        Returns
        -------
        acs_results: list
            A list object containing dictionaries, where each dictionary is a
            JSON response from the ACS API
        """
        if len(census_dict.keys()) > self.MAX_VARS:
            raise ValueError("Too many variables for ACS API. Max allowed is 50; you requested {num_vars}".format(
                num_vars=len(census_dict.keys())))

        c = self.get_connection(year=year)
        census_vars = self._generate_query_vars(census_dict)
        if zcta is None:
            zcta = "*"
        else:
            self.validate_zcta(zcta=zcta)

        acs_results = c.acs5.zipcode(fields=census_vars, zcta=zcta)
        return acs_results


class ACSDataset(Dataset):
    """
    Dataset class for processing data from the census API.
    This class instantiates ACSFetcher as the fetcher_class
    """
    fetcher_class = ACSFetcher
    census_list = [
        ('B01002_001E', 'med_age'),
        ('B01002_002E', 'med_age_male'),
        ('B01002_003E', 'med_age_female'),
        ('B19049_001E', 'med_household_income_adj'),
        ('B19049_002E', 'med_household_income_adj_under_25'),
        ('B19049_003E', 'med_household_income_adj_25_44'),
        ('B19049_004E', 'med_household_income_adj_45_64'),
        ('B19049_005E', 'med_household_income_adj_65_over'),
        ('B18140_001E', 'med_earnings'),
        ('B18140_002E', 'med_earnings_disability'),
        ('B18140_003E', 'med_earnings_male_disability'),
        ('B18140_004E', 'med_earnings_female_disability'),
        ('B18140_005E', 'med_earnings_no_disability'),
        ('B18140_006E', 'med_earnings_male_no_disability'),
        ('B18140_007E', 'med_earnings_female_no_disability'),
        ('B01003_001E', 'total_pop'),
        ('B25104_001E', 'total_monthly_housing_costs'),
        ('B02001_005E', 'total_asian'),
        ('B02001_002E', 'total_white'),
        ('B02001_003E', 'total_black'),
        ('B02001_004E', 'total_native'),
        ('B02001_006E', 'total_pacific_islander'),
        ('B02001_007E', 'total_other_race'),
        ('B02001_008E', 'total_mixed_race'),
        ('B01001I_001E', 'total_latinx'),
        ('B05001_006E', 'total_non_citizens'),
        ('B05002_013E', 'total_foreign_born'),
        ('B05002_002E', 'total_born_america'),
        ('B05002_004E', 'total_born_other_state'),
        ('B12006_046E', 'total_divorced'),
        ('B12006_047E', 'total_divorced_male'),
        ('B12006_052E', 'total_divorced_female'),
        ('B12006_035E', 'total_widowed'),
        ('B01001_026E', 'total_female'),
        ('B23006_001E', 'total_degrees'),
        ('B23006_023E', 'total_degree_bachelor_higher'),
        ('B15003_025E', 'total_doctorate_degree'),
        ('B12006_001E', 'total_labor_force'),
        ('B16010_016E', 'total_labor_force_high_school'),
        ('B14005_001E', 'total_enrolled'),
        ('B14005_002E', 'total_enrolled_male'),
        ('B14005_006E', 'total_enrolled_female'),
        ('B17001_002E', 'total_below_poverty'),
        ('B18101_004E', 'total_under_five_disability'),
        ('B18101_007E', 'total_five_seventeen_disability'),
        ('B18101_009E', 'total_eighteen_thirty_four_disability'),
        ('B18101_012E', 'total_thirty_five_sixty_four_disability'),
        ('B18101_015E', 'total_sixty_four_seventy_four_disability'),
        ('B18101_018E', 'total_over_seventy_five_disability')
    ]
    COLUMNS = [val for _, val in census_list]
    census_dict = dict(census_list)

    def __init__(self, **kwargs):
        """
        Initialize an acs Dataset class.

        Parameters
        ----------
        kwargs:
            Additional kwarg arguments to feed the fetcher class.
        """
        super(ACSDataset, self).__init__(**kwargs)

    def _fix_na(self, df):
        """
        The ACS API returns NA's as -666666666.0, we need to replace
        these values with NA in order to aid downstream processing.

        Parameters
        ----------
        df: dataframe, required
            Dataframe containing ACS data
        """
        df = df.replace(-666666666.0, np.nan)

        return df

    def _clean_dataframe(self, acs_dict_list, year):
        """ This function takes a list of dictionaries and produces
        a dataframe.

        Parameters
        ----------
        acs_dict_list: list
            A list containing dicts, each corresponding to ACS data for a
            particular geography

        Returns
        -------
        census_df: dataframe
            A dataframe containing where the columns are the keys in each
            dictionary within acs_dict_list and the values are the rows
        """
        # Eliminate rows that are all nas when a zcta is not found.
        # They’re useless and have to be removed to do any JOIN downstream anyway.
        census_df = pd.DataFrame.from_dict([d for d in acs_dict_list if d is not None])
        census_df['year'] = year
        census_df = self._fix_na(df=census_df)

        return census_df

    def _get_data_filepath(self, zctas, year, census_dict):
        """Generate filepath for caching zcta results."""
        zctas = zctas or ["*"]
        pkl_file = op.join(self.data_dir, "yr{year}-zip{hash}-vars{hash2}.pkl".format(
            year=year,
            hash=md5_hash(",".join(sorted(zctas))),
            hash2=md5_hash(",".join(sorted(census_dict.keys())))))
        return pkl_file

    @staticmethod
    def _to_zip(zc):
        """Clean a 5-digit USA zip code to properly formatted string."""
        try:
            return "{zc:05d}".format(zc=int(zc))
        except:  # noqa
            return zc

    def map_zip_codes_to_zctas(self, zip_codes, force=False):
        """Map a list of 5-digit zip codes to ZCTAs.

        Returns
        -------
        dataframe : zip_code, zcta as columns.
        """
        zcta_df = self.fetcher.fetch_zip_code_to_zcta_mapping(
            csv_file=op.join(self.data_dir, "zip_code_to_zcta_mapping.csv"),
            force=force)
        # Normalize dataframe
        zcta_df['ZIP_CODE'] = zcta_df['ZIP_CODE'].apply(self._to_zip)
        zcta_df['ZCTA'] = zcta_df['ZCTA'].apply(self._to_zip)
        # Normalize data
        zip_code_df = pd.DataFrame(data=[self._to_zip(zc) for zc in zip_codes], columns=["zip_code"])
        # Join data
        return (
            zip_code_df
            .merge(zcta_df, left_on='zip_code', right_on='ZIP_CODE', how='left')
            [['zip_code', 'ZCTA']]
            .rename(columns={'ZCTA': 'zcta'})
        )

    def get_zctas_from_zip_codes(self, zip_codes, force=False):
        """Map a list of 5-digit zip codes into ZCTAs

        Returns
        -------
        List of unique zctas.
        """
        zcta_df = self.map_zip_codes_to_zctas(zip_codes=zip_codes, force=force)
        return (
            zcta_df[~zcta_df['zcta'].isna()]['zcta']
            .unique()
        )

    def fetch(self, zctas=None, census_dict=census_dict, acs_zcta_name='zip code tabulation area',
              year=2016, force=False):
        """ This function takes a list of ZCTAs and for each ZCTA, it pulls the variables
        found in the keys of census_dict from the ACS API.
        Alternatively, if zctas is None then we assume the call
        is for all ZCTAs. census_dict is expected to be a dict object where the keys are ACS variables and the
        values are human readable names. The ACS API returns one value for each key in census_dict. CensusException gets
        thrown whenever there is an error on the API side.

        Parameters
        ----------
        zctas: list, optional
            A list containing dicts, each corresponding to ACS data for a
            particular geography. ZCTA is not required, by default we pull all ZCTAs for a given year
        census_dict: dict
            A list containing dicts, each corresponding to ACS data for a
            particular geography
        acs_zcta_name: str
            A str containing the ZCTA column returned by the census API
        force: boolean
            Whether to force re-create data.
            NOTE: ignored; data is never cached.

        Returns
        -------
        census_df: dataframe
            A dataframe containing where the columns are the keys in each
            dictionary within acs_dict_list and the values are the rows
        """
        from census import CensusException

        df = pd.DataFrame(columns=['zcta'])

        if zctas is None or len(zctas) > 3:
            # this “3” magic number is just empirically, for speed.
            # It’s so slow to pull individual zctas, that pulling the entire dataframe
            # can be more efficient, then fish the zip codes from there.
            logger.info("Pulling ACS data for all zip code tabulation areas in {year}".format(year=year))
            acs_results = self.fetcher.fetch(
                census_dict=census_dict,
                year=year,
                pkl_file=self._get_data_filepath(year=year, zctas=None, census_dict=census_dict),
                force=force)

            data = []
            for acs_result in acs_results:
                acs_result['zcta'] = acs_result.pop(acs_zcta_name)
                acs_result = {census_dict.get(k, k): v for k, v in acs_result.items()}
                data.append(acs_result)

            df = self._clean_dataframe(acs_dict_list=data, year=year)

        if zctas is not None:
            def get_zip(zcta):
                logger.info("Pulling ACS data for {zcta}".format(zcta=zcta))
                # We want to allow users to request ZCTAs of unknown data quality in sequence, some ZCTAs
                # may not be well-formatted. We want to catch these exceptions in order to process the
                # ZCTAs that are well-formatted.
                try:
                    acs_results = self.fetcher.fetch(
                        census_dict=census_dict,
                        zcta=zcta, year=year,
                        pkl_file=self._get_data_filepath(year=year, zctas=(zcta,), census_dict=census_dict),
                        force=force)
                    acs_results = acs_results[0]  # eliminate top-level list wrapper
                    # Rename column names into more intuitive variables
                    acs_results['zcta'] = acs_results.pop(acs_zcta_name)
                    # Census_dict is expected to have intuitive variable names as values
                    acs_results = {census_dict.get(k, k): v for k, v in acs_results.items()}
                    return acs_results
                except IndexError as ie:
                    logger.info("No ACS data found for {zcta}: {ie}".format(zcta=zcta, ie=ie))
                except CensusException as ce:
                    logger.info("Census API threw an error: {error}".format(error=ce))
                except Exception as ex:  # noqa
                    logger.info("{error}".format(error=ex))

            df = df[df.zcta.isin(zctas)]
            missing_zctas = [zcta for zcta in zctas if zcta not in df.zcta.values]
            logger.info("Got data for {num_found_zctas} ZCTAs; pulling data for {num_missing_zctas}".format(
                num_found_zctas=len(df),
                num_missing_zctas=len(missing_zctas)))

            data = run_in_parallel(
                func=get_zip,
                params=[{'zcta': zcta} for zcta in missing_zctas])

            df = pd.concat(
                [df, self._clean_dataframe(acs_dict_list=data, year=year)],
                sort=False)

        return df

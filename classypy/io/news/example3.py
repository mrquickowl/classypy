"""
Download and review the UCI News data.
"""
from classypy.io.news import UciNewsDataset

# Fetch (if not already downloaded) and parse.
ds = UciNewsDataset()
articles_df = ds.fetch()

# Print text body
print('Downloaded {num_articles} articles\' metadata with columns {column_names}.'.format(
    num_articles=len(articles_df), column_names=', '.join(articles_df.columns)))

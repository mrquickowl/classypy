from .news_api import NewsAPIDataset
from .newspaper_codelucas import NewspaperDataset
from .uci import UciNewsDataset

__all__ = ['NewsAPIDataset', 'NewspaperDataset', 'UciNewsDataset']

from datetime import datetime

import pandas as pd

from classypy.compat import u as unicode
from classypy.io.core.datasets import HttpDataset
from classypy.util.dates import convert_datetime_columns
from classypy.util.execution import run_in_parallel


class NewspaperDataset(HttpDataset):
    dependencies = HttpDataset.add_dependencies(newspaper='newspaper')
    DATASET_COLUMNS = ['url', 'title', 'text', 'metadata', 'collected_at']
    DATETIME_COLUMNS = ('collected_at',)

    def __init__(self, *args, **kwargs):
        super(NewspaperDataset, self).__init__(*args, **kwargs)
        self.articles_df = None

    @convert_datetime_columns(column_names=DATETIME_COLUMNS)
    def fetch(self, urls, force=False, verbose=0):
        """
        Fetches news articles specified by URL, parses
        them using codelucas/newspaper and loads the article
        text and metadata with URL into a Dataframe.

        See: https://github.com/codelucas/newspaper/tree/python-2-head
        for more information on newspaper.

        Parameters
        ----------
        urls : list-like
            List of URLs to fetched and parsed.
        force : bool
            Flags whether to force download articles
        verbose : int
            Specifies HTTPDataset verbosity level

        Returns
        -------
        self.articles_df : DataFrame
            Dataframe of articles. See DATASET_COLUMNS for
            columns.

        """
        def _download_and_read_article(url):
            """Download and read article. Wrapper, to be used in run_in_parallel.

            Parameters
            ----------
            url: str
              url of article to be read

            Returns
            -------
            list
              list containing article title, text, meta data, and time collected
            """
            from newspaper import Article, ArticleException
            try:
                article = Article(url)
                article.download()
                article.parse()
                return [url,
                        unicode(article.title),
                        unicode(article.text),
                        unicode(article.meta_data),
                        str(datetime.utcnow().date())]
            except ArticleException:
                # As in original code, ignore articles that no longer exist.
                pass

        articles = run_in_parallel(
            func=_download_and_read_article,
            params=[{'url': path} for path in urls],
            return_errors=True)

        articles = [article for article in articles if article]

        self.articles_df = pd.DataFrame(articles, columns=self.DATASET_COLUMNS)

        if verbose > 0:
            n_no_text = sum(self.articles_df['text'] == '')
            if n_no_text > 0:
                print("WARNING: # of URLs with no text found: {no_text}".format(no_text=n_no_text))

        return self.articles_df

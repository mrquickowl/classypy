import pandas as pd

from classypy.util.dates import convert_datetime_columns

from ..core.datasets import HttpDataset
from ..core.fetchers import HttpFetcher


class UciNewsDatasetFetcher(HttpFetcher):
    """
    Fetch the UCI machine learning dataset.
    """
    UCI_URL = 'https://archive.ics.uci.edu/ml/machine-learning-databases/00359/NewsAggregatorDataset.zip'

    def fetch(self, verbose=1, force=False):
        """Download and unpack the dataset"""
        return super(UciNewsDatasetFetcher, self).fetch(
            (('newsCorpora.csv', self.UCI_URL, {'uncompress': True}),),
            verbose=verbose, force=force)


class UciNewsDataset(HttpDataset):
    """Wrapper around https://archive.ics.uci.edu/ml/datasets/News+Aggregator"""
    fetcher_class = UciNewsDatasetFetcher
    COLUMNS = [
        'id', 'title', 'url', 'newspaper', 'category_id',
        'story_id', 'hostname', 'timestamp',
    ]
    DATETIME_COLUMNS = ('timestamp',)

    @convert_datetime_columns(column_names=DATETIME_COLUMNS, unit='ms')
    def fetch(self, force=False, verbose=1):
        """
        Fetches news articles specified by URL, parses
        them using codelucas/newspaper and loads the article
        text and metadata with URL into a Dataframe.

        See: https://github.com/codelucas/newspaper/tree/python-2-head
        for more information on newspaper.

        Parameters
        ----------
        force : bool
            Flags whether to force download articles
        verbose : int
            Specifies verbosity level (0 = silent)

        Returns
        -------
        self.articles_df : DataFrame
            Dataframe of articles. See COLUMNS for list of columns.

        """
        csv_file = self.fetcher.fetch(verbose=verbose, force=force)[0]
        df = pd.read_csv(csv_file, names=self.COLUMNS, delimiter='\t', encoding='utf-8')
        # Catch ill-formed URLs by stripping out "\"
        df['url'] = df['url'].apply(lambda x: x.replace("\\", ""))
        return df

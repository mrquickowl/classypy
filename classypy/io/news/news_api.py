import json
import os
import urllib
from datetime import datetime

import numpy as np
import pandas as pd
import pytz
from dateutil import parser

from classypy.util.dates import convert_datetime_columns

from ..core.datasets import HttpDataset


class NewsAPIDataset(HttpDataset):
    DATETIME_COLUMNS = ('date_fetched', 'published_at')

    # Endpoint URLs
    SOURCES_ENDPOINT = 'https://newsapi.org/v1/sources'
    ARTICLES_ENDPOINT = 'https://newsapi.org/v1/articles'

    # To snake_case
    SOURCES_COLUMN_MAP = {'urlsToLogos': 'urls_to_logos', 'sortBysAvailable': 'sort_by_available'}
    ARTICLES_COLUMN_MAP = {'publishedAt': 'published_at', 'urlToImage': 'url_to_image'}

    def __init__(self, api_key, *args, **kwargs):
        """
        Parameters
        ----------
        api_key : str
            Alphanumeric News API key
        """
        super(NewsAPIDataset, self).__init__(*args, **kwargs)

        self.api_key = api_key or os.environ.get('NEWS_API_KEY')
        if self.api_key is None:
            raise ValueError('API key is required to use News API'
                             ' service. Please ensure that either'
                             ' the key is passed an argument or defined'
                             ' as environment variable.')

    def _get_sort_by_option(self, sources_df, source, sort_by):
        """
        Returns a valid sort-by option for a given source.

        Parameters
        ----------
        sources_df : DataFrame
            Dataframe of known News API sources and their metadata.
        source : str
            News API ID for a source
        sort_by : str
            Sort-by option of interest.

        Returns
        -------
        sort_by : str
            Returns a valid sort_by query parameter for self.fetch.
        """
        # Get valid sortBy parameters options for a given source.
        valid_options = sources_df.loc[sources_df['id'] == source]['sort_by_available'].tolist()[0]

        if sort_by not in valid_options:
            return np.random.choice(valid_options, 1)[0]
        else:
            return sort_by

    @convert_datetime_columns(column_names=DATETIME_COLUMNS)
    def fetch(self, sources=None, sort_by=None, verbose=1, force=False,
              earliest_date=None, max_articles=None, **kwargs):
        """
        Requests articles for specified sources using News API.
        See: https://newsapi.org/#documentation.

        Parameters
        ----------
        sources : list(str), optional
            News source IDs, see 'id' key in sources. (default: all)
        sortby : str, optional
            Sortby options: 'top', 'latest', 'popular'.
        force : bool, optional
            Force download articles JSON even if cached.
        verbose : int, optional
            Specifies whether to be explicit when fetching JSONs.
        earliest_date : str, optional
            Fetch filters articles returning only ones whose
            date is new than the specified date. Any format
            parsable by dateutil.parser is supported.
        max_articles : int, optional
            Maximum number of articles to be returned. Articles
            are selected so that distribution across sources is
            the same as the unthresholded return.
        **kwargs : dict
            Arguments to be passed to parent fetch call
            other than force or verbose.

        Returns
        -------
        articles_df : Dataframe
            Dataframe of all articles acquired from valid sources
            in sources iterable according to sortby criteria.

        Requests with non-200 response have three relevant keys:
        status, code, message. code response describes the internal
        News API error, while message describes the internal error in
        more detail.

        """
        news_api_sources = self.fetch_sources()

        # Default to all, check for invalid values.
        sources = sources if sources is not None else news_api_sources
        unknown_sources = set(sources) - set(news_api_sources['id'].tolist())
        if unknown_sources:
            raise ValueError(
                "{sources} not found in News API sources; call 'list_sources' to"
                " for a list of verified source IDs.".format(
                    sources=', '.join(unknown_sources)))

        # Used below for time-stamping
        cur_date = str(datetime.utcnow().date())

        source_tuples = []
        for source in sources:
            sort_by = self._get_sort_by_option(news_api_sources, source, sort_by)
            params = {'source': source, 'sortby': sort_by, 'apiKey': self.api_key}
            articles_json = "{source}_{sort_by}_{date}.json".format(
                source=source, sort_by=sort_by, date=cur_date)
            query_url = "?".join([self.ARTICLES_ENDPOINT, urllib.urlencode(params)])
            source_tuples.append((articles_json, query_url, {}))

        source_jsons = super(NewsAPIDataset, self).fetch(
            files=source_tuples, verbose=verbose, force=force)

        article_dfs = []
        for source_json in source_jsons:
            with open(source_json, 'r') as f_obj:
                response = json.load(f_obj)
            article_df = pd.DataFrame(response['articles'])
            article_df['source'] = response['source']
            article_df['date_fetched'] = cur_date
            article_dfs.append(article_df)

        articles_df = pd.concat(article_dfs, sort=True)

        # Map camel to snake-case
        articles_df = articles_df.rename(columns=self.ARTICLES_COLUMN_MAP)

        # Filter on dates
        if earliest_date:
            published_at = articles_df['published_at'].map(parser.parse)
            articles_df = articles_df[published_at > parser.parse(earliest_date).replace(tzinfo=pytz.utc)]

        # Filter of sources
        if max_articles:
            articles_df = _sample_articles(articles_df, max_articles)

        return articles_df

    def fetch_sources(self, category=None, language=None, country=None, **kwargs):
        """
        Gets a JSON of News API accessible news sources with metadata using
        News API. See: https://newsapi.org/#documentation.

        Parameters
        ----------
        category : str
            If None, all categories are returned.
        language : str
            ISO-639-1 code of languages of sources to return.
            If None, then all sources of all languages are
            returned.
        country : str
            ISO 3166-1 code of country to get sources from.
            If None, then all sources are returned.

        Returns
        -------
        source_df : DataFrame
            Returns DataFrame contain source metadata for all
            sources meeting specified category, country and
            language criteria.

        """
        # Build query string and local JSON filename
        params = {'apiKey': self.api_key}
        source_json_name = 'news_api_sources'

        for param_name, param_value in zip(('category', 'language', 'country'), (category, language, country)):
            if param_value is not None:
                params.update({param_name: param_value})
                source_json_name += '-' + param_value

        source_json_name += '.json'

        query_url = "?".join([self.SOURCES_ENDPOINT, urllib.urlencode(params)])
        source_file = [(source_json_name, query_url, {})]
        source_json = super(NewsAPIDataset, self).fetch(files=source_file, **kwargs)[0]

        # Read sources JSON
        with open(source_json, 'r') as f_obj:
            sources_metadata = json.load(f_obj)

        # Generate Dataframe of sources metadata
        sources_df = pd.DataFrame(sources_metadata['sources'])

        # Map camel to snake-case
        sources_df = sources_df.rename(columns=self.SOURCES_COLUMN_MAP)

        return sources_df


def _sample_articles(articles_df, n):
    """
    Samples articles such that source distributions
    are maintained.

    Parameters
    ----------
    articles_df : DataFrame
        DataFrame containing article metadata.

    n : int
        Number of articles to sample from articles_df

    Returns
    -------
    sampled_df : DataFrame
        An n-sample Dataframe derived from articles_df
    """
    p = n / float(len(articles_df))

    sampled_dfs = []
    for a, articles_df in articles_df.groupby('source'):
        source_count = len(articles_df)
        sub_n = int(np.ceil(p * source_count))
        sampled_dfs.append(articles_df.sample(n=sub_n))

    sampled_df = pd.concat(sampled_dfs, sort=True)

    # If sampled_df is too large; subsample over all sources.
    if len(sampled_df) > n:
        sampled_df = sampled_df.sample(n=n)

    return sampled_df

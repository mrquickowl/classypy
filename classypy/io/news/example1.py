"""
Fetches and saves metadata for all News API sources,
and fetches and saves articles for all sources.
Prints first five articles.
"""
from classypy.devops import find_secrets
from classypy.io.news import NewsAPIDataset

# Fetching API key via Credstash or .env file
secrets = find_secrets()

# Fetching all sources
ds = NewsAPIDataset(api_key=secrets['NEWS_API_KEY'])
sources = ds.fetch_sources()
selected_sources = sources['name'].sample(n=5).tolist()
print("{n_sources} available news sources, e.g. {sources}".format(
    n_sources=len(sources), sources=', '.join(selected_sources)))

# Getting articles for all sources

articles_df = ds.fetch(
    sources=sources[sources['name'].isin(selected_sources)]['id'],
    max_articles=10)
print(articles_df.head(5))

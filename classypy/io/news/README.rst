News Datasets

Notes
-----
News API can be utilized to grab current articles from prominent digital media organizations according to a variety of user-specified criteria.

Obtaining Credentials
-------
1. Visit https://newsapi.org/
2. Register for an account.
3. You should be shown an API key.

Contents
--------

    :news_api: Contains NewsAPIDataset which provides an interface for News API. All responses are stored as DataFrames.
    :news_paper: Contains NewspaperDataset which fetches and parses digital news articles specified by URL.
    :uci: Contains UCI News dataset, a static, curated dataset for machine learining.

Examples
--------

    :example1.py: Gets all known News API sources and gets latest articles from the BBC and Reuters websites.
    :example2.py: Fetches and parses two digital articles and prints their metadata and text body.
    :example3.py: Fetches and returns the UCI News dataset.



References
----------

https://newsapi.org/#documentation
https://github.com/codelucas/newspaper
https://archive.ics.uci.edu/ml/datasets/News+Aggregator
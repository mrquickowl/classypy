"""
Uses Newspaper Dataset to fetch and parse two news articles
specified via URL. Prints article text body and metadata for each.
"""
from classypy.io.news import NewspaperDataset

urls = ['http://www.dailynews.com/sports/20170814/claire-carew-team-with-city-of-hope-to-fight-cancer',
        'http://www.foxla.com/good-day/good-day-la-experts-and-other-guests/268863288-story']

# Fetch (if not already downloaded) and parse.
ds = NewspaperDataset()
articles_df = ds.fetch(urls=urls)

# Print text body
for _, article in articles_df.iterrows():
    print('Text body: {text}\n'.format(text=article['text']))
    print('metadata: {metadata}\n\n'.format(metadata=article['metadata']))

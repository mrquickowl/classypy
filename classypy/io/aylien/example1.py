"""Detect spikes in five IPTC categories relevant to the non-profit space."""

from classypy.devops import find_secrets
from classypy.io.aylien import AylienCategoryActivityDataset

# IPTC codes
category_codes = ["16011000", "11011000", "05000000", "05007000", "06000000", "06005000"]

secrets = find_secrets()
dataset = AylienCategoryActivityDataset(
    api_id=secrets['AYLIEN_API_ID'],
    api_key=secrets['AYLIEN_API_KEY'])
df = dataset.fetch(category_ids=category_codes)
print(df)

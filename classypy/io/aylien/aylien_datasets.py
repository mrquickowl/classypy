import abc
import copy
import re
import time
from datetime import datetime
from functools import reduce

import dateutil.parser as date_parser
import numpy as np
import pandas as pd

from ..core.datasets import Dataset
from ..core.fetchers import Fetcher
from .iptc import IptcCategoriesDataset


def _strip_tz_info(dt):
    if dt is None or getattr(dt, 'tz_localize', None) is None:
        # None or datetime-unaware timezone
        return dt
    # Strip utc data
    return dt.tz_localize(None)


def _nested_attr_ref(base_obj, cascaded_call):
    attrs = cascaded_call.split('.')
    return reduce(lambda obj, attr: getattr(obj, attr), [base_obj] + attrs)


def _format_aylien_date(date):
    """If needed, formats a date into the Aylien format (e.g. NOW-7DAY)"""
    if isinstance(date, datetime):
        date = date.strftime('%Y-%m-%d')
    if re.match(r'NOW-[0-9]+DAY', date):
        formatted_date = date
    else:
        date_diff = datetime.today() - date_parser.parse(date)
        days = date_diff.days
        formatted_date = 'NOW-{days}DAY'.format(days=days)
    return formatted_date


def _get_articles_per_category(max_articles, category_ids):
    num_categories = float(max(1, len(category_ids)))
    articles_per_category = int(np.floor(max_articles / num_categories))
    return max(1, articles_per_category)


class AylienFetcher(Fetcher):
    """
    Aylien API Singleton wrapper that creates new instance
    if it doesn't exist and otherwise returns extant instance.
    """
    dependencies = Fetcher.add_dependencies(
        aylien_news_api='aylien_news_api', pyopenssl='pyopenssl',
        ndg='ndg-httpsclient', pyasn1='pyasn1')  # suppresses InsecurePlatformWarning
    API_DAILY_RATE_LIMIT = 333
    API_SLEEP_TIME = 0.5
    api_instance = None

    def __init__(self, data_dir, api_id, api_key):
        super(AylienFetcher, self).__init__(data_dir=data_dir)
        self.api_instance = self.get_instance(api_id=api_id, api_key=api_key)

    def sleep_and_return(self, api_response):
        time.sleep(AylienFetcher.API_SLEEP_TIME)
        return api_response

    def fetch_category_activity(self, category_id, n_days=7, force=False, verbose=1):
        opts = {
            'published_at_start': 'NOW-{n_days}DAY'.format(n_days=n_days),
            'published_at_end': 'NOW-1DAY',
            'categories_taxonomy': 'iptc-subjectcode',
            'categories_id': [category_id],
        }

        return self.sleep_and_return(self.api_instance.list_time_series(**opts))

    def fetch_trend(self, category_id, start_date, end_date=None, force=False, verbose=1):
        opts = {
            'published_at_start': _format_aylien_date(start_date),
            'categories_taxonomy': 'iptc-subjectcode',
            'categories_id': [category_id],
        }
        if end_date:
            opts['published_at_end'] = _format_aylien_date(end_date)

        return self.sleep_and_return(self.api_instance.list_trends('keywords', **opts))

    def fetch_articles(self, sort_by='relevance', start_date=None, end_date=None, category_id=None,
                       max_articles=100, force=False, verbose=1, **kwargs):

        start_date = _format_aylien_date(start_date) if start_date else None
        end_date = _format_aylien_date(end_date) if end_date else None

        default_args = {
            'start_date': start_date,
            'end_date': end_date,
            'sort_by': sort_by,
            'category_id': [category_id],
            'max_articles': max_articles
        }
        default_args = {k: v for k, v in default_args.iteritems() if v is not None}

        option_mappings = {
            'keyword': 'text',
            'start_date': 'published_at_start',
            'end_date': 'published_at_end',
            'max_articles': 'per_page',  # don't deal with paging
            'category_id': 'categories_id'
        }

        # replace argument names with correct aylien names
        kwargs.update(default_args)
        for opt_name, aylien_opt_name in option_mappings.iteritems():
            if opt_name in kwargs:
                kwargs[aylien_opt_name] = kwargs.pop(opt_name)

        # See: https://newsapi.aylien.com/docs/working-with-categories for explanation
        kwargs['categories_taxonomy'] = 'iptc-subjectcode'

        return self.sleep_and_return(self.api_instance.list_stories(**kwargs))

    @classmethod
    def get_instance(cls, api_id, api_key):
        import aylien_news_api

        if cls.api_instance:
            return cls.api_instance

        aylien_news_api.configuration.api_key['X-AYLIEN-NewsAPI-Application-ID'] = api_id
        aylien_news_api.configuration.api_key['X-AYLIEN-NewsAPI-Application-Key'] = api_key

        cls.api_instance = aylien_news_api.DefaultApi()
        return cls.api_instance


class AylienDataset(Dataset):
    fetcher_class = AylienFetcher
    COLUMNS = None

    def __init__(self, api_id, api_key):
        super(AylienDataset, self).__init__(
            api_id=api_id, api_key=api_key)
        assert self.COLUMNS is not None

    def fetch(self, categories, verbose=1, force=False, **kwargs):
        """
        Base fetch function that iterates over categories. Calls
        _call_api_return_dataframe with each category_id

        Parameters
        ----------
        categories: list or dict
            If a list, it should be a list of category_ids.
            If a dict, each dict should contain an 'id' key (category_id),
            along with any other data relevant to API call per category.
        """
        from aylien_news_api.rest import ApiException

        if not np.any([isinstance(category, dict) for category in categories]):
            # Just a list of categories, no extra variables to iterate on
            category_ids = categories
            iter_vars = [dict()] * len(category_ids)
        else:
            # Each category contains an id and other variables to iterate on.
            # Let's separate these!
            iter_vars, category_ids = [], []
            for category in copy.copy(categories):
                category_ids.append(category['id'])
                del category['id']
                iter_vars.append(category)
        del categories

        category_ids = IptcCategoriesDataset.filter_valid_categories(
            category_ids, verbose=verbose, force=force)

        dfs = []
        for category_id, other_vars in zip(category_ids, iter_vars):
            other_vars.update(kwargs)
            try:
                df = self._call_api_return_dataframe(category_id=category_id, **other_vars)
            except ApiException as ex:
                if getattr(ex, 'status', None) == 401:
                    # Unauthorized; bubble this up
                    raise
                if verbose > 0:
                    print("Exception when calling Aylien API: {ex}".format(ex=ex))
            else:
                dfs.append(df)

        return pd.concat(dfs, sort=True) if dfs else pd.DataFrame(columns=self.COLUMNS)

    @abc.abstractmethod
    def _call_api_return_dataframe(self, category_id, verbose=1, **kwargs):
        raise NotImplementedError


class AylienCategoryActivityDataset(AylienDataset):
    """
    Leverages to Aylien API time series to detect
    spikes in news relevant to an IPTC news
    category.
    """
    COLUMNS = ('count', 'published_at', 'category_id', 'category_name')

    def fetch(self, category_ids, n_days=7, force=False, verbose=1):
        return super(AylienCategoryActivityDataset, self).fetch(
            categories=category_ids, n_days=n_days, force=force, verbose=verbose)

    def _call_api_return_dataframe(self, category_id, n_days=7, verbose=1):
        api_response = self.fetcher.fetch_category_activity(
            category_id=category_id, n_days=n_days, verbose=verbose)

        df = pd.DataFrame(data=[{
            'count': r.count,
            'published_at': r.published_at,
            'category_id': category_id,
            'category_name': IptcCategoriesDataset.lookup_category(category_id),

        } for r in api_response.time_series])
        df['published_at'] = df['published_at'].map(_strip_tz_info)

        return df


class AylienTrendsDataset(AylienDataset):
    """
    Returns top trends for categories.
    """
    COLUMNS = ('count', 'keyword', 'category_id', 'category_name')

    def fetch(self, category_ids, start_date, end_date=None, n_trends=10, force=False, verbose=1):
        return super(AylienTrendsDataset, self).fetch(
            categories=category_ids, start_date=start_date, end_date=end_date, n_trends=n_trends,
            force=force, verbose=verbose)

    def _call_api_return_dataframe(self, category_id, start_date, end_date=None, n_trends=10, verbose=1):
        api_response = self.fetcher.fetch_trend(
            category_id=category_id, start_date=start_date, end_date=end_date)

        df = pd.DataFrame(data=[{
            'count': r.count,
            'keyword': r.value,
            'category_id': category_id,
            'category_name': IptcCategoriesDataset.lookup_category(category_id),
        } for r in api_response.trends])

        return df.sort_values(by='count', ascending=False).head(n_trends)


class AylienArticlesDataset(AylienDataset):
    """
    Grabs most relevant articles for specified keywords.
    Articles are fetched from 'start_date' to time of fetch call.
    """
    RESPONSE_ATTRIBUTES = (
        'id', 'author.id', 'author.name', 'title', 'body', 'source.domain',
        'published_at', 'source.id', 'social_shares_count.facebook', 'social_shares_count.reddit',
        'social_shares_count.linkedin', 'social_shares_count.google_plus'
    )

    COLUMNS = tuple(
        # response attribute mappings are assumed to be first, in code below
        [re.sub(r'\.', '_', attr) for attr in RESPONSE_ATTRIBUTES] +
        ['keyword', 'collected_at', 'category_id', 'category_name', 'total_social_shares']
    )

    def fetch(self, category_ids, start_date, end_date=None, sort_by='relevance',
              keywords=None, max_articles=None, force=False, verbose=1, **kwargs):
        assert keywords is None or len(keywords) == len(category_ids), "There must be one keyword per category_id"

        if max_articles is None:
            max_articles_per_category = 10
        else:
            max_articles_per_category = _get_articles_per_category(max_articles, category_ids)

        # Create a dict of the keyword and category together
        if keywords:
            categories = [dict(id=category_id, keyword=keyword) for category_id, keyword in zip(category_ids, keywords)]
        else:
            categories = list(category_ids)

        return super(AylienArticlesDataset, self).fetch(
            categories=categories, start_date=start_date, end_date=end_date, sort_by=sort_by,
            max_articles=max_articles_per_category, force=force, verbose=verbose, **kwargs)

    def _call_api_return_dataframe(self, category_id, start_date, end_date=None, max_articles=100, verbose=1, **kwargs):
        api_response = self.fetcher.fetch_articles(
            category_id=category_id, start_date=start_date,
            end_date=end_date, max_articles=max_articles, **kwargs
        )

        stories = []
        for story in api_response.stories:
            story = dict(zip(
                self.RESPONSE_ATTRIBUTES,
                [_nested_attr_ref(story, attr) for attr in self.RESPONSE_ATTRIBUTES]
            ))
            story.update({
                'published_at': _strip_tz_info(story['published_at']),
                'collected_at': datetime.utcnow(),
                'category_id': category_id,
                'category_name': IptcCategoriesDataset.lookup_category(category_id),
            })

            if 'keyword' in kwargs:
                story.update({'keyword': kwargs['keyword']})
            stories.append(story)

        df = pd.DataFrame(data=stories) if stories else pd.DataFrame(columns=self.COLUMNS)
        df['published_at'] = df['published_at'].map(_strip_tz_info)

        # Rename columns
        response_attributes_to_columns_mapping = dict(zip(
            self.RESPONSE_ATTRIBUTES, self.COLUMNS[:len(self.RESPONSE_ATTRIBUTES)]))
        df = df.rename(columns=response_attributes_to_columns_mapping)

        # unpack and sum social_share_count columns
        social_share_cols = [col for col in df.columns if col.startswith('social_shares')]
        for col in social_share_cols:
            df[col] = df[col].map(lambda x: x[0].count)
        df['total_social_shares'] = df[social_share_cols].apply(sum, axis=1)

        return df

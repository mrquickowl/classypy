# Fetch 10 articles from last 7 days most relevant to 'benefits'.

from classypy.devops import find_secrets
from classypy.io.aylien import AylienArticlesDataset

secrets = find_secrets()
dataset = AylienArticlesDataset(
    api_id=secrets['AYLIEN_API_ID'],
    api_key=secrets['AYLIEN_API_KEY'])
articles = dataset.fetch(
    keywords=['benefits'], category_ids=['14015000'],  # Welfare
    start_date='NOW-7DAY', max_articles=10)
print(articles)

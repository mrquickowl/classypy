import os.path as op
import re

import numpy as np
import pandas as pd
import six

from classypy.io.core._utils.parsing import BeautifulSoupParser
from classypy.io.core.datasets import HttpDataset
from classypy.text import text_from_disk
from classypy.util.caching.csv import cache_dataframe


class IptcCategoriesDataset(HttpDataset):
    dependencies = HttpDataset.add_dependencies(**BeautifulSoupParser.dependencies)
    CATEGORIES_MAP = None  # id => name

    """
    Dataset class IPTC category subject codes.
    """
    def __init__(self, *args, **kwargs):
        self.subject_code_url = 'http://cv.iptc.org/newscodes/subjectcode'
        self.dest_file = 'iptc_categories.html'
        self.csv_file = 'iptc_categories.csv'
        self.fetch_headers = {
            'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8',
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': 'en-GB,en;q=0.8,en-US;q=0.6',
            'User-Agent': ('Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_6) AppleWebKit/537.36 (KHTML, like Gecko)'
                           ' Chrome/60.0.3112.113 Safari/537.36')
        }
        super(IptcCategoriesDataset, self).__init__(*args, **kwargs)

    def fetch(self, force=False, verbose=1, *args, **kwargs):
        file = [(self.dest_file, self.subject_code_url, {
            'headers': self.fetch_headers,
            'uncompress': True
        })]
        iptc_file_path = super(IptcCategoriesDataset, self).fetch(
            files=file, verbose=verbose, force=force, **kwargs)[0]

        raw_content = text_from_disk(iptc_file_path)

        cache_file_path = op.join(self.data_dir, self.csv_file)
        categories_df = self.get_valid_categories(raw_content, force=force, csv_file=cache_file_path)

        return categories_df

    @cache_dataframe(encoding='utf-8', dtype={'code': str})
    def get_valid_categories(self, raw_content):
        """
        Takes raw HTML and returns structured DataFrame
        containing IPTC codes and relevant metadata.

        Parameters
        ----------
        raw_content : str
            Raw read from subject_code_url.

        Returns
        -------
        categories_df : DataFrame
            DataFrame containing name, code, parent concept,
            uri and description of IPTC catergories.

        """
        bso = BeautifulSoupParser.get_bs_html_obj(raw_content, encoding='utf-8')

        # Ignore first meta-data table
        content_tables = bso.find_all('table', attrs={'class': 'cptdata1'})[1:]
        categories = []

        for row in content_tables:
            topic = {}
            for i, tr in enumerate(row.find_all('tr')):
                if i == 0:
                    topic['uri'] = tr.a.get('href')
                    raw_concept = tr.find('span', attrs={'class': "infoheading1"}).text
                    topic['status'] = re.sub(r'\n[\s]*', '', raw_concept)
                    topic['code'] = op.split(topic['uri'])[1]

                name_span = tr.find('span', attrs={'class': 'infoname'})
                if name_span:
                    if name_span.text == 'Name':
                        topic['name'] = tr.find('span', attrs={'class': 'infotext1'}).text

                parent_span = tr.find('span', attrs={'class': 'infolabel'})
                if parent_span:
                    if parent_span.text == 'Broader concept':
                        topic['parent'] = tr.find('a').get('href')

                description_span = tr.find('span', attrs={'class': 'infolabel'})
                if description_span:
                    if description_span.text == 'Definition':
                        topic['description'] = tr.find('span', attrs={'class': 'infotext2'}).text

            categories.append(topic)

        return pd.DataFrame(categories)

    @classmethod
    def get_category_map(cls, force=False, verbose=1):
        if cls.CATEGORIES_MAP is None:
            categories_df = cls().fetch(force=force, verbose=verbose)
            cls.CATEGORIES_MAP = dict(zip(categories_df['code'], categories_df['name']))
        return cls.CATEGORIES_MAP

    @classmethod
    def lookup_category(cls, category_id, force=False, verbose=1):
        return cls.get_category_map().get(category_id)

    @classmethod
    def filter_valid_categories(cls, categories, force=False, verbose=1):
        """
        Ensure target categories codes are valid.

        Parameters
        ----------
        categories : list-like
            List of candidate IPTC codes.

        Returns
        -------
        valid_categories: list
            List of valid IPTC codes
        """
        assert np.all([isinstance(cat, six.string_types) for cat in categories]), 'categories should be strings.'

        code_reference_list = list(cls.get_category_map(force=force, verbose=verbose).keys())
        valid_categories = [cat for cat in categories if cat in code_reference_list]
        if len(valid_categories) != len(categories) and verbose > 0:
            bad_categories = list(set(categories) - set(valid_categories))
            print('Dropping invalid IPTC codes: {codes}'.format(codes=', '.join(bad_categories)))

        return valid_categories

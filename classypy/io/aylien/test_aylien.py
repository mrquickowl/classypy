import pytest

from classypy.io.aylien.iptc import IptcCategoriesDataset


def test_filter_valid_categories():
    with pytest.raises(AssertionError):
        IptcCategoriesDataset.filter_valid_categories([1])

    # All valid
    assert len(IptcCategoriesDataset.filter_valid_categories([])) == 0
    assert len(IptcCategoriesDataset.filter_valid_categories(['01010001', '01010001'])) == 2

    # Some invalid
    assert len(IptcCategoriesDataset.filter_valid_categories(['01010001', '12345679'])) == 1

    # All invalid
    assert len(IptcCategoriesDataset.filter_valid_categories(['12345679'])) == 0

Aylien Datasets

Notes
-----
Contains multiple Aylien datasets and a wrapper dataset around IPTC news category subject codes.


Obtaining Credentials
---------------------
1. Visit https://newsapi.aylien.com
2. Register for an account.
3. Save API and ID key to local .env file.

Contents
--------

    :aylien_datasets: Contains 'AylienSpikeDataset', 'AylienArticlesDataset' and 'AylienTrendsDataset' datasets. Each dataset is wrapper around SDK API call and all responses are stored as DataFrames.

Examples
--------

    :example1.py: Detect spikes in five IPTC categories relevant to the non-profit space.
    :example2.py: Grab top 10 trends for 'education' IPTC category.
    :example3.py: Fetch 10 articles from last 7 days most relevant to 'benefits'.



References
----------

https://newsapi.aylien.com/docs/introduction
http://cv.iptc.org/newscodes/subjectcode
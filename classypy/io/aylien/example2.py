# Grab top 10 trends for 'education' IPTC category.

from classypy.devops import find_secrets
from classypy.io.aylien import AylienTrendsDataset

secrets = find_secrets()
dataset = AylienTrendsDataset(
    api_id=secrets['AYLIEN_API_ID'],
    api_key=secrets['AYLIEN_API_KEY'])
df = dataset.fetch(category_ids=["05000000"], start_date='NOW-3DAY', n_trends=10)
print(df)

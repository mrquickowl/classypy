Social Media Accounts on Websites

Notes
-----
This is a small scraper that, given a webpage URL, will download the page and try to detect various social media URLs/accounts on it.


Examples
----------

    :example1.py: Fetches data for two public websites.


References
----------

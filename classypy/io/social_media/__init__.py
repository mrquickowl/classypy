"""
"""
import os.path as op
import re
import warnings
from collections import defaultdict

import numpy as np
import pandas as pd

from classypy.io.core._utils.parsing import BeautifulSoupParser
from classypy.io.core.datasets import HttpDataset
from classypy.io.core.fetchers import WebpageFetcher
from classypy.ml.distances import levenshtein
from classypy.text import TextParsingWarning, text_from_disk
from classypy.util.etl import to_ascii


def accept_all(url):
    return True


def fb_cleaner(url):
    # http://www.facebook.com/share.php?https://www.anchoragemuseum.org/&t=home
    return ('facebook.com/share.php' not in url
            and 'facebook.com/sharer/sharer.php' not in url
            and 'facebook.com/facebook' not in url)


class SocialMediaOnWebpageDataset(HttpDataset, BeautifulSoupParser):
    """TODO: docstring"""
    dependencies = HttpDataset.add_dependencies(*BeautifulSoupParser.dependencies)
    fetcher_class = WebpageFetcher
    COLUMNS = (
        'website', 'website_source', 'charitynavigator.org', 'charitywatch.org',
        'facebook.com', 'facebook_id', 'instagram.com', 'instagram_id',
        'plus.google.com', 'plus.google_id', 'snapchat.com', 'snapchat_id',
        'twitter.com', 'twitter_id', 'vimeo.com', 'vimeo_id',
        'website_clean', 'youtube.com', 'youtube_id'
    )

    LINK_REGEXPS = {
        'facebook': [r'https?\:\/\/[^\/]*facebook.com[\#\/\!]*[a-zA-Z0-9\/+-]+'],
        'twitter': [r'https?\:\/\/[^\/]*twitter.com[\#\/\!]*[a-zA-Z0-9\/+-]+'],
        'google+': [r'https?\:\/\/[^\/]*plus.google.com[\#\/\!]*[a-zA-Z0-9\/+-]+'],
        'youtube': [r'https?\:\/\/[^\/]*youtube.com[\#\/\!]*[a-zA-Z0-9\/+-]+'],
        'instagram': [r'https?\:\/\/[^\/]*instagram.com[\#\/\!]*[a-zA-Z0-9\/+-]+'],
        'vimeo': [r'https?\:\/\/[^\/]*vimeo.com[\#\/\!]*[a-zA-Z0-9\/+-]+'],
    }

    DOMAINS_OF_INTEREST = {
        'facebook.com': fb_cleaner,
        'twitter.com': accept_all,
        'plus.google.com': accept_all,
        'youtube.com': accept_all,
        'instagram.com': accept_all,
        'snapchat.com': accept_all,

        'vimeo.com': accept_all,
        'charitynavigator.org': accept_all,
        'charitywatch.org': accept_all,
    }

    def _parse(self, content, url, verbose=1):
        # Parse info
        links = {
            key: np.unique([
                result
                for regexp in regexps
                for result in re.findall(regexp, content, re.MULTILINE + re.IGNORECASE)
            ])
            for key, regexps in self.LINK_REGEXPS.items()
        }

        # Parse with beautifulsoup
        desired_links = defaultdict(lambda: [])
        try:
            for link in self.get_bs_html_obj(content, tags=['a']):
                if not link.has_attr('href'):
                    continue
                href = link['href'].strip()
                for domain, filter_fn in self.DOMAINS_OF_INTEREST.items():
                    if domain in href and filter_fn(href) and href not in desired_links[domain]:
                        desired_links[domain].append(href)
        except Exception as ex:  # noqa
            if verbose > 0:
                print("Exception while parsing {url}: {ex}".format(url=url, ex=ex))
            # Fall through and return current parse status.

        return desired_links, links

    def _get_id(self, url):
        if url is None:
            return None

        if 'facebook.com' in url.lower():
            exprs = [
                r'facebook.com.*/pages/[^#\?]+/([0-9]{7,})',
                r'facebook.com.*/pages/([^/#\?]+)',
                r'facebook.com.*/[^/#\?]+-([0-9]{7,})',
                r'facebook.com/home.php\#\!/([^/#\?\s!%]+)',
                r'facebook.com/([^/#\?\s!%]+)',
            ]
        elif 'twitter.com' in url.lower():
            exprs = [
                r'twitter.com/@?([^/#\?!\s\'"!%]+)',
                r'twitter.com/#!/@([^/#\?!\s\'"!%]+)',
            ]
        elif 'instagram.com' in url.lower():
            exprs = [r'instagram.com/([^/]+)']
        elif 'vimeo.com' in url.lower():
            exprs = [r'vimeo.com/([^/]+)']
        elif 'youtube.com' in url.lower():
            exprs = [
                r'youtube.com/user/([^/\?]+)',
                r'youtube.com/channel/([^/\?]+)',
                r'youtube.com/c/([^/\?]+)',
                r'youtube.com/((?!watch\?)[^/\?]+)',
            ]
        elif 'plus.google.com' in url.lower():
            exprs = [
                r'plus.google.com/u/0/\+?([^/\?\#]+)',
                r'plus.google.com/\+?([^/\?\#]+)',
            ]
        elif 'snapchat.com' in url.lower():
            exprs = [r'snapchat.com/add/([^/]+)']
        else:
            return None

        for expr in exprs:
            match = re.search(expr, url, re.IGNORECASE)
            if match:
                break
        return match.groups()[0] if match else None

    def _parse_pages(self, files, orig_urls, true_urls, verbose=1):

        # Parse links
        if verbose > 0:
            print("Parsing links...")

        links = []
        for li, (orig_url, page, true_url) in enumerate(zip(orig_urls, files, true_urls)):
            if (li + 1) % 50 == 0:
                print('Processed %d of %d pages.' % (li + 1, len(files)))

            cur_links = {
                'website': orig_url,
                'website_clean': true_url}

            if not page or not op.exists(page):
                # If the webpage doesn't exist, we still want to return the original URL to show we tried
                links.append(cur_links)
                continue

            with warnings.catch_warnings():
                warnings.simplefilter('ignore', TextParsingWarning)  # load files with best available encoding.
                content = text_from_disk(page, default='')

            parsed_links, _ = self._parse(content=content, url=true_url, verbose=verbose)

            for domain in self.DOMAINS_OF_INTEREST:
                domain_links = parsed_links[domain]
                if len(domain_links) == 0:
                    # No link
                    cur_links[domain] = None
                elif len(domain_links) == 1:
                    # One link - use it.
                    cur_links[domain] = domain_links[0]
                else:
                    # Many links; use minimum levenshtein distance to input url,
                    # Which is often a domain name.
                    dists = [levenshtein(dl, orig_url, case_sensitive=False) for dl in domain_links]
                    idx = np.argmin(dists)
                    cur_links[domain] = domain_links[idx]
                # Parse id from url
                # TODO make this way more principled and clean.
                cur_links[domain.replace('.com', '_id')] = self._get_id(cur_links[domain])
            links.append(cur_links)
        return links

    def fetch(self, urls, force=False, verbose=1, cache_on_fail=False):
        files, true_urls = self.fetcher.fetch(
            urls=urls, force=force, verbose=verbose,
            cache_on_fail=cache_on_fail)

        links = self._parse_pages(
            files=files, orig_urls=urls, true_urls=true_urls,
            verbose=verbose)

        df = pd.DataFrame(data=links)
        return df.applymap(to_ascii)

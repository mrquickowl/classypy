"""
Http Datasets.
"""
import os

import pandas as pd

from classypy.io.core.datasets.base import Dataset
from classypy.io.core.fetchers import HttpFetcher
from classypy.text import json_from_disk


class HttpDataset(Dataset):
    """
    Unauthenticated Http dataset.
    """
    fetcher_class = HttpFetcher


class AuthenticatedHttpDataset(HttpDataset):
    """
    Authenticated Http dataset.
    """
    fetcher_class = HttpFetcher

    USERNAME_ENV_VAR = None
    PASSWD_ENV_VAR = None

    def __init__(self, data_dir=None, username=None, passwd=None):
        """
        Initialize AuthenticatedHttpDataset.

        Parameters
        ----------
        data_dir: string
            Path of the data directory. Used to force data storage in a specified
            location.
        username: string, optional
            Username used for HTTP authentication.
        passwd: string, optional
            Password used for HTTP authentication.
        """
        username = username or os.environ.get(self.USERNAME_ENV_VAR)
        passwd = passwd or os.environ.get(self.PASSWD_ENV_VAR)
        if username is None or passwd is None:
            raise ValueError("username/passwd must be passed in, or %s/%s "
                             "environment variables must be set." % (
                                 self.USERNAME_ENV_VAR, self.PASSWD_ENV_VAR))
        self.username = username
        self.passwd = passwd

        super(AuthenticatedHttpDataset, self).__init__(
            data_dir=data_dir,
            username=self.username,
            passwd=self.passwd)


class HttpApiDataset(HttpDataset):
    """
    Unauthenticated Api Dataset.
    """
    COLUMN_MAP = {}

    @classmethod
    def response2dataframe(cls, response_file=None, data_array=None):
        assert data_array or response_file, 'Must specify object or file parameter.'
        if data_array is None:
            data_array = json_from_disk(response_file)

        return pd.DataFrame(data=data_array)

    @classmethod
    def _rename_df_cols(cls, df):
        """
        Rename a dataframe's columns.

        Parameters
        ----------
        df: pandas.Dataframe
            dataframe with columns to be renamed.

        Returns
        -------
        pandas.DataFrame with renamed columns.
        """
        return df.rename(columns=cls.COLUMN_MAP)

    def fetch(self, files, delete_archive=True, convert=True, **kwargs):
        """
        Retrieve and (optionally) convert files.

        files: list
            list of strings. Each string a file-path.
        delete_archive: bool, optional
            Whether or not to delete archive once it is uncompressed.
            (Default: True)
        convert: bool, optional
            If True, the files (which are assumed to be JSON objects) are converted to dataframes, and then returned as
            a dictionary where keys are the file location strings and values are the dataframes.
            If False, returns a list of strings to the file locations on disk
            (Default: True)
        kwargs: iterable
            Various keyword arguments to be fed to HTTPDataset.fetch().
        """
        files = super(HttpApiDataset, self).fetch(
            files=files, delete_archive=True, **kwargs)

        if convert:
            dfs = [self.response2dataframe(response_file=fil) for fil in files]
            dfs = [self._rename_df_cols(df) for df in dfs]
            return dict(zip(files, dfs))
        else:
            return files

"""
Core Dataset object.
"""
import inspect
import os
import os.path as op
import shutil

from classypy.io.core.fetchers import Fetcher
from classypy.io.core.objdep import ClassWithDependencies
from classypy.text import text_from_disk


def _readlinkabs(link):
    """
    Return an absolute path for the destination
    of a symlink
    """
    path = os.readlink(link)
    if op.isabs(path):
        return path
    return op.join(op.dirname(link), path)


def get_dataset_dir(dataset_name, data_dir=None, env_vars=[],
                    verbose=1):
    """Create if necessary and returns data directory of given dataset.

    Parameters
    ----------
    dataset_name: string
        The unique name of the dataset.
    data_dir: string, optional
        Path of the data directory. Used to force data storage in a specified
        location. Default: None
    env_vars: list of string, optional
        Add environment variables searched even if data_dir is not None.
    verbose: int, optional
        verbosity level (0 means no message).

    Returns
    -------
    data_dir: string
        Path of the given dataset directory.

    Notes
    -----
    This function retrieves the datasets directory (or data directory) using
    the following priority :
    1. the keyword argument data_dir
    3. the user environment variable CLASSYIO_PATH
    4. CLASSYIO_PATH in the user home folder
    """
    # We build an array of successive paths by priority
    paths = []

    # Search given environment variables
    for env_var in env_vars:
        env_data = os.getenv(env_var, '')
        paths.extend(env_data.split(':'))

    # Check data_dir which force storage in a specific location
    if data_dir is not None:
        paths = data_dir.split(':')
    else:
        local_data = os.getenv('CLASSYIO_PATH')
        if local_data is not None:
            paths.extend(local_data.split(':'))

        paths.append(op.expanduser('~/classyio_data'))

    if verbose > 2:
        print('Dataset search paths: %s' % paths)

    # Check if the dataset exists somewhere
    for path in paths:
        path = op.join(path, dataset_name)
        if op.islink(path):
            # Resolve path
            path = _readlinkabs(path)
        if op.exists(path) and op.isdir(path):
            if verbose > 1:
                print('\nDataset found in %s\n' % path)
            return path

    # If not, create a folder in the first writeable directory
    errors = []
    for path in paths:
        path = op.join(path, dataset_name)
        if not op.exists(path):
            try:
                os.makedirs(path)
                if verbose > 0:
                    print('\nDataset created in %s\n' % path)
                return path
            except Exception as exc:  # noqa
                short_error_message = getattr(exc, 'strerror', str(exc))
                errors.append('\n -{0} ({1})'.format(
                    path, short_error_message))

    raise OSError('Classypy tried to store the dataset in the following '
                  'directories, but:' + ''.join(errors))


class Dataset(ClassWithDependencies):
    """
    Dataset class with dependencies.
    """
    dependencies = []
    fetcher_class = Fetcher

    @classmethod
    def get_dataset_descr_path(cls):
        """
        Find the RST file for the current dataset class.
        """
        class_path = op.dirname(inspect.getfile(cls))

        if op.exists(op.join(class_path, 'README.rst')):
            return op.join(class_path, 'README.rst')

        name = op.basename(class_path)
        return op.join(class_path, name + '.rst')

    @classmethod
    def get_dataset_descr(cls):
        """
        Return the documentation for this class.
        """
        rst_path = cls.get_dataset_descr_path()
        try:
            return text_from_disk(rst_path)
        except IOError:
            return ''

    def __init__(self, data_dir=None, **kwargs):
        """
        Initialize Dataset with dependencies class.

        Parameters
        ----------
        data_dir: string, optional
            Path of the data directory. Used to force data storage in a specified
            location. Default: None
        kwargs:
            Additional kwarg arguments to feed to fetcher class.
        """
        super(Dataset, self).__init__()
        class_path = op.dirname(inspect.getfile(self.__class__))
        self.name = op.basename(class_path)
        self.modality = op.basename(op.dirname(class_path))  # assume
        self.description = self.get_dataset_descr()
        self.data_dir = get_dataset_dir(self.name, data_dir=data_dir)
        if self.fetcher_class:
            self.fetcher = self.fetcher_class(data_dir=self.data_dir, **kwargs)  # set to *something*.

        # Feeling lazy... handle this here, for now.
        if self.__class__.__doc__ is None:
            self.__class__.__doc__ = self.description

    def clean_data_directory(self):
        """
        Empties the data directory for the dataset
        """
        if os.path.exists(self.data_dir):

            shutil.rmtree(self.data_dir)
        os.makedirs(self.data_dir)

    def fetch(self, force=False, verbose=1, *args, **kwargs):
        """
        Fetch method for Dataset class.

        Parameters
        ----------
        force: bool, optional
            If true, fetch the files even if they already exist
        verbose: int, optional
            verbosity level (0 means no message).
        """
        if force:
            self.clean_data_directory()
        return self.fetcher.fetch(verbose=verbose, *args, **kwargs)

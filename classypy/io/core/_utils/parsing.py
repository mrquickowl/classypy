"""
Class for BeautifulSoup Parsers.
"""
import warnings

import six

from classypy.io.core.objdep import ClassWithDependencies
from classypy.util.etl import to_ascii


class BeautifulSoupParser(ClassWithDependencies):
    """
    Class to create BeautifulSoup parsers.
    """
    dependencies = dict(bs4='beautifulsoup4', lxml='lxml', chardet='chardet', html5lib='html5lib')

    @classmethod
    def get_bs_obj(cls, content, parser, encoding=None, tags=None, throw_errors=False):
        """
        Template for creating a BeautifulSoup object with a specified parser.

        Parameters
        ----------
        content: string
            Document to be transformed.
        parser: string
            Name of parser.
            Options: ["html.parser", "lxml", "lxml-xml". "xml", "html5lib"]
        encoding: string (optional)
            Sets encoding. Default is detected from `content`,
            or utf-8 if detection fails.
        tags: list
            Optional list of tags.
            If not None, only those tags will be parsed.
        throw_errors: Boolean (default=False)
            Whether or not to pass decoding error to caller.

        Returns
        -------
            BeautifulSoup object: soup fit to the selected content.
                If an error is raised, an empty list is returned.
        """
        import chardet
        from bs4 import BeautifulSoup, SoupStrainer

        # Get encoding
        content = to_ascii(content)

        if content and six.PY2:
            encoding = encoding or chardet.detect(content)['encoding']
        encoding = encoding or 'utf-8'

        # Get kwargs
        kwargs = {}
        if tags:
            kwargs['parse_only'] = SoupStrainer(tags)

        try:
            # Try eliminating strange encoding-specific characters,
            # by decoding the encoding and ignoring any errors.
            return BeautifulSoup(to_ascii(content or ""), features=parser, **kwargs)
        except ValueError as ex:
            if throw_errors:
                # Allow option for parser to pass any decoding error
                # back to the caller.
                raise

            # If there is a decoding error, we're pretty much in trouble.
            # So just return any empty iterable.
            warnings.warn("Failed to create BeautifulSoup parser: {err}".format(err=ex))
            return BeautifulSoup("", parser, **kwargs)

    @classmethod
    def get_bs_html_obj(cls, content, tags=None, throw_errors=False, encoding=None):
        """
        BeautifulSoup object with html parser.
        """
        return cls.get_bs_obj(
            content=content, parser='html.parser', tags=tags,
            throw_errors=throw_errors, encoding=encoding)

    @classmethod
    def get_bs_xml_obj(cls, content, tags=None, throw_errors=False, encoding=None):
        """
        BeautifulSoup object with xml parser.
        """
        return cls.get_bs_obj(
            content=content, parser='xml', tags=tags,
            throw_errors=throw_errors, encoding=encoding)

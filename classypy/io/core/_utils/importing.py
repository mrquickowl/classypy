"""
Functions for identifying and importing modules.
"""
import os
import os.path as op


def _is_module_dir(dir_path):
    """
    Identify if dir_path is a module directory.

    Parameters
    ----------
    dir_path: string
        File-path to directory.

    Returns
    -------
        Boolean: Whether or not dir_path is a module directory or not.
    """
    return op.isdir(dir_path) and op.exists(op.join(dir_path, '__init__.py'))  # nosec


def _get_all_subdirs(dir_path):
    """
    Return all module subdirectories inside dir_path.

    Parameters
    ----------
    dir_path: string
        File-path to directory.

    Returns
    -------
        list: List of modules in dir_path.
    """
    all_paths = (op.join(dir_path, fil) for fil in os.listdir(dir_path))
    all_modules = [p for p in all_paths if _is_module_dir(p)]
    all_module_names = (op.basename(fil) for fil in all_modules)
    return all_module_names


def import_all_submodules(dir_path, locals, globals, recursive=True):
    """
    Import all submodules inside of dir_path.

    Parameters
    ----------
    dir_path: string
        File-path to directory.
    locals: dict
        Dictionary of local variables to execute with import.
    globals: dict
        Dictionary of global variables to execute with import.
    recursive: Boolean (default=True)
        If True, use recursive import.
    """
    subdirs = _get_all_subdirs(dir_path)
    for subdir in subdirs:
        if recursive:
            exec('from .{subdir} import *'.format(subdir=subdir), globals, locals)  # nosec
        else:
            exec('from . import {subdir}'.format(subdir=subdir), globals, locals)  # nosec

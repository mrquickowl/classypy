"""
Utilities to download NeuroImaging datasets

Author: Alexandre Abraham, Philippe Gervais
License: simplified BSD
"""
import collections
import hashlib
import os
import os.path as op
import re
import sys
import time

from six import string_types

from ....compat import _urllib, md5_hash
from ..objdep import ClassWithDependencies


def format_time(t):
    """
    Format time to minutes/seconds.
    """
    if t > 60:
        return "%4.1fmin" % (t / 60.)
    else:
        return " %5.1fs" % (t)


def md5_sum_file(path):
    """
    Calculate MD5 sum of a file.

    Parameters
    ----------
    path: string
        File-path to MD5 checksum file.

    Returns
    -------
    MD5 sum of `path`.
    """
    with open(path, 'rb') as f:
        m = hashlib.md5()
        while True:
            data = f.read(8192)
            if not data:
                break
            m.update(data)
    return m.hexdigest()


def chunk_report(bytes_so_far, total_size, initial_size, t0):
    """
    Report progress of the upload.
    In this case, `upload` refers to the retrieval and storage of an object from S3 to a file.

    For more information, see the `cb` parameter of the `get_contents_to_filename` method
    at the following link: http://boto.cloudhackers.com/en/latest/ref/s3.html

    Parameters
    ----------
    bytes_so_far: int
        Number of downloaded bytes successfuly transimtted to S3.

    total_size: int
        Total size of the file/transmitted object (may be 0/None, depending on download method).
    t0: int
        The time in seconds (as returned by time.time()) at which the
        download was resumed / started.
    initial_size: int
        If resuming, indicate the initial size of the file.
        If not resuming, set to zero.
    """
    if not total_size:
        sys.stderr.write("Downloaded %d of ? bytes\r" % (bytes_so_far))

    else:
        # Estimate remaining download time
        total_percent = float(bytes_so_far) / total_size

        current_download_size = bytes_so_far - initial_size
        bytes_remaining = total_size - bytes_so_far
        dt = time.time() - t0
        download_rate = current_download_size / max(1e-8, float(dt))
        # Minimum rate of 0.01 bytes/s, to avoid dividing by zero.
        time_remaining = bytes_remaining / max(0.01, download_rate)

        # Trailing whitespace is to erase extra char when message length
        # varies
        sys.stderr.write(
            "Downloaded %d of %d bytes (%0.2f%%, %s remaining)  \r"
            % (bytes_so_far, total_size, total_percent * 100,
               format_time(time_remaining)))


def construct_url(url, params=None):
    """Create a URL from querystring params."""
    if params is not None:
        querystring_args = _urllib.parse.urlencode(params)
        url += ('&' if '?' in url else '?') + querystring_args
    return url


class Fetcher(ClassWithDependencies):
    """
    Base class for Fetcher objects.
    """
    dependencies = []

    def __init__(self, data_dir=None, verbose=1):
        """
        Initialize Fetcher class.

        Parameters
        ----------
        data_dir: string, optional
            Path of the data directory.
        verbose: int, optional
            verbosity level (0 means no message).
        """
        self.data_dir = data_dir or os.environ.get('CLASSYIO_PATH',
                                                   'classyio_data')
        if verbose > 0 and not op.exists(self.data_dir):
            print("Files will be downloaded to %s" % self.data_dir)

    @classmethod
    def construct_url(cls, url, params=None):
        """Create a URL from querystring params."""
        return construct_url(url, params=params)

    @classmethod
    def url2file(cls, url):
        """Convert url to a local filename for caching."""
        file_name = re.sub(r'^.*://(.*)$', r'/\1', url)  # remove protocol
        file_name = re.sub(r'[/]', '', file_name)  # remove forward slashes.
        if len(file_name) > 128 or len(file_name) < 3 or re.search(r'[?@+&^%#]', file_name):
            # too long, or not safe (e.g. DPLAT-1172 '.'); hash it to avoid errors.
            file_name = md5_hash(file_name)
        return file_name

    @classmethod
    def reformat_downloads(cls, downloads):
        """
        Take an iterable, and put it into the expected format of
        a tuple if triplet tuples.

        Parameters
        ----------
        downloads : iterable (list, tuple, generator)
          Any of the following are acceptable.
            => [src_url, ...]
            => [(dest_file, src_url), ...]
            => [(dest_file, src_url, options_dict), ...]

        Returns
        -------
        list of 3-tuples: [(dest_file, src_url, options_dict), ...]
        """
        def _strip_file_prefix(downloads):
            """
            Take an iterable, modify file destinations to strip shared prefixes.
            If there's only a single download, strip off everything that's not the base file name.

            Parameters
            ----------
            downloads : iterable (list, tuple, generator)
              Any of the following are acceptable.
                => [(dest_file, src_url), ...]
                => [(dest_file, src_url, options_dict), ...]

            Returns
            -------
            list of 3-tuples: [(dest_file, src_url, options_dict), ...]
            """
            if len(downloads) == 1:
                if '/' in downloads[0][0]:
                    downloads[0] = list(downloads[0])  # convert to list
                    downloads[0][0] = op.basename(downloads[0][0])
            else:
                prefix = op.commonprefix([d[0] for d in downloads])
                for di in range(len(downloads)):
                    downloads[di] = list(downloads[di])  # convert to list
                    file_path = downloads[di][0]
                    file_name = op.basename(file_path)
                    if len(file_path) - len(prefix) < len(file_name):
                        # Avoid removing any characters from the base filename
                        downloads[di][0] = file_name
                    else:
                        downloads[di][0] = file_path[len(prefix):]
            return downloads

        # Make sure every element has an output destination.
        needs_dest = [isinstance(tup, string_types) for tup in downloads]
        downloads = [[cls.url2file(tup), tup] if needs_dest[ti] else tup for ti, tup in enumerate(downloads)]

        # Categorize outputs and manipulate ones that need it.
        downloads = _strip_file_prefix(downloads)

        # Clean files where needed
        out_files = []
        for download in downloads:
            if not isinstance(download, collections.Iterable):
                raise ValueError("Unexpected format: %s" % str(download))
            elif len(download) == 2:  # (dest, src)
                out_files.append((download[0], download[1], dict()))
            elif len(download) == 3:  # (dest, src, opts)
                out_files.append(download)
            else:
                raise ValueError("Unexpected format: %s" % str(download))

        # Strip special chars
        return out_files

    def fetch(self, files, force=False, verbose=1):
        """
        Abstract fetch method.

        files: list of (string, string, dict)
            List of files and their corresponding url.
        force: bool, optional
            If true, fetch the files even if they already exist
        verbose: int, optional
            verbosity level (0 means no message).
        """
        raise NotImplementedError()

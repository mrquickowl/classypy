from .aws_fetcher import AmazonS3Fetcher
from .base import Fetcher
from .http_fetcher import HttpApiFetcher, HttpFetcher
from .requests_fetcher import RequestsFetcher
from .webpage_fetcher import WebpageFetcher

__all__ = [
    'AmazonS3Fetcher', 'Fetcher', 'HttpApiFetcher', 'HttpFetcher',
    'RequestsFetcher', 'WebpageFetcher',
]

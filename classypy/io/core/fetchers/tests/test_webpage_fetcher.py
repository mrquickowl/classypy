from classypy.io.core.fetchers import WebpageFetcher


def test_clean_url():
    # None
    assert WebpageFetcher.clean_url('N/A') is None
    assert WebpageFetcher.clean_url('"N/A"') is None

    # Fix breaks
    assert 'http://cnn.com/' == WebpageFetcher.clean_url('HTTP//cnn.com/')
    assert 'http://cnn.com/' == WebpageFetcher.clean_url('HTTP:/cnn.com/')
    assert 'https://cnn.com/' == WebpageFetcher.clean_url('HTTPS:/cnn.com/')

    # Case sensitive
    assert 'https://CNN.com/testMeOut' == WebpageFetcher.clean_url('https:/CNN.com/testMeOut')

    assert 'http://www.emtta.org/' == WebpageFetcher.clean_url('htt://www.emtta.org/')


def test_generate_urls():
    urls, exceptions = WebpageFetcher.generate_candidate_urls("CNN.COM")
    assert len(urls) == 8
    assert [url == url.lower() for url in urls[:4]], "lowercase first"
    assert [url != url.lower() for url in urls[5:]], "uppercase last"

    urls, exceptions = WebpageFetcher.generate_candidate_urls("cnn.com")
    assert len(urls) == 4
    assert all([url == url.lower() for url in urls])

    urls, exceptions = WebpageFetcher.generate_candidate_urls("NO WEBSITE")
    assert len(urls) == 0

from classypy.io.core.fetchers import HttpApiFetcher


def test_http_api_fetcher_in_parts():
    # DI-586: make sure ids are fetched and output in order, so that
    # output rezipping of data to ids gives the correct result.
    def fetch_fn(ids):
        return ids

    ids = list(range(1000))
    id_dict = HttpApiFetcher().fetch_in_parts(ids=ids, max_ids_per_request=2)
    assert list(id_dict.keys()) == list(id_dict.values())

from classypy.io.core.fetchers.base import Fetcher


def test_path_expansion():
    mappings = [
        # URLs
        {'downloads': ['http://cnn.com/'], 'files': ['cnn.com']},
        {'downloads': ['/my/path'], 'files': ['mypath']},
        {'downloads': ['http://cnn.com/', '/my/path'], 'files': ['cnn.com', 'mypath']},
        {'downloads': ['http://cnn.com/?doc'], 'files': ['dd55ca6d7a281cbe3c3c457da525f296']},

        # Files
        {'downloads': [('test', 'http://cnn.com/')], 'files': ['test']},
        {'downloads': [('/my/path', 'http://test')], 'files': ['path']},
        {'downloads': [('test', 'http://cnn.com/'), ('/my/path', 'http://test')],
         'files': ['test', '/my/path']},

        # Combinations
        {'downloads': ['http://cnn.com/', ('test', 'http://cnn.com/')], 'files': ['cnn.com', 'test']},
    ]

    for test_case in mappings:
        rv = Fetcher.reformat_downloads(test_case['downloads'])
        for fil, proj, expected_proj in zip(test_case['downloads'], rv, test_case['files']):
            assert proj[0] == expected_proj, test_case

import time

from classypy.io.core.fetchers.base import Fetcher
from classypy.util.execution import run_in_parallel


class RequestsFetcher(Fetcher):
    """Fetcher class using requests to fetch via HTTP"""
    dependencies = Fetcher.add_dependencies(requests='requests[security]')

    def __init__(self, data_dir=None, username=None, passwd=None, login_url=None, auth=None):
        """
        Initialize HTTPFetcher

        Parameters
        ----------
        data_dir: string
            Path of the data directory. Used to force data storage in a specified
            location.
        username: string, optional
            Username used for HTTP authentication
        passwd: string, optional
            Password used for HTTP authentication
        login_url : string, optional
            Login URL used to start a session
        auth : requests.auth, optional
            An authentication object to be used by requests
        """
        super(RequestsFetcher, self).__init__(data_dir=data_dir)
        self.username = username
        self.passwd = passwd

        if login_url is not None and not login_url.startswith('https'):
            raise ValueError('Login URL must be protected by https encryption.')
        self.login_url = login_url  # for sessions
        self.session = None  # for sessions

        if auth is not None and any([self.username is not None, self.passwd is not None, self.session is not None]):
            raise ValueError('If auth is provided, all other authentication arguments should be None.')
        self.auth = auth

    def _needs_session(self):
        """True if we have data to get a session, but there's no session yet."""
        return (self.username is not None and
                self.passwd is not None and
                self.login_url is not None and
                self.session is None)

    def _start_session(self, cookies=None, headers=None):
        """Start a session."""
        import requests

        self.session = requests.Session()
        self.session.cookies.update(cookies or {
            'sessionid': '',
            'mid': '',
            'ig_pr': '1',
            'ig_vw': '1920',
            'csrftoken': '',
            's_network': '',
            'ds_user_id': ''
        })
        self.session.headers.update(headers or {
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': "en",
            'Connection': 'keep-alive',
            'User-Agent': ("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 "
                           "(KHTML, like Gecko) Chrome/48.0.2564.103 Safari/537.36"),
            'X-Requested-With': 'XMLHttpRequest'
        })

        login_post = {
            'username': self.username,
            'password': self.passwd
        }

        r = self.session.get(self.login_url)
        self.session.headers.update({'X-CSRFToken': r.cookies['csrftoken']})
        login = self.session.post(self.login_url, data=login_post, allow_redirects=True)
        self.session.headers.update({'X-CSRFToken': login.cookies['csrftoken']})

    def _end_session(self):
        """End a session."""
        # TODO: add explicit logout
        self.session = None

    def fetch_url(self, url, error_codes_to_retry=[], retries=0, time_between_retries=1, verbose=1, **kwargs):
        """Fetch a single url.

        Parameters
        ----------
        url : str
            The URL to fetch.
        error_codes_to_retry : list of ints (Default: [])
            Status codes to retry fetching for.
        retries : int (Default: 0)
            The number of times to retry fetching, if you can an error code from error_codes_to_retry.
        time_between_retries : int (Default: 1)
            The number of seconds to wait before retrying.
        verbose : int (Default: 1)
            Level of verbosity
        kwargs : additional keyword arguments for requests
        """
        import requests

        if verbose > 1:
            print("Fetching {url}".format(url=url))

        if self._needs_session():
            self._start_session(cookies=kwargs.get('cookies'), headers=kwargs.get('headers'))

        data, ex = None, None
        try:
            if self.session:
                # Session-based auth
                data = self.session.get(url)
            elif self.username:
                # Basic auth
                if not url.startswith('https'):
                    raise ValueError('URL using basic authentication must be protected by https encryption.')
                data = requests.get(url, auth=(self.username, self.passwd), **kwargs)
            elif self.auth:
                # Provided auth object
                data = requests.get(url, auth=self.auth)
            else:
                # No auth
                data = requests.get(url, **kwargs)

            data.raise_for_status()

        except Exception as err:  # noqa
            if type(err) == requests.exceptions.HTTPError:
                # Some services return status codes that tell us a retry might work
                if retries > 0 and err.response.status_code in error_codes_to_retry:
                    time.sleep(time_between_retries)

                    return self.fetch_url(
                        url=url,
                        error_codes_to_retry=error_codes_to_retry,
                        retries=retries - 1,
                        time_between_retries=time_between_retries,
                        verbose=verbose,
                        **kwargs
                    )

            if verbose > 0:
                # Only print the exception if you aren't going to retry
                print("Exception fetching {url}: {ex}".format(url=url, ex=err))

            ex = err

        return data, ex

    def fetch(self, urls, verbose=1, retries=0, codes_to_retry=[], **kwargs):
        """Fetch many urls."""
        outputs = run_in_parallel(
            self.fetch_url,
            params=[
                dict(
                    url=url,
                    verbose=verbose,
                    retries=retries,
                    codes_to_retry=codes_to_retry,
                    **kwargs)
                for url in urls
            ])

        # Redistribute outputs, for expected format
        data = [data for data, _ in outputs]
        exceptions = [exceptions for _, exceptions in outputs]
        return data, exceptions

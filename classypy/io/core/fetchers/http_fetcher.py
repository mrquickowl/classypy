"""
HTTPFetcher definition and helper functions.
"""
import base64
import gzip
import hashlib
import os
import os.path as op
import re
import shutil
import sys
import tarfile
import time
import zipfile
from collections import OrderedDict

import numpy as np

from classypy.compat import _urllib
from classypy.compat import b as bytes
from classypy.compat import cPickle, md5_hash
from classypy.devops import logger
from classypy.io.core.fetchers.base import Fetcher, chunk_report, md5_sum_file
from classypy.text import text_from_disk, text_to_disk
from classypy.util.execution import run_in_parallel


class _PreemptiveBasicAuthHandler(_urllib.request.HTTPBasicAuthHandler):
    """Preemptive basic auth.

    Instead of waiting for a 403 to then retry with the credentials,
    send the credentials if the url is handled by the password manager.
    Note: please use realm=None when calling add_password.

    See: https://stackoverflow.com/a/24048852
    """
    def http_request(self, req):
        url = req.get_full_url()
        realm = None
        # this is very similar to the code from retry_http_basic_auth()
        # but returns a request object.
        user, pw = self.passwd.find_user_password(realm, url)
        if pw:
            raw = "%s:%s" % (user, pw)
            b64_encoded = base64.b64encode(bytes(raw)).decode('utf-8')
            auth = 'Basic {b64_encoded}'.format(b64_encoded=b64_encoded)
            req.add_unredirected_header(self.auth_header, auth)
        return req

    https_request = http_request


def movetree(src, dst):
    """
    Move an entire tree to another directory.
    Any existing file is overwritten.

    Parameters
    ----------
    src: string
        Source directory.
    dst: string
        Destination directory.
    """
    names = os.listdir(src)

    # Create destination dir if it does not exist
    if not op.exists(dst):
        os.makedirs(dst)
    errors = []

    for name in names:
        srcname = op.join(src, name)
        dstname = op.join(dst, name)
        try:
            if op.isdir(srcname) and op.isdir(dstname):
                movetree(srcname, dstname)
                os.rmdir(srcname)
            else:
                shutil.move(srcname, dstname)
        except (IOError, os.error) as why:
            errors.append((srcname, dstname, str(why)))
        # catch the Error from the recursive movetree so that we can
        # continue with other files
        except Exception as err:  # noqa
            errors.extend(err.args[0])
    if errors:
        raise Exception(errors)


def _chunk_read_(response, local_file, chunk_size=8192, report_hook=None,
                 initial_size=0, total_size=None, verbose=1):
    """
    Download a file chunk by chunk and show advancement

    Parameters
    ----------
    response: _urllib.response.addinfourl
        Response to the download request in order to get file size
    local_file: file
        Hard disk file where data should be written
    chunk_size: int, optional
        Size of downloaded chunks. Default: 8192
    report_hook: bool
        Whether or not to show downloading advancement. Default: None
    initial_size: int, optional
        If resuming, indicate the initial size of the file
    total_size: int, optional
        Expected final size of download (None means it is unknown).
    verbose: int, optional
        verbosity level (0 means no message).

    Returns
    -------
    data: string
        The downloaded file.
    """
    if total_size is None:
        total_size = response.info().get('Content-Length', response.headers.get('content-length'))
        if total_size is not None:
            total_size = total_size.strip()
    try:
        total_size = int(total_size) + initial_size
    except Exception as ex:  # noqa
        if verbose > 1:
            print("Warning: total size could not be determined.")
            if verbose > 2:
                print("Full stack trace: %s" % ex)
        total_size = None
    bytes_so_far = initial_size

    t0 = time.time()
    while True:
        chunk = response.read(chunk_size)
        bytes_so_far += len(chunk)

        if not chunk:
            if report_hook:
                sys.stderr.write('\n')
            break

        local_file.write(chunk)
        if report_hook:
            chunk_report(bytes_so_far, total_size, initial_size, t0)

    return


def _uncompress_file(file_, delete_archive=True, verbose=1):
    """
    Uncompress files contained in a data_set.

    Parameters
    ----------
    file: string
        path of file to be uncompressed.
    delete_archive: bool, optional
        Whether or not to delete archive once it is uncompressed.
        (Default: True)
    verbose: int, optional
        verbosity level (0 means no message).

    Returns
    -------
    filename: string
        Name of uncompressed file.

    Notes
    -----
    This handles zip, tar, gzip and bzip files only.
    """
    if verbose > 0:
        print('Extracting data from %s...' % file_)
    data_dir = op.dirname(file_)

    # We first try to see if it is a zip file
    try:
        filename, ext = op.splitext(file_)
        if ext == '':
            # Avoid overwriting downloaded file.
            filename = filename + '.uncompressed'

        with open(file_, "rb") as fd:
            header = fd.read(4)

        processed = False
        if zipfile.is_zipfile(file_):
            with zipfile.ZipFile(file_) as z:
                z.extractall(data_dir)
            processed = True
            filename = None  # We don't know what it was unzipped to... or if there are multiple files.
            ext = None
        elif ext == '.gz' or header.startswith(b'\x1f\x8b'):
            if ext == '.tgz':
                filename = filename + '.tar'

            with gzip.open(file_) as gz:
                with open(filename, 'wb') as out:
                    shutil.copyfileobj(gz, out, 8192)

            # If file is .tar.gz, this will be handle in the next case
            if delete_archive:
                os.remove(file_)
            file_ = filename
            if ext != '':
                filename, ext = op.splitext(file_)
            processed = False

        if tarfile.is_tarfile(file_):
            with tarfile.open(file_, "r") as tar:
                tar.extractall(path=data_dir)
            processed = True

        if not processed and verbose > 0:
            print("WARNING: [Uncompress] unknown archive file format: "
                  "%s" % file_)
            return file_
        if delete_archive and file_ != filename:
            os.remove(file_)
        if verbose > 0:
            print('   ...done.')

    except Exception as ex:  # noqa
        if verbose > 0:
            print('Error uncompressing file: %s' % ex)
        raise

    return filename


def _fetch_file(url, data_dir, resume=True, overwrite=False,
                md5sum=None, username=None, passwd=None,
                handlers=None, headers=None, cookies=None,
                data=None, verbose=1, timeout=30, output_encoding=None):
    """
    Load requested file, downloading it if needed or requested.

    Parameters
    ----------
    url: string
        Contains the url of the file to be downloaded.
    data_dir: string
        Path of the data directory. Used to force data storage in a specified
        location.
    resume: bool, optional
        If true, try to resume partially downloaded files
    overwrite: bool, optional
        If true and file already exists, delete it.
    md5sum: string, optional
        MD5 sum of the file. Checked if download of the file is required
    username: string, optional
        Username used for HTTP authentication
    passwd: string, optional
        Password used for HTTP authentication
    handlers: list of BaseHandler, optional
        urllib handlers passed to urllib.request.build_opener. Used by
        advanced users to customize request handling.
    headers: dictionary, optional
        specifying headers
    cookies: dictionary, optional
        specifying cookies
    data: dictionary, optional
        specifying data for a POST request.
    verbose: int, optional
        verbosity level (0 means no message).
    timeout: int, optional
        specifies a timeout in seconds for blocking operations like the connection attempt
    output_encoding: str, optional
        specifies an encoding to convert data to, on disk (default: None; no conversion)

    Returns
    -------
    files: string
        Absolute path of downloaded file.

    Notes
    -----
    If, for any reason, the download procedure fails, all downloaded files are
    removed.
    """
    def _convert_encoding(response_data, path, output_encoding):
        input_encoding = re.search(r'charset=([^\s;]+)', response_data.info().get("Content-Type"))
        input_encoding = input_encoding and input_encoding.groups()[0].lower()

        if verbose > 0:
            print("converting {input_encoding} to {output_encoding}".format(
                input_encoding=input_encoding, output_encoding=output_encoding))

        # DPLAT-1656: Even if encodings are the same, don't trust them;
        # load/re-save content to normalize.
        content = text_from_disk(path=path, disk_encoding=input_encoding)
        text_to_disk(content, path=path, encoding=output_encoding)

    if handlers is None:
        handlers = []
    if headers is None:
        headers = dict(),
    if cookies is None:
        cookies = dict()
    if data is not None and isinstance(data, dict):
        data = _urllib.parse.urlencode(data)

    # Determine data path
    if not op.exists(data_dir):
        os.makedirs(data_dir)

    # Determine filename using URL
    parse = _urllib.parse.urlparse(url)
    file_name = op.basename(parse.path)
    if file_name == '':
        file_name = md5_hash(parse.path)

    temp_file_name = file_name + ".part"
    full_name = op.join(data_dir, file_name)
    temp_full_name = op.join(data_dir, temp_file_name)
    if op.exists(full_name):
        if overwrite:
            os.remove(full_name)
        else:
            return full_name
    if op.exists(temp_full_name):
        if overwrite:
            os.remove(temp_full_name)
    t0 = time.time()
    local_file = None
    initial_size = 0

    try:
        # Download data
        if username:
            # Make sure we're secure, basic auth is unencrypted
            if parse.scheme and parse.scheme != 'https':
                raise ValueError("Specifying username currently requires using"
                                 " a secure (https) URL (%s)." % url)
            password_mgr = _urllib.request.HTTPPasswordMgrWithDefaultRealm()
            password_mgr.add_password(realm=None, uri=url, user=username, passwd=passwd)
            # Don't append, don't want to update caller's list with this!
            handlers = ([_urllib.request.HTTPSHandler(debuglevel=max(0, verbose - 2)),
                         _PreemptiveBasicAuthHandler(password_mgr)] +
                        handlers)
        url_opener = _urllib.request.build_opener(*handlers)

        # Prep the request (add headers, cookies)
        request = _urllib.request.Request(url, data=data)
        request.add_header('Connection', 'Keep-Alive')
        if cookies:
            if 'Cookie' in headers:
                headers['Cookie'] += ';'
            else:
                headers['Cookie'] = ''
            headers['Cookie'] += ';'.join(['%s=%s' % (k, v)
                                           for k, v in cookies.items()])
        for header_name, header_val in headers.items():
            request.add_header(header_name, header_val)

        if verbose > 0:
            print('Downloading data from %s ...' % url)
        if not resume or not op.exists(temp_full_name):
            # Simple case: no resume
            response_data = url_opener.open(request, timeout=timeout)
            local_file = open(temp_full_name, "wb")
        else:
            # Complex case: download has been interrupted, we try to resume it.
            local_file_size = op.getsize(temp_full_name)
            # If the file exists, then only download the remainder
            request.add_header("Range", "bytes=%s-" % (local_file_size))
            try:
                response_data = url_opener.open(request)
                content_range = response_data.info().get('Content-Range')
                if (content_range is None or not content_range.startswith(
                        'bytes %s-' % local_file_size)):
                    raise IOError('Server does not support resuming')
            except Exception as ex:  # noqa
                # A wide number of errors can be raised here. HTTPError,
                # URLError... I prefer to catch them all and rerun without
                # resuming.
                if verbose > 0:
                    print('Resuming failed ({ex}), try to download the whole file.'.format(ex=ex))
                return _fetch_file(
                    url, data_dir, resume=False, overwrite=overwrite,
                    md5sum=md5sum, username=username, passwd=passwd,
                    handlers=handlers, headers=headers, cookies=cookies,
                    data=data, verbose=verbose, timeout=timeout, output_encoding=output_encoding)
            else:
                local_file = open(temp_full_name, "ab")
                initial_size = local_file_size

        # Download the file.
        _chunk_read_(response_data, local_file, report_hook=(verbose > 1),
                     initial_size=initial_size, verbose=verbose)

        # temp file must be closed prior to the move
        if not local_file.closed:
            local_file.close()

        if output_encoding is not None:
            _convert_encoding(response_data, path=temp_full_name, output_encoding=output_encoding)

        shutil.move(temp_full_name, full_name)
        dt = time.time() - t0
        if verbose > 0:
            print('...done. (%i seconds, %i min)' % (dt, dt // 60))
    except _urllib.error.HTTPError as e:
        if verbose > 0:
            print("Error while fetching file %s. "
                  "Dataset fetching aborted." % (file_name))
        if verbose > 1:
            print("HTTP Error: %s, %s" % (e, url))
        raise
    except _urllib.error.URLError as e:
        if verbose > 0:
            print("Error while fetching file %s. "
                  "Dataset fetching aborted." % (file_name))
        if verbose > 1:
            print("URL Error: %s, %s" % (e, url))
        raise
    finally:
        if local_file is not None and not local_file.closed:
            local_file.close()
    if md5sum is not None:
        if (md5_sum_file(full_name) != md5sum):
            raise ValueError("File %s checksum verification has failed."
                             " Dataset fetching aborted." % local_file)
    return full_name


def fetch_files(data_dir, files, resume=True, force=False, verbose=1,
                delete_archive=True, skip_errors=False,
                clean_on_error=None, retries=1):
    """
    Load requested dataset, downloading it if needed or requested.

    This function retrieves files from the hard drive or download them from
    the given urls. Note to developers: All the files will be first
    downloaded in a sandbox and, if everything goes well, they will be moved
    into the folder of the dataset. This prevents corrupting previously
    downloaded data. In case of a big dataset, do not hesitate to make several
    calls if needed.

    Parameters
    ----------
    data_dir: string
        Path of the data directory. Used to force data storage in a specified
        location.
    files: list of (string, string, dict)
        List of files and their corresponding url. The dictionary contains
        options regarding the files. Options supported are:
            'md5sum' to check the md5 sum of the file.
            'move' if renaming the file or moving it to a subfolder is needed.
            'retries' to indicate that a number of retries before download failure.
            'uncompress' to indicate that the file is an archive.
            'username' used for HTTP authentication
            'passwd' used for HTTP authentication
            'handlers' urllib handlers passed to urllib.request.build_opener. Used by
                advanced users to customize request handling.
            'headers' dictionary, specifying headers
            'cookies' dictionary, specifying cookies
            'data' dictionary, specifying data for a POST request.
            'timeout' specifies a timeout in seconds used by urllib
    resume: bool, optional
        If true, try resuming download if possible
    force: bool, optional
        If true, fetch the files even if they already exist
    verbose: int, optional
        verbosity level (0 means no message).
    delete_archive: bool, optional
        If true, delete archives of compressed files after uncompression.
    skip_errors: bool, optional
        If true, ignore errors while downloading.
    clean_on_error: bool, optional
        If true, removes download directories on an error.
    retries: int, optional
        # of retries of a file download before failing (default: 1)

    Returns
    -------
    files: list of string
        Absolute paths of downloaded files on disk
    """
    # We may be in a global read-only repository. If so, we cannot
    # download files.
    if not os.access(data_dir, os.W_OK):
        raise ValueError('Dataset files are missing but dataset'
                         ' repository is read-only. Contact your data'
                         ' administrator to solve the problem')

    # Create destination dirs
    if not op.exists(data_dir):
        os.makedirs(data_dir)

    def _fetch_file_internal(file_, url, opts):
        try:
            # There are two working directories here:
            # - data_dir is the destination directory of the dataset
            # - temp_dir is a temporary directory dedicated to this fetching call.
            #   All files that must be downloaded will be in this directory. If a
            #   corrupted file is found, or a file is missing, this working
            #   directory will be deleted.
            files_pickle = cPickle.dumps(url)
            files_md5 = hashlib.md5(files_pickle).hexdigest()
            temp_dir = op.join(data_dir, files_md5)

            # 3 possibilities:
            # - the file exists in data_dir, nothing to do.
            # - the file does not exists: we download it in temp_dir
            # - the file exists in temp_dir: this can happen if an archive has been
            #   downloaded. There is nothing to do

            # Target file in the data_dir
            target_file = op.join(data_dir, file_)
            move = False

            if not force and op.exists(target_file):
                if verbose > 1:
                    print('{file} is cached, skipping download.'.format(file=file_))
            else:
                # Fetch the file, if it doesn't already exist.
                n_retries = opts.get('retries', retries)
                for ti in range(n_retries):
                    try:
                        fetched_file = _fetch_file(
                            url, temp_dir,
                            resume=resume,
                            overwrite=force,
                            verbose=verbose,
                            md5sum=opts.get('md5sum'),
                            username=opts.get('username'),
                            passwd=opts.get('passwd'),
                            handlers=opts.get('handlers', []),
                            headers=opts.get('headers', dict()),
                            cookies=opts.get('cookies', dict()),
                            data=opts.get('data'),
                            timeout=opts.get('timeout', 30),
                            output_encoding=opts.get('output_encoding'))
                        break  # Only one successful trial needed :)
                    except Exception as ex:  # noqa
                        if ti < n_retries - 1:
                            if verbose > 0:
                                print("Warning: failed try {try_num} of {n_retries} download attempts: {ex}".format(
                                    try_num=ti + 1, n_retries=n_retries, ex=ex))
                        else:
                            # final try.
                            raise

                # First, uncompress.
                if opts.get('uncompress'):
                    uncompressed_file = _uncompress_file(
                        fetched_file, verbose=verbose, delete_archive=delete_archive)
                    if uncompressed_file is not None and op.exists(uncompressed_file):
                        # did nothing; gotta move.
                        fetched_file = uncompressed_file
                    move = uncompressed_file is not None  # Movetree, not move, on zips.

                elif op.exists(fetched_file) or not op.exists(target_file):
                    # Let's examine our work
                    if not op.exists(fetched_file):
                        raise Exception("An error occurred while fetching %s; "
                                        "the expected target file cannot be found."
                                        " (%s)\nDebug info: %s" % (
                                            file_, target_file,
                                            {'fetched_file': fetched_file,
                                             'target_file': target_file}))
                    move = True

                if move:
                    # Move the fetched file to the target location.
                    target_dir = op.dirname(target_file)
                    if not op.exists(target_dir):
                        os.makedirs(target_dir)
                    shutil.move(fetched_file, target_file)

            # If needed, move files from temps directory to final directory.
            if op.exists(temp_dir):
                # XXX We could only moved the files requested
                # XXX Movetree can go wrong
                movetree(temp_dir, data_dir)
                shutil.rmtree(temp_dir)
        except Exception as ex:  # noqa
            if not skip_errors:
                raise
            elif verbose > 0:
                print('Exception while fetching %s: %s' % (url, str(ex)))
        finally:
            if clean_on_error and temp_dir and op.exists(temp_dir):
                shutil.rmtree(temp_dir)

        return target_file

    files_ = run_in_parallel(
        func=_fetch_file_internal,
        params=[dict(file_=file_, url=url, opts=opts)
                for file_, url, opts in files])

    # Validate
    for target_file in files_:
        # Let's examine our work
        if not op.exists(target_file):
            msg = "Could not find expected output: %s" % target_file
            if not skip_errors:
                raise Exception(msg)
            elif verbose > 0:
                print("Warning: %s" % msg)

    return files_


def copytree(src, dst, symlinks=False, ignore=None):
    """
    Copy entire directory of files from one directory to another.

    Parameters
    ----------
    src: string
        File-path of source directory from which to copy content.
    dst: string
        File-path of destination directory to copy content into.
    symlinks: Boolean (default=False)
        Whether or not to create symlinks between the items and destination file.
    ignore: function, optional
        Function that takes two arguments: a dir name and the files in it.
        Should return an iterable of ignore files.
    """
    import os
    import shutil
    import stat

    if not op.exists(dst):
        os.makedirs(dst)
        shutil.copystat(src, dst)
    lst = os.listdir(src)
    if ignore:
        excl = ignore(src, lst)
        lst = [x for x in lst if x not in excl]
    for item in lst:
        s = op.join(src, item)
        d = op.join(dst, item)
        if symlinks and op.islink(s):
            if op.lexists(d):
                os.remove(d)
            os.symlink(os.readlink(s), d)
            try:
                st = os.lstat(s)
                mode = stat.S_IMODE(st.st_mode)
                os.lchmod(d, mode)
            except AttributeError:
                pass  # lchmod not available
        elif op.isdir(s):
            copytree(s, d, symlinks, ignore)
        else:
            shutil.copy2(s, d)


class HttpFetcher(Fetcher):
    def __init__(self, data_dir=None, username=None, passwd=None):
        """
        Initialize HTTPFetcher

        Parameters
        ----------
        data_dir: string
            Path of the data directory. Used to force data storage in a specified
            location.
        username: string, optional
            Username used for HTTP authentication
        passwd: string, optional
            Password used for HTTP authentication
        """
        super(HttpFetcher, self).__init__(data_dir=data_dir)
        self.username = username
        self.passwd = passwd

    def fetch(self, files, force=False, resume=True, verbose=1,
              retries=1, delete_archive=True, skip_errors=False,
              clean_on_error=None):
        """
        Fetch files.
        See `fetch_files` for doc-string.
        """
        downloads = self.reformat_downloads(files)  # allows flexibility
        if self.username is not None:
            for tgt, src, opts in downloads:
                opts['username'] = opts.get('username', self.username)
                opts['passwd'] = opts.get('passwd', self.passwd)

        return fetch_files(files=downloads, data_dir=self.data_dir, resume=resume, force=force,
                           verbose=verbose, delete_archive=delete_archive,
                           skip_errors=skip_errors, clean_on_error=clean_on_error,
                           retries=retries)


class HttpApiFetcher(Fetcher):
    def _get_cache_file(self, key):
        """
        Get cache file.

        Parameters
        ----------
        id: string
            Any unique identifier

        Returns
        -------
            string: File-path to id from data directory.
        """
        return op.join(self.data_dir, '%s.pkl' % md5_hash(key))

    def _split_ids_by_cached(self, ids, force=False):
        """
        Parameters
        ----------
        ids: list
            list of unique identifiers
        force: bool, optional
            If true, fetch the files even if they already exist

        Returns
        -------
        tuple: dictionary of cached_data and list of ids not cached.
        """
        cached_data = OrderedDict()
        if force:
            return cached_data, ids

        missing_data = []
        for key in ids:
            cache_file = self._get_cache_file(key)
            if not op.exists(cache_file):
                missing_data.append(key)
                continue
            try:
                with open(cache_file, 'rb') as fp:
                    cached_data[key] = cPickle.load(fp)
                continue
            except Exception as ex:  # noqa
                logger.warning("Exception loading {cache_file}: {ex}".format(
                    cache_file=cache_file, ex=ex))
                missing_data.append(key)
        return cached_data, missing_data

    def fetch_in_parts(self, ids, fetch_fn, max_ids_per_request=None, force=False):
        """
        Fetch ids in batches.

        Parameters
        ----------
        ids: list
            list of unique identifiers.
        fetch_fn: function
            Function with which to download ids.
        max_ids_per_request: int
            Optional max number of ids to download per batch.
            If None, grab all.
        force: bool, optional
            If true, fetch the files even if they already exist

        Returns
        -------
        cached_data: dictionary of cached data.
        """
        # Check for cached before splitting to max_ids_per_request, to
        # avoid API calls with > max_ids_per_request
        cached_data, download_ids = self._split_ids_by_cached(
            ids, force=force)

        if max_ids_per_request is None or len(download_ids) <= max_ids_per_request:
            new_data = OrderedDict(zip(download_ids, fetch_fn(download_ids)))
            for key, val in new_data.items():
                with open(self._get_cache_file(key), 'wb') as fp:
                    cPickle.dump(val, fp)

        else:
            # Recursive case: slice list into manageable parts.
            url_batches = np.array_split(
                download_ids,
                np.ceil(1. * len(download_ids) / max_ids_per_request))
            new_data = OrderedDict()

            dicts = run_in_parallel(
                func=lambda ids: self.fetch_in_parts(
                    ids=ids, fetch_fn=fetch_fn, max_ids_per_request=max_ids_per_request,
                    force=force),
                params=[{'ids': url_batch.tolist()} for url_batch in url_batches])
            for d in dicts:  # keep order
                new_data.update(d)

        cached_data.update(new_data)
        return cached_data

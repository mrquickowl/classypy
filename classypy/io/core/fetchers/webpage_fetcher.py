import os.path as op
import re

import numpy as np
import pandas as pd

from classypy.compat import _urllib
from classypy.devops import logger
from classypy.io.core.fetchers.http_fetcher import HttpFetcher
from classypy.util.etl import to_ascii
from classypy.util.execution import run_in_parallel


class WebpageFetcher(HttpFetcher):
    dependencies = HttpFetcher.add_dependencies(spam_lists="spam-lists")
    opts = {
        'uncompress': True,
        'timeout': 5,
        'headers': {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_4) AppleWebKit/537.36 (KHTML, like Gecko)',
        },
        'output_encoding': 'utf-8',  # convert any charset to utf-8
    }

    @staticmethod
    def clean_url(url, protocol='http'):
        """
        Take various URLs, and try to clean them.

        Inspired by, but not exclusive to, Form 990 data.
        """
        if pd.isnull(url):
            return url
        if url.lower() in ('no website', 'n/a', '"n/a"', "'n/a'"):
            return None

        url = to_ascii(url.strip())  # guarantee ascii only.
        if url.startswith('//'):
            url = protocol + ':' + url
        if url.startswith('/'):
            url = WebpageFetcher.clean_url(url=url[1:], protocol=protocol)

        url = re.sub(r'^([a-zA-Z]{0,5})\/\/', '\\1://', url)  # http// => http://
        url = re.sub(r'^([a-zA-Z]{0,5}\:\/)([^/])', '\\1/\\2', url)  # http:/test => http://test

        if '://' not in url:
            url = protocol + '://' + url

        # fix some common formatting errors
        url = re.sub(r'[\s,]+', '.', url)
        url = url.replace('..', '.')
        url = re.sub(r'[\[\]]+', '', url)

        # Should be good. Now let's really clean up.
        parse = _urllib.parse.urlparse(url)

        # Remove spaces from host name, normalize the scheme
        # See: https://stackoverflow.com/questions/21628852/changing-hostname-in-a-url
        # for working with a urlparse (named tuple) object.
        parse = parse._replace(
            scheme=parse.scheme.lower(),
            netloc=parse.netloc.replace(' ', ''))
        if parse.scheme not in ('http', 'https'):
            parse = parse._replace(scheme=protocol)
        return parse.geturl()

    @staticmethod
    def generate_candidate_urls(url, verbose=1):
        """Generate all URLs we may want to access."""
        exceptions = dict()
        candidate_urls = []

        # Generate lowercase/original-case version, with different protocols.
        for cased_url in [url.lower(), url]:
            for protocol in ['http', 'https']:
                cur_url = cased_url
                try:
                    cur_url = WebpageFetcher.clean_url(cur_url, protocol=protocol)
                    if cur_url:
                        candidate_urls.append(cur_url)
                except Exception as ex:  # noqa
                    if verbose > 0:
                        print("Exception cleaning {url}: {ex}".format(url=cur_url, ex=ex))
                    exceptions[cur_url] = (ex, [])
                    continue

        # Add attempts with 'www' prepended.
        if candidate_urls:
            parse = _urllib.parse.urlparse(candidate_urls[0])
            if len(parse.netloc.split('.')) == 2 and parse.netloc.split('.')[0].lower() != 'www':
                www_urls = []
                for u in candidate_urls:
                    # Retry with 'www.' prepended.
                    parse = _urllib.parse.urlparse(u)
                    parse = parse._replace(netloc='www.' + parse.netloc)
                    www_urls.append(parse.geturl())
                candidate_urls += www_urls
        # try to normalize casing, sort in reverse to get lcased first.
        candidate_urls = np.unique(candidate_urls)[::-1]

        # try to normalize casing, sort in reverse to get lcased first.
        candidate_urls = np.unique(candidate_urls)[::-1]

        return candidate_urls, exceptions

    @classmethod
    def is_spam(cls, url):
        from spam_lists import SPAMHAUS_DBL
        from spam_lists.exceptions import InvalidURLError

        candidate_url = cls.generate_candidate_urls(url)[0]

        try:
            return SPAMHAUS_DBL.any_match(candidate_url)
        except InvalidURLError as iue:
            # Passed an invalid URL. It's not spam (just invalid)
            logger.debug(iue)
            return False
        except Exception as ex:  # noqa
            # Unexpected error while checking the service.
            # Assume URL is not spam (baseline rates are low,
            # shouldn't block scraping based on failed safeguard checks)
            logger.warning("Error while checking if {candidate_url} spam: {ex}".format(
                candidate_url=candidate_url, ex=ex))
            return False

    def fetch_url(self, url, cache_on_fail=False, skip_spam=True, force=False, verbose=1):
        local_file = None
        true_url = None
        if skip_spam and self.is_spam(url):
            logger.warning("Avoiding spam URL {url}".format(url=url))
            return '', url, Exception("Spam URL")

        # Get candidate urls, and any errors in generating them.
        candidate_urls, exceptions = self.generate_candidate_urls(url)

        for cur_url in candidate_urls:
            files = ((self.url2file(cur_url) + '.html', cur_url, self.opts),)

            try:
                pg = super(WebpageFetcher, self).fetch(
                    files, force=force, verbose=0,
                    delete_archive=False, clean_on_error=True)
            except Exception as ex:  # noqa
                if verbose > 0:
                    print("Exception downloading {url}: {ex}".format(url=cur_url, ex=ex))
                exceptions[cur_url] = (ex, files)
                continue

            # Store the downloaded file and actual URL location.
            local_file = pg[0]
            true_url = cur_url
            break

        # Tried downloads, now cache/report.
        if true_url is not None:
            # Success
            if verbose > 0:
                print("Success! From {url}".format(url=true_url))
            return local_file, true_url, exceptions

        # Failed to download. Clean up and move on.
        if cache_on_fail:
            local_file = op.join(self.data_dir, self.url2file(url))
            if force or not op.exists(local_file):
                # Cache an empty result for this one.
                try:
                    with open(local_file, 'w') as fp:
                        fp.write("")
                except Exception as ex:  # noqa
                    if verbose > 0:
                        print("ERROR saving to {local_file}: {ex}".format(
                            local_file=local_file, ex=ex))
                    # Save off an exception.
                    exceptions[local_file] = (ex, [local_file])

        return local_file, true_url, exceptions

    def fetch(self, urls, cache_on_fail=False, skip_spam=True, force=False, verbose=1):
        """
        Parameters
        ----------
        urls : iterable
            Website URL, to scrape & parse.
        cache_on_fail: boolean
            If True, an empty file is stored on an invalid website response.
        skip_spam : boolean
            If a requested URL is suspected spam (via Safe Browsing Lookup API),
            returns None
        force : boolean
            When False (default), previously downloaded data are reused.
            When True, reports are downloaded at runtime.
        verbose: int
            Level of output.
        """
        outputs = run_in_parallel(
            func=lambda url: self.fetch_url(
                url=url, cache_on_fail=cache_on_fail, skip_spam=skip_spam,
                force=force, verbose=verbose),
            params=[{'url': url} for url in urls])

        pages = [o[0] for o in outputs]
        true_urls = [o[1] for o in outputs]
        return pages, true_urls

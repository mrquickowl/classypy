"""
Fetcher class for AmazonS3.
"""
import os
import os.path as op
import time
import warnings
from functools import partial

import numpy as np

from .base import Fetcher, chunk_report


def test_cb(cur_bytes, total_bytes, t0=None):
    """
    Report progress of the upload.
    In this case, `upload` refers to the retrieval and storage of an object from S3 to a file.

    For more information, see the `cb` parameter of the `get_contents_to_filename` method
    at the following link: http://boto.cloudhackers.com/en/latest/ref/s3.html

    Parameters
    ----------
    cur_bytes: int
        Number of downloaded bytes successfuly transimtted to S3.
    total_bytes: int
        Total size of the file/transmitted object (may be 0/None, depending on download method).
    t0: int
        The time in seconds (as returned by time.time()) at which the
        download was resumed / started.
    """
    return chunk_report(bytes_so_far=cur_bytes, total_size=total_bytes,
                        initial_size=0, t0=t0)


class AmazonS3Fetcher(Fetcher):
    """
    Fetcher for Amazon S3.
    """

    dependencies = Fetcher.add_dependencies('boto')

    def __init__(self, data_dir=None, access_key=None, secret_access_key=None,
                 profile_name=None):
        """
        Initialize AmazonS3Fetcher.

        Parameters
        ----------
        data_dir: string, optional
            Path of the data directory. Used to force data storage in a specified
            location.
        access_key: string, optional
            AWS access key.
        secret_access_key: string, optional
            AWS secret access key.
        profile_name: string, optional
            AWS profile name.
        """
        if not (profile_name or (access_key and secret_access_key)):
            raise ValueError("profile_name or access_key / secret_access_key "
                             "must be provided.")
        super(AmazonS3Fetcher, self).__init__(data_dir=data_dir)
        self.access_key = access_key
        self.secret_access_key = secret_access_key
        self.profile_name = profile_name

    def fetch(self, files, force=False, verbose=1):
        """
        Fetch specified files.

        Parameters
        ----------
        files: list of (string, string, dict)
            List of files and their corresponding url. The dictionary contains
            options regarding the files. Options supported are:
                * 'md5sum' to check the md5 sum of the file.
                * 'move' if renaming the file or moving it to a subfolder is needed.
                * 'retries' to indicate that a number of retries before download failure.
                * 'uncompress' to indicate that the file is an archive.
                * 'username' used for HTTP authentication
                * 'passwd' used for HTTP authentication
                * 'handlers' urllib handlers passed to urllib.request.build_opener. Used by
                    advanced users to customize request handling.
                * 'headers' dictionary, specifying headers
                * 'cookies' dictionary, specifying cookies
                * 'data' dictionary, specifying data for a POST request.
                * 'timeout' specifies a timeout in seconds used by urllib
        force: Boolean (default=False)
            Whether or not to force download of files.
        verbose: int (default=1)
            Level of verbosity. 0 means no message.

        Returns
        -------
        list: list of specified files.
        """
        assert (self.profile_name or
                (self.access_key and self.secret_access_key))

        downloads = Fetcher.reformat_downloads(files)  # allows flexibility
        import boto
        if self.profile_name is not None:
            s3 = boto.connect_s3(profile_name=self.profile_name)
        elif (self.access_key is not None and
              self.secret_access_key is not None):
            s3 = boto.connect_s3(self.access_key, self.secret_access_key)

        bucket_names = np.unique([opts.get('bucket') for f, rk, opts in downloads])
        files_ = []
        for bucket_name in bucket_names:  # loop over bucket names: efficient
            if bucket_name:  # bucket requested
                buck = s3.get_bucket(bucket_name)
            else:  # default to first bucket
                buck = s3.get_all_buckets()[0]

            for file_, remote_key, opts in downloads:
                if opts.get('bucket') != bucket_name:
                    continue  # get all files from the current bucket only.
                target_file = op.join(self.data_dir, file_)
                key = buck.get_key(remote_key)
                if not key:
                    warnings.warn('Failed to find key: %s' % remote_key)
                    files_.append(None)
                else:
                    do_download = force or not op.exists(target_file)

                    if do_download:
                        # Ensure destination directory exists
                        destination_dir = op.dirname(target_file)
                        if not op.isdir(destination_dir):
                            if verbose > 0:
                                print("Creating base directory %s" % (
                                    destination_dir))
                            os.makedirs(destination_dir)

                        if verbose > 0:
                            print("Downloading [%s]/%s to %s." % (
                                bucket_name or 'default bucket',
                                remote_key,
                                target_file))
                        with open(target_file, 'wb') as fp:
                            cb = partial(test_cb, t0=time.time())
                            key.get_contents_to_file(fp, cb=cb, num_cb=None)

                    files_.append(target_file)
        return files_

"""
Functions for dynamically managing dependencies.
"""

import importlib
import subprocess
import warnings

from pip._internal.main import main as pip_main
from six import with_metaclass

from classypy.devops import logger


def install_dependency(module_name, install_info=None, verify=False):
    """
    Use pip to install a dependency on the fly.

    Parameters
    ----------
    module_name: string
        Name of python module to install.
    install_info: Optional string or list.
        Additional info for installing package.
        See https://github.com/pypa/pip/tree/master/pip
    verify: Boolean (default=False)
        If True, verify that install was successful.
    """
    install_info = install_info or module_name

    # Install it.
    try:
        print("Installing %s from %s..." % (module_name, install_info))
        rv = pip_main(['install', install_info])
        if rv != 0:
            warnings.warn('Pip returned %d' % rv)
    except Exception as ex:  # noqa
        logger.warning("Exception while installing %s: %s" % (module_name, ex))
        return False

    # Verify it
    if verify:
        try:
            importlib.import_module(module_name)
        except ImportError as ie:
            logger.warning('Failed to import %s: %s' % (module_name, ie))
            return False

    return True


def get_missing_dependencies(dependencies):
    """
    Iterate through dependencies to determine which are missing.

    Parameters
    ----------
    dependencies: list
        List of dependenceies to install.

    Returns
    -------
    missing_dependencies: list
        List of dependencies that weren't successfully installed.
    """
    missing_dependencies = []
    for dep in dependencies:
        try:
            importlib.import_module(dep)
        except ImportError:  # as ie:
            # print('Import error: %s' % str(ie))
            missing_dependencies.append(dep)
    return missing_dependencies


class DependenciesMetaclass(type):
    """
    Metaclass that wraps all instances with dependency resolution
    in its init function.
    """
    def __new__(cls, name, parents, props):
        def _init__wrapper(init_fn):
            """
            Decorator to initialize class with missing dependencies installed.
            """
            def wrapper_fn(self, *args, **kwargs):
                self.__class__.install_missing_dependencies()
                return init_fn(self, *args, **kwargs)
            return wrapper_fn

        new_cls = super(DependenciesMetaclass, cls) \
            .__new__(cls, name, parents, props)
        new_cls.__init__ = _init__wrapper(new_cls.__init__)
        return new_cls


class ClassWithDependencies(with_metaclass(DependenciesMetaclass, object)):
    """
    Class with methods for dealing with dependencies.
    """
    @classmethod
    def add_dependencies(cls, *args, **kwargs):
        """
        Identify required dependencies for the class.

        Example code:
        >>> dependencies = ClassName.add_dependencies(name = 'pypi-name')
        """
        return cls.combine_dependencies(args, kwargs, cls.dependencies)

    @classmethod
    def combine_dependencies(cls, *args):
        """
        Aggregate all dependencies into a single dict.
        """
        all_deps = dict()
        # Loop backwards, so that newer dependencies override old
        for cur_deps in args[::-1]:
            if not cur_deps:
                continue
            if not isinstance(cur_deps, dict):
                cur_deps = {d: d for d in cur_deps}
            all_deps.update(cur_deps)
        return all_deps

    @classmethod
    def get_missing_dependencies(cls):
        """
        Return missing dependencies.
        """
        return get_missing_dependencies(
            getattr(cls, 'dependencies', ()))

    @classmethod
    def install_missing_dependencies(cls, dependencies=None):
        """
        Install missing dependencies.

        Parameters
        ----------
        dependencies: list
            Optional list of dependencies to install.
        """
        if dependencies is None:
            dependencies = cls.get_missing_dependencies()
        for dep in dependencies:
            print("Installing missing dependencies '%s', for %s" % (
                dep, str(cls)))

            # Allow install info to be a dict; value is some
            # alternate pip string for installing the module name.
            # (e.g. git+git://github.com/gldnspud/virtualenv-pythonw-osx)
            install_info = None
            if isinstance(cls.dependencies, dict):
                install_info = cls.dependencies[dep]

            if not install_dependency(dep, install_info=install_info):
                out = subprocess.Popen(
                    ['pip', 'list'],
                    stdout=subprocess.PIPE).communicate()[0].decode()
                raise Exception("Failed to install dependency '%s'; "
                                "you will need to install it manually "
                                "and re-run your code.\n%s" % (dep, out))

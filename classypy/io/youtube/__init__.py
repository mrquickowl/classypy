# Author: Aaron Trefler
# License: simplified BSD
import json
import re

import pandas as pd
import requests

from ..core.datasets import HttpApiDataset


class YouTubeDataset(HttpApiDataset):
    """
    Child class of HttpApiFetcher for youtube API.
    """
    # Specifics for youtube API
    YOUTUBE_CHANNELID_LEN = 24
    YOUTUBE_URL_ID_TYPE_INDICATOR_IDX = 3
    ID_TYPES = ['channel_id', 'username', 'display_name']

    def __init__(self, token=None):
        super(YouTubeDataset, self).__init__()

        self.token = token
        if self.token is None:
            raise ValueError("Must pass an authentication token.")

    def fetch(self, identifiers, force=False, verbose=1):
        """
        Method to query youtube API for channel statistics related to
        given YouTube IDs or YouTube URLs.

        Parameters
        ----------
        identifiers : iterable
            Each element should be a YouTube channel identifier:
            a YouTube ID (i.e., channel_id, username, or display_name),
            or a YouTube channel URL (beggining with "http").
        force : boolean
            When False (default), previously downloaded data are reused.
            When True, reports are downloaded at runtime.
        verbose: int
            Level of output.

        Returns
        --------
            out_df : pandas.DataFrame containing YouTube channel statistics for each given id.
        """
        # Extract YouTube IDs and types
        ids = [self._get_id(identifier) for identifier in identifiers]
        id_types = [self._get_id_type(id) for id in ids]

        # URLs have secrets in them, so specify a filename to save to.
        src_urls = [('%s.json' % id, self._generate_url(id, id_type=id_type))
                    for id, id_type in zip(ids, id_types)]

        json_files = super(YouTubeDataset, self).fetch(
            src_urls, force=force, verbose=verbose, convert=False)

        # Convert data in json files to pandas.DataFrame object
        data_arrays = []
        for fi, file in enumerate(json_files):
            with open(file, 'r') as fp:
                data_array = json.load(fp)
                data_array = data_array['items'][0]['statistics']
            data_array['YouTubeId'] = ids[fi]
            data_array['YouTubeIdType'] = id_types[fi]
            data_arrays.append(data_array)
        out_df = pd.DataFrame(data=data_arrays)

        # Convert columns to proper types
        out_df["commentCount"] = out_df["commentCount"].map(pd.to_numeric)
        out_df["subscriberCount"] = out_df["subscriberCount"].map(pd.to_numeric)
        out_df["viewCount"] = out_df["viewCount"].map(pd.to_numeric)
        out_df["videoCount"] = out_df["videoCount"].map(pd.to_numeric)

        return out_df

    def _get_id(self, identifier):
        """
        Converts a YouTube channel identifier to a Youtube ID.

        Parameters
        ----------
        identifier: str
            YouTube channel identifier.

        Returns
        --------
        string for YouTube ID.
        """
        # identifier is a YouTube ID
        if not self._is_url(identifier):
            return identifier

        id_indicator = identifier.split('/')[self.YOUTUBE_URL_ID_TYPE_INDICATOR_IDX]
        # identifier is a YouTube URL with a username or channel_id
        if id_indicator == "user" or id_indicator == "channel":
            return identifier.split('/')[self.YOUTUBE_URL_ID_TYPE_INDICATOR_IDX + 1]
        # identifier is a YouTube URL with a display_name
        return id_indicator

    def _is_url(self, identifier):
        """
        Determine if YouTube channel identifier is a URL.
        """
        return re.match('^https?://', identifier) is not None

    def _get_id_type(self, input_id):
        """
        Infer YouTube ID type from an ID.

        Parameters
        ----------
        input_id : str
            YouTube channel_id, username, or display_name.

        Returns
        --------
        string specifiying the YouTube ID type.
        """
        if self._is_channel_id(input_id):
            return "channel_id"
        elif self._is_username(input_id):
            return "username"
        else:
            return "display_name"

    def _is_channel_id(self, input_id):
        """
        Determine if input_id is a YouTube channel_id
        """
        return input_id[:2] == "UC" and len(input_id) == self.YOUTUBE_CHANNELID_LEN

    def _is_username(self, input_id):
        """
        Determine if a input_id is a YouTube username
        """
        request = ("https://www.youtube.com/user/{username}").format(username=input_id)
        r = requests.get(request)
        return str(r.status_code)[0] == "2"

    def _generate_url(self, id, id_type=None):
        """
        Generate url to query youtube API.

        Parameters
        ----------
        id : str
            YouTube ID
        id_type: str
            Speciifies YouTube ID type.

        Returns
        -------
            : str
            URL string to extract statisitcs from the YouTube Data v3 API.
        """
        id_type = id_type or self._get_id_type(id)
        assert id_type in self.ID_TYPES, id_type

        if id_type == "channel_id":
            # youtube_id passed is a channel_id
            return("https://www.googleapis.com/youtube/v3/channels?"
                   "id={channel_id}&key={token}&part=statistics").format(
                channel_id=id, token=self.token)
        elif id_type == "username":
            # youtube_id passed is a username
            return("https://www.googleapis.com/youtube/v3/channels?"
                   "forUsername={username}&key={token}&part=statistics"
                   ).format(username=id, token=self.token)

        else:
            # youtube_id passed is a display name
            url = ("https://www.googleapis.com/youtube/v3/search?"
                   "part=snippet&q={display_name}&type=channel&key={token}").format(
                display_name=id, token=self.token)
            fil = super(YouTubeDataset, self).fetch([url], convert=False)
            with open(fil[0], 'r') as fp:
                d = json.load(fp)

            channel_id = d['items'][0]['id']['channelId']
            return("https://www.googleapis.com/youtube/v3/channels?"
                   "id={channel_id}&key={token}&part=statistics"
                   ).format(channel_id=channel_id, token=self.token)

# Author: Aaron Trefler
"""
Simple report download & info.
"""
print("Importing...")
from classypy.devops import find_secrets
from classypy.io.youtube import YouTubeDataset

# Read env file into environ, if available
print("Accessing YouTube API authorization token...")
secrets = find_secrets()

print("Creating YouTubeDataset object...")
dataset = YouTubeDataset(
    token=secrets["YOUTUBE_API_TOKEN"])

identifiers = ["http://www.youtube.com/user/11thhourtheatreco",
               "https://www.youtube.com/channel/UC-0jQD3xk3M2ECMil-4hlAQ/",
               "https://www.youtube.com/3ieimpact",
               "schacademy",
               "UCadJuRhUQBdboNTFzFCfqbw",
               "scphca"]
print("Fetching YouTube statistics...")
df = dataset.fetch(identifiers=identifiers, force=False, verbose=1)

# Display example data
print('\n')
print("columns: %s" % ", ".join(sorted(df.columns)))
numeric_cols = ["commentCount", "subscriberCount", "videoCount", "viewCount"]
for col in numeric_cols:
    max_idx = df[col].idxmax()
    print("Max {col:15}: {val:10} Channel: {youtube_id}".format
          (col=col, val=str(df[col][max_idx]), youtube_id=str(df["YouTubeId"][max_idx])))
print('\n')
print(df)

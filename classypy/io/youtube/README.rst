Notes
-----
This is a thin wrapper around the YouTube Data v3 API.

Content
-------
    :'YouTube IDs': usernames, channel IDs, or display names for YouTube channels. For example:
        https://www.youtube.com/channel/{channel_id} or
        https://www.youtube.com/user/{username} or
        https://www.youtube.com/{display_name}

Examples
----------
    : example1.py: Fetches statistics for six YouTube IDs. 
        The first three are URLs with a username, channel ID, and legacy display name respectively,
        the last three are a stand-alone username, channel ID, and legacy display name.

Obtaining a developer key for the YouTube Data API v3
----------
https://www.youtube.com/watch?v=Im69kzhpR3I

References
----------
https://developers.google.com/youtube/v3/docs/
https://developers.google.com/youtube/v3/

# Author: Ben Cipollini
# License: simplified BSD

import os

import pandas as pd

from ..core.datasets import HttpApiDataset


class RjMetricsDataset(HttpApiDataset):
    """Dataset of RJ Reports"""

    COLUMN_MAP = {
        'Public (Yes/No)': 'Public', }

    def __init__(self, data_dir=None, api_key=None):
        super(RjMetricsDataset, self).__init__(data_dir=data_dir)
        self.api_key = api_key or os.enivorn.get("RJ_API_KEY")
        if not self.api_key:
            raise ValueError("Must define RJ_API_KEY environment variables, or "
                             "pass api_key arguments.")

    def fetch(self, report_ids, force=False, verbose=1):
        """
        Parameters
        ----------
        report_ids : iterable
            Each element should be a numeric report ID, from Datanyze.
        force : boolean
            When False (default), previously downloaded data are reused.
            When True, reports are downloaded at runtime.
        verbose: int
            Level of output.
        """
        # URLs have secrets in them, so specify a filename to save to.
        files = [('%s.csv' % report_id,
                  'https://api.rjmetrics.com/0.1/figure/%s/export' % report_id,
                  dict(headers={'X-RJM-API-Key': self.api_key},
                       data={'format': 'csv', 'includeColumnHeaders': '1'}))
                 for report_id in report_ids]
        cache_files = self.fetcher.fetch(files, force=force)
        return [pd.read_csv(cache_file) for cache_file in cache_files]

"""
Simple report download & info.
"""
from classypy.devops import find_secrets
from classypy.io.rjmetrics import RjMetricsDataset

secrets = find_secrets()
dataset = RjMetricsDataset(api_key=secrets['RJ_API_KEY'])

dfs = dataset.fetch(report_ids=[1794951])
for df in dfs:
    print("Downloaded %d results" % len(df))
    print("columns: %s" % ", ".join(sorted(df.columns)))
    print("Example data: unique values of column 'charity_id': %s" % str(sorted(df['charity_id'].unique())))

RJ Metrics

Notes
-----
RJMetrics is a paid service for general reporting on top of a data warehouse.


Content
-------
    :'reports': user-defined reports (csv).


Examples
----------

    :example1.py: Fetches an example report, parses it, and reports on the # of results.


References
----------

http://www.rjmetrics.com

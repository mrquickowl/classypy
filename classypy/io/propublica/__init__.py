import json
import time

from ...compat import _urllib
from ..core.datasets import HttpApiDataset
from ..core.fetchers import HttpFetcher


class ProPublicaFetcher(HttpFetcher):
    def __init__(self, data_dir=None, sleep_time=45, max_retries=2):
        super(ProPublicaFetcher, self).__init__(data_dir=data_dir)
        self.sleep_time = sleep_time
        self.max_retries = max_retries

    def _fetch_json_with_retries(self, urls, force=False, verbose=1,
                                 retries_so_far=0, default_value=None):
        try:
            files = self.fetch(urls, force=force, verbose=verbose)
            with open(files[0], 'r') as fp:
                res = json.load(fp)
        except Exception as ex:  # noqa
            # TODO: filter by error type in the except, rather than looking for a property.
            if not hasattr(ex, 'getcode'):
                raise

            elif ex.getcode() == 403 and retries_so_far < self.max_retries:
                # Too many API hits; chill...
                print("Sleeping after too many API hits...")
                time.sleep(self.sleep_time)
                res = self._fetch_json_with_retries(
                    urls=urls, default_value=default_value,
                    force=force, verbose=verbose,
                    retries_so_far=retries_so_far + 1)

            elif ex.getcode() == 404:
                # Save dummy data for 404s; these are unrecoverable
                print("Fetch: 404 for url=%s; creating dummy data." % urls[0][1])
                res = default_value

            else:
                raise

        return res

    def fetch_json(self, org_name, query_params=None,
                   verbose=1, force=False):
        """Gets data from cache or remote API, caches."""
        org_name_safe = _urllib.parse.quote(org_name).replace('/', '%2F').replace('%', 'x')
        query_params_safe = _urllib.parse.urlencode(query_params or {}).replace('%', 'x')
        filename_safe = 'pp_%s_%s.json' % (org_name_safe, query_params_safe)

        query_params = query_params or {}
        query_params['q'] = org_name

        url = self.construct_url(
            "https://projects.propublica.org/nonprofits/api/v1/search.json",
            params=query_params)

        return self._fetch_json_with_retries(
            urls=((filename_safe, url),),
            verbose=verbose, force=force,
            default_value=dict(filings=tuple()))

    def fetch_organization(self, ein, force=False, verbose=1):
        """Gets data from cache or remote API, caches."""
        ein_safe = _urllib.parse.quote(ein.replace('-', ''))
        ein_safe = ein_safe.replace('/', '%2F').replace('%', 'x')
        filename_safe = 'pp_%s.json' % (ein_safe)

        # Download
        url = "https://projects.propublica.org/nonprofits/api/v2/organizations/%s.json" % ein_safe
        return self._fetch_json_with_retries(
            urls=((filename_safe, url),),
            verbose=verbose, force=force,
            default_value=dict(filings=tuple()))


class ProPublicaDataset(HttpApiDataset):
    fetcher_class = ProPublicaFetcher
    address_keys = ['address', 'city', 'state', 'zipcode']

    def fetch(self, eins=None, verbose=1, force=False):
        """
        Parameters
        ----------
        eins : list-like
            List of charity EINs to fetch 990 returns for.
            EINs should be completely numeric; e.g: 261630239
        verbose : bool
            Level to log progress to screen; see HttpApiDataset.
        force : bool
            Forcing flag

        Returns
        -------
        dicts : list
            list of dictionaries.
        """
        dicts = []
        for ein in eins:
            try:
                dicts.append(self.fetcher.fetch_organization(
                    ein=ein, force=force, verbose=verbose))
            except Exception as ex:  # noqa
                if verbose > 0:
                    print("Exception while updating org {ein}: {ex}".format(
                        ein=ein, ex=ex))
                dicts.append({})

        return dicts

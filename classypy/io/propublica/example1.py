import random as rd

from classypy.io.irs import IrsEINDataset
from classypy.io.propublica import ProPublicaDataset

# Grab 3 EINs
ein_dataset = IrsEINDataset()
eins = ein_dataset.fetch().ein.values
rand_eins = [eins[rd.randint(0, len(eins))]
             for _ in range(10)]

dataset = ProPublicaDataset()
dicts = dataset.fetch(eins=rand_eins)
pct_bad = len([1 for d in dicts if not d]) / float(len(dicts))
print("%.2f%% of EINs found on ProPublica." % (100 * (1 - pct_bad)))
print("columns: %s" % dicts[0].keys())
print("Organization columns: %s" % dicts[0].get('organization', {}).keys())

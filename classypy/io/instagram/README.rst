Datanyze

Notes
-----
Instagram APIs are a set of classes to scrape data from Instagram. It loads pages dynamically by looking at the next_cursor token sent back using JSON by the graphql APIs of Instagram. No auth is required. `max_pages` determines how many times we will "scroll down".


Content
-------
    :'__init__.py': Implements the HttpApiFetcher and Dataset classes. This is all you need.


Examples
----------

    :example1.py: Fetches posts that contain a specific tag (or hashtag)
    :example2.py: Fetches the posts for a list of users
    :example3.py: Fetches the profile information for a list of users
    :example4.py: Fetches the list of followers for a list of users

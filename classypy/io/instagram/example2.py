"""
Simple report download & info.
"""

from classypy.io.instagram import InstagramUserPostDataset

# Read env file into environ, if available
dataset = InstagramUserPostDataset()

user_posts = dataset.fetch(usernames=["_disco_jesus"], max_posts=3)

# view results
print("Shape of resulting dataframe: {shape}".format(shape=user_posts.shape))
print("DataFrame:")
print(user_posts)

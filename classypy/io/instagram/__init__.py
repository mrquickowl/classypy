import json
import random
import re
import time

import pandas as pd

from classypy.io.core.datasets import Dataset
from classypy.io.core.fetchers import RequestsFetcher, WebpageFetcher
from classypy.text import json_from_disk, text_from_disk
from classypy.util import update_dict
from classypy.util.dates import convert_datetime_columns


class InstagramFetcher(WebpageFetcher):
    """
    TODO: extend HttpApiFetcher to follow structure and handle better the JSON response
    """
    USER_FIELDS = (
        "biography", "followers_count", "is_verified", "id",
        "full_name", "username", "statuses_count"
    )

    def __init__(self, data_dir=None, max_attempts=20, base_sleeping_time=10):
        super(InstagramFetcher, self).__init__(data_dir=data_dir)
        self.max_attempts = max_attempts
        self.base_sleeping_time = base_sleeping_time

    def set_urls(self, top_key):
        if top_key == "user":
            self.base_url = ("https://www.instagram.com/graphql/query/?query_id=17888483320059182&variables=%7B%22"
                             "id%22%3A%22{tag_name}%22%2C%22first%22%3A{first}%2C%22after%22%3A%22{after}%22%7D")
            self.top_key = "user"
            self.page_key = "edge_owner_to_timeline_media"
            self.initial_url = 'https://www.instagram.com/{query}'
        elif top_key == "hashtag":
            self.base_url = ("https://www.instagram.com/graphql/query/?query_id=17875800862117404&variables=%7B%22"
                             "tag_name%22%3A%22{tag_name}%22%2C%22first%22%3A{first}%2C%22after%22%3A%22{after}%22%7D")
            self.top_key = "hashtag"
            self.page_key = "edge_hashtag_to_media"
            self.initial_url = 'https://www.instagram.com/explore/tags/{query}'
        else:
            raise ValueError("Wrong top key")

    def fetch_username(self, shortcode, force=False):
        """
        Given a shortcode (it can be found along wih the id and the caption of the picture)
        returns the username of the user who pisted that pic
        """
        url = "https://www.instagram.com/p/{shortcode}/?__a=1".format(shortcode=shortcode)
        local_file, true_url, exceptions = self.fetch_url(url, force=force)
        json_response = json_from_disk(local_file)
        return json_response["graphql"]["shortcode_media"]["owner"]["username"]

    def fetch_profile_info(self, username, force=False):
        """
        Given an username returns the basic info about a user
        """
        initial_url = 'https://www.instagram.com/{query}'
        local_file, true_url, exceptions = self.fetch_url(initial_url.format(query=username), force=force)

        html = text_from_disk(local_file)

        text = html[html.index("window._sharedData = ") + 21:]
        text = (text[:text.index("};</script>")] + "}").replace('\\"', "")
        dictionary = json.loads(text)
        data = dictionary["entry_data"]["ProfilePage"][0]["user"]
        user_dict = {}

        for field in self.USER_FIELDS:
            if field == "followers_count":
                user_dict[field] = data["followed_by"]["count"]
            elif field == "statuses_count":
                user_dict[field] = data["media"]["count"]
            else:
                user_dict[field] = data.get(field)
        return user_dict

    def fetch_first_page_posts(self, initial_url, query, top_key, username=None, force=False):
        """
        Returns posts' data about the first page. The first page is in HTML while the 'scroll downs'
        are returned in JSON
        """
        assert top_key in ('user', 'hashtag')

        if username is not None:
            query = username
        local_file, true_url, exceptions = self.fetch_url(initial_url.format(query=query), force=force)

        html = text_from_disk(local_file)

        text = html[html.index("window._sharedData = ") + 21:]
        text = (text[:text.index("};</script>")] + "}").replace('\\"', "")
        dictionary = json.loads(text)["entry_data"]
        if top_key == "user":
            data = dictionary["ProfilePage"][0]["user"]["media"]

        else:
            data = dictionary["TagPage"][0]["tag"]["media"]

        initial_token = data["page_info"]["end_cursor"]
        return data["nodes"], initial_token

    def fetch_posts(self, top_key, query,
                    max_posts=10, username=None, verbose=1, force=False, first=300):
        """
        Returns a dataframe of posts given either an username or a hashtag

        Parameters
        ----------
        page_key: key used by instagram that stores posts of a JSON page
        top_key: key used by instagram to distinguish between users' profiles and hashtags searches
        base_url: url used by Instagram to query their graph apis
        query: the term we want to search for. It can be an user or a hashtag
        initial_url: url used to load the first HTML page
        max_posts: maximum number of 'scroll downs' when searching for a user's post or hashtags
        username: when searching for user's posts we query the first HTML page using the username and
                  then use the Instagram graph APIs using the user's ID
        first: number of followers we will load at each request. It can't be too large or we will get a server error
        """
        # based on the top key set the urls to use for the query
        self.set_urls(top_key)

        query = query.replace("#", "")

        first_page_posts, initial_token = self.fetch_first_page_posts(initial_url=self.initial_url,
                                                                      username=username,
                                                                      force=force,
                                                                      top_key=top_key,
                                                                      query=query)

        has_next = True
        # first corresponds to the number of results to return from a request.
        # It shouldn't be too large or the server will throw 502 errors
        first = min(max_posts, first)

        posts = []

        posts += first_page_posts

        posts_to_return = max_posts

        while has_next and max_posts > 0:
            url = self.base_url.format(tag_name=query, first=first, after=initial_token)
            local_file, true_url, exceptions = self.fetch_url(url, force=force)
            if exceptions.get(url):
                attempt_number = 1
                max_attempts = self.max_attempts
                sleeping_time = self.base_sleeping_time * random.random() + self.base_sleeping_time
                while exceptions.get(url) and attempt_number < max_attempts:
                    print("Too many requests: Sleeping for {sleeping_time} seconds - Attempt # {attempt_number}"
                          .format(attempt_number=attempt_number, sleeping_time=sleeping_time))
                    # If attempts continue to fail, try to sleep for longer
                    if attempt_number >= 10:
                        sleeping_time += 30
                    time.sleep(sleeping_time)
                    local_file, true_url, exceptions = self.fetch_url(url, force=True)
                    attempt_number += 1
                if attempt_number >= max_attempts:
                    # if 429 persists return data collected so far
                    return posts
            json_response = json_from_disk(local_file)["data"][top_key][self.page_key]
            has_next = json_response["page_info"]["has_next_page"]
            initial_token = json_response["page_info"]["end_cursor"]

            sleeping_time = random.randint(1, 3) + random.random()
            time.sleep(sleeping_time)

            posts_on_page = json_response["edges"]
            posts += posts_on_page
            max_posts -= len(posts_on_page)
        posts = posts[:posts_to_return]
        return posts


class InstagramFollowersFetcher(RequestsFetcher):
    def __init__(self, username=None, password=None):
        self.login_url = 'https://www.instagram.com/accounts/login/ajax/'
        super(InstagramFollowersFetcher, self).__init__(username=username, passwd=password, login_url=self.login_url)
        self.url = 'https://www.instagram.com/'
        self.headers = {
            'Accept-Encoding': 'gzip, deflate',
            'Accept-Language': "en",
            'Connection': 'keep-alive',
            'User-Agent': ("Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 "
                           "(KHTML, like Gecko) Chrome/48.0.2564.103 Safari/537.36"),
            'X-Requested-With': 'XMLHttpRequest',
            'Origin': 'https://www.instagram.com',
            'Referer': 'https://www.instagram.com/',
            'Host': 'www.instagram.com',
            'X-Instagram-AJAX': '1',
        }

        self.base_url = ("https://www.instagram.com/graphql/query/?query_id=17851374694183129&variables=%7B%22"
                         "id%22%3A%22{tag_name}%22%2C%22first%22%3A{first}%2C%22after%22%3A%22{after}%22%7D")

        self.page_key = "edge_followed_by"
        self.top_key = "user"
        self.initial_url = ("https://www.instagram.com/graphql/query/?query_id=17851374694183129&variables=%7B%22"
                            "id%22%3A%22{tag_name}%22%2C%22first%22%3A{first}%7D")

    def fetch_followers(self, query, max_followers=1000000, username=None, first=3000):

        """
        It returns a list of dictionaries each containing followers' info

        Parameters
        ----------
        top_key: it states if we are dealing with an user or a hashtag
        page_key: the key holding the data in the JSON request
        base_url: base url that we use to send requests to Instagram
        query: the numerical id of the person we are getting the followers from
        initial_url: url for the first loading page
        session: a requests.session object in which we are authenticated
        max_followers: maximum number of followers we are gonna collect
        username: the username of the person we are getting the followers from
        first: number of followers we will load at each request. It can't be too large or we will get a server error
        """
        if self.top_key != 'user':
            raise ValueError("top_key has incorrect value. It must be 'user'")

        url = self.initial_url.format(tag_name=query, first=first)

        has_next = True

        followers_dict = []

        num_followers = float('Inf')

        while has_next and (max_followers > 0 and len(followers_dict) < num_followers):
            json_response, exceptions = self.fetch_url(url, headers=self.headers)
            status = json_response.status_code

            if status == 429:
                attempt_number = 1
                max_attempts = self.max_attempts
                while status == 429 and attempt_number < max_attempts:
                    sleeping_time = self.base_sleeping_time * random.random() + self.base_sleeping_time
                    print("Too many requests: Sleeping for {sleeping_time} seconds - Attempt # {attempt_number}"
                          .format(attempt_number=attempt_number, sleeping_time=sleeping_time))
                    # If attempts continue to fail, try to sleep for longer
                    if attempt_number >= 10:
                        sleeping_time += 60
                    time.sleep(sleeping_time)
                    json_response, exceptions = self.fetch_url(url)
                    status = json_response.status_code
                    attempt_number += 1
                if attempt_number >= max_attempts:
                    # if 429 persists return data collected so far
                    return followers_dict
            elif status == 502:
                # from experience we noticed that 502 errors are often solvable by re-sending the same query.
                # If first is too big (over 5000), 502 erros might occur more frequently
                continue
            else:
                try:
                    json_response = json_response.json()["data"][self.top_key][self.page_key]
                except:  # noqa: E722
                    # TODO: specify possible exceptions.
                    return followers_dict
                num_followers = json_response["count"]
                initial_token = json_response["page_info"]["end_cursor"]

                followers_on_page = json_response["edges"]

                followers_dict += followers_on_page
                max_followers -= len(followers_on_page)
                has_next = json_response["page_info"]["has_next_page"]
                url = self.base_url.format(tag_name=query, first=first, after=initial_token)
                sleeping_time = random.randint(1, 3) + random.random()
                time.sleep(sleeping_time)

        return followers_dict


class _InstagramPostsDataset(Dataset):
    fetcher_class = InstagramFetcher

    def get_hashtags_from_post(self, text):
        """
        Returns hashtags in a pic's caption
        """
        try:
            return re.findall(r"#(\w+)", text)
        except TypeError:
            return []

    def clean_posts(self, posts, username=None):
        """
        Given json data returns several fields about the picture

        Parameters
        ----------
        posts: list of dicts containing data about each post
        first_page: when the first page is parsed the posts have different keys from the successive posts,
                    for this reason there is the need to declare when the first page is parsed.
                    First page returns HTML while successive pages are in JSON.
        username: if we are fetching the posts of a user there is no need to obtain the username from the shortcode,
                  hence we can pass the username of the current user and just store it in the row
        """
        results = []

        json_keys = {
            "shortcode": "shortcode",
            "date": "taken_at_timestamp",
            "display_src": "display_url",
            "owner": "owner",
            "text": "edge_media_to_caption",
            "likes": "edge_liked_by",
        }

        first_page_keys = {
            "shortcode": "code",
            "text": "caption",
            "likes": "likes",
            "date": "date",
            "owner": "owner",
            "display_src": "display_src"
        }

        for item in posts:
            if "node" in item:
                keys = json_keys
                item = item["node"]
            else:
                keys = first_page_keys

            d = {}

            for key, field in keys.items():
                if key == "likes":
                    if item.get("edge_liked_by"):
                        d["likes"] = item["edge_liked_by"]["count"]
                    elif item.get("edge_media_preview_like"):
                        d["likes"] = item["edge_media_preview_like"]["count"]
                    else:
                        d[key] = item[field]["count"]
                elif key == "owner":
                    d[key] = item[field]["id"]
                elif key == "text" and field == "edge_media_to_caption":
                    try:
                        d[key] = item[field]["edges"][0]["node"]["text"]
                    except IndexError:
                        d["text"] = ""
                else:
                    d[key] = item.get(field)

            d["username"] = username or self.fetcher.fetch_username(d["shortcode"])

            d["hashtags"] = " ".join(self.get_hashtags_from_post(d["text"]))
            results.append(d)
        return results

    def fetch(self, values, force=False, verbose=1, max_posts=10, id_username_dict=None, first=300):
        """
        Given a list of values returns the posts about those values (either usernames or hashtags)

        Parameters
        ----------
        values: list of hashtags/usernames
        max_posts: maximum number of 'scroll downs'

        Returns
        -------
        values_df: a dataframe containing posts
        """
        values_dicts = []
        if id_username_dict is None:
            id_username_dict = {}

        for value in values:
            if verbose > 0:
                print("Collecting data for {value}".format(value=value))
            try:
                value_results = self.fetcher.fetch_posts(
                    query=value, first=first, username=id_username_dict.get(value),
                    max_posts=max_posts, top_key=self.top_key,
                    force=force, verbose=verbose)
            except Exception as ex:  # noqa
                if verbose > 0:
                    print("Error for {value}: {ex}".format(value=value, ex=ex))
                continue
            value_results = self.clean_posts(value_results, username=id_username_dict.get(value))
            value_results = [update_dict({"tag": value}, post) for post in value_results]
            values_dicts += value_results

        values_df = pd.DataFrame(data=values_dicts)

        return values_df


class InstagramSearchDataset(_InstagramPostsDataset):
    DATETIME_COLUMNS = ('date',)

    def __init__(self):
        super(InstagramSearchDataset, self).__init__()
        self.top_key = "hashtag"

    @convert_datetime_columns(column_names=DATETIME_COLUMNS, unit='s')
    def fetch(self, tags, force=False, verbose=1, max_posts=10, first=300):
        """
        Returns a dataframe containing posts that have a certain tag/hashtag
        """
        return super(InstagramSearchDataset, self).fetch(
            values=tags, force=force, first=first,
            verbose=verbose, max_posts=max_posts)


class InstagramUserPostDataset(_InstagramPostsDataset):
    DATETIME_COLUMNS = ('date',)

    def __init__(self):
        super(InstagramUserPostDataset, self).__init__()
        self.top_key = "user"

    @convert_datetime_columns(column_names=DATETIME_COLUMNS, unit='s')
    def fetch(self, usernames, force=False, verbose=1, max_posts=10, first=300):
        """
        Returns a dataframe containing posts of a specific set of users
        """
        username_fetcher = InstagramUserDataset()
        usernames_df = username_fetcher.fetch(usernames=usernames, force=force, verbose=verbose)
        id_username_dict = dict(usernames_df[["id", "username"]].values)
        values = list(id_username_dict.keys())
        return super(InstagramUserPostDataset, self).fetch(
            values=values, force=force, first=first,
            verbose=verbose, max_posts=max_posts,
            id_username_dict=id_username_dict)


class InstagramUserFollowersDataset(Dataset):
    """
    It fetches the ids of an instagram account. In order to achieve this
    InstagramUserFollowerDataset needs to be logged in in a session with an Instagram account credentials.
    Differently from the other classes, InstagramUserFollowerDataset does not cache data since we are using requests
    rather than HTTPFetcher.
    """

    def __init__(self, username=None, password=None):
        self.username = username
        self.password = password

    def clean_followers(self, edges, username):
        results = []
        for edge in edges:
            user = edge["node"]
            d = {
                "full_name": user.get("full_name"),
                "id": user.get("id"),
                "is_verified": user.get("is_verified"),
                "profile_pic_url": user.get("profile_pic_url"),
                "username": user.get("username"),
                "tag": username
            }
            results.append(d)
        return results

    def fetch(self, usernames, force=False, verbose=1, max_followers=1000000, first=3000):
        """
        Returns a dataframe containing posts of a specific set of users
        """
        follower_fetcher = InstagramFollowersFetcher(username=self.username, password=self.password)

        username_fetcher = InstagramUserDataset()
        usernames_df = username_fetcher.fetch(usernames=usernames, force=force, verbose=verbose)
        id_username_dict = dict(usernames_df[["id", "username"]].values)

        followers_dict = []
        for value in id_username_dict.keys():
            try:
                username = id_username_dict[value]
                followers = follower_fetcher.fetch_followers(
                    query=value, first=first, max_followers=max_followers,
                    username=username)
            except Exception as ex:  # noqa
                if verbose > 0:
                    print("Error for {value}: {ex}".format(value=value, ex=ex))
                continue
            followers_dict += self.clean_followers(followers, username)
        followers_df = pd.DataFrame(data=followers_dict)
        return followers_df


class InstagramUserDataset(Dataset):
    fetcher_class = InstagramFetcher
    COLUMNS = (
        'biography', 'followers_count', 'full_name', 'id', 'is_verified',
        'statuses_count', 'username'
    )

    def fetch(self, usernames, force=False, verbose=1):
        """
        Returns a dataframe containing basic profile info for a list of users
        """
        users_dict = []
        for username in usernames:
            try:
                users_dict.append(self.fetcher.fetch_profile_info(username, force=force))
            except Exception as ex:  # noqa
                if verbose > 0:
                    print("Error for {value}: {ex}".format(value=username, ex=ex))
                continue

        return pd.DataFrame(data=users_dict) if users_dict else pd.DataFrame(columns=self.COLUMNS)

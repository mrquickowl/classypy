"""
Simple report download & info.
"""

from classypy.io.instagram import InstagramSearchDataset

# Read env file into environ, if available
dataset = InstagramSearchDataset()

posts = dataset.fetch(tags=["climatechange", "trump"], max_posts=10)

# view results
print("Shape of resulting dataframe: {shape}".format(shape=posts.shape))
print("DataFrame:")
print(posts)

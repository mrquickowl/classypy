"""
Simple report download & info.
"""
from classypy.devops import find_secrets
from classypy.io.instagram import InstagramUserFollowersDataset

# Read env file into environ, if available
secrets = find_secrets()
dataset = InstagramUserFollowersDataset(
    username=secrets["INSTAGRAM_USERNAME"], password=secrets["INSTAGRAM_PASSWORD"])

users = dataset.fetch(usernames=["_disco_jesus", "usaid"], max_followers=10000)

# view results
print("Shape of resulting dataframe: {shape}".format(shape=users.shape))
print("DataFrame:")
print(users)

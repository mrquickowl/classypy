"""
Simple report download & info.
"""

from classypy.io.instagram import InstagramUserDataset

# Read env file into environ, if available
dataset = InstagramUserDataset()

users = dataset.fetch(usernames=["_disco_jesus", "usaid"])

# view results
print("Shape of resulting dataframe: {shape}".format(shape=users.shape))
print("DataFrame:")
print(users)

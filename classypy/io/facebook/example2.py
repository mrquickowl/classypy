"""
Simple report download & info.
"""
from classypy.devops import find_secrets
from classypy.io.facebook import FacebookURLEngagementDataset

secrets = find_secrets()
dataset = FacebookURLEngagementDataset(
    app_id=secrets['FACEBOOK_APP_ID'],
    app_secret=secrets['FACEBOOK_APP_SECRET'])

df = dataset.fetch(urls=['https://classy.org', ])
print("Downloaded %d urls" % len(df))
print("columns: %s" % ", ".join(sorted(df.columns)))

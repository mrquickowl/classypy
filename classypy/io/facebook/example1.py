"""
Simple report download & info.
"""
from classypy.devops import find_secrets
from classypy.io.facebook import FacebookPostsDataset

secrets = find_secrets()
dataset = FacebookPostsDataset(
    app_id=secrets['FACEBOOK_APP_ID'],
    app_secret=secrets['FACEBOOK_APP_SECRET'])

df = dataset.fetch(page_ids=['classy.org'], max_posts=100)
print("Downloaded %d posts" % len(df))
print("Columns: %s" % ", ".join(sorted(df.columns)))

from classypy.io.facebook import FacebookPostsDataset


def test_twitter_user_posts_dataset():
    dataset = FacebookPostsDataset(
        app_id='app_id', app_secret='app_secret')
    df = dataset.fetch(page_ids=[])
    assert 'original_page_id' in df.columns

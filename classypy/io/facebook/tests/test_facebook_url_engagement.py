from classypy.io.facebook import FacebookURLEngagementDataset


def test_twitter_user_posts_dataset():
    dataset = FacebookURLEngagementDataset(
        app_id='app_id', app_secret='app_secret')
    df = dataset.fetch(urls=[])
    assert 'original_id' in df.columns

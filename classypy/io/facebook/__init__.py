# Author: Ben Cipollini
# License: simplified BSD

import os
import re
import time
from collections import defaultdict
from datetime import datetime, timedelta

import numpy as np
import pandas as pd

from classypy.devops import logger
from classypy.io.core.datasets import Dataset
from classypy.io.core.fetchers import HttpApiFetcher
from classypy.util.dates import convert_datetime_columns
from classypy.util.execution import run_in_parallel


class FacebookApiFetcher(HttpApiFetcher):
    """Can improve here: https://developers.facebook.com/docs/graph-api/making-multiple-requests/"""
    dependencies = HttpApiFetcher.add_dependencies(facebook='facebook-sdk', requests='requests[secure]')
    POST_FIELDS = ('shares', 'story', 'message', 'link', 'created_time')
    REACTION_TYPES = ('like', 'love', 'wow', 'haha', 'sad', 'angry', 'pride', 'thankful')
    REACTION_FIELDS = tuple("reactions.type({rxn_upper}).limit(0).summary(total_count).as({rxn})"
                            .format(rxn_upper=rxn.upper(), rxn=rxn)
                            for rxn in REACTION_TYPES)
    POSTS_PER_PAGE = 50
    WAIT_TIME = 1  # seconds to wait on API rate limit error

    GRAPH_API_TOO_MANY_CALLS_CODE = 4  # exception code

    def __init__(self, data_dir=None, app_id=None, app_secret=None, version='2.7'):
        super(FacebookApiFetcher, self).__init__(data_dir=data_dir)

        app_id = app_id or os.environ.get("FACEBOOK_APP_ID")
        app_secret = app_secret or os.environ.get("FACEBOOK_APP_SECRET")
        if app_id is None or app_secret is None:
            raise ValueError("Must define FACEBOOK_APP_ID and "
                             "FACEBOOK_APP_SECRET environment variables, or "
                             "pass app_id and app_secret arguments.")

        self.access_token = "{app_id}|{app_secret}".format(
            app_id=app_id, app_secret=app_secret)
        self.version = version
        self.graph = None

    def get_graph(self):
        import facebook
        if not self.graph:
            self.graph = facebook.GraphAPI(
                access_token=self.access_token, version=self.version)
        return self.graph

    @classmethod
    def _parse_ids_from_error(cls, error_msg):
        """
        Returns:

        bad_ids, good_ids
        """
        ERROR_RES = [
            r'Some of the aliases you requested do not exist: (.*)$',
            r'Cannot query users by their username \(([^\)]+)\)', ]
        for expr in ERROR_RES:
            match = re.search(expr, str(error_msg))
            if match:
                bad_ids = match.groups()[0].split(',')
                good_ids = []
                return bad_ids, good_ids

        expr = r'Page ID ([0-9]+) was migrated to page ID ([0-9]+)'
        match = re.search(expr, str(error_msg))
        if match:
            old_id = match.groups()[0]
            new_id = match.groups()[1]
            return [old_id], [new_id]
        return None, None  # Nothing we can do to fix.

    def _get_object(self, fb_object, retries=3, **kwargs):
        import facebook

        logger.info("Fetching object '{fb_object}'".format(fb_object=fb_object))
        try:
            data = self.get_graph().get_object(fb_object, **kwargs)
        except facebook.GraphAPIError as ex:
            if ex.code == self.GRAPH_API_TOO_MANY_CALLS_CODE and retries > 0:
                # Rate limit; go slower.
                logger.warning("We got rate limited, let's sleep a bit...")
                time.sleep(self.WAIT_TIME)
                return self._get_object(fb_object=fb_object, retries=retries - 1, **kwargs)

            ALIAS_DOES_NOT_EXIST_ERROR = r'Some of the aliases you requested do not exist: (.*)$'
            match = re.search(ALIAS_DOES_NOT_EXIST_ERROR, str(ex))
            if match:
                logger.warning("Failed to find Facebook ID: %s" % match.groups()[0])
                return {}

            UNSUPPORTED_ERROR = r'Unsupported get request. Object with ID \'(.*)\' ' \
                'does not exist, cannot be loaded due to missing permissions, or does not support this operation.'
            match = re.search(UNSUPPORTED_ERROR, str(ex))
            if match:
                logger.warning("Could not fetch posts for Facebook ID: %s" % match.groups()[0])
                return {}

            raise
        return data

    def _get_objects(self, fb_objects, msg=None, retries=3, **kwargs):
        import facebook

        if len(fb_objects) == 0:
            # graph API chokes on empty set.
            return dict()

        logger.info("Fetching {num_objects} objects{msg}.".format(
            num_objects=len(fb_objects), msg='' if not msg else ' ' + msg))
        try:
            data = self.get_graph().get_objects(fb_objects, **kwargs)
        except facebook.GraphAPIError as ex:
            if ex.code == 4 and retries > 0:
                # Rate limit; go slower.
                logger.warning("We got rate limited, let's sleep a bit...")
                time.sleep(self.WAIT_TIME)
                return self._get_objects(
                    fb_objects=fb_objects, msg=msg, retries=retries - 1, **kwargs)

            # Eliminate any bad objects, and retry.
            bad_ids, good_ids = self._parse_ids_from_error(ex)
            if not bad_ids:
                # Nothing we can do to fix
                logger.info("Failed to find objects {fb_objects}".format(fb_objects=fb_objects))
                raise

            logger.error("Error while getting objects: {ex}".format(ex=str(ex)))
            data = self._get_objects(
                fb_objects=list(set(fb_objects) - set(bad_ids)) + good_ids,
                retries=retries - 1, msg=msg, **kwargs)
            return data

            # Fill in the gaps
            missing_ids = list(set(fb_objects) - set(data.keys()))
            logger.info("Filling in %d missing objects (of %d)" % (len(missing_ids), len(fb_objects)))
            data.update({id: defaultdict(lambda: None) for id in missing_ids})
        return data

    def fetch_posts(self, page_id, max_posts=np.inf, start_date=None,
                    end_date=None, force=False):
        """Returns dictionary where keys are post IDs and values are dictionaries containing post data."""
        import requests

        start_date = start_date if start_date else datetime.utcnow() - timedelta(days=30)
        end_date = end_date if end_date else datetime.utcnow()
        # Grab & clean posts
        raw_posts = self._get_object('{page_id}/posts'.format(page_id=page_id),
                                     since=start_date, until=end_date, limit=self.POSTS_PER_PAGE,
                                     fields=','.join(self.POST_FIELDS))
        all_posts = {}

        while len(all_posts) < max_posts:
            # Get reactions, and further posts,
            # Until we hit our limit or we're done.
            next_url = raw_posts.get('paging', {}).get('next')

            # Trim the page of posts returned so that we won't have more than max_posts
            raw_posts_data = raw_posts.get('data', [])
            posts = raw_posts_data[:min(len(raw_posts_data), max_posts - len(all_posts))]
            cur_posts = {post['id']: post for post in posts}

            # Grab & clean reactions
            try:
                reactions = self._get_objects(
                    list(cur_posts.keys()), fields=','.join(self.REACTION_FIELDS),
                    msg="for id {page_id}".format(page_id=page_id))
            except Exception as ex:  # noqa
                logger.error("Exception while fetching posts: {ex}.".format(ex=ex))
                break

            # Remove IDs that get returned in the reaction data
            for post_id in reactions:
                del reactions[post_id]['id']

            reactions = {post_id: {reaction_type: int(reaction_data['summary']['total_count'])
                                   for reaction_type, reaction_data in rxns.items()}
                         for post_id, rxns in reactions.items()}

            # Update posts with reactions
            for post_id, reaction_count_data in reactions.items():
                cur_posts[post_id].update(reaction_count_data)

                # Check that reaction data is set for all posts
                for reaction_type in self.REACTION_TYPES:
                    if not cur_posts[post_id].get(reaction_type):
                        cur_posts[post_id][reaction_type] = 0

            # Add page_id to posts, clean share data
            for post_id in cur_posts:
                share_count = cur_posts[post_id].get('shares', {}).get('count', 0)
                cur_posts[post_id].update({
                    'page_id': page_id,
                    'shares': share_count,
                })

            # Now add to the list.
            all_posts.update(cur_posts)

            # Get next batch
            if not next_url:
                break
            raw_posts = requests.get(next_url).json()

        return all_posts

    def fetch_engagement(self, ids, force=False):
        import facebook

        def fetch_fn(ids):
            """Closure to fetch data."""
            if ids is None or len(ids) == 0:
                return []

            # Grab & clean reactions
            try:
                objs = self._get_objects(ids, msg="engagement")
            except Exception as ex:  # noqa
                logger.error("Exception while fetching engagement: {ex}.".format(ex=ex))
                return []

            #
            def _get_object(key, obj):
                if re.match(r'^[0-9]+$', key):
                    # Numeric Facebook ID
                    cur_data = {'id': key, 'og_id': key}
                elif 'og_object' in obj:
                    # URL
                    cur_data = {'url': key, 'og_id': obj['og_object']['id']}
                else:
                    # Text-based Facebook ID
                    cur_data = {'id': key, 'og_id': obj['id']}

                # Fetch the summary data
                if not cur_data['og_id']:
                    logger.warning("Object ID not found: %s" % key)
                else:
                    try:
                        og_obj = self._get_object(
                            str(cur_data['og_id']),
                            fields="likes.summary(True),engagement")
                        cur_data['og_obj'] = og_obj
                    except facebook.GraphAPIError as ex:
                        logger.error("Facebook API error for {id}: {ex}.".format(id=cur_data['og_id'], ex=ex))
                return cur_data

            return run_in_parallel(
                func=_get_object,
                params=[{'key': key, 'obj': val} for key, val in objs.items()])

        return self.fetch_in_parts(
            ids=ids, force=force, fetch_fn=fetch_fn, max_ids_per_request=50)


class FacebookPostsDataset(Dataset):
    """Access to Facebook posts"""
    fetcher_class = FacebookApiFetcher
    COLUMNS = (
        'id', 'page_id', 'created_time', 'shares', 'like', 'love', 'wow',
        'haha', 'sad', 'angry', 'thankful', 'pride', 'message', 'story', 'link',
        'original_page_id',  # added in post-processing
    )
    DATETIME_COLUMNS = ('created_time',)

    @convert_datetime_columns(column_names=DATETIME_COLUMNS)
    def fetch(self, page_ids, start_date=None, end_date=None, max_posts=np.inf):
        """
        Parameters
        ----------
        report_ids : iterable
            Each element should be a numeric report ID, from Datanyze.
        """
        def fetch_posts(page_id):
            try:
                data = self.fetcher.fetch_posts(
                    page_id, start_date=start_date, end_date=end_date,
                    max_posts=max_posts)
            except Exception as ex:  # noqa
                logger.error("Exception during posts fetch: {ex}.".format(ex=ex))
                data = None  # will get filtered out later.

            if data:
                cur_df = pd.DataFrame(data=list(data.values()))
                cur_df['original_page_id'] = page_id
                return cur_df

        dfs = run_in_parallel(
            func=fetch_posts,
            params=[{'page_id': page_id} for page_id in page_ids])
        dfs = [df for df in dfs if df is not None]

        return pd.concat(dfs, sort=True) if dfs else pd.DataFrame(columns=self.COLUMNS)


class FacebookURLEngagementDataset(Dataset):
    fetcher_class = FacebookApiFetcher

    COLUMNS = ('other_engagement', 'num_posts', 'original_id')
    DATETIME_COLUMNS = tuple()

    @convert_datetime_columns(column_names=DATETIME_COLUMNS)
    def fetch(self, urls, force=False):
        """
        Parameters
        __________
        urls (list of strings):
            list of urls for which to get shares
        """
        # Fetch, store off the original ID
        url_data = self.fetcher.fetch_engagement(urls, force=force)
        for key, val in url_data.items():
            val['original_id'] = key

        augmentable_keys = [key for key, val in url_data.items() if val.get('og_obj') is not None]
        for key in augmentable_keys:
            # Update objects with data
            og_obj = url_data[key]['og_obj']
            if 'likes' in og_obj and 'summary' in og_obj['likes']:
                url_data[key]['likes'] = og_obj['likes']['summary']['total_count']
            if 'engagement' in og_obj:
                url_data[key]['other_engagement'] = og_obj['engagement']['count']
                if 'likes' in url_data[key]:
                    url_data[key]['other_engagement'] -= url_data[key]['likes']
        return pd.DataFrame(data=list(url_data.values())) if url_data else pd.DataFrame(columns=self.COLUMNS)

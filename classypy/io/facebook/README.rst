Datanyze

Notes
-----
This is a thin wrapper around the Facebook SDK. The SDK isn't super-intuitive, and knowing how to retrieve and massage data is nice to abstract out.

Content
-------
    :'posts': Facebook posts from a given Facebook page. Includes the post text, author data, as well as reaction info.

    :'engagement': engagement data (likes, other) from facebook about pages hosted on classy


Examples
----------

    :example1.py: Fetches 100 posts from the classy.org Facebook page, and returns a dataframe of all posts.

    :example2.py: Fetches engagement data for a 1 item list of urls consisting of the
    classy.org homepage and returns engagement data as a pandas dataframe
    dataframe.


References
----------

https://github.com/mobolic/facebook-sdk

"""
Fetch all Jira issues for a given installation over the past three days.
"""
from datetime import datetime, timedelta

from classypy.devops import find_secrets
from classypy.io.jira import JiraIssuesDataset, JiraTransitionsDataset

secrets = find_secrets()
kwargs = dict(host=secrets['JIRA_HOST'], username=secrets['JIRA_USERNAME'], passwd=secrets['JIRA_PASSWORD'])

dataset = JiraIssuesDataset(**kwargs)
df_issues = dataset.fetch(start_date=datetime.utcnow() - timedelta(days=3), max_issues=10)
print(df_issues)

dataset = JiraTransitionsDataset(**kwargs)
df_transitions = dataset.fetch(start_date=datetime.utcnow() - timedelta(days=3), max_issues=10)
print(df_transitions)

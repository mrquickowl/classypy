# Author: Ben Cipollini
# License: simplified BSD
import numpy as np
import pandas as pd

from classypy.devops import logger
from classypy.io.core.datasets import Dataset
from classypy.io.core.fetchers import Fetcher
from classypy.util.etl import flatten_dict


class JiraFetcher(Fetcher):
    dependencies = Fetcher.add_dependencies(jira='jira')

    # As of 2018-05-08, the JIRA search API only returns up to 100 entries from the changelog from its search API
    MAX_CHANGELOG_FROM_SEARCH = 100

    def __init__(self, data_dir=None, username=None, passwd=None, host=None):
        super(JiraFetcher, self).__init__(data_dir=data_dir)
        self.username = username
        self.passwd = passwd
        self.host = host

    def get_connection(self):
        """Returns a JIRA connection object."""
        import jira
        return jira.JIRA(server=self.host, basic_auth=(self.username, self.passwd))

    def get_issue_changelog(self, issue_id):
        """Get the changelog for an issue via the JIRA API."""
        conn = self.get_connection()

        changelog_entries = []

        resource_format = 'issue/{0}/changelog?startAt={1}'
        pos = 0
        # Get the first page so we can see the total number of entries
        changelog_page = conn.find(resource_format, ids=(issue_id, pos))

        while len(changelog_entries) < changelog_page.total:
            changelog_page = conn.find(resource_format, ids=(issue_id, pos))
            new_changelog_entries = changelog_page.raw['values']
            if len(new_changelog_entries) == 0:
                # Prevent an infinite loop if something goes wrong with our fetching
                raise ValueError('Changelog page fetched without any entries, aborting!')
            changelog_entries += changelog_page.raw['values']
            pos = len(changelog_entries)

        return changelog_entries

    def search_issues(self, query, fields, expand=None, batch_size=100, max_issues=None):
        """Given a query, will retrieve up to max_issues issues via the JIRA api."""
        conn = self.get_connection()

        issues = []
        pos = 0
        while max_issues is None or len(issues) < max_issues:
            # Loop until we fill issues
            # Compute the max # results to fetch.
            # If no max # issues, then use the batch size.
            # Otherwise, use either the batch size or the remaining # of issues we can fetch, whichever is smaller.
            max_results = batch_size if max_issues is None else min(batch_size, max_issues - len(issues))
            new_issues = conn.search_issues(
                query, startAt=pos, maxResults=max_results, expand=expand,
                fields=None if fields is None else ','.join(fields))

            if len(new_issues) == 0:
                break
            logger.info("Fetched issues {start} - {end}".format(
                start=len(issues) + 1, end=len(issues) + len(new_issues)))

            issues += new_issues
            pos += len(new_issues)

        return issues

    @staticmethod
    def expand_issue_changelog(issue):
        """Expands issue changelog into a dataframe."""
        rows = []
        for history in issue.raw['changelog']['histories']:
            for item in history['items']:
                # Grab issue data, combine with history and history item data.
                issue_data = {key: val for key, val in issue.raw.items() if key != 'changelog'}
                issue_data.update({
                    'history': {key: val for key, val in history.items() if key != 'items'},
                    'history.item': item,
                })
                rows.append(flatten_dict(issue_data))
        return pd.DataFrame(data=rows)

    @staticmethod
    def _generate_query(projects=None, start_date=None, end_date=None):
        """Generate JQL for the given inputs."""
        # Set up query conditions, based in inputs.
        query_conditions = []
        if projects:
            query_conditions.append("project IN ({projects_list})".format(
                projects_list=','.join(["'{proj}'".format(proj=proj) for proj in projects])))
        if start_date:
            query_conditions.append("""
                (updatedDate >= '{start_date:%Y-%m-%d %H:%M}'
                    OR createdDate >= '{start_date:%Y-%m-%d %H:%M}'
            )""".format(start_date=start_date))
        if end_date:
            query_conditions.append("""
                (updatedDate <= '{end_date:%Y-%m-%d %H:%M}'
                    AND createdDate <= '{end_date:%Y-%m-%d %H:%M}'
            )""".format(end_date=end_date))

        # Generate query
        query = " AND ".join(["({cond})".format(cond=cond) for cond in query_conditions])
        query += " ORDER BY updated"  # allows resuming

        return query

    def fetch_transitions(self, projects=None, start_date=None, end_date=None, max_issues=None, fields=None):
        """Fetch state transitions for the given issues."""
        fields = fields or 'key'
        query = self._generate_query(projects=projects, start_date=start_date, end_date=end_date)
        issues = self.search_issues(query, fields=fields, expand='changelog', max_issues=max_issues)

        dfs = []
        for ii, issue in enumerate(issues):
            if ii % 1000 == 0:
                logger.info("Expanding issues {ii_start} to {ii_end}".format(
                    ii_start=ii + 1, ii_end=min(ii + 1000, len(issues))))

            if len(issue.raw['changelog']['histories']) >= self.MAX_CHANGELOG_FROM_SEARCH:
                # The changelog is the max size the search endpoint will return, fetch using a different endpoint
                logger.debug("Changelog for {key} has more than {max_entries} changelog entries; refetching.".format(
                    key=issue.key, max_entries=self.MAX_CHANGELOG_FROM_SEARCH))
                changelog = self.get_issue_changelog(issue_id=issue.id)
                issue.raw['changelog']['histories'] = changelog

            dfs.append(self.expand_issue_changelog(issue))
        transitions = pd.concat(dfs, sort=True)

        return transitions[transitions['history.item.field'] == 'status']

    def fetch_issues(self, projects=None, start_date=None, end_date=None, max_issues=None, fields=None):
        """Fetch issues in the given range."""
        fetch_fields = None if fields is None else np.unique([f.split('.')[0] for f in fields])
        query = self._generate_query(projects=projects, start_date=start_date, end_date=end_date)

        # Execute query, via pagination.
        return self.search_issues(query, fields=fetch_fields, max_issues=max_issues)


class JiraIssuesDataset(Dataset):
    """Dataset of JIRA issues."""
    fetcher_class = JiraFetcher
    COLUMNS = (
        'id', 'assignee.name', 'created', 'description', 'issuelinks', 'key', 'labels',
        'priority.name', 'project.name', 'reporter.name', 'resolution.name', 'sprint.name',
        'status.name', 'summary', 'type.name', 'updated',
    )

    def fetch(self, projects=None, start_date=None, end_date=None, max_issues=None, fields=COLUMNS):
        issues = self.fetcher.fetch_issues(
            projects=projects, start_date=start_date, end_date=end_date,
            max_issues=max_issues, fields=fields)

        data = []
        for iss in issues:
            raw_data = flatten_dict(iss.raw['fields'])
            raw_data['id'] = iss.raw['id']
            raw_data['key'] = iss.raw['key']
            filtered_data = {field: raw_data.get(field) for field in fields}
            data.append(filtered_data)

        df = pd.DataFrame(data=data, columns=fields)
        # DPLAT-681: fetcher can return duplicates.
        df = df.drop_duplicates(subset=('id',))
        return df


class JiraTransitionsDataset(Dataset):
    """Dataset of JIRA issue state transitions."""
    fetcher_class = JiraFetcher
    COLUMNS = (
        'history.id', 'history.author.name', 'history.created',
        'history.item.fromString', 'history.item.toString',
        'key',
    )

    def fetch(self, projects=None, start_date=None, end_date=None, max_issues=None, fields=COLUMNS):
        transitions = self.fetcher.fetch_transitions(
            projects=projects, start_date=start_date, end_date=end_date,
            max_issues=max_issues, fields=fields)

        # LOOK-98: fetcher can return duplicates.
        transitions = transitions.drop_duplicates(subset=('history.id',))

        return transitions[list(fields)]

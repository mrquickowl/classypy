# Author: Ben Cipollini
# License: simplified BSD

import os

from ..core.datasets import HttpApiDataset


class DatanyzeDataset(HttpApiDataset):
    """TODO: Datanyze docstring"""

    COLUMN_MAP = {
        'Public (Yes/No)': 'Public', }

    def __init__(self, data_dir=None, api_email=None, api_secret=None):
        super(DatanyzeDataset, self).__init__(data_dir=data_dir)

        self.api_email = api_email or os.environ.get("DATANYZE_API_EMAIL")
        self.api_secret = api_secret or os.environ.get("DATANYZE_API_SECRET")
        if self.api_email is None or self.api_secret is None:
            raise ValueError("Must define DATANYZE_API_EMAIL and "
                             "DATANYZE_API_SECRET environment variables, or "
                             "pass api_email and api_secret arguments.")

    def fetch(self, report_ids, force=False, verbose=1):
        """
        Parameters
        ----------
        report_ids : iterable
            Each element should be a numeric report ID, from Datanyze.
        force : boolean
            When False (default), previously downloaded data are reused.
            When True, reports are downloaded at runtime.
        verbose: int
            Level of output.
        """
        # URLs have secrets in them, so specify a filename to save to.
        src_urls = [('%s.json' % rid, self._generate_report_url(rid), {'uncompress': True})
                    for rid in report_ids]
        out_dfs = super(DatanyzeDataset, self).fetch(src_urls, force=force, verbose=verbose)
        return out_dfs

    def _generate_report_url(self, report_id):
        return ("http://api.datanyze.com/targeting/"
                "?email={email}&token={token}&report_id={report_id}&type=web").format(
                    email=self.api_email, token=self.api_secret, report_id=report_id)

Datanyze

Notes
-----
Datanyze is a paid service for understanding what websites use what technologies.


Content
-------
    :'reports': user-defined reports (JSON). This fetcher focuses on reports as it allows 200k results at a time, rather than 100 per other endpoints.


Examples
----------

    :example1.py: Fetches an example report, parses it, and reports on the # of results.


References
----------

http://www.datanyze.org

"""
Module for running SQL queries locally or via a SSH tunnel.

Currently contains a function that generates a
function that connects to MySQL DB via SSH-tunneling
and returns query results against that DB in a pandas
DataFrame.
"""
import functools
import os.path as op
import re

import numpy as np
import pandas as pd

from classypy.devops import logger
from classypy.devops.tunnel import apply_tunnel
from classypy.query.redshift import DDL_STATEMENTS
from classypy.text import text_from_disk
from classypy.util.caching import cache_dataframe
from classypy.util.dirs import get_caller_path, src_dir

AUTOCOMMIT_STATEMENTS = ('ANALYZE', 'CREATE OR REPLACE FUNCTION', 'CREATE OR REPLACE LIBRARY', 'VACUUM')


def _sql_starts_with(q, statement):
    q = q.strip()  # Strip whitespace
    q = re.sub(r'^/\*.*\*\/\s*', r'', q)  # Strip docstring comments
    return q.upper().startswith(statement.upper())


def echo_query(func):
    """Prints query specified in `func`."""
    @functools.wraps(func)
    def echo_query_inner_func(q, echo=None, *args, **kwargs):
        echo = echo if echo is not None else logger.logsLevel(logger.INFO)
        if echo:
            logger.info(q)
        return func(q, *args, **kwargs)
    return echo_query_inner_func


def handle_errors(*args, **kwargs):
    """Dual-purpose decorator: can take a kwarg, can take a func"""
    def handle_errors_decorator(func):
        @functools.wraps(func)
        def handle_errors_inner_func(q, catch_errors=False, *args, **kwargs):
            try:
                return func(q, *args, **kwargs)
            except Exception as ex:  # noqa
                if not catch_errors:
                    raise
                else:
                    logger.error("Exception handled: {ex}".format(ex=str(ex)))
        return handle_errors_inner_func

    return handle_errors_decorator if kwargs else handle_errors_decorator(*args)


def prepare_query(query, params=None):
    """Checks that if parameters are passed that they use named arguments, and escapes percent signs."""
    # Grab all named argument placeholders, they look like: `%(some_argument)s`
    named_arguments = set(re.findall(r'%(?=\((.+?)\)s)', query))

    if params is not None:
        if not isinstance(params, dict):
            # Avoid `%s` as the placeholder for parameters by requiring named arguments
            # `%s` is hard to use because it appears in normal queries, `%(some_argument)s` is rare!
            # See: http://initd.org/psycopg/docs/usage.html#passing-parameters-to-sql-queries
            raise ValueError('Only named arguments are supported; params must be a dict.')

        # Now check that we have the parameters we need
        missing_arguments = set(named_arguments) - set(params)
        if missing_arguments:
            raise ValueError("Not all named arguments were provided in params, "
                             "missing: {missing}".format(missing=', '.join(missing_arguments)))

        # python 3: int64
        for key, val in params.items():
            if isinstance(val, np.int64):
                params[key] = int(val)  # psycopg2 can't deal with int64

    elif named_arguments:
        raise ValueError("Named arguments found but no params provided: {args}".format(args=named_arguments))

    # Substitute percent signs (that are not part of named arguments) with double percent signs to escape them
    # See: http://stackoverflow.com/questions/8657508
    return re.sub(r'%(?!\(.+\)s)', '%%', query)


def redshift_prepare_query(func):
    @functools.wraps(func)
    def redshift_prepare_query_inner_func(q, *args, **kwargs):
        q = prepare_query(query=q, params=kwargs.get('params'))
        return func(q, *args, **kwargs)
    return redshift_prepare_query_inner_func


def get_engine(engine, prefix=None, **secrets):
    prefix = prefix or engine

    if engine.upper() == 'MYSQL':
        import pymysql as db  # reduce difficult dependencies.
        engine = db.connect(
            host=secrets['MYSQL_HOST'],
            port=int(secrets['MYSQL_PORT']),
            user=secrets['MYSQL_USERNAME'],
            passwd=secrets['MYSQL_PASSWORD'],
            db=secrets['MYSQL_DATABASE'])

    elif engine.upper() == 'REDSHIFT':
        from sqlalchemy import create_engine  # reduce difficult dependencies
        connection_str = (
            (
                'postgresql://{REDSHIFT_USERNAME}:{REDSHIFT_PASSWORD}'
                '@{REDSHIFT_HOST}:{REDSHIFT_PORT}'
                '/{REDSHIFT_DATABASE}'
            )
            .replace("REDSHIFT", prefix)
            .format(**secrets)
        )
        engine = create_engine(connection_str)

    elif engine.upper() == 'POSTGRES':
        from sqlalchemy import create_engine  # reduce difficult dependencies
        connection_str = (
            (
                'postgresql://{POSTGRES_USERNAME}:{POSTGRES_PASSWORD}'
                '@{POSTGRES_HOST}:{POSTGRES_PORT}'
                '/{POSTGRES_DATABASE}'
            )
            .replace("POSTGRES", prefix)
            .format(**secrets)
        )
        engine = create_engine(connection_str)

    else:
        raise NotImplementedError("Unknown engine: {engine}".format(engine=engine))

    return engine


def generate_query_func(engine, prefix=None, retries=0, **connection_params):
    """
    Return a closure that can query a MySQL database through an SSH-tunnel or locally.

    See https://sshtunnel.readthedocs.io/en/latest/ for
    implementation/usage details.

    Parameters
    ----------
    engine : string
        SQL engine (redshift or mysql)

    prefix : string
        Denotes prefix of secrets to use for specified engine.
        (e.g. REDSHIFT or MYSQL). Default: engine.upper() + "_"

    retries : int
        The number of times to retry queries that are run using the returned
        query function. Default: 0

    connection_params : dict
        Dictionary containing connection details, see below
        for required arguments.

        Required keys --
            - MYSQL_USERNAME
            - MYSQL_DATABASE
            - MYSQL_HOST
            - MYSQL_PORT

        Required keys (ssh tunneling only) --
            - SSH_HOST
            - SSH_USERNAME
            - SSH_PEM

        Optional keys (ssh tunneling only) --
            - SSH_PORT
            - LOCAL_HOST
            - LOCAL_PORT

    Returns
    -------
    query_prod : func
        Function that takes a str and passes it to a pd.DataFrame
        which passes to a MySQL DB connection where it is
        use as a query against the connected DB. The MySQL DB
        connection is severed after the query is made.
    """
    prefix = prefix or engine.upper()

    if retries > 0:
        # Recursive case: wrap function in a retry layer.
        def retries_query_func(query, retries=retries, **kwargs):
            inner_query_func = generate_query_func(
                engine=engine, prefix=prefix, retries=0, **connection_params)

            try:
                return inner_query_func(query, **kwargs)
            except Exception as ex:  # noqa
                if retries <= 0:
                    raise
                logger.error("Exception while running query: {ex}".format(ex=ex))
                return retries_query_func(query=query, retries=retries - 1, **kwargs)
        return retries_query_func

    if engine.upper() == 'MYSQL':
        # base case #1: do the connection
        @echo_query
        @handle_errors
        @apply_tunnel(prefix=prefix)
        def mysql_query_func(q, params=None, **secrets):
            import pymysql as db  # reduce difficult dependencies.
            conn = db.connect(
                host=secrets['MYSQL_HOST'],
                port=int(secrets['MYSQL_PORT']),
                user=secrets['MYSQL_USERNAME'],
                passwd=secrets['MYSQL_PASSWORD'],
                db=secrets['MYSQL_DATABASE'])
            try:
                table = pd.read_sql_query(q, conn, params=params)
            finally:
                conn.close()
            return table
        return functools.partial(mysql_query_func, prefix=prefix, **connection_params)

    elif engine.upper() == 'REDSHIFT':
        @echo_query
        @handle_errors
        @redshift_prepare_query
        @apply_tunnel(prefix=prefix)
        def redshift_query_func(q, params=None, command=None, **secrets):
            if command is None:
                # Guess
                command = any([_sql_starts_with(q, statement=ddl) for ddl in DDL_STATEMENTS])

            if not command:
                import sqlalchemy
                from sqlalchemy import create_engine  # reduce difficult dependencies
                connection_str = (
                    (
                        'postgresql://{REDSHIFT_USERNAME}:{REDSHIFT_PASSWORD}'
                        '@{REDSHIFT_HOST}:{REDSHIFT_PORT}'
                        '/{REDSHIFT_DATABASE}'
                    )
                    .replace("REDSHIFT", prefix)
                    .format(**secrets)
                )
                engine = create_engine(connection_str)

                try:
                    return pd.read_sql_query(q, engine, params=params)
                except sqlalchemy.exc.ResourceClosedError as rce:
                    if 'This result object does not return row' in getattr(rce, 'message', ''):
                        return

            if command:
                # Could get here from previous failure; could get here from command-line arg.
                from shiftmanager import Redshift

                manager = Redshift(
                    host=secrets['REDSHIFT_HOST'],
                    port=secrets['REDSHIFT_PORT'],
                    user=secrets['REDSHIFT_USERNAME'],
                    password=secrets['REDSHIFT_PASSWORD'],
                    database=secrets['REDSHIFT_DATABASE'],)

                autocommit = any([_sql_starts_with(q, statement=ddl) for ddl in AUTOCOMMIT_STATEMENTS])
                manager.connection.set_session(autocommit=autocommit)

                return manager.execute(q, parameters=params)

        def redshift_query_func_with_etl_retries(query, redshift_retries=3, **kwargs):
            """Retry wrapper to deal with transient redshift ETL errors."""
            try:
                return redshift_query_func(query, prefix=prefix, **kwargs)
            except Exception as ex:  # noqa
                if redshift_retries > 0 and re.search(r"table \d+ dropped by concurrent transaction", str(ex)):
                    # Special exception: redshift ETL errors should always be retried... but add a count, just in case.
                    return redshift_query_func_with_etl_retries(
                        query=query, redshift_retries=redshift_retries - 1, **kwargs)
                # Unknown errors get re-raised
                raise

        return functools.partial(redshift_query_func_with_etl_retries, **connection_params)

    elif engine.upper() == 'POSTGRES':
        @echo_query
        @handle_errors
        @apply_tunnel(prefix=prefix)
        def postgres_query_func(q, params=None, **secrets):
            import sqlalchemy
            from sqlalchemy import create_engine  # reduce difficult dependencies
            connection_str = (
                (
                    'postgresql://{POSTGRES_USERNAME}:{POSTGRES_PASSWORD}'
                    '@{POSTGRES_HOST}:{POSTGRES_PORT}'
                    '/{POSTGRES_DATABASE}'
                )
                .replace("POSTGRES", prefix)
                .format(**secrets)
            )
            engine = create_engine(connection_str)

            try:
                return pd.read_sql_query(q, engine, params=params)
            except sqlalchemy.exc.ResourceClosedError as rce:
                if 'This result object does not return row' in getattr(rce, 'message', ''):
                    return

        return functools.partial(postgres_query_func, prefix=prefix, **connection_params)

    else:
        raise NotImplementedError("Unknown engine: {engine}".format(engine=engine))


@cache_dataframe
def import_data(qf, query_name, query_dir=None, kwargs=None, retries=3):
    """
    Import data using SQL query, returns dataframe. Saves dataframe if csv_file is provided.

    Parameters
    ----------
    qf : func (query.generate_query_func)
        Closure that can query a MySQL database through an SSH-tunnel or locally.

    query_name : string
        Schema name to be queried.

    query_dir : string
        Location of query_name. If not given, defaults to the `data` directory in the
        caller's path.

    retries: int
        Maximum number of attempts before failing (default: 3)

    Returns
    -------
    df : Dataframe
    """
    kwargs = kwargs or {}
    query_dir = query_dir or src_dir(path=get_caller_path(), subdir="data")

    # Load the query
    query_filename = "{query_name}{template_extension}.sql".format(
        query_name=query_name,
        template_extension='.template' if kwargs else '')
    query = text_from_disk(op.join(query_dir, query_filename))

    # Format and run the query
    query = query.format(**kwargs)
    logger.info("Running query {query_name}".format(query_name=query_name))
    # terminate if impossibly long query is run
    df = qf(query=query, retries=retries)

    return df

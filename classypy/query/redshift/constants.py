
DDL_STATEMENTS = (
    'ALTER', 'ANALYZE', 'CREATE', 'DISTINCT', 'UPDATE', 'VACUUM',
)
DDL_KEYWORDS = (
    'AND', 'AS', 'BEGIN', 'BETWEEN', 'CASE',
    'CREATE INDEX', 'CREATE TABLE',
    'CROSS JOIN', 'CURRENT ROW', 'DESC',
    'DROP TABLE', 'DROP TABLE IF EXISTS',
    'ELSE', 'END', 'END)', 'FALSE', 'FROM', 'GROUP BY', 'HAVING',
    'ILIKE', 'IN', 'INTERVAL', 'IS',
    'JOIN', 'LEFT JOIN', 'LIKE', 'LIMIT',
    'NOT', 'NULL', 'ON', 'OR', 'ORDER BY', 'OVER',
    'PARTITION BY', 'PRECEDING', 'RANK', 'ROWS BETWEEN', 'ROWS UNBOUNDED',
    'SELECT', 'THEN', 'TRUE', 'UNION', 'UNION ALL',
    'WHEN', 'WHERE', 'WITH',
    # extract parts
    # 'DATE', 'MONTH', 'HOUR',
    'ARRAY',  # postgres
)
SYMBOLIC_OPERATORS = (
    '=', '!=', '<', '>', '<=', '>=', '::',
)
TYPES = (
    'BIGINT', 'BOOLEAN', 'FLOAT', 'INTEGER', 'TIMESTAMP', 'VARCHAR',
)
FUNCTION_NAMES = (
    'ABS', 'AVG',
    'CAST', 'COALESCE', 'CONVERT_TZ', 'COUNT', 'CROSS',
    'DATE_PART', 'DATE_ADD', 'DATE_DIFF',
    'DATE', 'DATE_TRUNC', 'DISTINCT', 'EXTRACT',
    'GETDATE', 'GREATEST', 'GROUP_CONCAT',
    'IFNULL', 'INTERVAL',
    'LAG', 'LAST_DAY', 'LEAD', 'LEAST', 'LEFT', 'LEN',
    'LISTAGG', 'LOG', 'LN', 'LOWER', 'LPAD',
    'MAX', 'MEDIAN', 'MIN',
    'NOW', 'NULLIF', 'POWER',
    'RIGHT', 'ROUND', 'ROW_NUMBER',
    'SUBSTRING', 'SUM', 'TO_DATE', 'TRUNC',
    'IF', 'IFNULL', 'MONTH', 'YEAR',  # MySQL ONLY
    'DECODE',  # Redshift only
    'ARRAY_AGG', 'ARRAY_POSITION', 'ARRAY_UPPER', 'UNNEST',  # postgres
)
DATE_FUNCTION_NAMES_WITH_UNDERSCORES = (
    # Use underscores, as function-specific logic below
    # Will make sure all align without the underscore.
    'DATE_PART', 'DATE_ADD', 'DATE_DIFF'
)
DATE_KEYWORDS = ('YEAR', 'MONTH', 'DAY')

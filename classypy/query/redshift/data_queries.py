"""Redshift convenience functions."""


def _extract_row_count(qf, query, catch_errors=False):
    """
    Extract the single row count value from a query.

    Parameters
    -----------
    qf : func (query.generate_query_func)
        Closure that can query a Redshift database through an SSH-tunnel or locally.
    query : str
        Schema name to be queried
    catch_errors : bool
        Return 0 on error if True

    Returns
    --------
        int: # of rows in the table.
    """
    try:
        result = qf(query)
        return result.iloc[0][0]
    except:  # noqa: E722
        # TODO: specify list of possible errors.
        if catch_errors:
            return 0
        raise


def row_count(qf, schema_name, table_name):
    """
    Check # of rows in a table. Defaults to zero if table doesn't exist.

    Parameters
    -----------
    qf : func (query.generate_query_func)
        Closure that can query a Redshift database through an SSH-tunnel or locally.
    schema_name : str
        Schema name to be queried
    table_name : str
        Schema name to be queried

    Returns
    --------
        int: # of rows in the table.
    """
    return _extract_row_count(qf, catch_errors=True, query="""
        SELECT
            COUNT(*)
        FROM "{schema_name}"."{table_name}"
    """.format(schema_name=schema_name, table_name=table_name))


def user_exists(qf, username):
    """
    Check if a given user exists in the Redshift database.

    Parameters
    -----------
    qf : func (query.generate_query_func)
        Closure that can query a Redshift database through an SSH-tunnel or locally.
    username : str
        Username to be queried for existence.

    Returns
    --------
        Boolean: Whether or not the user exists in the Redshift database.
    """
    return 0 < _extract_row_count(qf, query="""
        SELECT
            COUNT(*)
        FROM pg_user
        WHERE usename = '{username}'
    """.format(username=username))


def group_exists(qf, group_name):
    """
    Check if a group exists in the Redshift database.

    Parameters
    -----------
    qf : func (query.generate_query_func)
        Closure that can query a Redshift database through an SSH-tunnel or locally.
    group_name : str
        Name of group to be queried for existence.

    Returns
    --------
        Boolean: Whether or not the group exists in the Redshift database.
    """
    return 0 < _extract_row_count(qf, query="""
        SELECT
            COUNT(*)
        FROM pg_group
        WHERE groname = '{group_name}'
    """.format(group_name=group_name))


def existing_groups(qf, group_names):
    """
    Check if a set of groups exist in the Redshift database.

    Parameters
    -----------
    qf : func (query.generate_query_func)
        Closure that can query a Redshift database through an SSH-tunnel or locally.
    group_name : str
        Name of group to be queried for existence.

    Returns
    --------
        list of strings: which groups exist.
    """
    df = qf("""
        SELECT
            groname
        FROM pg_group
        WHERE groname IN ({group_names})
        GROUP BY groname
    """.format(group_names=", ".join(["'{gn}'".format(gn=gn) for gn in group_names])))
    return [gn for gn in group_names if gn in df['groname'].values]


def schema_exists(qf, schema_name):
    """
    Check if a schema exists in a given Redshift schema.

    Parameters
    -----------
    qf : func (query.generate_query_func)
        Closure that can query a Redshift database through an SSH-tunnel or locally.
    schema_name : str
        Name of schema

    Returns
    --------
        Boolean: Whether or not the schema exists in Redshift.
    """
    return 1 == _extract_row_count(qf, query="""
        SELECT
          COUNT(*)
        FROM pg_namespace
        WHERE nspname = '{schema_name}';
    """.format(schema_name=schema_name))


def table_exists(qf, schema_name, table_name):
    """
    Check if a table exists in a given Redshift schema.

    Parameters
    -----------
    qf : func (query.generate_query_func)
        Closure that can query a Redshift database through an SSH-tunnel or locally.
    schema_name : str
        Name of schema associated with queried table.
    table_name : str
        Name of table to be queried for existence.

    Returns
    --------
        Boolean: Whether or not the table exists in the Redshift schema.
    """
    return 1 == _extract_row_count(qf, query="""
        SELECT
          COUNT(*)
        FROM pg_tables
        WHERE schemaname = '{schema_name}'
          AND tablename = '{table_name}';
    """.format(schema_name=schema_name, table_name=table_name))


def tables_in_schema(qf, schema_name, owner=None):
    """List all tables that exist in a given Redshift schema.

    Parameters
    -----------
    qf : func (query.generate_query_func)
        Closure that can query a Redshift database through an SSH-tunnel or locally.
    schema_name : str
        Name of schema
    owner : str (optional)
        Name of user who is the owner of the tables

    Returns
    --------
        np.Array: Whether or not the tables exist in the Redshift schema.
    """
    df = qf("""
        SELECT
          tablename
        FROM pg_tables
        WHERE schemaname ILIKE '{schema_name}'
        {owner_clause}
        ORDER BY tablename;
    """.format(
        schema_name=schema_name,
        owner_clause='' if owner is None else "AND tableowner ILIKE '{owner}'".format(owner=owner)))
    return df['tablename'].values


def users_in_group(qf, group_name):
    """Return the usernames that exist within a group.

    Parameters
    -----------
    qf : func (query.generate_query_func)
        Closure that can query a Redshift database through an SSH-tunnel or locally.
    group_name : str
        Name of group.

    Returns
    --------
        list of strings: users in group
    """
    # See: https://github.com/awslabs/amazon-redshift-utils/blob/master/src/AdminViews/v_get_users_in_group.sql
    df = qf("""
        SELECT pg_user.usename
        FROM pg_group, pg_user
        WHERE pg_user.usesysid = ANY(pg_group.grolist)
          AND pg_group.groname = '{group_name}'
    """.format(group_name=group_name))
    return df['usename'].values

"""
Redshift convenience functions.
"""
import re
import shlex


def dummy_qf():
    """Return qf that echoes the query back. Good for verification or query building."""
    return lambda q, **kwargs: q


def join_queries(queries):
    """Combine queries / statements to execute as a single SQL block."""
    return "\n;".join(queries)


def split_queries(query):
    """Split queries from a single SQL block, into a list of queries."""
    queries = query.split(";\n\n")
    if len(queries) > 0:
        queries = [q + "\n" for qi, q in enumerate(queries) if qi < len(queries) - 1] + queries[-1:]
    return queries


def strip_atomic_statements(query):
    """Remove BEGIN/COMMIT on query block, if present."""
    query = re.sub(r'BEGIN\s*;', '', query)
    query = re.sub(r'COMMIT\s*;', '', query)
    query = re.sub(r';\s+;', ';\n', query, re.M | re.S)  # Remove consecutive semicolons
    return query


def atomize_queries(queries, strip=True):
    """Create transaction block to execute all given queries.

    Parameters
    ----------
    queries : list of strings
        List of queries
    strip : bool (optional)
        Strip out pre-existing transaction begin/commit, to create a larger block.
        If False and begin/commit exist, an error will be thrown.


    Returns
    -------
    query : string
        Single-block query wrapped in transaction DDL
    """
    query = join_queries(queries)

    if strip:
        query = strip_atomic_statements(query)
    else:
        assert re.match(r'BEGIN\s*;', query, re.I | re.M) is None, "Cannot atomize query with transactions."
        assert re.match(r'COMMIT\s*;', query, re.I | re.M) is None, "Cannot atomize query with transactions."

    return "BEGIN; {query}; COMMIT;".format(query=query)


def parse_fully_qualified_table_name(fully_qualified_table_name, require_schema_name=False):
    """Take a dotted table name and return the schema and table names as a tuple."""
    tokens = [t for t in shlex.shlex(fully_qualified_table_name) if t != '.']
    if len(tokens) not in (1, 2):
        raise ValueError("Could not parse input value.")
    if len(tokens) == 1 and require_schema_name:
        raise ValueError("No schema name specified.")
    if len(tokens) == 1:
        return None, tokens[0]
    return tokens

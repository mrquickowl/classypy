/* Drops duplicate rows by re-creating a table (all rows) */

-- Create temp table, with row numbers
CREATE TEMP TABLE "{temp_name_new}" AS
  SELECT
    *,
    ROW_NUMBER() OVER (PARTITION BY {column_names}) AS __row_num__
  FROM "{schema_name}"."{table_name}"

;  -- Delete all rows except the first of every distinct set, then drop count column
DELETE FROM "{temp_name_new}"
  WHERE __row_num__ != 1;
ALTER TABLE "{temp_name_new}" DROP COLUMN __row_num__

;  -- Delete old data, and re-insert
DELETE FROM "{schema_name}"."{table_name}";
INSERT INTO "{schema_name}"."{table_name}"
  SELECT * FROM "{temp_name_new}"

;  -- Clear temp table.
DROP TABLE "{temp_name_new}";

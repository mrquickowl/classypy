/* 
  Creates duplicate rows, copying/destroying only rows related to duplicates.
  Limitations: columns computing duplicates on must be able to be cast to VARCHAR (i.e. not boolean)
*/

-- Create temp table, with row numbers
CREATE TEMP TABLE "{temp_name_new}" AS
  WITH duplicate_rows AS (
    -- Temp table with row numbers
    SELECT {computed_unique_id} AS _temp_id
    FROM "{schema_name}"."{table_name}"
    GROUP BY _temp_id
    HAVING COUNT(*) > 1
  )
  SELECT
    src.*,
    dupes._temp_id,
    ROW_NUMBER() OVER (PARTITION BY {computed_unique_id}) AS __row_num__
  FROM "{schema_name}"."{table_name}" src
  JOIN duplicate_rows dupes
    ON dupes._temp_id = {computed_unique_id}

;  -- Delete all rows except the first of every distinct set
DELETE FROM "{temp_name_new}"
  WHERE __row_num__ != 1

;  -- Delete duplicated data
DELETE FROM "{schema_name}"."{table_name}"
  WHERE {computed_unique_id} IN (SELECT _temp_id FROM "{temp_name_new}")

;  -- Drop temp columns
ALTER TABLE "{temp_name_new}" DROP COLUMN __row_num__;
ALTER TABLE "{temp_name_new}" DROP COLUMN _temp_id

;  -- Insert deduped data back into the source table.
INSERT INTO "{schema_name}"."{table_name}"
  SELECT * FROM "{temp_name_new}"

;  -- Clear temp table.
DROP TABLE "{temp_name_new}";

import boto3


def fetch_temporary_credentials(user, database, cluster_identifier, credentials_duration_in_seconds=3600):
    """Fetches temporary credentials that can be used to connect to Redshift.

    Parameters
    ----------
    user : str
        The username to fetch temporary credentials as.
    database : str
        The Redshift database to fetch temporary credentials for.
    cluster_identifier : str
        The Redshift cluster identifier to fetch temporary credentials for.
    credentials_duration_in_seconds : int, optional
        The number of seconds the fetched credentials will be valid.
        Valid values are 900 (15 minutes) to 3600 (60 minutes). (Default: 3600)
    """
    redshift_client = boto3.client('redshift')
    temporary_creds = redshift_client.get_cluster_credentials(
        DbUser=user,
        DbName=database,
        ClusterIdentifier=cluster_identifier,
        DurationSeconds=credentials_duration_in_seconds
    )

    # Change key names to follow our standards
    return {
        'redshift_username': temporary_creds['DbUser'],
        'redshift_password': temporary_creds['DbPassword'],
        'expiration': temporary_creds['Expiration'],
        'metadata': temporary_creds['ResponseMetadata'],
    }

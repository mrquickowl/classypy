"""Redshift convenience functions."""
import os.path as op
import random
import re

from classypy.devops import logger
from classypy.query.redshift.base import atomize_queries, dummy_qf
from classypy.query.redshift.data_queries import existing_groups
from classypy.text import generate_random_string, text_from_disk
from classypy.util.dirs import this_files_dir

ADMIN_USERS_GROUP = 'admin_users'
DATA_SCIENTIST_GROUP = 'data_scientists'
ETL_GROUP = 'etls'
EXTERNAL_SOURCES_GROUP = 'external_sources'

ETL_REDSHIFT_USER = 'etl_app'

REDSHIFT_USER_GROUPS = (
    ETL_GROUP,  # Needs to go first in grant_permissions in order for ALTER DEFAULT PRIVILEGES to work
    ADMIN_USERS_GROUP,
    DATA_SCIENTIST_GROUP,
    EXTERNAL_SOURCES_GROUP
)


def grant_permissions(qf, schema_name, table_name=None, default_privileges_target_user=ETL_REDSHIFT_USER, env=None):
    """Grant permissions to schemas.

    Parameters
    ----------
    qf : func (query.generate_query_func)
        Closure that can query a Redshift database through an SSH-tunnel or locally.
    schema_name : string
        Schema to grant permissions to.
    table_name : string (optional)
        Table to grant permissions to.
    default_privileges_target_user : string (optional)
        The name of the user for which default privileges are defined (default: ETL_REDSHIFT_USER)
        See https://docs.aws.amazon.com/redshift/latest/dg/r_ALTER_DEFAULT_PRIVILEGES.html
    env : string (optional)
        The environment to grant permissions to. Used to control write access.
    """
    env = env or 'prod'  # treat None like prod, since it's more restrictive
    assert env in ('prod', 'staging')

    groups = existing_groups(qf, group_names=REDSHIFT_USER_GROUPS)

    queries = []

    # Grant permissions to schema
    for group in groups:
        grant_write_permissions = group in (ADMIN_USERS_GROUP, ETL_GROUP)
        grant_write_permissions |= group in (DATA_SCIENTIST_GROUP,) and env == 'staging'

        if table_name is None:
            # Grant schema-level permissions

            if grant_write_permissions:
                queries.append('GRANT ALL ON SCHEMA "{schema_name}" TO GROUP "{group}"'.format(
                    schema_name=schema_name, group=group))
                queries.append('GRANT ALL ON ALL TABLES IN SCHEMA "{schema_name}" TO GROUP "{group}"'.format(
                    schema_name=schema_name, group=group))
                queries.append(
                    'ALTER DEFAULT PRIVILEGES FOR USER {user} IN SCHEMA "{schema_name}" '
                    'GRANT ALL ON TABLES TO GROUP "{group}"'.format(
                        user=default_privileges_target_user, schema_name=schema_name, group=group))

            queries.append('GRANT USAGE ON SCHEMA "{schema_name}" TO GROUP "{group}"'.format(
                schema_name=schema_name, group=group))
            queries.append('GRANT SELECT ON ALL TABLES IN SCHEMA "{schema_name}" TO GROUP "{group}"'.format(
                schema_name=schema_name, group=group))
            queries.append(
                'ALTER DEFAULT PRIVILEGES FOR USER {user} IN SCHEMA "{schema_name}" '
                'GRANT SELECT ON TABLES TO GROUP "{group}"'.format(
                    user=default_privileges_target_user, schema_name=schema_name, group=group))

        else:
            # Grant table-level permissions

            if grant_write_permissions:
                queries.append('GRANT ALL ON TABLE "{schema_name}"."{table_name}" TO GROUP "{group}"'.format(
                    schema_name=schema_name, table_name=table_name, group=group))

            queries.append('GRANT SELECT ON TABLE "{schema_name}"."{table_name}" TO GROUP "{group}"'.format(
                schema_name=schema_name, table_name=table_name, group=group))

            # All ETL'd tables should be owned by ETL Redshift user
            queries.append('ALTER TABLE "{schema_name}"."{table_name}" OWNER TO {etl_user}'.format(
                schema_name=schema_name, table_name=table_name, etl_user=ETL_REDSHIFT_USER))

    query = atomize_queries(queries)
    return qf(query, command=True)


def create_schema(qf, schema_name, owner=None):
    """
    Create a schema in the Redshift database.

    Parameters
    -----------
    qf : func (query.generate_query_func)
        Closure that can query a Redshift database through an SSH-tunnel or locally.
    schema_name : str
        Name of schema to be created in Redshift.
    owner : str (optional)
        Name of database user to be the owner of the schema

    Returns
    --------
        None
    """
    authorization = "AUTHORIZATION {username}".format(username=owner) if owner is not None else ''
    return qf("""CREATE SCHEMA IF NOT EXISTS "{schema_name}" {authorization}""".format(
        schema_name=schema_name, authorization=authorization))


def drop_table(qf, schema_name, table_name, cascade=False):
    """Drop a table from a given redshift schema (if exists)."""
    return qf("""DROP TABLE IF EXISTS "{schema_name}"."{table_name}" {cascade}""".format(
        schema_name=schema_name, table_name=table_name, cascade="CASCADE" if cascade else ""))


def create_view(qf, schema_name, view_name, query, use_schema_binding=True):
    """Create a view within the given schema."""
    return qf("""
        CREATE VIEW "{schema_name}"."{view_name}"
        AS {query}
        {schema_binding}
    """.format(
        schema_name=schema_name,
        view_name=view_name,
        query=query,
        schema_binding='' if use_schema_binding else 'WITH NO SCHEMA BINDING'
    ))


def drop_view(qf, schema_name, view_name, cascade=False):
    return qf("""DROP VIEW IF EXISTS "{schema_name}"."{view_name}" {cascade}""".format(
        schema_name=schema_name, view_name=view_name, cascade="CASCADE" if cascade else ""))


def create_table_as(qf, query, schema_name, table_name, table_attributes=None):
    return qf("""
        CREATE TABLE "{schema_name}"."{table_name}"
        {table_attributes}
        AS (
            {query}
        )
    """.format(query=query, schema_name=schema_name, table_name=table_name, table_attributes=table_attributes or ''))


def create_table_from_table(qf, source_schema_name, source_table_name, target_schema_name, target_table_name):
    return create_table_as(
        qf=qf,
        query="""SELECT * FROM "{source_schema_name}"."{source_table_name}" """.format(
            source_schema_name=source_schema_name, source_table_name=source_table_name),
        schema_name=target_schema_name, table_name=target_table_name)


def insert_via_query(qf, query, schema_name, table_name):
    return qf("""
        INSERT INTO "{schema_name}"."{table_name}" (
            {query}
        )
    """.format(query=query, schema_name=schema_name, table_name=table_name))


def drop_column(qf, schema_name, table_name, column_name):
    return qf("""
        ALTER TABLE "{schema_name}"."{table_name}"
          DROP COLUMN "{column_name}"
    """.format(schema_name=schema_name, table_name=table_name, column_name=column_name))


def add_column(qf, schema_name, table_name, column_name, column_type):
    return qf("""
        ALTER TABLE "{schema_name}"."{table_name}"
          ADD COLUMN "{column_name}" {column_type}
    """.format(schema_name=schema_name, table_name=table_name, column_name=column_name, column_type=column_type))


def rename_column(qf, schema_name, table_name, column_name, new_name):
    return qf("""
        ALTER TABLE "{schema_name}"."{table_name}"
          RENAME COLUMN "{column_name}" TO "{new_name}"
    """.format(schema_name=schema_name, table_name=table_name, column_name=column_name, new_name=new_name))


def alter_column_type(qf, schema_name, table_name, column_name, column_type):
    """Can't alter column type directly, so wrap a few operations."""
    # See: https://stackoverflow.com/questions/17101918/alter-column-data-type-in-amazon-redshift
    new_name = column_name + '__tmp__col__'

    queries = []
    queries.append(add_column(
        qf=dummy_qf(), schema_name=schema_name, table_name=table_name,
        column_name=new_name, column_type=column_type))

    queries.append("""
        UPDATE "{schema_name}"."{table_name}"
            SET "{new_name}" = "{column_name}"
    """.format(
        schema_name=schema_name, table_name=table_name, new_name=new_name, column_name=column_name))

    queries.append(drop_column(
        qf=dummy_qf(), schema_name=schema_name, table_name=table_name, column_name=column_name))

    # Rename new to old
    queries.append(rename_column(
        qf=dummy_qf(), schema_name=schema_name, table_name=table_name, column_name=new_name, new_name=column_name))

    return qf(atomize_queries(queries))


def _raise_if_unique_rows_are_null(qf, schema_name, table_name, column_names):
    """Raise an exception if the columns specified have any null values.

    Parameters
    ----------
    qf : function
        function capable to run a query and return a result set
    schema_name : string
        Schema name for table to run this test on.
    table_name : string
        Table name for the table to run this test on.
    column_names : list-like
        Array of column names that we are computing unique values from.

    Throws
    ------
    ValueError : if the check fails (i.e. null values exist)
    """
    num_null_unique_ids = len(qf("""
        -- Find any rows with NULL in the columns we want to dedupe by;
        -- select any value (we'll just count rows)
        SELECT 1
        FROM "{schema_name}"."{table_name}" src
        WHERE {where_checks}
    """.format(
        schema_name=schema_name,
        table_name=table_name,
        where_checks="\n          OR ".join(  # add indenting to make it pretty :)
            # Check for null values in any column
            ['"{col_name}" IS NULL'.format(col_name=col_name)
             for col_name in column_names]))))

    # Check completed, validate data
    if num_null_unique_ids > 0:
        raise ValueError(
            "Column names contain {num_null_unique_ids} nulls; "
            "cannot be used to dedupe {schema_name}.{table_name}: {column_names}".format(
                schema_name=schema_name,
                table_name=table_name,
                column_names=column_names,
                num_null_unique_ids=num_null_unique_ids))


def drop_duplicate_rows(qf, schema_name, table_name, column_names):
    """Drop duplicate rows by copying/re-creating data."""
    _raise_if_unique_rows_are_null(
        qf=qf, schema_name=schema_name, table_name=table_name, column_names=column_names)
    commands_template = text_from_disk(op.join(this_files_dir(), "drop_duplicate_rows_commands.template.sql"))
    commands = commands_template.format(
        schema_name=schema_name,
        table_name=table_name,
        column_names=', '.join(['"{col}"'.format(col=col) for col in column_names]),
        temp_name_new="{table_name}_{rint}".format(table_name=table_name, rint=random.randint(1, 1E16)),
    )
    return qf(atomize_queries([commands]))


def _get_computed_column_sql(column_names, separator="|"):
    """Return SQL computation that represents a computed column based on the column names given.

    Works via string concatenation, so will not work with boolean columns
    (which cannot be implicitly converted to string).

    Parameters
    ----------
    column_names : list-like
        Array of column names
    separator : string, optional
        How to separate values in the string concatenation

    Returns
    -------
    SQL computation
    """
    return "({computation})".format(computation=" || '{sep}' || ".join(column_names))


def drop_duplicate_rows_low_disk(qf, schema_name, table_name, column_names):
    """Drop duplicate rows by copying/re-creating data."""
    _raise_if_unique_rows_are_null(
        qf=qf, schema_name=schema_name, table_name=table_name, column_names=column_names)
    commands_template = text_from_disk(op.join(this_files_dir(), "drop_duplicate_rows_low_disk_commands.template.sql"))
    commands = commands_template.format(
        schema_name=schema_name,
        table_name=table_name,
        computed_unique_id=_get_computed_column_sql(column_names),
        temp_name_new="{table_name}_{rint}".format(table_name=table_name, rint=random.randint(1, 1E16)),
    )
    return qf(atomize_queries([commands]))


def rename_table(qf, schema_name, table_name, new_name):
    """Change the table name (schema stays the same)."""
    return qf("""
        ALTER TABLE "{schema_name}"."{table_name}"
          RENAME TO "{new_name}"
    """.format(schema_name=schema_name, table_name=table_name, new_name=new_name))


def change_owner(qf, schema_name, table_name, new_owner):
    """Change table owner to a new username"""
    return qf("""
        ALTER TABLE "{schema_name}"."{table_name}"
          OWNER TO "{new_owner}"
    """.format(schema_name=schema_name, table_name=table_name, new_owner=new_owner))


def _generate_constraint_statement(primary_key, sort_key, diststyle_all, dist_key):
    """Massage our keys and generate the constraints for the end of the create table DDL.

    See: https://docs.aws.amazon.com/redshift/latest/dg/r_CREATE_TABLE_NEW.html
    """
    primary_key = '' if primary_key is None else ', PRIMARY KEY ({primary_key})'.format(primary_key=primary_key)
    sort_key = '' if sort_key is None else 'SORTKEY ({sort_key})'.format(sort_key=sort_key)

    if dist_key is not None and diststyle_all:
        raise ValueError('Only one of dist_key or diststyle_all should be provided.')

    if dist_key is None:
        distribution = 'DISTSTYLE ALL' if diststyle_all else 'DISTSTYLE EVEN'
    else:
        distribution = "DISTSTYLE KEY DISTKEY ({dist_key})".format(dist_key=dist_key)

    return "{primary_key}) {distribution} {sort_key}".format(
        primary_key=primary_key, sort_key=sort_key, distribution=distribution)


def add_constraints(qf, schema_name, table_name, temp_table_name=None,
                    primary_key=None, sort_key=None, diststyle_all=False, dist_key=None):
    """Recreate a table with desired primary key, sort key, distribution style, and distribution key.

    At this time, you can only add these to tables that start without any of them.
    Supporting more options requires additional regex logic, but is possible!

    Parameters
    -----------
    qf : func (query.generate_query_func)
        Closure that can query a Redshift database through an SSH-tunnel or locally.
    schema_name : str
        Name of schema for the table in Redshift.
    schema_name : str
        Name of table in Redshift.
    temp_table_name : str (optional)
        Name of the temp table to hold table data in while the new table is created (Default: randomly generated name).
    primary_key : str (optional)
        Name of the column (or comma-separated columns) to be used as the primary key of the table (Default: None).
    sort_key : str (optional)
        Name of the column (or comma-separated columns) that table data will be sorted by (Default: None).
        If comma-separated columns are specified, they will create a compound sort key.
    diststyle_all : boolean (optional)
        If true, a copy of the entire table is distributed to every Redshift node (Default: False).
    dist_key : str (optional)
        Name of the column to be used as the distribution key of the table (Default: None).

    Side-effects
    ------------
    Table permissions are not maintained.
    """
    if all([key is None for key in (primary_key, sort_key, dist_key)]) and not diststyle_all:
        # We have nothing to do here, don't recreate tables for no reason
        raise ValueError('No keys or diststyle provided!')

    create_table_ddl = get_create_table_ddl(qf, schema_name=schema_name, table_name=table_name)

    match_expression = r'\)\nDISTSTYLE EVEN\n;$'
    if (re.search(match_expression, create_table_ddl) is None
            or 'PRIMARY KEY' in create_table_ddl):
        # Here we assume that the table has been created with no special settings, so the DDL ends with DISTSTYLE EVEN
        # Also expect no PRIMARY KEY set; a primary key would appear before the final parenthesis
        raise ValueError(('Create table DDL does not have the format we expect: {create_table_ddl}. '
                          'Must contain DISTSTYLE EVEN and PRIMARY KEY.').format(
            create_table_ddl=create_table_ddl))

    injected_expression = _generate_constraint_statement(primary_key=primary_key, sort_key=sort_key,
                                                         diststyle_all=diststyle_all, dist_key=dist_key)
    create_table_ddl = re.sub(match_expression, injected_expression, create_table_ddl)

    temp_table_name = temp_table_name or "{table_name}_{random_string}".format(
        table_name=table_name, random_string=generate_random_string(20))

    return qf(atomize_queries([
        """
        ALTER TABLE "{schema_name}"."{table_name}" RENAME TO "{temp_table_name}";
        {create_table_ddl};
        INSERT INTO "{schema_name}"."{table_name}" (SELECT * FROM "{schema_name}"."{temp_table_name}");
        DROP TABLE "{schema_name}"."{temp_table_name}";
        """.format(
            schema_name=schema_name, table_name=table_name,
            temp_table_name=temp_table_name, create_table_ddl=create_table_ddl)
    ]))


def get_create_table_ddl(qf, schema_name, table_name):
    with open(op.join(this_files_dir(), 'redshift_create_table_ddl.template.sql'), 'r') as fp:
        df = qf(fp.read().format(schema_name=schema_name, table_name=table_name), echo=logger.logsLevel(logger.DEBUG))

    if len(df) == 0:
        raise ValueError("Table not found: {schema_name}.{table_name}".format(
            schema_name=schema_name, table_name=table_name))
    return '\n'.join(df['ddl'].values)


def vacuum_table(qf, schema_name, table_name, vacuum_option='FULL'):
    """Run the vacuum command.

    Parameters
    -----------
    qf : func (query.generate_query_func)
        Closure that can query a Redshift database through an SSH-tunnel or locally.
    schema_name : str
        Name of schema for the table in Redshift.
    schema_name : str
        Name of table in Redshift.
    vacuum_option : str (optional)
        One of FULL, SORT ONLY, DELETE ONLY, or REINDEX (Default: FULL).
    """
    valid_vacuum_options = ('FULL', 'SORT ONLY', 'DELETE ONLY', 'REINDEX')
    vacuum_option = vacuum_option.upper()
    if vacuum_option not in valid_vacuum_options:
        raise ValueError("Invalid vacuum_option provided, valid options are: {valid_vacuum_options}".format(
            valid_vacuum_options=valid_vacuum_options))

    return qf("""VACUUM {vacuum_option} "{schema}"."{table}" """.format(
        schema=schema_name, table=table_name, vacuum_option=vacuum_option))


def analyze_table(qf, schema_name, table_name):
    return qf("""ANALYZE VERBOSE "{schema}"."{table}" PREDICATE COLUMNS""".format(schema=schema_name, table=table_name))


def truncate_table(qf, schema_name, table_name):
    return qf("""TRUNCATE TABLE "{schema}"."{table}" """.format(schema=schema_name, table=table_name), command=True)

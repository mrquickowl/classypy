import re
from functools import partial

import mock
import pandas as pd
import pytest
import six

from classypy.query import prepare_query, redshift


def has_no_vars(query):
    return re.match(r'\{.*\}', query) is None


class RedshiftTestBase(object):
    def setup(self):
        def test_qf(query, substrs=tuple(), rv=None, **kwargs):
            # Really just a smoke test, to make sure that the variable replacements all happened.
            assert has_no_vars(query)

            for substr in substrs:
                assert substr in query

            return rv if rv is not None else query  # return for further verification.

        self.qf = test_qf

        def test_prepared_query(query, expected, params=None):
            prepared_query = prepare_query(query, params=params)
            assert expected == prepared_query

        self.test_prepared_query = test_prepared_query


class TestRedshiftMethods(RedshiftTestBase):
    def test_parse_fully_qualified_table_name(self):
        schema_name, table_name = redshift.parse_fully_qualified_table_name("a.b")
        assert schema_name == "a"
        assert table_name == "b"


class TestRedshiftDDL(RedshiftTestBase):
    def test_row_count(self):
        kwargs = dict(schema_name='my_schema', table_name='my_table')

        assert 1 == redshift.row_count(
            qf=partial(self.qf, substrs=kwargs.values(), rv=pd.DataFrame(data=[1])),
            **kwargs)

        assert 0 == redshift.row_count(
            qf=partial(self.qf, substrs=kwargs.values(), rv=pd.DataFrame(data=[])),
            **kwargs)

    def test_user_exists(self):
        kwargs = dict(username='my_user')

        assert redshift.user_exists(
            qf=partial(self.qf, substrs=kwargs.values(), rv=pd.DataFrame(data=[1])),
            **kwargs)

        assert not redshift.user_exists(
            qf=partial(self.qf, substrs=kwargs.values(), rv=pd.DataFrame(data=[0])),
            **kwargs)

    def test_group_exists(self):
        kwargs = dict(group_name='my_group')

        assert redshift.group_exists(
            qf=partial(self.qf, substrs=kwargs.values(), rv=pd.DataFrame(data=[1])),
            **kwargs)

        assert not redshift.group_exists(
            qf=partial(self.qf, substrs=kwargs.values(), rv=pd.DataFrame(data=[0])),
            **kwargs)

    def test_table_exists(self):
        kwargs = dict(schema_name='my_schema', table_name='my_table')

        assert redshift.table_exists(
            qf=partial(self.qf, substrs=kwargs.values(), rv=pd.DataFrame(data=[1])),
            **kwargs)

        assert not redshift.table_exists(
            qf=partial(self.qf, substrs=kwargs.values(), rv=pd.DataFrame(data=[0])),
            **kwargs)

    def test_create_schema(self):
        kwargs = dict(schema_name='my_schema')
        redshift.create_schema(
            qf=partial(self.qf, substrs=kwargs.values()), **kwargs)

    def test_drop_table(self):
        kwargs = dict(schema_name='my_schema', table_name='my_table')
        redshift.drop_table(
            qf=partial(self.qf, substrs=kwargs.values()), **kwargs)

    def test_drop_view(self):
        kwargs = dict(schema_name='my_schema', view_name='my_view')
        redshift.drop_view(
            qf=partial(self.qf, substrs=kwargs.values()), **kwargs)

    def test_create_table_as(self):
        kwargs = dict(query='my_query', schema_name='my_schema', table_name='my_table')
        redshift.create_table_as(
            qf=partial(self.qf, substrs=kwargs.values()), **kwargs)

    def test_create_table_from_table(self):
        kwargs = dict(
            source_schema_name='my_schema1', source_table_name='my_table1',
            target_schema_name='my_schema2', target_table_name='my_table2',)
        redshift.create_table_from_table(
            qf=partial(self.qf, substrs=kwargs.values()), **kwargs)

    def test_insert_via_query(self):
        kwargs = dict(query='my_query', schema_name='my_schema', table_name='my_table')
        redshift.insert_via_query(
            qf=partial(self.qf, substrs=kwargs.values()), **kwargs)

    def test_drop_column(self):
        kwargs = dict(schema_name='my_schema', table_name='my_table', column_name='my_column')
        redshift.drop_column(
            qf=partial(self.qf, substrs=kwargs.values()), **kwargs)

    def test_add_column(self):
        kwargs = dict(
            schema_name='my_schema', table_name='my_table',
            column_name='my_column', column_type='my_type')
        redshift.add_column(
            qf=partial(self.qf, substrs=kwargs.values()), **kwargs)

    def test_rename_column(self):
        kwargs = dict(
            schema_name='my_schema', table_name='my_table',
            column_name='my_column1', new_name='my_column2')
        redshift.rename_column(
            qf=partial(self.qf, substrs=kwargs.values()), **kwargs)

    def test_alter_column_type(self):
        kwargs = dict(
            schema_name='my_schema', table_name='my_table',
            column_name='my_column', column_type='my_type')
        redshift.alter_column_type(
            qf=partial(self.qf, substrs=kwargs.values()), **kwargs)

    def _qf_to_workaround_null_check(self, query, **kwargs):
        # hack to work around a specific function's check
        # that requires and a query function that returns a dataframe...
        # and these tests' needs to have a qf that returns a string.
        if 'Find any rows with NULL' in query:
            return pd.DataFrame()
        return self.qf(query, **kwargs)

    def test_drop_duplicate_rows(self):
        # Smoke test to make sure call to drop_duplicate_rows works.
        # Gets query back from function, if function runs successfully.
        query = redshift.drop_duplicate_rows(
            qf=partial(self._qf_to_workaround_null_check, substrs=('my_schema', 'my_table', 'my_col')),
            schema_name='my_schema', table_name='my_table', column_names=['my_col'])
        assert isinstance(query, six.string_types)

    def test_drop_duplicate_rows_low_disk(self):
        # Smoke test to make sure call to drop_duplicate_rows works
        # Gets query back from function, if function runs successfully.
        query = redshift.drop_duplicate_rows_low_disk(
            qf=partial(self._qf_to_workaround_null_check, substrs=('my_schema', 'my_table', 'my_col')),
            schema_name='my_schema', table_name='my_table', column_names=['my_col'])
        assert isinstance(query, six.string_types)

    def test_rename_table(self):
        kwargs = dict(schema_name='my_schema', table_name='my_table1', new_name='my_table2')
        redshift.rename_table(
            qf=partial(self.qf, substrs=kwargs.values()), **kwargs)

    def test_redshift_prepare_query_no_change(self):
        query = "SELECT * FROM table"
        expected_query = "SELECT * FROM table"
        self.test_prepared_query(query=query, expected=expected_query)

    def test_redshift_prepare_query_escape_percentages(self):
        query = """
            SELECT *
            FROM table
            WHERE field ILIKE '%series%'
        """
        expected_query = """
            SELECT *
            FROM table
            WHERE field ILIKE '%%series%%'
        """

        self.test_prepared_query(query=query, expected=expected_query)

    def test_redshift_prepare_query_escape_placeholders(self):
        query = """
            SELECT *
            FROM table
            WHERE field = %s
        """
        expected_query = """
            SELECT *
            FROM table
            WHERE field = %%s
        """

        self.test_prepared_query(query=query, expected=expected_query)

    def test_redshift_prepare_query_no_params_list(self):
        query = """
            SELECT *
            FROM table
            WHERE field = %s
        """
        expected_query = None
        params = ['zardoz']

        with pytest.raises(ValueError):
            self.test_prepared_query(query=query, expected=expected_query, params=params)

    def test_redshift_prepare_query_named_arguments(self):
        query = """
            SELECT *
            FROM table
            WHERE field = %(foo)s
              AND field2 = %(hello)s
        """
        expected_query = """
            SELECT *
            FROM table
            WHERE field = %(foo)s
              AND field2 = %(hello)s
        """
        params = {
            'hello': 'world',
            'foo': 'bar',
        }

        self.test_prepared_query(query=query, expected=expected_query, params=params)

    def test_redshift_prepare_query_no_params(self):
        query = """
            SELECT *
            FROM table
            WHERE field = %(foo)s
              AND field2 = %(hello)s
        """
        expected_query = None

        with pytest.raises(ValueError):
            self.test_prepared_query(query=query, expected=expected_query)

    def test_redshift_prepare_query_missing_params(self):
        query = """
            SELECT *
            FROM table
            WHERE field = %(foo)s
              AND field2 = %(hello)s
        """
        expected_query = """
            SELECT *
            FROM table
            WHERE field = %(foo)s
              AND field2 = %(hello)s
        """
        params = {
            'hello': 'world',
        }

        with pytest.raises(ValueError):
            self.test_prepared_query(query=query, expected=expected_query, params=params)


class TestRedshiftAtomic(RedshiftTestBase):
    def test_join_queries(self):
        queries = ['my_query1', 'my_query2']
        output_query = redshift.join_queries(queries=queries)
        assert queries == [q.strip() for q in output_query.split(';')]

    def test_strip_atomic_statements(self):
        query = 'my_query1'

        # Nothing to strip
        kwargs = dict(query=query)
        output_query = redshift.strip_atomic_statements(**kwargs)
        assert 'BEGIN' not in output_query
        assert 'COMMIT' not in output_query

        # Something to strip
        kwargs = dict(query='BEGIN; {query} COMMIT;'.format(query=query))
        output_query = redshift.strip_atomic_statements(**kwargs)
        assert 'BEGIN' not in output_query
        assert 'COMMIT' not in output_query

    def test_atomize_queries(self):
        queries = ['my_query1', 'my_query2']
        output_query = redshift.atomize_queries(queries=queries)
        assert 'BEGIN;' in output_query
        assert 'COMMIT;' in output_query


class TestRedshiftAddConstraints(RedshiftTestBase):
    create_table_patch_target = 'classypy.query.redshift.ddl_statements.get_create_table_ddl'

    def create_table_ddl_mock(self, add_primary_key=False, add_dist_key=False, add_sort_key=False,
                              *args, **kwargs):
        return (
            'CREATE TABLE IF NOT EXISTS "my_schema"."my_table"'
            '\n('
            '\n\t"some_id" BIGINT{primary_key_constraint}   ENCODE lzo'
            '\n\t,"some_string" VARCHAR(65535)   ENCODE lzo'
            '\n\t,"some_bool" BOOLEAN'
            '{primary_key}'
            '\n)'
            '{distribution}'
            '{sort_key}'
            '\n;'
        ).format(
            primary_key_constraint='NOT NULL' if add_primary_key else '',
            primary_key='\n\t,PRIMARY KEY (some_id)' if add_primary_key else '',
            distribution='\nDISTSTYLE KEY\nDISTKEY ("some_string")' if add_dist_key else '\nDISTSTYLE EVEN',
            sort_key='\nSORTKEY ("some_id")' if add_sort_key else '')

    @mock.patch(create_table_patch_target, new=create_table_ddl_mock)
    def test_add_constraints_none_provided(self):
        """Raise an error if no constraints are provided to add to the table."""
        with pytest.raises(ValueError):
            redshift.add_constraints(self.qf, schema_name='my_schema', table_name='my_table')

    @mock.patch(create_table_patch_target, new=partial(create_table_ddl_mock, add_primary_key=True))
    def test_add_constraints_existing_primary_key(self):
        """Raise an error if a primary key is already set on the table."""
        with pytest.raises(ValueError):
            redshift.add_constraints(self.qf, schema_name='my_schema', table_name='my_table',
                                     primary_key='some_id', sort_key='some_id',
                                     diststyle_all=True)

    @mock.patch(create_table_patch_target, new=partial(create_table_ddl_mock, add_sort_key=True))
    def test_add_constraints_existing_sort_key(self):
        """Raise an error if a sort key is already set on the table."""
        with pytest.raises(ValueError):
            redshift.add_constraints(self.qf, schema_name='my_schema', table_name='my_table',
                                     primary_key='some_id', sort_key='some_id',
                                     diststyle_all=True)

    @mock.patch(create_table_patch_target, new=partial(create_table_ddl_mock, add_dist_key=True))
    def test_add_constraints_existing_distribution_key(self):
        """Raise an error if a distribution key is already set on the table."""
        with pytest.raises(ValueError):
            redshift.add_constraints(self.qf, schema_name='my_schema', table_name='my_table',
                                     primary_key='some_id', sort_key='some_id',
                                     diststyle_all=True)

    @mock.patch(create_table_patch_target, new=create_table_ddl_mock)
    def test_add_constraints_diststyle_all(self):
        """Check that query to add constraints is generated with diststyle all specified."""
        add_constraints_query = redshift.add_constraints(self.qf, schema_name='my_schema', table_name='my_table',
                                                         primary_key='some_id', sort_key='some_id',
                                                         diststyle_all=True)
        assert 'PRIMARY KEY (some_id)' in add_constraints_query
        assert 'DISTSTYLE ALL' in add_constraints_query
        assert 'SORTKEY (some_id)' in add_constraints_query

    @mock.patch(create_table_patch_target, new=create_table_ddl_mock)
    def test_add_constraints_distkey(self):
        """Check that query to add constraints is generated with a distribution key specified."""
        add_constraints_query = redshift.add_constraints(self.qf, schema_name='my_schema', table_name='my_table',
                                                         primary_key='some_id', sort_key='some_id',
                                                         dist_key='some_string')
        assert 'PRIMARY KEY (some_id)' in add_constraints_query
        assert 'DISTSTYLE KEY' in add_constraints_query
        assert 'DISTKEY (some_string)' in add_constraints_query
        assert 'SORTKEY (some_id)' in add_constraints_query

    @mock.patch(create_table_patch_target, new=create_table_ddl_mock)
    def test_add_constraints_diststyle_all_and_distkey(self):
        """Raise an error if diststyle all and a distribution key are both specified."""
        with pytest.raises(ValueError):
            redshift.add_constraints(self.qf, schema_name='my_schema', table_name='my_table',
                                     primary_key='some_id', sort_key='some_id',
                                     diststyle_all=True, dist_key='some_string')

    @mock.patch(create_table_patch_target, new=create_table_ddl_mock)
    def test_add_constraints_temp_table(self):
        """Check that query to add constraints is generated with diststyle all specified."""
        add_constraints_query = redshift.add_constraints(self.qf, schema_name='my_schema', table_name='my_table',
                                                         primary_key='some_id', sort_key='some_id',
                                                         diststyle_all=True, temp_table_name='foobar')
        assert 'PRIMARY KEY (some_id)' in add_constraints_query
        assert 'DISTSTYLE ALL' in add_constraints_query
        assert 'SORTKEY (some_id)' in add_constraints_query
        assert 'RENAME TO "foobar"' in add_constraints_query

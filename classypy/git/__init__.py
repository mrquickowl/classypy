import errno
import os
import os.path as op

from classypy.util.dirs import caller_dir

VALID_GIT_HOOKS = (
    'applypatch-msg',
    'pre-applypatch',
    'post-applypatch',
    'pre-commit',
    'prepare-commit-msg',
    'commit-msg',
    'post-commit',
    'pre-rebase',
    'post-checkout',
    'post-merge',
    'pre-receive',
    'update',
    'post-receive',
    'post-update',
    'pre-auto-gc',
    'post-rewrite',
    'pre-push',
)


def _symlink_force(src, dst):
    """Create a symbolic link pointing to src named dst.

    Simulates forced symlink creation by removing an existing symlink, if os.symlink throws a file exists error.

    Parameters
    ----------
    src : str
        The source of the symlink
    dst : str
        The name of the symlink
    """
    try:
        os.symlink(src, dst)
    except OSError as err:
        if err.errno == errno.EEXIST:
            os.remove(dst)
            os.symlink(src, dst)
        else:
            raise err


def install_git_hooks():
    """Create symlinks in the `.git/hooks` directory to hooks in the `git-hooks` directory.

    Both `.git` and `git-hook` are expected to be within the current directory.
    """
    git_dir = op.join(caller_dir(frames_above=1), '.git')
    if not op.isdir(git_dir):
        raise ValueError(".git directory not found, are you sure you're in the repo dir?")

    git_hooks_dir = op.join(caller_dir(frames_above=1), 'git-hooks')
    if not op.isdir(git_hooks_dir):
        raise ValueError('Directory not found: {dir}'.format(dir=git_hooks_dir))

    git_hook_files = [f for f in os.listdir(git_hooks_dir) if f in VALID_GIT_HOOKS]
    for filename in git_hook_files:
        src_file = op.join(git_hooks_dir, filename)
        link_name = op.join(git_dir, 'hooks', filename)
        _symlink_force(src_file, link_name)

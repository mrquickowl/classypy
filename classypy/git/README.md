# Git Hooks

Git hooks allow custom scripts to be triggered by specific git actions. By default, they are stored in the `.git/hooks` directory. Because the `.git` directory can not be checked into version control, some setup is required to use our hooks.

## Setup

To install git hooks in a repo, first write your git hooks in a directory called `git-hooks`, inside of the repo directory. Then run `python -c "import classypy.git; classypy.git.install_git_hooks()"` from the repo directory.

This will create symlinks in `.git/hooks` for all files in `git-hooks`. That means that if a new file is added to this directory, you must install again. But if a file is updated, the update will be effective as a hook immediately.

If you have existing hooks that are also defined in `git-hooks`, they will be overwritten. Existing hooks that are not defined in this directory will not be removed.

"""
"""
from __future__ import absolute_import, division

import sys

# Format expected by setup.py and doc/source/conf.py: string of form "X.Y.Z"
_version_major = 0
_version_minor = 1
_version_micro = ''  # use '' for first of series, number for 1 and above
_version_extra = 'dev0'
# _version_extra = ''  # Uncomment this for full releases

# Construct full version string from these.
_ver = [_version_major, _version_minor]
if _version_micro:
    _ver.append(_version_micro)
if _version_extra:
    _ver.append(_version_extra)

__version__ = '.'.join(map(str, _ver))

CLASSIFIERS = ["Development Status :: 3 - Alpha",
               "Environment :: Console",
               "Operating System :: OS Independent",
               "Programming Language :: Python :: 2.7",
               "Topic :: Scientific/Engineering"]

# Description should be a one-liner:
description = "classypy: a set of shared Python libraries for use within Classy.org"
# Long description will go up on the pypi page
long_description = """

ClassyPy
========
ClassyPy is a set of shared libraries for internal use within Classy.org

.

.. _README: https://bitbucket.com/stayclassy/classypy/blob/master/README.md

License
=======
``ClassyPy`` is private, and not for distribution.

All trademarks referenced herein are property of their respective holders.

Copyright (c) 2016--, Classy.org
"""

NAME = "classypy"
MAINTAINER = "Ben Cipollini"
MAINTAINER_EMAIL = "bcipollini@classy.org"
DESCRIPTION = description
LONG_DESCRIPTION = long_description
KEYWORDS = ['Data', 'Connector']
URL = "https://www.classy.org"
DOWNLOAD_URL = ""
LICENSE = "For internal use only. Any distribution outside of Classy.org is strictly prohibited."
AUTHOR = "Ben Cipollini"
AUTHOR_EMAIL = "bcipollini@classy.org"
PLATFORMS = "OS Independent"
MAJOR = _version_major
MINOR = _version_minor
MICRO = _version_micro
VERSION = __version__
PACKAGE_DATA = {'': ['**/*.csv', '**/*.sh', '**/*.sql', '**/*.txt', '**/*.yaml', '**/*.yml']}
REQUIRES = (
    "bandit",  # testing
    "bugsnag",  # classypy.devops
    "cerberus",  # classypy.linters
    "chardet",  # classypy.text
    "credstash",  # classypy.devops
    "cryptography>=2.5",  # needed for Python3
    "dask[complete]",  # classypy.util.execution
    "dill",  # classypy.reporting
    "flake8",  # testing
    "isort",  # see: dependency_links
    "lxml",  # io, ml
    "matplotlib(>2.0.1)",  # reporting
    "mistletoe" if sys.version_info[0] == 3 else "",  # R report linter
    "mock",  # testing
    "nbconvert",  # ipynb linter
    "nbstripout",  # see: dependency_links
    "numpy",
    "pandas(>=0.20.3)",
    "pip(>=19.0.3)",  # for classypy.io dynamic module install
    "psycopg2(<=2.7.3)",  # DPLAT-1554 locking version used by shiftmanager
    "pydocstyle",  # testing
    "PyMySQL",  # classypy.query
    "python_dotenv",  # classypy.devops
    "python_frontmatter",  # R report linter
    "pytest(==3.9.3)",  # testing
    "pytz",  # classypy.util
    "radon(==2.2.0)",  # testing
    "ruamel.yaml",  # classypy.linters, utils
    "shiftmanager",  # classypy.query
    "six",  # classypy.compat
    "sshtunnel",  # classypy.query
    "tzlocal",
    "vulture",  # testing
    "xenon",  # testing
)
# Eliminate any emptied packages
REQUIRES = tuple([req for req in REQUIRES if req != ""])
EXTRAS_REQUIRE = {
    'etl': (
        "requests",  # etl (PayClient)
        "requests_oauthlib",  # etl (PayClient)
    ),
    'ml': (
        "beautifulsoup4",  # ml.nlp
        "bs4",  # ml.nlp
        "gensim",  # classypy.ml
        "hyperopt",  # classypy.ml
        "lda",  # classypy.ml
        "nltk",  # classypy.ml
        "pycorenlp",  # classypy.ml.np
        "scikit-learn(==0.20.3)",
        "scipy",
        "spacy",
    ),
    'viz': (
        "bokeh",  # classypy.viz
        "plotly(==2.0.8)",
        "python_slugify",  # classypy.viz
        "reverse_geocode",  # in mapping_functions
    ),
}
# Gather all
EXTRAS_REQUIRE['complete'] = sorted(set([pkg for pkgs in EXTRAS_REQUIRE.values() for pkg in pkgs]))
DEPENDENCY_LINKS = (
    "https://github.com/bcipolli/isort.git",
    "https://github.com/bcipolli/nbstripout.git@7c842d72480698466606cc4355130c3b4aac309f",  # ipynb linter
)

import tempfile

from classypy.compat import b as bytes
from classypy.compat import u as unicode
from classypy.text import text_from_disk
from classypy.text.utils import render_jinja


def test_text_from_disk_ascii():
    ascii_content = u'abc'

    _, temp_file = tempfile.mkstemp()
    with open(temp_file, 'w') as fp:
        fp.write(ascii_content)
    assert text_from_disk(temp_file) == ascii_content


def test_text_from_disk_bytes(temp_file):
    bytes_content = bytes(u'abc')

    _, temp_file = tempfile.mkstemp()
    with open(temp_file, 'wb') as fp:
        fp.write(bytes_content)
    assert text_from_disk(temp_file) == unicode(bytes_content), (
        "Converts bytes to unicode.")


def test_text_from_disk_iso_8859_1(temp_file):
    iso_8859_1_content = u'Abc\xa0def'.encode('iso-8859-1')

    _, temp_file = tempfile.mkstemp()
    with open(temp_file, 'wb') as fp:
        fp.write(iso_8859_1_content)
    assert text_from_disk(temp_file) == iso_8859_1_content.decode('iso-8859-1'), (
        "Detects charset and returns as regular unicode.")


def test_render_jinja():
    jinja_template = u'{% for letter in letters %}{{letter}};{% endfor %}'
    letters = ['a', 'b', 'c']
    _, temp_file = tempfile.mkstemp()
    with open(temp_file, 'w') as fp:
        fp.write(jinja_template)

    expected = 'a;b;c;'
    assert render_jinja(template_path=temp_file, letters=letters) == expected, (
        "Jinja template did not render correctly.")
    assert text_from_disk(temp_file) == jinja_template, "Jinja template was modified on disk."

    _, temp_file2 = tempfile.mkstemp()
    assert render_jinja(template_path=temp_file, render_path=temp_file2, letters=letters) == expected, (
        "Jinja template did not render correctly when render_path is provided.")
    assert text_from_disk(temp_file2) == expected, (
        "Jinja template was not saved properly to render_path.")

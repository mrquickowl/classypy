import codecs
import json
import os.path as op
import random
import warnings
from string import ascii_lowercase

import chardet
import jinja2
from ruamel import yaml

from classypy.devops import logger


class TextParsingWarning(UserWarning):
    """Warn during text parsing."""

    pass


def generate_random_string(length):
    """Generate random string of lowercase ascii characters with given length."""
    return ''.join(random.choice(ascii_lowercase) for _ in range(length))


def json_from_disk(path, **kwargs):
    """Read formatted json into a dictionary."""
    with open(path, 'r', **kwargs) as fp:
        return json.load(fp)


def json_to_disk(obj, path, **kwargs):
    """Write a dictionary to formatted json."""
    with open(path, 'w') as fp:
        return json.dump(obj, fp, **kwargs)


def quote_string(val):
    """Produce a Python-evaluatable string by escaping double-quotes and quoting the resulting value."""
    return '"{val}"'.format(val=val.replace('"', '\"'))


def render_jinja(template_path, render_path=None, **kwargs):
    """Renders a Jinja2 template.

    Parameters
    ----------
    template_path : str
        A path to where a Jinja2 template is saved on disk.
    render_path : str, optional
        The path where the rendered template should be saved.
    kwargs : dict
        Dictionary containing variables to render the Jinja template with.

    Side-effects
    ------------
    If render_path is provided, the rendered Jinja2 template will be saved that location.

    Returns
    -------
    str : the rendered Jinja2 template.
    """
    template_file_dir, template_file_name = op.split(template_path)
    template_loader = jinja2.FileSystemLoader(searchpath=template_file_dir)
    # Creating a custom environment so that we can keep trailing newlines
    jinja_env = jinja2.Environment(
        loader=template_loader,
        keep_trailing_newline=True,
        # Set based on bandit (security) recommendation
        # https://bandit.readthedocs.io/en/latest/plugins/b701_jinja2_autoescape_false.html
        autoescape=True,
    )
    template = jinja_env.get_template(template_file_name)
    rendered_template = template.render(**kwargs)
    if render_path is not None:
        text_to_disk(rendered_template, path=render_path)
    return rendered_template


def text_from_disk(path, disk_encoding=None, default=None, confidence_threshold=0.8, **kwargs):
    """Read file from disk; capture errors if requested.

    Parameters
    ----------
    path : string
        File path on disk.
    disk_encoding : string, optional
        Encoding to read file as (Default: detect via chardet).
    default : string, optional
        If specified (non-None), returns default on error. (Default: None => raise errors)
        Note: sets errors=ignore flag to open()
    confidence_threshold: float, optional
        Raise warning if guessed encoding is below threshold. Default: 0.8
    """
    if default is not None:
        kwargs['errors'] = kwargs.get('errors', 'ignore')  # set default to errors=ignore

    try:
        if disk_encoding is None:
            # Detect encoding and convert
            with codecs.open(path, 'rb', **kwargs) as fp:
                content = fp.read()
            guess = chardet.detect(content)

            if guess and guess['encoding']:
                # We found an encoding; validate
                if guess['confidence'] >= confidence_threshold:
                    # High confidence: keep it
                    disk_encoding = guess['encoding']
                elif guess['encoding'].lower() not in ('charmap', 'windows-1252', 'windows-1254'):
                    # Low confidence on unknown errors: keep it, but warn.
                    disk_encoding = guess['encoding']
                    msg = "Guessed {encoding} with low confidence ({confidence}) for {path}.".format(
                        encoding=guess['encoding'], confidence=guess['confidence'], path=path)
                    warnings.warn(msg, category=TextParsingWarning)
                else:
                    # Low confidence on common errors: override it
                    disk_encoding = 'utf-8'

                # Encoding detected; decode and return.
                return content.decode(disk_encoding)

        # Encoding specified, or none specified but none detected.
        with codecs.open(path, 'r', encoding=disk_encoding, **kwargs) as fp:
            return fp.read()
    except Exception as ex:  # noqa
        if default is not None:
            # Catch errors and return the default.
            logger.info("Exception while reading {path}: {ex}".format(path=path, ex=ex))
            return default
        raise


def text_to_disk(val, path, encoding='utf-8', **kwargs):
    """Write a string to a plain text file."""
    with codecs.open(path, 'w', encoding=encoding, **kwargs) as fp:
        fp.write(val)


def yaml_from_disk(path, **kwargs):
    """Read formatted json into a dictionary."""
    with open(path, 'r', **kwargs) as fp:
        return yaml.safe_load(fp)


def yaml_to_disk(obj, path, **kwargs):
    """Write a dictionary to formatted yml."""
    with open(path, 'w') as fp:
        yaml.round_trip_dump(obj, fp, **kwargs)

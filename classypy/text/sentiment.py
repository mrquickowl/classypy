import pandas as pd


def analyze_sentiment(df, columns=None):
    from nltk.sentiment.vader import SentimentIntensityAnalyzer

    sid = SentimentIntensityAnalyzer()
    if columns:
        df = df[columns]
    df = df.where((pd.notnull(df)), '')
    texts = df.apply(lambda r: ' '.join(r.values), axis=1).values
    sentiment_df = pd.DataFrame(data=[sid.polarity_scores(txt) for txt in texts])

    text_df = pd.DataFrame(data={'text': texts})
    return pd.concat([sentiment_df, text_df], axis=1, sort=True)


if __name__ == '__main__':
    from classypy.devops import logger

    df = pd.DataFrame(data=[
        {'body': 'test', 'title': 'test'},
        {'body': 'test2', 'title': 'test2'},
    ])
    logger.info(analyze_sentiment(df))

#!/bin/bash
# Lint annotations

LINTING_PATH="${1:-.}"
EXCLUDES="${2:-venv,venv2,venv3}"
EXCLUDE_DIR=`echo $EXCLUDES | sed -e "s/,/ --exclude-dir=/g"`

# A pretend Python dictionary with bash 3
ANNOTATED_TABLES_ARRAY=( "mongodb.blocks:annotated.blocks"
                         # Annotated tables in favor of raw tables
                         "salesforce.accounts:annotated.accounts"
                         "salesforce.opportunities:annotated.opportunities"
                         # migration to annotated schema
                         "redshift.annotated_accounts.accounts:annotated.accounts"
                         "redshift.annotated_blocks.accounts:annotated.blocks"
                         "redshift.annotated_campaigns:annotated.campaigns"
                         "redshift.annotated_fundraising_pages:annotated.fundraising_pages"
                         "redshift.annotated_fundraising_teams:annotated.fundraising_teams"
                         "redshift.annotated_jira_issues:annotated.jira_issues"
                         "redshift.annotated_members:annotated.members"
                         "redshift.annotated_opportunities:annotated.opportunities"
                         "redshift.annotated_orgs:annotated.organizations"
                         "redshift.annotated_supporters:annotated.supporters"
                         "redshift.annotated_tickets:annotated.tickets"
                         "redshift.annotated_transactions:annotated.transactions"
)

# Check that mysql files are named with .mysql.sql
TOTAL_MYSQL_CHECK=0
for table_map in "${ANNOTATED_TABLES_ARRAY[@]}" ; do
    raw_table=${table_map%%:*}
    schema=${raw_table%%\.*}
    table=${raw_table#*\.}

    # Must be in the mysql schema
    if [ "$schema" != "mysql" ]; then
        continue
    fi

    # Must fail the mysql check
    MYSQL_CHECK="`grep -r \"FROM ${table} \" --include=*.sql --exclude *.mysql.sql --exclude-dir=$EXCLUDE_DIR $LINTING_PATH | grep -v '\-- noqa'`"
    if [ "$MYSQL_CHECK" = "" ]; then
        continue
    fi

    echo "Your SQL file runs on mysql (contains \"FROM ${table} \") and should be named .mysql.sql:"
    # Construct a move command
    for lin in `echo "${MYSQL_CHECK%x}"`; do
        if [[ "$lin" != *".sql"* ]]; then
            continue
        fi
        src_fil=${lin%%:*}
        if [ -e $src_fil ]; then
            dest_fil=`echo $src_fil|sed -e "s/\.sql/.mysql.sql/g"`
            echo "mv $src_fil $dest_fil"
        fi
    done
    TOTAL_MYSQL_CHECK=$((TOTAL_MYSQL_CHECK + 1))  # do all checks before bailing
done
if [ "$TOTAL_MYSQL_CHECK" != "0" ]; then
    exit $TOTAL_MYSQL_CHECK
fi

# Validate only annotated tables are used
TOTAL_RAW_CHECK=0
for table_map in "${ANNOTATED_TABLES_ARRAY[@]}" ; do
    raw_table=${table_map%%:*}
    annotated_table=${table_map#*:}

    # Search for raw table name (followed by space, as tables should be aliased)
    RAW_CHECK="`grep -r \"$raw_table \" --include=*.sql --exclude=annotated_*.sql --exclude-dir=$EXCLUDE_DIR $LINTING_PATH | grep -v '\-- noqa'`"
    if [ "$RAW_CHECK" != "" ]; then
        echo "You used raw table \"$raw_table\" instead of annotated table \"$annotated_table\" on lines:"
        echo "${RAW_CHECK%x}"
        TOTAL_RAW_CHECK=$((TOTAL_RAW_CHECK + 1))  # do all checks before bailing

        # Fixing the errors
        for lin in `echo "${RAW_CHECK%x}"`; do
            if [[ "$lin" != *".sql"* ]]; then
                continue
            fi

            src_fil=${lin%%:*}
            if [ -e $src_fil ]; then
                sed -e "s/${raw_table} /${annotated_table} /g" -i "" "$src_fil"
            fi
        done
    fi
done
# Found a problem; exit
if [ "$TOTAL_RAW_CHECK" != "0" ]; then
    exit $TOTAL_RAW_CHECK
fi

# Check use of mysql.charity.is_test (instead of annotated.organizations.is_valid)
# NOTE: excludes progress queries, which lacks annotations
IS_TEST_CHECK="`grep -r is_test --include=*.sql --exclude=annotated_*.sql --exclude=*.mysql.sql --exclude-dir=$EXCLUDE_DIR $LINTING_PATH | grep -v progress | grep -v '\-- noqa'`"
if [ "$IS_TEST_CHECK" != "" ]; then
    echo "Use is_valid instead of is_test on lines:"
    echo "${IS_TEST_CHECK%x}"
    exit 1
fi

# Check use of mysql.orders.orders_status (instead of annotated.transactions.is_valid)
ORDERS_STATUS_CHECK="`grep -r orders_status --include=*.sql --exclude=annotated_*.sql --exclude=*.mysql.sql --exclude-dir=$EXCLUDE_DIR $LINTING_PATH | grep -v '\-- noqa'`"
if [ "$ORDERS_STATUS_CHECK" != "" ]; then
    echo "Use is_valid instead of orders_status on lines:"
    echo "${ORDERS_STATUS_CHECK%x}"
    exit 1
fi

# Check use of mongodb.blocks.deleted_at (instead of annotated.blocks.is_valid)
# NOTE: excludes progress queries, which lacks annotations
DELETED_AT_CHECK="`grep -r deleted_at --include=*.sql --exclude=annotated_*.sql --exclude=*.mysql.sql --exclude-dir=$EXCLUDE_DIR $LINTING_PATH | grep -v progress | grep -v '\-- noqa'`"
if [ "$DELETED_AT_CHECK" != "" ]; then
    echo "Use is_valid instead of deleted_at on lines:"
    echo "${DELETED_AT_CHECK%x}"
    exit 1
fi

"""
Script for finding (and fixing) SQL linting errors.

Usage:
python ~/code/insights/classypy/classypy/linters/sql.py median_growth.sql --fix
"""
import re
from functools import partial

import six

from classypy.query.redshift import (DATE_FUNCTION_NAMES_WITH_UNDERSCORES, DATE_KEYWORDS, DDL_KEYWORDS, DDL_STATEMENTS,
                                     FUNCTION_NAMES, SYMBOLIC_OPERATORS, TYPES, join_queries, split_queries)

# Requires different method
VARIABLE_PREFIXES = {
    'n_': 'num_',
    'cnt_': 'num_',
    'average_': 'avg_',
    'median_': 'med_',
    'median_': 'med_',
    'total_': 'sum_',
    'perc_': 'pct_',
    'percent_': 'pct_',
}
TABLE_PREFIXES = {'orders': 't', 'charity': 'o', 'customers': 'm', 'events': 'c'}

DDL_REMAP = {
    # Keyword Aliases
    r'CURRENT_TIMESTAMP': r'GETDATE()',
    r'<>': r'!=',
    r'INNER JOIN': 'JOIN',

    # Function aliases
    r'NVL\(': r'COALESCE(',
    # Sadly, our IF clauses are too complex
    # r'IF\(([^,]+),\s*([^,]+),\s*([^)]+)\)': r'CASE WHEN \1 THEN \2 ELSE \3 END',
    r'IFNULL\(': r'COALESCE(',
    r'NULLIF\(\s*([^,]+),\s*([^)]+)\)': r'CASE WHEN (\1) != (\2) THEN \1 END',
    r'NVL2\(([^,]+),\s*([^,]+),\s*([^)]+)\)': r'CASE WHEN (\1) IS NULL THEN \2 ELSE \3 END',
    # Sadly, no date_part
    r'EXTRACT\((\w+)\s+FROM\s+([^)]+)\)': r'DATE_PART(\1, \2)',
}


def get_subquery_name(line):
    """Pulls subquery name from a WITH...-style syntax subquery definition."""
    subquery_name_match = re.match(r'^\s*WITH\s+(\w+)\s+AS\s+\(.*$', line) or re.match(
        r'^\s*(\w+)\s+AS\s+\(.*$', line)
    return subquery_name_match.groups()[0] if subquery_name_match else None


def is_subquery_definition(line):
    """Line begins a subquery"""
    return get_subquery_name(line) is not None


def is_subquery_completion(line):
    """Line ends a subquery"""
    return line in (")", "),")  # functions should not appear at zero indenting.


def get_indent_level(line):
    """Return an integer count of the number of whitespace characters to the left of the line."""
    return len(line) - len(line.lstrip())


def indent_line(line, indent_level, indent_char=' '):
    """Ensure the line is left-indented to the number of spaces requested."""
    indenting = indent_char * indent_level
    return re.sub(r'^\s*(.*)$', r'%s\1' % indenting, line)


def line_startswith_keyword(line, keywords):
    """Returns TRUE if the given line begins with one of the given keywords."""
    if isinstance(keywords, six.string_types):
        # Allow strings to be passed
        keywords = [keywords]

    for keyword in keywords:
        keyword_match = re.match(r'^\s*%s\s' % keyword, line) or re.match(r'^\s*%s$' % keyword, line)
        if keyword_match:
            return True
    return False


def fix_dispreferred_functions(line):
    """Swap out dis-preferred functions for preferred ones."""
    new_line = line
    errs = []

    for re_search, re_repl in DDL_REMAP.items():
        new_line = re.sub(re_search, re_repl, new_line)
        if line != new_line:
            errs.append("{line} => {new_line}".format(line=line, new_line=new_line))
            line = new_line
    return errs, new_line


def fix_subquery_docstrings(line, prev_lines, indent_level=2):
    """Make sure all subqueries have docstrings."""
    line, prev_line = line or '', prev_lines and prev_lines[-1] or ''
    subquery_name = get_subquery_name(prev_line)
    if not subquery_name:
        return [], line

    if not line.strip().startswith('--'):
        err = 'Must start subquery "{subquery_name}" with docstring.'.format(subquery_name=subquery_name)
        new_line = '{comment}\n{line}'.format(
            comment=indent_line('-- Please add a docstring', indent_level=get_indent_level(prev_line) + indent_level),
            line=line)
        return [err], new_line
    return [], line


def is_creation_statement(line):
    return "CREATE" in line and "TABLE" in line


def _get_indenting_level_via_conditionals_set1(line, prev_lines, indent_increment=2):
    """Group 1 of indent level tests"""
    # Reset indenting, then report an error if anything changed.
    line, prev_line = line or '', prev_lines and prev_lines[-1] or ''  # Protect against None / empty list
    prev_indent_level, cur_indent_level = get_indent_level(prev_line), get_indent_level(line)

    new_indent_level, reason = None, None

    if line.strip() == '':
        new_indent_level = 0
        reason = "Empty Line"

    elif cur_indent_level % indent_increment != 0:
        # Couldn't-possibly-be-right indenting. Guess at what could be correct.
        new_indent_level = cur_indent_level + (cur_indent_level % indent_increment)
        reason = "Indenting should be multiple of {indent_increment}: {line}".format(
            indent_increment=indent_increment, line=line)

    elif cur_indent_level - prev_indent_level > indent_increment and prev_line != '':
        # Should never increase by more than 2 at a time,
        # unless the previous line was a blank one.
        new_indent_level = prev_indent_level + indent_increment
        reason = "Indenting more than {indent_increment}".format(indent_increment=indent_increment)

    elif is_comment(prev_line):
        # Overriding everything: don't indent after comments.
        new_indent_level = prev_indent_level
        reason = 'Maintain indenting level after comment'

    elif (is_subquery_definition(line) or is_subquery_completion(line)) and not is_creation_statement(prev_line):
        # Unindent to base level
        new_indent_level = 0
        reason = 'Begins/Ends Subquery'

    elif is_subquery_definition(prev_line):
        # Expect indenting to increase after a subquery start
        new_indent_level = prev_indent_level + indent_increment
        reason = 'Is Subquery'

    return new_indent_level, reason


def _get_indenting_level_via_conditionals_set2(line, prev_lines, indent_increment=2):
    """Group 2 of indent level tests"""
    # Reset indenting, then report an error if anything changed.
    line, prev_line = line or '', prev_lines and prev_lines[-1] or ''  # Protect against None / empty list
    prev_indent_level = get_indent_level(prev_line)

    new_indent_level, reason = None, None

    if (line_startswith_keyword(prev_line, ('JOIN', 'LEFT JOIN', 'RIGHT JOIN', 'FULL OUTER JOIN'))
            and line_startswith_keyword(line, 'ON')):
        # Expect indenting to increase after a JOIN (on)
        new_indent_level = prev_indent_level + indent_increment
        reason = 'JOIN/ON'

    elif (line_startswith_keyword(prev_line, 'SELECT')
            and not line_startswith_keyword(line, ('FROM', 'UNION', 'SELECT'))
            and not is_subquery_completion(line)):
        # select ... not followed by from/union/select (with union on prev line)
        new_indent_level = prev_indent_level + indent_increment
        reason = 'SELECT'

    elif (line_startswith_keyword(prev_line, ('WHERE'))
            and (line_startswith_keyword(line, ('AND', 'OR')))):
        # Expect indenting to increase in filter clauses
        new_indent_level = prev_indent_level + indent_increment
        reason = 'WHERE => AND/OR'

    elif line_startswith_keyword(prev_line, ('AND', 'ON')) and line_startswith_keyword(line, 'AND'):
        # Expect indenting to increase in filter clauses
        # NOTE: this doesn't account for ANDs from our multi-line BETWEENs,
        # but it's an uncommon case, and there are simple workarounds for that
        if len(prev_lines) > 1 and not line_startswith_keyword(prev_lines[-2], 'BETWEEN'):
            new_indent_level = prev_indent_level
            reason = 'Chain ANDs at the same indent level'
        else:
            # multi-line between indents twice (once for BETWEEN, once for AND)
            prev_prev_line = prev_lines[-3] if len(prev_lines) > 2 else ''
            if line_startswith_keyword(prev_prev_line, 'WHERE'):
                # deal with special case of BETWEEN directly after WHERE clause
                new_indent_level = prev_indent_level - indent_increment
                reason = 'logical AND after WHERE BETWEEN AND'
            else:
                new_indent_level = prev_indent_level - 2 * indent_increment
                reason = 'logical AND after BETWEEN AND'

    elif line_startswith_keyword(line, 'BETWEEN'):
        # BETWEEN {start_date} AND {end_date}
        new_indent_level = prev_indent_level + indent_increment
        reason = 'BETWEEN'

    elif line_startswith_keyword(prev_line, 'BETWEEN') and line_startswith_keyword(line, 'AND'):
        # BETWEEN {start_date}
        #   AND {end_date}
        new_indent_level = prev_indent_level + indent_increment
        reason = 'multi-line BETWEEN'
    return new_indent_level, reason


def fix_indenting(line, prev_lines, indent_increment=2):
    """Selectively enforce indenting rules."""
    # Get indent level
    new_indent_level, reason = _get_indenting_level_via_conditionals_set1(
        line=line, prev_lines=prev_lines, indent_increment=indent_increment)
    if new_indent_level is None:
        # Indenting level wasn't captured in the first set of conditionals,
        # try a second set.
        new_indent_level, reason = _get_indenting_level_via_conditionals_set2(
            line=line, prev_lines=prev_lines, indent_increment=indent_increment)
    if new_indent_level is None:
        # Indenting level wasn't captured in the second set of conditionals,
        # default to the indent level of the current line,
        # which leads to a no-op below (i.e. indent the line to the current indenting level)
        new_indent_level = get_indent_level(line)

    # Update data
    new_line = indent_line(line, indent_level=new_indent_level)

    # Communicate results
    if line != new_line:
        err = """Fixing indenting ({reason}): "{old_line}" => "{new_line}" """.format(
            reason=reason, old_line=line, new_line=new_line)
        return [err, new_line]
    return None, new_line


def fix_double_newlines(line, prev_lines):
    """Don't allow two consecutive newlines."""
    line, prev_line = line or '', prev_lines and prev_lines[-1] or ''  # Protect against None / empty list
    if line.strip() == '' and prev_line.strip() == '':
        return ["Eliminate double-newlines"], None
    return [], line


def fix_join_ordering(line, prev_lines):
    """Don't allow two consecutive newlines."""
    new_line, prev_line = line or '', prev_lines and prev_lines[-1] or ''  # Protect against None / empty list

    if not new_line.strip().startswith('ON ') or 'JOIN ' not in prev_line.strip():
        # must look like a basic join
        return [], new_line
    elif contains_comment(new_line) or contains_comment(prev_line):
        # Regexes don't deal with comments
        return [], new_line

    # parse out aliases, and compare
    join_alias = re.sub(r'^.*\s*JOIN\s.*\s+([^\s]+)$', '\\1', prev_line)
    on_first_alias = re.sub(r'^\s+ON\s+([a-z_0-9]+).*$', '\\1', new_line)
    if join_alias == prev_line or on_first_alias == new_line or join_alias == on_first_alias:
        # failed to parse, or aliases match as expected
        return [], new_line

    # Flip columns (assumes no whitespace in column name)
    new_line = re.sub(r'^(\s+ON\s+)([^\s]+)(\s+[^\s]+\s+)([^\s]+)(.*)$', r'\1\4\3\2\5', line)
    return ["{line} => {new_line}".format(line=line, new_line=new_line), new_line]


def is_comment(line):
    """Is the text a comment snippet?"""
    line = line.strip()
    return line.startswith('--') or (line.startswith('/*') and line.endswith('*/'))


def contains_comment(line):
    return '--' in line


def is_comment_open(line):
    """Is the text starting a comment that it does not complete?"""
    return '/*' in line and '*/' not in line


def is_comment_close(line):
    """Is the text ending a comment that it does not start?"""
    return '/*' not in line and '*/' in line


def aggregate_errors(fn, line, errors=None, *args, **kwargs):
    """
    Run a line-based linting function, aggregate errors to a given error array.

    Parameters
    ----------
    fn : function
        Assumptions:
            * First parameter is 'line'
            * Function returns either:
                + an error (i.e. only searches for errors, no fixing)
                + a 2-tuple (error, fixed_line)
                + a 2-tuple (error-list, fixed line)
    line : the SQL line to lint
    errors : errors received so far.
    *args, **kwargs - args to pass to fn

    Returns
    -------
    errors : array of errors (can be empty)
    new_line : string

    Side Effects
    ------------
    none; this function will NOT append data to errors.
    """
    obj = fn(line, *args, **kwargs)
    errors = errors or []

    if obj is None:
        # function returned no error.
        return errors, line
    if isinstance(obj, six.string_types):
        # function returned an error (no updated line)
        return (errors + [obj]), line

    if len(obj) != 2:
        raise ValueError("Evaluation fn returned unrecognized object type.")

    obj, new_line = obj
    if obj is None:
        pass
    elif isinstance(obj, six.string_types):
        # function returned error, new_line
        errors = errors + [obj]  # do not use +=, which has side-effects
    else:
        # Function returned list_of_errors (None ok), new_line
        errors = errors + list(obj)  # do not use +=, which has side-effects

    return errors, new_line


def detect_errors(line, re_search, msg):
    """Detect error only (can't fix)"""
    if re.search(re_search, line, flags=re.M | re.S | re.I):
        return '{msg}: {line}'.format(msg=msg, line=line)
    return None


def detect_and_fix_errors(line, re_search, re_replace, msg):
    """
    Detect and fix an error, based on regular expressions.

    Parameters
    ----------
    line : string
    re_search : string
        Regular expression to find in string
    re_replace : string
        Replacement string (for fixing)
    msg : string
        Message to help user understand the error.

    Returns
    -------
    error, line tuple

    error : string
    line : string
    """
    fixed_line = re.sub(re_search, re_replace, line, flags=re.M | re.S | re.I)

    # Re-fix comments that break
    refixed_line = re.sub(r'\s*--\s*', '  -- ', fixed_line)

    # Re-fix things inside quotes, comments.
    for expr in ("^(.*')(.*)('.*)$", '^(.*")(.*)(".*)$', '^(.*--)(.*)($)'):
        while True:
            match_orig = re.match(expr, line, flags=re.M | re.S | re.I)
            match_new = re.match(expr, refixed_line, flags=re.M | re.S | re.I)
            if not match_orig or not match_new:
                break
            if match_orig.groups()[1] == match_new.groups()[1]:
                break

            # revert what's inside the quotes
            refixed_line = match_new.groups()[0] + match_orig.groups()[1] + match_new.groups()[2]

    if refixed_line != line:
        return '{msg}: {line} => {refixed_line}'.format(msg=msg, refixed_line=refixed_line, line=line), refixed_line


def fix_sql_statements(line, keywords):
    """Detect and fix linting errors on various SQL kw."""
    def _subs(s, subs_list):
        # Substitute in s, according to the substitution list.
        sub_s = s
        for v1, v2 in subs_list:
            sub_s = sub_s.replace(v1, v2)
        return sub_s

    def _unsubs(s, subs_list):
        # Unsubstitute in s, by reversing the substitution list.
        sub_s = s
        for v1, v2 in subs_list:
            sub_s = sub_s.replace(v2, v1)
        return sub_s

    def _detect_and_fix_sql_errors(line, re_search, re_replace, msg):
        """Swap out/in string literals, to avoid in-literal error finding in SQL."""
        # Build substitution list, a unique substitution per literal.
        literals = re.findall("'([^']*)'", line)
        string_replacements = [("'{lit}'".format(lit=lit), "'__literal{li}__'".format(li=li))
                               for li, lit in enumerate(literals)]

        # Detect errors on safe literals
        res = detect_and_fix_errors(
            line=_subs(line, string_replacements), re_search=re_search, re_replace=re_replace, msg=msg)
        if res is None:
            return res
        error, new_line = res

        # Un-substitute, to get right errors & suggestions.
        if error:
            error = _unsubs(error, string_replacements)
            new_line = _unsubs(new_line, string_replacements)
        return error, new_line

    errors = []
    for kw in keywords:
        # Note: kw is the dest string (assumed all caps).
        # We compute a search string below.

        if kw.lower() not in line.lower():
            # This simple check vastly speeds up the linter. Woo!
            continue

        # Detect whether the keyword is a symbol.
        # Could have been passed in, but we're doing SQL heavy lifting in here,
        # so, localizing things here.
        is_symbolic = re.match(r'^[^a-zA-Z]+$', kw) is not None
        SYMBOLIC_CHARS = re.escape(''.join(set(''.join(SYMBOLIC_OPERATORS))))

        # Compute a search string, to make sure we match keywords with arbitrary whitespace
        kw_search = re.escape(kw).replace(r'\ ', r'\s+')

        # Surrounded by whitespace, to start.
        errors, line = aggregate_errors(  # end of string
            _detect_and_fix_sql_errors,
            errors=errors, line=line, msg=kw,
            re_search=r'([\s()]+){kw}\s+'.format(kw=kw_search),
            re_replace=r'\1{kw} '.format(kw=kw))
        errors, line = aggregate_errors(  # start of string, preserving leading whitespace.
            _detect_and_fix_sql_errors,
            errors=errors, line=line, msg=kw,
            re_search=r'([^\s])\s+{kw}([\s()]+)'.format(kw=kw_search),
            re_replace=r'\1 {kw}\2'.format(kw=kw))
        if is_symbolic:
            errors, line = aggregate_errors(
                _detect_and_fix_sql_errors,
                errors=errors, line=line, msg=kw,
                re_search=r'([^\s{re_sym}]){kw}([^\s{re_sym}])'.format(kw=kw_search, re_sym=SYMBOLIC_CHARS),
                re_replace=r'\1 {kw} \2'.format(kw=kw))

        # Or can start
        errors, line = aggregate_errors(
            _detect_and_fix_sql_errors,
            errors=errors, line=line, msg=kw,
            re_search=r'^{kw}\s+'.format(kw=kw_search),
            re_replace=r'{kw} '.format(kw=kw))
        if is_symbolic:
            errors, line = aggregate_errors(
                _detect_and_fix_sql_errors,
                errors=errors, line=line, msg=kw,
                re_search=r'([^\s{re_sym}]){kw}([^{re_sym}])'.format(kw=kw_search, re_sym=SYMBOLIC_CHARS),
                re_replace=r'\1 {kw}\2'.format(kw=kw))

        # Or can end
        errors, line = aggregate_errors(
            _detect_and_fix_sql_errors,
            errors=errors, line=line, msg=kw,
            re_search=r'([\s()]+){kw}$'.format(kw=kw_search),
            re_replace=r'\1{kw}'.format(kw=kw))
        if is_symbolic:
            # For symbolic operators only: spacing with text
            errors, line = aggregate_errors(
                _detect_and_fix_sql_errors,
                errors=errors, line=line, msg=kw,
                re_search=r'([^{re_sym}]){kw}([^\s{re_sym}])'.format(kw=kw_search, re_sym=SYMBOLIC_CHARS),
                re_replace=r'\1{kw} \2'.format(kw=kw))

        # Or can be the whole line
        errors, line = aggregate_errors(
            _detect_and_fix_sql_errors,
            errors=errors, line=line, msg=kw,
            re_search=r'^{kw}$'.format(kw=kw_search),
            re_replace=r'{kw}'.format(kw=kw))

    return errors, line


def fix_function_names(line, function_names=FUNCTION_NAMES):
    """Detect and fix linting errors on various SQL functions."""
    errors = []
    for func_name in function_names:
        errors, line = aggregate_errors(
            detect_and_fix_errors,
            line=line, errors=errors, msg=func_name,
            re_search=r'([^\w]){func_name}\s*\('.format(func_name=func_name),  # capitalization, spacing
            re_replace=r'\1{func_name}('.format(func_name=func_name))
    return errors, line


def fix_date_function_names(line, date_function_names_with_underscores=DATE_FUNCTION_NAMES_WITH_UNDERSCORES):
    """Detect and fix linting errors on date functions."""
    # Date functions are special; they can optionally have an underscore.
    # Let's first convert from DATE_FUNC to DATEFUNC
    errors = []
    for func_name in date_function_names_with_underscores:
        errors, line = aggregate_errors(
            detect_and_fix_errors,
            line=line, errors=errors, msg=func_name,
            re_search=r'{func_name}\s*\('.format(func_name=func_name),  # capitalization, spacing
            re_replace='{func_name}('.format(func_name=func_name.replace('_', '')))

    # Now let's deal with the desired (DATEFUNC) ones, for capitalization and spacing
    new_errors, line = fix_function_names(
        line, function_names=[dfn.replace('_', '') for dfn in date_function_names_with_underscores])
    errors += new_errors

    return errors, line


def fix_date_keywords(line, date_keywords=DATE_KEYWORDS):
    """Detect and fix linting errors on date keywords."""
    errors = []
    for kw in date_keywords:
        # FUNC(kw,
        errors, line = aggregate_errors(
            detect_and_fix_errors,
            errors=errors, line=line, msg=kw,
            re_search=r'\({kw},'.format(kw=kw),  # capitalization, spacing
            re_replace='({kw},'.format(kw=kw))
        # FUNC('kw',
        errors, line = aggregate_errors(
            detect_and_fix_errors,
            errors=errors, line=line, msg=kw,
            re_search=r"\('{kw}',".format(kw=kw),  # capitalization, spacing
            re_replace="('{kw}',".format(kw=kw))
    return errors, line


def fix_distinct(line):
    """Detect and fix linting errors on date keywords."""
    return detect_and_fix_errors(
        line=line, msg='DISTINCT',
        re_search=r'COUNT\(DISTINCT\s+([^\(].*)\)',  # capitalization, spacing
        re_replace=r'COUNT(DISTINCT(\1))')


def fix_docstring(line):
    """Detect or add docstrings to queries."""
    return detect_and_fix_errors(
        line=line,
        re_search=r'^((?!\/\*).*)$',
        re_replace=r"/* Add your comment here */\n\n\1",
        msg="File must start with query comment, starting with /*")
    return [], line


def fix_trailing_newline(line):
    """Detect or add trailing newlines to queries."""
    if line != "":
        return ("File must end with an empty line", line + "\n")
    return [], line


def fix_case_else_null(line):
    """Detect and remove ELSE NULL in CASE statements."""
    if 'ELSE' not in line:
        # Shortcut exit
        return [], line

    regexes = (
        (r'^\s*ELSE\s+NULL\s*$', ''),  # entire line
        # Commented for ELSE NULLIF()
        # (r'^(.*)\s+ELSE\s+NULL(.*)$', r'\1\2')  # partial line
    )

    errors = []
    for re_search, re_replace in regexes:
        errors, line = aggregate_errors(
            detect_and_fix_errors,
            errors=errors, line=line,
            msg="ELSE NULL not allowed in CASE statements; it's the default.",
            re_search=re_search,
            re_replace=re_replace)
    return errors, line


def fix_inline_comment(line):
    errors, new_line = [], line

    if line.strip().startswith('--'):
        # An inline comment
        new_line = re.sub(r'(\s*)--\s*(.*)$', r'\1-- \2', new_line).rstrip()
        if new_line != line:
            errors.append("Comment spacing error: {line} => {new_line}".format(
                line=line, new_line=new_line))

    return errors, new_line


def fix_explicit_boolean_comparisons(line):
    """Detect and fix explicit boolean comparisons (e.g. abc = TRUE)"""
    # True
    errors, line = aggregate_errors(
        detect_and_fix_errors,
        line=line, msg="No '= TRUE'",
        re_search=r"([^!])\s*(=|IS)\s*TRUE",
        re_replace=r"\1")
    errors, line = aggregate_errors(
        detect_and_fix_errors,
        errors=errors, line=line, msg="No '!= FALSE'",
        re_search=r"\s*(!=|IS\s+NOT)\s*FALSE",
        re_replace=r"")

    # False; can't do replacement.
    errors, line = aggregate_errors(
        detect_errors,
        errors=errors, line=line, msg="No '= FALSE'",
        re_search=r"[^!]\s*(=|IS)\s*FALSE",)
    errors, line = aggregate_errors(
        detect_errors,
        errors=errors, line=line, msg="No '!= TRUE'",
        re_search=r"\s*(!=|IS\s+NOT)\s*TRUE",)

    # No problem.
    return errors, line


def fix_bad_comments(line):
    """Sometimes we add "#" as a comment. Let's use --"""
    if not line.strip().startswith("#"):
        # Shortcut exit
        return [], line

    new_line = re.sub(r'^(\s*)\#(.*)$', r'\1--\2', line)
    if line == new_line:
        return [], line
    return ["Comments (Use --, not #): {line} => {new_line}".format(line=line, new_line=new_line)], new_line


def skip_line(line):
    """Determines if line should be skipped."""
    return line.endswith('-- noqa')


def lint_query(query, full_file=False, indent_increment=2):
    """Lint SQL query line-by-line.

    Parameters
    ----------
    query : string

    full_file ; boolean (Optional)
        Whether to lint as if it's a full SQL file (docstring, trailing newline) (Default: False)

    Returns
    -------
    (errors, sql) tuple:
        errors : list of errors (can be empty)
        sql : linted SQL
    """
    lines = query.split('\n')

    errors = []
    fixed_lines = []
    is_in_comment = False

    for li, line in enumerate(lines):
        # Store some state.
        is_in_comment = (is_in_comment or is_comment_open(line)) and not is_comment_close(line)

        # Start with no changes.
        fixed_line = line

        # Fix up sql statements
        if not is_in_comment and is_comment(fixed_line):
            errors, fixed_line = aggregate_errors(
                partial(fix_indenting, indent_increment=indent_increment),
                line=fixed_line,
                errors=errors,
                prev_lines=fixed_lines)
            errors, fixed_line = aggregate_errors(
                fix_inline_comment, line=fixed_line, errors=errors)

        elif not is_in_comment and not skip_line(fixed_line):
            # Lint single lines
            for fn in (partial(fix_sql_statements, keywords=DDL_STATEMENTS),
                       partial(fix_sql_statements, keywords=DDL_KEYWORDS),
                       partial(fix_sql_statements, keywords=SYMBOLIC_OPERATORS),
                       partial(fix_sql_statements, keywords=TYPES),
                       fix_function_names, fix_case_else_null, fix_explicit_boolean_comparisons,
                       # Removed: fix_date_function_names,
                       fix_date_keywords, fix_distinct, fix_bad_comments, fix_dispreferred_functions):
                errors, fixed_line = aggregate_errors(fn, line=fixed_line, errors=errors)

            # Lint lines with previous lines
            for fn in (fix_subquery_docstrings, fix_indenting, fix_double_newlines):
                errors, fixed_line = aggregate_errors(
                    fn, line=fixed_line, errors=errors,
                    prev_lines=fixed_lines)

        if full_file and fixed_line is not None:
            # Only do checks for docstrings / trailing whitespace for non-snippets.
            #
            # Muck around with initial / final whitespace (interact poorly with
            # the above whitespace-normalizing functions)
            if li == 0:
                errors, fixed_line = aggregate_errors(
                    fix_docstring,
                    errors=errors, line=fixed_line)
            elif li == len(lines) - 1:
                errors, fixed_line = aggregate_errors(
                    fix_trailing_newline,
                    line=fixed_line, errors=errors)

        if fixed_line is not None:
            # Allow line elimination by returning None.
            # Fixed line can contain multiple lines,
            # so we need to decimate into lines again before appending.
            fixed_lines += fixed_line.split('\n')

    return errors, '\n'.join(fixed_lines)


def lint_sql(text, full_file=False):
    """Lint sql line-by-line.

    Parameters
    ----------
    text : string

    full_file ; boolean (Optional)
        Whether to lint as if it's a full SQL file (docstring, trailing newline) (Default: False)

    Returns
    -------
    errors : list of errors (can be empty)
    sql : linted SQL
    """
    queries = split_queries(text)
    new_queries, all_errors = [], []
    for qi, query in enumerate(queries):
        # Only lint full-file on the first query.
        errors, new_query = lint_query(query, full_file=full_file and qi == 0)
        new_queries.append(new_query)
        all_errors += errors
    return all_errors, join_queries(new_queries)


if __name__ == '__main__':
    import sys

    from classypy.linters.base import Linter, LinterParser

    parser = LinterParser(description='Find and fix SQL linting errors.')
    args = vars(parser.parse_args())

    linter = Linter(linter_func=lint_sql, ext='.sql')
    sys.exit(linter.lint_path(**args))

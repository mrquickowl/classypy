import copy
import re

import frontmatter
import pandas as pd

from classypy.devops import logger
from classypy.io.google import GoogleSheetsDataset
from classypy.util.caching import cache_dataframe

RENDER_FREQUENCIES = (
    'daily', 'weekly', 'quarterly',
    'monthly', 'yearly', 'once',
)
DOC_TYPES = (
    'blog', 'data-dive', 'data-intelligence-audit',
    'report-proposal',
    'report-short', 'report-short-di',
    'report-long', 'report-long-di',
)
REPORT_LINTER_FUNCTION_NAMES = {
    # Did function names, instead of functions,
    # so that this code (and check) can live up top
    'blog': ('lint_blog',),
    'data-dive': ('lint_data_dive',),
    'data-intelligence-audit': ('lint_data_intelligence_audit',),
    'report-proposal': tuple(),
    'report-short': ('lint_report_short',),
    'report-short-di': ('lint_report_short',),
    'report-long': ('lint_report_long',),
    'report-long-di': ('lint_report_long',),
}
assert all([doc_type in REPORT_LINTER_FUNCTION_NAMES for doc_type in DOC_TYPES]), (
    "Make sure we have a linter for each doc type")

TAG_METADATA = None
TAGS = None  # Will be fetched dynamically


def fetch_tags(**secrets):
    """Helper function to retrieve remote and local metadata (with caching)."""
    global TAG_METADATA, TAGS

    @cache_dataframe
    def _fetch_tags_to_dataframe():
        logger.info("Fetching tags...")
        dataset = GoogleSheetsDataset(
            client_id=secrets.get('GOOGLE_API_CLIENT_ID'),
            client_secret=secrets.get('GOOGLE_API_CLIENT_SECRET'),
            credentials_json=secrets.get('GOOGLE_API_SHEETS_CREDENTIALS'))
        return dataset.fetch(
            spreadsheet_id=secrets.get('KR_PRIVATE_GOOGLE_SHEET'),
            sheet_name='Tags',
            sheet_range='A1:C1000',
            force=True,
            verbose=logger.verbosity())

    # Get sheets metadata
    if TAG_METADATA is None:
        # Only validate the private sheet here; public is controlled by us.
        TAG_METADATA = _fetch_tags_to_dataframe(csv_file='tags.csv')
        TAGS = (
            TAG_METADATA
            ["Tag"]
            .values
            .tolist()
        )
        logger.debug("Fetched {num_tags} tags".format(num_tags=len(TAGS)))

    return TAGS


def get_mistletoe():
    # Helper function to avoid Py2/Py3 import issues.
    import mistletoe
    return mistletoe


def get_doc_types(fm):
    return fm.get("categories", tuple())


def ensure_tags_contain_parent_tag(tags):
    """Ensure hierarchical parents of tag are also contained within tags."""
    errors = []
    new_tags = copy.deepcopy(tags)

    for tag in [t for t in tags if '/' in t]:
        tag_hierarchy = tag.split('/')
        parent_tag = '/'.join(tag_hierarchy[:-1])  # rejoin all but final tag
        if parent_tag not in tags:
            errors.append("Parent tag '{parent_tag}' must be included in tags, since '{tag}' is.".format(
                parent_tag=parent_tag, tag=tag))
            new_tags.append(parent_tag)
    return errors, new_tags


def should_suggest_tag(tag, keywords, content):
    """Return non-None if tag should be recommended based on keywords appearing in content."""
    for keyword in keywords:
        if not keyword.strip():
            continue
        if re.search(r"[\s([]{kw_cln}[\s.)\],;:/-]".format(kw_cln=keyword.strip().lower()), content, flags=re.M | re.I):
            # Found the keyword in the content; suggest this tag
            return tag


def suggest_tags(tags, content, raw_text):
    """Ensure that content-suggested keywords are contained in tags."""
    suggested_tags = []
    for ri, row in TAG_METADATA.iterrows():
        if should_suggest_tag(
            tag=row["Tag"],
            keywords=row["Keywords"].split(",") if not pd.isnull(row["Keywords"]) else [],
            content=content
        ):
            suggested_tags.append(row["Tag"])

    missing_tags = set(suggested_tags) - set(tags)
    # Find tags that appear as comments
    commented_tags = [tag for tag in missing_tags
                      if re.search(
                          # # "{tag}", with variable whitespace and quoting
                          r"\#\s*['\"]{tag}['\"]".format(tag=tag.replace('/', r'\/')),
                          raw_text)]

    # Strip out tags that appear as comments
    missing_tags = sorted(missing_tags - set(commented_tags))
    if missing_tags:
        new_tags = tags + missing_tags
        # Emit one error per missing tag
        return ["Missing suggested tag {missing_tag}".format(missing_tag=missing_tag)
                for missing_tag in missing_tags], new_tags
    return [], tags


def lint_tags(tags, content, raw_text, all_tags=None):
    """Identify tag issues."""
    all_tags = all_tags or TAGS
    errors = []
    new_tags = tags

    # Search for missing suggested tags
    new_errors, new_tags = suggest_tags(tags, content=content, raw_text=raw_text)
    errors += new_errors

    # Search for missing parent tags
    new_errors, new_tags = ensure_tags_contain_parent_tag(tags=new_tags)
    errors += new_errors

    return errors, new_tags


def lint_frontmatter(fm, content, raw_text, all_tags=None):
    """Identify frontmatter issues."""
    all_tags = all_tags or TAGS

    defaults = {
        'title': {'default': '[TITLE]'},
        'author': {'default': '[AUTHOR]'},
        'date': {
            'default': "YYYY-MM-DD",
            'formats': (
                r"\d{4}-\d{2}-\d{2}",
                r"`r format\(Sys\.Date\(\)\)`"
            )
        },
        'render_frequency': {'choices': RENDER_FREQUENCIES, 'default': "render_frequency"},
        'categories': {'type': 'list', 'choices': DOC_TYPES, 'default': []},
        'tags': {'type': 'list', 'choices': all_tags, 'default': []},
    }

    for key, props in defaults.items():
        fm_val = fm.get(key, props.get('default'))

        if fm_val == defaults.get('default'):
            # Check the value is not the default
            return ["Add frontmatter {key} / change from default".format(key=key)], fm

        if props.get("type", "string") == "list":
            assert 'choices' in props, "make sure we have choices for this list prop"
            # Check that list values are of the valid choices.
            bad_vals = list(set(fm_val) - set(props['choices']))
            if len(bad_vals) > 0:
                return ["Invalid value(s) for {key}: ({bad_vals}); choose from: {good_vals}".format(
                    key=key, bad_vals=', '.join(bad_vals), good_vals=', '.join(props['choices']))], fm

        if props.get('formats') and not any([re.match(fmt, fm_val) for fmt in props['formats']]):
            return [
                "Invalid format for {key}; '{val}' does not match any formats '{formats}'".format(
                    key=key, val=fm_val, formats=props['formats'])
            ], fm

        if key == 'tags' and 'tags' in fm:
            errors, fm['tags'] = lint_tags(fm['tags'], content=content, all_tags=all_tags, raw_text=raw_text)
            if errors:
                return errors, fm
    return [], fm


def lint_doc(content):
    doc = get_mistletoe().Document(content)

    if len(doc.children) < 1 or not isinstance(doc.children[0], get_mistletoe().block_token.CodeFence):
        return ["Must start markdown doc with R code block "], content
    return [], content


def lint_blog(fm, content):
    return [], fm, content


def lint_data_dive(fm, content):
    return [], fm, content


def lint_data_intelligence_audit(fm, content):
    return [], fm, content


def lint_report_short(fm, content):
    return [], fm, content


def lint_report_long(fm, content):
    return [], fm, content


def lint_rmd_report(text, full_file=False, **secrets):
    errors = []

    post = frontmatter.loads(text)
    fm, content = post.metadata, post.content
    new_fm, new_content = fm, content

    all_tags = fetch_tags(**secrets)
    new_errors, new_fm = lint_frontmatter(new_fm, content=new_content, all_tags=all_tags, raw_text=text)
    errors += new_errors

    new_errors, new_content = lint_doc(new_content)
    errors += new_errors

    # Lint doc-specific things
    doc_types = get_doc_types(new_fm)
    linter_fns = [globals().get(linter_fn_name) for doc_type in doc_types
                  for linter_fn_name in REPORT_LINTER_FUNCTION_NAMES.get(doc_type, [])]
    for linter_fn in linter_fns:
        new_errors, new_fm, new_content = linter_fn(fm=new_fm, content=new_content)
        errors += new_errors

    # reconstitute doc, with extra newline (we like that!)
    new_doc = frontmatter.dumps(frontmatter.Post(content, **new_fm)) + "\n"
    return errors, new_doc


if __name__ == '__main__':
    import sys

    from classypy.devops.parsers import ArgumentParserWithSecrets
    from classypy.linters.base import Linter, LinterParser

    class KRParser(LinterParser, ArgumentParserWithSecrets):
        def __init__(self, *args, **kwargs):
            ArgumentParserWithSecrets.__init__(self, *args, **kwargs)
            LinterParser.__init__(self, *args, **kwargs)

    parser = KRParser(description='Find and fix knowledge repository report linting errors.')
    # Allow secrets to be passed on the command-line, to deal with environment variables
    parser.add_argument('--sheet-id', nargs='?', default='1GCZgS7Kfo7IjnYowzoAy2wWYQJL5mZo6ZuALO8Efn6g')
    args = vars(parser.parse_args())

    # Set secrets from the command-line
    args['KR_PRIVATE_GOOGLE_SHEET'] = args.get('KR_PRIVATE_GOOGLE_SHEET', args['sheet_id'])

    linter = Linter(linter_func=lint_rmd_report, ext='.Rmd')
    sys.exit(linter.lint_path(**args))

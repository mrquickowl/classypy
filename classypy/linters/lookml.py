import os.path as op
import re

import numpy as np

from classypy.devops import logger
from classypy.linters.sql import get_indent_level, indent_line, lint_sql

ALLOWED_RAW_VIEW_GROUP_LABELS = (
    "Address", "Annotations", "Core", "Dates and Times",
    "Financial", "Foreign Keys", "Garbage (hidden)", "Metadata", "Status",
)
DEFAULT_LOOKER_INDENTING = 2
KNOWN_ANNOTATION_SCHEMAS = (
    'annotated',
    # TODO: remove classy_org from annotated schemas, when we get there
    # Currently, too much effort to annotate
    'classy_org',
    'looker',
    'query',
    'redshift',
)


def is_hidden_view(view_name):
    # Some views only appear within explores, as a special case.
    return view_name.startswith("_")


def is_raw_view(view_name=None, view_file=None, known_annotation_schemas=KNOWN_ANNOTATION_SCHEMAS,
                known_raw_schemas=tuple()):
    """Returns True if the given filename represents a raw view.

    Parameters
    ----------
    view_name: string
    view_file: string (optional)
    known_annotation_schemas: iterable of strings
        These are black list of schemas known to be annotation views, not raw views.
    known_raw_schemas: iterable of strings
        These are white list of schemas known to be raw views.
        If specified, only views identified to be within
        these schemas will return True

    Returns
    -------
    boolean
        True if the named view (& view file) is for a raw view.
    """
    view_name = view_name or op.basename(view_file)
    if known_raw_schemas:
        # We got an inclusion/whitelist
        return any([view_name.startswith("{schema}_".format(schema=schema))
                    for schema in known_raw_schemas])
    else:
        # We got an exclusion/blacklist
        return all([not view_name.startswith("{schema}_".format(schema=schema))
                    for schema in known_annotation_schemas])


def is_model_file_lookml(lookml):
    """Determine from lookml if file is a model file"""
    return 'include: "_shared_settings.model"' in lookml  # very classy-specific


def is_dashboard_file_lookml(lookml):
    """Determine from lookml if file is a dashboard file"""
    return "- dashboard:" in lookml


def re_search(*args, **kwargs):
    match = re.search(*args, **kwargs)
    if match and match.groups():
        if len(match.groups()) == 1:
            return match.groups()[0]
        return match.groups()
    return match


def get_warehouse_columns_from_dimension(dimension_lookml):
    """Returns the database columns leveraged in a dimension/dimension group."""
    dimension_sql = re_search(
        r".*sql:(.*);;", dimension_lookml or '',
        flags=re.M | re.S)
    # Find column surrounded by quotes
    dimension_columns = re.findall(
        r"\$\{TABLE\}\.\"([^\"]+)\"",
        dimension_sql or '')
    # Find column ending with a valid character (e.g. space, comma, paren)
    dimension_columns = dimension_columns or re.findall(
        r"\$\{TABLE\}\.([^\s,)]+)[\s,)]",
        dimension_sql or '',
        flags=re.M | re.S)
    # Find column ending line
    dimension_columns = dimension_columns or re.findall(
        r"\$\{TABLE\}\.([^\s,)]+)$",
        dimension_sql or '',
        flags=re.M)

    # Normalize: lowercase, strip quotes
    dimension_columns = [dimension_column.strip().replace('"', "") for dimension_column in dimension_columns]

    if not dimension_columns:
        logger.debug("Could not find table column from dimension lookml {dimension_lookml}".format(
            dimension_lookml=dimension_lookml))
    return dimension_columns


def get_warehouse_table_from_view(view_lookml):
    """Returns view table name (via the sql_table_name property)."""
    return re_search(r"sql_table_name:\s*([^\s]+)\s*;;", view_lookml or '')


def yield_object_matches(lookml, object_types=('dimension', 'dimension_group', 'measure')):
    """Yield all matches for objects of the given types."""
    assert not any([ot.endswith('s') for ot in object_types]), (
        "Make sure no object types end with 's' (common error)")

    for object_type in object_types:
        # Match open and close braces (NOTE: will stop after first embedded objects)
        re_str = r"%s:([^{]*\{(?!\s\{).*?\s\})" % object_type
        matched_lookmls = re.findall(re_str, lookml, flags=re.M | re.S)
        for matched_lookml in matched_lookmls:
            # Found matches, but if it holds nested objects, it's gonna be broken.
            # Search for mismatched braces.
            # NOTE: this will totally fail on literals/comments containing }
            mismatched_count = matched_lookml.count("{") - matched_lookml.count("}")
            if mismatched_count != 0:
                # We found mismatched braces. Do our best to append more data
                # until we can match all opens with closes,
                # or until we get to the end of the string.

                # start after the current match
                cur_pos = lookml.index(matched_lookml) + len(matched_lookml)
                # iterate over all close braces
                for match in re.findall(r'([^\}]*\})', lookml[cur_pos:], re.M | re.S):
                    # Append the string
                    matched_lookml += match
                    # Update the mismatch count
                    mismatched_count += match.count("{") - match.count("}")
                    # If we took care of the mismatches, we're done!
                    if mismatched_count == 0:
                        break

            # We either have no mismatches or no more things we can try.. so just spit it out!
            yield object_type, matched_lookml


def is_liquid_template_tag(line):
    """Is the text a Liquid template tag?"""
    line = line.strip()
    return line.startswith('{%') and line.endswith('%}')


def strip_comments(lookml_snippet):
    """Strips trailing comments from lookml snippets"""
    if lookml_snippet is None:
        return None
    return re.sub(r"^(.*)  #.*$", r"\1", lookml_snippet, flags=re.M | re.S)


def get_object_name(lookml):
    """Split out object name from a LookML object string."""
    return re_search(r"([a-zA-Z_0-9]+)", lookml)


def _add_noqa_liquid_template_tags(sql):
    """Add the noqa flag to all lines which are Liquid template tags."""
    lines = sql.split('\n')
    lines_with_noqa_flag = [li + '  -- noqa' if is_liquid_template_tag(li) else li for li in lines]
    return '\n'.join(lines_with_noqa_flag)


def _remove_noqa_liquid_template_tags(sql):
    """Remove the noqa flag from all lines which are Liquid template tags."""
    lines = sql.split('\n')
    for i in range(len(lines)):
        new_line = lines[i].replace('  -- noqa', '')
        if is_liquid_template_tag(new_line):
            lines[i] = new_line
    return '\n'.join(lines)


def unindent_lookml_sql(sql):
    """Eliminate indenting for SQL snippets embedded in LookML"""
    sql_lines = sql.split('\n')
    if len(sql_lines) <= 1:
        # Single line: strip away indenting
        sql = sql.strip()
    else:
        if sql_lines[0] == '':
            # Remove (expected) leading newline
            sql_lines = sql_lines[1:]
        # Unindent max shared spacing
        indent_levels = [get_indent_level(line) for line in sql_lines]
        max_shared_spacing = np.min([lvl for lvl in indent_levels if lvl > 0])
        unindented_lines = [indent_line(line, indent_level=max(0, indent_level - max_shared_spacing))
                            for line, indent_level in zip(sql_lines, indent_levels)]
        # Add in (expected) trailing newline
        if sql_lines[-1] != '':
            sql_lines.append('')
        # Reconstitute into SQL snippet
        sql = '\n'.join(unindented_lines)
    return sql


def reindent_lookml_sql(sql, indent_increment=DEFAULT_LOOKER_INDENTING):
    """Add indenting around SQL snippets necessary for embedding in LookML"""
    sql_lines = sql.split('\n')

    if len(sql_lines) <= 1:
        # Add single leading and trailing spaces
        sql = ' ' + indent_line(sql, indent_level=0) + ' '

    else:
        # Re-indent / reconstitute
        reindented_lines = []
        for line in sql_lines:
            cur_indent_level = get_indent_level(line)
            if line != '':
                line = indent_line(line, indent_level=cur_indent_level + indent_increment * 3)
            reindented_lines.append(line)  # don't add trailing whitespace to empty lines

        if reindented_lines[0] != '':
            reindented_lines.insert(0, '')
        if reindented_lines[-1] != '':
            reindented_lines.append('')

        # add trailing whitespace for final ';;'
        reindented_lines[-1] = indent_line('', indent_level=indent_increment * 3)

        sql = '\n'.join(reindented_lines)
    return sql


def lint_array_field(lookml, field_name, acceptable_values):
    """Enforce that array has desired values."""
    errors, new_lookml = [], lookml

    matched_arrays = re.findall(
        r"\s+{field_name}:\s*\[([^\]]+)\]".format(field_name=field_name),
        new_lookml,
        flags=re.M | re.S)
    for matched_array in matched_arrays or []:
        current_values = [m.strip() for m in matched_array.split(",") if m.strip() != ""]
        new_values = current_values + list(set(acceptable_values) - set(current_values))

        if len(current_values) >= len(new_values):
            # no problem
            nonstandard_values = set(current_values) - set(acceptable_values)
            if any(nonstandard_values):
                logger.warning("Atypical array values: {values} not in {acceptable_values}".format(
                    values=", ".join(list(nonstandard_values)),
                    acceptable_values=acceptable_values))
            continue
        if len(new_values) == len(acceptable_values):
            new_values = acceptable_values  # take ordering
        replacement = "\n      " + ",\n      ".join(new_values) + "\n    "  # make assumption about spacing.

        # Swap out match with replacement, and report on it.
        new_lookml = new_lookml.replace(matched_array, replacement)
        errors.append("{matched_array} => {replacement}".format(
            matched_array=matched_array,
            replacement=replacement))

    return errors, new_lookml


def lint_intervals(lookml):
    return lint_array_field(
        lookml=lookml,
        field_name="intervals",
        acceptable_values=(
            "day", "hour",  # not: minute, second
            "month", "quarter", "year",  # not: week
            "year",
        ))


def lint_lookml_sql(lookml):
    """Enforce SQL standards on LookML SQL snippets."""
    new_lookml = lookml
    errors = []

    # Extract SQL snippet
    matched_lookmls = re.findall(r"\s+sql:(.*?);;", new_lookml, flags=re.M | re.S)

    for old_sql in matched_lookmls or []:
        # Do an ad-hoc test to make sure a missing ;; didn't just go crazy
        is_missing_double_semis = any([object_name in old_sql
                                       for object_name in ("dimension:", "dimension_group:", "measure:")])
        if is_missing_double_semis:
            errors.append("Missing ;; detected in: {old_sql}".format(old_sql=old_sql))
            continue  # get out of here before really bad stuff happens

        # Prevent Liquid templating from getting linted by adding noqa
        sql_with_noqa = _add_noqa_liquid_template_tags(old_sql)
        unindented_sql = unindent_lookml_sql(sql_with_noqa)
        new_errors, new_unindented_sql = lint_sql(unindented_sql, full_file=False)

        # Now remove noqa from Liquid templating
        new_sql_with_noqa = reindent_lookml_sql(new_unindented_sql)
        new_sql = _remove_noqa_liquid_template_tags(new_sql_with_noqa)

        # Make sure that errors are reported
        if not new_errors and new_sql != old_sql:
            # TODO: push this logic into unindent
            new_errors.append("SQL LookML indenting was corrected: {old_sql} => {new_sql}".format(
                old_sql=old_sql, new_sql=new_sql))

        # And apply any changes
        new_lookml = new_lookml.replace("sql:" + old_sql + ";;", "sql:" + new_sql + ";;")
        errors += new_errors
    return errors, new_lookml


def lint_query_timezones(lookml):
    """Enforce a no-query-timezones policy."""
    new_lookml = lookml
    errors = []

    if '    query_timezone:' in new_lookml:
        new_lookml = re.sub('    query_timezone: .*\n', '', new_lookml)
        errors.append("Do not specify query timezones.")

    return errors, new_lookml


def lint_timeframes(lookml):
    """Enforce that all timeframes have desired values."""
    return lint_array_field(
        lookml=lookml,
        field_name="timeframes",
        acceptable_values=(
            "raw", "time", "hour", "hour_of_day", "date", "day_of_week", "day_of_month", "day_of_year",
            "week", "week_of_year", "month", "month_name", "month_num", "fiscal_month_num",
            "quarter", "fiscal_quarter", "fiscal_quarter_of_year", "year", "fiscal_year",
        ))


def lint_required_joins_in_joins(lookml):
    """Enforce 'from' used with 'required_join'."""
    errors, new_lookml = [], lookml

    for _, matched_lookml in yield_object_matches(new_lookml, object_types=("join",)):
        if "required_joins:" not in matched_lookml:
            continue
        if "from:" in matched_lookml:
            continue

        # Remove group label, label
        replacement = re.sub(
            r"^(\s*)([^{\s]+)(\s*\{.*\n)",  # grab inital line defining join (spacing, name, brackets)
            r"\1\2\3    from: \2\n",  # echo it back, add from: join_name
            matched_lookml)

        # Swap out matched_lookml with replacement, and report on it.
        new_lookml = new_lookml.replace(matched_lookml, replacement)
        errors.append("{matched_lookml} => {replacement}".format(
            matched_lookml=matched_lookml, replacement=replacement))

    return errors, new_lookml


def lint_required_joins_max_of_one(lookml):
    """Enforce max of one view listed in required_joins."""
    errors, new_lookml = [], lookml

    for _, matched_lookml in yield_object_matches(new_lookml, object_types=("join",)):
        if "required_joins:" not in matched_lookml:
            continue

        required_joins = re_search(r"(\[[^]]+\].*)\n", matched_lookml)
        if required_joins.endswith("# noqa"):
            # Valid ignore
            continue

        num_views = 1 + required_joins.count(",")
        if num_views > 1:
            errors.append(
                "required_joins must have one and only one view listed; "
                "we found {num_views} in '{required_joins}'.".format(
                    num_views=num_views, required_joins=required_joins))

    return errors, new_lookml


def lint_join_name_order(lookml):
    """Enforce alphabetization in joins."""
    errors, new_lookml = [], lookml

    for _, explore_lookml in yield_object_matches(new_lookml, object_types=("explore",)):
        explore_name = get_object_name(explore_lookml)
        join_matches = list(yield_object_matches(explore_lookml, object_types=("join",)))
        join_names = [get_object_name(matched_lookml) for _, matched_lookml in join_matches]

        if sorted(join_names) != join_names:
            errors.append("Alphabetize JOINs in {explore_name}: {sorted_join_names}".format(
                explore_name=explore_name, sorted_join_names=', '.join(sorted(join_names))))

    return errors, new_lookml


def _flip_join_order(sql_on, names_in_join):
    """Auto-correct Flip the join order"""
    assert len(names_in_join) == 2, "Ensure there are two JOINs to swap places"

    join_conditions = [re_search(r"(\$\{%s\.[^}]+\})" % name, sql_on) for name in names_in_join]
    assert all(join_conditions), "Ensure we can parse the {table.column} on both sides of the join"

    join_left, join_right = join_conditions
    return (
        sql_on
        .replace(join_left, "__tmp__")
        .replace(join_right, join_left)
        .replace("__tmp__", join_right)
    )


def _lint_join_sql_on(source_name, sql_on, names_in_join, join_lookml):
    """
    Lint sql_on to make sure that, for the view, the proper left and right sides are specified.
    private function used exclusively in lint_join_sql_order

    Parameters
    ----------
    source_name : string
        Source view name of explore
    sql_on : string
        Parsed sql_on value from a single join
    names_in_join : list
        2-element list that defines left and right view names in join
    join_lookml : string
        original join lookml; passed for auto-updates

    Returns
    -------
    """
    assert sql_on, "Make sure that we've filtered out failed sql_on parses before here"

    errors, new_join_lookml = [], join_lookml

    joined_view_name = get_object_name(new_join_lookml)
    if source_name not in names_in_join:
        errors.append("Explore name '{source_name}' should be found inside sql_on '{sql_on}'.".format(
            source_name=source_name, sql_on=sql_on))

    elif joined_view_name not in names_in_join:
        # Only a useful error if check above passes.
        errors.append(
            "Joined view name '{joined_view_name}' should be found inside sql_on '{sql_on}'.".format(
                joined_view_name=joined_view_name, sql_on=sql_on))

    elif source_name != names_in_join[0]:
        # Only a useful error if checks above pass.
        errors.append(
            "Join source name '{source_name}' must appear first in joined view sql_on '{sql_on}'.".format(
                source_name=source_name, sql_on=sql_on))
        # Flip join order
        new_sql_on = _flip_join_order(sql_on, names_in_join=names_in_join)
        new_join_lookml = new_join_lookml.replace(sql_on, new_sql_on)

    # Return everything needed for an update
    return errors, new_join_lookml


def _parse_view_names_from_sql_on(join_lookml):
    """Pull view names out of left and right sides of a join. Assumes a single-line match."""
    errors = []
    names_in_join = []
    sql_on = (
        (re_search(r"sql_on:(.*);;", join_lookml, flags=re.M | re.S) or '')
        .strip()
        # If there are multiple lines in a sql_on, choose the first.
        # TODO: fix / accept all awkward cases:
        # 1. JOIN with a filter (e.g. transactions.is_valid); filter must come second (this is good)
        # 2. In a *_to_many relationship with required_joins, this is valid (necessary!)
        #   We currently require the part of the join to the required_join to go first
        #   and the part of the join to the explore to go second.
        #   We really should define an order, and validate both parts of the join.
        .split("\n", 1)[0]
    )

    if not sql_on and 'foreign_key:' in join_lookml:
        # Valid case of no parseable sql_on: where foreign_key is used.
        pass

    elif not sql_on:
        logger.debug("Could not parse sql_on from {join_lookml}; moving on ...".format(
            join_lookml=join_lookml))

    else:
        # Grab two sides of sql_on, and validate parse
        names_in_join = re.findall(r"\$\{([^}.]+)", sql_on)
        if len(names_in_join) != 2:
            errors.append("There must be two values in a JOIN sql_on ({num_names} found in '{sql_on}').".format(
                num_names=len(names_in_join), sql_on=sql_on))

    return errors, sql_on, names_in_join


def _get_explore_view_name(explore_lookml):
    """Get the lookml reference name for an explore"""
    if 'view_name:' in explore_lookml:
        return re_search(r'view_name:\s*([^\s]+)', explore_lookml)
    else:
        return get_object_name(explore_lookml)


def lint_join_sql_order(lookml):
    """Enforce join order having explore / required_join first, joined view second, in `sql_on` property."""
    errors, new_lookml = [], lookml

    for _, explore_lookml in yield_object_matches(new_lookml, object_types=("explore",)):
        new_explore_lookml = explore_lookml  # prep for cascading replacement

        explore_name = _get_explore_view_name(new_explore_lookml)
        logger.debug("Processing join SQL order for explore {explore_name}".format(explore_name=explore_name))

        for _, join_lookml in yield_object_matches(new_explore_lookml, object_types=("join",)):
            new_join_lookml = join_lookml  # prep for cascading replacement
            new_errors, sql_on, names_in_join = _parse_view_names_from_sql_on(new_join_lookml)
            if not sql_on:
                # Could be an error case or valid case, but either way, nothing more to do.
                errors += new_errors
                continue

            # Validate ordering of names
            if "required_joins:" not in new_join_lookml:
                new_errors, new_join_lookml = _lint_join_sql_on(
                    source_name=explore_name,
                    sql_on=sql_on,
                    names_in_join=names_in_join,
                    join_lookml=new_join_lookml)
                errors += new_errors

            else:
                # take care of required_joins. More complex ...
                required_joins = re_search(r"\[([^]]+)\].*\n", new_join_lookml)
                num_views = 1 + required_joins.count(",")
                if num_views > 1:
                    # Cannot handle more than one view, currently
                    continue
                required_join_view_name = required_joins
                # Store result to temp variable
                new_errors, new_join_lookml = _lint_join_sql_on(
                    source_name=required_join_view_name,
                    sql_on=sql_on,
                    names_in_join=names_in_join,
                    join_lookml=new_join_lookml)
                errors += new_errors

            # Cascade any change all the way down the chain of lookml
            old_explore_lookml, new_explore_lookml = (
                new_explore_lookml, new_explore_lookml.replace(join_lookml, new_join_lookml))
            new_lookml = new_lookml.replace(old_explore_lookml, new_explore_lookml)

    return errors, new_lookml


def _lint_section_comments(lookml, matched_lookml, group_label, section_comment, object_type):
    """
    Apply linting rules to section comments.

    Parameters
    ----------
    lookml : string
        LookML document
    matched_lookml : string
        Substring LookML snippet for a single object (e.g. dimension)
    group_label : string
        Pre-parsed group label, from the matched_lookml snippet.
    section_comment : string
        Pre-parsed section comment from LookML nearby matched_lookml
    object_type : string
        Type of object we're working with (e.g. dimension)

    Returns
    -------
    errors : list
        List of errors encountered during parsing.
    new_lookml : string
        Updated version of lookml, when correctable errors are encountered.
    """
    errors, new_lookml = [], lookml

    if group_label is None:
        # No group label, so skip.
        pass

    elif section_comment not in new_lookml:
        errors.append("Missing {object_type} section comment for group '{group_label}'".format(
            object_type=object_type, group_label=group_label))

    elif new_lookml.index(section_comment) > new_lookml.index(matched_lookml):
        errors.append("Section {object_type} comment for group '{group_label}' appears after dimensions.".format(
            object_type=object_type, group_label=group_label))

    elif re_search(
        r"\n\n\n  {section_comment}\n\n".format(
            section_comment=re.escape(section_comment)),
        new_lookml
    ) is None:
        errors.append("Section {object_type} comment for group '{group_label}' has improper spacing.".format(
            object_type=object_type, group_label=group_label))

    return errors, new_lookml


def lint_dimension_group_section_comments(lookml):
    """Enforce that all dimension group labels are preceded by a section comment."""
    errors, new_lookml = [], lookml

    for object_type, matched_lookml in yield_object_matches(new_lookml, object_types=("dimension", "dimension_group")):
        group_label = re_search(r"\n\s*group_label:\s['\"]?([^'\"]+)['\"]?\n", matched_lookml)
        section_comment = "## {group_label}".format(group_label=group_label)

        new_errors, new_lookml = _lint_section_comments(
            lookml=new_lookml, matched_lookml=matched_lookml, object_type=object_type,
            group_label=group_label, section_comment=section_comment)
        # Only add unique errors.
        errors += [e for e in new_errors if e not in errors]

    return errors, new_lookml


def lint_measure_group_section_comments(lookml):
    """Enforce that all measure group labels are preceded by a ## Measure comment"""
    errors, new_lookml = [], lookml

    for object_type, matched_lookml in yield_object_matches(new_lookml, object_types=("measure", "set")):
        if object_type == 'set':
            group_label = "Field Sets"
        else:
            group_label = object_type[:1].upper() + object_type[1:].lower() + "s"
        section_comment = "## {group_label}".format(group_label=group_label)

        new_errors, new_lookml = _lint_section_comments(
            lookml=new_lookml, matched_lookml=matched_lookml, object_type=object_type,
            group_label=group_label, section_comment=section_comment)
        # Only add unique errors.
        errors += [e for e in new_errors if e not in errors]

    return errors, new_lookml


def lint_dimension_group_labels_in_raw_views(lookml, strict=True):
    """Enforce dimension groups are of allowed labels."""
    errors, new_lookml = [], lookml

    for _, view_lookml in yield_object_matches(new_lookml, object_types=("view",)):
        allowed_group_labels = list(ALLOWED_RAW_VIEW_GROUP_LABELS)

        for _, matched_lookml in yield_object_matches(new_lookml, object_types=("dimension", "dimension_group")):
            group_label = re_search(r"\n\s*group_label:(.*)\n", matched_lookml)

            if not group_label:
                if strict and 'primary_key' not in matched_lookml:
                    # Failure case 1: no group label.
                    errors.append("Missing group label: {matched_lookml}".format(matched_lookml=matched_lookml))
                continue

            group_label = group_label.strip().replace("\"", "")
            if group_label not in allowed_group_labels:
                # Failure case 2: unknown group name
                errors.append("Unknown / disallowed group label: {group_label}".format(group_label=group_label))

    return errors, new_lookml


def lint_measure_group_labels(lookml):
    """Enforce measure groups are of allowed labels."""
    errors, new_lookml = [], lookml

    for _, matched_lookml in yield_object_matches(new_lookml, object_types=("measure",)):
        if re_search(r"hidden:\s*yes", matched_lookml):
            # Don't lint hidden measures
            continue

        object_name = get_object_name(matched_lookml)
        object_subtype = re_search(r"type:\s*([a-zA-Z_0-9]+)", matched_lookml)  # split out object type.
        group_label = re_search(r"\n\s*group_label:(.*)\n", matched_lookml)

        if object_name == "count" and group_label:
            # Failure case 1: group label on count
            replacement = re.sub(r"\n\s*group_label:[^\n]*\n", "\n", matched_lookml)
            new_lookml = new_lookml.replace(matched_lookml, replacement)
            errors.append("Remove group label from counts.")

        elif object_subtype != "count" and not group_label:
            # Failure case 2: no group label
            errors.append("Missing group label: {matched_lookml}".format(matched_lookml=matched_lookml))

    return errors, new_lookml


def lint_measure_has_count(lookml):
    """Enforce count measure on every view."""
    errors, new_lookml = [], lookml

    has_count = False

    for _, matched_lookml in yield_object_matches(new_lookml, object_types=("measure",)):
        object_subtype = re_search(r"type:\s*([a-zA-Z_0-9]+)", matched_lookml)  # split out object type.
        # Every view should have count. Keep track.
        has_count |= (object_subtype == "count")

    if not has_count:
        # Failure case 1: missing count
        errors.append("Missing count measure.")

    return errors, new_lookml


def lint_measure_names(lookml):
    """
    Enforce measure naming conventions.

    See: https://classy.looker.com/projects/classy/documents/dev.2.contributing.md
    """
    # Dictionary where keys are measure types, and values are dictionaries.
    # The "value dictionaries" have keys 'starts' and 'ends',
    # and their values are what those measure names need to start/end with.
    measure_naming_rules = {
        'count': {'ends': 'count'},
        'count_distinct': {'starts': 'unique', 'ends': 'count'},
        'sum': {'starts': 'total'},
        'average': {'starts': 'average'},
        'median': {'starts': 'median'},
        'max': {'starts': 'max'},
        'min': {'starts': 'min'},
    }

    errors, new_lookml = [], lookml

    for _, match in yield_object_matches(new_lookml, object_types=("measure",)):
        measure_name = re_search(r" (\w+) \{", match)
        if measure_name is None:
            errors.append("Measure name has unexpected format: {match}".format(match=match))

        if re_search(r" \w+ \{\s*#\s*noqa", match):
            # Measure name should not be linted
            continue

        for measure_type, rules in measure_naming_rules.items():
            if re_search("type: {measure_type}\n".format(measure_type=measure_type), match):
                if 'starts' in rules and not measure_name.startswith(rules['starts']):
                    errors.append("Measure {measure} should start with '{start}'.".format(
                        measure=measure_name, start=rules['starts']))
                if 'ends' in rules and not measure_name.endswith(rules['ends']):
                    errors.append("Measure {measure} should end with '{end}'.".format(
                        measure=measure_name, end=rules['ends']))

    return errors, new_lookml


def lint_primary_key(lookml):
    """Enforce no group label, label='ID' (or no label), for primary keys."""
    errors, new_lookml = [], lookml

    for _, matched_lookml in yield_object_matches(new_lookml, object_types=("dimension",)):
        if "primary_key" not in matched_lookml:
            continue

        bad_properties = ["group_label"]
        bad_properties += ["label"] if get_object_name(matched_lookml) == "id" else []

        for prop_name in bad_properties:
            if prop_name in matched_lookml:
                # Remove group label, label
                replacement = re.sub(
                    r"\n\s*{prop_name}:.*\n".format(prop_name=prop_name),
                    "\n", matched_lookml)

                # Swap out match with replacement, and report on it.
                new_lookml = new_lookml.replace(matched_lookml, replacement)
                errors.append("{matched_lookml} => {replacement}".format(
                    matched_lookml=matched_lookml, replacement=replacement))

    return errors, new_lookml


def get_property_names(lookml):
    return re.findall(r"\n\s*([a-zA-Z_0-9]+):", lookml)


def lint_object_property_ordering(lookml, desired_ordering, object_types=None):
    """Enforce property order in objects."""
    errors, new_lookml = [], lookml

    for object_type, matched_lookml in yield_object_matches(new_lookml, object_types=object_types):
        object_name = get_object_name(matched_lookml)
        prop_names = get_property_names(matched_lookml)

        # Error on any unknown property.
        unknown_prop_names = [p for p in prop_names if p not in desired_ordering]
        for prop_name in unknown_prop_names:
            errors.append("Unknown / unordered property '{key}' in {object_type} '{object_name}'.".format(
                key=prop_name, object_type=object_type, object_name=object_name))
        known_prop_names = [p for p in prop_names if p in desired_ordering]

        # Error on misordered properties.
        order_indices = [desired_ordering.index(p) for p in known_prop_names]
        for idx, d in enumerate(np.diff(order_indices)):
            if d < 0:
                errors.append(
                    "Key '{key1}' should appear after '{key2}', in {object_type} '{object_name}'.".format(
                        key1=known_prop_names[idx], key2=known_prop_names[idx + 1],
                        object_type=object_type, object_name=object_name))

    return errors, new_lookml


def lint_dimension_property_ordering(lookml):
    """Enforce property order in dimensions, dimension_groups, and measures."""
    return lint_object_property_ordering(
        lookml=lookml,
        object_types=("dimension", "dimension_group", "measure",),
        desired_ordering=(
            "primary_key", "group_label", "hidden", "label", "type",
            "timeframes", "intervals", "convert_tz",  # core definition
            "filters", "field", "value",  # measure filters
            "tiers", "style",  # tier dimensions
            "description", "sql", "sql_start", "sql_end",  # metadata, DB reference
            "can_filter", "order_by_field", "value_format", "value_format_name", "map_layer_name", "html",  # display
            "drill_fields",  # interactions
            "alias",  # backwards compatibility
        )
    )


def lint_join_property_ordering(lookml):
    """Enforce property order in joins."""
    return lint_object_property_ordering(
        lookml=lookml,
        object_types=("join",),
        desired_ordering=(
            "from",
            "required_joins",
            "relationship",
            "sql_on",
            "foreign_key",
            "view_label",
            "fields",
        )
    )


def lint_trailing_whitespace(lookml):
    new_lookml = lookml
    errors = []

    for line in new_lookml.split('\n'):
        if not re.match(r'^.*\s$', line):
            continue
        replacement = re.sub(r'^(.*)\s+$', r'\1', line)
        errors.append('{line} => {replacement}'.format(line=line, replacement=replacement))
        new_lookml = new_lookml.replace(line, replacement)

    return errors, new_lookml


def lint_one_explore_per_explore_file(lookml):
    """Lint that explore files (.explore.lkml) should only define a single explore.

    Parameters:
    --------
    lookml : str
        some LookML, assumed to be the contents of a file defining an explore
    """
    errors = []
    explore_object_matches = list(yield_object_matches(lookml, object_types=('explore',)))
    if len(explore_object_matches) != 1:
        errors.append('Only one explore should be defined in an explore file.')
    return errors, lookml


def lint_explore_includes_at_most_one_explore(lookml):
    """Lint that explore files (.explore.lkml) include at most one other explore file.
    This included file should be the file it's exploring.

    Parameters:
    --------
    lookml : str
        some LookML, assumed to be the contents of a file defining an explore
    """
    errors = []
    included_explore_lines = [line for line in lookml.split('\n') if 'include:' in line and '.explore' in line]
    if len(included_explore_lines) > 1:
        errors.append('An explore file should only include at most one other explore file.')
    return errors, lookml


def contains_property(lookml, prop_tag):
    for expr in (r"\s+{prop_tag}:", "^{prop_tag}:"):
        if any(re.findall(expr.format(prop_tag=prop_tag), lookml)):
            return True
    return False


def get_view_names(lookml):
    return [get_object_name(view) for _, view in yield_object_matches(lookml, object_types=("view",))]


def lint_views(lookml):
    """Lint views within lookml"""
    errors, new_lookml = [], lookml

    # Some core state variables
    view_names = get_view_names(lookml=new_lookml)
    has_private_view = any([view_name.startswith('_') for view_name in view_names])
    is_raw_view_file = is_raw_view(view_names[0])
    is_hidden_view_file = is_hidden_view(view_names[0])

    new_errors, new_lookml = lint_dimension_property_ordering(new_lookml)
    errors += new_errors

    if "intervals:" in new_lookml:
        new_errors, new_lookml = lint_intervals(new_lookml)
        errors += new_errors

    if "timeframes:" in new_lookml:
        new_errors, new_lookml = lint_timeframes(new_lookml)
        errors += new_errors

    if is_raw_view_file and not is_hidden_view_file:
        # We only lint raw views that are not hidden (and therefore need a good UI)
        if "dimension:" in new_lookml or "dimension_group:" in new_lookml:
            new_errors, new_lookml = lint_dimension_group_labels_in_raw_views(new_lookml)
            errors += new_errors

            new_errors, new_lookml = lint_dimension_group_section_comments(new_lookml)
            errors += new_errors

    if 'extends:' not in new_lookml and not has_private_view:
        # If this view extends another view, count might have been defined already
        # Private views do not need to define measures
        new_errors, new_lookml = lint_measure_has_count(new_lookml)
        errors += new_errors

    if "measure:" in new_lookml:
        new_errors, new_lookml = lint_measure_group_labels(new_lookml)
        errors += new_errors

        new_errors, new_lookml = lint_measure_group_section_comments(new_lookml)
        errors += new_errors

        new_errors, new_lookml = lint_measure_names(new_lookml)
        errors += new_errors

    if "primary_key:" in new_lookml:
        new_errors, new_lookml = lint_primary_key(new_lookml)
        errors += new_errors

    return errors, new_lookml


def lint_explores(lookml):
    """Lint explores within lookml"""
    errors, new_lookml = [], lookml

    # Some core state variables
    defines_joins = contains_property(new_lookml, prop_tag="join")

    if not is_model_file_lookml(lookml) and not is_dashboard_file_lookml(lookml):
        # This is an explore file
        new_errors, new_lookml = lint_one_explore_per_explore_file(new_lookml)
        errors += new_errors

        new_errors, new_lookml = lint_explore_includes_at_most_one_explore(new_lookml)
        errors += new_errors

    if defines_joins:
        new_errors, new_lookml = lint_join_property_ordering(new_lookml)
        errors += new_errors

        new_errors, new_lookml = lint_join_name_order(new_lookml)
        errors += new_errors

        new_errors, new_lookml = lint_join_sql_order(new_lookml)
        errors += new_errors

        if 'required_joins:' in new_lookml:
            new_errors, new_lookml = lint_required_joins_in_joins(new_lookml)
            errors += new_errors

            new_errors, new_lookml = lint_required_joins_max_of_one(new_lookml)
            errors += new_errors

    if "primary_key:" in new_lookml:
        new_errors, new_lookml = lint_primary_key(new_lookml)

    return errors, new_lookml


def lint_lookml(text, full_file=False):
    # Some core state variables
    is_dashboard_file = is_dashboard_file_lookml(text)
    defines_explores = contains_property(text, prop_tag="explore")
    defines_views = contains_property(text, prop_tag="view")

    # Prep for massaging text / recording errors
    errors, new_lookml = [], text

    if defines_explores:
        new_errors, new_lookml = lint_explores(new_lookml)
        errors += new_errors

    if defines_views:
        new_errors, new_lookml = lint_views(new_lookml)
        errors += new_errors

    if 'sql:' in new_lookml and ';;' in new_lookml:
        new_errors, new_lookml = lint_lookml_sql(new_lookml)
        errors += new_errors

    if is_dashboard_file:
        new_errors, new_lookml = lint_query_timezones(new_lookml)
        errors += new_errors

    new_errors, new_lookml = lint_trailing_whitespace(new_lookml)
    errors += new_errors

    return errors, new_lookml


if __name__ == '__main__':
    import sys

    from classypy.linters.base import Linter, LinterParser

    parser = LinterParser(description='Find and fix LookML linting errors.')
    args = vars(parser.parse_args())

    linter1 = Linter(linter_func=lint_lookml, ext='.lkml')
    linter2 = Linter(linter_func=lint_lookml, ext='.lookml')

    sys.exit(linter1.lint_path(**args) + linter2.lint_path(**args))

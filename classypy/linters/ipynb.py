import nbformat
import nbstripout


def lint_ipynb(text, full_file=False):
    """Test whether ipython notebook output has been stripped."""
    try:
        nb_stripped = nbstripout.strip_output(nbformat.reads(text, as_version=nbformat.NO_CONVERT), [], [])
        text_stripped = nbformat.writes(nb_stripped, version=nbformat.NO_CONVERT) + '\n'
    except Exception as ex:  # noqa
        # With an error, we can't correct text
        return [str(ex)], None

    if text == text_stripped:
        return [], text

    return ["Output was not stripped in iPython Notebook."], text_stripped


if __name__ == '__main__':
    import sys

    from classypy.linters.base import Linter, LinterParser

    parser = LinterParser(description='Find non-output-stripped ipython notebooks.')
    args = vars(parser.parse_args())

    linter = Linter(linter_func=lint_ipynb, ext='.ipynb')
    sys.exit(linter.lint_path(**args))

LINTING_PATH="${1:-.}"
EXCLUDES="${2:-venv,venv2,venv3}"
RUN_PACKAGE_CHECKS="$3"  # any arg triggers this
EXCLUDE_FLAGS="`echo $EXCLUDES | sed -e 's/,/ --exclude-dir=/g' && echo $EXCLUDES | sed -e 's/,/ --exclude=/g'`"

# In some checks, globally exclude classypy/io.
EXCLUDE_FLAGS_WITH_CLASSYPY_IO="`echo io,external,$EXCLUDES | sed -e 's/,/ --exclude-dir=/g' && echo ,datasets.py,$EXCLUDES | sed -e 's/,/ --exclude=/g'`"


# Avoid apply + reduce
ESCAPED_REGULAR_EXPRESSION="`grep -r \"re.[a-z]*[(][\'\\\"]*[^\'\\\"][\\]\" --include=*.py --exclude-dir=$EXCLUDE_FLAGS $LINTING_PATH | grep -v '# noqa'`"
if [ "$ESCAPED_REGULAR_EXPRESSION" != "" ]; then
    echo "Regular expressions should be raw strings (e.g. r'abc')"
    echo "${ESCAPED_REGULAR_EXPRESSION%x}"
    exit 1
fi

# Never use dataframe.keys()
KEYS_CHECK="`grep -r df\\\\.keys --include=*.py --exclude-dir=$EXCLUDE_FLAGS $LINTING_PATH`"
if [ "$KEYS_CHECK" != "" ]; then
    echo "You used df.keys() instead of df.columns on lines:"
    echo "${KEYS_CHECK%x}"
    exit 1
fi

# Make sure verbose is not passed as boolean True (it's an int)
VERBOSE_TRUE="`grep -r 'verbose=True' --include=*.py --exclude-dir=$EXCLUDE_FLAGS $LINTING_PATH`"
if [ "$VERBOSE_TRUE" != "" ]; then
    echo "You used 'verbose=True' instead of 'verbose=1' on lines:"
    echo "${VERBOSE_TRUE%x}"
    exit 1
fi

# Make sure verbose is not passed as boolean False (it's an int)
VERBOSE_FALSE="`grep -r 'verbose=False' --include=*.py --exclude-dir=$EXCLUDE_FLAGS $LINTING_PATH`"
if [ "$VERBOSE_FALSE" != "" ]; then
    echo "You used 'verbose=False' instead of 'verbose=0' on lines:"
    echo "${VERBOSE_FALSE%x}"
    exit 1
fi

# Make sure verbose is not checked as boolean (it's an int)
VERBOSE_CHECK="`grep -r 'if verbose:' --include=*.py --exclude-dir=$EXCLUDE_FLAGS $LINTING_PATH`"
if [ "$VERBOSE_CHECK" != "" ]; then
    echo "You used 'if verbose:' instead of 'if verbose > 0:' on lines:"
    echo "${VERBOSE_CHECK%x}"
    exit 1
fi

# Make sure echo is not turned off, use logger level
ECHO_FALSE_CHECK="`grep -r 'echo=False' --include=*.py --exclude-dir=$EXCLUDE_FLAGS $LINTING_PATH`"
if [ "$ECHO_FALSE_CHECK" != "" ]; then
    echo "You used 'echo=False' instead of 'echo=logger.logsLevel(logger.DEBUG)' on lines:"
    echo "${ECHO_FALSE_CHECK%x}"
    exit 1
fi

# Make sure echo is not turned off, use logger level
ECHO_TRUE_CHECK="`grep -r 'echo=True' --include=*.py --exclude-dir=$EXCLUDE_FLAGS $LINTING_PATH`"
if [ "$ECHO_TRUE_CHECK" != "" ]; then
    echo "You used 'echo=True' instead of 'echo=logger.logsLevel(logger.INFO)' on lines:"
    echo "${ECHO_TRUE_CHECK%x}"
    exit 1
fi

# Check import ordering
SKIP_GLOBS=`echo $EXCLUDES | sed -e "s/,/ --skip-glob=/g"`
IMPORT_ORDERING_CHECK="`isort --quiet --diff --skip-glob=$SKIP_GLOBS --recursive $LINTING_PATH`"
if [ "$IMPORT_ORDERING_CHECK" != "" ]; then
    echo "Import checks failed in these files:"
    echo "${IMPORT_ORDERING_CHECK%x}"
    echo "Make sure unusual 3rd party imports are included in your tox.ini file."
    exit 1
fi

# Avoid bare exceptions
EXCEPTIONS_CHECK="`grep -r 'except Exception' --include=*.py --exclude-dir=$EXCLUDE_FLAGS $LINTING_PATH | grep -v '\# noqa'`"
if [ "$EXCEPTIONS_CHECK" != "" ]; then
    echo "You used 'except Exception'; specify errors or add '# noqa'"
    echo "${EXCEPTIONS_CHECK%x}"
    exit 1
fi

# Avoid print()
PRINT_CHECK_1="`grep -r '\s\sprint[\s\(]' --include=*.py --exclude-dir=$EXCLUDE_FLAGS_WITH_CLASSYPY_IO $LINTING_PATH | grep -v '\# noqa'`"
if [ "$PRINT_CHECK_1" != "" ]; then
    echo "You used 'print'; use our logger instead ('from classypy.devops import logger')"
    echo "${PRINT_CHECK_1%x}"
    exit 1
fi

# Avoid print_function
PRINT_CHECK_2="`grep -r 'print_function' --include=*.py --exclude-dir=$EXCLUDE_FLAGS_WITH_CLASSYPY_IO $LINTING_PATH | grep -v '\# noqa'`"
if [ "$PRINT_CHECK_2" != "" ]; then
    echo "You used 'print_function'; use our logger instead ('from classypy.devops import logger')"
    echo "${PRINT_CHECK_2%x}"
    exit 1
fi

# Force use of classypy parsers
ARGUMENT_PARSER_CHECK="`grep -r '\sArgumentParser[\(]' --include=*.py --exclude-dir=$EXCLUDE_FLAGS $LINTING_PATH | grep -v '\# noqa'`"
if [ "$ARGUMENT_PARSER_CHECK" != "" ]; then
    echo "You used 'ArgumentParser'; use our parsers instead ('from classypy.devops.parsers import [LoggingParser,ArgumentParserWithSecrets,CsvScriptArgumentParser]')"
    echo "${ARGUMENT_PARSER_CHECK%x}"
    exit 1
fi

# Avoid dataframe.as_matrix
AS_MATRIX_CHECK="`grep -r 'as_matrix' --include=*.py --exclude-dir=$EXCLUDE_FLAGS $LINTING_PATH`"
if [ "$AS_MATRIX_CHECK" != "" ]; then
    echo "pandas.DataFrame.as_matrix is deprecated; use DataFrame.values instead:"
    echo "${AS_MATRIX_CHECK%x}"
    exit 1
fi

# Avoid apply + reduce
REDUCE_PARAMETER_APPLY_CHECK="`grep -r '.apply[\(]' --include=*.py --exclude-dir=$EXCLUDE_FLAGS $LINTING_PATH | grep 'reduce='`"
if [ "$REDUCE_PARAMETER_APPLY_CHECK" != "" ]; then
    echo "The reduce parameter of .apply() is deprecated in favor of result_type='reduce':"
    echo "${REDUCE_PARAMETER_APPLY_CHECK%x}"
    exit 1
fi


if [[ "$RUN_PACKAGE_CHECKS" != "" ]]; then
    # Avoid add_to_path() (and avoid "def add_to_path())
    ADD_TO_PATH_CHECK="`grep -r '^add_to_path[(]' --include=*.py --exclude-dir=$EXCLUDE_FLAGS $LINTING_PATH`"
    if [ "$ADD_TO_PATH_CHECK" != "" ]; then
        echo "Use absolute package imports within packages; no need for add_to_path"
        echo "${ADD_TO_PATH_CHECK%x}"
        exit 1
    fi

    # Avoid isort:stay; should be absolute imports with no add_to_path
    ISORT_STAY_CHECK="`grep -r '# isort:stay' --include=*.py --exclude-dir=$EXCLUDE_FLAGS $LINTING_PATH | grep import`"
    if [ "$ISORT_STAY_CHECK" != "" ]; then
        echo "No need for isort:stay within packages (using absolute imports)"
        echo "${ISORT_STAY_CHECK%x}"
        exit 1
    fi
fi

import glob
import os.path as op
from datetime import datetime, timedelta

import numpy as np

from classypy.compat import func_has_param
from classypy.devops import LoggingParser, logger
from classypy.text.utils import text_from_disk
from classypy.util.parsers import valid_datetime_as_utc


def get_files_list(path, exclusions=tuple(), since=None, ext=None):
    """Find all files in a given path (excluding exclusions)

    Parameters
    ----------
    path : string
        File path to get files from
    exclusions : iterable (optional)
        Iterable of prefixes to exclude from the file list
    since : datetime (optional)
        Datetime, before which we should exclude from the file list
    ext : string (optional)
        File extension that files must obey to be in the file list

    Returns
    -------
    list - all files in the path (and subdirs)
    """
    path = op.abspath(path)
    # Exclusions may be:
    # * relative paths with no '/' (e.g. 'my_dir'),
    # * relative paths with '/' (e.g. 'my_dir/my_subdir')
    # * absolute paths. (e.g. /my_root/my_dir/my_subdir)
    #
    # Because of this, we can't just join the exclusion with the current path.
    # Instead, we append the op.basename(), only if the op.dirname() matches
    # what's at the end of the path.
    cur_exclusions = [op.abspath(op.join(path, op.basename(ex))) for ex in exclusions if path.endswith(op.dirname(ex))]

    if any([path.startswith(ex) for ex in cur_exclusions]):
        # Check exclusions (directory or file)
        return []
    if op.isdir(path):
        # Recursive case: directory
        sub_paths = [p for p in glob.glob(op.join(path, '*'))
                     if p not in cur_exclusions]  # exclude on dirnames
        return [fil for sub_path in sub_paths
                for fil in get_files_list(path=sub_path, exclusions=exclusions, since=since, ext=ext)]
    if not op.isfile(path):
        # Not a directory, not a file... huh? SKIP
        return []
    if ext and not path.endswith(ext):
        # Exclude files with unrelated extensions.
        return []
    if since is not None and datetime.fromtimestamp(op.getmtime(path)) < since:
        # Exclude old files when we're looking for something more recent.
        return []

    return [path]


class Linter(object):
    def __init__(self, linter_func, ext=None):
        """
        Parameters
        ----------
        linter_func : function
            Function that does the linting.
        ext : string (optional)
            File extension to search for within path;
            None means no restriction. (Default: None)
        """
        self.linter_func = linter_func
        self.ext = ext

    def lint_file(self, file_path, fix=False, **kwargs):
        """
        Detect, report on, and potentially fix linting errors.

        Parameters
        ----------
        file_path : string
            Path of file to lint.
        fix : boolean (Optional)
            Whether to write updated text to disk (Default: False)
        **kwargs : dict
            Args to pass through to linter_func

        Returns
        -------
        integer : # of linting errors found (can be used for exit code)

        Side-effects
        ------------
        If fix=True, overwrites file (in-place) contents (only when content changed).
        """
        logger.info(file_path)
        old_text = text_from_disk(file_path, confidence_threshold=0.0)  # suppress warnings

        # Create an arg list, based on what args the linter_func accepts
        for arg_name, arg_val in dict(full_file=True, file_path=file_path).items():
            if func_has_param(self.linter_func, param_name=arg_name):
                kwargs[arg_name] = arg_val

        # Lint
        errors, new_text = self.linter_func(text=old_text, **kwargs)

        # Output details
        for error in errors:
            logger.info("\t{message}".format(message=error))

        # Fix on disk
        if fix and (old_text != new_text):
            with open(file_path, 'w') as fp:
                fp.write(new_text)

        return len(errors)

    def lint_path(self, path, exclusions=None,
                  since=None, fix=False, **kwargs):
        """
        Detect, report on, and potentially fix linting errors.

        Parameters
        ----------
        path : string
            Base path of files to lint.
        exclusions : list (optional)
            List of substrings for matching, and excluding, paths found under path.
            None means no exclusions. (Default: None)
        since : datetime
            Datetime of earliest file to include.
            None means no exclusion (Default: None)
        fix : boolean (Optional)
            Whether to write updated text to disk (Default: False)
        **kwargs : dict
            Args to pass through to linter_func

        Returns
        -------
        integer : sum of # of linting errors found (can be used for exit code)

        Side-effects
        ------------
        If fix=True, overwrites files (in-place) contents (only when content changed).
        """
        files = get_files_list(path=path, exclusions=exclusions or [], since=since, ext=self.ext)
        files = sorted(files)  # nicer to output a sorted list than seemingly unordered list
        error_counts = [self.lint_file(file_path=fil, fix=fix, **kwargs) for fil in files]
        return int(np.sum(error_counts))  # Return total sum of errors


class LinterParser(LoggingParser):
    """Command-line parser for linters."""
    def __init__(self, *args, **kwargs):
        """Add arguments."""
        super(LinterParser, self).__init__(*args, **kwargs)

        self.add_argument('path', help='path to file')
        self.add_argument('--fix', action='store_true', help='Fixes file, in place.')
        self.add_argument('--exclude', dest='exclusions', default=None,
                          help='File/folder exclusion list')
        self.add_argument('--since', default=None, type=valid_datetime_as_utc)
        self.add_argument('--max-age', default=None, type=int,
                          help="Skip unless less than some number of days since last file update")

    def parse_args(self):
        """Clean arguments."""
        args = super(LinterParser, self).parse_args()

        # Compute max age
        max_age = self.popattr(args, 'max_age')
        if max_age is not None:
            args.since = datetime.now() - timedelta(days=max_age)

        # Clean exclusions
        args.exclusions = args.exclusions.split(',') if args.exclusions else []

        return args

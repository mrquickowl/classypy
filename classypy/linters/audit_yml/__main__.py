if __name__ == "__main__":
    import sys

    from classypy.linters.audit_yml import lint_audit_yaml
    from classypy.linters.base import Linter, LinterParser

    parser = LinterParser(
        description='Find Data Intelligence Audit YAML linting errors.')
    args = vars(parser.parse_args())

    linter1 = Linter(linter_func=lint_audit_yaml, ext='.yml')
    linter2 = Linter(linter_func=lint_audit_yaml, ext='.yaml')

    sys.exit(linter1.lint_path(**args) + linter2.lint_path(**args))

import os.path as op

from classypy.linters.yaml_base import lint_yaml
from classypy.util.dirs import this_files_dir


def lint_audit_yaml(text, file_path):
    """Lint audit yml files main.

    Parameters
    ----------
    text: string
        audit yaml text to lint
    file_path: string
        file path where text string came from

    Returns
    -------
    errors: string
        list of linting error strings
    updated_text: string
        text with any suggested updates, per linting failures
    """
    return lint_yaml(
        text=text,
        file_path=file_path,
        schema_file=op.join(this_files_dir(), 'audit_schema.yml'))

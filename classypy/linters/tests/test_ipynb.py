import os.path as op

from classypy.linters import ipynb
from classypy.text import text_from_disk
from classypy.util.dirs import this_files_dir


def test_ipynb_output():
    good_notebook_text = text_from_disk(op.join(this_files_dir(), 'notebooks', 'stripped_notebook.json'))
    errors, fixed = ipynb.lint_ipynb(good_notebook_text)
    assert not any(errors)

    bad_notebook_text = text_from_disk(op.join(this_files_dir(), 'notebooks', 'unstripped_notebook.json'))
    errors, fixed = ipynb.lint_ipynb(bad_notebook_text)
    assert len(errors) == 1
    assert fixed != bad_notebook_text
    assert fixed == good_notebook_text

"""Tests for the data intelligence audit YAML validator."""
import os
import os.path as op
import re
import tempfile

from ruamel import yaml

from classypy.linters.audit_yml import lint_audit_yaml
from classypy.text import yaml_from_disk, yaml_to_disk
from classypy.util.dirs import this_files_dir


def lint(obj, **kwargs):
    _, yaml_path = tempfile.mkstemp()
    # file must be named 'test_audit_yml_base.yml'
    yaml_path = op.join(op.dirname(yaml_path), 'test_audit_yml_base.yml')
    yaml_to_disk(obj, yaml_path)
    try:
        return lint_audit_yaml(
            yaml.dump(obj),
            file_path=yaml_path,
            **kwargs)[0]
    finally:
        os.remove(yaml_path)


def get_base_yaml():
    return yaml_from_disk(op.join(this_files_dir(), 'test_audit_yml_base.yml'))


def assert_contains_error(errors, error_regex):
    assert any([re.search(error_regex, err) for err in errors]), "{regex} not contained in {errors}".format(
        regex=error_regex, errors=str(errors))


def test_working_parse():
    obj = get_base_yaml()
    errors = lint(obj)
    assert not any(errors), "No errors on base parse"


def test_unknown_field():
    obj = get_base_yaml()
    obj.update(dict(unknown_field=True))

    errors = lint(obj)
    assert_contains_error(errors, r"unknown_field: unknown field")


def test_missing_screenshot():
    obj = get_base_yaml()
    obj["steps"][0]["screen-shots"].append("does-not-exist.png")

    errors = lint(obj)
    assert_contains_error(errors, r"file does not exist: does-not-exist.png")
    assert_contains_error(errors, r"steps.0.screen-shots.1: must be of file type")


def test_bad_url():
    obj = get_base_yaml()
    obj["url"] = "abc"

    errors = lint(obj)
    assert_contains_error(errors, r"Malformed url: abc")
    assert_contains_error(errors, r"url: must be of url type")

import re

from classypy.linters.lookml import (_get_explore_view_name, contains_property, lint_join_property_ordering,
                                     lint_join_sql_order, lint_lookml_sql, lint_required_joins_max_of_one,
                                     yield_object_matches)

LOOKML_EXPLORE_WITH_JOINS = """

explore: pay_recurring_history {
  label: "Recurring History (Pay)"
  view_label: "Recurring History (Pay)"
  view_name: the_name  # used for test get_explore_name()

  join: mysql_organizations {
    from: mysql_organizations
    required_joins: [pay_recurring_profiles]
    relationship: many_to_one
    sql_on: ${pay_recurring_profiles.organization_id} = ${mysql_organizations.id} ;;
    view_label: "Organizations (APIv2)"
  }

  join: mysql_recurring_donation_plans {
    from: mysql_recurring_donation_plans
    required_joins: [pay_recurring_profiles]
    relationship: many_to_one
    sql_on: ${pay_recurring_profiles.id} = ${mysql_recurring_donation_plans.pay_recurring_id} ;;
    view_label: "Recurring Donation Plans (APIv2)"
  }
}
"""

LOOKML_EXPLORE_WITH_REQUIRED_JOIN_TESTS = """
explore: pay_recurring_history {
  join: dummy1 {
    required_joins: [a,b,c]  # used for lint_required_joins_max_of_one
  }

  join: dummy2 {
    # used for lint_required_joins_max_of_one to validate use of noqa
    required_joins: [a,b,c]  # noqa
  }
}
"""

LOOKML_EXPLORE_WITH_JOIN_ORDERING_TESTS = """
explore: explore_name {
  join: dummy1 {
    # Error: doesn't contain explore_name
    sql_on: ${some_other_name.id} = ${dummy1.foo} ;;
  }

  join: dummy2 {
    # Error: doesn't contain dummy2
    sql_on: ${explore_name.id} = ${dummy1.foo} ;;
  }

  join: dummy3 {
    # correct sql_on order wout/ required_joins; used for lint_join_sql_order
    sql_on: ${explore_name.bar} = ${dummy3.foo} ;;
  }

  join: dummy4 {
    # incorrect sql_on order wout/ required_joins; used for lint_join_sql_order
    sql_on: ${dummy4.foo} = ${explore_name.bar} ;;
  }
"""


LOOKML_VIEW_WITH_MISSING_DOUBLE_SEMI = """
view: mysql_organizations {
  sql_table_name: mysql.charity ;;

  dimension: id {
    sql: ${TABLE}.charity_id
  }


  ## Foreign Keys

  dimension: subscription_id {
    group_label: "Foreign Keys"
    hidden: yes
    type: string
    description: "Not surfaced in APIv2, ClassyApp only."
    sql: ${TABLE}.subscription_id ;;
  }
"""


def test_yield_object_matches_on_explore_with_joins():

    explore_matches = list(yield_object_matches(LOOKML_EXPLORE_WITH_JOINS, object_types=("explore",)))
    assert len(explore_matches) == 1

    _, explore_lookml = explore_matches[0]
    join_matches = list(yield_object_matches(explore_lookml, object_types=("join",)))
    assert len(join_matches) == 2


def test_missing_double_semis_in_view():
    errors, _ = lint_lookml_sql(LOOKML_VIEW_WITH_MISSING_DOUBLE_SEMI)
    assert len(errors) == 1, "That an error for view with missing semicolons in sql: was caught"
    assert "Missing ;; detected" in errors[0], (
        "That the correct error message for a view with missing semicolons in sql: was emitted")


def test_lint_join_property_ordering():
    errors, _ = lint_join_property_ordering(LOOKML_EXPLORE_WITH_JOINS)
    assert not any(errors), "That a join with correct property ordering passes"

    errors, _ = lint_join_property_ordering(
        LOOKML_EXPLORE_WITH_JOINS
        # reverse ordering of sql_on and required_joins
        .replace("sql_on", "sql_on_tmp")
        .replace("required_joins", "sql_on")
        .replace("sql_on_tmp", "required_joins"))
    assert any(errors), "That a join with incorrect property ordering fails"


def test_get_explore_view_name():
    # with view_name
    explore_lookml_with_view_name = LOOKML_EXPLORE_WITH_JOINS
    explore_lookml_sans_view_name = re.sub(r"view_name:[^\n]*\n", "", LOOKML_EXPLORE_WITH_JOINS)

    for _, explore_lookml in yield_object_matches(explore_lookml_with_view_name, ("explore",)):
        assert _get_explore_view_name(explore_lookml) == "the_name", (
            "Match explore name (via view_name)")

    # without view_name
    for _, explore_lookml in yield_object_matches(explore_lookml_sans_view_name, ("explore",)):
        assert _get_explore_view_name(explore_lookml) == "pay_recurring_history", (
            "Match explore name (via explore object name)")


def test_lint_join_sql_order():
    errors, new_explore_lookml = lint_join_sql_order(LOOKML_EXPLORE_WITH_JOINS)
    assert len(errors) == 0, "When it's good, it's good!"

    errors, new_explore_lookml = lint_join_sql_order(LOOKML_EXPLORE_WITH_JOIN_ORDERING_TESTS)
    # Check the individual errors
    EXPLORE_NAME_NOT_FOUND_ERROR = (
        "Explore name 'explore_name' should be found inside sql_on "
        "'${some_other_name.id} = ${dummy1.foo}'.")
    assert len([err for err in errors if err == EXPLORE_NAME_NOT_FOUND_ERROR]) == 1
    JOINED_VIEW_NAME_NOT_FOUND_ERROR = (
        "Joined view name 'dummy2' should be found inside sql_on "
        "'${explore_name.id} = ${dummy1.foo}'.")
    assert len([err for err in errors if err == JOINED_VIEW_NAME_NOT_FOUND_ERROR]) == 1
    BAD_ORDER_ERROR = (
        "Join source name 'explore_name' must appear first in joined view sql_on "
        "'${dummy4.foo} = ${explore_name.bar}'.")
    assert len([err for err in errors if err == BAD_ORDER_ERROR]) == 1
    #
    assert len(errors) == 3, "Catch only expected errors"

    # Check that the lookml has been updated
    assert new_explore_lookml != LOOKML_EXPLORE_WITH_JOIN_ORDERING_TESTS, (
        "Ensure the linter auto-updated the explore lookml")
    assert "${explore_name.bar} = ${dummy4.foo}" in new_explore_lookml, (
        "Ensure the linter auto-updated the explore lookml")


def test_lint_required_joins_max_of_one():
    errors, new_explore_lookml = lint_required_joins_max_of_one(LOOKML_EXPLORE_WITH_JOINS)
    assert len(errors) == 0, "When it's good, it's good!"

    errors, new_explore_lookml = lint_required_joins_max_of_one(LOOKML_EXPLORE_WITH_REQUIRED_JOIN_TESTS)
    assert len(errors) == 1, "Only hit an error with the non-qa multi-list required_joins"
    assert "required_joins must have one and only one view listed" in errors[0]


def test_contains_property():
    assert contains_property("view: {blah}", "view"), (
        "beginning-of-file view: should contain property 'view'")
    assert contains_property("view: {\n  my_prop: 'my_val'\n}", "my_prop"), (
        "view should contain property 'my_prop'")

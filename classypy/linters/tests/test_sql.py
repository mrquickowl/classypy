"""
"""
import pytest

from classypy.linters.sql import aggregate_errors, lint_sql


def lint(sql, **kwargs):
    return lint_sql(sql, **kwargs)[1]


def test_comments():
    sql = "/* This is a test of the from keyword. */"
    assert sql == lint(sql)

    sql = """  /* This is a test of the from keyword. */ """
    assert sql == lint(sql)

    sql = """/*
    This is a test of the from keyword.
    This is a test of the from keyword.
    This is a test of the from keyword.
    This is a test of the from keyword.
*/
"""
    assert sql == lint(sql)

    sql = "--bad comment"
    assert "-- bad comment" == lint(sql)


def test_keywords():
    sql = "select * from table_name"
    assert "SELECT * FROM table_name" == lint(sql)

    sql = """/* test inside from on and */
select
  column_name  as col,   -- comment from on and
from table_name tn
join table_two tt
  on tt.id  = tn.id
where tn.col in (1, 2, 3)
  and  max(least(1, 2))  =  3
group by  1
"""

    assert lint(sql, full_file=True) == """/* test inside from on and */
SELECT
  column_name AS col,  -- comment from on and
FROM table_name tn
JOIN table_two tt
  ON tt.id = tn.id
WHERE tn.col IN (1, 2, 3)
  AND MAX(LEAST(1, 2)) = 3
GROUP BY 1
"""


def test_case_else():
    assert "CASE\n\nEND" == lint("CASE\n  ELSE NULL\nEND")

    assert "CASE WHEN foo THEN 1 END" == lint("CASE WHEN foo THEN 1 ELSE NULL END")


def test_full_file():
    sql = """/* test inside from on and */
SELECT
  column_name AS col,  -- comment from on and
FROM table_name tn
JOIN table_two tt
  ON tt.id = tn.id
WHERE tn.col IN (1, 2, 3)
  AND MAX(LEAST(1, 2)) = 3
GROUP BY 1
"""

    assert lint(sql, full_file=True) == sql

    assert lint(" " + sql, full_file=True) != sql, "Docstring must start with first character"
    assert lint(" " + sql, full_file=True)[0:2] == '/*', "Docstring must start with first character"

    assert lint(sql[:-1], full_file=True) == sql, "File must end with newline"


def test_aggregate_errors_aggregation():
    # Test how aggregate_errors aggregates
    for input_errors in (None, [], ['err']):  # No input errors, empty input errors, some input errors
        kwargs = {}
        if input_errors is not None:
            kwargs['errors'] = input_errors
            expected_errors = input_errors
        else:
            # Skip input
            expected_errors = []

        # Test 1: function returns a string.
        return_errors, _ = aggregate_errors(fn=lambda line: line, line='line', **kwargs)
        assert return_errors == expected_errors + ['line']

        # Test 2: function returns None
        return_errors, _ = aggregate_errors(fn=lambda line: line, line=None, **kwargs)
        assert return_errors == expected_errors

        # Test 3: function returns a tuple (None, line)
        return_errors, _ = aggregate_errors(fn=lambda line: (None, line), line='line', **kwargs)
        assert return_errors == expected_errors

        # Test 4: function returns a tuple ([]], line)
        return_errors, _ = aggregate_errors(fn=lambda line: ([], line), line='line', **kwargs)
        assert return_errors == expected_errors

        # Test 5: function returns a tuple ([error]], line)
        return_errors, _ = aggregate_errors(fn=lambda line: (['error'], line), line='line', **kwargs)
        assert return_errors == expected_errors + ['error']

        # Test 6: function returns a tuple ([error1. error2]], line)
        return_errors, _ = aggregate_errors(fn=lambda line: (['error1', 'error2'], line), line='line', **kwargs)
        assert return_errors == expected_errors + ['error1', 'error2']


def test_fix_dispreferred_functions():
    assert lint_sql("EXTRACT(MONTH FROM my_column)")[1] == "DATE_PART(MONTH, my_column)"


def test_fix_double_newlines():
    assert lint_sql("a\n\n\nb")[1] == "a\n\nb"  # strip double newline
    assert lint_sql("\n\nb")[1] == "b"  # strip leading newlines
    assert lint_sql("a\n\n")[1] == "a\n"  # strip trailing double-newline


def test_fix_indenting():
    assert lint_sql("SELECT\n*")[1] == "SELECT\n  *"


def test_fix_select_w_parens():
    assert lint_sql("  (select")[1] == "  (SELECT"


def test_fix_subquery_docstrings():
    assert lint_sql("WITH abc AS (\n  abc\n)")[1] == "WITH abc AS (\n  -- Please add a docstring\n  abc\n)"


def test_boolean_equality():
    assert lint_sql("SELECT\n  a = TRUE")[1] == "SELECT\n  a"
    assert lint_sql("SELECT\n  a IS TRUE")[1] == "SELECT\n  a"
    assert lint_sql("SELECT\n  a != FALSE")[1] == "SELECT\n  a"
    assert lint_sql("SELECT\n  a IS NOT FALSE")[1] == "SELECT\n  a"

    assert len(lint_sql("SELECT\n  a = FALSE")[0]) == 1  # linter error
    assert lint_sql("SELECT\n  a = FALSE")[1] == "SELECT\n  a = FALSE"  # but can't be linted
    assert len(lint_sql("SELECT\n  a IS FALSE")[0]) == 1  # linter error
    assert lint_sql("SELECT\n  a IS FALSE")[1] == "SELECT\n  a IS FALSE"  # but can't be linted
    assert len(lint_sql("SELECT\n  a != TRUE")[0]) == 1  # linter error
    assert lint_sql("SELECT\n  a != TRUE")[1] == "SELECT\n  a != TRUE"  # but can't be linted
    assert len(lint_sql("SELECT\n  a IS NOT TRUE")[0]) == 1  # linter error
    assert lint_sql("SELECT\n  a IS NOT TRUE")[1] == "SELECT\n  a IS NOT TRUE"  # but can't be linted


@pytest.mark.skip
def test_join_order():
    good_query = """SELECT
  *
FROM abc
JOIN foo f
  ON f.a = abc.c
"""
    assert lint_sql(good_query)[1] == good_query

    # Reversible
    rev_query = """SELECT
  *
FROM abc
JOIN foo f
  ON abc.c = f.a
"""
    assert lint_sql(rev_query)[1] == good_query

    # Parens cancel detection
    rev_query = """SELECT
  *
FROM abc
JOIN foo f
  ON (abc.c = f.a)
"""
    assert lint_sql(rev_query)[1] == rev_query

    # Comment cancel detection
    rev_query = """SELECT
  *
FROM abc
JOIN foo f
  ON abc.c = f.a  -- boo
"""
    assert lint_sql(rev_query)[1] == rev_query


def test_date_function_underscores():
    """That date function underscores are normalized (no underscore)"""
    bad_query = """SELECT
  DATE_PART(foo)
FROM bar
"""
    good_query = bad_query.replace("DATE_PART", "DATEPART")
    assert lint_sql(bad_query)[1] == good_query
    assert lint_sql(good_query)[1] == good_query

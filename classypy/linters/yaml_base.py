"""functions to support data intelligence audit YAML validation."""
import os
import os.path as op
import re
import warnings

import six
from cerberus import Validator
from ruamel import yaml

from classypy.text import yaml_from_disk

# No good examples using new TypeDefinitions and getting granular output,
# so, will use the old cerberus system
warnings.filterwarnings("ignore", "Methods for type testing are deprecated", DeprecationWarning)


def translate_validator_errors_to_flattened_list(errors, prefix=None):
    """Take the output of a cerberus validator errors, and flattens to a list of errors."""
    def _compute_prefix(prefix, field):
        """Compute dot-notation concatenation between a prefix (could be None) and field."""
        return ".".join([str(val) for val in (prefix, field) if val is not None])

    output_errors = []
    for field, field_errors in errors.items():
        if isinstance(field_errors, dict):
            # Dictionary: can be nested, so unravel recursively
            field_errors = translate_validator_errors_to_flattened_list(
                errors=field_errors,
                prefix=_compute_prefix(prefix=prefix, field=field))
        elif isinstance(field_errors[0], dict):
            # List of dictionaries: can be nested, so unravel recursively
            field_errors = [err for lst in field_errors
                            for err in translate_validator_errors_to_flattened_list(
                                errors=lst,
                                prefix=_compute_prefix(prefix=prefix, field=field))
                            ]
        else:
            # Base case: List of strings(errors)
            field_errors = ["{prefix}: {err}".format(prefix=_compute_prefix(prefix=prefix, field=field), err=err)
                            for err in field_errors]
        output_errors += field_errors
    return output_errors


class ClassyYAMLValidator(Validator):
    """Validate with types url and file."""

    def __init__(self, *args, **kwargs):
        """
        Initialize validator with working directory.

        Parameters
        ----------
        path : str, optional
            working_dir of object being validated
        """
        assert 'schema' in kwargs or 'schema_file' in kwargs
        self.schema_data = kwargs.get('schema') or yaml_from_disk(kwargs.get('schema_file'))

        return super(ClassyYAMLValidator, self).__init__(*args, **kwargs)

    def validate(self, document, update=False, normalize=True, working_dir=None):
        """Validates, changing the working directory to allow relative file paths to validate."""
        # Set the working directory before validation
        prev_working_dir = os.getcwd()
        if working_dir:
            os.chdir(working_dir)

        try:
            super(ClassyYAMLValidator, self).validate(
                document=document,
                schema=self.schema_data,
                update=update,
                normalize=normalize)

        finally:
            # Reset the working directory post-validation
            os.chdir(prev_working_dir)

        return translate_validator_errors_to_flattened_list(self.errors)

    def _get_current_node_name(self):
        """During a parse, gets the name of the current node being validated."""
        return ".".join([str(node) for node in self.document_path])

    def _validate_type_url(self, value):
        """Custom validation that a string is in fully-qualified URL format."""
        if not isinstance(value, six.string_types):
            # Will already add appropriate error, just exit
            return False
        if not re.match("^http.*", value):
            self._error(
                self._get_current_node_name(),
                "Malformed url: {value}".format(value=value))
            return False
        return True

    def _validate_type_file(self, value):
        """Custom validation that a string denotes a file that exists."""
        if not isinstance(value, six.string_types):
            # Will already add appropriate error, just exit
            return False
        if not op.exists(value):
            self._error(
                self._get_current_node_name(),
                "file does not exist: {value}".format(value=value))
            return False
        return True


def lint_yaml(text, file_path, schema_file):
    """Lint audit yml files main."""
    # Prep for massaging text / recording errors
    document = yaml.safe_load(text)

    v = ClassyYAMLValidator(schema_file=schema_file)
    errors = v.validate(document, working_dir=op.dirname(file_path))
    return errors, text

"""
Standardized logging for all Data Insights applications.

See https://docs.python.org/2/howto/logging.html for usage.

Basic example:
>>> from classypy.devops import logging
>>> try:
>>>     logging.info("Downloading data ...")
>>> except ValueError as ve:
>>>     logging.warning("Something happened!! {ve}".format(ve=ve))
"""
import logging
import os
import re
import socket
import sys
import time

import bugsnag
import numpy as np
import psycopg2
from bugsnag.utils import fully_qualified_class_name as bugsnag_class_name

# Create logger, and make levels accessible.
logger = logging.getLogger("insights")
handler = logging.StreamHandler(sys.stdout)
formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')
formatter.formatTime = lambda record, datefmt=None: time.strftime(
    datefmt or '%m/%d %H:%M:%S', time.localtime(record.created))
handler.setFormatter(formatter)
logger.addHandler(handler)
del handler, formatter  # clear module var

# Allow code to reference logging levels, without having to import logging module
logger.level_names = {
    logging.CRITICAL: 'CRITICAL',
    logging.DEBUG: 'DEBUG',
    logging.ERROR: 'ERROR',
    logging.INFO: 'INFO',
    logging.WARNING: 'WARNING',
}
for level, level_name in logger.level_names.items():
    setattr(logger, level_name, level)
del level, level_name  # clear module vars

logger.setLevel(logging.INFO)  # default level

# Allow code to ask if logging would happen at a giving level.
# Temporarily function while we migrate code to pass loggers.
logger.logsLevel = lambda level: logger.level <= level
logger.verbosity = lambda: int((max(0, logger.INFO - logger.level + 10)) / 10)


def setup_bugsnag(api_key, env, project_root):
    # Assume that ETLs are run locally using the --env-file flag and from AWS with --env
    release_stage = env if env is not None else 'local'
    logger.debug("Bugsnag set to log for release_stage={release_stage}".format(release_stage=release_stage))
    bugsnag.configure(
        api_key=api_key,
        project_root=project_root,
        release_stage=release_stage,
        notify_release_stages=('prod', 'staging'))


def _get_grouping_hash(exception, metadata):
    if 'schema_name' in metadata and 'table_name' in metadata:
        return "{schema}:{table}:{exception}".format(
            schema=metadata['schema_name'],
            table=metadata['table_name'],
            exception=bugsnag_class_name(exception))
    return None  # Return None and fall back on default Bugsnag grouping


def notify_bugsnag(exception, metadata={}, grouping_hash=None, **kwargs):
    new_metadata = {
        'cmd_line': " ".join(sys.argv),
        'process_id': os.getpid(),
        'host_name': socket.gethostname(),
    }
    new_metadata.update(metadata)
    grouping_hash = grouping_hash or _get_grouping_hash(exception, new_metadata)

    bugsnag.notify(exception, meta_data=new_metadata, grouping_hash=grouping_hash, **kwargs)


def manipulate_data_for_better_jira_info(exception, source_func=None, traceback=None,
                                         qualifier=None, release_stage=None, title=None, description=None,
                                         max_title_length=128, normalize_title=True):
    """
    Manipulate exception data to tunnel error information through Bugsnag to JIRA.

    Output formats:
    * title: "{exception_name} ({qualifier}): {title}",
    * description: "In release stage {release_stage}': {description}"

    Parameters
    ----------
    exception : Exception
        Original exception
    source_func : function
        Function where the error occurred
    traceback : traceback object
        Traceback.
    qualifier : string (optional)
        secondary qualifier to exception class (Default: omitted)
    release_stage : string (optional)
        release stage (Default: pulled from bugsnag)
    title : string (optional)
        title of exception (Default: exception message)
    description : string (optional)
        main message of exception (Default: exception message)
    max_title_length : int (optional)
        length of title (Default: 128)
    normalize_title : bool (optional)
        whether to normalize our potentially churning title chars (e.g. numbers/dates) (Default: True)

    Returns
    -------
    Exception : with hacked class name and message, to work with JIRA.
    Function : with hacked name
    Traceback : to work around all the manipulation

    Side-effects
    ------------
    May affect the display name of source_func.
    """
    source_func = source_func or (lambda: None)  # dummy function; we're just gonna manipulate the name.
    title = str(title or getattr(exception, 'message', None) or getattr(exception, 'args', [''])[0])
    description = description or str(exception)
    release_stage = release_stage or bugsnag.configuration.release_stage
    exception_name = type(exception).__name__.split('.')[-1]  # strip module paths, get class name

    # Build the title
    formatted_str = exception_name
    if qualifier:
        formatted_str = "{formatted_str} ({qualifier})".format(
            formatted_str=formatted_str, qualifier=qualifier)
    if title:
        if normalize_title:
            # Kill an object pointer, which creates titles
            # that constantly churn
            title = re.sub(r'object at 0x[0-9a-e]+', '', title, flags=re.I)
            # Strip hex numbers
            title = re.sub(r' x[0-9a-e]+', ' #', title, flags=re.I)
            # strip out any numbers from messages, as they tend to cause JIRA churn
            # (e.g. dates, or reports on specific debug info like row counts)
            title = re.sub(r'\d+', '#', title)
        if max_title_length:
            # Shorten the title, to keep it readable within JIRA
            title = title[:max_title_length]
        formatted_str = "{formatted_str}: {title}".format(
            formatted_str=formatted_str, title=title)

    # Build the description
    if description:
        description = "In release stage {release_stage}:\n\n{description}".format(
            description=description, release_stage=release_stage)
    else:
        description = "Error occurred in release stage {release_stage}.".format(
            release_stage=release_stage)

    # Output the string.
    test_exception_class = type(formatted_str, (Exception,), {})  # Create an exception class
    test_exception_class.__module__ = None  # shorten output string
    newex = test_exception_class(description)
    newex.__dict__ = exception.__dict__
    if description:
        try:
            source_func.__name__ = description
        except:  # noqa: E722
            pass  # sometimes this fails (Py3, bound method)

    return newex, source_func, traceback


def _soft_exception_match(exception, exception_type, exception_string):
    """Match exception based on soft exception type match, and regex string match."""
    def soft_contains_type(exception, exception_type):
        if isinstance(exception_type, type):
            # exception_type is an object type, and not just a string
            if isinstance(exception, exception_type):
                return True
            # Convert to string, fall through to base check
            exception_type = getattr(exception_type, '__name__', str(exception_type))
        # Base check: stringified type is found in the exception itself.
        return str(exception_type) in str(exception)

    if not soft_contains_type(exception, exception_type=exception_type):
        # Failed type check
        return False
    # Succeeded type check; return check on string
    return re.search(exception_string, str(exception)) is not None


def match_exception_by_type_or_message(exception, ignorable_exceptions, p_ignore_match=0):
    """
    Returns True with probability p_ignore_match
    if the exception's type or message match an ignorable class of exceptions.

    Parameters
    ----------
    exception : Exception
        The exception to examine
    ignorable_exceptions : iterable
        Iterable of dictionaries containing 'type' and 'message' values
        for exceptions that can be ignored
    p_ignore_match : float
        probability of ignoring a match
        (i.e. emitting when we should ignore)
    """
    if np.random.rand() >= (1 - p_ignore_match):
        # Don't even try the check; if we ignore, just exit.
        # Note: use >=, so that this works for p_ignore_match=(0, 1)
        return False

    for err in ignorable_exceptions:
        if _soft_exception_match(exception, exception_type=err.get("type"), exception_string=err.get("message", "")):
            return True
    return False


def is_ignorable_error(exception, p_ignore_match=0):
    """
    Identify common exceptions that we want to ignore.

    Parameters
    ----------
    exception : Exception
        The exception to examine
    p_ignore_match : float
        probability of ignoring a match
        (i.e. emitting when we should ignore)

    Returns
    -------
    boolean : whether we should ignore the error.
    """
    IGNORABLE_ERRORS = [
        # ETL server ran out of memory
        {'type': OSError,
         'message': 'Memory error'},
        # Old error when two ETLs conflict.
        {'type': '',
         'message': r'table \d+ dropped by concurrent transaction'},
        # DPLAT-1776: happens when server cuts out partway through
        {"type": psycopg2.OperationalError,
         "message": "EOF detected"},
        # DPLAT-1774: happens when server is down
        {"type": psycopg2.OperationalError,
         "message": "could not connect to server"},
        # DPLAT-1772: happens when server cuts out partway through
        {"type": UserWarning,
         "message": "Connection aborted"},
        # DPLAT-1773: Java error that happens when server cuts out partway through
        {"type": "org.postgresql.util.PSQLException",
         "message": "An I/O error occurred while sending to the backend."},
        # DPLAT-1780: Java error that happens when server is down.
        {"type": "org.postgresql.util.PSQLException",
         "message": r"Connection to \S+ refused"},
        # DPLAT-1451: Mongo error that happens when server is down.
        {"type": "com.mongodb.MongoNodeIsRecoveringException",
         "message": "The server is in recovery mode and did not execute the operation"},
    ]

    return match_exception_by_type_or_message(
        exception, ignorable_exceptions=IGNORABLE_ERRORS,
        p_ignore_match=p_ignore_match)  #


def notify_bugsnag_for_jira(exception, source_func=None, traceback=None, qualifier=None, release_stage=None,
                            title=None, description=None, max_title_length=128, normalize_title=True,
                            **kwargs):
    """
    Tunnel data to JIRA via bugsnag.

    Parameters
    ----------
    exception : Exception
        Original exception
    source_func : function
        Function where the error occurred
    traceback : Traceback object
        Traceback
    qualifier : string (optional)
        secondary qualifier to exception class (Default: omitted)
    release_stage : string (optional)
        release stage (Default: pulled from bugsnag)
    title : string (optional)
        title of exception (max length: 128 chars) (Default: exception message)
    description : string (optional)
        main message of exception (Default: exception message)
    max_title_length : int (optional)
        length of title (Default: 128)
    normalize_title : bool (optional)
        whether to normalize our potentially churning title chars (e.g. numbers/dates) (Default: True)
    kwargs : other args to pass to notify_bugsnag

    Side-effects
    ------------
    Notifies bugsnag (depends on integrations)
    """
    exception, source_func, traceback = manipulate_data_for_better_jira_info(
        exception=exception, source_func=source_func, traceback=traceback,
        qualifier=qualifier, release_stage=release_stage, title=title, description=description,
        max_title_length=max_title_length, normalize_title=normalize_title)

    return notify_bugsnag(exception, source_func=source_func, traceback=traceback, **kwargs)

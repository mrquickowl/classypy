
"""
Sets up ssh tunnels, to access resources (e.g. databases).

Examples:
>>> parser = ArgumentParserWithSecrets()
>>> secrets = parser.parse_args()
>>> kwargs, redshift_context = create_ssh_tunnel_context("REDSHIFT", **secrets)
>>> with redshift_context:
>>>    access_database(**kwargs)
"""
import copy
import functools
import logging

import numpy as np
import six
import sshtunnel

from classypy.compat import at_least_python_version
from classypy.devops.pylogging import logger


def _get_prefix_secret(prefix, key, default=None, **secrets):
    """Retrieve secret as {prefix}_{key} (prefix-specific) or {key} (global)."""
    for secret_key in ('{prefix}_{key}'.format(prefix=prefix, key=key), key):
        if secret_key in secrets:
            return secrets[secret_key]
    return default


def needs_tunnel(prefix, **secrets):
    """
    Determines whether this resource requires a proxy.

    Parameters
    ----------
    prefix : string
        args that denote which keys to swap out values for local equivalents.
        (e.g. REDSHIFT or MYSQL)

    Returns
    --------
        Boolean : Whether or not this resource requires a proxy.
    """
    host_key = '{prefix}_HOST'.format(prefix=prefix)
    if secrets.get(host_key) == _get_prefix_secret(prefix=prefix, key='LOCAL_HOST', **secrets):
        return False
    if _get_prefix_secret(prefix=prefix, key='NO_TUNNEL', **secrets):
        return False
    if secrets.get('%s_PORT' % prefix) is None:
        return False  # no port

    has_host_key = host_key in secrets
    has_pem = _get_prefix_secret(prefix=prefix, key='SSH_PEM', **secrets)
    return has_pem and has_host_key


def apply_tunnel(prefix):
    """
    Generate a decorator with a SSH tunnel for the given secrets prefix.

    Parameters
    ----------
    prefix : string
        args that denote which keys to swap out values for local equivalents. (e.g. REDSHIFT or MYSQL)
        If None, the prefix will be determined from the decorated function arguments dynamically

    Returns
    --------
    Decorator that will wrap a function in a ssh tunnel, with the secrets prefix given.
    """
    assert prefix is None or isinstance(prefix, six.string_types)
    prefixx = prefix

    def apply_tunnel_decorator(func):
        """
        Decorate a function with a SSH tunnel

        Parameters
        ----------
        func : function pointer

        Returns
        --------
        Boolean : Whether or not this resource requires a proxy.
        """
        @functools.wraps(func)
        def apply_tunnel_inner_func(*args, **kwargs):
            if prefixx is not None:
                prefix = prefixx
            elif 'prefix' in kwargs:
                prefix = kwargs['prefix']
            elif 'engine' in kwargs:
                prefix = kwargs['engine']
            elif len(args) > 0:
                prefix = args[0].upper()
            else:
                raise ValueError("Prefix nor engine specified.")
            prefix = prefix.upper()

            # Keep a copy of kwargs that doesn't contain 'prefix'
            nonprefix_kwargs = {key: val for key, val in kwargs.items() if key != 'prefix'}
            if not needs_tunnel(prefix=prefix, **nonprefix_kwargs):
                return func(*args, **kwargs)

            # Set up the tunnel before calling.
            logger.info("Setting up {prefix} ssh tunnel...".format(prefix=prefix))
            tunnel_kwargs, ssh_tunnel_context = create_ssh_tunnel_context(prefix=prefix, **nonprefix_kwargs)
            with ssh_tunnel_context:
                return func(*args, **tunnel_kwargs)
        return apply_tunnel_inner_func
    return apply_tunnel_decorator


def create_ssh_tunnel_context(prefix, **secrets):
    """
    Create a SSH tunnel, swapping out {prefix}_HOST and {prefix}_PORT for their local replacements.

    Parameters
    ----------
    prefix : string
        args that denote which keys to swap out values for local equivalents.
        (e.g. REDSHIFT or MYSQL)


    Returns
    ----------
    tuple of (tunnel_secrets, context)

    tunnel_secrets : dict
        copy of secrets, with values swapped out as described above.

    context
        python context that, when used, will open a ssh tunnel.
    """
    tunnel_secrets = copy.copy(secrets)

    # Set up remote secrets.
    tunnel_secrets.update({
        '{prefix}_LOCAL_HOST'.format(prefix=prefix): _get_prefix_secret(
            prefix=prefix, key='LOCAL_HOST', default='127.0.0.1', **secrets),
        '{prefix}_LOCAL_PORT'.format(prefix=prefix): _get_prefix_secret(
            prefix=prefix, key='LOCAL_PORT', default=np.random.randint(8300, 8699), **secrets),
    })

    # Some weirdness I can't explain:
    # warnings are getting ignored, the py.warnings logger has no handlers,
    # and this all goes really poorly for SSH Tunnel Forwarder,
    # which mucks with all this stuff.
    logging.captureWarnings(True)

    warnings_logger = logging.getLogger("py.warnings")
    if not warnings_logger.handlers:
        handler = logging.StreamHandler()
        handler.level = warnings_logger.level
        warnings_logger.addHandler(handler)

    ssh_host = _get_prefix_secret(prefix=prefix, key='SSH_HOST', **tunnel_secrets)
    ssh_port = _get_prefix_secret(prefix=prefix, key='SSH_PORT', default=22, **tunnel_secrets)
    ssh_user = _get_prefix_secret(prefix=prefix, key='SSH_USERNAME', **tunnel_secrets)
    ssh_pem = _get_prefix_secret(prefix=prefix, key='SSH_PEM', **tunnel_secrets)
    ssh_passwd = _get_prefix_secret(prefix=prefix, key='SSH_PASSWORD', **tunnel_secrets)

    context = sshtunnel.SSHTunnelForwarder(
        ssh_address_or_host=(ssh_host, ssh_port),
        ssh_username=ssh_user,
        ssh_pkey=ssh_pem,
        ssh_password=ssh_passwd,
        remote_bind_address=(
            tunnel_secrets['{prefix}_HOST'.format(prefix=prefix)],
            int(tunnel_secrets['{prefix}_PORT'.format(prefix=prefix)])),
        local_bind_address=(
            _get_prefix_secret(prefix=prefix, key='LOCAL_HOST', **tunnel_secrets),
            _get_prefix_secret(prefix=prefix, key='LOCAL_PORT', **tunnel_secrets)),)

    if at_least_python_version(3, 7):
        # Workaround to deal with Python 3.7 hanging using a ssh tunnel:
        # https://github.com/pahaz/sshtunnel/issues/138
        # https://github.com/pahaz/sshtunnel/issues/102
        context.daemon_forward_servers = True
        context.daemon_transport = True

    tunnel_secrets.update({
        ('{prefix}_HOST'.format(prefix=prefix)): _get_prefix_secret(
            prefix=prefix, key='LOCAL_HOST', **tunnel_secrets),
        ('{prefix}_PORT'.format(prefix=prefix)): _get_prefix_secret(
            prefix=prefix, key='LOCAL_PORT', **tunnel_secrets), })
    return tunnel_secrets, context

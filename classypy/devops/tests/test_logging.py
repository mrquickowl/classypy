from classypy.devops import logger
from classypy.devops.pylogging import is_ignorable_error
from classypy.devops.pylogging import manipulate_data_for_better_jira_info as manip4jira
from classypy.devops.pylogging import match_exception_by_type_or_message, setup_bugsnag


def test_logsLevel():
    # Test default (INFO)
    assert not logger.logsLevel(logger.DEBUG)
    assert logger.logsLevel(logger.INFO)
    assert logger.logsLevel(logger.WARNING)
    assert logger.logsLevel(logger.ERROR)

    # Test WARNING
    logger.setLevel(logger.WARNING)
    assert not logger.logsLevel(logger.DEBUG)
    assert not logger.logsLevel(logger.INFO)
    assert logger.logsLevel(logger.WARNING)
    assert logger.logsLevel(logger.ERROR)


def test_manipulate_data_for_better_jira_info_default_descriptions():
    # Exception with no description
    setup_bugsnag(api_key=None, env=None, project_root=None)

    ex, _, _ = manip4jira(ValueError(), title="Hi!")
    assert "ValueError" in ex.__class__.__name__
    assert "Hi!" in ex.__class__.__name__
    assert "Error occurred in release stage local." == str(ex)

    # Exception with implicit description
    ex, _, _ = manip4jira(ValueError('d1'), title="Hi!")
    assert "ValueError" in ex.__class__.__name__
    assert "Hi!" in ex.__class__.__name__
    assert "d1" in str(ex)

    # Exception with explicit description
    ex, _, _ = manip4jira(ValueError('d1'), title="Hi!", description="My momma says...")
    assert "ValueError" in ex.__class__.__name__
    assert "Hi!" in ex.__class__.__name__
    assert "My momma says..." in str(ex)
    assert "d1" not in str(ex)


def test_manipulate_data_for_better_jira_info_default_titles():
    # Exception with no title
    setup_bugsnag(api_key=None, env=None, project_root=None)

    ex, _, _ = manip4jira(ValueError("Hi!"))
    assert "ValueError" in ex.__class__.__name__
    assert "Hi!" in ex.__class__.__name__
    assert str(ex).startswith("In release stage local:")


def test_manipulate_data_for_better_jira_info_normalizing():
    # Exception with no title
    setup_bugsnag(api_key=None, env=None, project_root=None)

    # Normalization
    ex, _, _ = manip4jira(ValueError("Hi! 12345"))
    assert "ValueError" in ex.__class__.__name__
    assert "Hi!" in ex.__class__.__name__
    assert "#" in ex.__class__.__name__
    assert "12345" not in ex.__class__.__name__

    # No normalization
    ex, _, _ = manip4jira(ValueError("Hi! 12345"), normalize_title=False)
    assert "ValueError" in ex.__class__.__name__
    assert "Hi!" in ex.__class__.__name__
    assert "#" not in ex.__class__.__name__
    assert "12345" in ex.__class__.__name__

    # Length limit
    ex, _, _ = manip4jira(ValueError("Hi! abcde"), max_title_length=3)
    assert "ValueError" in ex.__class__.__name__
    assert "Hi!" in ex.__class__.__name__
    assert "abcde" not in ex.__class__.__name__

    # No Length limit
    ex, _, _ = manip4jira(ValueError("Hi! abcde"), max_title_length=None)
    assert "ValueError" in ex.__class__.__name__
    assert "Hi!" in ex.__class__.__name__
    assert "abcde" in ex.__class__.__name__


def test_is_ignorable_error():
    assert not is_ignorable_error(Exception("boo")), (
        "Ignore random stuff")

    assert is_ignorable_error(Exception("OSError: Memory error")), (
        "Detect with type in description")

    assert is_ignorable_error(UserWarning("Connection aborted")), (
        "Detect with type as exception")

    assert is_ignorable_error(UserWarning("Connection aborted, so .. go home")), (
        "Detect with message as substring")

    err = (
        "Error: java.lang.RuntimeException: org.postgresql.util.PSQLException: Connection to "
        "staging-insights-warehousestack-12t3l-dwhredshift.codomrwkg7d1.us-west-2.redshift.amazonaws.com:5439"
        " refused. Check that the hostname and port are correct.")
    assert is_ignorable_error(err), (
        "Detect with message as substring")


def test_match_exception_by_type_or_message():
    for mi in range(10000):
        # Iterate many times, to make sure we're solid on probabilistic computation.

        # Always accept
        assert match_exception_by_type_or_message(
            exception=Exception, ignorable_exceptions=[{'type': Exception}])

        # Always ignore
        assert not match_exception_by_type_or_message(
            exception=Exception, ignorable_exceptions=[{'type': Exception}], p_ignore_match=1)

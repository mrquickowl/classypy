import logging
import warnings

from classypy.devops.tunnel import create_ssh_tunnel_context, needs_tunnel


def test_needs_tunnel():
    assert not needs_tunnel(prefix='REDSHIFT', **{
        'REDSHIFT_HOST': 'abc',
        'REDSHIFT_PORT': 12345,
    }), 'Host with no SSH information => no tunnel'

    assert needs_tunnel(prefix='REDSHIFT', **{
        'REDSHIFT_HOST': 'abc',
        'REDSHIFT_PORT': 12345,
        'SSH_HOST': 'def',
        'SSH_PEM': 'pem',
    }), 'Host with global SSH information => tunnel'

    assert not needs_tunnel(prefix='REDSHIFT', **{
        'REDSHIFT_HOST': 'abc',
        'REDSHIFT_PORT': 12345,
        'REDSHIFT_NO_TUNNEL': True,
        'SSH_HOST': 'def',
        'SSH_PEM': 'pem',
    }), 'Host with global SSH information, but explicitly turned off => no tunnel'

    assert needs_tunnel(prefix='REDSHIFT', **{
        'REDSHIFT_HOST': 'abc',
        'REDSHIFT_PORT': 12345,
        'REDSHIFT_SSH_HOST': 'def',
        'REDSHIFT_SSH_PEM': 'pem',
    }), 'Host with host-specific SSH information => tunnel'

    assert not needs_tunnel(prefix='REDSHIFT', **{
        'REDSHIFT_HOST': 'abc',
        'REDSHIFT_PORT': 12345,
        'MYSQL_SSH_HOST': 'def',
        'MYSQL_SSH_PEM': 'pem',
    }), 'Host with SSH information specific to another host = no tunnel'


def test_warnings_in_tunnel():
    _, context = create_ssh_tunnel_context('REDSHIFT', **{
        'REDSHIFT_HOST': '127.0.0.1',
        'REDSHIFT_PORT': 12345,
        'REDSHIFT_SSH_HOST': '127.0.0.1',
        'REDSHIFT_SSH_PASSWORD': "test",
    })

    with warnings.catch_warnings(record=True) as w:
        # Cause all warnings to always be triggered.
        warnings.simplefilter("always")
        # Trigger a warning.
        warnings.warn('hi')
        # Verify some things
        assert len(w) == 1
        assert issubclass(w[-1].category, UserWarning)
        assert "hi" == str(w[-1].message)
        assert len(logging.getLogger("py.warnings").handlers) > 0
        assert logging.getLogger("py.warnings").handlers[0].level <= 30

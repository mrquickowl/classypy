import sys

from classypy.devops import logger
from classypy.devops.parsers import ArgumentParserWithSecrets, LoggingParser


class TestLoggerParser():
    argv = sys.argv

    def setup(self, args=None):
        logger.setLevel(logger.INFO)
        sys.argv = self.argv[:1] + (args or [])

    def test_logging_level_default(self):
        self.setup()
        parser = LoggingParser()
        args = vars(parser.parse_args())

        # Default
        assert 'log_level' not in args
        assert 'verbose' not in args
        assert logger.level == logger.INFO

    def test_logging_level_updated(self):
        self.setup(args=['--log-level', 'CRITICAL'])
        parser = LoggingParser()
        args = vars(parser.parse_args())

        # Critical
        assert 'log_level' not in args
        assert 'verbose' not in args
        assert logger.level == logger.CRITICAL

    def test_critical_with_verbose(self):
        self.setup(args=['--log-level', 'CRITICAL'])
        parser = LoggingParser()
        parser.add_argument('--verbose', type=int, nargs="?", default=1)
        args = vars(parser.parse_args())

        # Critical
        assert 'log_level' not in args
        assert logger.level == logger.CRITICAL
        assert 'verbose' in args
        assert args['verbose'] == 0

    def test_info_with_verbose(self):
        self.setup(args=['--log-level', 'INFO'])
        parser = LoggingParser()
        parser.add_argument('--verbose', type=int, nargs="?", default=1)
        args = vars(parser.parse_args())

        # Info
        assert 'log_level' not in args
        assert logger.level == logger.INFO
        assert 'verbose' in args
        assert args['verbose'] == 1

    def test_debug_with_verbose(self):
        self.setup(args=['--log-level', 'DEBUG'])
        parser = LoggingParser()
        parser.add_argument('--verbose', type=int, nargs="?", default=1)
        args = vars(parser.parse_args())

        # Debug
        assert 'log_level' not in args
        assert logger.level == logger.DEBUG
        assert 'verbose' in args
        assert args['verbose'] == 2


class TestArgumentParserWithSecrets():
    def setup(self, env='staging', env_file='/file/does/not/exist'):
        self.argv = sys.argv
        sys.argv = ['test.py', '--env-file', env_file, '--env', env]

    def teardown(self):
        sys.argv = self.argv

    def test_basics(self):
        parser = ArgumentParserWithSecrets(auto_fetch_secrets=False)

        args = vars(parser.parse_args())
        assert args['app'] == 'insights'
        assert args['env'] == 'staging'
        assert args['env_file'] == '/file/does/not/exist'
        assert args['region'] == 'us-west-2'
        assert not args['as_admin']
        assert len(args) == 6, 'No secrets'

    def test_allowed_secrets(self, temp_file):
        self.setup(env=None, env_file=temp_file)  # local only

        with open(temp_file, 'w') as fp:
            fp.write("SECRET1 = my_val\n")
            fp.write("SECRET2 = my_val\n")

        parser = ArgumentParserWithSecrets()
        secrets = vars(parser.parse_args())
        assert 'SECRET1' in secrets
        assert 'SECRET2' in secrets

        parser = ArgumentParserWithSecrets(secret_keys=['SECRET1'])
        secrets = vars(parser.parse_args())
        assert len(secrets) == 1
        assert 'SECRET1' in secrets

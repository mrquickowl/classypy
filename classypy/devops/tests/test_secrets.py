from classypy.devops.secrets import find_secrets


def test_explicit_secrets(temp_file):
    with open(temp_file, 'w') as fp:
        fp.write("SECRET = my_val")
    secrets = find_secrets(env_file=temp_file)

    assert 'SECRET' in secrets
    assert secrets['SECRET'] == 'my_val'


def test_find_secrets_file():
    secrets = find_secrets()  # Find the .env file in this path.

    assert 'SECRET' in secrets
    assert secrets['SECRET'] == 'my_env'


def test_allowed_secrets(temp_file):
    with open(temp_file, 'w') as fp:
        fp.write("SECRET1 = my_val\n")
        fp.write("SECRET2 = my_val\n")

    secrets = find_secrets(env_file=temp_file)
    assert 'SECRET1' in secrets
    assert 'SECRET2' in secrets

    secrets = find_secrets(env_file=temp_file, allowed_secrets=['SECRET1'])
    assert len(secrets) == 1
    assert 'SECRET1' in secrets

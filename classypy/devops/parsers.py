"""
Command-line parsers that standardize how we approach applications.

Most common parsers are:
* ArgumentParserWithSecrets - Standardized args / methods for fetching secrets.
* CsvScriptArgumentParser - Standardized ways to load/save CSV (on top of secrets fetching)
"""
import argparse
import os.path as op

import six

from classypy.devops.pylogging import logger
from classypy.devops.secrets import REGION_MAP, find_dotenv, find_secrets
from classypy.util.dirs import caller_dir, data_dir


class EnhancedParser(argparse.ArgumentParser):
    """Parser with popattr"""
    @staticmethod
    def popattr(object, name, **kwargs):
        """"Pop" an attribute value off the object - get the value and delete from the object.

        Side-effects
        ------------
        Removes object.name from object.
        """
        assert not any(kwargs) or (len(kwargs) == 1 and 'default' in kwargs)

        if 'default' in kwargs:
            val = getattr(object, name, kwargs['default'])
            if hasattr(object, name):
                delattr(object, name)
        else:
            val = getattr(object, name)
            delattr(object, name)

        return val


class LoggingParser(EnhancedParser):
    """Parser that understands logging"""
    def __init__(self, *args, **kwargs):
        super(LoggingParser, self).__init__(*args, **kwargs)
        self.add_argument('--log-level', choices=list(logger.level_names.values()), default=None)

    def parse_args(self):
        args = super(LoggingParser, self).parse_args()

        log_level = self.popattr(args, 'log_level')
        if log_level is not None:
            logger.setLevel(getattr(logger, log_level))

            if hasattr(args, 'verbose'):
                # log_level was specified; make sure verbose matches.
                # log_level == INFO => verbose = 1
                # log_level == DEBUG => verbose = 2
                # Note:
                # log_level == WARNING => verbose = 0 :(
                args.verbose = logger.verbosity()

        return args


class ArgumentParserWithSecrets(LoggingParser):
    """A parser for command line arguments and secrets from .env or credstash.

    ArgumentParserWithSecrets is a wrapper around argparse.ArgumentParser that will discover and
    parse secrets from a .env file or credstash, in addition to command line arguments.

    Examples:
    >>> parser = ArgumentParserWithSecrets(description='A short description of the script.')
    >>> parser.add_argument('--some-arg', type=str, default='default_value')

    >>> args = vars(parser.parse_args())  # parse input and convert to dictionary

    >>> args['some_arg']
    'default_value'
    >>> args['SOME_API_KEY']  # assumes SOME_API_KEY is specified in ~/.env or credstash
    'valueoftheapikey'

    """
    ALL_ARGS = ('credstash_table', 'env_file', 'app', 'env', 'region', 'as_admin')
    SERVER_SECRET_SUFFIXES = ('HOST', 'PORT', 'USERNAME', 'PASSWORD', 'SSH_HOST', 'SSH_USERNAME', 'SSH_PEM')

    def __init__(self, prefixes=None, secret_keys=None, keep_args=None,
                 auto_fetch_secrets=True, search_path=None, *args, **kwargs):
        """
        Parameters
        ----------

        prefixes: iterable (default: None)
            List of prefixes to pull connection secrets for. Default: none

        secret_keys: iterable (default: None)
            List of secrets to pull. Default: all

        keep_args: iterable (default: None)
            List of secrets-related keys to expose. Default: none

        auto_fetch_secrets: boolean (default: True)
            Whether to fetch secrets or not.
        """
        assert None in (prefixes, secret_keys), "Can only specify one of prefixes and secret_keys"
        search_path = search_path or caller_dir(frames_above=1)

        super(ArgumentParserWithSecrets, self).__init__(*args, **kwargs)
        self.add_argument('--credstash-table', nargs='?', default=None)
        self.add_argument('--env-file', nargs='?', default=find_dotenv(search_path=search_path))
        self.add_argument('--app', nargs='?', default='insights')
        self.add_argument('--env', nargs='?', default=None)
        self.add_argument('--region', nargs='?', default=None)
        # Add flag for standardized way to upgrade creds
        self.add_argument('--as-admin', action='store_true')

        if prefixes is not None:
            secret_keys = tuple([secret for prefix in prefixes for secret in self.server_secrets(prefix)])
        self.secret_keys = secret_keys
        self.auto_fetch_secrets = auto_fetch_secrets
        if keep_args is None:
            # By default, keep all args if we're not fetching; otherwise eliminate.
            keep_args = tuple() if auto_fetch_secrets else self.ALL_ARGS
        elif isinstance(keep_args, six.string_types):
            keep_args = tuple([keep_args])

        self.del_vars = set(self.ALL_ARGS) - set(keep_args)

    @classmethod
    def server_secrets(cls, prefix):
        """Get list of secret keys for a given service prefix."""
        return ('{prefix}_{suffix}'.format(prefix=prefix, suffix=suffix)
                for suffix in cls.SERVER_SECRET_SUFFIXES)

    def fetch_secrets(self, args=None):
        # Args can be passed during parsing. Or, we could
        # re-parse (with auto_fetch_off)
        if args is None:
            self.auto_fetch_secrets, tmp = False, self.auto_fetch_secrets
            args = self.parse_args()
            self.auto_fetch_secrets = tmp

        secrets = find_secrets(
            env_file=args.env_file,
            credstash_table=args.credstash_table,
            allowed_secrets=self.secret_keys,
            search_default_env_file=False,  # Only find local dotenv file if we pass env_file explicitly
        )

        return secrets

    def parse_args(self):
        args = super(ArgumentParserWithSecrets, self).parse_args()

        if args.env is None and args.env_file is None:
            # env not provided explicitly and no env_file provided or found, assume staging
            args.env = 'staging'

        args.credstash_table = getattr(args, 'credstash_table', None)
        if not args.credstash_table and args.app and args.env:
            args.credstash_table = "{env}-{app}-credstash".format(**vars(args))
            args.region = getattr(args, 'region', None) or REGION_MAP.get(args.env)

        if self.auto_fetch_secrets:
            secrets = self.fetch_secrets(args)

            if args.as_admin:
                # Move admin creds to default connector creds
                admin_keys = [key for key in list(secrets.keys()) if '_ADMIN_' in key]
                if len(admin_keys) == 0:
                    logger.warning('Trying to run as admin, but no admin credentials were found.')

                for key in admin_keys:
                    # REDSHIFT_USERNAME = REDSHIFT_ADMIN_USERNAME
                    secrets[key.replace('_ADMIN_', '_')] = secrets[key]

            for key, val in secrets.items():
                if getattr(args, key, None) is None:
                    setattr(args, key, val)

        # Clean off these vars
        for var_name in self.del_vars:
            delattr(args, var_name)

        return args


class CsvScriptArgumentParser(ArgumentParserWithSecrets):
    """A parser for specifying .csv output location, in addition to command line arguments and secrets.

    CsvScriptArgumentParser adds a standard interface for specifying the location of .csv output
    on top of ArgumentParserWithSecrets. Output location will be set to project_directory/data/processed/csv_file.csv
    by default, assuming the data science cookiecutter directory structure.

    Setting the --force flag to True will ignore classypy.util.caching.csv.cache_dataframe caching,
    and run the script even if the target .csv already exists.

    See also:
    http://drivendata.github.io/cookiecutter-data-science/

    """
    def __init__(self, csv_dir=None, *args, **kwargs):
        kwargs["search_path"] = kwargs.get("search_path") or caller_dir(frames_above=1)

        super(CsvScriptArgumentParser, self).__init__(*args, **kwargs)

        self.add_argument('--csv-file', nargs='?', default=None)
        self.add_argument('--force', action='store_true',
                          help='Regenerate cache files.')
        # Get data dir not for this path, but that of the caller
        self.csv_dir = csv_dir or data_dir(caller_dir(frames_above=1), subdir='processed')

    def parse_args(self):
        args = super(CsvScriptArgumentParser, self).parse_args()
        args.csv_file = args.csv_file and op.join(self.csv_dir, args.csv_file)
        return args


class ReportingParser(EnhancedParser):
    """Parser that understands logging"""
    def __init__(self, *args, **kwargs):
        super(ReportingParser, self).__init__(*args, **kwargs)
        self.add_argument('--force', action='store_true',
                          help='Regenerate results cache files.')
        self.add_argument('--force-data', action='store_true',
                          help='Regenerate data cache files.')

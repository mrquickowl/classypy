"""
Functions for dealing with secrets.

Should rarely be used; use ArgumentParserWithSecrets instead.
"""
import os
import os.path as op

from classypy.devops.pylogging import logger
from classypy.util.dirs import caller_dir

REGION_MAP = {
    'prod': 'us-east-1',
    'staging': 'us-west-2', }


class Secrets(dict):
    """Class that facilitates hiding the printing of secrets."""
    def __repr__(self):
        return '<%s.%s object at %s>' % (
            self.__class__.__module__,
            self.__class__.__name__,
            hex(id(self))
        )


def _key_prefix(key):
    """Return the string before the first '_'."""
    return key.split('_')[0]


def filter_dict(d, allowed_keys):
    return {k: v for k, v in d.items()
            if allowed_keys is None or k in allowed_keys}


def find_dotenv(search_path):
    import dotenv

    cur_path = os.getcwd()
    env_file = None
    try:
        os.chdir(search_path)
        env_file = dotenv.find_dotenv(usecwd=True)
    finally:
        os.chdir(cur_path)
    return env_file


def _region_from_credstash_tablename(credstash_table):
    env = credstash_table.split('-')[0]
    return REGION_MAP.get(env)


def find_secrets(env_file=None, credstash_table=None, region=None,
                 allowed_secrets=None, search_default_env_file=True,
                 search_path=None):
    """
    Finds secrets from credstash and local dotenv secrets file.
    Local secrets overwrite credstash secrets.

    Parameters
    ----------
    env_file : string (optional)
        Location of dotenv secrets file.
    credstash_table : str (optional)
        Name of credstash table to fetch secrets from.
    region : str (optional)
        Name of AWS region for credstash table.
    allowed_secrets : iterable (optional)
        Secret keys that will be returned.
    search_default_env_file : bool (optional)
        If True, search for a dotenv secrets file using find_dotenv. (default: True)
    """
    search_path = search_path or caller_dir(frames_above=1)

    secrets = Secrets()  # unprintable dict

    if credstash_table:
        # Fill with credstash secrets
        import credstash
        region = region or _region_from_credstash_tablename(credstash_table)
        logger.info("Fetching secrets from {table}...".format(table=credstash_table))
        new_secrets = credstash.getAllSecrets(table=credstash_table, region=region)
        logger.info("Done.")
        secrets.update(new_secrets)

    if env_file is None and search_default_env_file:
        env_file = find_dotenv(search_path=search_path)

    if env_file and not op.exists(env_file):
        # Give a good warning if .env file is specified, but not found.
        logger.warning("Could not find .env file at {env_file}".format(env_file=env_file))

    elif env_file:
        # Fill with local/.env secrets (override remote)
        # Avoid filling with environment variables.
        import dotenv
        current_env, os.environ = os.environ, {}
        logger.debug("Using secrets from {env_file}".format(env_file=env_file))
        try:
            dotenv.load_dotenv(env_file, verbose=logger.verbosity(), override=True)
            secrets_to_overwrite = set(secrets.keys()) & set(os.environ.keys())
            if len(secrets_to_overwrite) > 0:
                logger.info("Overwriting {num_overwritten_keys} secrets from local environment file.".format(
                    num_overwritten_keys=len(secrets_to_overwrite)))
            secrets.update(os.environ)
        finally:
            # Always recover the current environment.
            os.environ = current_env

    # Filter secrets
    if allowed_secrets:
        for key in list(secrets.keys()):
            if key not in allowed_secrets:
                del secrets[key]

    return secrets

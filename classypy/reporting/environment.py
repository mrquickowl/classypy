import inspect
import os.path as op

import dill

from classypy.util.dirs import data_dir, get_caller_path


def compute_environment_file_path(frames_above):
    """Return default path for saving an environment file."""
    return op.join(
        data_dir(path=get_caller_path(frames_above=frames_above), subdir='processed'),
        'environment.pkl',
    )


def is_valid_var(key, val):
    """
    Checks if an object in the namespace is valid to be saved in pickle file.

    Parameters
    ----------
    key : An object's name
    val : An object's value

    Returns
    ----------
    True or False if object deemed valid or not
    """
    return (not key.startswith("__")
            and dill.pickles(val)
            and not isinstance(val, is_valid_var.__class__)  # not a function
            and not isinstance(val, type))


def save_environment(file_path=None, local_vars=None, frames_above=1):
    """
    Saves global and local objects to a .pkl file.

    Parameters
    ----------
    file_path : string, optional
        File path to environment output file
    local_vars : dict, optional
        Local variables to save to file.
        If not specified, local vars are pulled from the call stack
    frames_above : int, optional
        If local vars are pulled from the stack, determines
        how many frames above this function's call to pull from.

    Returns
    -------
    None

    Side-effects
    ------------
    Outputs an environment.pkl file with all valid objects saved
    """
    file_path = file_path or compute_environment_file_path(frames_above=frames_above)

    _frame = inspect.stack()[frames_above].frame
    local_vars = local_vars or _frame.f_locals
    env_vars = {key: val for key, val in _frame.f_globals.items() if is_valid_var(key, val)}  # filtered globals
    env_vars.update(**local_vars)

    with open(file_path, 'wb') as fp:
        dill.dump(env_vars, fp)


def load_environment(file_path=None, frames_above=1):
    """
    Loads objects from .pkl file into global namespace.

    Parameters
    ----------
    fn : The .pkl file to be loaded

    Returns
    ----------
    Objects from .pkl file now available in namespace
    """
    file_path = file_path or compute_environment_file_path(frames_above=frames_above)

    with open(file_path, 'rb') as fp:
        env_vars = dill.load(fp)

    frame = inspect.stack()[1].frame
    frame.f_globals.update(**env_vars)
    frame.f_locals.update(**env_vars)

    return env_vars

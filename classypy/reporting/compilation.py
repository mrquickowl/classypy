import glob
import os
import os.path as op
from shutil import copytree, rmtree

from classypy.devops import LoggingParser, logger
from classypy.util.dirs import get_caller_path, reports_dir, src_dir


def compile_report(src_path, dest_path, render_reports_for_knowledge_repo=False, force=True):
    """
    Compile report using R (assumes R in the unix PATH variable).

    Parameters
    ----------
    src_path: The location of the Rmd file

    dest_path: Where the HTML file should be saved (note, if for Knowledge Repo changed to MD)

    render_reports_for_knowledge_repo: bool, optional
        If True renders MD instead of HTML;
        also moves figures to the 'static' directory for rendering with Hugo, and updates
        file paths to find moved figures; this only works with the specific directory
        structure used by Hugo to render the Knowledge Repo

    force: bool, optional
        If True rebuilds existing reports

    Returns
    -------
    Exit status

    Side-effects
    ------------
    A report in HTML or MD form.
    """
    if render_reports_for_knowledge_repo:
        # Knowledge repo expects MD file, but passes HTML file path
        dest_path = dest_path.replace('.html', '.rendered.md')

    if not force and op.exists(dest_path):
        logger.debug("Skipping existing report {dest_path}.".format(dest_path=dest_path))
        return None
    if op.exists(dest_path):
        logger.debug("Deleting existing report {dest_path}.".format(dest_path=dest_path))
        os.remove(dest_path)

    # Generate the report
    if render_reports_for_knowledge_repo:
        # Assumes directory structure [knowledge-repo-path]/content/post/[report-type]/[base_path]/reports/[dest_path]
        reports_path = reports_dir(path=get_caller_path(frames_above=1))
        base_path = op.dirname(reports_path)
        content_path = op.dirname(op.dirname(op.dirname(base_path)))
        # Want post/[report-type]/[base_path]
        base_path_relative = op.relpath(path=base_path, start=content_path)

        # Get file paths for figures: [knowledge-repo-path]/content/post/[report-type]/[base_path]/reports/figures
        figures_path = op.join(reports_path, 'figures')
        # For knowledge repo becomes: [knowledge-repo-path]/static/post/[report-type]/[base_path]/figures
        knowledge_repo_path = op.dirname(content_path)
        figures_path_knowledge_repo = op.join(knowledge_repo_path, 'static', base_path_relative, 'figures')

        if op.isdir(figures_path_knowledge_repo):
            rmtree(figures_path_knowledge_repo)

        copytree(figures_path, figures_path_knowledge_repo)

        # post/[report-type]/[base_path]/figures
        figures_path_knowledge_repo_relative = op.join(base_path, 'figures', '')

        # Build report for Knowledge Repo
        commands = [
            """R -e 'options(render_reports_for_knowledge_repo = TRUE)""",
            """knitr::opts_knit$set(base.dir = normalizePath("static/", mustWork = TRUE), base.url = "/")""",
            """knitr::opts_chunk$set(fig.path = "{figures_path_knowledge_repo_relative}")""",
            """knitr::knit("{src_path}", output = "{dest_path}", encoding = "UTF-8")'"""
        ]
        cmd = '; '.join(commands).format(figures_path_knowledge_repo_relative=figures_path_knowledge_repo_relative,
                                         src_path=src_path, dest_path=dest_path)
        logger.debug("Generating report to {dest_path}.".format(dest_path=dest_path))
        return os.system(cmd)  # nosec
    else:
        cmd = """R -e 'rmarkdown::render("{src_path}", output_file = "{dest_path}")'""".format(
            src_path=src_path, dest_path=dest_path)
        logger.debug("Generating report to {dest_path}.".format(dest_path=dest_path))
        return os.system(cmd)  # nosec


def compile_reports(reports_path=None, render_reports_for_knowledge_repo=False, frames_above=1, force=True):
    """Compile reports in a given directory, using R (assumes in path)"""
    caller_path = reports_dir(path=get_caller_path(frames_above=frames_above))
    reports_path = reports_path or reports_dir(path=caller_path)
    srces_paths = (
        src_dir(path=caller_path),
        src_dir(path=caller_path, subdir="reports")
    )

    for srces_path in srces_paths:
        for report_src_path in glob.glob(op.join(srces_path, '*.Rmd')):
            output_path = op.join(reports_path, op.basename(report_src_path).replace('.Rmd', '.html'))
            compile_report(src_path=report_src_path, dest_path=output_path,
                           render_reports_for_knowledge_repo=render_reports_for_knowledge_repo)


class KnowledgeRepoParser(LoggingParser):
    """Parser that understands knowledge repo compilation"""
    def __init__(self, *args, **kwargs):
        super(KnowledgeRepoParser, self).__init__(*args, **kwargs)
        self.add_argument('--render-reports-for-knowledge-repo', action="store_true")

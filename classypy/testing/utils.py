from datetime import datetime

import numpy as np
import pandas as pd


def _is_nat(val):
    return isinstance(val, datetime) and pd.isna(val)


def _nat_as_nan(val):
    return np.nan if _is_nat(val) else val


def assert_dataframes_are_equal_and_not_empty(left_df, right_df, test_name, skip_cols=tuple(),
                                              test_column_ordering=True):
    """Check all rows and dataframe metadata for equivalence, and that data is actually contained."""
    assert len(left_df) > 0, (
        "{test_name}: Empty result set".format(test_name=test_name))

    non_overlapping_cols = set(left_df.columns).symmetric_difference(set(right_df))
    np.testing.assert_array_equal(list(non_overlapping_cols), [], "No non-overlapping columns")

    if test_column_ordering:
        assert np.array_equal(left_df.columns.values, right_df.columns.values), (
            "{test_name}: Columns (with ordering) should match".format(test_name=test_name))

    assert len(left_df) == len(right_df), (
        "{test_name}: Number of rows should match".format(test_name=test_name))

    for (ri, left_row), (_, right_row) in zip(left_df.iterrows(), right_df.iterrows()):
        for col in [c for c in left_df.columns if c not in skip_cols]:
            msg = "{test_name}: Row {ri}: left_df[{col}] != right_df[{col}]\n{left_val}\n{right_val}".format(
                test_name=test_name, ri=ri, col=col, left_val=left_row[col], right_val=right_row[col])
            np.testing.assert_equal(
                _nat_as_nan(left_row[col]),
                _nat_as_nan(right_row[col]),
                msg)

"""
This file defines test configuration options, so that we can call pytest
with command-line arguments needed to run tests with authentication:

pytest --env-file ~/.env
or
pytest --env prod

Documentation:
https://docs.pytest.org/en/2.7.3/plugins.html

To use, simply import all pytest_* functions from here into the
conftest.py file in your repo.
"""
import logging  # import this, and chardet, so we can suppress its logging
import os.path as op

import pytest
import six
from _pytest import runner

from classypy import query as qt
from classypy.devops.parsers import REGION_MAP, ArgumentParserWithSecrets
from classypy.devops.pylogging import is_ignorable_error, notify_bugsnag_for_jira, setup_bugsnag
from classypy.devops.secrets import find_secrets
from classypy.util.dirs import base_dir

SECRETS = None

logging.getLogger('chardet.charsetprober').setLevel(logging.ERROR)
logging.getLogger('chardet.codingstatemachine').setLevel(logging.ERROR)
logging.getLogger('chardet.universaldetector').setLevel(logging.ERROR)
logging.getLogger('paramiko.transport').setLevel(logging.ERROR)


def _get_options():
    """Add command-line options, from ArgumentParserWithSecrets"""
    secrets_parser = ArgumentParserWithSecrets()
    options = []
    for act in secrets_parser._actions:
        if act.dest not in ArgumentParserWithSecrets.ALL_ARGS:
            continue
        if act.__class__.__name__ in ('_StoreTrueAction'):
            # For whatever reason, store_true options can't be re-created with this method.
            # So, we'll have to skip them.
            # This includes, for example, the --as-admin option.
            continue

        # Add args that were user-added (strings, not tuples)
        kwargs = {arg[0]: arg[1] for arg in act._get_kwargs() if isinstance(arg[0], six.string_types)}
        options.append(kwargs)
    return options


def _get_kwargs(metafunc):
    """Get kwargs from pytest command-line"""
    return {kwargs['dest']: getattr(metafunc.config.option, kwargs['dest'], None)
            for kwargs in _get_options()}


def _get_secrets(metafunc=None):
    """Retrieve secrets from command-line"""
    global SECRETS
    if SECRETS is None and metafunc is not None:
        kwargs = _get_kwargs(metafunc)
        if kwargs['env']:
            kwargs['credstash_table'] = "{env}-{app}-credstash".format(**kwargs)
            kwargs['region'] = REGION_MAP.get(kwargs['env'])

        kwargs.pop('app')
        kwargs.pop('env')

        SECRETS = find_secrets(**kwargs)
    return SECRETS


def pytest_addoption(parser):
    """Get 'option_string' from the argparse parse, and push into the pytest parser."""
    for kwargs in _get_options():
        option_strings = kwargs.pop('option_strings')
        parser.addoption(*option_strings, **kwargs)

    return parser


def pytest_runtest_setup(item):
    """If testing specific tickets, disable any other tests."""
    test_tickets = getattr(item.config.option, 'test_tickets', None)
    test_tickets = test_tickets and test_tickets.split(',') or []

    if test_tickets and 'test_tickets' not in item.fixturenames:
        # Test has no ticket information, and we're running via tickets, so ... skip.
        pytest.skip("Test tickets specified and not matched; skipping test.")

    return item


def pytest_generate_tests(metafunc):
    """Pass dynamically-generated parameters (secrets, test_tickets) to tests."""
    if 'secrets' in metafunc.fixturenames:
        secrets = _get_secrets(metafunc)
        if secrets:
            # Valid secrets; proceed
            metafunc.parametrize("secrets", [secrets])
        else:
            pytest.skip("Could not find secrets.")  # Skips the whole group of tests.

    if 'env' in metafunc.fixturenames:
        env = getattr(metafunc.config.option, 'env')
        env_file = getattr(metafunc.config.option, 'env_file', '')
        env = env or 'prod' if 'prod' in op.basename(env_file) else 'staging'

        metafunc.parametrize("env", [env])

    # Check if testing tickets only
    test_tickets = getattr(metafunc.config.option, 'test_tickets', None)
    test_tickets = test_tickets and test_tickets.split(',') or []
    if 'test_tickets' in metafunc.fixturenames:
        # Let test determine match
        metafunc.parametrize("test_tickets", [test_tickets])

    return metafunc


def pytest_runtest_makereport(item, call):
    """Report test failures to bugsnag."""
    test_report = runner.pytest_runtest_makereport(item, call)

    secrets = _get_secrets(item)
    bugsnag_api_key = secrets.get("BUGSNAG_API_KEY")
    if bugsnag_api_key:
        # Setup
        env = getattr(item.config.option, 'env')
        setup_bugsnag(api_key=bugsnag_api_key, env=env, project_root=base_dir())

        if test_report.outcome == 'failed':
            # Notify bugsnag
            test_name = item.name.split('[')[0]  # Strip junk; after [ character
            assertion_message = getattr(call.excinfo.value, 'message', None)

            report_exception_class_name = call.excinfo.value.__class__.__name__  # exception class name
            if report_exception_class_name == 'AssertionError':
                # Alias AssertionError as TestFailure.
                report_exception_class_name = "TestFailure"
            # Create a new exception type, so that we can control the message.
            report_exception_class = type(report_exception_class_name, (Exception,), {})  # For display purposes
            exception = report_exception_class(assertion_message or test_report.longreprtext)

            if not is_ignorable_error(exception, p_ignore_match=0.01):
                # Emit all "good" errors, with low probability of emitting "ignorable" errors.
                notify_bugsnag_for_jira(
                    exception=exception,
                    source_func=item.function,
                    traceback=call.excinfo.tb,
                    qualifier=test_name,
                    title=assertion_message,
                    description=test_report.longreprtext,
                    metadata={'description': test_report.longreprtext})

    return test_report


class TestWithSecrets(object):
    @classmethod
    def setup_with_secrets(cls, func):
        # NOTE: do not use @functools.wraps(func), so that
        # the func argspec has 'secrets' in it.
        def setup_with_secrets_inner(self, secrets, **kwargs):
            self.setup_secrets(**secrets)
            return func(self, **kwargs)
        return setup_with_secrets_inner

    @classmethod
    def setup_secrets(cls, **secrets):
        qf = qt.generate_query_func(engine='redshift', **secrets)

        @classmethod
        def redshift_qf(cls, q, **kwargs):
            # UGH to wrap the qf as a classmethod;
            # see https://stackoverflow.com/questions/5999575/cant-have-a-function-as-a-class-attribute-in-python
            return qf(q, **kwargs)

        cls.redshift_qf = redshift_qf

    @classmethod
    def skip_in_environment(cls, environment):
        def skip_in_environment_decorator(func):
            # NOTE: do not use @functools.wraps(func), so that
            # the func argspec has 'secrets' in it.
            def skip_in_environment_inner_func(self, secrets, env, *args, **kwargs):
                """
                Parameters
                ----------
                secrets: dict
                List of secrets for connecting to data sources.

                env: string
                The environment the tests will be running against (staging or prod).
                """
                # Re-declare secrets here, so that pytest sees secrets and hits our conftest code.
                self.setup_secrets(**secrets)

                if environment != env:
                    return func(self, *args, **kwargs)

                pytest.skip("Skipping test in {env} environment.".format(env=env))

            return skip_in_environment_inner_func
        return skip_in_environment_decorator

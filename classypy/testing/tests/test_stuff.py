import copy

import pandas as pd
import pytest

from classypy.testing import assert_dataframes_are_equal_and_not_empty


def test_assert_empty_fails():
    empty_df = pd.DataFrame(columns=['hi'])
    with pytest.raises(AssertionError):
        assert_dataframes_are_equal_and_not_empty(
            empty_df, empty_df, test_name="test_assert_empty_fails")


def test_assert_one_row_two_cols_succeeds():
    left_df = pd.DataFrame([{'col1': 1, 'col2': 2}])
    right_df = copy.copy(left_df)
    assert_dataframes_are_equal_and_not_empty(
        left_df, right_df, test_name="test_assert_one_row_two_cols_succeeds")


def test_assert_two_rows_two_cols_succeeds():
    left_df = pd.DataFrame([{'col1': 1, 'col2': 2}, {'col1': 3, 'col2': 4}])
    right_df = copy.copy(left_df)
    assert_dataframes_are_equal_and_not_empty(
        left_df, right_df, test_name="test_assert_two_rows_two_cols_succeeds")


def test_assert_two_rows_two_cols_different_fails():
    left_df = pd.DataFrame([{'col1': 1, 'col2': 2}, {'col1': 3, 'col2': 4}])
    right_df = copy.copy(left_df)
    right_df.iloc[0]['col1'] = -1
    with pytest.raises(AssertionError):
        assert_dataframes_are_equal_and_not_empty(
            left_df, right_df, test_name="test_assert_two_rows_two_cols_different_fails")

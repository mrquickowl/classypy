import os
import os.path as op
import tempfile

import pytest


@pytest.fixture
def temp_file():
    temp_file = tempfile.mkstemp()[1]
    os.remove(temp_file)  # start clean

    yield temp_file

    if op.exists(temp_file):
        os.remove(temp_file)

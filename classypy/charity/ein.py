"""
getein.py : self-contained module for scraping and
fetching charity metadata.

Currently contains one method: get_charity_ein()
See docstring for details.
"""
import re

import numpy as np
import pandas as pd

from classypy.compat import numeric_types
from classypy.devops import logger
from classypy.io.propublica import ProPublicaDataset
from classypy.ml.distances import levenshtein
from classypy.util.etl import to_ascii


def _score_pro_publica_results(org_name, res, dist_threshold):
    """Scores each name for distance vs. search results.

    Throws
    ------
    ValueError
      * If res (response) is invalid
      * If no distance is above threshold
    """
    org_names = []
    org_eins = []
    dists = []
    shortest_dist = np.inf

    try:
        for result in res['filings']:
            name = result['organization']['name']
            ein = result['ein']
            # Ordered by best results according to ProPublica Nonprofit Explorer
            if name not in org_names:
                org_names.append(name)
                org_eins.append(ein)
                dists.append(levenshtein(org_name, name, thresh=shortest_dist))
                shortest_dist = min(shortest_dist, dists[-1])
    except KeyError:
        raise ValueError(
            "ProPublica API JSON attributes "
            "may have changed; see docstring for details.")

    if len(dists) == 0:
        raise ValueError("No results found in response.")
    elif dist_threshold is not None and shortest_dist > dist_threshold:
        raise ValueError("Min distance above threshold: {shortest_dist} > {dist_threshold}".format(
            shortest_dist=shortest_dist, dist_threshold=dist_threshold))

    return org_names, org_eins, dists


def get_charity_ein(org_name, query_params=None, dist_threshold=None,
                    cache_dir=None, force=False):
    """
    Utilizes ProPublica's Nonprofit Explorer API to get a list of charities
    whose names most closely match 'org_name' parameter. Returns name and
    EIN of charity that most closely matches 'org_name'; and possibly None
    if the smallest observed Levenshtein distance (string similiarity metric)
    between 'org_name' and the charities' names is less than a specified
    threshold.

    Parameters
    ----------
    org_name : str
        Charity name
    query_params : dict, optional
        Dictionary of ProPublica API query parameters.
        See: https://projects.propublica.org/nonprofits/api
    dist_threshold : int, optional
        Maximum acceptable Levenshtein distance; will return a null
        result: ("", None) if the smallest Levenshtein distance exceeds
        this threshold. dist_threshold should be greater than or equal
        to 0.
    cache_dir : str, optional
        If specified, response data will be cached to that local directory.
    force : bool, optional
        Whether to force re-download of ProPublica data. (Default: False)

    Returns
    -------
    (name, ein, dist) : (str, int, int)
        Name, EIN, and distance of charity whose name most closely matches
        the 'org_name' parameter (by ProPublica judgment).

    """
    org_name = org_name.upper().strip()  # Standardize
    dataset = ProPublicaDataset(data_dir=cache_dir)
    res = dataset.fetcher.fetch_json(  # we want the in-deep response.
        org_name=org_name, query_params=query_params, force=force)

    try:
        org_names, org_eins, dists = _score_pro_publica_results(
            org_name=org_name, res=res, dist_threshold=dist_threshold)
    except ValueError as ex:
        logger.warning("Failed to get valid pro_publica results: {ex}".format(ex=ex))
        return None, None, None

    # Returns the 'best' match according to ProPublica Nonprofit Explorer
    indices = np.argsort(dists)
    best_idx = indices[0]
    return (org_names[best_idx].strip(), org_eins[best_idx], dists[best_idx])


def ein_finder(eins, known_eins, remove_unknown=True):
    known_eins_df = pd.DataFrame(index=known_eins)
    eins_df = pd.DataFrame(data=dict(ein=eins))
    clean_df = eins_df.join(known_eins_df, on='ein', how='inner' if remove_unknown else 'left')
    return clean_df['ein'].values


def ein_cleaner(ein, default_val=np.nan):
    """
    Takes various inputs and formats as XX-XXXXXXX. Inputs can be:
    * An actual positive number.
    * A string of at least 9 digits, and garbage (grabs first 9 digits).
    * A string of less than 9 digits, but digits only (left-pads with zeros).
    """
    if pd.isnull(ein):
        return default_val

    if isinstance(ein, numeric_types):
        # Ints
        if ein < 0:
            return default_val
        if ein >= 1E10:
            return default_val
        ein = '%09d' % int(ein)

    # Messy strings: matches first 9 digits of
    # strings with at least 9 digits
    try:
        ein = str(to_ascii(ein))
    except:  # noqa: E722
        return default_val

    match = re.match(r'[^\d]*(\d)' * 9 + r'.*', ein)
    if match:
        digits = ''.join(match.groups())
        return digits[:2] + '-' + digits[2:]

    # Int strings: matches strings of
    # between 5 and 8 digits.
    # Why 5? Anything less, and is it
    # really an EIN? :P
    match = re.match(r'^\d{5,8}$', ein)
    if match:
        digits = '%09d' % int(ein)
        return digits[:2] + '-' + digits[2:]

    return default_val


def clean_eins(eins, known_eins=None, remove_unknown=False,
               cache_dir=None, default_val=np.nan):
    """
    Applies ein_cleaner to DataFrame containing uncleaned EINs.

    NOTE: When applying this to all known EINs in production,
    this function may take >30mins to run on a local machine.
    """
    logger.debug("Cleaning %d EINs." % len(eins))

    # Reformat EINs
    eins = [ein_cleaner(ein, default_val=default_val) for ein in eins]

    # Filter out bad EINs
    if not remove_unknown:
        clean_eins = eins
    else:
        if not known_eins:
            # Hide recursive reference
            from classypy.io.irs import IrsEINDataset
            ein_ref_df = IrsEINDataset(data_dir=cache_dir).fetch(verbose=logger.verbosity)
            known_eins = list(ein_ref_df.ein)

        clean_eins = ein_finder(eins, known_eins, remove_unknown=remove_unknown)

    if len(eins) != len(clean_eins):
        logger.debug("Removed %d / %d rows with bad EINs." % (
            len(eins) - len(clean_eins), len(eins)))
    else:
        logger.debug("Done")

    return clean_eins


def fetch_ein(org_name, org_state=None, cache_dir=None, dist_threshold=None,
              force=False):
    """Wrapper around get_charity_ein."""
    if dist_threshold is None:
        dist_threshold = int(np.floor(len(org_name) / 3.0))

    best_match_name, best_match_ein, best_match_dist = get_charity_ein(
        org_name,
        query_params={'state[id]': org_state} if org_state else None,
        dist_threshold=dist_threshold,
        cache_dir=cache_dir,
        force=force)

    # Normalize the EIN format
    best_match_ein = ein_cleaner(best_match_ein)
    if best_match_ein:
        logger.info("Got EIN %s for org_name %s (matched name=%s)" % (
            best_match_ein,
            '%s (state=%s)' % (org_name, org_state) if org_state else org_name,
            best_match_name))

    return best_match_ein, best_match_name

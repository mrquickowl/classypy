import pandas as pd

from classypy.charity.ein import ein_cleaner


def test_ein_cleaner():
    assert pd.isnull(ein_cleaner('asdf')), 'Garbage is null'

    assert '12-3456789' == ein_cleaner('123456789'), 'EIN w/ no dash'
    assert '12-3456789' == ein_cleaner('12-3456789'), 'EIN w/ dash'
    assert '12-3456789' == ein_cleaner(' 12-3456789 '), 'EIN w/ trim whitespace'
    assert '12-3456789' == ein_cleaner(' 123456789 '), 'EIN w/ no dash, trim whitespace'
    assert '12-3456789' == ein_cleaner('123 456789'), 'EIN w/ no dash, central whitespace'
    assert '12-3456789' == ein_cleaner('1234567890'), 'EIN w/ 10 digits'

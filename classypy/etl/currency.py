import datetime

import requests
import six
from dateutil.parser import parse

BASE_URLS = {
    'prod': 'https://api.classy.org/currency/1.0',
    'staging': 'https://stagingapi.stayclassy.org/currency/1.0',
}


class CurrencyClient(object):
    """
    Simple client for accessing the Currency API.
    https://bitbucket.org/stayclassy/currency

    Note: exchange rates are available from 2017-04-05 onward (as of 2018-02-01).
    """

    def __init__(self, api_key, host=None, env=None):
        if host is not None and env is not None:
            raise ValueError('Only pass host or env arguments, not both.')
        if host is not None:
            self.base_url = "https://{host}/currency/1.0".format(host=host)
        else:
            env = env or 'staging'
            self.base_url = BASE_URLS[env]

        self.api_key = api_key

    def _get(self, path):
        """Makes a get request for the given path, and returns the result in JSON format."""
        url = "{base_url}{path}".format(base_url=self.base_url, path=path)
        headers = {'X-API-KEY': self.api_key}
        response = requests.get(url, headers=headers)
        response.raise_for_status()  # raise an exception if we don't get a 200 response
        return response.json()

    @classmethod
    def _prepare_date(cls, date):
        if date is None:
            return datetime.datetime.utcnow().date()
        if isinstance(date, datetime.datetime):
            return date.date()
        if isinstance(date, datetime.date):
            return date
        if isinstance(date, six.string_types):
            return parse(date).date()

        raise ValueError('The date argument must be of type date, datetime, string or None.')

    def get_exchange_rate(self, from_currency, to_currency='USD', date=None):
        """Gets an exchange rate for a given date.

        Parameters
        ----------
        from_currency : string
            The currency to convert from.
        to_currency : string (default: USD)
            The currency to convert to.
        date : string, date, or datetime (default: today)
            The date of the exchange rate.
        """
        date = self._prepare_date(date)
        path = "/{from_currency}/{to_currency}/{date}".format(
            from_currency=from_currency, to_currency=to_currency, date=date)
        return self._get(path)['rate']

    def get_available_currencies(self, date=None):
        """Gets the currencies available for exchange rate calculation for a given date.

        Parameters
        ----------
        date : string, date, or datetime (default: today)
            The date to return currencies for.
        """
        date = self._prepare_date(date)
        path = "/currencies/{date}".format(date=date)
        return self._get(path)['currencies']

    def get_all_currencies(self):
        """Returns all the currencies that have ever been available through the currency API."""
        return self._get('/currencies/all')['currencies']

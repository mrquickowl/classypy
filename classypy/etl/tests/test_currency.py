import datetime

import pytest
from requests.exceptions import HTTPError

from classypy.etl import CurrencyClient


class CurrencyClientSetup(object):
    @classmethod
    def setup_with_secrets(cls, func):
        # NOTE: do not use @functools.wraps(func), so that
        # the func argspec has 'secrets' in it.
        def setup_with_secrets_inner(self, secrets, **kwargs):
            self.setup_secrets(**secrets)
            return func(self, **kwargs)
        return setup_with_secrets_inner

    @classmethod
    def setup_secrets(cls, **secrets):
        cls.client = CurrencyClient(api_key=secrets.get('CURRENCY_API_KEY'), host=secrets.get('CURRENCY_API_HOST'))


class TestCurrencyClient(CurrencyClientSetup):
    # Taken from https://classydev.atlassian.net/browse/FRS-7395
    # Keys are currency codes, values are (name, precision) tuples
    _FRS_DEFAULT_CURRENCIES = {
        'AUD': ('Australian Dollar', 2),
        'CHF': ('Swiss Franc', 2),
        'EUR': ('Euro', 2),
        'GBP': ('British Pound Sterling', 2),
        'HKD': ('Hong Kong Dollar', 2),
        'JPY': ('Japanese Yen', 0),
        'USD': ('US Dollar', 2),
    }
    # Number of currencies as of 01/30/2018
    CURRENCY_COUNT = 184

    @classmethod
    def get_frs_default_currency_codes(cls):
        return cls._FRS_DEFAULT_CURRENCIES.keys()

    @classmethod
    def get_frs_default_currency_name(cls, code):
        return cls._FRS_DEFAULT_CURRENCIES[code][0]

    @classmethod
    def get_frs_default_currency_precision(cls, code):
        return cls._FRS_DEFAULT_CURRENCIES[code][1]

    @CurrencyClientSetup.setup_with_secrets
    def test_get_exchange_rate(self):
        # Historic exchange rates should not change, so we can test the value
        test_cases = (
            ('EUR', '2017-04-13', 1.06163),
            ('EUR', datetime.datetime(year=2017, month=4, day=13), 1.06163),
            ('CAD', '2018-01-30', 0.810653),
        )

        for currency, date, expected_rate in test_cases:
            rate = self.client.get_exchange_rate(currency, date=date)
            assert expected_rate == pytest.approx(rate)

    @CurrencyClientSetup.setup_with_secrets
    def test_get_exchange_rate_today(self):
        rate = self.client.get_exchange_rate('GBP')
        assert type(rate) is float

    def test_non_authenticated(self):
        bad_client = CurrencyClient(api_key='bad_api_key')
        with pytest.raises(HTTPError):
            bad_client.get_exchange_rate('EUR')

    @CurrencyClientSetup.setup_with_secrets
    def test_bad_requests(self):
        test_cases = (
            ('EUR', 'LOL', None),  # Nonexistant currency
            ('EUR', 'USD', '2030-01-01'),  # Date in the future
        )

        for from_currency, to_currency, date in test_cases:
            with pytest.raises(HTTPError):
                self.client.get_exchange_rate(from_currency, to_currency=to_currency, date=date)

    @CurrencyClientSetup.setup_with_secrets
    def test_bidirectional(self):
        aud_to_usd_rate = self.client.get_exchange_rate(from_currency='AUD', to_currency='USD', date='2017-12-31')
        usd_to_aud_rate = self.client.get_exchange_rate(from_currency='USD', to_currency='AUD', date='2017-12-31')
        assert aud_to_usd_rate == pytest.approx(1 / usd_to_aud_rate)
        assert usd_to_aud_rate == pytest.approx(1 / aud_to_usd_rate)

    @CurrencyClientSetup.setup_with_secrets
    def test_get_currencies(self):
        test_cases = ('2017-09-12', None, '2018-01-03')
        for date in test_cases:
            available_currency_codes = self.client.get_available_currencies(date)
            # Check that at least a core set of currencies are available
            frs_default_currency_codes = self.get_frs_default_currency_codes()
            assert all([currency_code in available_currency_codes for currency_code in frs_default_currency_codes])

    @CurrencyClientSetup.setup_with_secrets
    def test_get_all_currencies(self):
        all_currencies = self.client.get_all_currencies()
        for currency in all_currencies:
            for key in ('currency_code', 'is_disabled', 'name', 'precision'):
                assert key in currency

        assert len(all_currencies) >= self.CURRENCY_COUNT  # Currencies should not be dropped

        # Collect data on the FRS default currencies
        frs_default_currencies = [currency for currency in all_currencies
                                  if currency['currency_code'] in self.get_frs_default_currency_codes()]

        # All FRS default currencies should be present
        assert len(frs_default_currencies) == len(self.get_frs_default_currency_codes())

        for currency in frs_default_currencies:
            # All FRS default currencies should be enabled
            assert currency['is_disabled'] == 0

            # FRS default currency name and precision should not change
            currency_code = currency['currency_code']
            assert currency['name'] == self.get_frs_default_currency_name(currency_code)
            assert currency['precision'] == self.get_frs_default_currency_precision(currency_code)

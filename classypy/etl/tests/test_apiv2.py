from classypy.etl import APIv2Client


class APIv2ClientSetup(object):
    @classmethod
    def setup_with_secrets(cls, func):
        # NOTE: do not use @functools.wraps(func), so that
        # the func argspec has 'secrets' in it.
        def setup_with_secrets_inner(self, secrets, **kwargs):
            self.setup_secrets(**secrets)
            return func(self, **kwargs)
        return setup_with_secrets_inner

    @classmethod
    def setup_secrets(cls, **secrets):
        cls.client = APIv2Client(client_id=secrets.get('APIV2_CLIENT_ID'),
                                 client_secret=secrets.get('APIV2_CLIENT_SECRET'),
                                 host=secrets.get('APIV2_HOST'))


class TestClassyAPIClient(APIv2ClientSetup):
    @APIv2ClientSetup.setup_with_secrets
    def test_init(self):
        """Test that the client is properly initialized. Mostly a smoke test to ensure secrets are present."""
        assert self.client.client_id is not None
        assert self.client.client_secret is not None
        assert self.client.access_token is not None

    @APIv2ClientSetup.setup_with_secrets
    def test_credential_sets(self):
        """Test that the credential sets endpoint returns some sensible data."""
        expected_keys = (  # More keys could be added in the future, but we don't expect any to disappear
            'activity_wall',
            'campaign_manager',
            'global_admin',
            'id',
            'member',
            'member_id',
            'organization',
            'organization_id',
            'reporting_access'
        )
        creds = self.client.get_credential_sets(organization_id=34, max_pages=1)
        assert len(creds) > 0
        example_credential_set = creds[0]
        missing_keys = set(expected_keys) - set(example_credential_set.keys())
        assert len(missing_keys) == 0

import requests


class APIv2Client(object):
    """Simple client for accessing APIv2."""
    DEFAULT_MAX_PAGES = 100

    def __init__(self, client_id, client_secret, host):
        self.client_id = client_id
        self.client_secret = client_secret
        self.base_url = "https://{host}".format(host=host)

        self.access_token = self._fetch_token()

    def _fetch_token(self):
        url = "{base_url}/oauth2/auth".format(base_url=self.base_url)
        data = {
            'grant_type': 'client_credentials',
            'client_id': self.client_id,
            'client_secret': self.client_secret
        }
        resp = requests.post(url, data=data)
        resp.raise_for_status()
        return resp.json()['access_token']

    def _get(self, url, retries=1):
        headers = {
            'Authorization': "Bearer {access_token}".format(access_token=self.access_token)
        }
        resp = requests.get(url, headers=headers)
        if resp.status_code == 401 and resp.json().get('error') == 'invalid_token':
            # Access token might have expired, refresh and retry
            if retries > 0:
                self.access_token = self._fetch_token()
                return self._get(url, retries=retries - 1)
        resp.raise_for_status()
        return resp

    def _fetch_collection_of_resources(self, resource, resource_id=None, child_resource=None,
                                       max_pages=DEFAULT_MAX_PAGES):
        if int(resource_id is not None) + int(child_resource is not None) == 1:
            raise ValueError('The resource_id and child_resource arguments '
                             'must be used together to fetch child resources.')

        url = "{base_url}/2.0/{resource}".format(base_url=self.base_url, resource=resource)
        if child_resource is not None:
            url += "/{resource_id}/{child_resource}".format(resource_id=resource_id, child_resource=child_resource)

        resp = self._get(url).json()
        resources = resp['data']
        pages_fetched = 1  # Keep track of pages fetched to prevent an APIv2 bug from causing infinite pages
        while resp['current_page'] < resp['last_page'] and pages_fetched < max_pages:
            resp = self._get(resp['next_page_url']).json()
            resources += resp['data']
            pages_fetched += 1
        return resources

    def get_credential_sets(self, organization_id, max_pages=DEFAULT_MAX_PAGES):
        """Retrieve all the Credential Sets for a specified Organization."""
        return self._fetch_collection_of_resources(
            resource='organizations',
            resource_id=organization_id,
            child_resource='credential-sets',
            max_pages=max_pages
        )

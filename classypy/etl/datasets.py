"""
Download IRS 990 returns for three organizations
in 2015 and 2016.
"""
import warnings
from datetime import datetime

import pandas as pd

from classypy.charity.ein import ein_cleaner
from classypy.io.core.datasets import Dataset
from classypy.io.irs import IRS990Dataset, select_form_by_filing_year, select_latest_form
from classypy.text import text_from_disk
from classypy.util.caching import pickle_dataframe
from classypy.util.etl import dict_of_lists_to_list_of_dicts, unflatten_dict
from classypy.util.execution import run_in_parallel


class EnterpriseSalesForm990Dataset(Dataset):
    """Dataset that pulls Form 990 features indicative of online fundraising potential,
    as requested by Enterprise Sales
    """
    FEATURE_NAMES = [
        'ein',
        # Part VII.B.1
        'contractors.name',
        'contractors.service name',
        'contractors.compensation',
        'CntrctRcvdGreaterThan100KCnt',  # Part VII.B.2
        # Part VIII
        'ContriRptFundraisingEventAmt',  # Part VIII.1c
        'GovernmentGrants',  # Part VIII.1e
        'AllOtherContributionsAmt',  # Part VIII.1f
        'NoncashContributionsAmt',  # Part VIII.1g
        # Part IX
        'FeesForServicesProfFundraising/TotalAmt',  # Part IX.11e
        'AdvertisingGrp/FundraisingAmt',  # Part IX.12.D
        'InformationTechnologyGrp/FundraisingAmt',  # Part IX.14.D
        # Schedule G
        'IRS990ScheduleG/MailSolicitationsInd',  # G.1a
        'IRS990ScheduleG/EmailSolicitationsInd',  # G.1b
        'IRS990ScheduleG/PhoneSolicitationsInd',  # G.1c
        'IRS990ScheduleG/InPersonSolicitationsInd',  # G.1d
        'IRS990ScheduleG/SolicitationOfNonGovtGrantsInd',  # G.1e
        'IRS990ScheduleG/SolicitationOfGovtGrantsInd',  # G.1f
        'IRS990ScheduleG/SpecialFundraisingEventsInd',  # G.1g
        'IRS990ScheduleG/AgrmtProfFundraisingActyInd',  # G.2a
        'IRS990ScheduleG/FundraiserActivityInfoGrp/OrganizationBusinessName/BusinessNameLine1Txt',
        'IRS990ScheduleG/FundraiserActivityInfoGrp/ActivityTxt',
        'IRS990ScheduleG/FundraiserActivityInfoGrp/FundraiserControlOfFundsInd',
        'IRS990ScheduleG/FundraiserActivityInfoGrp/GrossReceiptsAmt',
        'IRS990ScheduleG/FundraiserActivityInfoGrp/RetainedByContractorAmt',
        'IRS990ScheduleG/FundraiserActivityInfoGrp/NetToOrganizationAmt',
        'IRS990ScheduleG/FundraisingEventInformationGrp/Event1Nm',
        'IRS990ScheduleG/FundraisingEventInformationGrp/GrossReceiptsEvent1Amt',
        'IRS990ScheduleG/FundraisingEventInformationGrp/CharitableContriEvent1Amt',
        'IRS990ScheduleG/FundraisingEventInformationGrp/GrossRevenueEvent1Amt',
        'IRS990ScheduleG/FundraisingEventInformationGrp/Event2Nm',
        'IRS990ScheduleG/FundraisingEventInformationGrp/GrossReceiptsEvent2Amt',
        'IRS990ScheduleG/FundraisingEventInformationGrp/CharitableContriEvent2Amt',
        'IRS990ScheduleG/FundraisingEventInformationGrp/GrossRevenueEvent2Amt',
        'IRS990ScheduleG/FundraisingEventInformationGrp/OtherEventsTotalCnt',
        'IRS990ScheduleG/FundraisingEventInformationGrp/GrossReceiptsOtherEventsAmt',
        'IRS990ScheduleG/FundraisingEventInformationGrp/CharitableContriOtherEventsAmt',
        'IRS990ScheduleG/FundraisingEventInformationGrp/GrossRevenueOtherEventsAmt',
    ]
    FEATURE_PATHS = [
        'Filer/EIN',
        # Part VII.B.1
        'ContractorCompensationGrp/ContractorName/BusinessName/BusinessNameLine1Txt',
        'ContractorCompensationGrp/ServicesDesc',
        'ContractorCompensationGrp/CompensationAmt',
        'CntrctRcvdGreaterThan100KCnt',  # Part VII.B.2
        # Part VIII
        'ContriRptFundraisingEventAmt',  # Part VIII.1c
        ('GovernmentGrantsAmt', 'GovernmentGrants'),  # Part VIII.1e
        'AllOtherContributionsAmt',  # Part VIII.1f
        'NoncashContributionsAmt',  # Part VIII.1g
        # Part IX
        'FeesForServicesProfFundraising/TotalAmt',  # Part IX.11e
        'AdvertisingGrp/FundraisingAmt',  # Part IX.12.D
        'InformationTechnologyGrp/FundraisingAmt',  # Part IX.14.D
        # Schedule G
        'IRS990ScheduleG/MailSolicitationsInd',  # G.1a
        'IRS990ScheduleG/EmailSolicitationsInd',  # G.1b
        'IRS990ScheduleG/PhoneSolicitationsInd',  # G.1c  TODO validate xpath is correct
        'IRS990ScheduleG/InPersonSolicitationsInd',  # G.1d
        'IRS990ScheduleG/SolicitationOfNonGovtGrantsInd',  # G.1e
        'IRS990ScheduleG/SolicitationOfGovtGrantsInd',  # G.1f TODO validate xpath is correct
        'IRS990ScheduleG/SpecialFundraisingEventsInd',  # G.1g
        'IRS990ScheduleG/AgrmtProfFundraisingActyInd',  # G.2a
        'IRS990ScheduleG/FundraiserActivityInfoGrp/OrganizationBusinessName/BusinessNameLine1Txt',
        'IRS990ScheduleG/FundraiserActivityInfoGrp/ActivityTxt',
        'IRS990ScheduleG/FundraiserActivityInfoGrp/FundraiserControlOfFundsInd',
        'IRS990ScheduleG/FundraiserActivityInfoGrp/GrossReceiptsAmt',
        'IRS990ScheduleG/FundraiserActivityInfoGrp/RetainedByContractorAmt',
        'IRS990ScheduleG/FundraiserActivityInfoGrp/NetToOrganizationAmt',
        'IRS990ScheduleG/FundraisingEventInformationGrp/Event1Nm',
        'IRS990ScheduleG/FundraisingEventInformationGrp/GrossReceiptsEvent1Amt',
        'IRS990ScheduleG/FundraisingEventInformationGrp/CharitableContriEvent1Amt',
        'IRS990ScheduleG/FundraisingEventInformationGrp/GrossRevenueEvent1Amt',
        'IRS990ScheduleG/FundraisingEventInformationGrp/Event2Nm',
        'IRS990ScheduleG/FundraisingEventInformationGrp/GrossReceiptsEvent2Amt',
        'IRS990ScheduleG/FundraisingEventInformationGrp/CharitableContriEvent2Amt',
        'IRS990ScheduleG/FundraisingEventInformationGrp/GrossRevenueEvent2Amt',
        'IRS990ScheduleG/FundraisingEventInformationGrp/OtherEventsTotalCnt',
        'IRS990ScheduleG/FundraisingEventInformationGrp/GrossReceiptsOtherEventsAmt',
        'IRS990ScheduleG/FundraisingEventInformationGrp/CharitableContriOtherEventsAmt',
        'IRS990ScheduleG/FundraisingEventInformationGrp/GrossRevenueOtherEventsAmt',
    ]
    assert len(FEATURE_PATHS) == len(FEATURE_NAMES)

    def __init__(self, data_dir=None, **kwargs):
        super(EnterpriseSalesForm990Dataset, self).__init__(data_dir=data_dir, **kwargs)
        self.irs_dataset = IRS990Dataset()

    @pickle_dataframe  # dataframe contains objects
    def parse_and_clean_990(self, xml_content):
        """Parse desired fields from 990, then clean"""
        # Parse data
        parser = self.irs_dataset.get_bs_xml_obj(xml_content, throw_errors=True)
        data = {name: self.irs_dataset.extract_path(parser, field_name=name, field_paths=[path])
                for name, path in zip(self.FEATURE_NAMES,
                                      self.FEATURE_PATHS)}

        # Construct objects from lists of properties
        data = unflatten_dict(data)
        try:
            data['contractors'] = dict_of_lists_to_list_of_dicts(data['contractors'])
        except Exception as ex:  # noqa
            # Contractors should be list of dicts; otherwise indicates garbage
            data['contractors'] = []

        # Clean data
        data['ein'] = ein_cleaner(data['ein'])

        # Convert 990 indicators
        indicator_fields = [field for field in data.keys() if field.endswith('Ind')]
        for field in indicator_fields:
            data[field] = data[field] == 'X'  # convert to boolean; True if value is 'X'

        return pd.DataFrame(data=[data])

    def fetch(self, eins, filing_years=None, verbose=1, force=False):
        """
        Fetch base and extended 990 features.
        Use either the latest 990 available or specified filing years.

        Parameters:
        -----------
        eins : list-like
            List of unique EINs to fetch 990 data for.
        filing_years : list-like (optional)
            List specifying filing years that correspond to EINs passed.

        Returns:
        --------
         : pd.DataFrame
            DataFrame containing tax data for specified eins and chosen filing years.
        """
        def _load_and_parse_990_from_disk(fil):
            # Thin wrapper around parse_990, for run_in_parallel
            if verbose > 0:
                print("Parsing {fil}...".format(fil=fil))
            xml_content = text_from_disk(fil, disk_encoding='utf-8-sig', errors='ignore')
            pkl_file = fil.replace('.xml', '.enterprise-sales.pkl')
            return self.parse_and_clean_990(xml_content=xml_content, pkl_file=pkl_file, force=force)

        # Fetch IRS data
        max_year = int(max(filing_years)) if filing_years is not None else datetime.now().year - 1
        df_irs_base = self.irs_dataset.fetch(
            eins=eins,
            years=tuple(range(2013, max_year)),
            verbose=verbose,
            force=force,
        )
        if filing_years is None:
            df_irs_base = select_latest_form(df_irs_base)
        else:
            df_irs_base = select_form_by_filing_year(df_irs_base, eins=eins, filing_years=filing_years)

        # Parse results
        if verbose > 0:
            print("Parsing 990 properties...")
        sales_data = run_in_parallel(
            func=_load_and_parse_990_from_disk,
            params=[{'fil': path} for path in df_irs_base['src_file'].values],
            return_errors=True)

        exceptions = [ex for ex in sales_data if isinstance(ex, Exception)]
        if exceptions:
            # Just warn on download exceptions; don't abort the whole thing
            warnings.warn("Exceptions while downloading data: {exes}".format(
                exes="\n".join([str(ex) for ex in exceptions])))

        df_sales = pd.concat([df for df in sales_data if not isinstance(df, Exception)])

        # Augment dataset
        df_sales['num_contractors'] = df_sales['contractors'].apply(len)

        return df_irs_base.merge(df_sales, how='left', on='ein')

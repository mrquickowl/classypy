import json
import warnings

import requests
from requests_oauthlib import OAuth2Session

from classypy.devops import logger
from classypy.util.caching import cache_as_json


class PayClient(object):
    """
    Simple client for accessing the Pay API.
    For public API endpoints, see:
    https://pay.classy.org/apidoc/
    Private API endpoints were explained locally; can also be found in code:
    https://bitbucket.org/stayclassy/classypay/src/master/routes/
    """
    MAX_LIMIT = 25

    OAUTH_DATA = {
        'scopes': [],
        'token_url': 'https://{host}/oauth2/auth',
        'refresh_url': 'https://{host}/oauth2/auth'
    }

    HOSTS = {
        'prod': 'pay.classy.org',
        'staging': 'staging-pay.stayclassy.org',
    }

    def __init__(self, authorization_response, host=None, env=None, max_retries=2):
        # Store data so that we can create an OAUTH2 client on-demand.
        assert int(host is None) + int(env is None) == 1, 'Must specify host or env'
        self.authorization_response = authorization_response
        self.max_retries = max_retries
        self.host = host or self.HOSTS[env]
        self.client = None

    def _get_client(self, force=False):
        """Retrieve or generate an OAUTH2 client."""
        if self.client and not force:
            # Use cached client
            return self.client

        # Use requests to get the oauth token;
        # couldn't figure out how to Pay's superuser flow
        # via the requests OAuth2 managed session.
        #
        # Calling this authorization_response, because
        # that's where this fits into the standard workflow;
        # see: http://requests-oauthlib.readthedocs.io/en/latest/oauth2_workflow.html#web-application-flow
        pay_auth = json.loads(self.authorization_response)
        resp = requests.post(
            self.OAUTH_DATA['token_url'].format(host=self.host),
            data={'client_id': pay_auth['token'], 'client_secret': pay_auth['secret']})
        resp.raise_for_status()
        token = resp.json()

        # We can't auto-refresh, so don't even provide that info.
        self.client = OAuth2Session(client_id=pay_auth['token'], token=token)
        return self.client

    def get(self, url, *args, **kwargs):
        """
        Wraps OAuth2Session.get, so that we can:

        * Handle unusual aspects of Pay (e.g. 404s on empty listings, using a "meta" flag to get all the data)
        * Handle token expiration gracefully.
        """
        client = self._get_client()
        retries = kwargs.pop('retries', 0)

        try:
            # Auto-add "meta" to get all data.
            meta_url = "{url}{sep}meta=true".format(url=url, sep="?" if "?" not in url else "&")
            resp = client.get(meta_url, *args, **kwargs)
            if resp.status_code == 404:
                # Handle 404s and translate to None.
                # This is intended for listings (where 404 is a bit unusual),
                # but also works well for retrieving objects.
                logger.debug("404 not found for {meta_url}".format(meta_url=meta_url))
                return None
            resp.raise_for_status()

        except requests.exceptions.HTTPError as ex:
            if retries >= self.max_retries:
                # Too many tries, raise the error.
                raise
            if ex.response.status_code not in (401, 504):
                # Unexpected error code; raise the error.
                raise
            if ex.response.status_code == 401:
                # Token expired; reset client to refresh our token.
                self.client = None
            # Try again! Either token was bad, or gateway timeout.
            return self.get(url, retries=retries + 1, *args, **kwargs)

        try:
            return resp.json()
        except ValueError as ex:
            # Could not parse json; raise warning, move on
            warnings.warn("{ex} (content={content})".format(ex=ex, content=resp.content))
            return {}

    def get_paginated(self, base_url, offset, limit=MAX_LIMIT, desc=None, *args, **kwargs):
        """Retrieve the specified page."""
        if limit > self.MAX_LIMIT or limit < 1:
            raise ValueError("Invalid limit: {limit}".format(limit=limit))
        if desc:
            logger.info("Retrieving {limit} {desc} ({start} - {end})...".format(
                desc=desc, limit=limit, start=offset, end=offset + limit - 1))

        url = "{url}{sep}limit={limit}&offset={offset}".format(
            url=base_url, limit=limit, offset=offset,
            sep='?' if '?' not in base_url else '&')
        return self.get(url, *args, **kwargs) or []

    @cache_as_json
    def retrieve_applications(self, limit=MAX_LIMIT, offset=0):
        return self.get_paginated(
            base_url="https://{host}/application".format(host=self.host),
            limit=limit, offset=offset, desc='apps')

    @cache_as_json
    def retrieve_processor(self, app_id, processor_id):
        logger.info("Retrieving payment processor {processor_id}...".format(processor_id=processor_id))
        return self.get("https://{host}/processor/{processor_id}?appId={app_id}".format(
            host=self.host, app_id=app_id, processor_id=processor_id)) or {}

    @cache_as_json
    def retrieve_processors(self, app_id, limit=MAX_LIMIT, offset=0):
        return self.get_paginated(
            base_url="https://{host}/processor?appId={app_id}".format(host=self.host, app_id=app_id),
            limit=limit, offset=offset,
            desc='payment processors for {app_id}'.format(app_id=app_id))

    @cache_as_json
    def retrieve_recurring_profiles(self, app_id, limit=MAX_LIMIT, offset=0):
        return self.get_paginated(
            base_url="https://{host}/recurring?appId={app_id}".format(host=self.host, app_id=app_id),
            limit=limit, offset=offset,
            desc='recurring profiles for {app_id}'.format(app_id=app_id))

    @cache_as_json
    def retrieve_recurring_history(self, app_id, profile_id, limit=MAX_LIMIT, offset=0):
        return self.get_paginated(
            base_url="https://{host}/recurringHistory/recurring/{profile_id}?appId={app_id}".format(
                host=self.host, profile_id=profile_id, app_id=app_id),
            limit=limit, offset=offset,
            desc='recurring history for profile {profile_id}'.format(profile_id=profile_id))

    @cache_as_json
    def retrieve_transactions(self, app_id, limit=MAX_LIMIT, offset=0):
        return self.get_paginated(
            base_url="https://{host}/transaction?appId={app_id}".format(host=self.host, app_id=app_id),
            limit=limit, offset=offset,
            desc='transactions for {app_id}'.format(app_id=app_id))

    @cache_as_json
    def retrieve_transaction_history(self, app_id, transaction_id, limit=MAX_LIMIT, offset=0):
        return self.get_paginated(
            base_url="https://{host}/transactionHistory/transaction/{transaction_id}?appId={app_id}".format(
                host=self.host, app_id=app_id, transaction_id=transaction_id),
            limit=limit, offset=offset,
            desc='transaction history for transaction {transaction_id}'.format(transaction_id=transaction_id))

    @cache_as_json
    def retrieve_payer(self, app_id, payer_id):
        logger.info("Retrieving payer data for {payer_id}...".format(payer_id=payer_id))
        return self.get(
            "https://{host}/payer/{payer_id}?appId={app_id}".format(
                host=self.host, app_id=app_id, payer_id=payer_id)) or {}

    @cache_as_json
    def retrieve_processor_result(self, app_id, result_id):
        logger.info("Retrieving processor results data for {result_id}...".format(result_id=result_id))
        result = self.get(
            "https://{host}/processorResult/{result_id}?appId={app_id}".format(
                host=self.host, app_id=app_id, result_id=result_id)) or {}

        if result:
            # Id, app_id not set on object; let's set it!
            result['id'] = result.get('id', result_id)
            result['appId'] = result.get('appId', app_id)

        return result

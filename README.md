Classypy is an internal repository of shared Python functions. The intent is to encapsulate functionality that spans repos.

Functionality is divided by subdirectory:

* `io` - A set of `Datasets` and `Fetchers` for documenting external datasets, retrieving the data / files, and returning them in a clean, consistent, and massaged format.
* `ml` - Machine learning functions
* `query` - Using ssh tunnels with queries / running devops-type queries on redshift.
* `util` - Utility functions (e.g. defining & accessing common repo structures)
* `viz` - Visualization functions

# Installation

## Dependencies (OSX)

* Install [https://brew.sh/](Homebrew)
* `brew install postgres` (required to install `pycopg2`)
* `brew install openssl` (required to install `pycopg2`)
* `brew install gcc` (required to install `scipy`)
* `brew install coreutils` (required to run test bash scripts)
* `echo "export LIBRARY_PATH=$LIBRARY_PATH:/usr/local/opt/openssl/lib/" >> ~/.bash_profile`

## End-user installation

`pip install git+ssh://git@bitbucket.org/stayclassy/classypy.git#classypy`

Extras are optional subpackage dependencies to install, and include:

* `etl` - functions for classy ETLs
* `ml` - functions for machine learning
* `viz` - functions for plotting & visualiation
* `complete` - all extras

To install, run: `pip install git+ssh://git@bitbucket.org/stayclassy/classypy.git#classypy[{comma-delimited-list-of-extras}]`


## Developers

1. `git clone git@bitbucket.org/stayclassy/classypy.git`
2. `pip install -r requirements.txt`
3. Add the directory the code was cloned to (e.g. `~/code/classypy`) to your `PYTHONPATH` environment variable.
4. Install our [Markdown linter](https://classy.atlassian.net/wiki/spaces/DI/pages/263962/R+Style+Guide#RMarkdownStyleGuide-Setup)
5. `python -c "import nltk; nltk.download('stopwords')"`  (if this fails with an SSL error, run "/Applications/Python 3.7/Install Certificates.command" first (see [https://stackoverflow.com/a/42890688](here)))


# Testing

Run `./bash/test.bash`. This will run linting and unit tests.


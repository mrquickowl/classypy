import os.path as op
import sys

from setuptools import find_packages, setup
PACKAGES = find_packages()  # isort:stay
# Get version and release info, which is all stored in classypy/version.py  # isort:stay
package_dir = op.join(op.dirname(op.abspath(__file__)), 'classypy')  # isort:stay
sys.path = [package_dir] + sys.path  # isort:stay
import version  # isort:stay

opts = dict(name=version.NAME,
            maintainer=version.MAINTAINER,
            maintainer_email=version.MAINTAINER_EMAIL,
            description=version.DESCRIPTION,
            long_description=version.LONG_DESCRIPTION,
            keywords=version.KEYWORDS,
            url=version.URL,
            download_url=version.DOWNLOAD_URL,
            license=version.LICENSE,
            classifiers=version.CLASSIFIERS,
            author=version.AUTHOR,
            author_email=version.AUTHOR_EMAIL,
            platforms=version.PLATFORMS,
            version=version.VERSION,
            packages=PACKAGES,
            package_data=version.PACKAGE_DATA,
            python_requires='>=2.7.6',
            install_requires=version.REQUIRES,
            dependency_links=version.DEPENDENCY_LINKS,
            extras_require=version.EXTRAS_REQUIRE,
            zip_safe=False)


if __name__ == '__main__':
    setup(**opts)
